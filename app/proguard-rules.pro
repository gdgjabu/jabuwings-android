# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\Falade James\AppData\Local\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keep class android.support.v4.** { *; }
-keep interface android.support.v4.** { *; }

-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }

# Allow obfuscation of android.support.v7.internal.view.menu.**
# to avoid problem on Samsung 4.2.2 devices with appcompat v21
# see https://code.google.com/p/android/issues/detail?id=78377
-keep class !android.support.v7.internal.view.menu.**,android.support.** {*;}
-keep class com.android.volley.** { *; }
-keep class butterknife.** { *;}
-keep class com.jabuwings.widgets.SweetAlert.** { *;}
-keep class com.nightonke.boommenu.** { *;}
-keep interface com.nightonke.boommenu.** { *;}
-keep class **$$ViewInjector {*;}
-keep class com.squareup.okhttp.** {*; }
-keep interface com.squareup.okhttp.** { *; }
-keep class com.parse.** {*; }
-keep interface com.parse.** { *; }
-keep class com.tooleap.sdk.** { *; }
-keep interface com.tooleap.sdk.** { *; }

-keep class opennlp.tools.** { *; }
-keep interface opennlp.tools.** { *; }

-keep class com.github.paolorotolo.appintro.** { *; }
-keep interface com.github.paolorotolo.appintro.** { *; }


-keep class com.github.mikephil.charting.** { *; }
-keep interface com.github.mikephil.charting.** { *; }


-keepattributes *Annotation*

-keepclasseswithmembernames class *{
    @butterknife.* <fields>;
}
-keepclasseswithmembernames class *{
    @butterknife.* <methods>;
}

-keepclasseswithmembernames class *{
    @ com.nightonke.boommenu.* <fields>;
}
-keepclasseswithmembernames class *{
    @ com.nightonke.boommenu.* <methods>;
}
-dontwarn butterknife.internal.**
-dontwarn java.nio.file.Files
-dontwarn java.nio.file.Path
-dontwarn java.nio.file.OpenOption
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn com.squareup.okhttp.**
-dontwarn okio.**
-dontwarn com.parse.**
-dontwarn com.tooleap.sdk.**
-dontwarn opennlp.tools.**
-dontwarn com.github.paolorotolo.appintro.**
# For RX JAVA
-dontwarn sun.misc.**

-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
   long producerIndex;
   long consumerIndex;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}

-dontnote rx.internal.util.PlatformDependent
# For RX JAVA