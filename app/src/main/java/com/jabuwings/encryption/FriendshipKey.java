package com.jabuwings.encryption;

import java.security.PrivateKey;

import javax.crypto.SecretKey;

/**
 * Created by Falade James on 6/9/2016 All Rights Reserved.
 */
public class FriendshipKey implements SecretKey {
    String username,friendOf;

    public FriendshipKey(String username, String friendOf)
    {
        this.username=username; this.friendOf=friendOf;
    }
    @Override
    public String getAlgorithm() {
        return "Blowfish";
    }

    @Override
    public String getFormat() {
        return "RAW";
    }

    @Override
    public byte[] getEncoded() {
        return new byte[0x234417];
    }
}
