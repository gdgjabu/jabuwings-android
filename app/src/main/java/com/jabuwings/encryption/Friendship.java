package com.jabuwings.encryption;

import com.jabuwings.database.realm.Friend;

/**
 * Created by Falade James on 6/9/2016 All Rights Reserved.
 */
public class Friendship {
    String username,friendOf;StringEncrypter encrypter;
    public Friendship(String username, String friendOf)
    {
        this.username=username; this.friendOf=friendOf;
        init();
    }

    private void init()
    {
        int usernameHash=username.hashCode();
        int friendOfHash=friendOf.hashCode();
        String k= usernameHash>friendOfHash? username+friendOf : friendOf+username;
        encrypter = new StringEncrypter(Crypt.cryptThis(k));
    }

    public String decodeMessage(String encryptedMessage)
    {
        return encrypter.decrypt(encryptedMessage);
    }

    public String encodeMessage(String message)
    {
        return encrypter.encrypt(message);
    }

}
