package com.jabuwings.encryption;

import android.util.Log;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * Created by Falade James on 11/5/2015 All Rights Reserved.
 */
public class Crypt {
    Cipher eCipher;
    Cipher dCipher;
    public Crypt(SecretKey key) throws Exception
    {
        eCipher=Cipher.getInstance("DES");
        dCipher=Cipher.getInstance("DES");
        eCipher.init(Cipher.ENCRYPT_MODE,key);
        dCipher.init(Cipher.ENCRYPT_MODE,key);

    }



    public static String getSHA256(String text)
    {

        MessageDigest md=null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        md.update(text.getBytes());

        byte byteData[] = md.digest();
/*
        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }*/


        //convert the byte to hex format method 2
        StringBuffer hexString = new StringBuffer();
        for (int i=0;i<byteData.length;i++) {
            String hex=Integer.toHexString(0xff & byteData[i]);
            if(hex.length()==1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
   public static String cryptThis(String value)
   {
        return getSHA256(getSHA256(getSHA256(value)));



   }







}
