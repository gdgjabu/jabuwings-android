package com.jabuwings.streaming;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.management.App;
import com.jabuwings.management.Const;
import com.jabuwings.views.main.JabuWingsActivity;
import com.parse.ParseConfig;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static com.jabuwings.streaming.Channels.Movies.retrieveVideoFrameFromVideo;

public class SeasonEpisodes extends JabuWingsActivity {

    @InjectView(R.id.rvEpisodes)
    RecyclerView recycler;
    String thumbnailUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_season_episodes);
        ButterKnife.inject(this);
        setUpToolbar(null);
        thumbnailUrl=getIntent().getStringExtra("thumb");

        String path =getIntent().getStringExtra("path");
        int noOfEpisodes = getIntent().getIntExtra("no", 1);
        int seasonNo = getIntent().getIntExtra("seasonNo", 1);
        String format=getIntent().getStringExtra("format");
        setTitle("Season "+seasonNo);

        String sNo="";
        if(seasonNo>10)
            sNo="0"+seasonNo;
        else sNo=String.valueOf(seasonNo);


        JSONArray jsonArray= new JSONArray();
        for(int i=1; i<=noOfEpisodes; i++)
        {
            String p=format.replace("#",sNo);
            String eNo="";
            if(noOfEpisodes>10)
                eNo="0"+i;
            else eNo=String.valueOf(i);
            p=p.replace("*",eNo);

            try {
                JSONObject object = new JSONObject();
                object.put("name", "Episode " + i);
                object.put("path",p);

            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }


        }

        recycler.setAdapter(new Adapter(jsonArray));
        recycler.setLayoutManager(new GridLayoutManager(this,3));




    }

    public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder>{

        JSONArray episodes;
        public Adapter(JSONArray m)
        {
            episodes=m;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(getApplicationContext()).inflate(R.layout.movies_list_item,parent,false));
        }

        @Override
        public void onBindViewHolder(Adapter.ViewHolder holder, int position) {
            holder.root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        JSONObject movie = (JSONObject) v.getTag();
                            startActivity(new Intent(getApplicationContext(), Watch.class).putExtra("path", movie.getString("path")));

                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                }
            });
            try {
                JSONObject movie = episodes.getJSONObject(position);
                holder.root.setTag(movie);
                holder.title.setText(movie.getString(Const.NAME));
                boolean isSeason= movie.getBoolean("seasoned");
                if(isSeason)
                {
                    Picasso.with(getApplicationContext()).load(thumbnailUrl.replace("localhost", App.url)).into(holder.thumb);
                }

            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return episodes.length();
        }

        public class ViewHolder extends RecyclerView.ViewHolder
        {
            @InjectView(R.id.ivMovieThumb)
            ImageView thumb;
            @InjectView(R.id.tvMovieTitle)
            TextView title;
            View root;
            public ViewHolder(View v)
            {
                super(v);
                root=v;
                ButterKnife.inject(this,v);
            }
        }
    }
}
