package com.jabuwings.streaming;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.jabuwings.R;
import com.jabuwings.streaming.library.SessionBuilder;
import com.jabuwings.streaming.library.gl.SurfaceView;
import com.jabuwings.streaming.library.rtsp.RtspServer;
import com.jabuwings.utilities.NetworkUtils;
import com.jabuwings.views.main.JabuWingsActivity;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import java.util.HashMap;

public class RecordLive extends JabuWingsActivity {

    private final static String TAG = "MainActivity";

    private SurfaceView mSurfaceView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_record_live);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mSurfaceView = (SurfaceView) findViewById(R.id.surface);

        // Sets the port of the RTSP server to 1234
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putString(RtspServer.KEY_PORT, String.valueOf(1234));
        editor.commit();

        // Configures the SessionBuilder
        SessionBuilder.getInstance()
                .setSurfaceView(mSurfaceView)
                .setPreviewOrientation(90)
                .setContext(getApplicationContext())
                .setAudioEncoder(SessionBuilder.AUDIO_NONE)
                .setVideoEncoder(SessionBuilder.VIDEO_H264);

        // Starts the RTSP server
        this.startService(new Intent(this,RtspServer.class));


        HashMap<String,Object> params=manager.getDefaultCloudParams();
        params.put("url", NetworkUtils.getIPAddress(true)+":1234");
        ParseCloud.callFunctionInBackground("registerLiveStream", params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {
                if(e==null)
                {
                    manager.shortToast("You are now live");
                }
                else{
                    manager.errorToast("Unable to register you");
                }
            }
        });
    }
}
