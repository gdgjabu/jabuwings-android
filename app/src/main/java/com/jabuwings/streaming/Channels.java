package com.jabuwings.streaming;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jabuwings.R;
import com.jabuwings.management.App;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.floating.FloatingActionButton;
import com.parse.Parse;
import com.parse.ParseConfig;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class Channels extends JabuWingsActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    static Manager manager;

    static ParseConfig parseConfig= ParseConfig.getCurrentConfig();
    static String wifiUrl=parseConfig.getString("wifiURL");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channels);
        manager=new Manager(this);
        getWifiUrl();
        ch("10.10.10");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_record);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Channels.this,RecordLive.class));
            }
        });

    }


    private void ch(final String subnet)
    {

        new AsyncTask<Void,Void,Void>()
        {
            @Override
            protected Void doInBackground(Void... params) {
                int timeout=1000;
                for(int i=1; i<255; i++)
                {
                    String host=subnet+"."+i;
                    try {
                        if (InetAddress.getByName(host).isReachable(timeout)) {
                            Log.d("wifi", host + " is available");
                        }
                        else{
                            Log.d("wifi", host + " is unavailable");
                        }
                    }
                    catch ( IOException e)
                    {
                        Log.d("wifi", host + " "+i+" caused exception "+e.getMessage());
                    }
                }
                return null;
            }
        }.execute();

    }

    public String intToIp(int i)
    {
        return ((i>>24)&0xFF)+"."+
                ((i>>16)&0xFF)+"."+
                ((i>>8)&0xFF)+"."+
                (i&0xFF);

    }

    public String convertToIp(int i)
    {
        return (i&0xFF)+"."+
                ((i>>8)&0xFF)+"."+
                ((i>>16)&0xFF)+"."+
                ((i>>24)&0xFF);
    }

    private void getWifiUrl()
    {
        Log.d("wifi","called");
        new AsyncTask<Void,Void,String>(){
            @Override
            protected String doInBackground(Void... params) {
                String s_dns1 ;
                String s_dns2;
                String s_gateway;
                String s_ipAddress;
                String s_leaseDuration;
                String s_netmask;
                String s_serverAddress;
                TextView info;
                DhcpInfo d;
                WifiManager wifii;
                wifii = (WifiManager)getApplicationContext(). getSystemService(Context.WIFI_SERVICE);
                d = wifii.getDhcpInfo();
                s_dns1 = "DNS 1: " + convertToIp(d.dns1);
                s_dns2 = "DNS 2: " + convertToIp(d.dns2);
                s_gateway = "Default Gateway: " + convertToIp(d.gateway);
                s_ipAddress = "IP Address: " + convertToIp(d.ipAddress);
                s_leaseDuration = "Lease Time: " + String.valueOf(d.leaseDuration);
                s_netmask = "Subnet Mask: " + String.valueOf(d.netmask);
                s_serverAddress = "Server IP: " + String.valueOf(d.serverAddress);

                Log.d("Wifi",s_dns1+"\n"+s_dns2+"\n"+s_gateway+"\n"+s_ipAddress+"\n"+s_leaseDuration+"\n"+s_netmask+"\n"+s_serverAddress);

        /*d.dns1 is host ip

        Now get connected ips by this*/


                String connections = "";
                InetAddress host;
                try {
                    host = InetAddress.getByName(convertToIp(d.dns1));
                    byte[] ip = host.getAddress();
                    for(int i = 1; i <= 254; i++)
                    { ip[3] = (byte) i;
                        InetAddress address = InetAddress.getByAddress(ip);
                        if(address.isReachable(100))
                        { System.out.println(address + " machine is turned on and can be pinged");
                            connections+= address+"\n";
                        } else if(!address.getHostAddress().equals(address.getHostName()))
                        {
                            System.out.println(address + " machine is known in a DNS lookup");

                        } } } catch(UnknownHostException e1) {
                    e1.printStackTrace();
                } catch(IOException e) {
                    e.printStackTrace();
                }
                Log.d("Wifi",connections);
                System.out.println(connections);

                return null;
            }


            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                //Log.d("wifi","url is "+s);
            }
        }.execute();



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_channels, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class Movies extends Fragment{

        View rootView;
        RecyclerView recyclerView;
        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_movies, container, false);
            recyclerView = (RecyclerView) rootView.findViewById(R.id.rvMovies);

            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(getActivity());
            String url;
            if(wifiUrl==null) {
                wifiUrl = "10.0.2.2";
            }
            url = wifiUrl + "/movies.json";


// Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            manager.shortToast(response);
                            processMovies(response);



                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    manager.errorToast("Couldn't load movies");
                    error.printStackTrace();
                }
            });
// Add the request to the RequestQueue.
            queue.add(stringRequest);
            return rootView;
        }
        private void processMovies(String r){
            try {
                JSONObject json = new JSONObject(r);
                JSONArray movies = json.getJSONArray("movies");
                recyclerView.setAdapter(new Adapter(movies));
                recyclerView.setLayoutManager(new GridLayoutManager(getContext(),3));

            }
            catch (JSONException e)
            {
               e.printStackTrace();
            }
        }

        public static Bitmap retrieveVideoFrameFromVideo(String videoPath)
        {
            Bitmap bitmap = null;
            MediaMetadataRetriever mediaMetadataRetriever = null;
            try
            {
                mediaMetadataRetriever = new MediaMetadataRetriever();
                    mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
                bitmap = mediaMetadataRetriever.getFrameAtTime();
            } catch (Exception e) {
                e.printStackTrace();
                return null;

            } finally {
                if (mediaMetadataRetriever != null) {
                    mediaMetadataRetriever.release();
                }
            }
            return bitmap;
        }
        public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder>{

            JSONArray movies;
            public Adapter(JSONArray m)
            {
                movies=m;
            }

            @Override
            public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return new ViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.movies_list_item,parent,false));
            }

            @Override
            public void onBindViewHolder(ViewHolder holder, int position) {
                holder.root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            final JSONObject movie = (JSONObject) v.getTag();

                            if( movie.getBoolean("seasoned"))
                            {

                                final int noOfSeasons=movie.getInt("no");
                                if(noOfSeasons==1)
                                startActivity(new Intent(getContext(), SeasonEpisodes.class).putExtra("path", movie.getString("path")).putExtra("format",movie.getString("format")).putExtra("no",movie.getInt("eNo")).putExtra("thumb",movie.getString("thumb")));
                                else{
                                    List<String> seasons=new ArrayList<String>();
                                    for(int i=1; i<=noOfSeasons; i++)
                                    {
                                        seasons.add("Season "+i);
                                    }

                                    new MaterialDialog.Builder(getActivity()).items(seasons).itemsCallback(new MaterialDialog.ListCallback() {
                                        @Override
                                        public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                                            try {
                                                startActivity(new Intent(getContext(), SeasonEpisodes.class).putExtra("path", movie.getString("path")).putExtra("no",movie.getInt("eNo")).putExtra("seasonNo",position+1).putExtra("format",movie.getString("format")).putExtra("thumb",movie.getString("thumb")));
                                            }
                                            catch (JSONException e)
                                            {
                                                e.printStackTrace();
                                            }
                                        }
                                    }).show();
                                }

                            }
                            else startActivity(new Intent(getContext(), Watch.class).putExtra("path", movie.getString("path")));

                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                });
                try {
                    JSONObject movie = movies.getJSONObject(position);
                    holder.root.setTag(movie);
                    holder.title.setText(movie.getString(Const.NAME));
                    boolean isSeason= movie.getBoolean("seasoned");
                    if(isSeason)
                    {
                        Picasso.with(getContext()).load(movie.getString("thumb").replace("localhost", App.url)).into(holder.thumb);
                    }
                    else {
                        Bitmap thumb = retrieveVideoFrameFromVideo((movie.getString("path")));
                        holder.thumb.setImageBitmap(thumb);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }

            @Override
            public int getItemCount() {
                return movies.length();
            }

            public class ViewHolder extends RecyclerView.ViewHolder
            {
                @InjectView(R.id.ivMovieThumb)
                ImageView thumb;
                @InjectView(R.id.tvMovieTitle)
                TextView title;
                View root;
                public ViewHolder(View v)
                {
                    super(v);
                    root=v;
                    ButterKnife.inject(this,v);
                }
            }
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class Live extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public Live() {
        }


        View rootView;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_live, container, false);
            RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.rvLive);

            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(getActivity());
            String url ="http://10.10.10.1/live.json";;

// Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {



                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
// Add the request to the RequestQueue.
            queue.add(stringRequest);

            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position)
            {
                case 0:
                    return new Live();
                case 1:
                    return new Movies();
                default:return null;
            }

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Live";
                case 1:
                    return "Movies";
            }
            return null;
        }
    }
}
