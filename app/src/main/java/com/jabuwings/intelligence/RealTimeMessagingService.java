package com.jabuwings.intelligence;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;

import com.jabuwings.database.DatabaseManager;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.intermediary.NotificationManager;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.FeedAdapter;
import com.jabuwings.utilities.ConnectionDetector;
import com.jabuwings.views.main.Chat;
import com.jabuwings.views.main.HubChat;
import com.parse.FindCallback;
import com.parse.LiveQueryException;
import com.parse.ParseException;
import com.parse.ParseLiveQueryClient;
import com.parse.ParseLiveQueryClientCallbacks;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.parse.SubscriptionHandling;

import java.util.List;

import static com.jabuwings.management.App.TAG;
import static com.jabuwings.management.App.parseLiveQueryClient;
import static com.jabuwings.management.Const.FRIEND_ID;
import static com.jabuwings.management.Const.HUB_ID;
import static com.jabuwings.management.Const.NOTIFICATION_INTENT;
import static com.jabuwings.management.Const.SENDER;


/**
 * Created by Falade James on 1/1/2016 All Rights Reserved.
 */
public class RealTimeMessagingService extends Service {
    private static String LOG_TAG=RealTimeMessagingService.class.getSimpleName();
    private Handler handler;
     Looper looper;
     boolean keep=true;
     Manager manager;
     boolean alive = true;
    ConnectionDetector connectionDetector;
    Context context;
     DatabaseManager helper;



    @Override
    public void onCreate() {
        super.onCreate();
        HandlerThread thread = new HandlerThread("MessagePollingThread",
                android.os.Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        looper= thread.getLooper();
        handler= new Handler(looper);
        context=this;
        manager= new Manager(context);
        connectionDetector = new ConnectionDetector(context);
        helper= new DatabaseManager(context);
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                manager.log(TAG,"Crashed");
                e.printStackTrace();
            }
        });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }



    private class Handler extends android.os.Handler
    {

        public Handler(Looper looper)
        {
            super(looper);
        }

        private void getMissedMessages()
        {
            try{
                manager.log("real time working");

                List<String> userFriends = manager.getFriendUsernames();

                // final List<String> msgIds=manager.g();
                ParseQuery<ParseObject> query = new ParseQuery<>(Const.CHATS_CLASS);

                query.whereContainedIn(Const.FROM, userFriends);

                query.whereEqualTo(Const.TO, manager.getUsername());

                query.whereGreaterThan(Const.CREATED_AT, manager.getLatestMessageDate());

                query.orderByAscending(Const.CREATED_AT);

//                    query.whereNotContainedIn(Const.OBJECT_ID,msgIds);
//                    mediaQuery.whereNotContainedIn(Const.OBJECT_ID,msgIds);



                query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> list, ParseException e) {
                        if(e==null)
                        {
                            manager.log(TAG,"past messages gotten "+list.size());
                            for (ParseObject messageObject : list) {
                                int t= messageObject.getInt(Const.TYPE);
                                String from = messageObject.getString(Const.FROM);
                                String to = messageObject.getString(Const.TO);
                                String msg = messageObject.getString(Const.MSG);
                                String msgId = messageObject.getObjectId();
                                String fileUrl= messageObject.getString(Const.FILE_URL);
                                long size=messageObject.getLong(Const.SIZE);

                                HouseKeeping.storeIncomingMessage(context, messageObject);
                                HotSpot.setMessageDelivered(context,from,msgId);
                                manager.log(TAG,"past message "+msg);


                            }
                        }
                        else e.printStackTrace();
                            
                    }
                });

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        private void connectToRealTimeServer()
        {
            final NotificationManager notificationManager = new NotificationManager(context);
            final ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery(Const.CHATS_CLASS).whereContainedIn(Const.FROM, manager.getFriendUsernames()).whereEqualTo(Const.TO,manager.getUsername());
            List<String> userHubs = manager.getHubIds();


            manager.log("starting subscription");
            feedRealTimeUpdate();
            SubscriptionHandling<ParseObject> subscriptionHandling = parseLiveQueryClient.subscribe(parseQuery);

            if(userHubs!=null && userHubs.size()>0)
            {
                ParseQuery<ParseObject> hubChatsQuery = ParseQuery.getQuery(Const.HUBS_CHAT_CLASS).whereContainedIn(Const.HUB_ID,userHubs).whereNotEqualTo(Const.SENDER,manager.getUsername());
                SubscriptionHandling<ParseObject> hubChatsSubscriptionHandling = parseLiveQueryClient.subscribe(hubChatsQuery);


                hubChatsSubscriptionHandling.handleEvents(new SubscriptionHandling.HandleEventsCallback<ParseObject>() {
                    @Override
                    public void onEvents(ParseQuery<ParseObject> query, SubscriptionHandling.Event event, ParseObject object) {
                        // HANDLING create event
                        manager.log("Event Create found for Hub chat.....");
                        if(event==SubscriptionHandling.Event.CREATE)
                        {
                            try {
                                //String title = json.getString(TITLE);
                                int type = object.getInt(Const.TYPE);
                                String not_message = "A new Message";
                                String sender = object.getString(Const.SENDER);
                                String msg = object.getString(Const.MSG);
                                String hubId=object.getString(Const.HUB_ID);
                                final String msgId = object.getObjectId();
                                String fileUrl;int size;

                                HouseKeeping.storeHubIncomingMessage(context, object);

                                /*if(type==0)
                                {
                                    //Text message
                                    HouseKeeping.storeHubIncomingMessage(context, msg, sender, hubId, msgId);
                                }
                                else{
                                    fileUrl = object.getParseFile(Const.FILE).getUrl();
                                    size = object.getInt(Const.SIZE);

                                    switch (type)
                                    {
                                        //A picture message
                                        case 1:
                                            HouseKeeping.storeIncomingHubMedia(context,fileUrl,null,size,msg,sender,hubId,DataProvider.MessageType.INCOMING_IMAGE.ordinal(),msgId);
                                            //TODO change sender to hubtitle+sender
                                            break;
                                        //An audio message
                                        case 2:
                                            HouseKeeping.storeIncomingHubMedia(context,fileUrl,null,size,msg,sender,hubId,DataProvider.MessageType.INCOMING_VOICE_NOTE.ordinal(),msgId);
                                            //TODO change sender to hubtitle+sender
                                            break;
                                        case 3:
                                            HouseKeeping.storeIncomingHubMedia(context,fileUrl,null,size,msg,sender,hubId,DataProvider.MessageType.INCOMING_VOICE_NOTE.ordinal(),msgId);
                                            //TODO change sender to hubtitle+sender
                                            break;

                                    }
                                    HouseKeeping.getMedia(context,msgId,sender,1);

                                }*/




                                Intent resultIntent = new Intent(context, HubChat.class);
                                resultIntent.putExtra(HUB_ID,hubId);
                                resultIntent.putExtra(NOTIFICATION_INTENT,true);
                                resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                //TODO replace with hubTitle
                                notificationManager.showNotificationMessage("Hub Title", not_message, resultIntent);

                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }

                    }
                });
            }


            subscriptionHandling.handleError(new SubscriptionHandling.HandleErrorCallback<ParseObject>() {
                @Override
                public void onError(ParseQuery<ParseObject> query, LiveQueryException exception) {
                    //error occurred. Retry.
                    manager.log("Real Time Messaging Exception "+exception.getMessage());
                    parseLiveQueryClient.connectIfNeeded();
                    connectToRealTimeServer();
                }
            });

            subscriptionHandling.handleEvent(SubscriptionHandling.Event.UPDATE, new SubscriptionHandling.HandleEventCallback<ParseObject>() {
                @Override
                public void onEvent(ParseQuery<ParseObject> query, ParseObject object) {
                    manager.log("Event Update for chat");
                    int deliveryStatus = object.getInt(Const.STATUS);
                    new DatabaseManager(context).setMessageStatus(object.getObjectId(),deliveryStatus==1 ? Const.MESSAGE_READ: Const.MESSAGE_DELIVERED);
                }
            });
            subscriptionHandling.handleEvent(SubscriptionHandling.Event.CREATE, new SubscriptionHandling.HandleEventCallback<ParseObject>() {
                @Override
                public void onEvent(ParseQuery<ParseObject> query, ParseObject object) {
                    // HANDLING create event
                    manager.log("Event Create found for chat.....");

                    manager.log(object.toString());

                    String from = object.getString(Const.FROM);
                    String msg = object.getString(Const.MSG);
                    String msgId = object.getString(Const.OBJECT_ID);

                    HouseKeeping.storeIncomingMessage(context,object);
/*
                    switch (t)
                    {
                        case 0:
                            HouseKeeping.storeIncomingMessage(context, msg, from, to, msgId, int type);
                            break;
                        case 1:

                            HouseKeeping.storeIncomingPersonalMedia(context,fileUrl,null,size,msg,from, DataProvider.MessageType.INCOMING_IMAGE.ordinal(),msgId);
                            HouseKeeping.getMedia(context,msgId,from,0);

                            break;
                        case 2:
                            HouseKeeping.storeIncomingPersonalMedia(context,fileUrl,null,size,msg,from,DataProvider.MessageType.INCOMING_VOICE_NOTE.ordinal(),msgId);
                            HouseKeeping.getMedia(context,msgId,from,0);
                            break;
                    }*/

                    Intent resultIntent = new Intent(context, Chat.class);
                    resultIntent.putExtra(FRIEND_ID, String.valueOf(from));
                    resultIntent.putExtra(SENDER,from);
                    resultIntent.putExtra(NOTIFICATION_INTENT,true);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    notificationManager.showNotificationMessage(from, msg, resultIntent);
                    HotSpot.setMessageDelivered(context,from,msgId);
                }
            });

            parseLiveQueryClient.registerListener(new ParseLiveQueryClientCallbacks() {
                @Override
                public void onLiveQueryClientConnected(ParseLiveQueryClient client) {

                }

                @Override
                public void onLiveQueryClientDisconnected(ParseLiveQueryClient client, boolean userInitiated) {

                }

                @Override
                public void onLiveQueryError(ParseLiveQueryClient client, LiveQueryException reason) {
                    reconnect();
                }

                @Override
                public void onSocketError(ParseLiveQueryClient client, Throwable reason) {

                    reconnect();

                }
            });

        }
        private void reconnect()
        {
            manager.log(LOG_TAG,"RealTime Messaging Service disconnect. Trying to reconnect");
            if(connectionDetector.isConnectingToInternet())
            {
                parseLiveQueryClient.reconnect();
                connectToRealTimeServer();
            }
            {
                manager.log(LOG_TAG,"Not connecting to internet");
                alive=false;
                //TODO remove when online
                connectToRealTimeServer();
            }
        }
        
        private void feedRealTimeUpdate()
        {
            manager.log("starting feed subscription");
            SubscriptionHandling<ParseObject> feedSubscriptionHandling = 
                    parseLiveQueryClient.subscribe(ParseQuery.getQuery(Const.FEED_CLASS_NAME)
                    .whereEqualTo("valid",true));

            feedSubscriptionHandling.handleEvent(SubscriptionHandling.Event.UPDATE, new SubscriptionHandling.HandleEventCallback<ParseObject>() {
                @Override
                public void onEvent(ParseQuery<ParseObject> query, ParseObject object) {
                    manager.log(TAG, "event update for feed");
                }
            });
            
            feedSubscriptionHandling.handleEvent(SubscriptionHandling.Event.CREATE, new SubscriptionHandling.HandleEventCallback<ParseObject>() {
                @Override
                public void onEvent(ParseQuery<ParseObject> query, ParseObject object) {
                    // HANDLING create event
                    manager.log(TAG, "Event Create found for Feed.....");

                    manager.log(object.toString());
                    object.pinInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            //call feed adapter
                        }
                    });

                    
                }
            });
            
        }

        private void loop()
        {
            while (true)
                try{
                    wait(10000);
                    parseLiveQueryClient.connectIfNeeded();
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                    loop();
                }
        }
        @Override
        public void handleMessage(Message msg) {

            
                    getMissedMessages();
                    try{
                        connectToRealTimeServer();
                    }
                    catch (Exception e)
                    {
                        manager.log(TAG,"Crashed");
                        e.printStackTrace();
                        connectToRealTimeServer();
                    }
        }


    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Message msg = handler.obtainMessage();
        msg.arg1 = startId;
        handler.sendMessage(msg);
        return Service.START_STICKY;
    }
}
