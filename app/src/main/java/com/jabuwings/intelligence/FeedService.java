package com.jabuwings.intelligence;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;

import com.jabuwings.database.DataProvider;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.download.ResultManager;
import com.jabuwings.feed.FeedItem;
import com.jabuwings.intermediary.UserQuery;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.FeedAdapter;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.main.Feed;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Falade James on 12/16/2015 All Rights Reserved.
 */
@Deprecated
public class FeedService extends Service {
    private Looper mServiceLooper;
    private Handler mServiceHandler;
    private static String LOG_TAG=FeedService.class.getSimpleName();
    private static boolean keep=true;
    private static boolean update=true;
    Manager manager;
    DatabaseManager databaseManager;
    SQLiteDatabase db;
    Context context;
    List<FeedItem> feedItems;
    static FeedService instance;
    public static SwipeRefreshLayout swipeRefreshLayout;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance=this;
        context=getApplicationContext();
        manager= new Manager(context);
        databaseManager =new DatabaseManager(context);
        db= databaseManager.getWritableDatabase();
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                android.os.Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new Handler(mServiceLooper);

    }

    public static void setUpdate(boolean update1)
    {
        update=update1;
    }
    public static FeedService getInstance()
    {
        return instance;
    }
    private class Handler extends android.os.Handler{
        public Handler(Looper looper)
        {
           super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            while (keep) {
                synchronized (this) {
                    try {
                        if(update)
                        {
                            final Feed feed = null;
                            manager.log("Feed updating");
                            ParseQuery<FeedItem> feedQuery= FeedItem.getQuery();
                            try {
                                feedItems = FeedItem.getQuery().fromLocalDatastore().whereEqualTo(Const.VALID, true).find();
                            }
                            catch (ParseException e)
                            {
                                manager.log("No local feed was found");
                            }

                            if(feedItems!=null &&feedItems.size()>0 )
                            {
                                for(int i=0; i<=feedItems.size()-1; i++)
                                {
                                    final int position=i;
                                    FeedItem feedItem= feedItems.get(i);
                                    feedItem.fetchIfNeededInBackground(new GetCallback<ParseObject>() {
                                        @Override
                                        public void done(ParseObject parseObject, ParseException e) {
                                            if(e==null)
                                            {
                                                parseObject.pinInBackground();
                                            }

                                            if(position==feedItems.size()-1)
                                            {
                                                Feed feed = null;
                                                if(feed!=null)
                                                    feed.updateFeed();

                                            }
                                        }
                                    });
                                }

                            }
                            if(new DatabaseManager(context).getFeedCount()==0)
                            {
                                feedQuery.whereEqualTo("valid",true);
                            }

                            else {

                                if( manager.getLatestFeedTime()!=null)
                                {
                                    feedQuery.whereEqualTo("valid",true);
                                    feedQuery.whereNotContainedIn(Const.OBJECT_ID,manager.getFeedIDs());
                                }

                            }

                            feedQuery.orderByDescending(Const.CREATED_AT);
                            feedQuery.setLimit(15);
                            List<String> possibleAuthors=manager.getFriendUsernames();
                            possibleAuthors.add("public");
                            feedQuery.whereContainedIn("filter",possibleAuthors);
                            feedQuery.include("UserPublic");
                            //or feedQuery.include("author_public");
//                            feedQuery.include("_User");
                            List<FeedItem> list=feedQuery.find();
                            manager.log("Feeds found "+list.toString()+" "+list.size());
                            if(list.size()==0)
                                if(feed!=null)
                                feed.updateFeed();
                            // If the new feeds are more than 15. Delete all previous locally stored feed
                            if(feedItems!=null && feedItems.size()+list.size()>=15)
                            {
                                for(FeedItem feedItem : list)
                                {
                                    feedItem.unpin();
                                }
                            }
                            for (final FeedItem feedItem : list) {

                                final ParseObject author = feedItem.getAuthor().fetchIfNeeded();
                                feedItem.put(Const.Feed.IS_LIKED, feedItem.isLiked());
                                if(author!=null)
                                {
                                    switch (feedItem.getPostedBy()) {
                                        case 0:
                                            feedItem.put(Const.USERNAME,author.getString(Const.USERNAME));
                                            feedItem.put(Const.NAME,author.getString(Const.NAME));
                                            break;
                                        case 1:
                                            feedItem.put(Const.USERNAME,author.getString(Const.HUB_TITLE));
                                            break;
                                        case 2:
                                            feedItem.put(Const.USERNAME,author.getString(Const.NAME));
                                            break;
                                    }
                                    String authorUrl= author.getString(Const.PROFILE_PICTURE_URL);
                                    if(authorUrl!=null)
                                        feedItem.put(Const.Feed.AUTHOR_URL,authorUrl);


                                    try {
                                        feedItem.pin();
                                        if(feed!=null)
                                            feed.updateFeed();
                                    }
                                    catch (ParseException e1){e1.printStackTrace();}

                                }

                            }







                           // if(Feed.onFeedChanged!=null){Feed.onFeedChanged.feedChanged();}
                           /*     ResultManager resultManager = new ResultManager(new android.os.Handler(),context);
                                Bundle resultData = new Bundle();
                                resultData.putBoolean("update",true);
                                resultManager.send(333,resultData);*/


                        }

                        wait(180000);
                    } catch (Exception e) {
                        //manager.errorToast("JabuWings Feed has stopped");
                        e.printStackTrace();
                    }
                }
            }
        }
    }


    public void updateFeed()
    {

        mServiceHandler.sendEmptyMessage(4);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "Feed Service on initialise command called");

        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

        return Service.START_STICKY;
    }
}
