package com.jabuwings.intelligence;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.TextUtils;
import android.util.Log;

import com.jabuwings.R;
import com.jabuwings.error.ExceptionHandler;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.TimeTableItem;
import com.jabuwings.utilities.Parrot;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.LectureDetails;
import com.jabuwings.views.main.Domicile;
import com.jabuwings.views.timetable.TimeKeeper;
import com.jabuwings.views.timetable.TimeTable2;
import com.parse.ParseConfig;

import java.util.List;

/**
 * Created by Falade James on 10/24/2015 All Rights Reserved.
 */
public class LectureService extends Service {
NotificationManager mNotificationManager;
static boolean keep=true;
private static final String LOG_TAG=LectureService.class.getSimpleName();
private IBinder binder= new LectureBinder();
    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    /**
     * This specifies if the lecture service is currently running or not
     */
    public static boolean ACTIVE=false;
    Time time;
    /**
     * This specifies a particular day that is confirmed that there are no lectures.
     * This is used to check if a "no more lectures" Notification has been sent for the particular day to avoid repetition
     */
    String noLecturesDay="";
    /**
     * This is the course that was last reminded of.
     */
    TimeTableItem currentCourse;
    public  LectureService()
    {
        super();
    }

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            keep=true;
            time=new Time();
            currentCourse=null;
            ParseConfig config = ParseConfig.getCurrentConfig();
//            ParseConfig.getInBackground();
            

            if(time.getDay()==1 || time.getDay()==7)
            {
                suspendLectureService();
                return;
            }
            if(config.getBoolean("in_session",false))
                not();
            
            synchronized (this){
            while (keep) {
                    try {
                        wait(1800000);
                        if(config.getBoolean("in_session",false))
                            not();
                        not();
                    } catch (Exception e) {
                        e.printStackTrace();
                       handleMessage(msg);

                    }
            } }

        }
    }


private void suspendLectureService()
{
    synchronized (this) {
        try {
            wait(1800000);
            stopSelf();
            startService(new Intent(this, LectureService.class));
        } catch (Exception e) {
            e.printStackTrace();
            stopSelf();
            startService(new Intent(this, LectureService.class));
        }
    }
}


    /**
     * This proceeds with checking the user's course and reminds the user as appropriate
     */
    private void not()
{
    Manager manager = new Manager(getApplicationContext());
    if(manager.getUserCourses()!=null && manager.isTimetableNotificationEnabled()) {
        List<TimeTableItem> nextCourses = TimeKeeper.getNextLectures(getApplicationContext());
        /**
         * This checks that there is at least one course in the list, else the user may be reminded of "No more lectures" as appropriate.
         */
        if (nextCourses.size() > 0) {
            manager.log("Passed 1");
            /**
             * The first course on the list.
             * This is the course nearest to the current time.
             */
            TimeTableItem _course = nextCourses.get(0);
            if(manager.getCompulsoryCourses().contains(_course.getCourseCode()) && !_course.equals(currentCourse) && _course.getStartingHour()-time.getHour()<=2)
            {
                manager.log("Compulsory course "+_course);
                String item,venue;

                switch (_course.getCourseCode())
                {
                    case "CMS": item="Community Service"; venue="AUDITORIUM";
                        break;
                    case "GAME": item= "Games"; venue="Anywhere Possible";
                        break;
                    default: item=""; venue="";
                }
                notifyL(_course,"You have "+item+" by "+_course.getTimeStart(),"Venue @"+venue);
                currentCourse=_course;
                return;
            }
            /**
             * The first condition checks if the last course reminded( {@code currentCourse} is not the same as the new arriving course {@code _course}
             * They must not be equal, else we remind the  user of the same course more than once. The condition prevents this.
             * The second condition checks that the difference between the starting time of the course and the current time is greater than or equal to 2.
             * This makes sure that we only remind the user of a course that is in the next 2 hours or less.
             */
            manager.log("Checking...");
            boolean condition=_course.getStartingHour()-time.getHour()<=2;
            if(!_course.equals(currentCourse) && condition){
            notifyL(_course);
            currentCourse=_course;
            }
            else{
                manager.log(LOG_TAG,"Could not notify "+_course+" currentCourse "+currentCourse+" Condition " +condition);
            }
        } else {
            manager.log("Failed 1");
            /**
             * The first condition checks that the user has not been reminded of "No more lectures" before.
             * The second condition makes sure the user has been reminded of at least one lecture before now.
             */
            if(!noLecturesDay.equals(time.getCalendarDay()) && currentCourse!=null ) {
                if(currentCourse.getEndingHour()>=time.getHour())
                {
                    notifyNoLectures();
                    suspendLectureService();
                }
            }
        }
    }
}

    @Override
    public void onCreate() {
        super.onCreate();
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        ACTIVE=true;
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                android.os.Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG,"Lecture Service on initialise command called");

        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

        return Service.START_STICKY;
    }



    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    /**
     * The service has been stopped. This may be from the service itself or from the phone's system.
     */
    @Override
    public void onDestroy() {
       ACTIVE=false;
     // notifyUserOnDestroy("Application Disrupted");

    }

    public class LectureBinder extends Binder {
        LectureService getService(){
            return  LectureService.this;
        }
    }


        private void notifyUserOnDestroy(String msg)

        { mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

            PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                    new Intent(this, LectureDetails.class), 0);

            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this);

            Notification notification = mBuilder.setSmallIcon(R.drawable.not_icon).setTicker(msg)
                    .setContentTitle(msg)
                    .setContentIntent(contentIntent)
                    .setContentText("A core component of our application has been stopped. Please restart your phone to make things work well")
                    .build();
            notification.flags |= Notification.FLAG_ONGOING_EVENT| Notification.FLAG_NO_CLEAR;
            //  startForeground(1, notification);
            mNotificationManager.notify(Const.LECTURE_NOTIFICATION, notification);}


    private String getNumberInWords(String word)
    {

        char[] ns=word.toCharArray();
        String result="";
        for(char a: ns)
        {
            result+=a+"-";
        }

        return result;

    }
    private String getNumberWord(int no)
    {
        switch(no)
        {
            case 0:
                return "zero";
            case 1:
                return "one";
            case 2:
                return "two";
            case 3:
                return "three";
            case 4:
                return "four";
            case 5:
                return "five";
            case 6:
                return "six";
            case 7:
                return "seven";
            case 8:
                return "eight";
            case 9:
                return "nine";
            default:return "zero";




        }
    }

    private void notifyL(TimeTableItem course,  String... data)
    {
        try {
            Log.d(LOG_TAG, "Attempting to notify " + course);
            int minutes;
            int currentMinute = time.getMinute(), currentHour = time.getHour();
            int hourDifference = course.getStartingHour() - currentHour;
            int hourDifference2=hourDifference-1;
            minutes = 60 - currentMinute;
            String hour = hourDifference2 > 1 ? "hours" : "hour";
            String courseCode=  course.getCourseCode();
            String courseCodeSpeech= null;

            if(courseCode.charAt(3)==' ' && courseCode.length()==7)
            {
                String numberFragment = "";
                String acronymFragment="";
                try {
                    numberFragment = courseCode.substring(4, 7);
                    acronymFragment=courseCode.substring(0,3);

                }
                catch(IndexOutOfBoundsException i)
                {
                    i.printStackTrace();
                }

                if(!TextUtils.isEmpty(numberFragment) && !TextUtils.isEmpty(acronymFragment) && TextUtils.isDigitsOnly(numberFragment))
                {
                    courseCodeSpeech=acronymFragment+" "+getNumberInWords(numberFragment);
                    /*String firstNo=getNumberWord(Integer.valueOf(String.valueOf(numberFragment.charAt(0))));
                    String secondNo=getNumberWord(Integer.valueOf(String.valueOf(numberFragment.charAt(1))));
                    String thirdNo=getNumberWord(Integer.valueOf(String.valueOf(numberFragment.charAt(2))));
                    courseCodeSpeech=acronymFragment+" "+firstNo+" "+secondNo+" "+thirdNo;*/
                }
            }
            if(courseCodeSpeech!=null)
            {
                courseCode=courseCodeSpeech;
            }
            String speech = hourDifference2 > 0 ? "You have " + courseCode+ " in the next " + hourDifference2 + " " + hour + ", " + minutes + " minutes. The venue is " + course.getCourseVenue() : "You have " + course.getCourseCode() + " in the next " + minutes + " minutes. The venue is " + course.getCourseVenue();





            new Parrot(getApplicationContext()).speak(speech);
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.not_icon)
                            .setTicker(speech)
                            .setContentTitle("You have " + course.getCourseCode() + " by " + course.getTimeStart())
                            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                            .setContentText("Venue @" + course.getCourseVenue());

            if (data.length >0) {
                mBuilder.setContentTitle(data[0]);
                mBuilder.setContentText(data[1]);
                mBuilder.setTicker(data[0] + " " + data[1]);
            }
// Creates an explicit intent for an Activity in your app
            Intent resultIntent = new Intent(this, LectureDetails.class);
            resultIntent.putExtra(Const.TAG, course.getCourseCode() + Const.SEPARATOR + course.getTimeStart() + Const.SEPARATOR + course.getTimeStop() + Const.SEPARATOR + course.getCourseVenue());
// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
            stackBuilder.addParentStack(Domicile.class);
// Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(
                            0,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
            mNotificationManager.notify(Const.LECTURE_NOTIFICATION, mBuilder.build());
        }
        catch (Exception e){e.printStackTrace();}
    }

    private void notifyNoLectures()
    {
        noLecturesDay=time.getCalendarDay();
        currentCourse=null;
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, TimeTable2.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this);

        Notification notification = mBuilder.setSmallIcon(R.drawable.not_icon).setTicker("Lectures are finished for today. Go out there and have some fun. Some fun with your books")
                .setContentTitle("No more lectures")
                .setContentIntent(contentIntent)
                .setContentText("You should check tomorrow's timetable")
                .build();

        mNotificationManager.notify(Const.LECTURE_NOTIFICATION, notification);
    }
    }

