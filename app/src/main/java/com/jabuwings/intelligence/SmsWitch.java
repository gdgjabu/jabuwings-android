package com.jabuwings.intelligence;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.jabuwings.views.registration.OnOTPReceived;
import com.jabuwings.views.registration.Verification;

/**
 * Created by Falade James on 1/29/2017 All Rights Reserved.
 */

public class SmsWitch extends BroadcastReceiver {

    public static OnOTPReceived received;
    @Override
    public void onReceive(Context context, Intent intent) {
        if(Verification.verifying)
        {
            final Bundle bundle =intent.getExtras();
            try{
                if(bundle!=null)
                {
                    Object[] pdusObj=(Object[])bundle.get("pdus");
                    for(Object a: pdusObj)
                    {
                        SmsMessage currentMessage=SmsMessage.createFromPdu((byte[]) a);
                        String senderAddress=currentMessage.getDisplayOriginatingAddress();
                        String message=currentMessage.getDisplayMessageBody();

                        if(senderAddress.contains("JabuWings") || message.contains("JabuWings"))
                        {
                            if(received!=null)
                            {
                                received.onOTPReceived(message.substring(0,7));
                            }

                        }

                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

    }
}
