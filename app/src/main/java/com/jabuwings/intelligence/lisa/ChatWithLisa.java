package com.jabuwings.intelligence.lisa;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.realm.File;
import com.jabuwings.database.realm.Friend;
import com.jabuwings.database.realm.Message;
import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.Parrot;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.ProfileOfFriend;
import com.jabuwings.views.main.Chat;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import java.util.HashMap;
import java.util.UUID;

import io.realm.Realm;

public class ChatWithLisa extends Chat implements View.OnClickListener {
Lisa lisa;
Time time;
private String lisa_= "lisa";
Parrot parrot;
Manager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager=new Manager(this);
        lisa= new Lisa(getApplicationContext());
        send.setOnClickListener(this);
        time= new Time();
        parrot = new Parrot(this);



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        parrot.shutdown();
        
    }

    private void initAI()
    {

    }


    @Override
    public void onClick(View v) {
        final String t=txt.getText().toString().trim();
        switch (v.getId()) {

            case R.id.btnSend:
                    if (TextUtils.isEmpty(t)) {
                        break;
                    }
                    txt.setText(null);
                    final String msgId = HouseKeeping.storeOutgoingMessage(getApplicationContext(), t, lisa_,DataProvider.MessageType.OUTGOING.ordinal());
                    final Message message = realm.where(Message.class).equalTo(Const.ID,msgId).findFirst();
                    HashMap<String,Object> params = manager.getDefaultCloudParams();
                    params.put(Const.MSG, t);
                    params.put(Const.SESSION, manager.getUser().getSessionToken());

                    ParseCloud.callFunctionInBackground("lisaClient", params, new FunctionCallback<String>() {
                        @Override
                        public void done(final String response, ParseException e) {
                            if(e==null)
                            {
                                final LisaIntent lisaIntent = new LisaIntent(response);
                                parrot.speak(lisaIntent.getContent());


                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        message.setDeliveryStatus(Const.MESSAGE_READ);
                                    }
                                });
                                realm.executeTransactionAsync(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        Friend friend = realm.where(Friend.class).equalTo(Const.USERNAME, lisa_).findFirst();
                                        Message message = realm.createObject(Message.class, UUID.randomUUID().toString());
                                        message.setSender(lisa_);
                                        message.setReceiver(manager.getUsername());
                                        if (!TextUtils.isEmpty(lisaIntent.getContent()))
                                            message.setMsg(lisaIntent.getContent());
                                        message.setMsgTime(time.getDisplayTimeDate());
                                        message.setDeliveryStatus(Const.MESSAGE_READ);
                                        if (lisaIntent.getType()==0)
                                        {
                                            message.setMsgType(DataProvider.MessageType.INCOMING.ordinal());

                                        }
                                        else if(lisaIntent.getType()==1)
                                        {
                                            message.setMsgType(DataProvider.MessageType.INCOMING_IMAGE.ordinal());
                                            File file = realm.createObject(File.class, UUID.randomUUID().toString());
                                            file.setSize(0);
                                            file.setAddress(lisaIntent.getFileUrl());
                                            message.setFile(file);
                                        }
                                        else if(lisaIntent.getType()==2)
                                        {
                                            message.setMsgType(DataProvider.MessageType.INCOMING.ordinal());
                                            manager.log(String.valueOf(lisaIntent.getLocation().getLatitude()));
                                            final Intent intent =new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?"+"daddr="+lisaIntent.getLocation().getLatitude()+","+lisaIntent.getLocation().getLongitude()));
                                            intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
                                            startActivity(intent);

                                        }

                                        friend.setLastMessage(message);

                                    }
                                });
                            }
                            else{
                                e.printStackTrace();
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        message.setDeliveryStatus(Const.MESSAGE_FAILED);
                                    }
                                });

                            }

                        }
                    });




                break;

            case R.id.toolbar:
                startActivity(new Intent(this, ProfileOfFriend.class).putExtra(Const.USERNAME, "lisa"));
                break;
        }
    }


    @Override
    protected void onPause() {
        lisa.close();
        super.onPause();
    }

}
