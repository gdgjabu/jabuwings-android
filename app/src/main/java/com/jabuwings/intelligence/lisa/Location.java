package com.jabuwings.intelligence.lisa;

/**
 * Created by jamesfalade on 16/09/2017.
 */

class Location {
    private double latitude ,longitude;
    Location(double latitude, double longitude)
    {
        this.latitude=latitude;
        this.longitude=longitude;
    }
    double getLatitude() {
        return latitude;
    }

    double getLongitude() {
        return longitude;
    }
}
