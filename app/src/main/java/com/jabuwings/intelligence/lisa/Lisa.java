package com.jabuwings.intelligence.lisa;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.jabuwings.R;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.Time;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class Lisa {

    InputStream modelIn;
//    SentenceModel model;
    Context mContext;
    Manager manager;
    Time time;
    private static final String TAG=Lisa.class.getSimpleName();
    String hi, hello , hey, howAreYou, iAmFine, okay, hopeYouAreFine,
            helloThere,helloMyFriend, cannotAnswer, iCannotLove, iCannotLove2, whoAreYou,whoCreatedYou,
            howOldAreYou, whatsYourName, whereAreYou;
//    SentenceDetectorME sentenceDetector;
    String sentences[];
    public Lisa(Context context){
        this.mContext= context;
        time= new Time();
        manager = new Manager(mContext);

        strings();

    }



    void close()
    {
        if (modelIn != null) {
            try {
                modelIn.close();
            }
            catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(mContext, e.toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void strings()
    {

        hi = mContext.getString(R.string.lisa_hi);
        hello= mContext.getString(R.string.lisa_hello);
        hey= mContext.getString(R.string.lisa_hey);
        howAreYou=  mContext.getString(R.string.lisa_how_are_you);
        iAmFine=  mContext.getString(R.string.lisa_fine);
        okay= mContext.getString(R.string.lisa_okay);
        hopeYouAreFine=  mContext.getString(R.string.lisa_hope_you_are_fine);
        helloThere=  mContext.getString(R.string.lisa_hello_there);
        helloMyFriend=  mContext.getString(R.string.lisa_hello_my_friend);
        cannotAnswer=  mContext.getString(R.string.lisa_cannot_answer);
        iCannotLove=mContext.getString(R.string.lisa_love_long);
        iCannotLove2=mContext.getString(R.string.lisa_love);
        whoAreYou= mContext.getString(R.string.lisa_intro);
        whoCreatedYou=mContext.getString(R.string.lisa_creation);
        howOldAreYou=mContext.getString(R.string.lisa_dob);
        whereAreYou=mContext.getString(R.string.lisa_where);
        whatsYourName=mContext.getString(R.string.lisa_name);





    }




    private String cannotAnswerAdviser(String msg)
    {
        String cannotAnswer1= mContext.getString(R.string.lisa_cannot_answer1);
        String cannotAnswer2= mContext.getString(R.string.lisa_cannot_answer2);
        String cannotAnswer3= mContext.getString(R.string.lisa_cannot_answer3);
        String cannotAnswer4= mContext.getString(R.string.lisa_cannot_answer4);

        int noOfSentence=0;



        int decide = new Random().nextInt(5);
        String answer= null;
        

        switch (decide)
        {
            case 0:
               answer=replyCannotAnswer(cannotAnswer, noOfSentence);
                break;
            case 1:
                answer=replyCannotAnswer(cannotAnswer1, noOfSentence);
                break;
            case 2:
                answer=replyCannotAnswer(cannotAnswer2, noOfSentence);
                break;
            case 3:
                answer=replyCannotAnswer(cannotAnswer3, noOfSentence);
                break;
            case 4:
                answer=replyCannotAnswer(cannotAnswer4, noOfSentence);
                break;
            



        }






        return answer;


    }
private String replyCannotAnswer(String cannotAnswer, int noOfSentence)
    
{

    
    String a=mContext.getResources().getQuantityString(
            R.plurals.sentence, noOfSentence, noOfSentence);
   return cannotAnswer+ " "+mContext.getString(R.string.lisa_cannot_answer_contains)+" "+noOfSentence+" "+a;
    
    
    
}
    private String thinkAndDecide(String msg)
    {
        String qHi, qHello, qHey, qHowAreYou, qIAmFine, qOkay, qILoveYou,
                qWhoAreYou,qWhoCreatedYou,qHowOldAreYou, qWhatsUp, qWhereAreYou, qWhatsYourName;
        qHi= mContext.getString(R.string.ask_lisa_hi);
        qHello= mContext.getString(R.string.ask_lisa_hello);
        qHey= mContext.getString(R.string.ask_lisa_hey);
        qHowAreYou=mContext.getString(R.string.ask_lisa_how_are_you);
        qIAmFine=mContext.getString(R.string.ask_lisa_fine2);
        qOkay=mContext.getString(R.string.ask_lisa_okay);
        qILoveYou=mContext.getString(R.string.ask_lisa_i_love_you);
        qWhoAreYou=mContext.getString(R.string.ask_lisa_who_are_you);
        qWhoCreatedYou=mContext.getString(R.string.ask_lisa_who_created_you);
        qHowOldAreYou=mContext.getString(R.string.ask_lisa_how_old_are_you);
        qWhatsUp=mContext.getString(R.string.ask_lisa_whats_up);
        qWhatsYourName=mContext.getString(R.string.ask_lisa_what_is_your_name);
        qWhereAreYou=mContext.getString(R.string.ask_lisa_where_are_you);


        String lisaReply = null;






        if(msg.equals(qHi)|| msg.equals(qHello)|| msg.equals(qHey)|| msg.equals(qWhatsUp))
        {
            Random choose = new Random();
            int decision= choose.nextInt(5);



            switch (decision)
            {
                case 0:
                    lisaReply= hi+" "+manager.getName();
                break;
                case 1:
                    lisaReply= hi;
                    break;
                case 2:
                    lisaReply= hello;
                    break;
                case 3:
                    lisaReply= hey;
                    break;
                case 4:
                    lisaReply= hopeYouAreFine;
                    break;
                case 5:
                    lisaReply= helloThere;
                    break;
                case 6:
                    lisaReply= helloMyFriend;
                    break;

            }







        }

        if(msg.equals(qHowAreYou))
        {
            lisaReply=iAmFine;
        }
        if (msg.equals(qOkay)||msg.equals(qIAmFine)|| msg.equals(mContext.getString(R.string.ask_lisa_fine)))
        {
            lisaReply= okay;
        }

        if(msg.equals(qILoveYou))
        {
         int d= new Random().nextInt(2);
            if(d==1)
            {
                lisaReply=iCannotLove;
            }

            else{
                lisaReply=iCannotLove2;
            }

        }

        if(msg.equals(qWhoAreYou))
        {
            lisaReply=whoAreYou;
        }
        if(msg.equals(qWhoCreatedYou))
        {
            lisaReply=whoCreatedYou;
        }
        if(msg.equals(qHowOldAreYou))
        {
            lisaReply= howOldAreYou;
        }


        if(msg.equals(qWhereAreYou))
        {
            lisaReply=whereAreYou;
        }

        if(msg.equals(qWhatsYourName))
        {
            lisaReply=whatsYourName;
        }
        if(lisaReply==null)

        {
            lisaReply= cannotAnswerAdviser(msg);
        }


     return lisaReply;










    }


    public static String generateRandomString
            (int length)
    {



        char[] values={'a','b','c', 'd','1','3','2','3','4','5','6','7','8', '9', '0' };
        String out="";
        for (int i=0; i<length; i++)
        {
            int idx= new Random().nextInt(values.length);
            out+= values[idx];
        }
        return out;

    }

    public static String generateHubPin(String hubId)
    {
        char[] values={'1','5','4','2','0','a','3','6','b'};
        String out="";
        for(int i=0; i<7; i++)
        {
            if(i==0)
            {
                out+="7";
            }
            else {
                int idx = new Random().nextInt(values.length);
                out += values[idx];
            }
        }

        return out;
    }


    public static String generateMessageId()
    {
        char[] values= {'a','b','c', 'd','e','1','3','2','3','4','5','6','7','8', '9', '0','f','g','h','i','j' };
        String out="";
        for (int i=0; i<20; i++)
        {
            int idx= new Random().nextInt(values.length);
            out+= values[idx];

        }
        return System.nanoTime()+out;

    }

    public static String generateHubMessageId(String hubId)
    {
        char[] values= {'a','e','1','3','2','3','4','b','c', 'd','5','6','7','8', '9', '0','f','g','h','i','j' };
        String out="";
        for (int i=0; i<20; i++)
        {
            int idx= new Random().nextInt(values.length);
            out+= values[idx];

        }
        return hubId+"_"+out;

    }

    public static String generateFeedId(String username)
    {
        char[] values= {'3','2','c', 'd','e','1','1','2','3','4','b','6','7','a', '9', '0','f','g','h','i','j' };
        String out="";
        for (int i=0; i<20; i++)
        {
            int idx= new Random().nextInt(values.length);
            out+= values[idx];

        }
        return out+"_"+username;
    }

    public static String generateAudioFileName(String username)

    {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        return username+"_"+timeStamp;



    }

    public static String generateImageFileName(String username)
    {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        return username+"_"+timeStamp;
    }
    
    public static String generateCapitalisedName(String firstName, String lastName)
    {
        firstName=firstName.toLowerCase();
        lastName=lastName.toLowerCase();
        firstName=firstName.substring(0,1).toUpperCase()+firstName.substring(1);
        if(lastName.contains(" "))
        {
            String lastNames[]= lastName.split(" ");
            String combinedName="";
            for(String name: lastNames)
            {
                name=name.substring(0,1).toUpperCase()+name.substring(1);
                combinedName+=name+" ";
            }
            lastName=combinedName.trim();



        }

        else{

            lastName=   lastName.substring(0,1).toUpperCase()+lastName.substring(1);
        }

        return firstName+" "+lastName;
    }


    public static Long generateSessionId()
    {
        return System.nanoTime();
    }


}
