package com.jabuwings.intelligence.lisa;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * Created by jamesfalade on 12/09/2017.
 */

 class LisaIntent {
    private JsonObject json;
     LisaIntent(String s)
    {
        JsonParser parser = new JsonParser();
        json = parser.parse(s).getAsJsonObject();
    }

    int getType()
    {
        return json.get("type").getAsInt();
    }

    Location getLocation()
    {
        JsonObject location = json.getAsJsonObject("location");
        return new Location(location.get("latitude").getAsDouble(), location.get("longitude").getAsDouble());
    }

    String getContent()
    {
        return json.get("content").getAsString();
    }

    String getFileUrl()
    {
        return json.get("fileUrl").getAsString();
    }
}
