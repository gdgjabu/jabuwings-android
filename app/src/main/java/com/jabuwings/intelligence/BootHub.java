package com.jabuwings.intelligence;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.jabuwings.management.Manager;

/**
 * Created by Falade James on 10/24/2015 All Rights Reserved.
 */
public class BootHub extends BroadcastReceiver {
    LectureNotice alarm = new LectureNotice();
    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context=context;
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED"))
        {
        //    alarm.setAlarm(context);
       //     context.startActivity(new Intent(context, LectureDetails.class).putExtra("boot", true).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            Manager manager = new Manager(context);
            manager.setUpPush();
            manager.setUpRealTimeMessagingService();
            startTimer(100);
        }
    }

    private void startTimer(int millis) {

        try {
            Thread.sleep(millis);

        } catch (InterruptedException e) {
            context.startService(new Intent(context,LectureService.class).putExtra("msg","No more lectures today"));
        }

        context.startService(new Intent(context,LectureService.class));
    }
}
