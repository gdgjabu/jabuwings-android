package com.jabuwings.intelligence;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;

import java.util.Calendar;

/**
 * Created by Falade James on 3/7/2016 All Rights Reserved.
 */
public class MessageService extends IntentService {

    Context context;
    public MessageService()
    {
        super("Message Service");
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        final String msg=intent.getStringExtra(Const.MSG);
        final String msgId=intent.getStringExtra(Const.MSG_ID);
        final String sender=intent.getStringExtra(Const.SENDER);
        final String receiver=intent.getStringExtra(Const.RECEIVER);
        context=getApplicationContext();
        new Thread(new Runnable() {
            @Override
            public void run() {
                HotSpot.sendMSG(context,msg,msgId,sender,receiver,true);
            }
        }).start();

    }


    public static void sendTimedMessage(Context context, int hour, int minute, String message,String msgId,String sender,String receiver)
    {
        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, MessageService.class).putExtra(Const.MSG, message).putExtra(Const.SENDER,sender).putExtra(Const.RECEIVER,receiver).putExtra(Const.MSG_ID,msgId);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE,minute);

        alarmMgr.set(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(),  alarmIntent);

        // Enable {@code SampleBootReceiver} to automatically restart the alarm when the
        // device is rebooted.
        ComponentName _receiver = new ComponentName(context, BootHub.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(_receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }
}
