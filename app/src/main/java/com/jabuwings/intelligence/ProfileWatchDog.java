package com.jabuwings.intelligence;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;

import com.jabuwings.database.DataProvider;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.intermediary.UserQuery;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Falade James on 12/16/2015 All Rights Reserved.
 */
public class ProfileWatchDog extends Service {
    private Looper mServiceLooper;
    private Handler mServiceHandler;
    private static String LOG_TAG=ProfileWatchDog.class.getSimpleName();
    private static boolean keep=true;
    private static boolean watchFriends=true;
    Manager manager;
    DatabaseManager databaseManager;
    SQLiteDatabase db;
    Context context;
    static ProfileWatchDog instance;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance=this;
        context=getApplicationContext();
        manager= new Manager(context);
        databaseManager =new DatabaseManager(context);
        db= databaseManager.getWritableDatabase();
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                android.os.Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new Handler(mServiceLooper);

    }

    public static void setWatchFriends(boolean watchFriends1)
    {
        watchFriends=watchFriends1;
    }
    public static ProfileWatchDog getInstance()
    {
        return instance;
    }
    private class Handler extends android.os.Handler{
        public Handler(Looper looper)
        {
           super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            while (keep) {
                synchronized (this) {
                    try {
                        setUserLastSeen();
                        if(watchFriends)
                        {
                            List<String> usernames= 
                              databaseManager.getFriends();
                            if(usernames.size()>0)
                                for (String username: usernames)
                                {
                                    UserQuery.queryUserPresentState(context,username);
                                }


                            if(manager.getUserHubs()!=null )
                                for(String hub_id: manager.getUserHubs())
                                {
                                    UserQuery.queryHubState(context,hub_id);
                                }
                        }

                        wait(180000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void setUserLastSeen()
    {
        HashMap<String,Object> params= new HashMap<>();
        params.put(Const.USERNAME,manager.getUsername());
        if(manager.getUsername()!=null)
        ParseCloud.callFunctionInBackground("userLastSeen", params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {
                if(e==null)
                {
                    manager.log("user last seen updated");
                }
                else{
                    manager.log("user last seen error");
                    e.printStackTrace();
                    //ErrorHandler.handleError(context,e);
                }
            }
        });
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "ProfileWatchDog Service on initialise command called");

        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

        return Service.START_STICKY;
    }
}
