package com.jabuwings.intelligence;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.jabuwings.database.DataProvider;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.database.realm.Message;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.List;

import io.realm.Realm;

import static com.jabuwings.management.Const.MESSAGE_SENT;

/**
 * Created by Falade James on 1/7/2017 All Rights Reserved.
 */
public class ConnectivityWatch extends BroadcastReceiver {
    Manager manager;
    @Override
    public void onReceive(Context context, Intent intent) {
        manager=new Manager(context);

        if(isOnline(context))
        {
            resendPending(context);
            manager.setUpRealTimeMessagingService();
        }
    }


    private boolean isOnline(Context context)
    {
        ConnectivityManager cm =(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo= cm.getActiveNetworkInfo();
        return netInfo!=null && netInfo.isConnected();
    }

    private void resendPending(final Context context)
    {
        final Realm realm = Realm.getDefaultInstance();
        ParseQuery<ParseObject>  query = ParseQuery.getQuery(Const.PENDING);
        query.fromLocalDatastore().findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> list, ParseException e) {
                if(e==null && list.size()>0)
                {


                    //Resend all failed messages
                    ParseObject.saveAllInBackground(list, new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e==null)
                            {
                                for(int i=0; i<list.size(); i++)
                                {
                                    final int position = i;
                                    realm.executeTransactionAsync(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            Message message = realm.where(Message.class).equalTo(Const.MSG_ID,list.get(position).getString("mId")).findFirst();
                                            message.setDeliveryStatus(Const.MESSAGE_SENT);
                                            message.setMsgId(list.get(position).getObjectId());
                                            realm.close();
                                        }
                                    });
                                  //  manager.log("Failed Message "+i+" id "+list.get(i).getObjectId()+" and mId ="+list.get(i).getString("mId"));
                                }

                                ParseObject.unpinAllInBackground(list, new DeleteCallback() {
                                    @Override
                                    public void done(ParseException e) {

                                    }
                                });
                            }
                        }
                    });
                    /*for(ParseObject o: list)
                    {
                        switch (o.getInt("t"))
                        {
                            case 0:

                                HotSpot.sendMSG(context,o.getString(Const.MSG),o.getString(Const.MSG_ID),o.getString(Const.FROM),o.getString(Const.TO),true);
                                o.unpinInBackground();
                                break;
                        }

                    }*/
                }
            }
        });
    }

}
