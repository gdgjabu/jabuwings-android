package com.jabuwings.intelligence;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;

import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.image.PictureUploader;
import com.jabuwings.intelligence.lisa.Lisa;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.FeedMonger;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Falade James on 12/16/2015 All Rights Reserved.
 */
public class PostalService extends Service {
    private Looper mServiceLooper;
    private Handler mServiceHandler;
    private static String LOG_TAG=PostalService.class.getSimpleName();
    private static boolean keep=true;
    Manager manager;
    DatabaseManager databaseManager;
    SQLiteDatabase db;
    Context context;
    static PostalService instance;
    public static SwipeRefreshLayout swipeRefreshLayout;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance=this;
        context=getApplicationContext();
        manager= new Manager(context);
        databaseManager =new DatabaseManager(context);
        db= databaseManager.getWritableDatabase();
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                android.os.Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new Handler(mServiceLooper);

    }

    public void postFeed(String title, String content, Uri uri)
    {
        if(uri!=null) {
            new PictureUploader(this, null, null, Const.MEDIA_UPLOAD_TYPE.FEED_PICTURE.ordinal()).execute(uri);
        }
        else {
            HashMap<String, Object> params = new HashMap<>();
            params.put(Const.Feed.AUTHOR, manager.getUsername());
            params.put(Const.Feed.TITLE, title);
            params.put(Const.Feed.CONTENT,content);
            params.put(Const.Feed.FEED_ID, Lisa.generateFeedId(manager.getUsername()));
            params.put(Const.Feed.EXTERNAL_URL, FeedMonger.feedUrl);
            params.put(Const.VERSION,context.getString(R.string.version_number));

            try{
              String  g=  ParseCloud.callFunction(Const.FEED_MONGER_CLASS, params);
            }
            catch (ParseException e)
            {

            }
            ParseCloud.callFunctionInBackground(Const.FEED_MONGER_CLASS, params, new FunctionCallback<String>() {
                @Override
                public void done(String s, ParseException e) {
                    if (e == null) {
                        if (s.equals(Const.POSITIVE)) {
                            manager.shortToast(R.string.success_feed);
                        } else {
                            manager.shortToast(s);

                        }

                    } else {
                        ErrorHandler.handleError(context,e);
                    }


                }
            });


        }
    }

    public static PostalService getInstance()
    {
        return instance;
    }
    private class Handler extends android.os.Handler{
        public Handler(Looper looper)
        {
           super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            while (keep) {
                synchronized (this) {
                    try {

                        wait(180000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "Postal Service on initialise command called");

        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

        return Service.START_STICKY;
    }
}
