package com.jabuwings.image;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.download.ImageDownloader;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.intelligence.lisa.Lisa;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.management.Const;
import com.jabuwings.management.CreditManager;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.FileManager;
import com.jabuwings.views.hooks.FeedMonger;
import com.jabuwings.views.main.Domicile;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Falade James on 12/12/2015 All Rights Reserved.
 */
@Deprecated
public class PictureUploader extends AsyncTask<Uri, Void, ByteArrayOutputStream> {
    private boolean fromGallery;
    Uri uri;
   // String fileName;
    ParseFile file;
    ParseUser user=ParseUser.getCurrentUser();
    ImageProcessor utils;
    Context context;
    MaterialDialog dialog;
    Manager manager;
    ByteArrayOutputStream stream= new ByteArrayOutputStream();
    private String LOG_TAG=PictureUploader.class.getSimpleName();
    private String filePath;
    ImageView imageView;
    int uploadType;
    String[] extraDetails;
    Bitmap bitmap;
    boolean drawProfilePicture=false;
    public PictureUploader(Context context, MaterialDialog dialog, ImageView imageView, int uploadType, String... extraDetails) {
        this.context=context.getApplicationContext();
        manager= new Manager(context);
        this.dialog=dialog;
        utils=new ImageProcessor(context);
        this.imageView=imageView;
        this.uploadType=uploadType;
        this.extraDetails=extraDetails;
    }

    public PictureUploader(Context context, Bitmap bitmap, MaterialDialog dialog)
    {
        this.context=context;
        this.bitmap=bitmap;
        manager= new Manager(context);
        this.dialog=dialog;
        this.drawProfilePicture=true;
    }

    @Override
    protected ByteArrayOutputStream doInBackground(Uri... params) {
        if(uploadType==Const.MEDIA_UPLOAD_TYPE.PROFILE_PICTURE.ordinal() && drawProfilePicture)
        {
            sendProfilePictureToServer(bitmap);
            return null;
        }
        else{
            uri=params[0];
        return  compressImage();
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        Log.d(LOG_TAG,"ContentUri "+contentURI);
        Log.d(LOG_TAG,"ContentUri parsed path "+contentURI.getPath());
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            Log.d(LOG_TAG,"index is "+idx);
            String path;
            try{
                path=cursor.getString(idx);
                cursor.close();
                return path;
            }
            catch (Exception e)
            {
                cursor.close();
                    return contentURI.getPath();
            }

        }
    }
    private ByteArrayOutputStream compressImage() {
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bmp;
        options.inJustDecodeBounds = true;
        filePath=getRealPathFromURI(uri);
        Log.d(LOG_TAG,"file path is "+filePath);
        bmp = BitmapFactory.decodeFile(filePath,options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        if(actualHeight==0 || actualWidth==0)
        {
            return null;
        }
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

            scaledBitmap.compress(Bitmap.CompressFormat.JPEG,80,stream);

        return stream;

    }


    private void sendFeedPictureToServer(ByteArrayOutputStream stream)
    {final ParseFile picture = new ParseFile(stream.toByteArray());
        picture.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {

                if (e == null) {
                    String url = picture.getUrl();
                    HashMap<String, Object> params=new HashMap<>();
                    params.put(Const.Feed.AUTHOR,manager.getUsername());
                    params.put(Const.Feed.TITLE, FeedMonger.title);
                    params.put(Const.Feed.CONTENT, FeedMonger.content);
                    params.put(Const.Feed.EXTERNAL_URL,FeedMonger.feedUrl);
                    params.put(Const.Feed.PICTURE_URL, url);
                    params.put(Const.Feed.PICTURE_FILE,picture);
                    params.put(Const.Feed.FEED_ID, Lisa.generateFeedId(manager.getUsername()));
                    params.put(Const.VERSION,context.getString(R.string.version_number));

                    ParseCloud.callFunctionInBackground(Const.FEED_MONGER_CLASS, params, new FunctionCallback<String>() {
                        @Override
                        public void done(String s, ParseException e) {
                            if (e == null) {
                                if (s.equals(Const.POSITIVE)) {
                                    dialog.dismiss();
                                    manager.shortToast(R.string.success_feed);
                                    manager.shortToast("Alert: We have deducted " + FeedMonger.feedPriceTag + " chrimatas");
                                    FeedMonger.getInstance().finish();
                                }
                                else
                                {
                                    manager.shortToast(s);
                                    new CreditManager(context).add(FeedMonger.feedPriceTag);
                                }
                            } else {
                                dialog.dismiss();
                                ErrorHandler.handleError(context,e);
                            }


                        }
                    });

                } else {
                    dialog.dismiss();
                    manager.shortToast(R.string.error_picture_network);

                }


            }
        });}

    private void sendProfilePictureToServer(Bitmap bitmap)
    {

        try {

          //  String a=MediaStore.Images.Media.insertImage(context.getContentResolver(),bitmap,"Pro","JabuWings profile Picture");

            final File pictureFile = new FileManager(context).getProfilePictureFile();
            ByteArrayOutputStream stream;
            stream= new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            FileOutputStream fos = new FileOutputStream(pictureFile);
            final byte[] image = stream.toByteArray();
            fos.write(image);
            fos.close();


            file = new ParseFile(Const.PROFILE_PICTURE_NAME,image);
            manager.log("Saving Profile Picture");
            file.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if(e==null)
                    {
                        user.put("ProfilePicture",file);
                        user.put(Const.PROFILE_PICTURE_URL, file.getUrl());
                        user.put(Const.PROFILE_PICTURE_DATE, new Date());
                        user.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if(e==null)
                                {
                                    dialog.dismiss();
                                    try{
                                    FileOutputStream pp = context.openFileOutput(Const.PROFILE_PICTURE_NAME, Context.MODE_PRIVATE);
                                    pp.write(image);
                                    pp.close();}
                                    catch (IOException er)
                                    {
                                        er.printStackTrace();
                                    }
                                    openPicture();
                                    manager.shortToast("Profile Picture Uploaded");
                                    Domicile.refreshPicture();
                                    openPicture();
                                    manager.storeProfilePictureUrl(file.getUrl());
                                }
                                else{

                                    manager.longToast("Picture saved but unable to change profile picture");
                                }
                            }
                        });
                    }
                    else
                    {
                        dialog.dismiss();
                        manager.shortToast("Unable to upload to server");

                    }

                }
            }, new ProgressCallback() {
                @Override
                public void done(Integer integer) {


                }
            });
        }
        catch (IOException e)
        {

        }
    }

    private void sendProfilePictureToServer(ByteArrayOutputStream stream){


       final byte[] image= stream.toByteArray();
       final File pictureFile =new FileManager(context).getProfilePictureFile();

        file = new ParseFile(Const.PROFILE_PICTURE_NAME,image);

        file.saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {
                if (e == null) {
                    user.put("ProfilePicture", file);
                    user.put(Const.PROFILE_PICTURE_URL, file.getUrl());
                    user.put(Const.PROFILE_PICTURE_DATE, new Date());
                    user.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                            if (e == null) {
                                dialog.dismiss();
                                Toast.makeText(context, "Picture Uploaded and attached Successfully ", Toast.LENGTH_LONG).show();
                                Domicile.refreshPicture();
                                try {
                                    //save to gallery
                                    FileOutputStream fos = new FileOutputStream(pictureFile);
                                    fos.write(image);
                                    fos.close();


                                    FileOutputStream pp = context.openFileOutput(Const.PROFILE_PICTURE_NAME, Context.MODE_PRIVATE);
                                    pp.write(image);
                                    pp.close();
                                    openPicture();

                                } catch (Exception f) {
                                    Toast.makeText(context, "Unable to save to gallery and app : " + f.toString(), Toast.LENGTH_SHORT).show();
                                }

                                manager.storeProfilePictureUrl(file.getUrl());
                            } else {
                                dialog.dismiss();
                                ErrorHandler.handleError(context,e);

                            }

                        }
                    });

                } else {
                    dialog.dismiss();
                    Toast.makeText(context, "Unable to upload file to server", Toast.LENGTH_LONG).show();
                }

            }
        }, new ProgressCallback() {
            @Override
            public void done(Integer integer) {


            }
        });






    }

    private void sendPictureMessage(ByteArrayOutputStream stream)
    {
        final   ParseFile file= new ParseFile(String.valueOf(System.currentTimeMillis()),stream.toByteArray());
        //TODO null pointer below
        final String messageId= HouseKeeping.storeOutgoingPersonalMedia(context, filePath, "", extraDetails[0], DataProvider.MessageType.OUTGOING_IMAGE.ordinal()).getMsgId();


        file.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e==null)
                {
                    manager.shortToast("Message sent");
//                    HotSpot.sendPersonalMedia(0, file, extraDetails[0], context, messageId,extraDetails[1]);
                }

                else{
                    manager.shortToast(e.getMessage());
                    e.printStackTrace();
                    SQLiteDatabase db = new DatabaseManager(context).getWritableDatabase();

                    db.execSQL("update messages set delivery_status='" + Const.MESSAGE_FAILED + "' where msgId=?",
                            new Object[]{messageId});
                    context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_MESSAGES, null);
                }

            }
        }, new ProgressCallback() {
            @Override
            public void done(Integer percentDone) {
                SQLiteDatabase db = new DatabaseManager(context).getWritableDatabase();

                Log.d(LOG_TAG,"Progress "+percentDone);
                db.execSQL("update messages set file_progress='" + percentDone + "' where msgId=?",
                        new Object[]{messageId});
                context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_MESSAGES,null);
            }
        });

    }

    private void sendHubPicture(ByteArrayOutputStream stream)
    {
        final byte[] image= stream.toByteArray();
        File imageFile;
        File sd = Environment.getExternalStorageDirectory();
        File dir;
        try{
            dir = new File(sd.getAbsolutePath()+"/JabuWings/JabuWings Photos");
            dir.mkdirs();
        }
        catch (Exception e)
        {
            manager.shortToast(R.string.error_phone_storage);
            return;
        }
        imageFile = new File(dir, Lisa.generateImageFileName(extraDetails[0])+".jpg");
        file = new ParseFile(Const.PROFILE_PICTURE_NAME,image);

        file.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e==null)
                {
                    ParseQuery<ParseObject> query= new ParseQuery<ParseObject>(Const.HUB_CLASS_NAME);
                    query.getInBackground(extraDetails[1], new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject parseObject, ParseException e) {
                            if(e==null)
                            {
                                parseObject.put(Const.PROFILE_PICTURE,file);
                                parseObject.put(Const.PROFILE_PICTURE_DATE,new Date());

                                parseObject.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if(e==null)
                                        {
                                            dialog.dismiss();
                                            manager.shortToast("Picture uploaded");
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }
    private void openPicture()
    {
        if(imageView!=null)
        imageView.setImageBitmap(ImageDownloader.openFile(context, Const.PROFILE_PICTURE_NAME));
    }



    @Override
    protected void onPostExecute(ByteArrayOutputStream result) {
        super.onPostExecute(result);
        if(!drawProfilePicture)
        if(result!=null)
        switch (uploadType)
        {
            case 0:
                sendProfilePictureToServer(result);
                break;
            case 1:
                sendFeedPictureToServer(result);
                break;
            case 2:
                sendPictureMessage(result);
                break;
            case 3:
                sendHubPicture(result);
                break;
        }

        else{
            if(dialog!=null)
            {
                dialog.dismiss();
            }
            manager.shortToast("Unable to decode image");
        }

    }

}

