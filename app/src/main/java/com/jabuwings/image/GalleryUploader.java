package com.jabuwings.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.jabuwings.intelligence.BootHub;
import com.jabuwings.intelligence.lisa.Lisa;
import com.jabuwings.management.Manager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Falade James on 12/11/2015 All Rights Reserved.
 */
public class GalleryUploader extends AsyncTask<String, Void, Boolean> {
String fileName;
Bitmap bitmap;
Context context;
Manager manager;
    public GalleryUploader(Context context,String fileName, Bitmap bitmap) {
        this.fileName=fileName;
        this.bitmap=bitmap;
        this.context=context;
        manager=new Manager(context);
    }

    @Override
    protected Boolean doInBackground(String... params) {

        return compressImage();
    }

    private Boolean compressImage() {

        FileOutputStream out;
        try {
            File image;
            File sd = Environment.getExternalStorageDirectory();
            File dir = new File(sd.getAbsolutePath()+"/JabuWings/JabuWings Photos");
            dir.mkdirs();
            image = new File(dir, Lisa.generateImageFileName(fileName)+".jpg");
            out = new FileOutputStream(image);
            //MediaStore.Images.Media.insertImage(context.getContentResolver(),image.getAbsolutePath(),image.getName(),null);
           return bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);


        } catch (FileNotFoundException|OutOfMemoryError| NullPointerException e) {
            e.printStackTrace();
            return false;

        }


    }






    @Override
    protected void onPostExecute(Boolean successful) {
        super.onPostExecute(successful);
        if(successful) manager.shortToast("Picture saved to gallery");
        else {
            //TODO change to xml error toast
            manager.shortToast("Unable to save to gallery due to a file error");
        }
    }
}
