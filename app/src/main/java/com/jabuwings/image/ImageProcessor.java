package com.jabuwings.image;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Parcel;
import android.support.v4.util.LruCache;
import android.util.TypedValue;
import android.widget.ImageView;

import com.jabuwings.R;
import com.jabuwings.management.App;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

public class ImageProcessor {
	private Context context;
	public Bitmap icon;
	LruCache<String,Bitmap> memoryCache;
    boolean isPrivateStorage=true;
	public ImageProcessor(Context context){
		this.context = context;
		memoryCache= App.getImageCache();
        if(context!=null)
		icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.default_avatar);
	}

    public ImageProcessor(Context context, boolean isPrivateStorage){
        this.context = context;
        memoryCache= App.getImageCache();
        icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.default_avatar);
        this.isPrivateStorage=isPrivateStorage;
    }

	public int convertDipToPixels(float dips){
		 Resources r = context.getResources();
		 return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dips, r.getDisplayMetrics());
	}
	
	public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	
	    if (height > reqHeight || width > reqWidth) {
	        final int heightRatio = Math.round((float) height / (float) reqHeight);
	        final int widthRatio = Math.round((float) width / (float) reqWidth);
	        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
	    }
	    final float totalPixels = width * height;
	    final float totalReqPixelsCap = reqWidth * reqHeight * 2;

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

	    return inSampleSize;
	}
	
	public Bitmap decodeBitmapFromPath(String filePath){
		Bitmap scaledBitmap = null;
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inSampleSize = calculateInSampleSize(options, convertDipToPixels(150), convertDipToPixels(200));
		options.inDither = false;
		options.inPurgeable = true;
		options.inInputShareable = true;
		options.inJustDecodeBounds = false;
		
		scaledBitmap = BitmapFactory.decodeFile(filePath, options);		
		return scaledBitmap;
	}


    public Bitmap decodeBitmapFromUri(Uri uri)
    {
        try {
            return BitmapFactory.decodeFile(uri.getPath());

        } catch (OutOfMemoryError|NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Bitmap decodeBitmapFromPrivateStorage(String fileName)
    {
        try {
            FileInputStream imageInput;
            imageInput = context.openFileInput(fileName);
            return BitmapFactory.decodeStream(imageInput);

        } catch (IOException| OutOfMemoryError|NullPointerException e) {
           // e.printStackTrace();
            return null;
        }
    }
    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            memoryCache.put(key, bitmap);
        }
    }

	public Bitmap getBitmapFromMemCache(String key) {
		return memoryCache.get(key);
	}

    public void removeBitmapFromMemoryCache(String key)
    {
        memoryCache.remove(key);
    }

    /**
     * This method is to scale a bitmap to the size of an ImageView for memory optimization
     * @param mImageView The imageView to put the bitmap
     * @param fileName The name of the file in the private storage
     */
	public void setScaledImage(ImageView mImageView,String fileName) {
		// Get the dimensions of the View
		int targetW = mImageView.getWidth();
		int targetH = mImageView.getHeight();

		// Get the dimensions of the bitmap
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;

        try {
            FileInputStream imageInput;


            imageInput = context.openFileInput(fileName);
            BitmapFactory.decodeStream(imageInput, null, bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;

            // Determine how much to scale down the image
            int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;
            Bitmap bitmap = BitmapFactory.decodeStream(imageInput,null, bmOptions);
            mImageView.setImageBitmap(bitmap);
        }
        catch (Exception e)
        {
			e.printStackTrace();
            Bitmap bitmap= getBitmapFromMemCache(fileName);
            if(bitmap!=null)
            mImageView.setImageBitmap(getBitmapFromMemCache(fileName));
        }

	}




    public void loadBitmap(String fileName, ImageView imageView) {
        if (cancelPotentialWork(fileName, imageView)) {
            final Bitmap bitmap = getBitmapFromMemCache(fileName);
            if(bitmap != null){
                imageView.setImageBitmap(bitmap);
                //imageProcessor.setScaledImage(imageView,fileName);
            }
            else{
                final BitmapWorkerTask task = new BitmapWorkerTask(imageView,this);
                final AsyncDrawable asyncDrawable = new AsyncDrawable(context.getResources(), icon, task);
                imageView.setImageDrawable(asyncDrawable);
                task.execute(fileName);
            }
        }
    }
    public boolean cancelPotentialWork(String filePath, ImageView imageView) {

        final BitmapWorkerTask bitmapWorkerTask = BitmapWorkerTask.getBitmapWorkerTask(imageView);

        if (bitmapWorkerTask != null) {
            final String bitmapFilePath = bitmapWorkerTask.fileName;
            if (bitmapFilePath != null && !bitmapFilePath.equalsIgnoreCase(filePath)) {
                bitmapWorkerTask.cancel(true);
            } else {
                return false;
            }
        }
        return true;
    }


    public byte[] getByteFromBitmap(Bitmap b)
    {
        //b is the Bitmap

//calculate how many bytes our image consists of.
        int bytes = b.getByteCount();
//or we can calculate bytes this way. Use a different value than 4 if you don't use 32bit images.
//int bytes = b.getWidth()*b.getHeight()*4;

        ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
        b.copyPixelsToBuffer(buffer); //Move the byte data to the buffer

        byte[] array = buffer.array(); //Get the underlying array containing the data.
        return array;
    }

    public byte[] getByteFromFile(File file)
    {
        byte[] bytes = new byte[(int)file.length()];
        try{
            BufferedInputStream buf= new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes,0,bytes.length);
            buf.close();
            return bytes;
        }
        catch (IOException e)
        {
            return null;
        }
    }





}
