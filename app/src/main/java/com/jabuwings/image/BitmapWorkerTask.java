package com.jabuwings.image;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.widget.ImageView;

import com.jabuwings.management.App;

import java.lang.ref.WeakReference;

public class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
	private final WeakReference<ImageView> imageViewReference;
			public String fileName;
            ImageProcessor utils;
            private LruCache<String, Bitmap> memoryCache;
			public BitmapWorkerTask(ImageView imageView, ImageProcessor utils){
                this.utils=utils;
				imageViewReference = new WeakReference<>(imageView);
                memoryCache= App.getImageCache();
			}
			@Override
			protected Bitmap doInBackground(String... params) {
				fileName = params[0];Bitmap bitmap;
				if(!utils.isPrivateStorage) bitmap=utils.decodeBitmapFromPath(fileName);
				else
				bitmap = utils.decodeBitmapFromPrivateStorage(fileName);
				addBitmapToMemoryCache(fileName, bitmap);
				return bitmap;
			}
			@Override
			protected void onPostExecute(Bitmap bitmap) {
				if (isCancelled()) {
		            bitmap = null;
		        }
				if(imageViewReference != null && bitmap != null){
					final ImageView imageView = imageViewReference.get();
					final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);
		            if (this == bitmapWorkerTask && imageView != null) {
		                imageView.setImageBitmap(bitmap);
		            }
				}
			}

    public static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getBitmapWorkerTask();
            }
        }
        return null;
    }
    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
			if(bitmap!=null)
            memoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return memoryCache.get(key);
    }
		}