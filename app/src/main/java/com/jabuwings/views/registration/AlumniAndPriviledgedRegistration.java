package com.jabuwings.views.registration;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.encryption.Crypt;
import com.jabuwings.intelligence.lisa.Lisa;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.EmailVerificationHook;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.main.Login;
import com.jabuwings.views.main.LoginHook;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseACL;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.rey.material.app.DatePickerDialog;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.widget.Spinner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class AlumniAndPriviledgedRegistration extends JabuWingsActivity implements View.OnClickListener {
    Spinner spStateOfOrigin,spCountryOfOrigin,spSex;
    Spinner spOfficialDepartment,spYearOfGraduation;
    String[] arStateOfOrigin,arCountryOfOrigin;
    String[] arOfficialDepartment;
    ArrayAdapter<String> adpStateOfOrigin;
    ArrayAdapter<String> adpCountryOfOrigin;
    ArrayAdapter<String> adpOfficialDepartment;
    ArrayAdapter<String> adpSex;
    EditText etFirstName;
    EditText etLastName;
    Button btRegister;
    EditText etPhoneNo;
    EditText etUsername;
    EditText etEmail;
    EditText etPassword;
    EditText etPasswordAgain;
    Button btDob;
    String dob,birthday;
    Manager manager;
    int type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        type=getIntent().getIntExtra(Const.TYPE,0);
        String title= type==0 ? getString(R.string.alumini_registration) : getString(R.string.priviledged_registration);
        setContentView(R.layout.activity_alumini_registration);
        setUpToolbar(null, title);
        etFirstName= (EditText)findViewById(R.id.alr_first_name);
        etLastName= (EditText)findViewById(R.id.alr_last_name);
        etPhoneNo= (EditText)findViewById(R.id.alr_phone_no);
        etUsername= (EditText)findViewById(R.id.alr_username);
        btDob=(Button)findViewById(R.id.alr_dob);
        btDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpYearPicker();
            }
        });
        manager= new Manager(getApplicationContext());
        etEmail= (EditText)findViewById(R.id.alr_email);
        etPassword= (EditText)findViewById(R.id.alr_password);
        etPasswordAgain= (EditText)findViewById(R.id.alr_password_again);
        spSex=(Spinner)findViewById(R.id.alr_sex);

        spStateOfOrigin= (Spinner)findViewById(R.id.alr_states);
        spCountryOfOrigin= (Spinner)findViewById(R.id.alr_countries);
        spOfficialDepartment=(Spinner)findViewById(R.id.alr_department);
        spYearOfGraduation=(Spinner)findViewById(R.id.alr_year_of_graduation);
        if(type==1)
        {
            spOfficialDepartment.setVisibility(View.GONE);
            spYearOfGraduation.setVisibility(View.GONE);
        }
        btRegister= (Button)findViewById(R.id.alr_register); btRegister.setOnClickListener(this);
        arStateOfOrigin= getResources().getStringArray(R.array.nigerian_states);
        arCountryOfOrigin= getResources().getStringArray(R.array.countries_of_the_world);
        arOfficialDepartment=getResources().getStringArray(R.array.departments);
        String[] arYearOfGraduation={Const.CHOOSE,"2016","2015","2014","2013","2012","2011","2010"};
        String[] sexes={Const.CHOOSE,"Male","Female"};
        ArrayAdapter<String> adpYearOfGraduation=new ArrayAdapter<>(getApplicationContext(), R.layout.row_spn,arYearOfGraduation); adpYearOfGraduation.setDropDownViewResource(R.layout.row_spn_dropdown);
        adpStateOfOrigin= new ArrayAdapter<>(getApplicationContext(), R.layout.row_spn,arStateOfOrigin); adpStateOfOrigin.setDropDownViewResource(R.layout.row_spn_dropdown);
        adpCountryOfOrigin= new ArrayAdapter<>(getApplicationContext(), R.layout.row_spn,arCountryOfOrigin); adpCountryOfOrigin.setDropDownViewResource(R.layout.row_spn_dropdown);
        spStateOfOrigin.setAdapter(adpStateOfOrigin);
        spCountryOfOrigin.setAdapter(adpCountryOfOrigin);
        adpOfficialDepartment= new ArrayAdapter<>(getApplicationContext(), R.layout.row_spn,arOfficialDepartment); adpOfficialDepartment.setDropDownViewResource(R.layout.row_spn_dropdown);
        spOfficialDepartment.setAdapter(adpOfficialDepartment);
        spYearOfGraduation.setAdapter(adpYearOfGraduation);
        adpSex=new ArrayAdapter<>(this,R.layout.row_spn,sexes);adpSex.setDropDownViewResource(R.layout.row_spn_dropdown);
        spSex.setAdapter(adpSex);


        spStateOfOrigin.setOnItemClickListener(new Spinner.OnItemClickListener() {
            @Override
            public boolean onItemClick(Spinner parent, View view, int position, long id) {
                if (position==arStateOfOrigin.length-1)
                {
                    //spCountryOfOrigin.setVisibility(View.VISIBLE);
                    spStateOfOrigin.setVisibility(View.GONE);}
                else{
                    spStateOfOrigin.setSelection(position);

                }
                return false;
            }
        });
        new MaterialDialog.Builder(this)
                .title(R.string.terms_and_conditions)
                .content(R.string.terms)
                .positiveText(R.string.agree)
                .cancelable(false)
                .negativeText(R.string.disagree)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                        Uri packageUri = Uri.parse("package:com.jabuwings");
                        Intent uninstallIntent =
                                new Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageUri);
                        startActivity(uninstallIntent);

                    }
                })
                .show();

    }


    public boolean isEmpty(String value)

    {
        return TextUtils.isEmpty(value);


    }
    public void setUpYearPicker(){
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Dialog.Builder builder;
        builder = new DatePickerDialog.Builder(){
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                DatePickerDialog dialog = (DatePickerDialog)fragment.getDialog();
                dialog.getMonth();
                dialog.getDay();

                dob = dialog.getFormattedDate(sdf);
                birthday=dialog.getFormattedDate(new SimpleDateFormat("MM-dd", Locale.US));
                btDob.setText(dob);

                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {

                super.onNegativeActionClicked(fragment);
            }
        }.dateRange(1,1,1930,31,12,2002);

        builder.positiveAction("OK")
                .negativeAction("CANCEL");
        DialogFragment fragment= DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(),null);
    }
    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.alr_register)

        {
            boolean cancel= false;
            String firstName= etFirstName.getText().toString();
            String lastName= etLastName.getText().toString();
            String stateOfOrigin= spStateOfOrigin.getSelectedItem().toString();
            String countryOfOrigin=spCountryOfOrigin.getSelectedItem().toString();
            String officialDepartment= spOfficialDepartment.getSelectedItem().toString();
            String sex=spSex.getSelectedItem().toString();
            switch (spSex.getSelectedItemPosition())
            {
                case 0:
                    sex=Const.CHOOSE;
                    break;
                case 1:
                    sex="M";
                    break;
                case 2:
                    sex="F";
                    break;
            }
            String yearOfGraduation=spYearOfGraduation.getSelectedItem().toString();
            String phoneNo= etPhoneNo.getText().toString();
            final String username = etUsername.getText().toString().toLowerCase();
            String email= etEmail.getText().toString();
            final String password=etPassword.getText().toString();
            String passwordAgain=etPasswordAgain.getText().toString();





            //check for a valid username
            {
                if (username.length() <= 2) {

                    etUsername.setError(getString(R.string.error_invalid_username));

                    cancel = true;
                } else if (username.length() > 15) {

                    etUsername.setError(getString(R.string.error_invalid_username_long));
                    cancel = true;
                }
                else if (!username.matches("[a-zA-Z0-9_]*")) {

                    etUsername.setError(getString(R.string.error_invalid_character));
                    cancel = true;

                }
                else if(TextUtils.isDigitsOnly(String.valueOf(username.charAt(0))))
                {
                    etUsername.setError(getString(R.string.error_username_char));
                    cancel=true;
                }

                else if(username.charAt(0)=='h'&&username.charAt(1)=='u'&& username.charAt(2)=='b')
                {
                    etUsername.setError(getString(R.string.error_username));
                    cancel = true;
                }
                else if (manager.getForbiddenUsernames().contains(username)) {

                    etUsername.setError(getString(R.string.error_username));

                    cancel = true;

                }
            }

            //check for a valid first name
            {
                if (TextUtils.isEmpty(firstName)) {
                    etFirstName.setError(getString(R.string.error_field_required));
                    cancel = true;
                } else if (firstName.length() <= 2) {

                    etFirstName.setError(getString(R.string.error_invalid_name));
                    cancel = true;
                }
                else if (!firstName.matches("[a-zA-Z]*")) {

                    etFirstName.setError(getString(R.string.error_invalid_character));
                    cancel = true;

                }
            }
            //check for a valid last name
            {
                if (TextUtils.isEmpty(lastName)) {
                    etLastName.setError(getString(R.string.error_field_required));
                    cancel = true;
                } else if (lastName.length() <= 2) {

                    etLastName.setError(getString(R.string.error_invalid_name));
                    cancel = true;
                }
                else if (!lastName.matches("[a-zA-Z ]*")) {

                    etLastName.setError(getString(R.string.error_invalid_character));
                    cancel = true;

                }
            }

            if (TextUtils.isEmpty(email)) {
                etEmail.setError(getString(R.string.error_field_required));
                cancel = true;
            } else if (!email.contains("@") || !email.contains(".")|| (email.length()<7)) {
                etEmail.setError(getString(R.string.error_invalid_email));
                cancel = true;
            }

            {
                if(spOfficialDepartment.getSelectedItemPosition()==0 &&type==0)
                {
                    manager.shortToast("Please select a department");
                    cancel= true;
                }
            }

            {
                if (spStateOfOrigin.getVisibility()==View.VISIBLE &&   stateOfOrigin.equals(Const.CHOOSE))
                {
                    manager.shortToast(getString(R.string.error_invalid_state));
                    cancel=true;
                }
            }

            {
                if (countryOfOrigin.equals(Const.CHOOSE) && spCountryOfOrigin.getVisibility()==View.VISIBLE)
                {
                    manager.shortToast(R.string.error_invalid_country);
                    cancel=true;

                }


            }
            {
                if(sex.equals(Const.CHOOSE))
                {
                    manager.shortToast(R.string.error_invalid_sex);
                    cancel=true;
                }
            }
            {
                if(TextUtils.isEmpty(phoneNo)|| phoneNo.length()<11)
                {
                    etPhoneNo.setError(getString(R.string.error_invalid_phone_no));
                    cancel=true;
                }
            }
            try
            {
                if (dob.contains(DateFormat.getTimeInstance().format(new Date()))|| dob==null|| dob.contains(String.valueOf(new Time().getYear())))
                {
                    btDob.setError(getString(R.string.error_invalid_date_of_birth));
                    cancel=true;
                }
            }
            catch (Exception e)
            {
                btDob.setError(getString(R.string.error_invalid_date_of_birth));
                cancel=true;
            }






            if(!password.equals(passwordAgain))

            {
                new MaterialDialog.Builder(AlumniAndPriviledgedRegistration.this)
                        .title("Passwords do not match")
                        .positiveText("Check")
                        .show();
                cancel=true;
            }


            boolean cancelAlumini= type==0 && (isEmpty(officialDepartment) || yearOfGraduation.equals(Const.CHOOSE));

            if(cancelAlumini){manager.errorToast("Check your department and year of graduation");}



            if(isEmpty(firstName)|| isEmpty(lastName)|| isEmpty(stateOfOrigin)|| cancelAlumini ||isEmpty(phoneNo)
                    || isEmpty(username)|| isEmpty(email)|| isEmpty(password)|| isEmpty(passwordAgain) || isEmpty(sex))

            {

                cancel=true;

            }





            if(cancel)
            {
                new MaterialDialog.Builder(AlumniAndPriviledgedRegistration.this)
                        .title("All fields must be correctly filled")
                        .positiveText("Check")
                        .show();
                return;

            }

            String name = Lisa.generateCapitalisedName(firstName,lastName);

            HashMap<String, Object> params = new HashMap<>();

            params.put(Const.USERNAME,username);
            params.put(Const.EMAIL, email);
            params.put(Const.PASSWORD, password);
            params.put(Const.NAME, name);
            params.put(Const.DATE_OF_BIRTH,dob);
            params.put(Const.BIRTHDAY,birthday);
            if(stateOfOrigin.equals(Const.CHOOSE))
            params.put(Const.STATE, stateOfOrigin);
            if(countryOfOrigin.equals(Const.CHOOSE))
                params.put(Const.COUNTRY,Const.NIGERIA);
            params.put(Const.PHONE_NO, "0"+phoneNo);
            params.put(Const.SEX,sex);
            if(type==0){
            params.put(Const.DEPARTMENT,officialDepartment);
            params.put(Const.YEAR_OF_GRADUATION,yearOfGraduation);
                params.put(Const.ACCOUNT_TYPE, Const.REGISTRATION_TYPE.ALUMNI.ordinal());}
            else{
                params.put(Const.ACCOUNT_TYPE, Const.REGISTRATION_TYPE.PRIVILEDGED.ordinal());
            }


            final SweetAlertDialog dialog = new DialogWizard(AlumniAndPriviledgedRegistration.this).showProgress(getString(R.string.please_wait),getString(R.string.logging_in),false);

            ParseCloud.callFunctionInBackground(Const.USER_CREATOR, params, new FunctionCallback<String>() {
                @Override
                public void done(String s, ParseException e) {
                    if(e==null)
                    {
                        ParseUser.logInInBackground(username, password, new LogInCallback() {
                            @Override
                            public void done(ParseUser parseUser, ParseException e) {

                                if(e==null)
                                {
                                    parseUser.getParseObject(Const.USER_DATA).fetchIfNeededInBackground(new GetCallback<ParseObject>() {
                                        @Override
                                        public void done(ParseObject parseObject, ParseException e) {

                                            if(e==null)
                                            {
                                                parseObject.pinInBackground();
                                                dialog.dismiss();


                                                context.deleteFile(Const.PROFILE_PICTURE_NAME);
                                                manager.longToast(R.string.registration_message);
                                                finish();
                                                manager.deletePreferences();
                                                manager.getDatabaseManager().deleteDatabase();
                                                manager.setSHA(Crypt.getSHA256(password));
                                                manager.createLoginSession(username);
                                                startActivity(new Intent(AlumniAndPriviledgedRegistration.this, EmailVerificationHook.class));
                                            }
                                            else{
                                                new DialogWizard(AlumniAndPriviledgedRegistration.this).showErrorDialog("",e.getMessage());
                                            }

                                        }
                                    });

                                }
                                else new DialogWizard(AlumniAndPriviledgedRegistration.this).showErrorDialog("",e.getMessage());
                            }
                        });

                    }
                    else new DialogWizard(AlumniAndPriviledgedRegistration.this).showErrorDialog("",e.getMessage());
                }
            });

















        }


    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                startActivity(new Intent(this,LoginHook.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(this, LoginHook.class));
    }
}

