package com.jabuwings.views.registration;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

/**
 * Created by Falade James on 9/19/2015.  .
 *
 */
public class StudentDisclaimer extends Fragment implements Step {
//Button next;
    TextView tvTerms;
View rootView;


    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {


        rootView= inflater.inflate(R.layout.student_disclaimer, container, false);
//        StudentRegistration.toolbar.setTitle();
//        next = (Button)rootView.findViewById(R.id.sd_next);
        tvTerms=(TextView)rootView.findViewById(R.id.tv_terms);

        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new MaterialDialog.Builder(getActivity())
                        .title(R.string.terms_and_conditions)
                        .content(R.string.terms)
                        .positiveText(R.string.agree)
                        .cancelable(false)
                        .negativeText(R.string.disagree)
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                getActivity().finish();
                                Uri packageUri = Uri.parse("package:com.jabuwings");
                                Intent uninstallIntent =
                                        new Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageUri);
                                startActivity(uninstallIntent);

                            }
                        })
                        .show();
            }
        });


        /*next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                StudentRegistration.toolbar.setTitle(getString(R.string.personal_details));
               StudentRegistration.mPager.setCurrentItem(StudentRegistration.mPager.getCurrentItem()+1);
            }
        });
*/


           return rootView;
    }
}
