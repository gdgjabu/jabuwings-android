package com.jabuwings.views.registration;

import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.jabuwings.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;

import com.stepstone.stepper.viewmodel.StepViewModel;

public class StepAdapter extends AbstractFragmentStepAdapter {

    public StepAdapter(@NonNull FragmentManager fm, @NonNull Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(int position) {
        Fragment fragment = null;

        switch (position) {
            case 0:
                return new StudentDisclaimer();
            case 1:
                return new PersonalDetails();
            case 2:
                return new AcademicDetails();
            case 3:
                return new AccountDetails();


            default:
                return  null;


        }

    }

    @Override
    public int getCount() {
        return 4;
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        StepViewModel.Builder builder = new StepViewModel.Builder(context)
                .setTitle(R.string.title_activity_student_registration);
        switch (position) {
            case 0:
                builder
                        .setNextButtonLabel("This way")
                        .setTitle("Disclaimer");
                break;
            case 1:
                builder
                        .setNextButtonLabel("Academic")
                        .setTitle("Personal")
                        .setBackButtonLabel("Disclaimer");
                break;
            case 2:
                builder
                        .setNextButtonLabel("Account")
                        .setTitle("Academic")
                        .setBackButtonLabel("Academic");
                break;
            case 3:
                builder.setBackButtonLabel("Go back").setTitle("Account");
                break;
            default:
                throw new IllegalArgumentException("Unsupported position: " + position);
        }
        return builder.create();
    }
}