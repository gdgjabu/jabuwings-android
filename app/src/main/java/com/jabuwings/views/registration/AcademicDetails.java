package com.jabuwings.views.registration;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.jabuwings.R;
import com.rey.material.widget.Button;
import com.rey.material.widget.Spinner;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

;

/**
 * A simple {@link Fragment} subclass.
 */
public class AcademicDetails extends Fragment implements Spinner.OnItemClickListener,Step {
EditText etMatricNo;
static String mMatricNo;
static String mLevel;
static String mDepartment;
static String mModeOfEntry;
View rootView;
Spinner spModeOfEntry;
Spinner spDepartment;
Spinner spLevel;
String[] arModeOfEntry;
ArrayAdapter<String>adpModeOfEntry;
String[] arDepartment;
ArrayAdapter<String>adpDepartment;
static String[] arLevel;
ArrayAdapter<String>adpLevel;
/*Button prev;
Button next;*/
boolean cancel= false;

PersonalDetails details;

    public AcademicDetails() {
        // Required empty public constructor
    }

    @Override
    public VerificationError verifyStep() {
        if (checkValues())
        {
            return new VerificationError(getString(R.string.error_fields_must_be_filled));

                    }

        else{

            //checkValues();

           return null;
        }

    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.academic_details, container, false);


        etMatricNo= (EditText)rootView.findViewById(R.id.ad_matric_no);
        spModeOfEntry= (Spinner)rootView.findViewById(R.id.ad_mode_of_entry);
        spDepartment = (Spinner)rootView.findViewById(R.id.ad_department);
        spLevel = (Spinner)rootView.findViewById(R.id.ad_level);
        /*prev= (Button)rootView.findViewById(R.id.ad_reg_prev); prev.setOnClickListener(this);
        next= (Button)rootView.findViewById(R.id.ad_reg_next); next.setOnClickListener(this);*/

        arModeOfEntry = getResources().getStringArray(R.array.mode_of_entry);
        adpModeOfEntry= new ArrayAdapter<>(getActivity(), R.layout.row_spn,arModeOfEntry); adpModeOfEntry.setDropDownViewResource(R.layout.row_spn_dropdown);
        spModeOfEntry.setAdapter(adpModeOfEntry);

        spModeOfEntry.setOnItemClickListener(this);

        arDepartment= getResources().getStringArray(R.array.departments);
        adpDepartment= new ArrayAdapter<>(getActivity(), R.layout.row_spn,arDepartment); adpDepartment.setDropDownViewResource(R.layout.row_spn_dropdown);
        spDepartment.setAdapter(adpDepartment);

        arLevel= getResources().getStringArray(R.array.levels);
        adpLevel= new ArrayAdapter<>(getActivity(), R.layout.row_spn,arLevel); adpLevel.setDropDownViewResource(R.layout.row_spn_dropdown);
        spLevel.setAdapter(adpLevel);






        return rootView;
    }

   /* @Override
    public void onClick(View v) {
         CustomViewPager pager = StudentRegistration.mPager;
        switch(v.getId())
        {

            case R.id.ad_reg_prev:
                StudentRegistration.toolbar.setTitle(getString(R.string.personal_details));
              pager.setCurrentItem(pager.getCurrentItem()-1);
                break;
            case R.id.ad_reg_next:
                if (checkValues())
                {
                    Toast.makeText(getActivity(),getString(R.string.error_fields_must_be_filled),Toast.LENGTH_SHORT).show();
                }

                else{

                    //checkValues();

                    pager.setCurrentItem(pager.getCurrentItem()+1);
                    StudentRegistration.toolbar.setTitle(getString(R.string.account_details));
                }

                break;



        }


    }*/


    public boolean checkValues(){
        View focusView ;
        focusView= null;
        boolean privilege=false;
        mMatricNo = etMatricNo.getText().toString().toUpperCase(); AccountDetails.fnMatricNo=mMatricNo;
        Log.d("check", "Matric No is "+mMatricNo);
        mLevel=spLevel.getSelectedItem().toString();  AccountDetails.fnLevel= mLevel;
        Log.d("check", "Level is "+mLevel);
        mDepartment=spDepartment.getSelectedItem().toString(); AccountDetails.fnDepartment= mDepartment;
        Log.d("check", "Department is "+mDepartment);
        mModeOfEntry=spModeOfEntry.getSelectedItem().toString(); AccountDetails.fnModeOfEntry=mModeOfEntry;
        Log.d("check", "Mode of entry is "+mModeOfEntry);


        // check for a valid matriculation number
        try {
            if (mMatricNo.charAt(0) == '0' && spLevel.getSelectedItemPosition() == 1) {
                privilege = true;
            }
        }
        catch (Exception e)
        {}
        {
            if (TextUtils.isEmpty(mMatricNo)) {
                etMatricNo.setError(getString(R.string.error_field_required));
                focusView = etMatricNo;
                cancel = true;
            }

            else if (mMatricNo.length() < 10 || mMatricNo.length() > 11) {

                etMatricNo.setError(getString(R.string.error_invalid_matric_no));
                focusView = etMatricNo;
                cancel = true;
            } else if (!mMatricNo.matches("[FC0-9]*")) {

                etMatricNo.setError(getString(R.string.error_invalid_character));
                focusView = etMatricNo;
                cancel = true;

            } else if (mMatricNo.charAt(0) != 'F' && mMatricNo.charAt(0) != 'C' &&
                    mMatricNo.charAt(0) != '1'&&!privilege) {

                etMatricNo.setError(getString(R.string.error_invalid_matric_no));
                focusView = etMatricNo;
                cancel = true;

            }
        }

        {
            if (spModeOfEntry.getSelectedItemPosition()==0)
            {
                cancel= true;
                focusView= spModeOfEntry;
            }
        }

        {
            if(spDepartment.getSelectedItemPosition()==0)
            {
                cancel= true;
                focusView= spDepartment;
            }
        }

        {
            if (spLevel.getSelectedItemPosition()== 0)
            {
                cancel= true;
                focusView= spLevel;
            }
        }


        if(focusView==null)
        {
            return false;
        }

        else{
            focusView.requestFocus();
            Toast.makeText(getActivity(),getString(R.string.error_fields_must_be_filled),Toast.LENGTH_SHORT).show();

            return true;
        }



        
    }

    @Override
    public boolean onItemClick(Spinner parent, View view, int position, long id) {

        try {

            mMatricNo = etMatricNo.getText().toString().toUpperCase();
            boolean foundation;
            foundation = false;
            char firstChar;
            char secondChar;
            if (mMatricNo.charAt(0) == 'F') {
                foundation = true;
            }


            if (foundation) {
                firstChar = mMatricNo.charAt(1);
                secondChar = mMatricNo.charAt(2);
            } else {
                firstChar = mMatricNo.charAt(0);
                secondChar = mMatricNo.charAt(1);
            }
            StringBuilder builder = new StringBuilder();
            int yearOfEntry = Integer.valueOf(builder.append(firstChar).append(secondChar).toString());

                switch (position) {
                    case 1:
                        spModeOfEntry.setSelection(1);

                        if (yearOfEntry == 15 && foundation)
                            spLevel.setSelection(2);
                        if (yearOfEntry == 16)
                            spLevel.setSelection(1);
                        else {
                            spLevel.setSelection(16 - yearOfEntry + 1);
                        }


                        break;
                    case 2:

                        spModeOfEntry.setSelection(2);
                        spLevel.setSelection(16 - yearOfEntry + 1);
                        break;
                    case 3:

                        spModeOfEntry.setSelection(3);
                        spLevel.setSelection(16 - yearOfEntry + 2);
                        break;
                    case 4:

                        spModeOfEntry.setSelection(4);
                        spLevel.setSelection(1);
                        break;
                    case 5:

                        spModeOfEntry.setSelection(5);
                        spLevel.setVisibility(View.GONE);
                        break;
                    case 6:

                        spModeOfEntry.setSelection(6);
                        spLevel.setVisibility(View.GONE);
                        break;
                }

            if(firstChar=='0')
            {
                spLevel.setSelection(1);
            }



        }
        catch (Exception e)
        {e.printStackTrace();
            spModeOfEntry.setSelection(position);
        }

        return false;
    }
    public static String getMatricNo(){ return mMatricNo;}
    public static String getDepartment(){return  mDepartment ;}
    public static String getLevel(){return  mLevel;}
    public static String getModeOfEntry(){ return mModeOfEntry;}


}
