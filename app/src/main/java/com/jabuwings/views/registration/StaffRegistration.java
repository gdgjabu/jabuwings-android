package com.jabuwings.views.registration;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.encryption.Crypt;
import com.jabuwings.intelligence.lisa.Lisa;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.EmailVerificationHook;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.main.LoginHook;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.rey.material.app.DatePickerDialog;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;

import com.rey.material.widget.Spinner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class StaffRegistration extends JabuWingsActivity implements View.OnClickListener{
Spinner spStateOfOrigin;
Spinner spOfficialDepartment;
Spinner spAcademicDepartment;
Spinner spSex;
String[] arStateOfOrigin;
String[] arOfficialDepartment;
String[] arAcademicDepartment;
ArrayAdapter<String> adpStateOfOrigin;
ArrayAdapter<String> adpOfficialDepartment;
ArrayAdapter<String> adpAcademicDepartment;
ArrayAdapter<String> adpSex;
EditText etFirstName;
EditText etLastName;
Button btRegister,btDob;
EditText etPhoneNo;
EditText etUsername;
EditText etEmail;
EditText etPassword;
EditText etPasswordAgain;
Manager manager;
boolean academic=false;
String dob,birthday,country;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        setContentView(R.layout.activity_staff_registration);
        setUpToolbar(null,getString(R.string.title_activity_staff_registration));
        etFirstName= (EditText)findViewById(R.id.sr_first_name);
        etLastName= (EditText)findViewById(R.id.sr_last_name);
        etPhoneNo= (EditText)findViewById(R.id.sr_phone_no);
        etUsername= (EditText)findViewById(R.id.sr_username);
        manager= new Manager(getApplicationContext());
        etEmail= (EditText)findViewById(R.id.sr_email);
        etPassword= (EditText)findViewById(R.id.sr_password);
        etPasswordAgain= (EditText)findViewById(R.id.sr_password_again);
        spStateOfOrigin= (Spinner)findViewById(R.id.sr_states);
        spOfficialDepartment=(Spinner)findViewById(R.id.sr_official_department);
        spAcademicDepartment=(Spinner)findViewById(R.id.sr_academic_department);
        spSex=(Spinner)findViewById(R.id.sr_sex);
        btDob=(Button)findViewById(R.id.sr_dob);
        btDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpYearPicker();
            }
        });
        btRegister= (Button)findViewById(R.id.sr_register); btRegister.setOnClickListener(this );
        arStateOfOrigin= getResources().getStringArray(R.array.nigerian_states);
        arOfficialDepartment=getResources().getStringArray(R.array.staff_departments);
        arAcademicDepartment=getResources().getStringArray(R.array.departments);
        String[] sexes={Const.CHOOSE,"Male","Female"};
        adpStateOfOrigin= new ArrayAdapter<>(getApplicationContext(), R.layout.row_spn,arStateOfOrigin); adpStateOfOrigin.setDropDownViewResource(R.layout.row_spn_dropdown);
        spStateOfOrigin.setAdapter(adpStateOfOrigin);
        adpOfficialDepartment= new ArrayAdapter<>(getApplicationContext(), R.layout.row_spn,arOfficialDepartment); adpOfficialDepartment.setDropDownViewResource(R.layout.row_spn_dropdown);
        spOfficialDepartment.setAdapter(adpOfficialDepartment);
        adpAcademicDepartment=new ArrayAdapter<>(this,R.layout.row_spn,arAcademicDepartment);adpAcademicDepartment.setDropDownViewResource(R.layout.row_spn_dropdown);
        spAcademicDepartment.setAdapter(adpAcademicDepartment);
        adpAcademicDepartment=new ArrayAdapter<>(this,R.layout.row_spn,arAcademicDepartment);adpAcademicDepartment.setDropDownViewResource(R.layout.row_spn_dropdown);
        adpSex=new ArrayAdapter<>(this,R.layout.row_spn,sexes);adpSex.setDropDownViewResource(R.layout.row_spn_dropdown);
        spSex.setAdapter(adpSex);
        spOfficialDepartment.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(Spinner parent, View view, int position, long id) {

                if(position==1)
                {
                    academic=true;
                    spAcademicDepartment.setVisibility(View.VISIBLE);
                }
                else{
                    academic=false;
                    spAcademicDepartment.setVisibility(View.GONE);
                }
            }
        });



        new MaterialDialog.Builder(this)
                .title(R.string.terms_and_conditions)
                .content(R.string.terms)
                .positiveText(R.string.agree)
                .cancelable(false)
                .negativeText(R.string.disagree)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                        Uri packageUri = Uri.parse("package:com.jabuwings");
                        Intent uninstallIntent =
                                new Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageUri);
                        startActivity(uninstallIntent);

                    }
                })
                .show();

    }


    public boolean isEmpty(String value)

    {
        return TextUtils.isEmpty(value);


    }
    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.sr_register)

        {
            boolean cancel= false;
            String firstName= etFirstName.getText().toString();
            String lastName= etLastName.getText().toString();
            String stateOfOrigin= spStateOfOrigin.getSelectedItem().toString();
            String officialDepartment= spOfficialDepartment.getSelectedItem().toString();
            final String academicDepartment=spAcademicDepartment.getSelectedItem().toString();
            String sex=spSex.getSelectedItem().toString();
            switch (spSex.getSelectedItemPosition())
            {
                case 0:
                    sex=Const.CHOOSE;
                    break;
                case 1:
                    sex="M";
                    break;
                case 2:
                    sex="F";
                    break;
            }
            String phoneNo= etPhoneNo.getText().toString();
            final String username = etUsername.getText().toString().toLowerCase();
            String email= etEmail.getText().toString();
            final String password=etPassword.getText().toString();
            String passwordAgain=etPasswordAgain.getText().toString();



            if (!email.contains("@") || !email.contains(".")|| (email.length()<7)) {
                etEmail.setError(getString(R.string.error_invalid_email));
                cancel = true;
            }
            else if(!email.split("@")[1].equals("jabu.edu.ng"))
        {
            etEmail.setError(getString(R.string.error_invalid_staff_email));
            cancel = true;
        }

            //check for a valid username
            {
                if (username.length() <= 2) {

                    etUsername.setError(getString(R.string.error_invalid_username));

                    cancel = true;
                }
                else if (username.length() > 15) {

                    etUsername.setError(getString(R.string.error_invalid_username_long));
                    cancel = true;
                }
                else if (!username.matches("[a-zA-Z0-9_]*")) {

                    etUsername.setError(getString(R.string.error_invalid_character));
                    cancel = true;

                }
                else if(TextUtils.isDigitsOnly(String.valueOf(username.charAt(0))))
                {
                    etUsername.setError(getString(R.string.error_username_char));
                    cancel=true;
                }
                else if(username.charAt(0)=='h'&&username.charAt(1)=='u'&& username.charAt(2)=='b')
                {
                    etUsername.setError(getString(R.string.error_username));
                    cancel = true;
                }
                else if (manager.getForbiddenUsernames().contains(username) ) {
                    etUsername.setError(getString(R.string.error_username));
                    cancel = true;

                }
            }

            if(!password.equals(passwordAgain))

            {
                new MaterialDialog.Builder(StaffRegistration.this)
                        .title("Passwords do not match")
                        .positiveText("Check")
                        .show();
              cancel=true;
            }





            if(isEmpty(firstName)|| isEmpty(lastName)|| isEmpty(stateOfOrigin)|| isEmpty(officialDepartment)||isEmpty(phoneNo)
                    || isEmpty(username)|| isEmpty(email)|| isEmpty(password)|| isEmpty(passwordAgain) || isEmpty(sex))

            {

                cancel=true;

            }


            if(officialDepartment.equals(Const.CHOOSE))
            {
                cancel=true;
            }
            if(academic && academicDepartment.equals(Const.CHOOSE))
            {
                cancel=true;
            }
            if(sex.equals(Const.CHOOSE)) cancel=true;

            try
            {
                if (dob.contains(DateFormat.getTimeInstance().format(new Date()))|| dob==null|| dob.contains(String.valueOf(new Time().getYear())))
                {
                    btDob.setError(getString(R.string.error_invalid_date_of_birth));
                    cancel=true;
                }
            }
            catch (Exception e)
            {
                btDob.setError(getString(R.string.error_invalid_date_of_birth));
                cancel=true;
            }

            if(cancel)
            {
                new MaterialDialog.Builder(StaffRegistration.this)
                        .title("All fields must be correctly filled")
                        .positiveText("Check")
                        .show();
                return;

            }







            String name = Lisa.generateCapitalisedName(firstName,lastName);
            final HashMap<String, Object> params = new HashMap<>();
            params.put(Const.VERSION, getString(R.string.version_number));
            params.put(Const.USERNAME,username);
            params.put(Const.EMAIL,email);
            params.put(Const.PASSWORD,password);
            params.put(Const.NAME,name);
            params.put(Const.DATE_OF_BIRTH,dob);
            params.put(Const.BIRTHDAY,birthday);
            params.put(Const.STATE,stateOfOrigin);
            if(!academicDepartment.equals(Const.CHOOSE))
                params.put(Const.ACADEMIC_DEPARTMENT,academicDepartment);
            if(country==null)
            params.put(Const.COUNTRY,Const.NIGERIA);
            else params.put(Const.COUNTRY,country);
            params.put(Const.PHONE_NO,"0"+phoneNo);
            params.put(Const.SEX,sex);
            params.put(Const.DEPARTMENT,officialDepartment);
            params.put(Const.TYPE,Const.REGISTRATION_TYPE.STAFF.ordinal());





            final SweetAlertDialog dialog = new DialogWizard(StaffRegistration.this).showProgress(getString(R.string.please_wait),getString(R.string.logging_in),false);


            ParseCloud.callFunctionInBackground(Const.USER_CREATOR, params, new FunctionCallback<String>() {
                @Override
                public void done(String s, ParseException e) {
                    if(e==null)
                    {
                          ParseUser.logInInBackground(username, password, new LogInCallback() {
                              @Override
                              public void done(ParseUser parseUser, ParseException e) {

                                  if(e==null)
                                  {
                                      parseUser.getParseObject("data").fetchIfNeededInBackground(new GetCallback<ParseObject>() {
                                          @Override
                                          public void done(ParseObject parseObject, ParseException e) {

                                                  if(e==null)
                                                  {
                                                      parseObject.pinInBackground();
                                                          dialog.dismiss();


                                                      context.deleteFile(Const.PROFILE_PICTURE_NAME);
                                                      manager.longToast(R.string.registration_message);
                                                      finish();
                                                      manager.deletePreferences();
                                                      manager.getDatabaseManager().deleteDatabase();
                                                      manager.setSHA(Crypt.getSHA256(password));
                                                      manager.createLoginSession(username);
                                                      startActivity(new Intent(StaffRegistration.this, EmailVerificationHook.class));
                                                  }
                                                  else{
                                                      new DialogWizard(StaffRegistration.this).showErrorDialog("",e.getMessage());
                                                  }

                                          }
                                      });

                                  }
                                  else new DialogWizard(StaffRegistration.this).showErrorDialog("",e.getMessage());
                              }
                          });

                    }
                    else{
                        dialog.dismiss();
                        new DialogWizard(StaffRegistration.this).showErrorDialog("",e.getMessage());
                    }
                }
            });

        }


    }

    public void setUpYearPicker(){
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Dialog.Builder builder;
        builder = new DatePickerDialog.Builder(){
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                DatePickerDialog dialog = (DatePickerDialog)fragment.getDialog();
                dialog.getMonth();
                dialog.getDay();

                dob = dialog.getFormattedDate(sdf);
                birthday=dialog.getFormattedDate(new SimpleDateFormat("MM-dd", Locale.US));
                btDob.setText(dob);

                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {

                super.onNegativeActionClicked(fragment);
            }
        }.dateRange(1,1,1930,31,12,2002);

        builder.positiveAction("OK")
                .negativeAction("CANCEL");
        DialogFragment fragment= DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(),null);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                startActivity(new Intent(this,LoginHook.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(this, LoginHook.class));
    }
}
