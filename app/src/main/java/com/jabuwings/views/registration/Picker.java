package com.jabuwings.views.registration;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.main.Login;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Picker extends JabuWingsActivity implements AdapterView.OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_picker);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        setUpToolbar(null,R.string.reg_type);
        String[] pick ={
                "Student", "Staff", "Alumni", "Priviledged"
        };
        List<String> a = new ArrayList<>(Arrays.asList(pick));

        ArrayAdapter pickerAdapter;
        pickerAdapter= new ArrayAdapter<>(this, R.layout.picker_item, R.id.tv_picker,a);
        ListView listView = (ListView)findViewById(R.id.picker_listView);
        listView.setAdapter(pickerAdapter);
        listView.setOnItemClickListener(this);
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        switch (position)
        {
            case 0:
                finish();
                startActivity(new Intent(this,StudentRegistration.class));
                break;
            case 1:
                finish();
                startActivity(new Intent(this,StaffRegistration.class));
                break;
            case 2:
                finish();
                startActivity(new Intent(this,AlumniAndPriviledgedRegistration.class));
                break;
            case 3:
                finish();
                startActivity(new Intent(this,AlumniAndPriviledgedRegistration.class).putExtra(Const.TYPE,1));
                break;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       // getMenuInflater().inflate(R.menu.menu_with_login_option,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                startActivity(new Intent(this, Login.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(this,Login.class));
    }
}
