package com.jabuwings.views.registration;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jabuwings.R;
import com.jabuwings.encryption.Crypt;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.Const;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.main.Domicile;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.main.LoginHook;
import com.jabuwings.widgets.PasswordView;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.LogInCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class StudentRegistrationJabuPortal extends JabuWingsActivity {

    DialogFragment fragment;
    @InjectView(R.id.username)
    EditText etUsername;
    @InjectView(R.id.email)
    EditText etEmail;
    @InjectView(R.id.name)
    TextView tvName;
    @InjectView(R.id.password)
    PasswordView etPassword;
    @InjectView(R.id.password2)
    PasswordView etPassword2;

    @InjectView(R.id.level)
    EditText etLevel;
    @InjectView(R.id.btnRegister)
    Button register;

    JsonObject jsonObject;
    String portalPassword, matric;
    AnimationDrawable anim;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_registration_jabu_portal);
        ButterKnife.inject(this);


        Dialog.Builder builder;
        builder = new SimpleDialog.Builder(R.style.SimpleDialog) {


            @Override
            protected void onBuildDone(Dialog dialog) {
                dialog.layoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                final EditText etMatric = (EditText) dialog.findViewById(R.id.matric);
                final TextView tvManual = (TextView) dialog.findViewById(R.id.tvManual);
                final EditText etPassword = (EditText) dialog.findViewById(R.id.password);
                Button btLogin = (Button) dialog.findViewById(R.id.btlogin);
                Button btCancel = (Button) dialog.findViewById(R.id.btCancel);

                btLogin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        processLogin(etMatric,etPassword);
                    }
                });

                etPassword.setOnEditorActionListener(new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            processLogin(etMatric,etPassword);
                            return true;
                        }
                        return false;
                    }
                });

                etPassword.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if(event.getAction()==KeyEvent.ACTION_DOWN)
                        {
                            switch (keyCode) {
                                case KeyEvent.KEYCODE_ENTER:
                                    processLogin(etMatric,etPassword);
                                    break;
                            }
                        }
                        return false;
                    }
                });

                btCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(StudentRegistrationJabuPortal.this,LoginHook.class));
                        finish();
                    }
                });

                tvManual.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(StudentRegistrationJabuPortal.this, StudentRegistration.class));
                        finish();
                    }
                });


            }

        };

        builder.title("First Things First")
                .contentView(R.layout.jabu_portal_login);
        fragment = DialogFragment.newInstance(builder);
        fragment.setCancelable(false);
        fragment.show(getSupportFragmentManager(), null);

        ScrollView container = (ScrollView) findViewById(R.id.container);

        anim = (AnimationDrawable) container.getBackground();
        anim.setEnterFadeDuration(3000);
        anim.setExitFadeDuration(1000);

    }

    private void processLogin(EditText etMatric, EditText etPassword)
    {
        matric = etMatric.getText().toString();
        portalPassword = etPassword.getText().toString();

        if(TextUtils.isEmpty(matric)||TextUtils.isEmpty(portalPassword))
        {
            manager.shortToast("Please fill in all details");
            return;
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put(Const.MATRIC_NO, matric);
        params.put(Const.PASSWORD, portalPassword);

        final SweetAlertDialog sweetAlertDialog = new DialogWizard(StudentRegistrationJabuPortal.this).showProgress(getString(R.string.please_wait), "Connecting to JABU portal", false);

        ParseCloud.callFunctionInBackground("getStudentDetailsFromJabuPortal", params, new FunctionCallback<String>() {
            @Override
            public void done(String string, ParseException e) {
                sweetAlertDialog.dismiss();
                if (e == null) {
                    fragment.dismiss();
                    JsonParser parser = new JsonParser();
                    jsonObject = parser.parse(string).getAsJsonObject();

                    tvName.setText(jsonObject.get(Const.NAME).getAsString());

                } else {
                    ErrorHandler.handleError(context, e);
                }

            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (anim != null && !anim.isRunning())
            anim.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (anim != null && anim.isRunning())
            anim.stop();
    }

    @OnClick(R.id.btnRegister)
    public void completeRegistration() {
        String name = jsonObject.get(Const.NAME).getAsString();
        String phone = jsonObject.get(Const.PHONE_NO).getAsString();
        String studentsEmail = jsonObject.get(Const.EMAIL).getAsString();
        String department = jsonObject.get(Const.DEPARTMENT).getAsString();
        String sex = jsonObject.get(Const.SEX).getAsString();
        String state = jsonObject.get(Const.STATE).getAsString();
        String dob = jsonObject.get("dob").getAsString();

        final String username = etUsername.getText().toString();
        String email = etEmail.getText().toString();
        final String password = etPassword.getText().toString();
        String confirmPassword = etPassword.getText().toString();
        String level = etLevel.getText().toString();

        boolean cancel = false;
        if (TextUtils.isEmpty(username)) {
            etUsername.setError(getString(R.string.error_field_required));
            cancel = true;
        } else if (username.length() > 15) {

            etUsername.setError(getString(R.string.error_invalid_username_long));
            cancel = true;
        } else if (username.length() <= 2) {

            etUsername.setError(getString(R.string.error_invalid_username));
            cancel = true;
        } else if (TextUtils.isDigitsOnly(String.valueOf(username.charAt(0)))) {
            etUsername.setError(getString(R.string.error_username_char));
            cancel = true;
        } else if (username.charAt(0) == 'h' && username.charAt(1) == 'u' && username.charAt(2) == 'b') {
            etUsername.setError(getString(R.string.error_username));
            cancel = true;
        } else if (!username.matches("[a-zA-Z0-9_]*")) {

            etUsername.setError(getString(R.string.error_invalid_character));
            cancel = true;

        } else if (manager.getForbiddenUsernames().contains(username) || username.contains("jabu")) {
            etUsername.setError(getString(R.string.error_username));
            cancel = true;
        }

        if(level.length()<3)
        {
            etLevel.setError(getString(R.string.error_level));
            cancel=true;
        }
        else if(!level.equals("100") && !level.equals("200") && !level.equals("300") && !level.equals("400") && !level.equals("500"))
        {
            etLevel.setError(getString(R.string.error_level));
            cancel=true;
        }

        // Check for a valid confirm password.
        {
            if (TextUtils.isEmpty(confirmPassword)) {
                etPassword2.setError(getString(R.string.error_field_required));
                cancel = true;
            } else if (!confirmPassword.equals(password)) {
                etPassword.setError(getString(R.string.error_password_mismatch));
                cancel = true;
            }
        }
        // Check for a valid password.
        {
            if (TextUtils.isEmpty(password)) {
                etPassword.setError(getString(R.string.error_field_required));
                cancel = true;
            } else if (password.length() < 4) {
                etPassword.setError(getString(R.string.error_invalid_password));
                cancel = true;
            }

        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            etEmail.setError(getString(R.string.error_field_required));
            cancel = true;
        } else if (!email.contains("@") || !email.contains(".")|| (email.length()<7)) {
            etEmail.setError(getString(R.string.error_invalid_email));
            cancel = true;
        }

        if(!cancel)
        {
            HashMap<String, Object> params = new HashMap<>();
            params.put(Const.VERSION, getString(R.string.version_number));
            params.put(Const.USERNAME,username);
            params.put(Const.EMAIL,email);
            params.put("sEmail",studentsEmail);
            params.put(Const.PASSWORD,password);
            params.put(Const.PORTAL_PASSWORD,portalPassword);
            params.put(Const.NAME,name);
            params.put(Const.STATE,state);
            params.put(Const.BIRTHDAY, dob.substring(5));
            params.put(Const.DATE_OF_BIRTH,dob);
            params.put(Const.PHONE_NO,phone);
            params.put(Const.MATRIC_NO,matric);
            params.put(Const.SEX,sex);
            params.put(Const.DEPARTMENT,department);
            params.put(Const.TYPE,Const.REGISTRATION_TYPE.STUDENT.ordinal());
            params.put(Const.LEVEL,level);

            final SweetAlertDialog loadingDialog = new DialogWizard(StudentRegistrationJabuPortal.this).showProgress(getString(R.string.please_wait),getString(R.string.logging_in),false);

            ParseCloud.callFunctionInBackground("completeStudentRegistration", params, new FunctionCallback<String>() {
                @Override
                public void done(String s, ParseException e) {
                    if(e==null)
                    {
                        Bundle bundle = new Bundle();
                        bundle.putString(Const.USERNAME, username);
                        bundle.putInt(Const.TYPE, 0);
                        bundle.putString(Const.MATRIC_NO, matric);
                        manager.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);

                        ParseUser.logInInBackground(username, password, new LogInCallback() {
                            @Override
                            public void done(ParseUser parseUser, ParseException e) {
                                loadingDialog.dismiss();
                                if(e==null)
                                {

                                    context.deleteFile(Const.PROFILE_PICTURE_NAME);
                                    manager.successToast( getString(R.string.registration_message));
                                    manager.getDatabaseManager().deleteDatabase();
                                    manager.deletePreferences();
                                    manager.setSHA(Crypt.getSHA256(password));
                                    manager.createLoginSession(username);

                                    manager.successToast("Welcome aboard");
                                    manager.createLoginSession(manager.getUsername());
                                    manager.setUpPush();
                                    manager.enableBootService();
                                    startActivity(new Intent(context,Domicile.class).putExtra(Const.TUTORIAL,true));
                                    finish();
                                }

                                else{
                                    new DialogWizard(StudentRegistrationJabuPortal.this).showSimpleDialog("","You have been registered but an unexpected error error occurred:\n"+e.getMessage()+"\n Please try to log in with your credentials");                                        }
                            }
                        });;
                    }
                    else {
                        loadingDialog.dismiss();
                        new DialogWizard(StudentRegistrationJabuPortal.this).showErrorDialog("Oops",e.getMessage());
                    }
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                startActivity(new Intent(this,LoginHook.class));
                finish();
                break;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(this,LoginHook.class));
        finish();

    }
}
