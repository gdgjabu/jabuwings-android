package com.jabuwings.views.registration;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Falade James on 9/22/2015 All Rights Reserved.
 */
public class CustomViewPager extends ViewPager {

    public CustomViewPager(Context context)
    {
        super(context);

    }

    public CustomViewPager(Context context, AttributeSet attributeSet)
    {
        super(context,attributeSet);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return false;
    }

}
