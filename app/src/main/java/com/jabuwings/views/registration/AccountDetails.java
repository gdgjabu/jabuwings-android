package com.jabuwings.views.registration;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.encryption.Crypt;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.hooks.EmailVerificationHook;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.rey.material.widget.Button;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountDetails extends Fragment implements Step {
EditText etUsername;
EditText etEmail;   
EditText etPassword;    
EditText etConfirmPassword;
View rootView;   
String mUsername;
String mEmail;
String mPassword;
String mConfirmPassword;
Activity context;
/*Button btPrev;
Button btFinish;*/
AcademicDetails academicDetails;
Manager manager;
static String fnName,fnStateOfOrigin,fnCountry, fnPhoneNo, fnDateOfBirth, fnBirthday, fnMatricNo, fnDepartment,fnLevel,fnModeOfEntry,fnSex;
SweetAlertDialog loadingDialog;
    
    public AccountDetails() {
        // Required empty public constructor
    }


    @Override
    public VerificationError verifyStep() {
        if (checkValues() ) {
            return new VerificationError(getString(R.string.error_fields_must_be_filled));

        }
        else{
            showDialog();
            return null;
        }
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context= getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.account_details, container, false);

        academicDetails= new AcademicDetails();
        manager = new Manager(getActivity());

        etUsername= (EditText)rootView.findViewById(R.id.acd_username);
        etEmail= (EditText)rootView.findViewById(R.id.acd_email);
        etPassword= (EditText)rootView.findViewById(R.id.acd_password);
        etConfirmPassword = (EditText)rootView.findViewById(R.id.acd_password_again);
       /* btPrev= (Button)rootView.findViewById(R.id.acd_reg_prev); btPrev.setOnClickListener(this);
        btFinish=(Button)rootView.findViewById(R.id.acd_reg_finish); btFinish.setOnClickListener(this);*/
        fnName=PersonalDetails.getFirstName() +" "+ PersonalDetails.getLastName();
        fnStateOfOrigin= PersonalDetails.getState();
        fnCountry=PersonalDetails.getCountry();
        if(fnCountry==null || fnCountry.equals(Const.CHOOSE)){
            fnCountry=Const.NIGERIA;
        }

        if(TextUtils.isEmpty(fnStateOfOrigin) || fnStateOfOrigin.equals(Const.CHOOSE))
        {
            fnStateOfOrigin= PersonalDetails.getCountry();
        }
        fnPhoneNo= PersonalDetails.getPhoneNo();
        fnDateOfBirth = PersonalDetails.getDateOfBirth();
        fnBirthday = PersonalDetails.getBirthday();
        fnSex=PersonalDetails.getSex();
        fnMatricNo=AcademicDetails.getMatricNo();
        fnDepartment=AcademicDetails.getDepartment();
        fnLevel=AcademicDetails.getLevel();
        fnModeOfEntry=AcademicDetails.getModeOfEntry();


        return rootView;
    }

    /*@Override
    public void onClick(View v) {
       CustomViewPager pager = StudentRegistration.mPager;
        switch (v.getId())

        {
            case R.id.acd_reg_prev:
                StudentRegistration.toolbar.setTitle(getString(R.string.academic_details));
                pager.setCurrentItem(pager.getCurrentItem() - 1);
                break;
            case R.id.acd_reg_finish:

                if (checkValues() ) {
                    Toast.makeText(getActivity(), getString(R.string.error_fields_must_be_filled), Toast.LENGTH_SHORT).show();
                }
               else{
                   showDialog();
                }
                break;

        }


    }
*/
    public boolean checkValues()
    {
        mUsername= etUsername.getText().toString().toLowerCase().trim();
        mEmail= etEmail.getText().toString().toLowerCase().trim();
        mPassword= etPassword.getText().toString();
        mConfirmPassword=etConfirmPassword.getText().toString();


        View focusView = null;
        boolean cancel= false;

        //check for a valid username
        {
            if (TextUtils.isEmpty(mUsername)) {
                etUsername.setError(getString(R.string.error_field_required));
                focusView = etUsername;
                cancel = true;
            }
            else if (mUsername.length() > 15) {

                etUsername.setError(getString(R.string.error_invalid_username_long));
                focusView = etUsername;
                cancel = true;
            }
            else if (mUsername.length() <= 2) {

                etUsername.setError(getString(R.string.error_invalid_username));
                focusView = etUsername;
                cancel = true;
            }
            else if(TextUtils.isDigitsOnly(String.valueOf(mUsername.charAt(0))))
            {
                etUsername.setError(getString(R.string.error_username_char));
                focusView=etUsername;
                cancel=true;
            }

            else if(mUsername.charAt(0)=='h'&&mUsername.charAt(1)=='u'&& mUsername.charAt(2)=='b')
            {
                etUsername.setError(getString(R.string.error_username));
                focusView = etUsername;
                cancel = true;
            }
            else if (!mUsername.matches("[a-zA-Z0-9_]*")) {

                etUsername.setError(getString(R.string.error_invalid_character));
                focusView = etUsername;
                cancel = true;

            }

            else if(manager.getForbiddenUsernames().contains(mUsername)||mUsername.contains("jabu"))
            {
                etUsername.setError(getString(R.string.error_username));
                focusView = etUsername;
                cancel = true;
            }

        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(mEmail)) {
            etEmail.setError(getString(R.string.error_field_required));
            focusView = etEmail;
            cancel = true;
        } else if (!mEmail.contains("@") || !mEmail.contains(".")|| (mEmail.length()<7)) {
            etEmail.setError(getString(R.string.error_invalid_email));
            focusView = etEmail;
            cancel = true;
        }
/*
        else if(!mEmail.contains("@students.jabu.edu.ng"))
        {
            etEmail.setError(getString(R.string.error_invalid_students_email));
            focusView = etEmail;
            cancel = true;
        }

*/

        // Check for a valid confirm password.
        {
            if (TextUtils.isEmpty(mConfirmPassword)) {
                etConfirmPassword.setError(getString(R.string.error_field_required));
                focusView = etConfirmPassword;
                cancel = true;
            } else if (mPassword != null && !mConfirmPassword.equals(mPassword)) {
                etPassword.setError(getString(R.string.error_password_mismatch));
                focusView = etPassword;
                cancel = true;
            }
        }
        // Check for a valid password.
        { if (TextUtils.isEmpty(mPassword)) {
            etPassword.setError(getString(R.string.error_field_required));
            focusView = etPassword;
            cancel = true;
        } else if (mPassword.length() < 4) {
            etPassword.setError(getString(R.string.error_invalid_password));
            focusView = etPassword;
            cancel = true;
        }

        }
        
        if (cancel)
        {
            focusView.requestFocus();
            return true;
        }
        else{
            return false;
        }

    }


  public void showDialog()
  {
      String sex= fnSex.equals("M")? "Male" : "Female";
      new MaterialDialog.Builder(getActivity())
              .title("Please Confirm")
              .content("Name: "+fnName+"\n\nEmail: "+ mEmail+"\n\nUsername: "
                      +mUsername+
                      "\n\nSex: "
                      +sex+
                      "\n\nMatric No: "+fnMatricNo+ "\n\nDepartment: "+fnDepartment+ "\n\nLevel: "+fnLevel+ "\n\nState: "+fnStateOfOrigin)
              .positiveText("Confirm")
              .onPositive(new MaterialDialog.SingleButtonCallback() {
                  @Override
                  public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                      if (isEmpty(fnName) || isEmpty(mUsername) || isEmpty(fnStateOfOrigin) || fnStateOfOrigin.equals("Choose") || isEmpty(fnDateOfBirth) || isEmpty(fnPhoneNo)
                              || isEmpty(fnMatricNo) || isEmpty(fnDepartment) || isEmpty(fnModeOfEntry) || isEmpty(fnLevel) || fnLevel.equals(AcademicDetails.arLevel[0])) {
                          new MaterialDialog.Builder(context)
                                  .content("We are sorry, you cannot continue with the registration, a field appears empty or incorrect, please go back and check" +
                                          ". It is possible that you have filled everything correctly, in this case, press the prev button" +
                                          ", then press next again")
                                  .positiveText("Edit")
                                  .show();
                      } else {
                          loadingDialog = new DialogWizard(getActivity()).showProgress(getString(R.string.please_wait),getString(R.string.logging_in),false);
                          signUp(fnName, mUsername,fnSex, fnStateOfOrigin,fnCountry, fnBirthday, fnPhoneNo, fnMatricNo, fnDepartment, fnDateOfBirth, fnModeOfEntry, fnLevel
                                  , mEmail, mPassword);



                      }

                  }
              })
              .negativeText("Edit")
              .show();
  }

    public boolean isEmpty(String value)
    {
        return TextUtils.isEmpty(value);
    }



    private void signUp(final String Name,final String Username,final String Sex, final String StateOfOrigin,final String Country,final String Birthday, final String PhoneNo,
                       final String MatricNo,final String Department,final String DateOfBirth,final String ModeOfEntry, final String Level,final String Email,final String Password) {



        HashMap<String, Object> params = new HashMap<>();
        params.put(Const.VERSION, getString(R.string.version_number));
        params.put(Const.USERNAME,Username);
        params.put(Const.EMAIL,Email);
        params.put(Const.PASSWORD,Password);
        params.put(Const.NAME,Name);
        params.put(Const.STATE,StateOfOrigin);
        params.put(Const.COUNTRY,Country);
        params.put(Const.BIRTHDAY, Birthday);
        params.put(Const.DATE_OF_BIRTH,DateOfBirth);
        params.put(Const.PHONE_NO,PhoneNo);
        params.put(Const.MATRIC_NO,MatricNo);
        params.put(Const.SEX,Sex);
        //params.put(Const.MEDICAL_NO,MedicalNo);
        params.put(Const.DEPARTMENT,Department);
        params.put(Const.TYPE,Const.REGISTRATION_TYPE.STUDENT.ordinal());
        params.put(Const.MODE,ModeOfEntry);
        params.put(Const.LEVEL,Level);

        ParseCloud.callFunctionInBackground(Const.USER_CREATOR, params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {
                if(e==null)
                {
                    ParseUser.getQuery().getInBackground(s, new GetCallback<ParseUser>() {
                        @Override
                        public void done(ParseUser parseUser, ParseException e) {
                            if(e==null)
                            {
                                ParseUser.logInInBackground(Username, Password, new LogInCallback() {
                                    @Override
                                    public void done(ParseUser parseUser, ParseException e) {
                                        if(e==null)
                                        {
                                            ParseQuery.getQuery(Const.USER_DATA).whereEqualTo(Const.USER,parseUser).getFirstInBackground(new GetCallback<ParseObject>() {
                                                @Override
                                                public void done(ParseObject userData, ParseException e) {
                                                    if(e==null)
                                                    {

                                                        userData.pinInBackground();
                                                        context.deleteFile(Const.PROFILE_PICTURE_NAME);
                                                        manager.successToast( getString(R.string.registration_message));
                                                        manager.getDatabaseManager().deleteDatabase();
                                                        manager.deletePreferences();
                                                        manager.setSHA(Crypt.getSHA256(Password));
                                                        manager.createLoginSession(mUsername);
                                                        Intent in = new Intent(getActivity(), Verification.class);
                                                        loadingDialog.dismiss();
                                                        startActivity(in);
                                                        context.finish();
                                                    }

                                                    else{
                                                        loadingDialog.dismiss();
                                                        new DialogWizard(getActivity()).showSimpleDialog("","You have been registered but an unexpected error error occurred:\n"+e.getMessage()+"\n Please try to log in with your credentials");
                                                    }
                                                }
                                            });
                                        }

                                        else{
                                            loadingDialog.dismiss();
                                            new DialogWizard(getActivity()).showSimpleDialog("","You have been registered but an unexpected error error occurred:\n"+e.getMessage()+"\n Please try to log in with your credentials");                                        }
                                    }
                                });
                            }
                            else{
                                loadingDialog.dismiss();
                                new DialogWizard(getActivity()).showSimpleDialog("","You have been registered but an unexpected error error occurred:\n"+e.getMessage()+"\n Please try to log in with your credentials");
                            }
                        }
                    });


                }

                else{
                    loadingDialog.dismiss();
                    new DialogWizard(getActivity()).showErrorDialog("Registration failed",e.getMessage());
                }
            }
        });

      /*  ParseCloud.callFunctionInBackground(Const.VERSION_CHECK, params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {
                if(e==null)
                {
                    if(s.equals(Const.POSITIVE)) {
                        ParseUser user = new ParseUser();

                        user.setUsername(Username);
                        user.setEmail(Email);
                        user.setPassword(Password);
                        user.put(Const.NAME, Name);
                        user.put(Const.STATE, StateOfOrigin);
                        user.put(Const.BIRTHDAY, Birthday);
                        user.put(Const.PHONE_NO, "+234"+PhoneNo);
                        user.put(Const.MATRIC_NO, MatricNo);
                        user.put(Const.SEX,Sex);
                        user.put(Const.MEDICAL_NO,MedicalNo);
                        user.put(Const.SHA,Crypt.getSHA256(Password));
                        user.put(Const.DEPARTMENT, Department);
                        user.put(Const.DATE_OF_BIRTH, DateOfBirth);
                        user.put(Const.STATUS, getString(R.string.default_status));
                        user.put(Const.ACADEMIC_SESSION,getString(R.string.school_session));
                        user.put(Const.CHRIMATA, 200);
                        user.put(Const.ACCOUNT_TYPE, Const.REGISTRATION_TYPE.STUDENT.ordinal());
                        user.put(Const.MODE, ModeOfEntry);
                        user.put(Const.LEVEL, Level);
                        //user.put(Const.DEVICE_NAME, Utils.getDeviceName());
                        user.signUpInBackground(new SignUpCallback() {
                            @Override
                            public void done(ParseException e) {
                                loadingDialog.dismiss();
                                if (e == null) {
                                    loadingDialog.setTitleText(getString(R.string.please_wait)).setContentText("We are securing your account");
                                    final ParseUser newUser= ParseUser.getCurrentUser();
                                    ParseACL dataACL= new ParseACL(newUser);
                                    dataACL.setPublicReadAccess(false);
                                    dataACL.setPublicWriteAccess(false);
                                    final ParseObject data= new ParseObject(Const.USER_DATA);
                                    data.put(Const.USER,newUser);
                                    data.put(Const.SHA,Crypt.getSHA256(Password));
                                    data.setACL(dataACL);
                                    data.saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            newUser.put(Const.DATA,data);
                                            newUser.saveInBackground(new SaveCallback() {
                                                @Override
                                                public void done(ParseException e) {
                                                    try{
                                                        loadingDialog.dismiss();}
                                                    catch (Exception e1){}
                                                    if(e==null)
                                                    {
                                                        data.pinInBackground();
                                                        newUser.getParseObject(Const.DATA2).fetchIfNeededInBackground(new GetCallback<ParseObject>() {
                                                            @Override
                                                            public void done(ParseObject parseObject, ParseException e) {
                                                                    if(e==null)
                                                                    {
                                                                        parseObject.pinInBackground();
                                                                        context.deleteFile(Const.PROFILE_PICTURE_NAME);
                                                                        Toast.makeText(getActivity(), getString(R.string.registration_message), Toast.LENGTH_LONG).show();
                                                                        manager.getDatabaseManager().deleteDatabase();
                                                                        manager.deletePreferences();
                                                                        manager.createLoginSession(mUsername);
                                                                        Intent in = new Intent(getActivity(), EmailVerificationHook.class);
                                                                        startActivity(in);
                                                                        context.finish();
                                                                    }
                                                                else{
                                                                        new DialogWizard(getActivity()).showSimpleDialog("","You have been registered but an unexpected error error occurred:\n"+e.getMessage()+"\n Please try to log in with your credentials");
                                                                    }
                                                            }
                                                        });



                                                    }

                                                    else{
                                                        new DialogWizard(getActivity()).showSimpleDialog("","You have been registered but an unexpected error error occurred:\n"+e.getMessage()+"\n Please try to log in with your credentials");

                                                    }
                                                }
                                            });

                                        }
                                    });








                                } else {
                                    e.printStackTrace();
                                    new DialogWizard(getActivity()).showSimpleDialog("",e.getMessage());
                                }


                            }
                        });
                    }
                    else{
                        new DialogWizard(getActivity()).showSimpleDialog("",s);
                    }

                }
                else
                {
                    loadingDialog.dismiss();
                    e.printStackTrace();
                    Toast.makeText(context, getString(R.string.error_network), Toast.LENGTH_SHORT).show();
                }

            }
        });*/


    }





}
