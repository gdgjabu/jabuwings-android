package com.jabuwings.views.registration;

public interface OnOTPReceived{
        void onOTPReceived(String otp);
    }