package com.jabuwings.views.registration;

import android.Manifest;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.intelligence.SmsWitch;
import com.jabuwings.management.Const;
import com.jabuwings.views.hooks.EmailVerificationHook;
import com.jabuwings.views.main.Domicile;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.rey.material.widget.ProgressView;

import java.util.HashMap;
import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class Verification extends JabuWingsActivity implements OnOTPReceived {
    public static boolean verifying=false;
    AnimationDrawable anim;
    @InjectView(R.id.container)
    RelativeLayout container;
    @InjectView(R.id.btProceed)
    Button btProceed;
    @InjectView(R.id.btChangePhone)
    Button btChangePhone;

    @InjectView(R.id.etVerify)
    EditText etVerify;
    @InjectView(R.id.tvVerify)
    TextView tvVerify;
    @InjectView(R.id.tvVerifyEmail)
    TextView tvVerifyEmail;

    @InjectView(R.id.pvVerify)
    ProgressView progressView;


    String otp;
    String m="Sit Back, relax, and stay cool while we verify your phone number\n\n";


    @OnClick(R.id.btChangePhone)
    public void changePhone()
    {
        new MaterialDialog.Builder(Verification.this)
                .title("Change Phone Number")
                .content("Enter your correct phone number. Please note that you must have access to this number")
                .inputType(InputType.TYPE_CLASS_PHONE)
                .inputRange(11,16)
                .input("Phone Number", null, false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {

                        final String number=input.toString();
                        final SweetAlertDialog progress=dialogWizard.showSimpleProgress(getString(R.string.please_wait),"Changing your phone number");

                        HashMap<String,Object> params=manager.getDefaultCloudParams();
                        params.put("number",number);
                        params.put(Const.TYPE,4);
                        ParseCloud.callFunctionInBackground("userUpdater", params, new FunctionCallback<String>() {
                            @Override
                            public void done(String s, ParseException e) {
                                progress.dismiss();
                                if(e==null)
                                {
                                    manager.shortToast(s);
                                    tvVerify.setText(m+number);

                                }
                                else{
                                    ErrorHandler.handleError(context,e);
                                }

                            }
                        });
                    }
                }).show();

    }
    public void cannotVerify(View v)
    {
        startActivity(new Intent(this, EmailVerificationHook.class));
        finish();
    }

    public String getOTP()
    {
        return etVerify.getText().toString();
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (anim != null && !anim.isRunning())
            anim.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (anim != null && anim.isRunning())
            anim.stop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            findViewById(android.R.id.content).setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        }
        setContentView(R.layout.activity_verification);
        ButterKnife.inject(this);


        boolean request= getIntent().getBooleanExtra("request",false);


        if(request)
        {
            final SweetAlertDialog dialog=dialogWizard.showSimpleProgress(getString(R.string.please_wait),"Requesting for verification via SMS");
            ParseCloud.callFunctionInBackground("requestVerification", manager.getDefaultCloudParams(), new FunctionCallback<String>() {
                @Override
                public void done(String s, ParseException e) {
                    dialog.dismiss();
                    if(e==null)
                    {

                    }
                    else{
                        ErrorHandler.handleError(context,e);
                    }

                }
            });
        }

        String[] permissions={Manifest.permission.READ_SMS,Manifest.permission.RECEIVE_SMS};

        if(!hasPermissions(this, permissions)){
            ActivityCompat.requestPermissions(this, permissions, 1);
        }

        SmsWitch.received=this;
        verifying=true;

        tvVerify.setText(m+manager.getUser().getString("phone"));


        anim = (AnimationDrawable) container.getBackground();
        anim.setEnterFadeDuration(3000);
        anim.setExitFadeDuration(1000);
    }

    @Override
    public void onOTPReceived(String otp) {
        if(etVerify!=null &&otp.length()==6)
        {
            this.otp=otp;
            etVerify.setText(otp);
        }
    }


    @OnClick(R.id.btProceed)
    public void onProceedClick()
    {
        otp=etVerify.getText().toString();
        if(otp.length()==6)
        {
            btProceed.setVisibility(View.GONE);
            btChangePhone.setVisibility(View.GONE);
            tvVerifyEmail.setVisibility(View.GONE);
            progressView.setVisibility(View.VISIBLE);

            HashMap<String,Object> params=manager.getDefaultCloudParams();
            params.put("otp",otp);
            ParseCloud.callFunctionInBackground("verifyOTP", params, new FunctionCallback<String>() {
                @Override
                public void done(String s, ParseException e) {
                    if(e==null)
                    {
                        manager.successToast("Welcome aboard");
                        manager.createLoginSession(manager.getUsername());
                        manager.setUpPush();
                        manager.enableBootService();
                        startActivity(new Intent(Verification.this,Domicile.class).putExtra(Const.TUTORIAL,true));
                        finish();
                    }
                    else{
                        btProceed.setVisibility(View.VISIBLE);
                        btChangePhone.setVisibility(View.VISIBLE);
                        tvVerifyEmail.setVisibility(View.VISIBLE);
                        progressView.setVisibility(View.GONE);
                        ErrorHandler.handleError(context,e);
                    }
                }
            });
        }
        else{
            manager.errorToast("Invalid digits");
        }

    }

    @Override
    protected void onDestroy() {
         super.onDestroy();verifying=false;
    }


}
