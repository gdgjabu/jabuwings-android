package com.jabuwings.views.registration;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;

import com.jabuwings.R;
import com.jabuwings.management.Manager;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.main.LoginHook;
import com.jabuwings.widgets.CirclePageIndicator;
import com.stepstone.stepper.StepperLayout;


public class StudentRegistration extends JabuWingsActivity {



CirclePageIndicator mIndicator;
private static final int NUM_PAGES = 4;
//static protected Toolbar toolbar;
ScreenSlidePagerAdapter mPagerAdapter;
static CustomViewPager mPager;

StepperLayout mStepperLayout;

    Manager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        setContentView(R.layout.activity_student_registration);

        String[] permissions={Manifest.permission.READ_SMS,Manifest.permission.RECEIVE_SMS};

        if(!hasPermissions(this, permissions)){
            ActivityCompat.requestPermissions(this, permissions, 1);
        }
        manager=new Manager(this);
        setUpToolbar(null,"Student Registration");
        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        mStepperLayout.setAdapter(new StepAdapter(getSupportFragmentManager(),this));
//        toolbar= (Toolbar)findViewById(R.id.toolbar);
//        setUpToolbar(null,"Student Registration");
        // Instantiate a ViewPager and a PagerAdapter.
//        mPager = (CustomViewPager) findViewById(R.id.pager);
//        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
//        mPager.setAdapter(mPagerAdapter);
      /*  mPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When changing pages, reset the action bar actions since they are dependent
                // on which page is currently active. An alternative approach is to have each
                // fragment expose actions itself (rather than the activity exposing actions),
                // but for simplicity, the activity provides the actions in this sample.
                invalidateOptionsMenu();
            }
        });*/

        /*// ViewPager Indicator
        mIndicator = (CirclePageIndicator) findViewById(R.id.student_reg_indicator);
        mIndicator.setCentered(true);
        mIndicator.setFillColor(getResources().getColor(R.color.default_title_indicator_footer_color));
        mIndicator.setPageColor(getResources().getColor(R.color.default_page_indicator));
        mIndicator.setViewPager(mPager);*/
        showTerms(null);

 

    }

    @Override
    public void onBackPressed() {
        final int currentStepPosition = mStepperLayout.getCurrentStepPosition();
        if (currentStepPosition > 0) {
            mStepperLayout.setCurrentStepPosition(currentStepPosition - 1);
        } else {
            startActivity(new Intent(StudentRegistration.this,LoginHook.class));
            finish();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                startActivity(new Intent(StudentRegistration.this,LoginHook.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showTerms(View v)
    {

        manager.showTerms();
    }
    public Fragment displayView(int position) {
        Fragment fragment = null;


        switch (position) {
            case 0:

                fragment = new StudentDisclaimer();
                break;
            case 1:

                fragment= new PersonalDetails();
                break;
            case 2:

                fragment= new AcademicDetails();
                break;
            case 3:

                fragment= new AccountDetails();
                break;

            default:
                break;

        }




        return fragment;



    }



    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return displayView(position);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
    public ActionBar getToolbar()
    {
        return getSupportActionBar();
    }

   /* @Override
    public void onBackPressed() {
        if(mPager.getCurrentItem()==0)
        {
            startActivity(new Intent(StudentRegistration.this,LoginHook.class));
            finish();
        }
        try{
        mPager.setCurrentItem(mPager.getCurrentItem()-1);}
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }*/


}
