package com.jabuwings.views.registration;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.utilities.Time;
import com.rey.material.app.DatePickerDialog;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.widget.Spinner;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalDetails extends Fragment implements Step{
EditText etFirstName;
EditText etLastName;
//EditText etMedNo;
Spinner spState;
Spinner spCountry;
Spinner spSex;
EditText etPhoneNo;
Button btDob;
//com.rey.material.widget.Button btPrev;
//com.rey.material.widget.Button btNext;
View rootView;
static String Dob;
static String _Dob;
static String mFirstName;
static String mLastName;
static String mPhoneNo;
static String mState;
static String mCountry;

static String mSex;

String[] arCountry;
String[] arState;
ArrayAdapter<String> adpCountry;
ArrayAdapter<String> adpState;
ArrayAdapter<String> adpSex;



    boolean cancel= false;

    public PersonalDetails() {
        // Required empty public constructor
    }


    @Override
    public VerificationError verifyStep() {

        try{
            if(checkValues())
                return new VerificationError(getString(R.string.error_fields_must_be_filled));

            else{
                return null;
            }
        }
        catch (Exception e)
        {

            e.printStackTrace();
            return new VerificationError(getString(R.string.error_fields_must_be_filled));
        }

    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.personal_details, container, false);

        etFirstName= (EditText)rootView.findViewById(R.id.pd_first_name);
        etLastName= (EditText)rootView.findViewById(R.id.pd_last_name);
        //etMedNo=(EditText)rootView.findViewById(R.id.pd_med_no);
        spState=(Spinner)rootView.findViewById(R.id.pd_states);
        spCountry=(Spinner)rootView.findViewById(R.id.pd_countries);
        spSex=(Spinner)rootView.findViewById(R.id.pd_sex);
        etPhoneNo=(EditText)rootView.findViewById(R.id.pd_phone_no);
        btDob =(Button)rootView.findViewById(R.id.pd_dob);
        btDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btDob.setError(null);
                setUpYearPicker();
            }
        });
//        btPrev= (com.rey.material.widget.Button)rootView.findViewById(R.id.pd_reg_prev); btPrev.setOnClickListener(this);
//        btNext= (com.rey.material.widget.Button)rootView.findViewById(R.id.pd_reg_next); btNext.setOnClickListener(this);

        arState = getResources().getStringArray(R.array.nigerian_states);
        arCountry = getResources().getStringArray(R.array.countries_of_the_world);
        adpState = new ArrayAdapter<>(getActivity(), R.layout.row_spn,arState); adpState.setDropDownViewResource(R.layout.row_spn_dropdown);
        adpCountry= new ArrayAdapter<>(getActivity(), R.layout.row_spn,arCountry);adpCountry.setDropDownViewResource(R.layout.row_spn_dropdown);
        String[] sexes={Const.CHOOSE,"Male","Female"};
        adpSex= new ArrayAdapter<>(getActivity(), R.layout.row_spn,sexes);adpSex.setDropDownViewResource(R.layout.row_spn_dropdown);
        spSex.setAdapter(adpSex);
        spState.setAdapter(adpState);
        spCountry.setAdapter(adpCountry);
        spCountry.setSelection(154);

        spState.setOnItemClickListener(new Spinner.OnItemClickListener() {
            @Override
            public boolean onItemClick(Spinner parent, View view, int position, long id) {

                if (position==arState.length-1)
                {
                //spCountry.setVisibility(View.VISIBLE);
                spState.setVisibility(View.GONE);
                }
                else{
                    spState.setSelection(position);

                }
                return false;
            }
        });



     return rootView;
    }

  /*  @Override
    public void onClick(View v) {
        CustomViewPager pager = StudentRegistration.mPager;
        switch (v.getId())
        {

            case R.id.pd_reg_prev:
                StudentRegistration.toolbar.setTitle(getString(R.string.title_activity_student_registration));
                pager.setCurrentItem(pager.getCurrentItem()-1);
                break;
            case R.id.pd_reg_next:
                try{
                    if(checkValues())
                        Toast.makeText(getActivity(),getString(R.string.error_fields_must_be_filled),Toast.LENGTH_SHORT).show();

                    else{
                        StudentRegistration.toolbar.setTitle(getString(R.string.academic_details));
                        pager.setCurrentItem(pager.getCurrentItem()+1);}
                }
                catch (Exception e)
                {

                    e.printStackTrace();
                    Toast.makeText(getActivity(),getString(R.string.error_fields_must_be_filled),Toast.LENGTH_SHORT).show();
                }


                break;
            case R.id.pd_dob:
                btDob.setError(null);
                setUpYearPicker();
                break;
            default:

        }
    }*/

    public boolean checkValues(){
        mFirstName = etFirstName.getText().toString().trim().toLowerCase();
        mFirstName=mFirstName.substring(0,1).toUpperCase()+mFirstName.substring(1);
        mLastName= etLastName.getText().toString().trim().toLowerCase();
        if(mLastName.contains(" "))
        {
            String lastNames[]= mLastName.split(" ");
            String combinedName="";
            for(String name: lastNames)
            {
                name=name.substring(0,1).toUpperCase()+name.substring(1);
                combinedName+=name+" ";
            }
            mLastName=combinedName.trim();



        }

        else{

         mLastName=   mLastName.substring(0,1).toUpperCase()+mLastName.substring(1);
        }
        mPhoneNo= etPhoneNo.getText().toString();
        mState= spState.getSelectedItem().toString();
        mCountry = spCountry.getSelectedItem().toString();


        switch (spSex.getSelectedItemPosition())
        {
            case 0:
                mSex=Const.CHOOSE;
                break;
            case 1:
                mSex="M";
                break;
            case 2:
                mSex="F";
                break;
        }






       clearErrors();


        View focusView = null;
        //check for a valid first name
        {
            if (TextUtils.isEmpty(mFirstName)) {
                etFirstName.setError(getString(R.string.error_field_required));
                focusView = etFirstName;
                cancel = true;
            } else if (mFirstName.length() <= 2) {

                etFirstName.setError(getString(R.string.error_invalid_name));
                focusView = etFirstName;
                cancel = true;
            }
            else if (!mFirstName.matches("[a-zA-Z]*")) {

                etFirstName.setError(getString(R.string.error_invalid_character));
                focusView = etFirstName;
                cancel = true;

            }
        }
        //check for a valid last name
        {
            if (TextUtils.isEmpty(mLastName)) {
                etLastName.setError(getString(R.string.error_field_required));
                focusView = etLastName;
                cancel = true;
            } else if (mLastName.length() <= 2) {

                etLastName.setError(getString(R.string.error_invalid_name));
                focusView = etLastName;
                cancel = true;
            }
            else if (!mLastName.matches("[a-zA-Z ]*")) {

                etLastName.setError(getString(R.string.error_invalid_character));
                focusView = etLastName;
                cancel = true;

            }
        }

        /*//check for a valid medical number
        {
            if (TextUtils.isEmpty(mMedNo)) {
                new DialogWizard(getActivity()).showSimpleDialog("Medical Number","You have omitted the JABU medical number. You will not be able to access some of features within the app. If you cannot get the number now, please update it later");
                //etMedNo.setError(getString(R.string.error_field_required));
                //focusView = etMedNo;
                //cancel = true;
            } else if (mMedNo.length() < 4) {

                etMedNo.setError(getString(R.string.error_invalid_med_no));
                focusView = etMedNo;
                cancel = true;
            }
            else if (!mMedNo.matches("[0-9]*")) {

                etMedNo.setError(getString(R.string.error_invalid_character));
                focusView = etMedNo;
                cancel = true;

            }
        }*/

        {
            if (spState.getVisibility()==View.VISIBLE &&   mState.equals(Const.CHOOSE))
            {
                Toast.makeText(getActivity(),getString(R.string.error_invalid_state),Toast.LENGTH_SHORT).show();
                cancel=true;
                focusView= spState;

            }

            {
                if (mCountry.equals(Const.CHOOSE) && spCountry.getVisibility()==View.VISIBLE)
                {
                    Toast.makeText(getActivity(),getString(R.string.error_invalid_country),Toast.LENGTH_SHORT).show();
                    cancel=true;
                    focusView = spCountry;

                }


            }
        }
        {
            if(mSex.equals(Const.CHOOSE))
            {
                Toast.makeText(getActivity(),getString(R.string.error_invalid_sex),Toast.LENGTH_SHORT).show();
                cancel=true;
                focusView = spSex;
            }
        }


                // check for a valid phoneNo
        {
            if (TextUtils.isEmpty(mPhoneNo)) {
                etPhoneNo.setError(getString(R.string.error_field_required));
                focusView = etPhoneNo;
                cancel = true;
            } else if (mPhoneNo.length() < 10|| mPhoneNo.charAt(0)=='0') {

                etPhoneNo.setError(getString(R.string.error_invalid_phone_no));
                focusView = etPhoneNo;
                cancel = true;
            }

        }

        try
        {
            if (Dob.contains(DateFormat.getTimeInstance().format(new Date()))|| Dob==null|| Dob.contains(String.valueOf(new Time().getYear())))
            {
                btDob.setError(getString(R.string.error_invalid_date_of_birth));
                cancel=true;
                focusView=btDob;
            }
        }
        catch (Exception e)
        {
            btDob.setError(getString(R.string.error_invalid_date_of_birth));
            cancel=true;
            focusView=btDob;
        }

if(focusView==null)
{
    return false;
}

        if (cancel)
        { focusView.requestFocus();
            return true;
        }
        else{


            return false;
        }



    }

    private void clearErrors(){
        etFirstName.setError(null);
        etLastName.setError(null);
        etPhoneNo.setError(null);
        etPhoneNo.setError(null);
        btDob.setError(null);


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    public static   String getFirstName(){ return mFirstName;}
    public static String getLastName(){return  mLastName;}
    public static   String getState(){return mState;}
    public static   String getCountry(){return mCountry;}
    public static   String getPhoneNo(){return "0"+mPhoneNo;}
    public static String getBirthday(){return _Dob;};
    public static String getDateOfBirth(){return Dob;}

    public static String getSex(){return mSex;}



    public boolean isCancel(){return cancel;}




    public void setUpYearPicker(){
      final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Dialog.Builder builder;
        builder = new DatePickerDialog.Builder(){
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                DatePickerDialog dialog = (DatePickerDialog)fragment.getDialog();
                dialog.getMonth();
                dialog.getDay();

                Dob = dialog.getFormattedDate(sdf);

                _Dob=dialog.getFormattedDate(new SimpleDateFormat("MM-dd", Locale.US));
                btDob.setText(Dob);

                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {

                super.onNegativeActionClicked(fragment);
            }
        }.dateRange(1,1,1950,31,12,2002);

        builder.positiveAction("OK")
                .negativeAction("CANCEL");
        DialogFragment fragment= DialogFragment.newInstance(builder);
        fragment.show(getFragmentManager(),null);
    }
}
