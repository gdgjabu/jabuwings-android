package com.jabuwings.views.interceptors;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jabuwings.views.main.Domicile;

public class Share extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, Domicile.class).putExtra("share",true).putExtra("intent",getIntent()));
        finish();
        //setContentView(R.layout.activity_share);


    }

}

