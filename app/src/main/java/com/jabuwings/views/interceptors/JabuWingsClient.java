package com.jabuwings.views.interceptors;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.encryption.StringEncrypter;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.JabuWingsUser;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.main.Login;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;


import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class JabuWingsClient extends JabuWingsActivity {

    @InjectView(R.id.tvClient)
    TextView tvClientInfo;
    @InjectView(R.id.btAllowClient)
    Button btAllow;
    @InjectView(R.id.btDeclineClient)
    Button btDecline;
    ParseUser user;
    JabuWingsUser jabuWingsUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jabuwings_client);
        setUpToolbar(null,"Authorize app");
        ButterKnife.inject(this);
        user=ParseUser.getCurrentUser();
        jabuWingsUser= new JabuWingsUser(user);
        String clientId=getIntent().getStringExtra(Const.CLIENT_ID);
        String clientKey=getIntent().getStringExtra(Const.CLIENT_KEY);

        if(!new Manager(this).isLoggedIn())
        {
            finish();
            startActivity(new Intent(this,Login.class));
        }

      //   StringEncrypter encrypter =new StringEncrypter("adic2342jkdfafal3823asd3");
      //   String[] a=encrypter.decrypt(clientKey).split("&");
     //   String verifier=a[0];
       // String appName=a[1];



        if(clientId.equals("idd") && clientKey.equals("keyy"))
        {
            tvClientInfo.setText("The app(NACOSS JABU) will like to access your account details");

        }

        else
        {
            rejectProposal();
        }





    }

    public void getDepartmentUsers(String department)
    {

        ParseQuery<ParseUser> q= ParseUser.getQuery();
        q.whereEqualTo(Const.DEPARTMENT,department);
        q.whereEqualTo(Const.TYPE, 0);
        q.orderByAscending(Const.NAME);


        final SweetAlertDialog sweetAlertDialog=dialogWizard.showSimpleProgress(getString(R.string.please_wait),"Loading Students");
        q.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> list, ParseException e) {
                sweetAlertDialog.dismiss();
                if(e==null)
                {

                    Intent result = new Intent("com.jabuwings.client", Uri.parse("content://result_uri"));


                }
                else{

                }
            }
        });


    }
    @OnClick(R.id.btAllowClient)
    public void allowClick()
    {
        Intent result = new Intent("com.jabuwings.client.login", Uri.parse("content://result_uri"));
        //result.putExtra(Const.USER,new JabuWingsUser(ParseUser.getCurrentUser()));

        result.putExtra(Const.USERNAME, jabuWingsUser.getUsername());
        result.putExtra(Const.NAME, jabuWingsUser.getName());
        result.putExtra(Const.DEPARTMENT, jabuWingsUser.getDepartment());
        result.putExtra(Const.EMAIL, jabuWingsUser.getEmail());


        setResult(Activity.RESULT_OK, result);
        finish();
    }

    @OnClick(R.id.btDeclineClient)
    public void declineClick()
    {
        rejectProposal();
    }

    private void rejectProposal()
    {
        Intent result = new Intent("com.jabuwings.client.login", Uri.parse("content://result_uri"));
        setResult(Activity.RESULT_CANCELED, result);
        finish();
    }
}
