package com.jabuwings.views;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.intermediary.HotSpot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProfileUpdater extends AppCompatActivity implements AdapterView.OnItemClickListener{
ListView lvItems;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_updater);




        String [] list= {"Update Status","Edit Name","Change Password"};
        List<String> stringList= new ArrayList<>(Arrays.asList(list));
        lvItems= (ListView)findViewById(R.id.lv_profile_updater);
        ArrayAdapter arrayAdapter;
        arrayAdapter=  new ArrayAdapter<>(this,R.layout.profile_item,R.id.tv_profile_item,stringList);
        lvItems.setAdapter(arrayAdapter);
        lvItems.setOnItemClickListener(this);







    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        switch (position){
            case 0:
                updateStatus();
                break;
            case 1:
                updateName();
                break;
            case 2:
                break;
        }


    }


    private void updateName(){}

    private void updateStatus()
    {
        new MaterialDialog.Builder(ProfileUpdater.this)
                .title(getString(R.string.status))
                .content(getString(R.string.new_status))
                .inputType(InputType.TYPE_CLASS_TEXT)
                .inputRange(1, 100)
                .positiveText(getString(R.string.update))
                .negativeText(getString(R.string.cancel))
                .input(getString(R.string.status), null, false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {

                        HotSpot.updateProfile(getApplicationContext(),2,input.toString());


                    }
                }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile_updater, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
