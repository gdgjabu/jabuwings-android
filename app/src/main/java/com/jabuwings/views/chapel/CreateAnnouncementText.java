package com.jabuwings.views.chapel;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.views.main.JabuWingsActivity;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class CreateAnnouncementText extends JabuWingsActivity {

    
    @InjectView(R.id.txt)
    EditText editText;
    @InjectView(R.id.address)
    RadioGroup radioGroup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_announcement_text);
        setUpToolbar(null,"Add announcement"); 
        ButterKnife.inject(this);
        
        String announcement = getIntent().getStringExtra("text");
        if(announcement!=null) editText.setText(announcement);
    }
    
    @OnClick(R.id.submit)
    public void submit()
    {
        String announcement = editText.getText().toString();
        String to="";
        if(radioGroup.getCheckedRadioButtonId()==R.id.students)
        {
            to="students";
        }
        else if(radioGroup.getCheckedRadioButtonId()==R.id.staff)
            to="staff";
        else if (radioGroup.getCheckedRadioButtonId()==R.id.all)
            to="all";

        
        if(!TextUtils.isEmpty(announcement) && announcement.length()>=10)
        {
            final ParseObject parseObject = new ParseObject("Chapel_Announcements");
            parseObject.put(Const.TITLE,"");
            parseObject.put(Const.CONTENT,announcement);
            parseObject.put(Const.TO,to);
            parseObject.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if(e!=null)
                        parseObject.saveEventually();
                    else{
                        manager.successToast("Announcement added");
                    }
                }
            });
            finish();
        }
        else manager.shortToast("Announce too short or empty");
        
    }
    
}
