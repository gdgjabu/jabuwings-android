package com.jabuwings.views.chapel;


import android.app.ProgressDialog;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * A simple {@link Fragment} subclass.
 */
public class Recordings extends Fragment {


    public Recordings() {
        // Required empty public constructor
    }


    @InjectView(R.id.rvRecordings)
    RecyclerView recycler;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_recordings, container, false);
        ButterKnife.inject(this,view);
        get();
        return view;
    }

    private void get()
    {
       recycler.setLayoutManager( new LinearLayoutManager(getContext()));

        ParseQuery.getQuery("Chapel_Recordings").findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e==null)
                {
                    recycler.setAdapter(new Adapter(list));
                }

                else get();
            }
        });

    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        @InjectView(R.id.recording_photo)
        ImageView thumbnail;
        @InjectView(R.id.recording_text)
        TextView text;
        public ViewHolder(View v)
        {
            super(v);
            ButterKnife.inject(this,v);

        }
    }
    boolean playPause;
    MediaPlayer
            mediaPlayer = new MediaPlayer();

    /**
     * remain false till media is not completed, inside OnCompletionListener make it true.
     */
    boolean initialStage = true;
    ImageButton btn;
    public void setUpAudio(final ParseObject recording)
    {
        final String url= recording.getString("url")==null? recording.getParseFile("media").getUrl() :  recording.getString("url");

        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        Dialog.Builder builder;
        builder = new SimpleDialog.Builder(R.style.SimpleDialog){
            @Override
            protected void onBuildDone(Dialog dialog) {
                btn= (ImageButton) dialog.findViewById(R.id.recordings_audio_pause_play);

                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (!playPause) {
                            btn.setBackgroundResource(R.drawable.img_btn_pause);
                            if (initialStage)
                                new Player()
                                        .execute(url);
                            else {
                                if (!mediaPlayer.isPlaying())
                                    mediaPlayer.start();
                            }
                            playPause = true;
                        } else {
                            btn.setBackgroundResource(R.drawable.img_btn_play);
                            if (mediaPlayer.isPlaying())
                                mediaPlayer.pause();
                            playPause = false;
                        }

                    }
                });
                super.onBuildDone(dialog);



            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
                mediaPlayer.stop();
                mediaPlayer.reset();
                mediaPlayer.release();
            }
        };

        builder.title(recording.getString(Const.TITLE)).negativeAction(getString(R.string.close))
                .contentView(R.layout.recordings_audio_item);
        DialogFragment recordFragment = DialogFragment.newInstance(builder);
        recordFragment.setCancelable(false);
        recordFragment.show(getFragmentManager(), null);

    }

    class Player extends AsyncTask<String, Void, Boolean> {
        private ProgressDialog progress;

        @Override
        protected Boolean doInBackground(String... params) {
            // TODO Auto-generated method stub
            Boolean prepared;
            try {

                mediaPlayer.setDataSource(params[0]);

                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        initialStage = true;
                        playPause=false;
                        btn.setBackgroundResource(R.drawable.img_btn_play);
                        mediaPlayer.stop();
                        mediaPlayer.reset();
                    }
                });
                mediaPlayer.prepare();
                prepared = true;
            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                Log.d("IllegarArgument", e.getMessage());
                prepared = false;
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                prepared = false;
                e.printStackTrace();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                prepared = false;
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                prepared = false;
                e.printStackTrace();
            }
            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (progress.isShowing()) {
                progress.cancel();
            }
            Log.d("Prepared", "//" + result);
            mediaPlayer.start();

            initialStage = false;
        }

        public Player() {
            progress = new ProgressDialog(getActivity());
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            this.progress.setMessage("Buffering...");
            this.progress.show();

        }
    }
    public class Adapter extends RecyclerView.Adapter<ViewHolder>
    {
        List<ParseObject> recordings;

        public Adapter(List<ParseObject> recordings)
        {
            this.recordings=recordings;
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.recordings_item,parent,false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            ParseObject recording= recordings.get(position);

            holder.text.setText(recording.getString(Const.TITLE));
            if(recording.getInt(Const.TYPE)==0)
            {
                //audio
                holder.thumbnail.setImageResource(R.drawable.ic_audiotrack_black_48dp);

            }
            else{
                //youtube video

                
                Picasso.with(getContext()).load(recording.getParseFile("thumbnail").getUrl()).into(holder.thumbnail);

            }
            holder.itemView.setTag(recording);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ParseObject record = (ParseObject) v.getTag();
                    final String url= record.getString("url")==null? record.getParseFile("media").getUrl() :  record.getString("url");
                    startActivity(new Intent(getActivity(),RecordDetails.class).putExtra(Const.URL,url).putExtra(Const.TITLE,record.getString(Const.TITLE)));
//                    setUpAudio((ParseObject)v.getTag());
                }
            });


        }

        @Override
        public int getItemCount() {
            return recordings.size();
        }
    }
}
