package com.jabuwings.views.chapel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.jabuwings.R;
import com.jabuwings.views.main.JabuWingsActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class PreOcrCapture extends JabuWingsActivity {

    int RC_OCR_CAPTURE  = 23456;
    @Override  
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_ocr_capture);
        ButterKnife.inject(this);
        setUpToolbar(null,"Add announcement");
    }

    @OnClick(R.id.enter_text)
    public void enterText()
    {
        startActivity(new Intent(this,CreateAnnouncementText.class));
    }

    @OnClick(R.id.capture_ocr)
    public void captureOcr()
    {
        Intent intent = new Intent(this, OcrCaptureActivity.class);
        intent.putExtra(OcrCaptureActivity.AutoFocus, true);
//        intent.putExtra(OcrCaptureActivity.UseFlash, useFlash.isChecked());

        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RC_OCR_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    String text = data.getStringExtra(OcrCaptureActivity.TextBlockObject);
                    manager.log("captured text "+text);
                    
                    
                } else {
                    
                    manager.log( "No Text captured, intent data is null");
                }
            } else {
               
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
