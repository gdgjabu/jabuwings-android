package com.jabuwings.views.chapel;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.utilities.Time;
import com.jabuwings.widgets.section.SectionedRecyclerViewAdapter;
import com.jabuwings.widgets.section.SimpleSectionedAdapter;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * A simple {@link Fragment} subclass.
 */
public class Announcements extends Fragment {

    @InjectView(R.id.announcement_text)
    TextView text;
    @InjectView(R.id.rvAnnouncements)
    RecyclerView recycler;
    View rootView;


    public Announcements() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView= inflater.inflate(R.layout.fragment_announcements, container, false);
        ButterKnife.inject(this,rootView);

        //TODO bad method;save offline
        ParseQuery.getQuery("Chapel_Announcements").addDescendingOrder(Const.CREATED_AT).setLimit(100).findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e==null)
                {
                    text.setVisibility(View.GONE);
                    recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recycler.setAdapter(new Adapter(getActivity(),list));

                }
                else{
                    text.setText("Unable to load announcements "+e.getMessage());
                }
            }
        });
        return rootView;
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        @InjectView(R.id.announcement_title)
        TextView title;
        @InjectView(R.id.announcement_content)
        TextView content;
        public ViewHolder(View v)
        {
            super(v);
            ButterKnife.inject(this,v);
        }
    }

    public class Adapter extends SimpleSectionedAdapter<ViewHolder> {

        List<String> headers=new ArrayList<>();
        List<Integer> headerCounts=new ArrayList<>();
        List<ParseObject> announcements;

        Context context;

        private int getHeaderPosition(String date)
        {
            for(int i=0; i<headers.size();)
            {
                String h= headers.get(i);
                if(h.equals(date))
                    return i;
            }

            return 0;
        }
        public Adapter (Context context,List<ParseObject> announcements)
        {
            this.announcements=announcements;
            this.context=context;
            int hPosition=0;
            for(ParseObject object: announcements)
            {
                Date date= object.getCreatedAt();
                int year =date.getYear()+1900;
                int month=date.getMonth()+1;
                int day=date.getDate();
                String headerFormat=""+day+"/"+month+"/"+year;
                if(!headers.contains(headerFormat))
                {
                    headers.add(headerFormat);
                    headerCounts.add(1);

                    hPosition++;
                }
                else{
                    int currentCount= headerCounts.get(getHeaderPosition(headerFormat));
                    headerCounts.set(getHeaderPosition(headerFormat),++currentCount);
                }


            }
        }

        @Override
        protected String getSectionHeaderTitle(int section) {
            return headers.get(section);
        }

        @Override
        protected int getSectionCount() {
            return headers.size();
        }

        @Override
        protected int getItemCountForSection(int section) {

            int size=0;
            String header= headers.get(section);
            for(ParseObject a: announcements)
            {
                Date date= a.getCreatedAt();
                int year =date.getYear()+1900;
                int month=date.getMonth()+1;
                int day=date.getDate();
                String headerFormat=""+day+"/"+month+"/"+year;

                if(header.equals(headerFormat)) size++;
            }
            return size;
        }

        @Override
        protected ViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.announcement_item,parent,false));
        }

        @Override
        protected void onBindItemViewHolder(ViewHolder holder, int section, int position) {
            String header= headers.get(section);

            ParseObject announcement =announcements.get(position);

            Date date= announcement.getCreatedAt();
            int year =date.getYear()+1900;
            int month=date.getMonth()+1;
            int day=date.getDate();
            String headerFormat=""+day+"/"+month+"/"+year;

            if(headerFormat.equals(header))
            {
                holder.title.setText(announcement.getString(Const.TITLE));
                holder.content.setText(announcement.getString(Const.CONTENT));
            }

        }





    }


}
