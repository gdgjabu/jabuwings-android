package com.jabuwings.views.chapel;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.management.Manager;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.main.JabuWingsFragment;
import com.lapism.searchview.SearchAdapter;
import com.lapism.searchview.SearchItem;
import com.lapism.searchview.SearchView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class Hymns extends Fragment {
    TextView tvHymn;
    Manager manager;
    Context context;
    View rootView;
    private static Hymns instance;
    SearchView searchView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=getActivity();
        instance=this;
        manager= new Manager(context);
        String[] suggestions=getResources().getStringArray(R.array.hymns_list);


    }

    public static Hymns getInstance()
    {
        return instance;
    }

    public void openVariousHymn()
    {
        new MaterialDialog.Builder(context)
                .title("Open various hymn")
                .content("Please enter the 'various hymn' number")
                .inputType(InputType.TYPE_CLASS_NUMBER)
                .inputRange(1,3)
                .positiveText("Open")
                .input("hymn number", null, false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {

                        try{
                            decodeVariousHymn(input.toString());}
                        catch (Exception e)
                        {
                            manager.shortToast("Hymn does not exist");
                        }


                    }
                }).show();
    }
    public void openStandardHymn()
    {
        new MaterialDialog.Builder(context)
            .title("Open standard hymn")
            .content("Please enter the hymn number")
            .inputType(InputType.TYPE_CLASS_NUMBER)
            .inputRange(1,3)
            .positiveText("Open")
            .input("hymn number", null, false, new MaterialDialog.InputCallback() {
                @Override
                public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {

                    try{
                    decodeHymn(input.toString());}
                    catch (Exception e)
                    {
                        manager.shortToast("Hymn does not exist");
                    }


                }
            }).show();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView=inflater.inflate(R.layout.activity_hyms, container, false);
        tvHymn=(TextView)rootView.findViewById(R.id.tv_hymn);
        searchView=(SearchView)rootView.findViewById(R.id.searchView);
        searchView.setArrowOnly(true);

        String[] s=getResources().getStringArray(R.array.hymns_list);
        List<SearchItem>  suggestions=new ArrayList<>();
        for(String h: s) suggestions.add(new SearchItem(h));
        SearchAdapter searchAdapter =new SearchAdapter(getActivity(),suggestions);
        searchView.setAdapter(searchAdapter);

        searchView.setOnVoiceClickListener(new SearchView.OnVoiceClickListener() {
            @Override
            public void onVoiceClick() {

            }
        });

        searchAdapter.addOnItemClickListener(new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                TextView textView = (TextView) view.findViewById(R.id.textView_item_text);
                String h = textView.getText().toString();
                searchView.close(false);

                if(h.contains("Various"))
                {
                    openHymn(h.replaceAll("[^0-9]", "")); // or str.replaceAll("\\D+",""); to delete non-digits
                }
                else
                    try {
                        openHymn(h.split(" ")[1]);
                    }
                    catch (Exception e)
                    {
                        manager.longToast("Invalid hymn. Please select from the suggestions");
                    }

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String h) {
                searchView.close(false);
                if(h.contains("Various"))
                {
                    openHymn(h.replaceAll("[^0-9]", "")); // or str.replaceAll("\\D+",""); to delete non-digits
                }
                else
                    try {
                        openHymn(h.split(" ")[1]);
                    }
                    catch (Exception e)
                    {
                        manager.longToast("Invalid hymn. Please select from the suggestions");
                    }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        JabuWingsActivity.setUpTextViews(getActivity(),tvHymn);
        try{
            String hymn =manager.getLastHymn();
            if(hymn.contains("v"))
            {
                decodeVariousHymn(new StringBuilder(hymn).delete(0,1).toString());
            }
            else
        decodeHymn(manager.getLastHymn());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return rootView;
    }

    public void openHymn(String hymnNo)
    {
        decodeHymn(hymnNo);
    }
    private void decodeVariousHymn(String hymnNo)
    {
        int variousHymnNumber=Integer.valueOf(hymnNo);
        tvHymn.setText(getResId("varioushymn_"+variousHymnNumber));
        manager.storeLastHymn("v"+hymnNo);
    }

    private void decodeHymn(String hymnNo)
    {
        switch (hymnNo.length())
        {
            case 1:
                tvHymn.setText(getResId("hymn_" + "00" + hymnNo));
                manager.storeLastHymn(hymnNo);
                //setTitle("Hymn "+hymnNo);
                break;
            case 2:
                tvHymn.setText(getResId("hymn_" + "0" + hymnNo));
                manager.storeLastHymn(hymnNo);
                //setTitle("Hymn " + hymnNo);
                break;
            case 3:
                tvHymn.setText(getResId("hymn_" + hymnNo));
                manager.storeLastHymn(hymnNo);
               // setTitle("Hymn "+hymnNo);
                break;
            default:
            manager.shortToast("Hymn does not exist");
        }
    }

    public static int getResId(String resName)
    {
        try{
            Class res= R.string.class;
            Field field=res.getField(resName);
            int id=field.getInt(null);
            return id;
        }catch (Exception e)
        {
            e.printStackTrace();
            return -1;
        }
    }
}
