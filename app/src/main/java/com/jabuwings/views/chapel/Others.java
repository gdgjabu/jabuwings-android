package com.jabuwings.views.chapel;



import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.DialogWizard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.InjectView;

/**
 * A simple {@link Fragment} subclass.
 */
public class Others extends Fragment {
    Manager manager;
    Context context;
    private static Others instance;
    ListView lvOthers;
    View rootView;
    public Others() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=getActivity();
        manager=new Manager(context);
        instance=this;
    }

    public static Others getInstance()
    {
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_chapel_others, container, false);
        lvOthers=(ListView)rootView.findViewById(R.id.lv_chapel_others);

        final DialogWizard dialogWizard= new DialogWizard(getActivity());
        String[] pick ={
                "CAC watchword","Chapel Hours"
        };
        List<String> a = new ArrayList<>(Arrays.asList(pick));

        ArrayAdapter pickerAdapter;
        pickerAdapter= new ArrayAdapter<>(getContext(), R.layout.picker_item, R.id.tv_picker,a);
        lvOthers.setAdapter(pickerAdapter);
        lvOthers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position)
                {
                    default:

                        new MaterialDialog.Builder(getActivity())
                                .title("Anthem")
                                .content(getString(R.string.school_anthem_verse1)+getString(R.string.school_anthem_chorus)+getString(R.string.school_anthem_verse2)+getString(R.string.school_anthem_chorus))
                                .show();
                        break;
                    case 0:
                        String watchword=PreferenceManager.getDefaultSharedPreferences(context).getString("watchword", getString(R.string.chapel_watchword));
                        dialogWizard.showSimpleDialog("Watchword",watchword);
                        break;
                    case 1:
                        new MaterialDialog.Builder(getActivity())
                                .title("Chapel Hours")
                                .customView(R.layout.chapel_activities, true)
                                //.content(getString(R.string.chapel_hours))
                                .show();
                        break;
                }
            }
        });
        return rootView;
    }


    @Override
    public boolean getUserVisibleHint() {
        return super.getUserVisibleHint();
    }

    private void openBible()
    {

    }

}
