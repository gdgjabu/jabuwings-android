package com.jabuwings.views.chapel;


import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.jabuwings.R;
import com.jabuwings.models.ViewPagerAdapter;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.button.FloatingActionButton;
import com.jabuwings.widgets.button.FloatingActionsMenu;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

public class Chapel extends JabuWingsActivity{
    TabLayout tabLayout;
    ViewPager viewPager;
    static FloatingActionsMenu btnDo;
    MaterialSearchView searchView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapel);
        tabLayout=(TabLayout)findViewById(R.id.chapel_tabs);
        viewPager=(ViewPager)findViewById(R.id.chapel_view_pager);
        btnDo=(FloatingActionsMenu)findViewById(R.id.fabChapel);

        searchView = (MaterialSearchView) findViewById(R.id.chapel_search_view);
        String[] suggestions=getResources().getStringArray(R.array.hymns_list);
        searchView.setSuggestions(suggestions);

        searchView.setHint("Enter Hymn Title or number");

        searchView.setSubmitOnClick(true);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
               String[] a= query.split(" - ");
               String h=a[0];
                if(h.contains("Various"))
                {
                    //String hymnNo=h.split()
                }
                else{
                    String hymnNo=h.split(" ")[1];
                    Hymns.getInstance().openHymn(hymnNo);
                }
                //Do some magic
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });

        FloatingActionButton fabStandard= new FloatingActionButton(this);
        fabStandard.setTitle("Standard Hymns");
        fabStandard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDo.collapse();
                handleClick(true);
            }
        });

        FloatingActionButton fabVarious= new FloatingActionButton(this);
        fabVarious.setTitle("Various Hymns");
        fabVarious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDo.collapse();
                handleClick(false);
            }
        });

        btnDo.addButton(fabStandard);
        btnDo.addButton(fabVarious);
        setUpToolbar(null, "Chapel");
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==0)
                {
                    btnDo.setVisibility(View.VISIBLE);
                }
                else
                    btnDo.setVisibility(View.GONE);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void handleClick(boolean standard)
    {
        Hymns hymns= Hymns.getInstance();
        Others bible= Others.getInstance();
        if(hymns.getUserVisibleHint())
        {
            if(standard)
            {
                hymns.openStandardHymn();
            }
            else{
                hymns.openVariousHymn();
            }
        }

        if(bible.getUserVisibleHint())
        {

        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chapel,menu);
        MenuItem item = menu.findItem(R.id.search_hymns);
        searchView.setMenuItem(item);

        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                searchView.setVisibility(View.VISIBLE);
                searchView.showSearch();
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private int[] tabIcons = {
            R.drawable.ic_audiotrack_white_18dp,
            R.drawable.ic_audiotrack_white_18dp,

    };

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Hymns(), "Hymns");
        adapter.addFragment(new Others(), "Others");
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
    }

    @SuppressWarnings("all")
    private void setupTabIcons() {

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
    }

}
