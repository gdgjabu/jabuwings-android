package com.jabuwings.views.chapel;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.models.ViewPagerAdapter;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.registration.CustomViewPager;
import com.parse.ParseConfig;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ChapelHome extends JabuWingsActivity {


    @InjectView(R.id.chapelPager)
    CustomViewPager pager;
    Menu menu;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.chapel_hymns:
                    setTitle(R.string.title_home);
                    pager.setCurrentItem(0);
                    return true;
                case R.id.chapel_recordings:
                    setTitle(R.string.recording);
                    pager.setCurrentItem(1);
                    return true;
                case R.id.chapel_announcements:
                    setTitle(R.string.announcements);
                    pager.setCurrentItem(2);
                    return true;
                case R.id.chapel_others:
                    setTitle(R.string.title_notifications);
                    pager.setCurrentItem(3);
                    return true;
            }
            return false;
        }

    };

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Hymns(), "Hymns");
        adapter.addFragment(new Recordings(), "Recordings");
        adapter.addFragment(new Announcements(), "Announcements");
        adapter.addFragment(new Others(), "Others");
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
              

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    BottomNavigationView navigation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapel_home);
        ButterKnife.inject(this);
        setUpToolbar(null,"Chapel");
        setupViewPager(pager);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_context_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int position = pager.getCurrentItem();
        switch (position)
        {
            case 1:
                break;
            case 2:
                //verify that the user has the right permission
                addAnnouncements();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    
    private void addAnnouncements()
    {
        ParseConfig parseConfig = ParseConfig.getCurrentConfig();
        List<String> users = parseConfig.getList("chapel_announcers");
        
        if(users!=null && users.contains(manager.getUserId()))
        {
            startActivity(new Intent(this,PreOcrCapture.class));
        }
        else {
            manager.errorToast("You don't have the permissions to add announcements.");
            ParseConfig.getInBackground();
            
        }
    }
}
