package com.jabuwings.views.widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.jabuwings.R;
import com.jabuwings.models.TimeTableItem;
import com.jabuwings.views.timetable.TimeKeeper;

import java.util.List;

/**
 * Created by Falade James on 9/9/2016 All Rights Reserved.
 */
public class LectureReminderWidgetProvider extends AppWidgetProvider {

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int count = appWidgetIds.length;


        for (int i = 0; i < count; i++) {
            int widgetId = appWidgetIds[i];
            //String number = String.format("%03d", (new Random().nextInt(900) + 100));

            RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                    R.layout.lecture_reminder_widget);
            List<TimeTableItem> nextLectures= TimeKeeper.getNextLectures(context);
            if(nextLectures.size()>0)
            {
                TimeTableItem nextLecture = nextLectures.get(0);
                remoteViews.setTextViewText(R.id.lecture_reminder_text, "Your next lecture is "+nextLecture.getCourseCode());
                remoteViews.setTextViewText(R.id.lecture_reminder_time,"Time: "+nextLecture.getStartingHour()+" to "+nextLecture.getEndingHour());
                remoteViews.setTextViewText(R.id.lecture_reminder_venue,"Venue: "+nextLecture.getCourseVenue());
            }
            else{
                remoteViews.setTextViewText(R.id.lecture_reminder_text, "You currently have no lectures");
                remoteViews.setTextViewText(R.id.lecture_reminder_time, "");
                remoteViews.setTextViewText(R.id.lecture_reminder_venue, "");
            }


            Intent intent = new Intent(context, LectureReminderWidgetProvider.class);
            intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                    0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            Intent timetableIntent= new Intent(context, LectureReminderWidgetProvider.class);
            timetableIntent.putExtra("check",true);
            PendingIntent timetablePending = PendingIntent.getBroadcast(context,
                    0, timetableIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            remoteViews.setOnClickPendingIntent(R.id.lecture_reminder_refresh, pendingIntent);
            remoteViews.setOnClickPendingIntent(R.id.lecture_reminder_view,timetablePending);
            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }
    }

    /*
    private void homeScreenOrLockScreen()
    {
        AppWidgetManager appWidgetManager;
        int widgetId;
        Bundle myOptions = appWidgetManager.getAppWidgetOptions (widgetId);

// Get the value of OPTION_APPWIDGET_HOST_CATEGORY
        int category = myOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_HOST_CATEGORY, -1);

// If the value is WIDGET_CATEGORY_KEYGUARD, it's a lockscreen widget
        boolean isKeyguard = category == AppWidgetProviderInfo.WIDGET_CATEGORY_KEYGUARD;

        int baseLayout = isKeyguard ? R.layout.keyguard_widget_layout : R.layout.widget_layout;

    }*/

}
