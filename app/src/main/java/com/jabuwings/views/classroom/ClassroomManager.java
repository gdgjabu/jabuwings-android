package com.jabuwings.views.classroom;

import android.content.Context;
import static com.jabuwings.management.Const.*;

import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.management.Manager;
import com.parse.ParseObject;

import java.util.Date;

/**
 * Created by Falade James on 7/23/2016 All Rights Reserved.
 */
public class ClassroomManager extends Manager {
    public ClassroomManager(Context context)
    {
        super(context);
    }

    public void addNotification(String title, String content)
    {
        HouseKeeping.storeNotification(4,new Date(),content,"JabuWings Classroom","");

    }
}
