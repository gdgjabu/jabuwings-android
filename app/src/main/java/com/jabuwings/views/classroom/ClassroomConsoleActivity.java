package com.jabuwings.views.classroom;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.AcademicsManager;
import com.jabuwings.management.Const;
import com.jabuwings.models.ClassroomCourse;
import com.jabuwings.models.ClassroomLecturer;
import com.jabuwings.models.ClassroomMaterial;
import com.jabuwings.models.ClassroomStudent;
import com.jabuwings.models.CourseMaterialAdapter;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.rey.material.widget.ProgressView;

import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Falade James on 7/19/2016 All Rights Reserved.
 */
public class ClassroomConsoleActivity extends JabuWingsActivity {

    DialogWizard dialogWizard;
    AcademicsManager academicsManager;
    ClassroomManager classroomManager;
    ClassroomConsoleActivity classroomConsoleActivity;
    Time time;
    String selectedCourseCode;

    ProgressView materialsProgressView;
    ListView materialsListView;
    TextView materialsTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    /**
     *
     * @param type type 0 is for a student and type 1, a lecturer.
     */
    public void initialiseClassroom(final ClassroomConsoleActivity classroomConsoleActivity,final ReadyNotifier readyNotifier,final int type)
    {
        this.classroomConsoleActivity=classroomConsoleActivity;
        time=new Time();
        classroomManager=new ClassroomManager(classroomConsoleActivity);
        dialogWizard= new DialogWizard(classroomConsoleActivity);
        academicsManager = new AcademicsManager(classroomConsoleActivity);
        final String classroomClass= type==0? Const.CLASSROOM_STUDENT : Const.CLASSROOM_LECTURER;
        Class currentClass=type==0? StudentConsole.class : LecturerConsole.class;
        if(type==0)
        {
                    
            ClassroomStudent.getQuery().fromLocalDatastore().whereEqualTo(Const.USER,manager.getUser()).getFirstInBackground(new GetCallback<ClassroomStudent>() {
                        @Override
                        public void done(ClassroomStudent classroomStudent, ParseException e) {
                if(e==null)
                {
                    getStudentCourses(readyNotifier,classroomStudent);
                }
                else if(e.getCode()==ParseException.OBJECT_NOT_FOUND){
                    final String message= "Classroom provides a platform of interaction between students and their lecturers." +
                            " You as a student will be able to submit assignments, access notifications, download course materials and much more. Please note that upon registration, we'll obtain the list of courses you've currently registered this semester.";
                    final SweetAlertDialog checkDialog=dialogWizard.showSimpleProgress(R.string.please_wait,R.string.lets_check_something);
                    ParseQuery<ClassroomStudent> query= ClassroomStudent.getQuery();
                    query.whereEqualTo(Const.USER,manager.getUser());
                    query.getFirstInBackground(new GetCallback<ClassroomStudent>() {
                        @Override
                        public void done(final ClassroomStudent classroomStudent, ParseException e) {
    
                            if(e==null)
                            {
    
                                classroomStudent.pinInBackground();
                                classroomStudent.getCourses().getQuery().findInBackground(new FindCallback<ClassroomCourse>() {
                                    @Override
                                    public void done(List<ClassroomCourse> list, ParseException e) {
                                        checkDialog.dismiss();
                                        if(e==null)
                                        {
                                            for(ClassroomCourse classroomCourse : list)
                                            {
                                                try {classroomCourse.pin();}catch (ParseException e1){e1.printStackTrace();}
                                            }
    
                                            readyNotifier.onStudentReady(classroomStudent,list);
    
                                        }
                                        else{
                                            readyNotifier.onStudentReady(classroomStudent,null);
                                        }
                                        dialogWizard.showSimpleDialog(getString(R.string.classroom),"Welcome back to Classroom. Just to remind you\n"+message);
                                    }
                                });
    
    
                            }
    
                            else if(e.getCode()==ParseException.OBJECT_NOT_FOUND)
                            {
                                checkDialog.dismiss();
                                registerStudent(readyNotifier,message);
    
                            }
                            else{
                                checkDialog.dismiss();
                                ErrorHandler.handleError(classroomConsoleActivity,e);
                                finish();
                            }
                        }
                    });
                } else{
                    dialogWizard.showErrorDialog(getString(R.string.error_unexpected),e.getMessage());
                }

                        }
            });
                
        }
        else if (type==1)
        {
            ClassroomLecturer.getQuery().fromLocalDatastore().whereEqualTo(Const.USER,manager.getUser()).getFirstInBackground(new GetCallback<ClassroomLecturer>() {
                @Override
                public void done(ClassroomLecturer classroomLecturer, ParseException e) {
                    if(e==null)
                    {
                        getLecturerCourses(readyNotifier,classroomLecturer);
                    }
                    else if(e.getCode()==ParseException.OBJECT_NOT_FOUND){
                        final String message= "Classroom provides a platform of interaction between students and their lecturer. You as a lecturer will be able to give your students assignments, mark the assignments, assign scores and send back the result to the student. You will also be able to send special notifications or messages to your students, schedule classes and distribute course materials within JabuWings Classroom.";
                        final SweetAlertDialog checkDialog=dialogWizard.showSimpleProgress(R.string.please_wait,R.string.lets_check_something);
                        ParseQuery<ClassroomLecturer> query = ClassroomLecturer.getQuery();
                        query.whereEqualTo(Const.USER,manager.getUser());
                        query.getFirstInBackground(new GetCallback<ClassroomLecturer>() {
                            @Override
                            public void done(final ClassroomLecturer classroomLecturer, ParseException e) {
                                if(e==null)
                                {
                                    classroomLecturer.getCourses().getQuery().findInBackground(new FindCallback<ClassroomCourse>() {
                                        @Override
                                        public void done(List<ClassroomCourse> list, ParseException e) {
                                            if(e==null)
                                            {


                                                for(ParseObject course: list)
                                                {
                                                    course.pinInBackground();
                                                }

                                                checkDialog.dismiss();
                                                classroomLecturer.pinInBackground();
                                                readyNotifier.onLecturerReady(classroomLecturer,list);
                                                dialogWizard.showSimpleDialog(getString(R.string.classroom),"Welcome back to Classroom. Just to remind you\n"+message);
                                            }


                                            else{
                                                checkDialog.dismiss();
                                                ErrorHandler.handleError(classroomConsoleActivity,e);
                                                finish();
                                            }
                                        }
                                    });
                                }

                                else if(e.getCode()==ParseException.OBJECT_NOT_FOUND)
                                {
                                    checkDialog.dismiss();
                                    registerLecturer(readyNotifier,message);

                                }

                                else{
                                    checkDialog.dismiss();
                                    ErrorHandler.handleError(classroomConsoleActivity,e);
                                    finish();
                                }
                            }
                        });

                    }  else{
                        dialogWizard.showErrorDialog(getString(R.string.error_unexpected),e.getMessage());
                    }
                }
            });
        }



    }

    private void getStudentCourses(final ReadyNotifier readyNotifier, final ClassroomStudent classroomStudent)
    {
        ClassroomCourse.getQuery().fromLocalDatastore().whereEqualTo(Const.USER,manager.getUser()).whereEqualTo(Const.VALID,true).findInBackground(new FindCallback<ClassroomCourse>() {
            @Override
            public void done(List<ClassroomCourse> list, ParseException e) {
                if(e==null)
                {
                    readyNotifier.onStudentReady(classroomStudent,list);
                }
                else
                {
                    e.printStackTrace();
                    readyNotifier.onStudentReady(classroomStudent,null);
                }

            }
        });

    }

    private void getLecturerCourses(final ReadyNotifier readyNotifier, final ClassroomLecturer classroomLecturer)
    {
      ClassroomCourse.getQuery().fromLocalDatastore().whereEqualTo(Const.USER,manager.getUser()).whereEqualTo(Const.VALID,true).findInBackground(new FindCallback<ClassroomCourse>() {
            @Override
            public void done(List<ClassroomCourse> list, ParseException e) {
                if(e==null)
                {
                    readyNotifier.onLecturerReady(classroomLecturer,list);
                }
                else
                {
                    e.printStackTrace();
                    readyNotifier.onLecturerReady(classroomLecturer,null);
                }

            }
        });

    }
    private void registerStudent(final ReadyNotifier readyNotifier, final String message)
    {
        new MaterialDialog.Builder(classroomConsoleActivity).title("Classroom").cancelable(false).content(message).positiveText(R.string.register).negativeText(R.string.i_am_not_interested).onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                List<String> courseList=academicsManager.getPlainUserCourses();
                String courses="";

                for(String course : courseList)
                {
                    courses+=course+"\n";
                }
                String m1="Classroom will have to make use of the courses you have registered for this semester. The courses will be automatically be obtained.";
                new MaterialDialog.Builder(classroomConsoleActivity).title(R.string.one_more_thing).cancelable(false).negativeText(R.string.cancel).neutralText("Review courses").positiveText("Register").content(m1).onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        final SweetAlertDialog registeringDialog=dialogWizard.showSimpleProgress(getString(R.string.please_wait),getString(R.string.classroom_registering));
                        HashMap<String,Object> params=new HashMap<>();
                        params.put(Const.VERSION,getString(R.string.version_number));
                        ParseCloud.callFunctionInBackground(Const.CLASSROOM_STUDENT_REGISTRAR, params, new FunctionCallback<String>() {
                            @Override
                            public void done(String s, ParseException e) {
                                if(e==null)
                                {
                                    if(s.contains(Const.NEGATIVE))
                                    {
                                        registeringDialog.dismiss();
                                        dialogWizard.showSimpleDialog("", s.split("&")[1]);
                                    }
                                    else{

                                        ClassroomStudent.getQuery().getInBackground(s, new GetCallback<ClassroomStudent>() {
                                            @Override
                                            public void done(ClassroomStudent parseObject, ParseException e) {
                                                registeringDialog.dismiss();
                                                if(e==null)
                                                { 
                                                    parseObject.pinInBackground();
                                                    readyNotifier.onStudentReady(parseObject,null);
                                                    academicsManager.longToast("Welcome to Classroom");

                                                }

                                                else {
                                                    finish();
                                                    ErrorHandler.handleError(context,e);
                                                }
                                            }
                                        });

                                    }

                                }
                                else{
                                    registeringDialog.dismiss();
                                    ErrorHandler.handleError(classroomConsoleActivity,e);
                                }
                            }
                        });
                    }
                }).onNeutral(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //registerStudent(readyNotifier,message);
                        startActivity(new Intent(classroomConsoleActivity, CourseRegistration.class));

                    }
                }).onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                    }
                }).show();
            }
        }).onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                finish();
            }
        }).show();
    }

    private void registerLecturer(final ReadyNotifier readyNotifier, final String message)
    {

        new MaterialDialog.Builder(classroomConsoleActivity).title("Classroom").content(message).positiveText(R.string.register).negativeText(R.string.i_am_not_interested).onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                final SweetAlertDialog registrationDialog= dialogWizard.showSimpleProgress(getString(R.string.please_wait),getString(R.string.classroom_registering));
                HashMap<String,Object> params= new HashMap<>();
                params.put(Const.VERSION,getString(R.string.version_number));
                ParseCloud.callFunctionInBackground(Const.CLASSROOM_LECTURER_REGISTRAR, params, new FunctionCallback<String>() {
                    @Override
                    public void done(String s, ParseException e) {
                        if(e==null){

                            ClassroomLecturer.getQuery().getInBackground(s, new GetCallback<ClassroomLecturer>() {
                                @Override
                                public void done(ClassroomLecturer classroomLecturer, ParseException e) {
                                    registrationDialog.dismiss();
                                    if(e==null)
                                    {
                                        classroomLecturer.pinInBackground();
                                        readyNotifier.onLecturerReady(classroomLecturer,null);
                                        academicsManager.successToast("Welcome to Classroom");
                                    }
                                    else{
                                        finish();
                                        ErrorHandler.handleError(classroomConsoleActivity,e);
                                    }
                                }
                            });

                        }
                        else{
                            registrationDialog.dismiss();
                            finish();
                            ErrorHandler.handleError(classroomConsoleActivity,e);
                        }

                    }
                });
            }
        }).onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
            finish();
            }
        }).show();

    }


    protected int getCurrentDay(String day)
    {
        switch (day)
        {
            case "SUN": return 1;
            case "MON": return 2;
            case "TUE": return 3;
            case "WED": return 4;
            case "THU": return 5;
            case "FRI": return 6;
            case "SAT": return 7;
            default: return 0;
        }

    }

    protected String getDay(String day)
    {
        switch (day)
        {
            case "SUN": return "Sunday";
            case "MON": return "Monday";
            case "TUE": return "Tuesday";
            case "WED": return "Wednesday";
            case "THU": return "Thursday";
            case "FRI": return "Friday";
            case "SAT": return "Saturday";
            default: return "Monday";
        }

    }

    public boolean isCourseSelected()
    {
        if(TextUtils.isEmpty(selectedCourseCode)) {
            manager.shortToast("Please select a  course first");
            return false;
        }
        return true;
    }

    protected void materials()
    {

        startActivity(new Intent(this,CourseMaterials.class).putExtra(Const.COURSE_CODE, selectedCourseCode));
       /* Dialog.Builder builder;
        builder=new SimpleDialog.Builder(R.style.SimpleDialog){
            @Override
            protected void onBuildDone(Dialog dialog) {
                materialsProgressView=(ProgressView)dialog.findViewById(R.id.pvStudentConsole);
                materialsTextView=(TextView)dialog.findViewById(R.id.tvCourseMaterialInfo);
                materialsListView= (ListView)dialog.findViewById(R.id.lvCourseMaterials);


                ClassroomMaterial.getQuery().whereEqualTo(Const.VALID,true).whereEqualTo(Const.COURSE_CODE,selectedCourseCode).fromLocalDatastore().findInBackground(new FindCallback<ClassroomMaterial>() {
                    @Override
                    public void done(List<ClassroomMaterial> list, ParseException e) {
                        if(e==null)
                        {
                            if(list.size()==0)
                            {
                                getMaterialsOnline(materialsListView,materialsProgressView);
                            }
                            else processMaterials(materialsListView,list);
                        }
                    }
                });

            }

            @Override
            public void onNeutralActionClicked(DialogFragment fragment) {
                getMaterialsOnline(materialsListView,materialsProgressView);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                // This always works
                Intent i = new Intent(context, FilePickerActivity.class);
                // This works if you defined the intent filter
                // Intent i = new Intent(Intent.ACTION_GET_CONTENT);

                // Set these depending on your use case. These are the defaults.
                i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
                i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
                i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);

                // Configure initial directory by specifying a String.
                // You could specify a String like "/storage/emulated/0/", but that can
                // dangerous. Always use Android's API calls to get paths to the SD-card or
                // internal memory.
                i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());

                startActivityForResult(i, Const.PICK_FILE_SHARE);
            }
        };

        builder.title(selectedCourseCode+ " Materials")
                .contentView(R.layout.activity_course_materials).neutralAction(getString(R.string.refresh)).positiveAction(getString(R.string.close)).negativeAction(getString(R.string.add));
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);*/
    }
    private void processMaterials(ListView listView,List<ClassroomMaterial> classroomMaterials)
    {
        listView.setAdapter(new CourseMaterialAdapter(classroomMaterials,this));
    }

    private void getMaterialsOnline(final ListView listView, final ProgressView progressView)
    {
        progressView.start();
        ClassroomMaterial.getQuery().whereEqualTo(Const.COURSE_CODE, selectedCourseCode).whereEqualTo(Const.VALID,true).findInBackground(new FindCallback<ClassroomMaterial>() {
            @Override
            public void done(List<ClassroomMaterial> list, ParseException e) {
                if(e==null)
                {
                    progressView.stop();
                    if(list.size()>0)
                    {
                        processMaterials(listView,list);
                    }
                    else{
                        materialsTextView.setText("No relevant material was found");
                    }
                }
                else ErrorHandler.handleError(context,e);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK && requestCode==Const.PICK_FILE_SHARE && data!=null) {
            final File file = new File(data.getData().getPath());
            if (file.length() > 10485760L)
                dialogWizard.showSimpleDialog("File too large", "The Material must not be more than 10 megabytes");
            else{
                final ParseFile parseFile = new ParseFile(file);

                final SweetAlertDialog dialog =dialogWizard.showProgress(getString(R.string.please_wait),"Uploading the material",false);
                parseFile.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e==null)
                        {
                            HashMap<String,Object> params= new HashMap<>();
                            params.put(Const.FILE,parseFile);
                            params.put(Const.TITLE,file.getName());

                            ParseCloud.callFunctionInBackground("classroomSubmitMaterial", params, new FunctionCallback<String>() {
                                @Override
                                public void done(String s, ParseException e) {
                                    dialog.dismiss();
                                    if(e==null)
                                    {
                                      dialogWizard.showSimpleDialog("",s);
                                    }
                                    else ErrorHandler.handleError(context,e);
                                }
                            });
                        }
                        else{
                            dialog.dismiss();
                            ErrorHandler.handleError(context,e);
                        }
                    }
                });
            }

        }
    }

    public interface ReadyNotifier{

         void onStudentReady(ClassroomStudent classroomStudent,List<ClassroomCourse> courses);
         void onLecturerReady(ClassroomLecturer classroomLecturer,List<ClassroomCourse> courses);


    }

    public interface Clicks{
        void onClick(View v);
    }
}
