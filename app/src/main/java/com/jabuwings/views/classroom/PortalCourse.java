package com.jabuwings.views.classroom;

import com.google.gson.JsonObject;

/**
 * Created by jamesfalade on 04/10/2017.
 */

public class PortalCourse {
    JsonObject jsonObject;
    boolean isSelected;
    public PortalCourse(JsonObject object, boolean isSelected)
    {
        jsonObject=object;
        this.isSelected=isSelected;
    }
    public String getCourseTitle()
    {
        return jsonObject.get("title").getAsString();
    }
    public String getCourseCode()
    {
        return jsonObject.get("code").getAsString();
    }

    public String getCourseUnits()
    {
        return jsonObject.get("unit").getAsString();
    }

    public String getCourseStatus()
    {
        return jsonObject.get("status").getAsString();
    }

    public int getCourseNo()
    {
        return jsonObject.get("no").getAsInt();
    }
    public boolean isSelected()
    {
        return isSelected;
    }
    public void setSelected(boolean isSelected)
    {
        this.isSelected=isSelected;
    }

    @Override
    public String toString() {
        return getCourseCode() +" " +getCourseUnits() + " units";
    }
}
