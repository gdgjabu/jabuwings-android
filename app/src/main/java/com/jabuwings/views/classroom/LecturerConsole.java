package com.jabuwings.views.classroom;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.AcademicsManager;
import com.jabuwings.management.Const;
import com.jabuwings.models.ClassroomAssignment;
import com.jabuwings.models.ClassroomCourse;
import com.jabuwings.models.ClassroomLecturer;
import com.jabuwings.models.ClassroomStudent;
import com.jabuwings.models.JabuWingsPublicUser;
import com.jabuwings.models.TimeTableItem;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.MultipleUsersSearch;
import com.jabuwings.views.timetable.TimeKeeper;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.jabuwings.widgets.audiorecorder.Util;
import com.jabuwings.widgets.button.FloatingActionButton;
import com.jabuwings.widgets.button.FloatingActionsMenu;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.rey.material.app.DatePickerDialog;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.rey.material.app.TimePickerDialog;
import com.rey.material.widget.ProgressView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class LecturerConsole extends ClassroomConsoleActivity  implements OnChartValueSelectedListener,View.OnClickListener{


    @InjectView(R.id.fabLecturerConsole)
    FloatingActionsMenu fab;
    @InjectView(R.id.lecturer_console_course)
    TextView tvCourse;
    @InjectView(R.id.lecturer_console_next_lecture)
    TextView tvNextLecture;
    @InjectView(R.id.lecturer_console_pie)
    PieChart pie;
    private  ClassroomLecturer classroomLecturer;
    DialogWizard dialogWizard;
    AcademicsManager academicsManager;
    static List<ClassroomCourse> courses;
    ClassroomCourse selectedClassroomCourse;

    int day,month,year,minute,hour;
    EditText etAssignmentQuestion,etAssignmentMark,etScheduleInfo;
    boolean isAssignmentDateChosen =false, isAssignmentTimeChosen =false,isScheduleDateChosen,isScheduleTimeChosen;
    public static boolean refresh=false;
    ReadyNotifier readyNotifier;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lecturer_console);
        ButterKnife.inject(this);
        setUpToolbar(null,getString(R.string.lecturer_console));
        courses=new ArrayList<>();
        dialogWizard=new DialogWizard(this);
        academicsManager=new AcademicsManager(this);

         readyNotifier = new ReadyNotifier() {

            @Override
            public void onStudentReady(ClassroomStudent classroomStudent, List<ClassroomCourse> courses) {

            }

            @Override
            public void onLecturerReady(ClassroomLecturer parseObject, List<ClassroomCourse> c) {
                classroomLecturer = parseObject;
                courses=c;

                setUpClassroom();
            }





        };
        initialiseClassroom(LecturerConsole.this,readyNotifier,1);
        initFabs();









    }


    @Override
    protected void onResume() {
        if(refresh)
        {
            initialiseClassroom(LecturerConsole.this,readyNotifier,1);
        }
        refresh=false;
        super.onResume();

    }

    private void initFabs()
    {

        FloatingActionButton fabSendMessage= new FloatingActionButton(this);
        fabSendMessage.setColorNormal(R.color.light_red);
        fabSendMessage.setSize(FloatingActionButton.SIZE_MINI);
        fabSendMessage.setTitle("Send Message");
        fabSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.collapse();
                if(isCourseSelected())
                    sendMessage();

            }
        });

        FloatingActionButton fabSubmitMaterial= new FloatingActionButton(this);
        fabSubmitMaterial.setColorNormal(R.color.light_red);
        fabSubmitMaterial.setSize(FloatingActionButton.SIZE_MINI);
        fabSubmitMaterial.setTitle("Course Materials");
        fabSubmitMaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.collapse();
                if(isCourseSelected())
                    materials();

            }
        });

        FloatingActionButton fabCheckAssignments= new FloatingActionButton(this);
        fabCheckAssignments.setColorNormal(R.color.light_red);
        fabCheckAssignments.setSize(FloatingActionButton.SIZE_MINI);
        fabCheckAssignments.setTitle("Assignments");
        fabCheckAssignments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.collapse();
                if(isCourseSelected())
                assignments();

            }
        });

        FloatingActionButton fabScheduleClass= new FloatingActionButton(this);
        fabScheduleClass.setColorNormal(R.color.light_red);
        fabScheduleClass.setSize(FloatingActionButton.SIZE_MINI);
        fabScheduleClass.setTitle("Schedule class or test");
        fabScheduleClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.collapse();
                if(isCourseSelected())
                    scheduleClass();

            }
        });

        FloatingActionButton fabViewStudents= new FloatingActionButton(this);
        fabViewStudents.setColorNormal(R.color.light_red);
        fabViewStudents.setSize(FloatingActionButton.SIZE_MINI);
        fabViewStudents.setTitle("View Students");
        fabViewStudents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.collapse();
                if(isCourseSelected())
                students();

            }
        });

        FloatingActionButton fabCourses= new FloatingActionButton(this);
        fabCourses.setColorNormal(R.color.light_red);
        fabCourses.setSize(FloatingActionButton.SIZE_MINI);
        fabCourses.setTitle("Manage Courses");
        fabCourses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.collapse();
                course();
            }
        });

        fab.addButton(fabSendMessage);
        fab.addButton(fabSubmitMaterial);
        fab.addButton(fabCheckAssignments);
        fab.addButton(fabScheduleClass);
        fab.addButton(fabViewStudents);
        fab.addButton(fabCourses);
    }


  /*  private ClassroomCourse getCourse(String courseCode)
    {
        for(ClassroomCourse course: courses)
        {
            if(course.getCourseCode().equals(courseCode))
            return course;
        }
        return null;
    }*/

    private void scheduleClass()
    {
        Dialog.Builder builder;
        builder = new SimpleDialog.Builder(R.style.SimpleDialog){
            @Override
            protected void onBuildDone(final Dialog dialog) {

                etScheduleInfo=(EditText)dialog.findViewById(R.id.etLecturerConsoleAdditional);
                final Button btDate=(Button)dialog.findViewById(R.id.btLecturerConsoleScheduleDate);
                final Button btTime=(Button)dialog.findViewById(R.id.btLecturerConsoleSchedulteTime);

                btTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Dialog.Builder builder;
                        isScheduleTimeChosen =true;
                        builder = new TimePickerDialog.Builder(time.getHour(), time.getMinute()) {
                            @Override
                            public void onPositiveActionClicked(DialogFragment fragment) {
                                TimePickerDialog dialog = (TimePickerDialog) fragment.getDialog();
                                //btTime.setText(dialog.getFormattedTime(SimpleDateFormat.getDateInstance()));

                                hour=dialog.getHour();
                                minute=dialog.getMinute();
                                btTime.setText(String.valueOf(hour)+":"+String.valueOf(minute));


                                super.onPositiveActionClicked(fragment);
                            }

                            @Override
                            public void onNegativeActionClicked(DialogFragment fragment) {
                                super.onNegativeActionClicked(fragment);
                            }
                        };

                        builder.positiveAction("OK")
                                .title("Choose Time")
                                .negativeAction("CANCEL");
                        DialogFragment fragment = DialogFragment.newInstance(builder);
                        fragment.show(getSupportFragmentManager(), null);
                    }
                });
                btDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                        Dialog.Builder builder;
                        isAssignmentDateChosen =true;
                        builder = new DatePickerDialog.Builder(){
                            @Override
                            public void onPositiveActionClicked(DialogFragment fragment) {
                                DatePickerDialog dialog1 = (DatePickerDialog)fragment.getDialog();
                                String date = dialog1.getFormattedDate(sdf);btDate.setText(date);
                                DatePickerDialog dialog = (DatePickerDialog)fragment.getDialog();
                                month=dialog.getMonth();
                                day=dialog.getDay();
                                year=dialog.getYear();






                                super.onPositiveActionClicked(fragment);
                            }

                            @Override
                            public void onNegativeActionClicked(DialogFragment fragment) {

                                super.onNegativeActionClicked(fragment);
                            }
                        }.dateRange(time.getDay(),time.getMonth(),time.getYear(),31,12,time.getYear());

                        builder.positiveAction("OK")
                                .negativeAction("CANCEL");
                        DialogFragment fragment= DialogFragment.newInstance(builder);
                        fragment.show(getSupportFragmentManager(),null);
                    }
                });







            }

            @Override
            public void onPositiveActionClicked(final DialogFragment fragment) {

                String info = etScheduleInfo.getText().toString();
                

              

                if(TextUtils.isEmpty(info))
                {
                    manager.errorToast("Question cannot be empty");
                }

                else if(!isScheduleTimeChosen)
                {
                    manager.errorToast("Please select the time");
                }

                else if(!isScheduleDateChosen)
                {
                    manager.errorToast("Please select the date");
                }
                else{
                    GregorianCalendar calendar = new GregorianCalendar(year,month,day,hour,minute);
                    Date date=calendar.getTime();
                    final SweetAlertDialog newScheduleProgress= dialogWizard.showProgress(getString(R.string.please_wait),"Scheduling your class",false);

                    HashMap<String,Object> params= manager.getDefaultCloudParams();
                    params.put("lecturer",classroomLecturer.getObjectId());
                    params.put(Const.COURSE,selectedClassroomCourse.getObjectId());
                    params.put(Const.COURSE_CODE, selectedCourseCode);
                    params.put(Const.DATE,date);
                    params.put(Const.MSG,info);
                    ParseCloud.callFunctionInBackground("classroomSchedule", params, new FunctionCallback<String>() {
                        @Override
                        public void done(String s, ParseException e) {
                            fragment.dismiss();
                            newScheduleProgress.dismiss();
                            if(e==null)
                            {
                                if(s!=null)
                                {
                                    classroomManager.addNotification("Classroom Schedule",s);
                                    dialogWizard.showSimpleDialog("",s);
                                }
                            }
                            else ErrorHandler.handleError(context,e);
                        }
                    });
                }
            }


        };

       
        
        builder.title("Schedule "+ selectedCourseCode +" Class/Test")
                .contentView(R.layout.lecturer_console_schedule).negativeAction(getString(R.string.close)).positiveAction("Add");
        DialogFragment rateFragment = DialogFragment.newInstance(builder);
        rateFragment.setCancelable(false);
        rateFragment.show(getSupportFragmentManager(), null);
    }
    private void sendMessage()
    {
        new MaterialDialog.Builder(LecturerConsole.this).title("Send Message").content("Send a message to all the students registered for this course").inputType(InputType.TYPE_CLASS_TEXT)
                .inputRange(1, 100)
                .positiveText(getString(R.string.send))
                .negativeText(getString(R.string.cancel)).input("Your message", null, false, new MaterialDialog.InputCallback() {
            @Override
            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                
                HashMap<String,Object> params= manager.getDefaultCloudParams();
                params.put(Const.MSG,input.toString());
                params.put(Const.COURSE,selectedClassroomCourse.getObjectId());
                params.put(Const.COURSE_CODE, selectedCourseCode);
                params.put("lecturer",classroomLecturer.getObjectId());


                
                ParseCloud.callFunctionInBackground("classroomSendMessage", params, new FunctionCallback<String>() {
                    @Override
                    public void done(String s, ParseException e) {
                        if(e==null)
                        {
                            manager.successToast(s);
                            
                        }
                        else {
                            if(e.getCode()==ParseException.SCRIPT_ERROR)
                            {
                                manager.errorToast(e.getMessage());
                            }
                            else
                                ErrorHandler.handleError(context,e);
                        }
                    }
                });
                
                
            }
        }).show();

    }
    private void students()
    {

        List<String> students = selectedClassroomCourse._getStudents();
        if(students.size()>0)
        {
            MultipleUsersSearch.query= JabuWingsPublicUser.getQuery()
                    .whereContainedIn(Const.USERNAME,students);
            MultipleUsersSearch.title= selectedCourseCode +" Students";
            MultipleUsersSearch.error="No student has registered for this course.";
            startActivity(new Intent(LecturerConsole.this, MultipleUsersSearch.class));
        }
        else{
            manager.errorToast("No student has registered for this course");
        }


    }
    private void course()
    {
        String s = "";
        for(ClassroomCourse course : courses)
        {
            s+=course.getCourseCode()+"\n";
        }

        new MaterialDialog.Builder(LecturerConsole.this)
                .title("Your courses")
                .content(s)
                .positiveText(R.string.add)
                .neutralText(R.string.remove)
                .negativeText(R.string.close)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        startActivity(new Intent(LecturerConsole.this,LecturerRegisterCourse.class).putExtra(Const.ID,classroomLecturer.getObjectId()));
                    }
                })
                .onNeutral(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();    }
    private void assignments()
    {

        Dialog.Builder builder;
        builder = new SimpleDialog.Builder(R.style.SimpleDialog){
            @Override
            protected void onBuildDone(Dialog dialog) {
                dialog.layoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
                final ListView lvAssignments = (ListView)dialog.findViewById(R.id.lvStudentConsoleAssignments);

                final ProgressView progressView =(ProgressView)dialog.findViewById(R.id.pvStudentConsole);
                final TextView text=(TextView)dialog.findViewById(R.id.tvStudentConsoleAssignments);
                progressView.start();
                ClassroomAssignment.getQuery().whereEqualTo(Const.COURSE_CODE, selectedCourseCode).whereEqualTo(Const.VALID,true).findInBackground(new FindCallback<ClassroomAssignment>() {
                    @Override
                    public void done(List<ClassroomAssignment> list, ParseException e) {
                        progressView.stop();
                        progressView.setVisibility(View.GONE);
                        if(e==null)
                        {
                            if(list.size()==0){
                                text.setVisibility(View.VISIBLE);
                                text.setText("You are yet to give any assignment.");
                            }
                            else{
                                lvAssignments.setAdapter(new AssignmentsAdapter(context,list));
                            }
                        }
                       else ErrorHandler.handleError(context,e);
                    }
                });





            }

            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                fragment.dismiss();
                newAssignment();
            }


        };

        builder.title(selectedCourseCode + " Assignments")
                .contentView(R.layout.student_console_assignments).negativeAction(getString(R.string.close)).positiveAction("Add");
        DialogFragment rateFragment = DialogFragment.newInstance(builder);
        rateFragment.setCancelable(false);
        rateFragment.show(getSupportFragmentManager(), null);

    }

    public void newAssignment()
    {

        Dialog.Builder builder;
        builder = new SimpleDialog.Builder(R.style.SimpleDialog){
            @Override
            protected void onBuildDone(final Dialog dialog) {

                 etAssignmentQuestion=(EditText)dialog.findViewById(R.id.etLecturerConsoleAssignment);
                 etAssignmentMark=(EditText)dialog.findViewById(R.id.etLecturerConsoleMark);

                final Button btDate=(Button)dialog.findViewById(R.id.btLecturerConsoleAssignmentDate);
                final Button btTime=(Button)dialog.findViewById(R.id.btLecturerConsoleAssignmentTime);

                btTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Dialog.Builder builder;
                        isAssignmentTimeChosen =true;
                        builder = new TimePickerDialog.Builder(time.getHour(), time.getMinute()) {
                            @Override
                            public void onPositiveActionClicked(DialogFragment fragment) {
                                TimePickerDialog dialog = (TimePickerDialog) fragment.getDialog();
                                //btTime.setText(dialog.getFormattedTime(SimpleDateFormat.getDateInstance()));

                                hour=dialog.getHour();
                                minute=dialog.getMinute();
                                btTime.setText(String.valueOf(hour)+":"+String.valueOf(minute));


                                super.onPositiveActionClicked(fragment);
                            }

                            @Override
                            public void onNegativeActionClicked(DialogFragment fragment) {
                                super.onNegativeActionClicked(fragment);
                            }
                        };

                        builder.positiveAction("OK")
                                .title("Choose Time")
                                .negativeAction("CANCEL");
                        DialogFragment fragment = DialogFragment.newInstance(builder);
                        fragment.show(getSupportFragmentManager(), null);
                    }
                });
                btDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                        Dialog.Builder builder;
                        isAssignmentDateChosen =true;
                        builder = new DatePickerDialog.Builder(){
                            @Override
                            public void onPositiveActionClicked(DialogFragment fragment) {
                                DatePickerDialog dialog1 = (DatePickerDialog)fragment.getDialog();
                                String date = dialog1.getFormattedDate(sdf);btDate.setText(date);
                                DatePickerDialog dialog = (DatePickerDialog)fragment.getDialog();
                                month=dialog.getMonth();
                                day=dialog.getDay();
                                year=dialog.getYear();






                                super.onPositiveActionClicked(fragment);
                            }

                            @Override
                            public void onNegativeActionClicked(DialogFragment fragment) {

                                super.onNegativeActionClicked(fragment);
                            }
                        }.dateRange(time.getDay(),time.getMonth(),time.getYear(),31,12,time.getYear());

                        builder.positiveAction("OK")
                                .negativeAction("CANCEL");
                        DialogFragment fragment= DialogFragment.newInstance(builder);
                        fragment.show(getSupportFragmentManager(),null);
                    }
                });







            }

            @Override
            public void onPositiveActionClicked(final DialogFragment fragment) {

                String question = etAssignmentQuestion.getText().toString();
                String marks=etAssignmentMark.getText().toString();
                int marksObtainable=0;

                if(!TextUtils.isEmpty(marks))
                {
                    marksObtainable=Integer.valueOf(marks);
                }

                if(TextUtils.isEmpty(question))
                {
                    manager.errorToast("Question cannot be empty");
                }
                else if(marksObtainable==0)
                {
                    manager.errorToast("Please provide the maximum marks obtainable to the student");
                }


                else if(!isAssignmentTimeChosen)
                {
                    manager.errorToast("Please select the time");
                }

                else if(!isAssignmentDateChosen)
                {
                    manager.errorToast("Please select the date");
                }
                else{
                    GregorianCalendar calendar = new GregorianCalendar(year,month,day,hour,minute);
                    Date date=calendar.getTime();
                    final SweetAlertDialog newAssignmentProgress= dialogWizard.showProgress(getString(R.string.please_wait),"Creating the assignment",false);

                    HashMap<String,Object> params= new HashMap<>();
                    params.put(Const.VERSION,manager.getVersion());
                    params.put("lecturer",classroomLecturer.getObjectId());
                    params.put(Const.COURSE,selectedClassroomCourse.getObjectId());
                    params.put(Const.COURSE_CODE, selectedCourseCode);
                    params.put("marks",marksObtainable);
                    params.put(Const.DATE,date);
                    params.put("question",question);
                    ParseCloud.callFunctionInBackground("classroomNewAssignment", params, new FunctionCallback<String>() {
                        @Override
                        public void done(String s, ParseException e) {
                            fragment.dismiss();
                            newAssignmentProgress.dismiss();
                            if(e==null)
                            {
                                if(s!=null)
                                {
                                  classroomManager.addNotification("Classroom Assignment",s);
                                  dialogWizard.showSimpleDialog("",s);
                                }
                            }
                            else ErrorHandler.handleError(context,e);
                        }
                    });
                }
            }


        };

        builder.title(selectedCourseCode + " New Assignment")
                .contentView(R.layout.lecturer_console_new_assignment).negativeAction(getString(R.string.close)).positiveAction("Submit");
        DialogFragment rateFragment = DialogFragment.newInstance(builder);
        rateFragment.setCancelable(false);

        rateFragment.show(getSupportFragmentManager(), null);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btLecturerAssignmentReview)
        startActivity(new Intent(context,ReviewAssignment.class).putExtra("id",(String)v.getTag()));
    }

    public class AssignmentsAdapter extends BaseAdapter{

        LayoutInflater inflater;
        Context context;
        List<ClassroomAssignment> assignments;
        public AssignmentsAdapter(Context context,List<ClassroomAssignment>list)

        {
            this.context=context;
            inflater = LayoutInflater.from(context);
            assignments=list;

        }
        @Override
        public int getCount() {
            return assignments.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.lecturer_console_assignments_item, null);
                holder = new ViewHolder();
                holder.tvAssignmentText = (TextView) convertView.findViewById(R.id.tvLecturerAssignmentText);
                holder.tvAssignmentNo = (TextView) convertView.findViewById(R.id.tvLecturerAssignmentNo);
                holder.btReview = (Button) convertView.findViewById(R.id.btLecturerAssignmentReview);
                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ClassroomAssignment assignment = assignments.get(position);
            if (assignment != null) {
                String content = assignment.getContent();
                holder.tvAssignmentText.setText(content);
                holder.btReview.setTag(assignment);
                holder.btReview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReviewAssignment.assignment=(ClassroomAssignment) v.getTag();;
                        startActivity(new Intent(context,ReviewAssignment.class).putExtra("id",classroomLecturer.getObjectId()));

                    }
                });

                int noOfSubmitted=assignment.getNoOfSubmitted();

                String no="";
                if(noOfSubmitted==0)
                {
                    no="No student has submitted this assignment yet.";
                }
                else{
                    no= noOfSubmitted==1? "A student has submitted the assignment": noOfSubmitted+ " students have submitted the assignment";
                }
                holder.tvAssignmentNo.setText(no);

            }




            return  convertView;
        }
    }

    public class  ViewHolder {
        TextView tvAssignmentText,tvAssignmentNo;
        Button btReview;

    }

    private void setUpClassroom()
    {

        if(courses!=null && courses.size()>0)
        {
            setUpPie();
        }

        else{
            new MaterialDialog.Builder(LecturerConsole.this).title("No courses").content("You are not registered for any course yet. Register now. If you have registered a course before, please choose 'check again'.").positiveText(R.string.register).negativeText(R.string.cancel).neutralText(R.string.check_again).cancelable(false).onPositive(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    startActivity(new Intent(LecturerConsole.this,LecturerRegisterCourse.class).putExtra(Const.ID,classroomLecturer.getObjectId()));


                }
            }).onNegative(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    finish();
                }
            }).onNeutral(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    final SweetAlertDialog checkDialog=dialogWizard.showSimpleProgress(getString(R.string.please_wait),getString(R.string.lets_check_something));
                    classroomLecturer.getCourses().getQuery().findInBackground(new FindCallback<ClassroomCourse>() {
                        @Override
                        public void done(List<ClassroomCourse> list, ParseException e) {
                            checkDialog.dismiss();
                            if(e==null)
                            {
                                if(list.size()>0) {
                                    //manager.shortToast("List greater than zero");
                                    pinCourses(list, true);
                                    setUpClassroom();
                                }
                                else{
                                    manager.errorToast("You have not registered any course");
                                    setUpClassroom();
                                }
                            }

                            else{
                                ErrorHandler.handleError(context,e);
                                setUpClassroom();
                            }
                        }
                    });

                }
            }).show();
        }




    }

    private void setUpPie() {

        pie.setUsePercentValues(true);
        Description description = new Description();
        description.setText("Choose a course and click the button below.");
        pie.setDescription(description);
        pie.setExtraOffsets(5, 10, 5, 5);

        pie.setDragDecelerationFrictionCoef(0.95f);


        pie.setCenterText("Your Courses");

        pie.setDrawHoleEnabled(true);
        pie.setHoleColor(Color.WHITE);

        pie.setTransparentCircleColor(Color.WHITE);
        pie.setTransparentCircleAlpha(110);

        pie.setHoleRadius(58f);
        pie.setTransparentCircleRadius(61f);

        pie.setDrawCenterText(true);

        pie.setRotationAngle(0);
        // enable rotation of the chart by touch
        pie.setRotationEnabled(true);
        pie.setHighlightPerTapEnabled(true);

        // pie.setUnit(" €");
        // pie.setDrawUnitsInChart(true);

        // add a selection listener
        pie.setOnChartValueSelectedListener(this);



        pie.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // pie.spin(2000, 0, 360);



        Legend l = pie.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        // entry label styling
        pie.setEntryLabelColor(Color.WHITE);
        //pie.setEntryLabelTypeface(mTfRegular);
        pie.setEntryLabelTextSize(12f);

        int size = courses.size();
        float entryValue= 100/size;
        List<PieEntry> pieEntries = new ArrayList<>();

        for(ClassroomCourse course : courses)
        {
            pieEntries.add(new PieEntry(entryValue,course.getCourseCode()));

        }

        PieDataSet dataSet = new PieDataSet(pieEntries,"Courses");


        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(Util.getDarkerColor(c));

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(Util.getDarkerColor(c));

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(Util.getDarkerColor(c));

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(Util.getDarkerColor(c));

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(Util.getDarkerColor(c));

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        //data.setValueTypeface(mTfLight);
        pie.setData(data);

        // undo all highlights
        pie.highlightValues(null);

        pie.invalidate();
    }

    @Override
    public void onNothingSelected() {
        tvCourse.setText("Select a course to begin");
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        selectedClassroomCourse=courses.get(Float.valueOf(h.getX()).intValue());
        selectedCourseCode =selectedClassroomCourse.getCourseCode();
        tvCourse.setText(selectedCourseCode);
        List<TimeTableItem> allCourses= TimeKeeper.getNextLectures(this, selectedCourseCode);

        String day = new Time().getTimetableDay();
        int currentDay= Calendar.getInstance().get(Calendar.DAY_OF_WEEK);



        if(allCourses!=null)
            for(TimeTableItem item : allCourses)
            {
                if(item.getDay().equals(day)&& item.getEndingHour() < new Time().getHour())
                {
                    setNextLecture(item);
                    break;
                }
                else{
                    if(getCurrentDay(item.getDay()) > currentDay)
                    {
                        setNextLecture(item);
                        break;
                    }
                    else{
                        setNextLecture(item);
                    }

                }

            }
        else{
            tvNextLecture.setText(null);
        }

    }
    private void setNextLecture(TimeTableItem s)
    {
        tvNextLecture.setText("Your next lecture is "+getDay(s.getDay())+" by "+s.getReadableStartHour());
    }
    private void pinCourses(List<ClassroomCourse> courseList,boolean store)
    {
        for(ClassroomCourse course: courseList)
        {
            courses.add(course);

        }
        if(store) {
        try {
            ParseObject.pinAll(courseList);
        }
        catch (ParseException e){e.printStackTrace();}
    };
    }





}
