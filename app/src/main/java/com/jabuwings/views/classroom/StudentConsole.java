package com.jabuwings.views.classroom;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.AcademicsManager;
import com.jabuwings.management.Const;
import com.jabuwings.models.ClassroomAssignment;
import com.jabuwings.models.ClassroomCourse;
import com.jabuwings.models.ClassroomLecturer;
import com.jabuwings.models.ClassroomStudent;
import com.jabuwings.models.TimeTableItem;
import com.jabuwings.models.ViewPagerAdapter;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.timetable.TimeKeeper;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.jabuwings.widgets.audiorecorder.Util;
import com.jabuwings.widgets.button.FloatingActionButton;
import com.jabuwings.widgets.button.FloatingActionsMenu;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.rey.material.widget.ProgressView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class StudentConsole extends ClassroomConsoleActivity implements OnChartValueSelectedListener {


    @InjectView(R.id.fabStudentConsole)
    FloatingActionsMenu fab;
    ClassroomStudent classroomStudent;
    DialogWizard dialogWizard;
    AcademicsManager academicsManager;

    @InjectView(R.id.student_console_pie)
    PieChart pie;
    List<String> courses=new ArrayList<>();
    List<Course> studentCourses=new ArrayList<>();
    @InjectView(R.id.student_console_course)
    TextView tvCourse;
    @InjectView(R.id.student_console_next_lecture)
    TextView tvNextLecture;
    List<ClassroomCourse> classroomCourses;

    Course selectedCourse;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_console);
        ButterKnife.inject(this);
        setUpToolbar(null,getString(R.string.student_console));
        setUpTextViews(tvCourse,tvNextLecture);
        academicsManager=new AcademicsManager(this);



        dialogWizard=new DialogWizard(this);


        ClassroomConsoleActivity.ReadyNotifier readyNotifier = new ClassroomConsoleActivity.ReadyNotifier() {
            @Override
            public void onStudentReady(final ClassroomStudent parseObject, List<ClassroomCourse> courses) {
                classroomStudent =parseObject;
                final String localCourses = classroomStudent.getPortalCourses().toString();
                ParseCloud.callFunctionInBackground(Const.UPDATE_CLASSROOM_STUDENT, academicsManager.getDefaultCloudParams(), new FunctionCallback<String>() {
                    @Override
                    public void done(String object, ParseException e) {
                            if(e==null)
                            {
                                parseObject.fetchInBackground(new GetCallback<ClassroomStudent>() {
                                    @Override
                                    public void done(ClassroomStudent object, ParseException e) {
                                        if(e==null){
                                            if(!localCourses.equals(object.getPortalCourses().toString()))
                                            {
                                                object.pinInBackground();
                                                classroomStudent = object;
                                                academicsManager.shortToast("Classroom Profile Updated");
                                                setUpClassroom();
                                            }
                                        }
                                        else e.printStackTrace();

                                    }
                                });
                            }
                    }
                });
                
                classroomCourses=courses;
                setUpClassroom();
            }

            @Override
            public void onLecturerReady(ClassroomLecturer classroomLecturer, List<ClassroomCourse> courses) {

            }



        };
        initialiseClassroom(StudentConsole.this,readyNotifier,0);

    }
    private void setUpPie() {


        pie.setUsePercentValues(false);
        Description description = new Description();
        description.setText("Choose a course and click the button below.");
        pie.setDescription(description);
        pie.setExtraOffsets(5, 10, 5, 5);

        pie.setDragDecelerationFrictionCoef(0.95f);


        pie.setCenterText("Your Courses");

        pie.setDrawHoleEnabled(true);
        pie.setHoleColor(Color.WHITE);

        pie.setTransparentCircleColor(Color.WHITE);
        pie.setTransparentCircleAlpha(110);

        pie.setHoleRadius(58f);
        pie.setTransparentCircleRadius(61f);

        pie.setDrawCenterText(true);

        pie.setRotationAngle(0);
        // enable rotation of the chart by touch
        pie.setRotationEnabled(true);
        pie.setHighlightPerTapEnabled(true);

        // pie.setUnit(" €");
        // pie.setDrawUnitsInChart(true);

        // add a selection listener
        pie.setOnChartValueSelectedListener(this);



        pie.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // pie.spin(2000, 0, 360);



        Legend l = pie.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        // entry label styling
        pie.setEntryLabelColor(Color.WHITE);
        //pie.setEntryLabelTypeface(mTfRegular);
        pie.setEntryLabelTextSize(12f);
        courses= academicsManager.getUserCourses();
        studentCourses = academicsManager.getStudentCourses(classroomStudent);
        
        int size = courses.size();
        List<PieEntry> pieEntries = new ArrayList<>();

        float totalUnits=0;
        for(Course course : studentCourses)
        {
            totalUnits+=course.getUnit();
        }
        academicsManager.log("units "+totalUnits);
        float totalEntry= 0;
        for(Course course : studentCourses)
        {
            float entryValue = course.getUnit()/totalUnits*100;
            float cal = course.getUnit()/totalUnits;
            academicsManager.log(":)");
            academicsManager.log(""+course.getUnit());
            academicsManager.log(String.valueOf(cal));
            academicsManager.log(""+course.getUnit()/totalUnits);
            academicsManager.log(""+course.getUnit()/totalUnits*100);
            totalEntry+=entryValue;
            academicsManager.log("entryValue "+entryValue);
            if(course.getUnit()>0)
            pieEntries.add(new PieEntry(entryValue,course.getCode()));

        }

        academicsManager.log("totalEntry "+totalEntry);
        PieDataSet dataSet = new PieDataSet(pieEntries,"Courses");


        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(Util.getDarkerColor(c));

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(Util.getDarkerColor(c));

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(Util.getDarkerColor(c));

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(Util.getDarkerColor(c));

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(Util.getDarkerColor(c));

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        //data.setValueTypeface(mTfLight);
        pie.setData(data);

        // undo all highlights
        pie.highlightValues(null);

        pie.invalidate();
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        selectedCourse = studentCourses.get(Float.valueOf(h.getX()).intValue());
        selectedCourseCode = selectedCourse.getCode();
        tvCourse.setText(selectedCourseCode +": "+selectedCourse.getTitle());
        List<TimeTableItem> allCourses= TimeKeeper.getNextLectures(this, selectedCourseCode);

        String day = new Time().getTimetableDay();
        int currentDay=Calendar.getInstance().get(Calendar.DAY_OF_WEEK);



        if(allCourses!=null)
        {
            for(TimeTableItem item : allCourses)
            {
                if(item.getDay().equals(day)&& item.getEndingHour() < new Time().getHour())
                {
                    setNextLecture(item);
                    break;
                }
                else{
                    if(getCurrentDay(item.getDay()) > currentDay)
                    {
                        setNextLecture(item);
                        break;
                    }
                    else{
                        setNextLecture(item);
                    }

                }

            }
        if(allCourses.size()<=0) tvNextLecture.setText(null);
        }
        else{  
            tvNextLecture.setText(null);
        }



    }


    private void setNextLecture(TimeTableItem s)
    {
        tvNextLecture.setText("Your next lecture is "+getDay(s.getDay())+" by "+s.getReadableStartHour());
    }

    @Override
    public void onNothingSelected() {
        tvCourse.setText("Select a course to begin");

    }

    private SpannableString generateCenterSpannableText() {

        SpannableString s = new SpannableString("MPAndroidChart\ndeveloped by Philipp Jahoda");
        s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
        return s;
    }

    private boolean isCourseRegistered(String courseCode)
    {
        if(classroomCourses!=null)
        for(ClassroomCourse classroomCourse: classroomCourses)
        {
            if(classroomCourse.getCourseCode().equals(courseCode))
                return true;
        }
        return false;

    }

    private void checkCourse(final String courseCode)
    {

            final SweetAlertDialog dialog=dialogWizard.showProgress(getString(R.string.please_wait),"Checking if a lecturer has registered the course",true);

            ClassroomCourse.getQuery().whereEqualTo(Const.VALID,true).whereEqualTo(Const.COURSE_CODE,courseCode).findInBackground(new FindCallback<ClassroomCourse>() {
                @Override
                public void done(List<ClassroomCourse> list, ParseException e) {
                    dialog.dismiss();
                    if(e==null)
                    {
                        if(list.size()==0)
                        {

                            manager.errorToast("No lecturer has registered for the course");
                        }
                        else{
                            if(list.size()==1) {

                                final ClassroomCourse classroomCourse = list.get(0);
                                new SweetAlertDialog(StudentConsole.this,SweetAlertDialog.NORMAL_TYPE).setTitleText(classroomCourse.getCourseCode()).setContentText("Please verify: \nLecturer's name :"+classroomCourse.getLecturerName()+"\nCourse title: "+classroomCourse.getCourseTitle()+"\nCourse Units: "+classroomCourse.getCourseUnits()).setConfirmText(getString(R.string.confirm)).setCancelText(getString(R.string.cancel)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        final SweetAlertDialog fDialog= dialogWizard.showProgress(getString(R.string.please_wait),"Finalising Registration",true);
                                        HashMap<String,Object> params = new HashMap<String, Object>();
                                        params.put("courseId",classroomCourse.getObjectId());
                                        params.put("classroomId",classroomStudent.getObjectId());

                                        ParseCloud.callFunctionInBackground("classroomStudentCourseRegistrar", params, new FunctionCallback<String>() {
                                            @Override
                                            public void done(String s, ParseException e) {
                                                fDialog.dismiss();
                                                if(e==null)
                                                {
                                                    try {
                                                        classroomCourse.pin();
                                                    } catch (ParseException e1) {
                                                    }
                                                    classroomCourses.add(classroomCourse);

                                                    manager.successToast(courseCode+" has been added to your Classroom Courses");
                                                }
                                                else ErrorHandler.handleError(context,e);

                                            }
                                        });

                                    }
                                }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                    }
                                }).show();

                            }
                            else{
                                manager.errorToast("Multiple lecturers not yet supported");
                                //TODO handle multiple course codes
                            }

                        }
                    }
                    else ErrorHandler.handleError(context,e);
                }
            });



    }
    private void setUpClassroom()
    {
        //setupViewPager(pager); tabs.setupWithViewPager(pager);
        FloatingActionButton fabAssignments= new FloatingActionButton(this);
        fabAssignments.setTitle("Assignments");
        fabAssignments.setColorNormal(R.color.cpb_red);
        fabAssignments.setSize(FloatingActionButton.SIZE_MINI);
        fabAssignments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.collapse();
                if(isCourseSelected())
                {
                    if(isCourseRegistered(selectedCourseCode))
                    {
                        assignments();
                    }
                    else {
                        checkCourse(selectedCourseCode);
                    }
                }


            }
        });
        FloatingActionButton fabNotifications= new FloatingActionButton(this);
        fabNotifications.setTitle("Notifications");
        fabNotifications.setSize(FloatingActionButton.SIZE_MINI);
        fabNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.collapse();
                if(isCourseSelected())
                    if(isCourseRegistered(selectedCourseCode))
                        notifications();
                    else
                        checkCourse(selectedCourseCode);

            }
        });

        FloatingActionButton fabMaterials= new FloatingActionButton(this);
        fabMaterials.setTitle("Course Materials");
        fabMaterials.setSize(FloatingActionButton.SIZE_MINI);
        fabMaterials.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.collapse();
                if(isCourseSelected())
                materials();
            }
        });

        FloatingActionButton fabClasses= new FloatingActionButton(this);
        fabClasses.setTitle("Classes");
        fabClasses.setSize(FloatingActionButton.SIZE_MINI);
        fabClasses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.collapse();
                if(isCourseSelected())
                classes();
            }
        });

        FloatingActionButton fabCourses= new FloatingActionButton(this);
        fabCourses.setTitle("Manage Courses");
        fabCourses.setSize(FloatingActionButton.SIZE_MINI);
        fabCourses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.collapse();
               courses();
            }
        });

        fab.addButton(fabAssignments);
        fab.addButton(fabNotifications);
        fab.addButton(fabMaterials);
        fab.addButton(fabClasses);
        fab.addButton(fabCourses);
        setUpPie();
    }

    private void courses()
    {
        List<String> courses = academicsManager.getUserCourses();
        String coursesContent="";
        String[] metaTag= courses.get(courses.size()-1).split("_");
        String session,semester;
        if(metaTag.length==2)
        {
            session=metaTag[0];
            semester= metaTag[1].equals("1")? getString(R.string.first_semester) : getString(R.string.second_semester);
            coursesContent+=session+"\n";
            coursesContent+=semester+"\n";

        }
        int position=0;
        int metaTagPosition=courses.size()-1;
        for(String course : courses)
        {
            if (position!=metaTagPosition)
            {
                coursesContent+=course+"\n";
                position++;
            }

        }
        new MaterialDialog.Builder(StudentConsole.this)
                .title("Your courses")
                .content(coursesContent)
                .positiveText(R.string.okay)
                .negativeText("Change courses")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        startActivity(new Intent(StudentConsole.this, CourseRegistration.class));
                    }
                })
                .show();
    }
    private void assignments()
    {
        Dialog.Builder builder;
        builder = new SimpleDialog.Builder(R.style.SimpleDialog){
            @Override
            protected void onBuildDone(Dialog dialog) {
                dialog.layoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
                final ListView lvAssignments = (ListView)dialog.findViewById(R.id.lvStudentConsoleAssignments);
                final ProgressView progressView =(ProgressView)dialog.findViewById(R.id.pvStudentConsole);
                final TextView text=(TextView)dialog.findViewById(R.id.tvStudentConsoleAssignments);
                progressView.start();
                ClassroomAssignment.getQuery().whereEqualTo(Const.COURSE_CODE, selectedCourseCode).whereEqualTo(Const.VALID,true).findInBackground(new FindCallback<ClassroomAssignment>() {
                    @Override
                    public void done(List<ClassroomAssignment> list, ParseException e) {
                        progressView.stop();
                        progressView.setVisibility(View.GONE);
                        if(e==null)
                        {
                            if(list.size()==0){
                                text.setVisibility(View.VISIBLE);
                                text.setText("Apparently, you don't have any assignment from your lecturer");
                            }
                            else{
                                lvAssignments.setAdapter(new AssignmentsAdapter(context,list));
                            }
                        }
                        else{
                            ErrorHandler.handleError(context,e);
                        }
                    }
                });





            }
        };

        builder.title(selectedCourseCode + " Assignments")
                .contentView(R.layout.student_console_assignments).positiveAction(getString(R.string.close));
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);

    }
    public class ClassroomNotification{
        private String content;
        private Date date;
        public ClassroomNotification(String content, Date date)
        {
            this.content=content;
            this.date=date;
        }

        public String getContent()
        {
            return  content;
        }
        public String getDate()
        {
            return SimpleDateFormat.getDateInstance().format(date);
        }
    }
    private void notifications()
    {
        final SweetAlertDialog dialog=dialogWizard.showProgress(getString(R.string.please_wait),"Checking notifications for "+ selectedCourseCode,true);

        ParseQuery.getQuery("ClassroomNotifications").whereEqualTo(Const.COURSE_CODE, selectedCourseCode).orderByDescending(Const.CREATED_AT).findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                dialog.dismiss();
                if(e==null)
                {
                    if(list.size()==0)
                    {
                        manager.longToast("You have no notification from your "+ selectedCourseCode +" lecturer");
                    }
                    else{

                        List<ClassroomNotification> notifications= new ArrayList<>();
                        for(ParseObject o : list)
                        {
                            notifications.add(new ClassroomNotification(o.getString(Const.CONTENT),o.getCreatedAt()));
                        }


                        List<String> options = new ArrayList<String>();
                        for(ClassroomNotification notification : notifications)
                        {
                            options.add(notification.getDate()+"\n"+notification.getContent());
                        }

                        new MaterialDialog.Builder(StudentConsole.this)
                                .title(selectedCourseCode +" Notifications")
                                .items(options)
                                .itemsCallback(new MaterialDialog.ListCallback() {
                                    @Override
                                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                                    }
                                })
                                .show();


                    }

                }
                else{
                    ErrorHandler.handleError(context,e);
                }
            }
        });

    }

    private void classes()
    {


        new AsyncTask<Context,Void,List<TimeTableItem>>()
        {
            @Override
            protected List<TimeTableItem> doInBackground(Context... params) {
                return TimeKeeper.getNextLectures(params[0], selectedCourseCode);
            }


            @Override
            protected void onPostExecute(List<TimeTableItem> timeTableItems) {
                String txt="";
                if(timeTableItems!=null && timeTableItems.size()>0)
                for(TimeTableItem item: timeTableItems)
                {
                    txt+=item.getDay()+" :"+item.getReadableStartHour()+" to "+item.getReadableEndHour()+"\n";
                }
                if(TextUtils.isEmpty(txt))
                {
                    txt="This course doesn't appear on the timetable. You can add it yourself via the timetable";
                }

                dialogWizard.showSimpleDialog(selectedCourseCode,txt);

            }
        }.execute(context,null,null);

    }


    public class AssignmentsAdapter extends BaseAdapter {

        LayoutInflater inflater;
        Context context;
        List<ClassroomAssignment> assignments;
        public AssignmentsAdapter(Context context,List<ClassroomAssignment>list)

        {
            this.context=context;
            inflater = LayoutInflater.from(context);
            assignments=list;

        }
        @Override
        public int getCount() {
            return assignments.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.student_console_assignments_item, null);
                holder = new ViewHolder();
                holder.tvAssignmentText = (TextView) convertView.findViewById(R.id.tvAssignmentText);
                holder.tvAssignmentInfo = (TextView) convertView.findViewById(R.id.tvAssignmentInfo);
                holder.btSubmit = (Button) convertView.findViewById(R.id.btAssignmentSubmit);
                holder.btSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ClassroomAssignment assignment = assignments.get((int)v.getTag());
                        SubmitAssignment.assignment=assignment;
                        startActivity(new Intent(context,SubmitAssignment.class).putExtra(Const.ID,classroomStudent.getObjectId()));
                    }
                });
                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ClassroomAssignment assignment = assignments.get(position);
            if (assignment != null) {
                holder.tvAssignmentText.setText(assignment.getContent());
                holder.btSubmit.setTag(position);
                holder.tvAssignmentInfo.setText("To be submitted before "+assignment.submittedBeforeText());
            }







            return  convertView;
        }
    }

    public class  ViewHolder {
        TextView tvAssignmentText,tvAssignmentInfo;
        Button btSubmit;

    }
    public class AssignmentsRecycler extends RecyclerView.Adapter<StudentConsole.AssignmentsViewHolder>
    {
        List<ClassroomAssignment> assignments;
        public AssignmentsRecycler(List<ClassroomAssignment> assignments)
        {
            this.assignments=assignments;
        }
        @Override
        public AssignmentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.student_console_assignments_item, parent, false);
            return  new AssignmentsViewHolder(v, new Clicks() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context,SubmitAssignment.class).putExtra("id",(String)v.getTag()));
                }
            });
        }

        @Override
        public void onBindViewHolder(AssignmentsViewHolder holder, int position) {
            ClassroomAssignment assignment = assignments.get(position);
            holder.tvAssignmentText.setText(assignment.getContent());
            holder.btSubmit.setTag(assignment.getObjectId());

        }

        @Override
        public int getItemCount() {
            return assignments.size();
        }
    }

    public class AssignmentsViewHolder extends RecyclerView.ViewHolder{

        @InjectView(R.id.tvAssignmentText)
        TextView tvAssignmentText;
        @InjectView(R.id.btAssignmentSubmit)
        Button btSubmit;

        public AssignmentsViewHolder(View itemView, final Clicks clicks) {
            super(itemView);
            ButterKnife.inject(this,itemView);
            btSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clicks.onClick(v);
                }
            });
        }
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
      //  adapter.addFragment(new Home(), "Home");
        adapter.addFragment(new Notifications(), "Notifications");
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
    }





}
