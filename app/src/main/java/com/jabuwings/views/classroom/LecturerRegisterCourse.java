package com.jabuwings.views.classroom;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.Const;
import com.jabuwings.models.ClassroomCourse;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.rey.material.widget.Spinner;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class LecturerRegisterCourse extends JabuWingsActivity {

    @InjectView(R.id.etRegisterCourseCode)
    EditText etCourseCode;
    @InjectView(R.id.etRegisterCourseTitle)
    EditText etCourseTitle;
    @InjectView(R.id.spRegisterCourseUnit)
    Spinner spCourseUnit;
    @InjectView(R.id.etRegisterCourseDesc)
    EditText etCourseDesc;
    @InjectView(R.id.btRegisterCourse)
    Button btRegister;
    String id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lecturer_register_course);
        setUpToolbar(null,"Classroom Course Registration");
        ButterKnife.inject(this);
        id=getIntent().getStringExtra(Const.ID);
        String[] units={Const.CHOOSE,"0", "1", "2", "3","4","5","6"};
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, R.layout.row_spn,units); arrayAdapter.setDropDownViewResource(R.layout.row_spn_dropdown);
        spCourseUnit.setAdapter(arrayAdapter);

    }

    @OnClick(R.id.btRegisterCourse)
    public void register()
    {
        boolean cancel=false;
        final String courseCode=etCourseCode.getText().toString();
        String courseTitle=etCourseTitle.getText().toString();
        int courseUnit=0;
        try{
        courseUnit=Integer.valueOf((String)spCourseUnit.getSelectedItem());}
        catch (Exception e)
        {
            e.printStackTrace();
            cancel=true;
        }
        String courseDesc=etCourseDesc.getText().toString();


        if(isEmpty(courseCode) || isEmpty(courseTitle) || isEmpty(courseDesc))
        {
            cancel=true;
        }

        if(courseCode.length()!=7)
        {
            dialogWizard.showSimpleDialog(getString(R.string.error),getString(R.string.error_l_course_reg));
        }


        if(cancel)
        {
            manager.shortToast(R.string.error_fields_must_be_filled);
        }
        else{

            final SweetAlertDialog dialog= new DialogWizard(LecturerRegisterCourse.this).showSimpleProgress(getString(R.string.please_wait),"Registering "+courseCode);



            HashMap<String,Object> params= new HashMap<>();
            params.put(Const.COURSE_CODE,courseCode);
            params.put(Const.COURSE_TITLE,courseTitle);
            params.put(Const.COURSE_DESC,courseDesc);
            params.put(Const.COURSE_UNIT,courseUnit);
            params.put(Const.OBJECT_ID,id);
            params.put(Const.NAME,manager.getName());
            params.put(Const.VERSION,getString(R.string.version_number));
            ParseCloud.callFunctionInBackground(Const.CLASSROOM_COURSE_REGISTRAR, params, new FunctionCallback<String>() {
                @Override
                public void done(final String s, ParseException e) {

                    if(e==null)
                    {
                        String[] res= s.split("&");
                        String id=res[0]; final String message=res[1];

                        ClassroomCourse.getQuery().getInBackground(id, new GetCallback<ClassroomCourse>() {
                            @Override
                            public void done(ClassroomCourse classroomCourse, ParseException e) {
                                dialog.dismiss();
                                    if(e==null)
                                    {
                                        classroomCourse.put(Const.USER,manager.getUser());
                                        try{
                                        classroomCourse.pin();} catch (ParseException e1){e1.printStackTrace();}
                                        new ClassroomManager(LecturerRegisterCourse.this).addNotification(courseCode+" Registered",message);
                                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(LecturerRegisterCourse.this,SweetAlertDialog.NORMAL_TYPE).setContentText(message).setTitleText(getString(R.string.success)).setConfirmText(getString(R.string.okay)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                LecturerConsole.refresh=true;
                                                finish();
                                            }
                                        });
                                        sweetAlertDialog.show();
                                    }
                                else{
                                        ErrorHandler.handleError(context,e);
                                    }
                            }
                        });



                    }
                    else {
                        dialog.dismiss();
                        ErrorHandler.handleError(context,e);
                    }
                }
            });



        }

    }
}
