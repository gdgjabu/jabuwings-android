package com.jabuwings.views.classroom;

/**
 * Created by jamesfalade on 04/10/2017.
 */

public interface LoggedInToPortal {
    void loggedIn();
}
