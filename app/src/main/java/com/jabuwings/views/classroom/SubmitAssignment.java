package com.jabuwings.views.classroom;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.Const;
import com.jabuwings.models.ClassroomAssignment;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.jabuwings.widgets.filepicker.FilePickerActivity;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.SaveCallback;

import java.io.File;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class SubmitAssignment extends JabuWingsActivity {

    @InjectView(R.id.tvSubmitAssignmentQuestion)
    TextView tvQuestion;
    @InjectView(R.id.tvSubmitAssignmentBefore)
    TextView tvBefore;
    @InjectView(R.id.etSubmitAssignment)
    EditText etAnswer;
    @InjectView(R.id.btSubmitAssignment)
    Button btSubmit;
    @InjectView(R.id.ibSubmitAssignment)
    ImageButton ibAttach;
    @InjectView(R.id.tvSubmitAssignmentFileName)
    TextView tvFileInfor;
    ParseFile file;
    String id;
    SweetAlertDialog dialog;
    static ClassroomAssignment assignment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_assignment);
        setUpToolbar(null,"Submit Assignment");
        ButterKnife.inject(this);
        id=getIntent().getStringExtra(Const.ID);
        tvQuestion.setText(assignment.getContent());
        tvBefore.setText(assignment.submittedBeforeText());


    }

    @OnClick(R.id.ibSubmitAssignment)
    public void onAttachClick()
    {
        // This always works
        Intent i = new Intent(this, FilePickerActivity.class);
        // This works if you defined the intent filter
        // Intent i = new Intent(Intent.ACTION_GET_CONTENT);

        // Set these depending on your use case. These are the defaults.
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
        i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);

        // Configure initial directory by specifying a String.
        // You could specify a String like "/storage/emulated/0/", but that can
        // dangerous. Always use Android's API calls to get paths to the SD-card or
        // internal memory.
        i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());

        startActivityForResult(i, Const.PICK_FILE_SHARE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_OK && requestCode==Const.PICK_FILE_SHARE && data!=null) {
            File file = new File(data.getData().getPath());
            if (file.length() > 5_242_888L)
                new DialogWizard(SubmitAssignment.this).showSimpleDialog("File too large", "The Attachment must not be more than 5 megabytes");
            else
            {this.file=new ParseFile(file); tvFileInfor.setText(file.getName());}
        }

    }

    @OnClick(R.id.btSubmitAssignment)
    public void onSubmitClick()
    {
        String answer =etAnswer.getText().toString();

        if(!TextUtils.isEmpty(answer))
        {
            submitToServer(answer);
        }
        else{
            if(file!=null)
            {
                submitToServer(null);
            }

        }


    }

    private void submitToServer(final String answer)
    {
        new SweetAlertDialog(SubmitAssignment.this,SweetAlertDialog.WARNING_TYPE).setTitleText(getString(R.string.confirm)).setContentText("Once the assignment has been submitted, you cannot edit it or submit another one. Are you sure you want to submit it now?").setConfirmText(getString(R.string.proceed)).setCancelText(getString(R.string.cancel)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
                final HashMap<String,Object> params= new HashMap<>();
                params.put(Const.VERSION, manager.getVersion());
                if(answer!=null) params.put(Const.CONTENT,answer);

                params.put(Const.ID,id);
                params.put(Const.COURSE,assignment.getCourse().getObjectId());
                params.put("assignment",assignment.getObjectId());

                if(file!=null)
                {
                    dialog= new DialogWizard(SubmitAssignment.this).showProgress(getString(R.string.please_wait),"Your assignment is being submitted",false);
                    file.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e==null)
                            {
                                params.put(Const.FILE,file);
                                proceed(params);
                            }
                            else{
                                dialog.dismiss();
                                ErrorHandler.handleError(context,e);
                            }
                        }
                    });
                }
                else{
                    dialog= new DialogWizard(SubmitAssignment.this).showProgress(getString(R.string.please_wait),"Your assignment is being submitted",false);
                    proceed(params);

                }


            }
        }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
            }
        }).show();




    }
    private void proceed(HashMap<String,Object> params)
    {
        ParseCloud.callFunctionInBackground("classroomSubmitAssignment", params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {
                dialog.dismiss();
                    if(e==null)
                    {
                        new SweetAlertDialog(SubmitAssignment.this,SweetAlertDialog.SUCCESS_TYPE).setTitleText("Success").setContentText(s).setConfirmText(getString(R.string.okay)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                                finish();
                            }
                        }).show();
                    }
                else{
                        ErrorHandler.handleError(context,e);
                    }
            }
        });
    }


}
