package com.jabuwings.views.classroom;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.Const;
import com.jabuwings.models.ClassroomAssignment;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.main.JabuWingsActivity;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.rey.material.widget.ProgressView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ReviewAssignment extends JabuWingsActivity {


    @InjectView(R.id.rvReviewAssignment)
    RecyclerView recyclerView;

    @InjectView(R.id.pvReviewAssignment)
    ProgressView loading;
    static ClassroomAssignment assignment;
    List<ClassroomAssignmentAnswer> answers;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_assignment);
        ButterKnife.inject(this);
        setUpToolbar(null,"Review Assignment");
        loading.start();

        assignment.getSubmitted().getQuery().findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                loading.stop();
                loading.setVisibility(View.GONE);
                if(e==null)
                {
                    for(final ParseObject a: list)
                    {
                        a.getParseObject("student").fetchIfNeededInBackground(new GetCallback<ParseObject>() {
                            @Override
                            public void done(ParseObject parseObject, ParseException e) {
                                if(e==null)
                                {
                                    answers.add(new ClassroomAssignmentAnswer(a,parseObject));
                                }
                            }
                        });

                        recyclerView.setLayoutManager(new LinearLayoutManager(ReviewAssignment.this));
                        recyclerView.setAdapter(new ReviewAssignmentRecycler(context,answers));
                    }


                }
                else {
                    ErrorHandler.handleError(context,e);
                }
            }
        });


    }

    public class  ReviewAssignmentRecycler extends RecyclerView.Adapter<ViewHolder>{

        List<ClassroomAssignmentAnswer> answers; Context context;
        public ReviewAssignmentRecycler(Context context, List<ClassroomAssignmentAnswer> answers)
        {
            this.answers=answers; this.context=context;

        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.review_assignment_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            ClassroomAssignmentAnswer answer= answers.get(position);
            holder.view.setTag(answer);
            holder.answer.setText(answer.getAnswer());
            holder.answerName.setText(answer.getName()+"("+answer.getMatric()+")");
            holder.answerTime.setText(new Time().parseTime(answer.object.getCreatedAt()));
        }

        @Override
        public int getItemCount() {
            return answers.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{


        @InjectView(R.id.tvAnswerName)
        TextView answerName;

        @InjectView(R.id.tvAnswer)
        TextView answer;

        @InjectView(R.id.tvAnswerTime)
        TextView answerTime;

        View view;
        public ViewHolder(View view)
        {
            super(view);
            this.view=view;
            ButterKnife.inject(this,view);
        }
    }

    public class ClassroomAssignmentAnswer{
        ParseObject object,student;
        public  ClassroomAssignmentAnswer(ParseObject o,ParseObject student)
        {
            object=o;

        }

        public String getName(){return student.getString(Const.NAME);}
        public String getMatric(){return student.getString(Const.MATRIC_NO);}
        public String getAnswer(){return object.getString(Const.CONTENT);}

        public boolean isFileAttached(){return object.getParseFile(Const.FILE)!=null;}

        public ParseFile getFile()
        {
            return object.getParseFile(Const.FILE);
        }
    }
}
