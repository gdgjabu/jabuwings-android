package com.jabuwings.views.classroom;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.NotificationsRecycler;
import com.jabuwings.views.main.JabuWingsFragment;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by Falade James on 7/26/2016 All Rights Reserved.
 */
public class Notifications extends JabuWingsFragment {

    public Notifications() {
    }

    View rootView;
    Manager manager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.classroom_notifications, container, false);
        setUpFragment(rootView);
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rvClassroomNotifications);
        final TextView tvNoNotification = (TextView) findViewById(R.id.tvNoClassroomNotification);
        manager = new Manager(getActivity());

        ParseQuery.getQuery(Const.CLASSROOM_NOTIFICATIONS).fromLocalDatastore().whereEqualTo(Const.TO, manager.getUser()).findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    recyclerView.setAdapter(new NotificationsRecycler(list, getContext()));
                } else {
                    tvNoNotification.setVisibility(View.VISIBLE);
                }

            }
        });
        return rootView;
    }
}
