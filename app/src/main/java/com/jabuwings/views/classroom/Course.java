package com.jabuwings.views.classroom;

/**
 * Created by jamesfalade on 20/10/2017.
 */

public class Course {
    private float unit;
    private int no;
    private String title, code;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public float getUnit() {
        return unit;
    }

    public void setUnit(int units) {
        this.unit = units;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
