package com.jabuwings.views.classroom;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.Const;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.rey.material.widget.Spinner;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class Classroom extends JabuWingsActivity {

    @InjectView(R.id.classroom_icon1)
    ImageView icon1;
    @InjectView(R.id.classroom_icon2)
    ImageView icon2;
    @InjectView(R.id.classroom_icon3)
    ImageView icon3;
    @InjectView(R.id.classroom_icon4)
    ImageView icon4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classroom);
        ButterKnife.inject(this);
        Animation hyperSpace= AnimationUtils.loadAnimation(this,R.anim.classroom1);
        icon1.setAnimation(hyperSpace);
        icon2.setAnimation(hyperSpace);
        icon3.setAnimation(hyperSpace);
        icon4.setAnimation(hyperSpace);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try{

                    Thread.sleep(3000);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        }).start();

        int type= manager.getUserType();

        if(type==0)
        {
            finish();
            startActivity(new Intent(this,StudentConsole.class));
        }

        else if (type==1)
        {
            if(manager.getAcademicDepartment()!=null)
            {
                finish();
                startActivity(new Intent(this,LecturerConsole.class));
            }
            else{


                new MaterialDialog.Builder(Classroom.this).content("Only academic staff are allowed here").positiveText(R.string.ok).neutralText("I am an academic staff").onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        finish();
                    }
                }).onNeutral(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull final MaterialDialog dialog, @NonNull DialogAction which) {

                        Dialog.Builder builder = new SimpleDialog.Builder(R.style.SimpleDialog){
                            Spinner spinner;
                            String department;
                            @Override
                            protected void onBuildDone(Dialog dialog) {
                                dialog.layoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                spinner= (Spinner) dialog.findViewById(R.id.spAcademicDepartment);
                                String[] departments= getResources().getStringArray(R.array.departments);
                                ArrayAdapter<String> adapter = new ArrayAdapter<>(Classroom.this,R.layout.row_spn,departments);
                                adapter.setDropDownViewResource(R.layout.row_spn_dropdown);
                                spinner.setAdapter(adapter);
                            }

                            @Override
                            public void onPositiveActionClicked(DialogFragment fragment) {
                                department=(String) spinner.getSelectedItem();
                                if(!department.equals(Const.CHOOSE)){
                                    final SweetAlertDialog sweetAlertDialog=dialogWizard.showSimpleProgress(getString(R.string.please_wait),"Effecting the change");
                                    HashMap<String,Object> params = manager.getDefaultCloudParams();
                                    params.put(Const.DEPARTMENT,department);
                                    ParseCloud.callFunctionInBackground("turnAcademicStaff", manager.getDefaultCloudParams(), new FunctionCallback<String>() {
                                        @Override
                                        public void done(final String s, ParseException e) {

                                            if(e==null)
                                            {
                                                ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseUser>() {
                                                    @Override
                                                    public void done(ParseUser u, ParseException e) {
                                                        sweetAlertDialog.dismiss();
                                                        if(e==null){
                                                            u.pinInBackground();
                                                            dialogWizard.showSimpleDialog("Result",s);
                                                        }
                                                        else
                                                            ErrorHandler.handleError(context,e);
                                                    }
                                                });

                                            }

                                            else{
                                                sweetAlertDialog.dismiss();
                                                ErrorHandler.handleError(context,e);
                                                finish();
                                            }
                                        }
                                    });
                                }
                                else{
                                    manager.shortToast("Please select a department");
                                }

                            }

                            @Override
                            public void onNegativeActionClicked(DialogFragment fragment) {
                                super.onNegativeActionClicked(fragment);
                            }
                        };

                        builder.title("What is your department?")
                                .contentView(R.layout.turn_academic_staff).positiveAction(getString(R.string.change)).negativeAction(getString(R.string.cancel));

                        DialogFragment rateFragment = DialogFragment.newInstance(builder);
                        rateFragment.show(getSupportFragmentManager(), null);


                    }
                }).show();
            }
        }
        else{
            finish();
            manager.longToast("Classroom only available for students and academic staff");
        }

    }

}
