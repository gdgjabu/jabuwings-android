package com.jabuwings.views.main;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListItem;
import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.views.registration.AlumniAndPriviledgedRegistration;
import com.jabuwings.views.registration.StaffRegistration;
import com.jabuwings.views.registration.StudentRegistration;
import com.jabuwings.views.registration.StudentRegistrationJabuPortal;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class LoginHook extends JabuWingsActivity {
@InjectView(R.id.login_hook_hello)
TextView tvHello;
@InjectView(R.id.login_hook_welcome)
TextView tvWelcome;
@InjectView(R.id.login_hook_login)
Button btLogin;
@InjectView(R.id.login_hook_register)
Button btRegister;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login_hook);

        ButterKnife.inject(this);
        Typeface font= Typeface.createFromAsset(getAssets(),"fonts/DancingScript-Regular.otf");
        tvHello.setTypeface(font);
        tvWelcome.setTypeface(font);

    }

    @OnClick(R.id.login_hook_login)
    public void login()
    {
        startActivity(new Intent(this,Login.class));
        finish();
    }


    @OnClick(R.id.login_hook_register)
    public void register()
    {
        SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(LoginHook.this,SweetAlertDialog.NORMAL_TYPE).setTitleText("Please Confirm").setContentText("Are you affiliated" +
                " with Joseph Ayo Babalola University as a Student,\nStaff, or \n Alumni").setConfirmText(getString(R.string.yes)).setCancelText(getString(R.string.no)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {

                sweetAlertDialog.dismiss();
                String[] options={"Student","Staff","Alumni"};
                new MaterialDialog.Builder(LoginHook.this)
                        .title(R.string.status)
                        .items(options)
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                dialog.dismiss();
                                switch (which)
                                {
                                    case 0:
                                        startActivity(new Intent(context, StudentRegistrationJabuPortal.class));
                                        break;
                                    case 1:
                                        startActivity(new Intent(context, StaffRegistration.class));
                                        break;
                                    case 2:
                                        startActivity(new Intent(context, AlumniAndPriviledgedRegistration.class));
                                        break;
                                }
                                finish();
                            }
                        })
                        .show();



            }
        }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
                startActivity(new Intent(context,AlumniAndPriviledgedRegistration.class).putExtra(Const.TYPE,1));
                finish();
            }
        });
        sweetAlertDialog.setCancelable(true);
        sweetAlertDialog.show();

    }
}
