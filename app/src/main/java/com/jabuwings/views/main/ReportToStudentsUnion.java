package com.jabuwings.views.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.rey.material.widget.Spinner;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class ReportToStudentsUnion extends JabuWingsActivity {

    @InjectView(R.id.problem_category)
    Spinner spinner;
    @InjectView(R.id.problem_details)
    EditText problemDetails;
    @InjectView(R.id.anonymous)
    Switch anonymous;
    @InjectView(R.id.hostel)
    Spinner hostel;
    @InjectView(R.id.room)
    EditText room;
    @InjectView(R.id.hostel_holder)
    View hostelHolder;
    /*@InjectView(R.id.submit)
    Button submit;*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_to_students_union);
        setUpToolbar(null,R.string.report_to_students_union);
        ButterKnife.inject(this);

        String[] categories= getResources().getStringArray(R.array.report_problem_to_student_union_categories);
        ArrayAdapter<String> arrayAdapter= new ArrayAdapter<>(this, R.layout.row_spn,categories);
        arrayAdapter.setDropDownViewResource(R.layout.row_spn_dropdown);
        spinner.setAdapter(arrayAdapter);

        spinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(Spinner parent, View view, int position, long id) {
                if(position==1)
                {

                     hostelHolder.setVisibility(View.VISIBLE);
                    if(manager.getUser().getString(Const.SEX).equals("M"))
                    {
                        ArrayAdapter<String> arrayAdapter= new ArrayAdapter<String>(ReportToStudentsUnion.this, R.layout.row_spn,  getResources().getStringArray(R.array.male_hostels));
                        arrayAdapter.setDropDownViewResource(R.layout.row_spn_dropdown);
                        hostel.setAdapter(arrayAdapter);
                    }

                    else{
                        ArrayAdapter<String> arrayAdapter= new ArrayAdapter<String>(ReportToStudentsUnion.this, R.layout.row_spn,  getResources().getStringArray(R.array.female_hostels));
                        arrayAdapter.setDropDownViewResource(R.layout.row_spn_dropdown);
                        hostel.setAdapter(arrayAdapter);
                    }
                }
                else {
                    hostelHolder.setVisibility(View.GONE);
                }
            }
        });


    }

    @OnClick(R.id.submit)
    public void submit()
    {
        String category = (String) spinner.getSelectedItem();
        if(!category.equals(Const.CHOOSE))
        {
            String details = problemDetails.getText().toString();
            if(TextUtils.isEmpty(details))
            {
                manager.shortToast("Please specify the details of the problem");
            }
            else if (details.length()<=10)
            {
                manager.shortToast("Please add more details");
            }
            else if(spinner.getSelectedItemPosition()==1)
            {
                if(hostel.getSelectedItemPosition()!=0)
                {
                    if(room.getText().toString().length() >=2)
                    {
                        complete();
                    }
                    else manager.shortToast("Please select the room");
                }
                else{
                    manager.shortToast("Please select your hostel");
                }
            }
            else{
               complete();
            }

        }
        else manager.shortToast("Please select a category");

    }

    private void complete()
    {
        boolean isHostel = spinner.getSelectedItemPosition() ==1;
        ParseACL parseACL = new ParseACL();
        parseACL.setPublicReadAccess(false);
        parseACL.setPublicWriteAccess(false);
        parseACL.setReadAccess(manager.getUser(),true);
        parseACL.setWriteAccess(manager.getUser(),false);
        final ParseObject object = new ParseObject("SUESubmittedProblems");
        object.setACL(parseACL);
        object.put(Const.CATEGORY,(String ) spinner.getSelectedItem());
        object.put(Const.DETAILS, problemDetails.getText().toString());
        if(isHostel)
        {
            object.put(Const.HOSTEL,(String) hostel.getSelectedItem());
            object.put(Const.ROOM, room.getText().toString());
        }
        if(anonymous.isChecked())
        {
            object.put(Const.ANONYMOUS, true);
        }
        else
            object.put(Const.USER, manager.getUser());

        object.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e==null) manager.successToast("Your issue was submitted to the Student's Union");
                else object.saveEventually();
            }
        });
        manager.longToast("Thanks for getting in touch.");
        finish();
    }
}
