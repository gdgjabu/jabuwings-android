package com.jabuwings.views.main;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.realm.Hub;
import com.jabuwings.database.realm.HubMessage;
import com.jabuwings.image.SiliCompressor;
import com.jabuwings.intelligence.lisa.Lisa;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.management.Const;
import com.jabuwings.models.HubChatAdapter;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.widgets.SwitchButton;
import com.jabuwings.widgets.audiorecorder.AndroidAudioRecorder;
import com.jabuwings.widgets.audiorecorder.model.AudioChannel;
import com.jabuwings.widgets.audiorecorder.model.AudioSampleRate;
import com.jabuwings.widgets.audiorecorder.model.AudioSource;
import com.jabuwings.widgets.filepicker.FilePickerActivity;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;
import com.sangcomz.fishbun.define.Define;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;

/**
 * Created by Falade James on 11/10/2015 All Rights Reserved.
 */
public class HubChat extends Converse implements  View.OnClickListener
{

    String hubTitle;


    String hubId;

    /**
     * The database id of the hub
     */
    String id;
    String status="Hub Status";
    static Intent shareIntent;
    static boolean share=false;
    String audioFilePath;
    DialogWizard dialogWizard;
    boolean hubMembershipActive=true;
    HubChatAdapter adapter;
    Hub hub;
    //private String LOG_TAG=HubChat.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setListener(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hub_chat);
        setUpToolbar(this);
        init();
        initViews(getWindow());
        initEditText(getWindow());
        loadPicture(hubId,hubTitle);
        setUpEmoji(getWindow());
        restoreMessage();
        getHub();
        getShareIntent();

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        RealmResults<HubMessage> realmResults = realm.where(HubMessage.class)
                .equalTo(Const.HUB_ID, hubId)
                .findAllAsync();


        OrderedRealmCollectionChangeListener<RealmResults<HubMessage>> callback =  new OrderedRealmCollectionChangeListener<RealmResults<HubMessage>>() {
            @Override
            public void onChange(RealmResults<HubMessage> messages, OrderedCollectionChangeSet changeSet) {
                if(changeSet==null)
                {
                    adapter = new HubChatAdapter(HubChat.this, messages.sort("time"), realm, hub);
                    recyclerView.setAdapter(adapter);
                    recyclerView.scrollToPosition(adapter.getItemCount()-1);
                }
                else {
                    manager.log("hub message adapter change");
                    manager.log("first visible "+linearLayoutManager.findFirstVisibleItemPosition());
                    manager.log("last visible "+linearLayoutManager.findLastVisibleItemPosition());
                    manager.log("adapter size "+adapter.getItemCount());

                    if(adapter.getItemCount() - linearLayoutManager.findLastVisibleItemPosition() <=2)
                        recyclerView.scrollToPosition(adapter.getItemCount()-1);
                }
            }
        };
        realmResults.addChangeListener(callback);

    }

    private void getShareIntent()
    {
        if(share&&shareIntent!=null)
        {
            Intent intent = shareIntent.getParcelableExtra("intent");
            String action = intent.getAction();
            String type = intent.getType();
            if (Intent.ACTION_SEND.equals(action) && type != null) {
                if ("text/plain".equals(type)) {
                    handleSendText(intent); // Handle text being sent
                } else if (type.startsWith("image/")) {
                    handleSendImage(intent); // Handle single image being sent
                }
            } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
                if (type.startsWith("image/")) {
                    handleSendMultipleImages(intent); // Handle multiple images being sent
                }
            } else {
                manager.errorToast("Not supported");
            }
        }

        share=false;
        shareIntent=null;

    }
    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            txt.setText(sharedText);
        }
    }

    void handleSendImage(Intent intent) {
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            String p = SiliCompressor.with(this).compress(imageUri.toString());
           // saveAndSendFileToServer("",profileUsername,new File(p),p);
        }
    }

    void handleSendMultipleImages(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (imageUris != null) {
            manager.errorToast("Multiple images are currently not supported");
        }
    }
    private void getHub()
    {
        ParseQuery<ParseObject> hubQuery= new ParseQuery<>(Const.HUB_CLASS_NAME);
        hubQuery.getInBackground(hubId, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) {
                    parseObject.pinInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e==null)
                            {

                            }
                            else{

                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }
    private void restoreMessage()
    {
        String msg = manager.getEntityLastDraft(hubId);
        if(!TextUtils.isEmpty(msg))
        {
            txt.setText(msg);
        }

    }
    private void init()
    {
        hubId = getIntent().getStringExtra(Const.HUB_ID);
        hub = realm.where(Hub.class).equalTo(Const.HUB_ID, hubId).findFirst();
        status = hub.getDesc();
        hubTitle= hub.getTitle();
        dialogWizard=new DialogWizard(HubChat.this);
        setTitle(hubTitle);
        setSubtitle(status);
    }
    

    @Override
    public void onClick(View v) {

        switch (v.getId())

        {
            case R.id.btnSend:
                if (send.getState()==SwitchButton.FIRST_STATE)
                {
                    // audioFilePath = Environment.getExternalStorageDirectory() + "/JabuWings/JabuWings Audio/"+username+"/"+Lisa.generateAudioFileName(username)+".wav";
                    new File(Environment.getExternalStorageDirectory() + "/JabuWings/JabuWings Audio/"+hubTitle+"/").mkdirs();
                    audioFilePath = Environment.getExternalStorageDirectory() + "/JabuWings/JabuWings Audio/"+hubTitle+"/"+ Lisa.generateAudioFileName(hubTitle)+".wav";

                    int color = getResources().getColor(R.color.colorPrimaryDark);
                    int requestCode = 234;
                    AndroidAudioRecorder.with(this)
                            // Required
                            .setFilePath(audioFilePath)
                            .setColor(color)
                            .setRequestCode(requestCode)

                            // Optional
                            .setSource(AudioSource.MIC)
                            .setTitle("Send to "+hubTitle)
                            .setChannel(AudioChannel.STEREO)
                            .setSampleRate(AudioSampleRate.HZ_48000)

                            // Start recording
                            .record();

                }

                else{
                    String msg= txt.getText().toString();
                    txt.setText(null);
                    if(TextUtils.isEmpty(msg)) break;
                    else{
                        send(msg);
                    }
                }



                break;
            case R.id.toolbar:
                startActivity(new Intent(HubChat.this,HubProfile.class).putExtra(Const.HUB_ID,hubId));
                break;
            case R.id.iv_chat_share_pic:
                if (hubMembershipActive)
                    shareImage();
                else flagMembership();
                break;
            case R.id.iv_chat_share_cam:
                if (hubMembershipActive)
                    shareCameraPhoto();
                else flagMembership();
                break;
            case R.id.iv_chat_share_others:
                if (hubMembershipActive) {

                    String[] options = {"Share File"};
                    new MaterialDialog.Builder(HubChat.this)
                            .title(R.string.share_others)
                            .items(options)
                            .itemsCallback(new MaterialDialog.ListCallback() {
                                @Override
                                public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                    dialog.dismiss();
                                    switch (which)
                                    {
                                        case 0:
                                            sendFile();
                                            break;
                                    }
                                }
                            })
                            .show();


                } else flagMembership();
                break;
        }
    }

    private void flagMembership()
    {
        new DialogWizard(HubChat.this).showErrorDialog("Removed","You are no longer a member of this hub");
    }
    private void sendFile()
    {
        // This always works
        Intent i = new Intent(context, FilePickerActivity.class);
        // This works if you defined the intent filter
        // Intent i = new Intent(Intent.ACTION_GET_CONTENT);

        // Set these depending on your use case. These are the defaults.
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
        i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);

        // Configure initial directory by specifying a String.
        // You could specify a String like "/storage/emulated/0/", but that can
        // dangerous. Always use Android's API calls to get paths to the SD-card or
        // internal memory.
        i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());

        startActivityForResult(i, Const.PICK_FILE_SHARE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri=null;
        if(data!=null)
        {uri = data.getData();}
        switch (requestCode)
        {
            case 234:
                if (resultCode == RESULT_OK) {
                    sendFileToServer(new File(audioFilePath),null, DataProvider.MessageType.OUTGOING_VOICE_NOTE.ordinal());;
                } else if (resultCode == RESULT_CANCELED) {
                    // Oops! User has canceled the recording
                }
                break;
            case Const.PICK_FILE_SHARE:
                if(resultCode==RESULT_OK && data!=null) {
                    File file = new File(uri.getPath());
                    if (file.length() > 10485760L)
                        dialogWizard.showSimpleDialog("File too large", "The file must not be more than 10 megabytes");
                    else
                        sendFileToServer(file, txt.getText().toString(), DataProvider.MessageType.OUTGOING_FILE.ordinal());
                }
                break;
            case Define.ALBUM_REQUEST_CODE:
                if(resultCode==RESULT_OK && data!=null) {
                    ArrayList<Uri> path = data.getParcelableArrayListExtra(Define.INTENT_PATH);
                    if (path.size() > 0) {
                        String p = SiliCompressor.with(this).compress(path.get(0).toString());
                        sendFileToServer( new File(p), txt.getText().toString(),  DataProvider.MessageType.OUTGOING_IMAGE.ordinal());
                    }
                }
                break;
            case Const.PICK_CAMERA_SHARE_ID:
                File image;
                File sd = Environment.getExternalStorageDirectory();
                File dir;
                try{
                    dir = new File(sd.getAbsolutePath()+Const.PUBLIC_IMAGE_DIRECTORY);
                    dir.mkdirs();
                }
                catch (Exception e)
                {
                    manager.shortToast(R.string.error_phone_storage);
                    return;
                }


                try{
                    Bitmap bitmap;
                    bitmap=(Bitmap) data.getExtras().get(Const.DATA);
                    //covert  to bytes[]
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    if(bitmap!=null) {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                        String path= MediaStore.Images.Media.insertImage(context.getContentResolver(),bitmap,Lisa.generateImageFileName(manager.getUsername()),null);
                        Uri u= Uri.parse(path);
                        Cursor cursor = getContentResolver().query(u,null,null,null,null);
                        cursor.moveToFirst();
                        path=cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA));
                        cursor.close();
                        String p=SiliCompressor.with(context).compress(path);
                        sendFileToServer(new File(p), txt.getText().toString(),  DataProvider.MessageType.OUTGOING_IMAGE.ordinal());
                    }



                /*    FileOutputStream fos = new FileOutputStream
                            (image);

                    fos.write(stream.toByteArray());
                    fos.close();
                    // saveAndSendFileToServer(txt.getText().toString(),profileUsername,stream.toByteArray(),image.getPath());*/
                    txt.setText(null);


                }
                catch (OutOfMemoryError e){
                    e.printStackTrace();
                    String error= e.getMessage();
                    Toast.makeText(getApplicationContext(), "Memory error: " + error, Toast.LENGTH_LONG).show();
                }
                break;
            default:
                if(resultCode==RESULT_OK && data!=null)
                {

                    if(requestCode==Const.PICK_IMAGE_SHARE_ID || requestCode==Const.PICK_CAMERA_SHARE_ID )
                    {


                    }


                }
                else {
                    manager.shortToast(R.string.error_nothing_was_obtained);
                }

        }
    }

    private void sendFileToServer(final File file,final String msg, final int type)
    {

        final   ParseFile parseFile= new ParseFile(file);
        final HubMessage message=HouseKeeping.storeOutgoingHubMedia(context, file.getAbsolutePath(), msg, hubId, type);
        final String messageId = message.getMsgId();




        parseFile.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    manager.log("File Message uploaded");
                    HotSpot.sendHubMedia(type, parseFile, file.length(),hubId, getApplicationContext(), messageId,msg);
                } else {
                    manager.shortToast(e.getMessage());
                    e.printStackTrace();
                    realm.beginTransaction();
                    message.setDeliveryStatus(Const.MESSAGE_FAILED);
                    realm.commitTransaction();
                }

            }
        }, new ProgressCallback() {
            @Override
            public void done(final Integer percentDone) {
                realm.beginTransaction();
                com.jabuwings.database.realm.File file = message.getFile();
                file.setProgress(percentDone);
                realm.commitTransaction();
                manager.log("Progress " + percentDone);
            }
        });
    }



    @Override
    protected void onPause()
    {
        super.onPause();
        String msg= txt.getText().toString();
        if(!TextUtils.isEmpty(msg))
        {
            manager.storeLastEntityDraft(hubId,msg);
        }
        else{
            manager.storeLastEntityDraft(hubId,null);
        }
        overridePendingTransition(R.anim.fade_scale_in, R.anim.slide_to_right);
      /*  ContentValues values = new ContentValues(1);
        values.put(DataProvider.COL_COUNT, 0);
        getContentResolver().update(Uri.withAppendedPath(DataProvider.CONTENT_URI_HUBS, id), values, null, null);
        getContentResolver().notifyChange(DataProvider.CONTENT_URI_HUBS, null);*/

    }


    private void send(final String msgToBeSent) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {

                    HotSpot.sendHubMessage(context,msgToBeSent.trim(),manager.getUsername(),hubId,DataProvider.MessageType.OUTGOING.ordinal());

                } catch (Exception ex) {
                    ex.printStackTrace();
                    msg = "Message could not be sent" + ex;
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (!TextUtils.isEmpty(msg)) {
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                }
            }
        }.execute(null, null, null);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}

