package com.jabuwings.views.main;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import static com.jabuwings.management.Const.*;
import com.jabuwings.R;
import com.jabuwings.error.ExceptionHandler;
import com.jabuwings.intermediary.NotificationManager;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.JabuWingsUser;
import com.jabuwings.utilities.DialogWizard;
import com.parse.ParseUser;

import io.realm.Realm;

/**
 * Created by Falade James on 11/28/2015 All Rights Reserved.
 */

/**
 * This activity is the basis activity for most JabuWings activities. Most of the activities extend this activity
 * to inherit some features so as not to keep repeating the same thing across activities
 */
@SuppressWarnings("ConstantConditions")
public class JabuWingsActivity extends AppCompatActivity {
protected Toolbar toolbar;
protected Context context;
protected Manager manager;
protected Realm realm;

protected DialogWizard dialogWizard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        context=getApplicationContext();
        realm = Realm.getDefaultInstance();
        manager= new Manager(this);
        dialogWizard=new DialogWizard(this);

        //check if the intent is to open a notification and clear the list if it is.
        if(getIntent().getBooleanExtra(Const.CLEAR_NOTIFICATIONS,false)){NotificationManager.clearNotifications();}
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    /**
     * This is used to initialise the toolbar and should only be used when the id of the toolbar is "R.id.toolbar"
     */
    protected Toolbar setUpToolbar(View.OnClickListener listener)
    {
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        toolbar.setOnClickListener(listener);
        toolbar.setNavigationIcon(R.drawable.btn_back);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return toolbar;
    }

    /**
     * This is used to initialise the toolbar and should only be used when the id of the toolbar is "R.id.toolbar"
     */
    protected Toolbar setUpToolbar(View.OnClickListener listener,int titleResId)
    {
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(titleResId);
        toolbar.setOnClickListener(listener);
        toolbar.setNavigationIcon(R.drawable.btn_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return toolbar;
    }

    protected Toolbar setUpToolbar(View.OnClickListener listener,String title)
    {
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        toolbar.setOnClickListener(listener);
        toolbar.setNavigationIcon(R.drawable.btn_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return toolbar;
    }

    protected void setUpTextViews(TextView... textViews){

        int preferredTypeface= manager.getPreferredTypeFace();
        fontHandler(context, preferredTypeface, textViews);

    }

    protected void setDisplayShowAsEnable(boolean showTitle)
    {
        getSupportActionBar().setDisplayShowTitleEnabled(showTitle);
    }
    public static void setUpTextViews(Context context,TextView... textViews){

        int preferredTypeface= new Manager(context).getPreferredTypeFace();
        fontHandler(context,preferredTypeface,textViews);
    }

    public static String getAssetPath(int preferredTypeface)
    {
        switch (preferredTypeface)
        {
            case 0:
                return null;
            case 1:
               return"fonts/walkway_ultrabold.ttf";
            case 2:
               return"fonts/GreatVibes-Regular.otf";
            case 3:
               return"fonts/GoodDog.otf";
            case 4:
               return"fonts/AlexBrush-Regular.ttf";
            default:
                return null;
        }
    }
    private static void fontHandler(Context context,int preferredTypeface,TextView... textViews)
    {
        String assetPath=getAssetPath(preferredTypeface);

        if(assetPath!=null)
        {
            Typeface font= Typeface.createFromAsset(context.getAssets(),assetPath);
            for (TextView textView: textViews)
            {
                if(textView!=null)
                textView.setTypeface(font);
            }
        }
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    protected boolean isEmpty(CharSequence str)
    {
        return TextUtils.isEmpty(str);
    }

    protected void setTitle(String title)
    {
        toolbar.setTitle(title);
    }
    protected void setSubtitle(String subtitle)
    {
        getSupportActionBar().setSubtitle(subtitle);
    }
    protected void shortToast(String t){manager.shortToast(t);}

}

