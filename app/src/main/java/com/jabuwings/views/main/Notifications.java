package com.jabuwings.views.main;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.intermediary.UserQuery;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.classroom.Classroom;
import com.jabuwings.views.hooks.FriendRequests;
import com.jabuwings.views.hooks.NotificationDetails;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Notifications extends JabuWingsActivity {

private static String TAG = Notifications.class.getSimpleName();

        RecyclerView rvNotifications;
        TextView lonely;
        Toolbar toolbar;
        List<ParseObject> notifications; List<String> friendNotifications=new ArrayList<>(),appNotifications=new ArrayList<>(),classroomNotifications=new ArrayList<>();
        TextView title;
        int firstSection=0,secondSection=1,thirdSection=2;


        OnBadgeChangedListener listener;


 public Notifications()
 {}


    public void setBadgeChangedListener(OnBadgeChangedListener listener)
    {
        this.listener=listener;
    }






    private void createListener()
    {
        if(listener==null)
        {
            Domicile core = Domicile.getInstance();
            if(core!=null)
            {
                listener= Domicile.getInstance().badgeChangedListener;
            }
        }
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        setUpToolbar(null,getString(R.string.notifications));
        rvNotifications=(RecyclerView) findViewById(R.id.rvNotifications);
        rvNotifications.setLayoutManager(new LinearLayoutManager(this));
        lonely= (TextView)findViewById(R.id.tv_notifications);


        ParseQuery<ParseObject>  query= new ParseQuery<>(Const.LOCAL_NOTIFICATIONS).fromLocalDatastore();
        query.whereEqualTo(Const.USER,new Manager(context).getUser());
        query.whereEqualTo(Const.VALID,true);
        query.orderByAscending(Const.TIME);
        query.setLimit(100);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e==null)
                {
                    if(list.size()==0)
                        lonely.setVisibility(View.VISIBLE);
                    rvNotifications.setAdapter(new NotificationsRecycler(list));

                    for(ParseObject o: list)
                    {
                        o.put(Const.MESSAGE_READ,true);
                        o.pinInBackground();
                    }
                }
                else{
                    ErrorHandler.handleError(context,e);
                }
            }
        });


    }


    public class NotificationsRecycler extends RecyclerView.Adapter<ViewHolder>
    {
        List<ParseObject> notifications;
        public NotificationsRecycler(List<ParseObject> notifications)
        {
            this.notifications=notifications;
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.section_notification_item, parent, false);
            return  new ViewHolder(v, new Clicks() {
                @Override
                public void onClick(View v) {
                 JabuWingsNotification notification=(JabuWingsNotification) v.getTag();
                 switch (notification.getType())
                 {
                     case 0:
                         startActivity(new Intent(Notifications.this, FriendRequests.class));
                         break;
                     case 1:
                         if(!manager.getDatabaseManager().getFriends().contains(notification.getMeta()))
                         {
                             UserQuery.getUserFriend(notification.getMeta(),Notifications.this);
                         }
                         break;
                     case 2:
                         break;
                     case 3:
                         startActivity(new Intent(Notifications.this, NotificationDetails.class).putExtra(Const.CONTENT,notification.getContent()).putExtra(Const.TIME,notification.getDate().getTime()));
                         break;
                     case 4:
                         startActivity(new Intent(Notifications.this, Classroom.class));
                         break;
                     case 5:
                         startActivity(new Intent(Notifications.this, NotificationDetails.class).putExtra(Const.CONTENT,notification.getContent()).putExtra(Const.TIME,notification.getDate().getTime()));
                         break;
                 }

                }
            });
        }

        
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            JabuWingsNotification notification  = new JabuWingsNotification(notifications.get(position));
            holder.rootView.setTag(notification);
            holder.tvHeader.setText(Html.fromHtml(notification.getContent()));
            holder.imgItem.setImageDrawable(getResources().getDrawable(R.drawable.logo));
            holder.tvDate.setText(new Time().parseTime(notification.getDate()));

        }

        @Override
        public int getItemCount() {
            return notifications.size();
        }
    }

    public class JabuWingsNotification{
        ParseObject o;
        public JabuWingsNotification(ParseObject o)
        {
            this.o=o;
        }
        public int getType()
        {
            return o.getInt(Const.TYPE);
        }
        public String getContent(){
            return o.getString(Const.MSG);
        }

        public Date getDate()
        {
            return o.getDate(Const.TIME);
        }
        public String getMeta()
        {
            return o.getString(Const.META);
        }
    }
    class ViewHolder extends RecyclerView.ViewHolder {

        private final View rootView;
        private final ImageView imgItem;
        private final TextView tvHeader;
        private final TextView tvDate;
        

        public ViewHolder(View view, final Clicks clicks) {
            super(view);

            rootView = view;
            imgItem = (ImageView) view.findViewById(R.id.imgItem);
            tvHeader = (TextView) view.findViewById(R.id.tvHeader);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            JabuWingsActivity.setUpTextViews(context,tvHeader,tvDate);
            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clicks.onClick(v);
                }
            });
        }
    }
    interface Clicks{
         void onClick(View v);
    }


   


    

    






}
