package com.jabuwings.views.main;


import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.database.realm.Friend;
import com.jabuwings.intelligence.lisa.ChatWithLisa;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.FriendsContextMenuManager;
import com.jabuwings.models.FriendsAdapter;
import com.jabuwings.widgets.CircleImage;
import com.nightonke.boommenu.BoomMenuButton;
import com.parse.ParseUser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * A simple {@link Fragment} subclass.
 */
public class Friends extends JabuWingsFragment implements AdapterView.OnItemClickListener, OrderedRealmCollectionChangeListener<RealmResults<Friend>>{






    // The user.

    public static ParseUser user;
    private static final String LOG_TAG = Friends.class.getSimpleName();

    TextView tvFriendUsername;
    String friendUsername;
    FriendsAdapter friendsAdapter;

    Manager manager;
    Context context;
    Boolean newSignIn;
    DatabaseManager databaseManager;
    SQLiteDatabase db;
    Bitmap bitmap;
    String username;
    int id;
    String profilePictureURL;
    View layout;
    View rootView;
    RecyclerView friendsRecycler;
    RealmResults<Friend> results;
    @Override
    public void onResume() {
        super.onResume();

    }

    public Friends() {
        // Required empty public constructor
    }



    private void setUpAdapter(RealmResults<Friend> friends)
    {
        friendsAdapter = new FriendsAdapter(getActivity(), friends.sort("lastMessageDate", Sort.DESCENDING));
        // registerForContextMenu(friendsRecycler);
        friendsRecycler.setAdapter(friendsAdapter);
        friendsRecycler.setLayoutManager(new LinearLayoutManager(context));
        friendsRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                FriendsContextMenuManager.getInstance().onScrolled(recyclerView, dx, dy);
            }
        });
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         rootView=inflater.inflate(R.layout.fragment_friends2, container, false);
         friendsRecycler=(RecyclerView)rootView.findViewById(R.id.rv_friends);
        setUpAdapter(results);
        //friendsRecycler.addOnScrollListener(new ToolbarHidingOnScrollListener(Core.toolbarContainer, Core.toolbar, Core.tabLayout, Core.parallaxView));
         return  rootView;
    }


    @Override
    public void onChange(RealmResults<Friend> friends, OrderedCollectionChangeSet changeSet) {
        /*manager.shortToast("Query friends done");
        if (changeSet == null) {
            results = friends;
            results = results .sort(Const.NAME);
            setUpAdapter(results);
        } else {
            // Called on every update.
            if(friendsCursorAdapter!=null)
                friendsCursorAdapter.notifyDataSetChanged();
        }*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(LOG_TAG, "onCreate called");
        context= getActivity();
        manager =new Manager(context);
        newSignIn = getActivity().getIntent().getBooleanExtra(Const.NEW_LOGIN, false);
        super.onCreate(savedInstanceState);

        Log.d(LOG_TAG, "New sign is " + newSignIn);
        user = ParseUser.getCurrentUser();

        databaseManager = new DatabaseManager(context);
        db = databaseManager.getWritableDatabase();

        RealmResults<Friend> realmResults = realm.where(Friend.class).findAll();
        results = realmResults;



        RealmChangeListener changeListener = new RealmChangeListener() {
            @Override
            public void onChange(Object o) {
                if(friendsAdapter !=null)
                    friendsAdapter.notifyDataSetChanged();
            }
        };

        realmResults.addChangeListener(this);
        realm.addChangeListener(changeListener);



    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        tvFriendUsername = (TextView) view.findViewById(R.id.user_item_username);
        friendUsername = tvFriendUsername.getText().toString();
        if (friendUsername.equals("lisa") || friendUsername.equals("Lisa")) {
            startActivity(new Intent(context,
                    ChatWithLisa.class).putExtra(
                    Const.FRIEND_ID, friendUsername));
        } else {

            startActivity(new Intent(context,
                    Chat.class).putExtra(
                    Const.FRIEND_ID, friendUsername));
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        /*MenuInflater menuInflater = getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.context_menu_friends,menu);*/
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info= (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        //int index= info.position;
        View view= info.targetView;
        tvFriendUsername = (TextView)view.findViewById(R.id.user_item_username);
        String friend= tvFriendUsername.getText().toString();

        switch (item.getItemId())
        {
            /*case R.id.contextual_action_view_profile:
                startActivity(new Intent(context, ProfileOfFriend.class).putExtra(Const.USERNAME,friend));
                break;
            case R.id.contextual_action_block_friend:
                Snackbar.make(rootView, "work in progress", Snackbar.LENGTH_SHORT).show();
                break;
            case R.id.contextual_action_remove_friend:
                HouseKeeping.deleteFriend(context,friend);
                Snackbar.make(rootView,"Friend successfully removed",Snackbar.LENGTH_SHORT).show();
                break;
            case R.id.contextual_action_report_friend:
                Snackbar.make(rootView,"work in progress",Snackbar.LENGTH_SHORT).show();
               break;*/
   }




        return super.onContextItemSelected(item);
    }


    public void dismissBoom()
    {
        BoomMenuButton poppedBoom= friendsAdapter.getPoppedBoom();
        if(poppedBoom!=null && poppedBoom.isShown())
        {
           // poppedBoom.dismiss();
        }
    }
    private static class ViewHolder {
        TextView name;
        TextView username;
        TextView messageCount;
        TextView lastMessage;
        CircleImage displayPicture;
        TextView status;
        TextView timeStamp;
        ImageView lastMessageStatus;
    }




    public void storeFile(Bitmap bitmap, String Filename, String username) {
        try {
            FileOutputStream pp =context.openFileOutput(Filename, Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 60, pp);

            Log.d(LOG_TAG,"picture of "+ username+" stored");
        } catch (IOException | OutOfMemoryError e) {
            e.printStackTrace();
        }
    }

    public Bitmap openFile(String fileName) {
        try {
            FileInputStream imageInput;
            imageInput = context.openFileInput(fileName);
            return BitmapFactory.decodeStream(imageInput);


        } catch (IOException |OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        /**
         * The system calls this to perform work in a worker thread and
         * delivers it the parameters given to AsyncTask.execute()
         */

        String FILENAME;
        int holderID;
        ViewHolder holder;

        public DownloadImageTask(ViewHolder holder)
        {
            this.holder=holder;
        }

        public DownloadImageTask()
        {

        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            try {
                Log.d(LOG_TAG, "Attempting to download picture @: " + urls[0]);
                URL url = new URL(urls[0]);
                username = urls[1];
                if (!username.equals("user")) {
                    FILENAME = urls[0].replace('/', 's');

                    Log.d("Async", "username is " + username);

                }


                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                return BitmapFactory.decodeStream(input);
            } catch (IOException e) {
                e.printStackTrace();
                return null;

            } catch (NullPointerException e) {
                e.printStackTrace();
                return null;
            }

        }

        /**
         * The system calls this to perform work in the UI thread and delivers
         * the result from doInBackground()
         */
        @Override
        protected void onPostExecute(Bitmap result) {
            bitmap = result;
            try {
                if (username.equals("user")) {
                    Domicile.refreshPicture();
                    try {
                        FileOutputStream pp = getActivity().openFileOutput(Const.PROFILE_PICTURE_NAME, Context.MODE_PRIVATE);
                        result.compress(Bitmap.CompressFormat.PNG, 100, pp);
                    } catch (IOException | NullPointerException q) {
                        q.printStackTrace();

                    }
                } else {
                    holder.displayPicture.setImageBitmap(result);
                    Log.d(LOG_TAG, "Profile url: " + profilePictureURL);
                    Log.d(LOG_TAG, "File url: " + FILENAME);
                    storeFile(result, FILENAME, username);
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }


        }



    }

    public interface onContextMenuClick {
        public void onMoreClick(View v, int position);
    }


}












