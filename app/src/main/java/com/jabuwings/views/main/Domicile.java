package com.jabuwings.views.main;


import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jabuwings.R;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.download.ImageDownloader;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.image.ImageProcessor;
import com.jabuwings.intelligence.FeedService;
import com.jabuwings.intelligence.ProfileWatchDog;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.management.SessionManager;
import com.jabuwings.models.JabuWingsPublicUser;
import com.jabuwings.models.MatricNo;
import com.jabuwings.streaming.Channels;
import com.jabuwings.utilities.ConnectionDetector;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.chapel.ChapelHome;
import com.jabuwings.views.football.Football;
import com.jabuwings.views.hooks.About;
import com.jabuwings.views.hooks.FeedMonger2;
import com.jabuwings.views.hooks.FriendRequests;
import com.jabuwings.views.hooks.HubCreationWizard;
import com.jabuwings.views.hooks.MultipleUsersSearch;
import com.jabuwings.views.vote.VotePre;
import com.jabuwings.widgets.CircleImage;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.jabuwings.widgets.contactpicker.ui.ContactPickerActivity;
import com.jabuwings.widgets.navtab.ntb.NavigationTabBar;
import com.jabuwings.widgets.spacenavigation.OnBoomReady;
import com.jabuwings.widgets.spacenavigation.SpaceItem;
import com.jabuwings.widgets.spacenavigation.SpaceNavigationView;
import com.jabuwings.widgets.spacenavigation.SpaceOnClickListener;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.OnBoomListener;
import com.parse.ParseConfig;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

//@SuppressWarnings("all")
public class Domicile extends JabuWingsActivity implements View.OnClickListener ,OnBadgeChangedListener

{


    static SpaceNavigationView space;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;public static Toolbar pToolbar;
    @InjectView(R.id.domicile_toolbar_title)
    TextView tvTitle;
    @InjectView(R.id.boomDomicileMenu)
    BoomMenuButton boom2;
    /*@InjectView(R.id.search_view)
    MaterialSearchView searchView;*/
    @InjectView(R.id.viewpager_domicile)
     ViewPager viewPager;
    @InjectView(R.id.cord_domicile)
    CoordinatorLayout layout;

    static Button btProfile;


    TextView notificationsBadge;
    public static boolean newSignIn;
    public static CircleImage profilePicture;
    static Domicile instance;
    NavigationTabBar ntb;
    static BoomMenuButton boom;
    boolean boomInit=false;
    NavigationTabBar.Model notificationsModel,feedModel,friendsModel,hubsModel,acadModel;
    boolean showTutorial=false,share=false;
    public static boolean showDownloadNewVersion=false;
    public OnBadgeChangedListener badgeChangedListener;
    public static boolean openCustom=false;
    public static int customPosition=0;
    Feed feed;
    Acad acad;
    Hubs hubs;
    Friends friends;
    int showCasePosition = 0;





    @Override
    protected void onResume() {
        handleService(true);
        getBadges();
        super.onResume();
    }

    public static Domicile getInstance()
    {
        return instance;
    }

    public  static void refreshPicture()
    {

        ImageProcessor imageProcessor=new ImageProcessor(getInstance());
        imageProcessor.removeBitmapFromMemoryCache(Const.PROFILE_PICTURE_NAME);
        imageProcessor.loadBitmap(Const.PROFILE_PICTURE_NAME, profilePicture);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_domicile);
        ButterKnife.inject(this);




        pToolbar=toolbar;
        instance=Domicile.this;
        badgeChangedListener=this;
        newSignIn =getIntent().getBooleanExtra(Const.NEW_LOGIN, false);
        showTutorial=getIntent().getBooleanExtra(Const.TUTORIAL, false);
        share=getIntent().getBooleanExtra("share", false);


        if(!manager.isUserLoggedIn())
        {
            manager.login();
        }

        initViews();
        Time time = new Time();
        //TODO REMOVE ALL LOGS

        Log.d("Memory limit",String.valueOf(Runtime.getRuntime().maxMemory() / 1024 / 1024));



        if(showDownloadNewVersion)
        {
            showDownloadNewVersion();
        }

        if (!manager.isAppRated() && manager.getAppOpenings()>=7 && !manager.dontShowRating())
        { //showRate();
             }
       if(showTutorial) {
           showTutorial();
       }

        checkPermissions();
        checkPush();


    }

    //Check if push notification is properly set and the installation object contains the right username
    private void checkPush()
    {
        String installationUsername= ParseInstallation.getCurrentInstallation().getString(Const.USERNAME);

        if(installationUsername==null || !installationUsername.equals(manager.getUsername()))
        {
            ConnectionDetector detector=new ConnectionDetector(this);
            if(detector.isConnectingToInternet())
            {
                final SweetAlertDialog pDialog= dialogWizard.showSimpleProgress(getString(R.string.please_wait),"Updating details");
                manager.getInstallationToSave().saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        pDialog.dismiss();
                        if(e==null)
                        {
                            manager.successToast("Updated");
                        }
                        else ErrorHandler.handleError(context,e);

                    }
                });

            }
            else manager.errorToast("Not connected to the internet");

        }
    }


    private void checkPermissions()
    {// The request code used in ActivityCompat.requestPermissions()
// and returned in the Activity's onRequestPermissionsResult()
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_CONTACTS, Manifest.permission.CAMERA};

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

    }


    private void showDownloadNewVersion()
    {
        final SessionManager sessionManager=new SessionManager(this);

        ParseConfig config=ParseConfig.getCurrentConfig();
        try {
            new SweetAlertDialog(Domicile.this, SweetAlertDialog.WARNING_TYPE).setTitleText("New Version Available: ")
                    .setConfirmText(context.getString(R.string.update)).setCancelText("Not Now?").setContentText("Version " + config.getString(Const.VERSION) + "\n " +
                    "Having the latest version guarantees you the maximum security and the latest features. Please Download now").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sessionManager.setUpdateNoRemind(0);
                    sweetAlertDialog.dismiss();
                    sessionManager.directToGooglePlay();
                }
            }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.dismiss();
                    sessionManager.setUpdateNoRemind(System.currentTimeMillis());
                    sessionManager.longToast("You will be reminded later. To turn off update dialogs, go to the settings page");
                }
            }).show();
        }
        catch(Exception e )
        {
            e.printStackTrace();
        }
        showDownloadNewVersion=false;
    }

    private void showRate()
    {
        manager.showRateUsDialog();

    }
    private void startTutorial(BoomMenuButton boomMenuButton)
    {
        final String feed="JabuWings feed contains all posts across the social network.";
        final String friends="All your friends on JabuWings wil appear her. Note that Lisa is a chat robot designed by JabuWings.";
        final String hubs="Hubs are like groups where you can chat and socialise together.";
        final String acad="Here. You have access to the extra academic benefits of the social network.";
        final String other="To access your profile, click the profile image on the toolbar above. \n" +
                " To perform several actions like adding friends, and so on, press the centre button below. And finally, to get help, and change somethings, press the button at the left top corner.";
        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500); // half second between each showcase view

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(Domicile.this);

        sequence.setConfig(config);

        sequence.addSequenceItem(space,
                acad,"GOT IT");

        sequence.addSequenceItem(space,
                feed, "GOT IT");

        sequence.addSequenceItem(space,
                friends, "GOT IT");

        sequence.addSequenceItem(space,
                hubs, "GOT IT");

        sequence.addSequenceItem(boomMenuButton,
                other, "GOT IT");

        sequence.start();
        viewPager.setCurrentItem(0);

        sequence.setOnItemShownListener(new MaterialShowcaseSequence.OnSequenceItemShownListener() {
            @Override
            public void onShow(MaterialShowcaseView materialShowcaseView, int i) {
                showCasePosition++;
                switch (showCasePosition)
                {
                    case 1:
                        viewPager.setCurrentItem(1);
                        break;
                    case 2:
                        viewPager.setCurrentItem(2);
                        break;
                    case 3:
                        viewPager.setCurrentItem(3);
                        break;
                    case 4:
                        viewPager.setCurrentItem(1);
                        break;
                }
            }
        });
    }
    private void showTutorial()
    {

        if(space.isBoomReady()) startTutorial(boom);
        else
        space.setOnBoomReadyListener(new OnBoomReady() {
            @Override
            public void onBoomReady(BoomMenuButton boomMenuButton) {
                startTutorial(boomMenuButton);
            }
        });

        
    }
    private void getBadges()
    {


        try {
            int friendsBadges=manager.getDatabaseManager().getFriendsNewMessagesCount();
            int hubsBadges=manager.getDatabaseManager().getHubsNewMessagesCount();
            if(friendsBadges!=0)
            space.showBadgeAtIndex(2, friendsBadges, Color.RED);
            else
            space.hideBudgeAtIndex(2);
            if(hubsBadges!=0)
            space.showBadgeAtIndex(3, hubsBadges, Color.RED);
            else
                space.hideBudgeAtIndex(3);
        }
        catch (IndexOutOfBoundsException e)
        {e.printStackTrace();}


        try{
            ParseQuery<ParseObject> query= new ParseQuery<>(Const.LOCAL_NOTIFICATIONS).fromLocalDatastore();
            query.whereEqualTo(Const.USER,new Manager(context).getUser());
            query.whereEqualTo(Const.VALID,true);
            query.whereEqualTo(Const.MESSAGE_READ,false);
            query.setLimit(100);
            int notCount=query.count();
            if(notificationsBadge!=null && notCount!=0){
            notificationsBadge.setVisibility(View.VISIBLE);
            notificationsBadge.setText(String.valueOf(notCount));}
        }
        catch (ParseException e){e.printStackTrace();}







    }
    private void initViews()
    {

        space= (SpaceNavigationView)findViewById(R.id.space);

        space.setSpaceItemTextSize(20);
        space.setFont(manager.getTypeFace());
        space.addSpaceItem(new SpaceItem("Acad", R.drawable.ic_school_white_24dp));
        space.addSpaceItem(new SpaceItem("Feed", R.drawable.ic_view_headline_white_24dp));
        space.addSpaceItem(new SpaceItem("Chats", R.drawable.ic_chat_bubble_outline_white_18dp));
        space.addSpaceItem(new SpaceItem("Hubs", R.drawable.ic_forum_white_24dp));




        boom=space.getBoom();
        setupViewPager(viewPager);
        space.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {

            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                viewPager.setCurrentItem(itemIndex,false);

            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {

            }
        });
        space.setOnBoomClickListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton, BoomMenuButton menuButton) {
                switch (index)
                {
                    case 0:
                        String[] options={"Add a friend","Check friend requests"};
                        new MaterialDialog.Builder(Domicile.this)
                                .title(R.string.friends)
                                .items(options)
                                .itemsCallback(new MaterialDialog.ListCallback() {
                                    @Override
                                    public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                                        dialog.dismiss();
                                        switch (position)
                                        {
                                            case 0:
                                                addFriend();
                                                break;
                                            case 1:
                                                getFriendRequests();
                                                break;
                                        }
                                    }
                                }).show();
                        break;
                    case 1:
                        hubs();
                        break;
                    case 2:
                        startActivity(new Intent(context,Channels.class));
                        break;
                    case 3:
                        startActivity(new Intent(context,Football.class));
                        break;
                    case 4:
                        startActivity(new Intent(context,ChapelHome.class));
                        break;
                    case 5:
                        //dialogWizard.showSuccessDialog("Vote","Express your opinion.\n#Coming Soon");
                        startActivity(new Intent(context, VotePre.class).putExtra("linked",false));
                        break;
                    case 6:
                        startActivity(new Intent(context,FeedMonger2.class));
                        break;
                    case 7:
                        if(manager.getUserType()==Const.REGISTRATION_TYPE.STUDENT.ordinal() || manager.getUserType()==Const.REGISTRATION_TYPE.STAFF.ordinal() )
                            startActivity(new Intent(Domicile.this,Medical.class));
                        else manager.shortToast("Medical services only available to students and staff.");
                        break;
                    case 8:
                        if(manager.getUserType()==0)
                        {
                            String[] options1={"Report an issue to the Student's Union","Report an issue about this app."};
                            new MaterialDialog.Builder(Domicile.this)
                                    .title(R.string.report)
                                    .items(options1)
                                    .itemsCallback(new MaterialDialog.ListCallback() {
                                        @Override
                                        public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                                            dialog.dismiss();
                                            switch (position)
                                            {
                                                case 0:
                                                    startActivity(new Intent(context,ReportToStudentsUnion.class));
                                                    break;
                                                case 1:
                                                    startActivity(new Intent(context,ReportAProblem.class));
                                                    break;
                                            }
                                        }
                                    }).show();

                        }
                        else
                        startActivity(new Intent(context,ReportAProblem.class));
                        break;
                }
            }

            @Override
            public void onBackgroundClick() {

            }

            @Override
            public void onBoomWillHide() {

            }

            @Override
            public void onBoomDidHide() {

            }

            @Override
            public void onBoomWillShow() {

            }

            @Override
            public void onBoomDidShow() {

            }
        });


        //profilePicture=(CircleImage)findViewById(R.id.coreProfilePicture);profilePicture.setOnClickListener(this);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);


        Typeface font= Typeface.createFromAsset(context.getAssets(),"fonts/DancingScript-Regular.otf");
        tvTitle.setTypeface(font);
        tvTitle.setText(R.string.app_name);

        setUpBoom();


        List<String> friends=manager.getDatabaseManager().getFriends();

        String[] suggestions=new String[friends.size()];
        for(int i=0; i<=friends.size()-1; i++) suggestions[i]=friends.get(i);

        /*searchView.setSuggestions(suggestions);
        searchView.setHint("Enter Username or Id");

        searchView.setSubmitOnClick(true);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                    String friendUsername=query;
                    if (friendUsername.equals("lisa")) {
                        startActivity(new Intent(context,
                                ChatWithLisa.class).putExtra(
                                Const.FRIEND_ID, friendUsername));
                    } else {
                        startActivity(new Intent(context,
                                Chat.class).putExtra(
                                Const.FRIEND_ID, friendUsername));
                    }

                //Do some magic
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });*/

        if(share) {
            manager.longToast("Select Friend or hub to share with");
            Chat.shareIntent=getIntent();
            Chat.share=true;
            HubChat.shareIntent=getIntent();
            HubChat.share=true;
            share=false;
        }

    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
        //fabs.collapse();
        //FriendsContextMenuManager.getInstance().hideContextMenu();
        //return true;
    }




    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode)
        {
            case KeyEvent.KEYCODE_MENU:
                /*
                if(drawer.isDrawerOpen(Gravity.LEFT))
                closeDrawer();
                else
                openDrawer();
                return true;
                */
            default:
                return super.onKeyDown(keyCode, event);
        }

    }

    public void showLikedSnackbar()

    {
        //Snackbar.make(drawer,"Liked!",Snackbar.LENGTH_SHORT);
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId())

        {
            /*case R.id.rv_drawer_header:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        int[] startingLocation = new int[2];
                        v.getLocationOnScreen(startingLocation);
                        startingLocation[0] += v.getWidth() / 2;

                        MyProfile.startUserProfileFromLocation(startingLocation, Domicile.this);
                        overridePendingTransition(0, 0);
                    }
                }, 200);
                break;
            case R.id.coreProfilePicture:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        int[] startingLocation = new int[2];
                        v.getLocationOnScreen(startingLocation);
                        startingLocation[0] += v.getWidth() / 2;

                        MyProfile.startUserProfileFromLocation(startingLocation, Domicile.this);
                        overridePendingTransition(0, 0);
                    }
                }, 200);
                break;*/


        }
    }

    public static Toolbar getToolbar()
    {
        return pToolbar;
    }


    private void getFriendRequests()
    {
        startActivity(new Intent(this, FriendRequests.class));
    }
    private void createHub()
    {startActivity(new Intent(Domicile.this, HubCreationWizard.class));}

    private void hubs(){
        String[] options={"Join Hub","Create Hub"};
        new MaterialDialog.Builder(this)
                .title(R.string.hubs)
                .items(options)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                        dialog.dismiss();
                        switch (position)
                        {
                            case 0:
                                new MaterialDialog.Builder(Domicile.this)
                                        .title("Join Hub")
                                        .content("Enter Hub pin")
                                        .inputRange(8,8)
                                        .negativeText(getString(R.string.cancel))
                                        .positiveText(getString(R.string.okay))
                                        .input("Hub pin", null, new MaterialDialog.InputCallback() {
                                            @Override
                                            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                                joinHub(input.toString().toLowerCase());
                                            }
                                        }).show();
                                break;
                            case 1:
                                createHub();
                                break;
                        }
                    }
                }).show();

    }
    public void addFriend()
    {

        String[] options= {"Add from your Contacts"," Add by username","Add by matric no","Add from your department"};
        new MaterialDialog.Builder(this)
                .title("Add friend")
                .items(options)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        dialog.dismiss();
                        switch (which)

                        {
                            case 0:

//                                startActivity(new Intent(Domicile.this, ContactsList.class));
                                Intent contactPicker = new Intent(Domicile.this, ContactPickerActivity.class);
                                startActivityForResult(contactPicker, 15);
                                break;
                            case 1:
                                new MaterialDialog.Builder(Domicile.this)
                                        .title("Username")
                                        .content("What's your friend's username")
                                        .inputType(InputType.TYPE_CLASS_TEXT)
                                        .inputRange(3, 16)
                                        .positiveText("Request")
                                        .input("username",null, false, new MaterialDialog.InputCallback() {
                                            @Override
                                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                                String usernameInput=input.toString().toLowerCase().trim();


                                                if(usernameInput.equals(manager.getUsername()))
                                                {

                                                    manager.errorToast(getString(R.string.error_cannot_add_self));
//                                                    Snackbar.make(boom, getString(R.string.error_cannot_add_self), Snackbar.LENGTH_SHORT).show();
                                                    return;

                                                }


                                                if(isFriendExist(usernameInput))
                                                {
                                                    manager.errorToast(getString(R.string.error_add_friend));
//                                                    Snackbar.make(boom,getString(R.string.error_add_friend),Snackbar.LENGTH_SHORT).show();


                                                    //Toast.makeText(getApplicationContext(),getString(R.string.error_add_friend),Toast.LENGTH_SHORT).show();
                                                }
                                                else {

                                                    manager.shortToast(getString(R.string.attempting_to_send_request)+" "+input.toString());
//                                                    Snackbar.make(boom, getString(R.string.attempting_to_send_request)+usernameInput, Snackbar.LENGTH_SHORT).show();
                                                    HotSpot.sendFriendRequest(Domicile.this, input.toString(), "Username");

                                                }

                                            }
                                        }).show();


                                break;
                            case 2:
                                new MaterialDialog.Builder(Domicile.this)
                                        .title("Matric No")
                                        .content("What's your friend's Matric No")
                                        .inputType(InputType.TYPE_CLASS_TEXT)
                                        .inputRange(3, 11)
                                        .positiveText("Request")
                                        .input("matric",null, false, new MaterialDialog.InputCallback() {
                                            @Override
                                            public void onInput(MaterialDialog dialog, CharSequence input) {



                                                if(input.toString().toLowerCase().equals(manager.getMatricNo()))
                                                {
                                                    manager.errorToast(getString(R.string.error_cannot_add_self));
                                                    //Snackbar.make(boom,getString(R.string.error_cannot_add_self),Snackbar.LENGTH_SHORT).show();
                                                    return;

                                                }


                                                if(isMatricExist(input.toString().toLowerCase()))
                                                {
                                                    manager.errorToast(getString(R.string.error_add_friend));
//                                                    Snackbar.make(boom,getString(R.string.error_add_friend),Snackbar.LENGTH_SHORT).show();


                                                    //Toast.makeText(getApplicationContext(),getString(R.string.error_add_friend),Toast.LENGTH_SHORT).show();
                                                }
                                                else {
                                                    manager.shortToast(getString(R.string.attempting_to_send_request)+input.toString());
//                                                    Snackbar.make(boom, getString(R.string.attempting_to_send_request)+input.toString(), Snackbar.LENGTH_SHORT).show();
                                                    HotSpot.sendFriendRequestWithMatricNo(Domicile.this, new MatricNo(input.toString())
                                                            , manager.getUsername(), manager.getName()
                                                            , manager.getDepartment(), manager.getLevel(), "Matric No");

                                                }

                                            }
                                        }).show();
                                break;
                            case 3:
                                if(manager.getDepartment()!=null)
                                {
                                    MultipleUsersSearch.query= JabuWingsPublicUser.getQuery().whereEqualTo(Const.DEPARTMENT,manager.getDepartment())
                                            .whereNotEqualTo(Const.USERNAME,manager.getUsername())
                                            .whereNotContainedIn(Const.USERNAME,manager.getFriendUsernames());
                                    MultipleUsersSearch.title=manager.getDepartment();
                                    MultipleUsersSearch.error="We didn't find anyone from your department who isn't already your friend";
                                    startActivity(new Intent(Domicile.this, MultipleUsersSearch.class));
                                }
                                else manager.shortToast("You do not belong to any department");
                                break;

                        }
                    }
                })
                .show();





    }

    private void joinHub(String pin)
    {
        HotSpot.joinHub(Domicile.this, pin);

    }



    public boolean isFriendExist(String username){
        List<String> friends= manager.getUserFriends();
        if(friends!=null)
        return friends.contains(username);
        else{
            return false;
        }
    }

    public boolean isMatricExist(String matricNo){
        return new DatabaseManager(context).getFriendWithMatricNo(matricNo)!=null;
    }



    private void setUpNavBar()
    {


    }

    private void setFeedBadge(String badge)
    {
        setModelBadge(feedModel,badge);
    }

    private void setNotificationsBadge(String badge)
    {
        setModelBadge(notificationsModel,badge);
    }

    private void setFriendsBadge(String badge)
    {
        setModelBadge(friendsModel,badge);

    }
    private void setHubsBadge(String badge)
    {
        setModelBadge(hubsModel,badge);

    }
    private void setAcadBadge(String badge)
    {
        setModelBadge(acadModel,badge);
    }

    private void setModelBadge(NavigationTabBar.Model model, String badge)
    {
        if(model!=null && !badge.equals("0")){model.updateBadgeTitle(badge); model.showBadge();}
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        getBadges();
       /* if(!boomInit)
        {
            setUpBoom();
        }
        boomInit=true;*/

    }
    private void setupViewPager(ViewPager viewPager) {
        space.changeCurrentItem(manager.getHomeScreenPreference());
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        feed =new Feed();
        friends = new Friends();
        hubs= new Hubs();
        acad = new Acad();
        adapter.addFragment(acad,getString(R.string.acad));
        adapter.addFragment(feed,getString(R.string.title_activity_feed));
        adapter.addFragment(friends,getString(R.string.friends));
        adapter.addFragment(hubs,getString(R.string.hubs));

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(manager.getHomeScreenPreference(),false);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                space.changeCurrentItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private String[] Colors = {
            "#F44336",
            "#E91E63",
            "#9C27B0",
            "#2012F3",
            "#03A9F4",
            "#00BCD4",
            "#009688",
            "#4CAF50",
            "#8BC34A",
            "#CDDC39",
            "#FFEB3B",
            "#FFC107",
            "#FF9800",
            "#FF5722",
            "#795548",
            "#9E9E9E",
            "#607D8B"};
    private void setUpBoom()
    {

        int[] drawablesResource2 = new int[]{
                R.drawable.help,
                R.drawable.info,
                R.drawable.settings,
                R.drawable.log_out,
        };
        String[] menuStrings= new String[]{
                getString(R.string.help),
                getString(R.string.title_activity_about),
                getString(R.string.settings),
                getString(R.string.log_out) +" "+"("+manager.getName()+")"
        };
        int[]  colors={R.color.hm1,R.color.hm2,R.color.hm3,R.color.hm4};
        String[] desc= new String[]{"Learn about the app","Who made the app","Change some things","Sign in as another user"};
        for (int i = 0; i < 4; i++)
            boom2.addBuilder( new HamButton.Builder()
                    .normalImageRes(drawablesResource2[i])
                    .normalText(menuStrings[i])
                    .normalColor(Color.parseColor(Colors[i]))
                    .imagePadding(new Rect(30,30,30,30))
                    .typeface(manager.getTypeFace())
                    .subTypeface(manager.getTypeFace())
                    .subNormalText(desc[i]));

        boom2.setOnBoomListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton, BoomMenuButton menuButton) {
                switch (index)
                        {
                            case 0:
                                startActivity(new Intent(context,Help.class));
                                break;
                            case 1:
                                startActivity(new Intent(context,About.class));
                                break;
                            case 2:
                                startActivity(new Intent(context,Control.class));
                                break;
                            case 3:
                                //SweetAlertDialog dialog=new DialogWizard(Domicile.this).showSimpleProgress("Please wait","Logging out");
                                manager.shortToast(R.string.logging_out);
                                manager.logout(Domicile.this);
                                break;

                        }
            }

            @Override
            public void onBackgroundClick() {

            }

            @Override
            public void onBoomWillHide() {

            }

            @Override
            public void onBoomDidHide() {

            }

            @Override
            public void onBoomWillShow() {

            }

            @Override
            public void onBoomDidShow() {

            }
        });
       /* Drawable[] drawables = new Drawable[9];
        Drawable[] drawables2=new Drawable[4];

        int[] drawablesResource2 = new int[]{
                R.drawable.help,
                R.drawable.info,
                R.drawable.settings,
                R.drawable.log_out,
        };
        int[] drawablesResource = new int[]{
                R.drawable.add_friend,
                R.drawable.ic_forum_white_24dp,
                R.drawable.requests,
                R.drawable.classroom,
                R.drawable.chapel,
                R.drawable.directions,
                R.drawable.ic_view_headline_white_24dp,
                R.drawable.medical,
                R.drawable.report_problem
        };
        for (int i = 0; i < 9; i++)
            drawables[i] = ContextCompat.getDrawable(context, drawablesResource[i]);
        for(int x=0; x<4; x++)
        {
            drawables2[x]=ContextCompat.getDrawable(context, drawablesResource2[x]);
        }

        String[] strings = new String[]{
                getString(R.string.add_friend),
                getString(R.string.create_hub),
                getString(R.string.requests),
                getString(R.string.classroom),
                getString(R.string.chapel),
                getString(R.string.directions),
                getString(R.string.post_feed),
                getString(R.string.medical),
                getString(R.string.report)
        };

        String[] menuStrings= new String[]{
                getString(R.string.help),
                getString(R.string.title_activity_about),
                getString(R.string.settings),
                getString(R.string.log_out) +" "+"("+manager.getName()+")"
        };
        final int[][] colors = new int[9][2];
        int[][] colors2= new int[4][2];
        for (int i = 0; i < 9; i++) {
            colors[i][1] = Color.parseColor(Colors[i]);
            colors[i][0] = Util.getInstance().getPressedColor(colors[i][1]);
        }


        for (int x = 0; x < 4; x++) {
            colors2[x][1] = Color.parseColor(Colors[x]);
            colors2[x][0] = Util.getInstance().getPressedColor(colors[x][1]);
        }


*/




//        new BoomMenuButton.Builder()
//                .subButtons(drawables2, colors2, menuStrings)
//                .button(ButtonType.HAM)
//                .boom(BoomType.PARABOLA_2)
//                .place(PlaceType.HAM_4_1)
//                .subButtonsShadow(Util.getInstance().dp2px(2), Util.getInstance().dp2px(2))
//                .onSubButtonClick(new BoomMenuButton.OnSubButtonClickListener() {
//                    @Override
//                    public void onClick(int buttonIndex,BoomMenuButton boomMenuButton) {
//
//                    }
//                })
//                .animator(this)
//                .dim(DimType.DIM_0)
//                .init(boom2);
//
//                TextView[] textViews=boom2.getTextViews();
//
//                Typeface typeface= manager.getTypeFace();
//                for(TextView textView: textViews)
//                {
//                    if(textView!=null)
//                        textView.setTypeface(typeface);
//                }


    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
 
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_domicile, menu);
      /*  MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                searchView.setVisibility(View.VISIBLE);
                searchView.showSearch();
                return true;
            }
        });*/

        MenuItem item1 = menu.findItem(R.id.menu_profile);
        MenuItemCompat.setActionView(item1, R.layout.toolbar_menu_count);
        RelativeLayout layout = (RelativeLayout) MenuItemCompat.getActionView(item1);
        profilePicture=(CircleImage) layout.findViewById(R.id.toolbar_profile);

        ((Button)layout.findViewById(R.id.toolbar_search)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MultipleUsersSearch.title=getString(R.string.results);
                MultipleUsersSearch.error="No results were found";
                onSearchRequested();
            }
        });




        profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                startActivity(new Intent(Domicile.this,MyProfile.class));
            }
        });

        if (newSignIn)
        {
            new ImageDownloader(getApplicationContext(),profilePicture,Const.PROFILE_PICTURE_NAME,null).execute(manager.getProfilePictureUrl());
        }
        else {
            if(manager.getProfilePictureUrl()!=null)
            {
                try{
                 FileInputStream inputStream= openFileInput(Const.PROFILE_PICTURE_NAME);
                    if(inputStream==null)
                    {
                        new ImageDownloader(getApplicationContext(),profilePicture,Const.PROFILE_PICTURE_NAME,null).execute(manager.getProfilePictureUrl());
                    }
                    else{
                        new ImageProcessor(context).loadBitmap(Const.PROFILE_PICTURE_NAME, profilePicture);
                    }
                }
                catch (IOException e)
                {
                    new ImageDownloader(getApplicationContext(),profilePicture,Const.PROFILE_PICTURE_NAME,null).execute(manager.getProfilePictureUrl());
                }
            }

        }

        Button notifications=(Button)layout.findViewById(R.id.toolbar_notifications);

        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context,Notifications.class));
                notificationsBadge.setVisibility(View.GONE);
            }
        });
        notificationsBadge=(TextView)layout.findViewById(R.id.toolbar_notifications_badge);
        getBadges();



        return true;

    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        switch(id)
        {
            case R.id.action_logout:
                manager.outrightLogOut();
                break;
            case R.id.menu_profile:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        int[] startingLocation = new int[2];
                        item.getActionView().getLocationOnScreen(startingLocation);
                        startingLocation[0] += item.getActionView().getWidth() / 2;

                        MyProfile.startUserProfileFromLocation(startingLocation, Domicile.this);
                        overridePendingTransition(0, 0);
                    }
                }, 200);
                break;

        }


        return super.onOptionsItemSelected(item);
    }




    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if(intent.getBooleanExtra(Const.NOTIFICATION_INTENT,false))

        {
            //viewPager.setCurrentItem(2);
        }


    }

    public static void openNotifications()
    {
        openCustom=true;
        customPosition=0;
    }
    public static void openFeed()
    {
        openCustom=true;
        customPosition=1;
    }

    public static void openFriend()
    {
        openCustom=true;
        customPosition=2;

    }
    public static void openHubs()
    {
        openCustom=true;
        customPosition=3;
    }



    @Override
    public void onBackPressed() {
        try {
            boolean spaceDismissed=false,feedDismissed=false,boomDismissed=false,searchClosed=false;

            if(space!=null)
            spaceDismissed=space.dismissBoom();
//            if(feed!=null)
//            feedDismissed=feed.dismissBoom();

//            if (boom2!=null && boom2.isOpen()) {
//               boomDismissed= boom2.dismiss();
//            }
           /* if (searchView!=null && searchView.isSearchOpen()) {
                //searchClosed=true;
                searchView.closeSearch();
            }*/
            if(!spaceDismissed&& !feedDismissed && !boomDismissed && !searchClosed)
            finish();


        }
        catch (Exception e){
            e.printStackTrace();
        }



    }



    @Override
    protected void onDestroy() {
        stopServices();
        manager.logOutSession();
        super.onDestroy();
    }

    private void handleService(boolean keep)
    {
        ProfileWatchDog.setWatchFriends(keep);
    }
    private void stopServices()
    {
       stopService(new Intent(Domicile.this,ProfileWatchDog.class));
        stopService(new Intent(Domicile.this,FeedService.class));
    }

    private void startServices()
    {
        startService(new Intent(Domicile.this,ProfileWatchDog.class));
        startService(new Intent(Domicile.this,FeedService.class));
    }

    @Override
    protected void onPause() {
        handleService(false);
        super.onPause();
    }




    public static void changeBadge(int position, String newBadge) {
       space.showBadgeAtIndex(0,1, 0xf44336);
        space.showBadgeAtIndex(1,20, 0xf44336);
        space.showBadgeAtIndex(2,200, 0xf44336);
        space.showBadgeAtIndex(3, 30, 0xf44336);



    }

    private void changeB(final int position,final int newBadge)
    {
        try {
            switch (position) {
                case 0:
                    notificationsBadge.setVisibility(View.VISIBLE);
                    notificationsBadge.setText(newBadge);
                    break;
                case 1:
                    space.showBadgeAtIndex(1, newBadge, 0xf44336);
                    break;
                case 2:
                    space.showBadgeAtIndex(2, newBadge, 0xf44336);
                    break;
                case 3:
                    space.showBadgeAtIndex(3, newBadge, 0xf44336);
                    break;
                case 4:
                    space.showBadgeAtIndex(0, newBadge, 0xf44336);
                    break;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onBadgeChanged(int position, String newBadge) {
        manager.log("Core badge "+position+" '"+newBadge+"'");
        if(newBadge!=null)
        if(newBadge.equals("0")) { return; }


            if(space.getSpaceSize()!=0) changeB(position,Integer.valueOf(newBadge));





    }
}

