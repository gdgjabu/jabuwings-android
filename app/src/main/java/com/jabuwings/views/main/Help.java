package com.jabuwings.views.main;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.jabuwings.R;
import com.jabuwings.widgets.Web;

public class Help extends JabuWingsActivity {
Web web;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        setUpToolbar(null,getString(R.string.help));
        web =(Web)findViewById(R.id.web_help);
        toolbar.setTitle(getString(R.string.help));
        web.loadUrl("file:///android_asset/help/index.html");
    }
}
