package com.jabuwings.views.main;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.management.AcademicsManager;
import com.jabuwings.management.Manager;
import com.jabuwings.models.TimeTableItem;
import com.jabuwings.utilities.CGPA;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.classroom.CourseRegistration;
import com.jabuwings.views.hooks.IQTest;
import com.jabuwings.views.timetable.TimeKeeper;
import com.jabuwings.widgets.DigitalCircle;
import com.jabuwings.widgets.button.FloatingActionsMenu;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.rey.material.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static com.jabuwings.management.Const.CURRENT_SESSION;

public class AcademicManagerActivity extends JabuWingsActivity {
    @InjectView(R.id.tvAcadManagerInfo)
    TextView tvInfo;
    @InjectView(R.id.tvAcadManagerCourseInfo)
    TextView tvCourseInfo;
    @InjectView(R.id.dc_cgpa)
    DigitalCircle dcCgpa;

    @InjectView(R.id.fabAcadManager)
    FloatingActionsMenu fabs;
    static AcademicsManager academicsManager;
    static DialogWizard dialogWizard;
    static FragmentManager fragmentManager;
    static AcademicManagerActivity instance;
    FrameLayout frameLayout;
    Spinner  spAwards;
    static boolean activityActive=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance=AcademicManagerActivity.this;
        activityActive=true;
        setContentView(R.layout.activity_academic_manager);
        academicsManager= new AcademicsManager(this);
        academicsManager.requireCourseRegistration(AcademicManagerActivity.this,AcademicManagerActivity.class);
        ButterKnife.inject(this);
        initViews();
        frameLayout=(FrameLayout)findViewById(R.id.cgpa_container);
        dialogWizard= new DialogWizard(AcademicManagerActivity.this);
        fragmentManager=getSupportFragmentManager();
        setUpToolbar(null, "Academics Manager");
        String semester = academicsManager.getSemester()==1? getString(R.string.first_semester): getString(R.string.second_semester);
        tvInfo.setText(manager.getConfig().getString(CURRENT_SESSION,context.getString(R.string.school_session)) + " " + semester);


        handleCourses(TimeKeeper.getNextLectures(this));
        float currentCGPA= academicsManager.getCurrentCGPA();


        if(!academicsManager.isActivated())
        {
            complain();
            dcCgpa.setLabel("0");

        }

        else{
         dcCgpa.setCGPAColor(currentCGPA);
        }



    }

    private void handleCourses(List<TimeTableItem> nextCourses)
    {
        if(nextCourses.size()!=0) {
            TimeTableItem nextCourse = nextCourses.get(0);
            String d = "";
            if(nextCourses.size()>0)
            for (int i = 0; i <= nextCourses.size()-1; i++) {
                TimeTableItem course = nextCourses.get(i);
                d += course.getCourseCode() + " " + course.getTimeStart() + "\n";

            }
            String info = "You have " + nextCourse.getCourseCode() + " by " + nextCourse.getTimeStart() + "\n";
            tvCourseInfo.setText(info + d);
        }

        else
        tvCourseInfo.setText("You have no lectures today");


    }

    private void initViews()
    {

        com.jabuwings.widgets.button.FloatingActionButton fabCalculateCGPA= new com.jabuwings.widgets.button.FloatingActionButton(this);
        fabCalculateCGPA.setTitle(getString(R.string.calculate_cgpa));
        fabCalculateCGPA.setSize(com.jabuwings.widgets.button.FloatingActionButton.SIZE_MINI);
        fabCalculateCGPA.setIcon(R.drawable.ic_add);
        fabCalculateCGPA.setColorNormal(R.color.pink);
        fabCalculateCGPA.setColorPressed(R.color.pink_pressed);
        fabCalculateCGPA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                calculateCgpaClick();
            }
        });
        fabs.addButton(fabCalculateCGPA);

        com.jabuwings.widgets.button.FloatingActionButton fabViewCourses= new com.jabuwings.widgets.button.FloatingActionButton(this);
        fabViewCourses.setTitle(getString(R.string.view_courses));
        fabViewCourses.setSize(com.jabuwings.widgets.button.FloatingActionButton.SIZE_MINI);
        fabViewCourses.setIcon(R.drawable.ic_add);
        fabViewCourses.setColorNormal(R.color.pink);
        fabViewCourses.setColorPressed(R.color.pink_pressed);
        fabViewCourses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                viewCoursesClick();
            }
        });
        fabs.addButton(fabViewCourses);


        com.jabuwings.widgets.button.FloatingActionButton fabChangeCGPA= new com.jabuwings.widgets.button.FloatingActionButton(this);
        fabChangeCGPA.setTitle(getString(R.string.change_cgpa));
        fabChangeCGPA.setSize(com.jabuwings.widgets.button.FloatingActionButton.SIZE_MINI);
        fabChangeCGPA.setIcon(R.drawable.ic_add);
        fabChangeCGPA.setColorNormal(R.color.pink);
        fabChangeCGPA.setColorPressed(R.color.pink_pressed);
        fabChangeCGPA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                changeCGPAClick();

            }
        });
        fabs.addButton(fabChangeCGPA);

        com.jabuwings.widgets.button.FloatingActionButton fabIQTest= new com.jabuwings.widgets.button.FloatingActionButton(this);
        fabIQTest.setTitle(getString(R.string.iq_test));
        fabIQTest.setSize(com.jabuwings.widgets.button.FloatingActionButton.SIZE_MINI);
        fabIQTest.setIcon(R.drawable.ic_add);
        fabIQTest.setColorNormal(R.color.pink);
        fabIQTest.setColorPressed(R.color.pink_pressed);
        fabIQTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                finish();startActivity(new Intent(AcademicManagerActivity.this, IQTest.class));

            }
        });
//        fabs.addButton(fabIQTest);

        com.jabuwings.widgets.button.FloatingActionButton fabProposeCGPA= new com.jabuwings.widgets.button.FloatingActionButton(this);
        fabProposeCGPA.setTitle("Propose CGPA");
        fabProposeCGPA.setSize(com.jabuwings.widgets.button.FloatingActionButton.SIZE_MINI);
        fabProposeCGPA.setIcon(R.drawable.ic_add);
        fabProposeCGPA.setColorNormal(R.color.pink);
        fabProposeCGPA.setColorPressed(R.color.pink_pressed);
        fabProposeCGPA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                proposeCGPA();


            }
        });
        //fabs.addButton(fabProposeCGPA);
    }
    private void collapseFab()
    {
        fabs.collapse();
    }

    private void proposeCGPA()
    {
        activityActive=false;
        setTitle("Predict CGPA");
        getFragmentManager().beginTransaction()
                .replace(R.id.cgpa_container, new CGPAPredictor())
                .commit();

    }

    private void viewCoursesClick()
    {
        List<String> courses = academicsManager.getUserCourses();
        String coursesContent="";
        String[] metaTag= courses.get(courses.size()-1).split("_");
        String session,semester;
        if(metaTag.length==2)
        {
        session=metaTag[0];
        semester= metaTag[1].equals("1")? getString(R.string.first_semester) : getString(R.string.second_semester);
            coursesContent+=session+"\n";
            coursesContent+=semester+"\n";

        }
        int position=0;
        int metaTagPosition=courses.size()-1;
        for(String course : courses)
        {
            if (position!=metaTagPosition)
            {
                coursesContent+=course+"\n";
                position++;
            }

        }
        new MaterialDialog.Builder(instance)
                .title("Your courses")
                .content(coursesContent)
                .positiveText(R.string.okay)
                .negativeText("Change courses")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        startActivity(new Intent(instance, CourseRegistration.class));
                    }
                })
        .show();
    }

    private void changeCGPAClick()
    {
        new  MaterialDialog.Builder(instance)
                .title(getString(R.string.change_cgpa))
                .content("Change your current CGPA. Only do this when your current CGPA is incorrect.")
                .inputType(InputType.TYPE_NUMBER_FLAG_DECIMAL)
                .positiveText(getString(R.string.change))
                .negativeText(getString(R.string.cancel))
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .input("New CGPA", String.valueOf(academicsManager.getCurrentCGPA()), false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                        dialog.dismiss();
                        float cgpa = academicsManager.getFloatFromUser(input.toString());
                        academicsManager.storeLatestCGPA(cgpa);
                        dcCgpa.setCGPAColor(cgpa);
                        academicsManager.shortToast("Your CGPA has been successfully changed");

                    }
                }).show();




    }
    private void calculateCgpaClick()
    {
        activityActive=false;
        setTitle("CGPA Wizard");
        getFragmentManager().beginTransaction()
                .replace(R.id.cgpa_container, new CGPACalculator())
                .commit();

    }

    @Override
    public void onBackPressed() {
        if(!activityActive)
        {
            finish();
            recreate();
            setTitle("Academics Manager");
        }
        else{
            super.onBackPressed();
        }
    }

    public void activateAcademicManager(){
        Dialog.Builder builder;

        builder= new SimpleDialog.Builder(R.style.SimpleDialog){
            @Override
            protected void onBuildDone(Dialog dialog) {
                dialog.layoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                spAwards = (Spinner)dialog.findViewById(R.id.aam_award);
                String[] awards = getResources().getStringArray(R.array.awards);
                ArrayAdapter adapter= new ArrayAdapter<>(getApplicationContext(), R.layout.row_spn,awards);
                adapter.setDropDownViewResource(R.layout.row_spn_dropdown);
                spAwards.setAdapter(adapter);
            }

            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                int awardPosition=spAwards.getSelectedItemPosition();
                academicsManager.storeAcademicAwardGoal(awardPosition);
                academicsManager.longToast("Thank you for filling me in. The Academics Manager has been successfully activated");
                super.onPositiveActionClicked(fragment);

            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {

                super.onNegativeActionClicked(fragment);
            }
        };
        builder.title("Your goal")
                .positiveAction("Activate")
                .negativeAction(getString(R.string.cancel))
                .contentView(R.layout.activate_academic_manager);

        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(fragmentManager, null);





    }




    private void complain()
    {
        new MaterialDialog.Builder(AcademicManagerActivity.this)
                .title("Activate me")
                .content("Please follow the steps to activate the academics manager")
                .cancelable(false)
                .negativeText(R.string.i_am_not_interested)
                .positiveText("Let's do it")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new MaterialDialog.Builder(AcademicManagerActivity.this).title("Your CGPA")
                                .content("Please input your current CGPA. This will be used to help the academics manager help you achieve your goals")
                                .cancelable(false)
                                .inputType(InputType.TYPE_NUMBER_FLAG_DECIMAL)
                                .inputRange(3, 4)
                                .negativeText(getString(R.string.cancel))
                                .positiveText("Store")
                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        dialog.dismiss();
                                        finish();
                                    }
                                })
                                .input("Current CGPA", null, false, new MaterialDialog.InputCallback() {
                                    @Override
                                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                        dialog.dismiss();
                                        float cgpa=academicsManager.getFloatFromUser(input.toString());
                                        academicsManager.storeLatestCGPA(cgpa);
                                        dcCgpa.setCGPAColor(Float.valueOf(String.format(Locale.ENGLISH,"%4.3f", cgpa)));
                                        activateAcademicManager();
                                    }
                                }).show();
                    }
                })
                .show();

    }


     public static class CGPACalculator extends JabuWingsFragment2 {

         List<CourseItem> courses= new ArrayList<>();
         List<String> courseCodes= new ArrayList<>();
         View rootView;
         ViewGroup mContainerView;
         LayoutInflater inflater;
         TextView tvEmpty;

         FloatingActionButton fabAddCourse,fabCalculate;
         @Nullable
         @Override
         public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
             rootView= inflater.inflate(R.layout.fragment_cgpa_calculator,container,false);
             this.inflater=inflater;
             setUpFragment(rootView);
             mContainerView=(ViewGroup)findViewById(R.id.container);
             tvEmpty= (TextView)findViewById(R.id.tvEmpty_cgpa);
             fabAddCourse=(FloatingActionButton)findViewById(R.id.btnAddCourse);
             fabCalculate=(FloatingActionButton)findViewById(R.id.btnCalculateCgpa);
             final String[] oneUnitCourses={"PHY 112", "CSC 112"}; final List<String> oneUnitList= Arrays.asList(oneUnitCourses);
             final String[] twoUnitCourses=  {"CSC 111","PHY 123"};
             final List<String> twoUnitList= Arrays.asList(twoUnitCourses);
             final String[] threeUnitCourses={"CHM 111","MAT 111","PHY 111", "BIO 111", "CSC 211", "CSC 212","CSC 213", "CSC 214"};
             final List<String> threeUnitList= Arrays.asList(threeUnitCourses);


             fabAddCourse.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {

                     Dialog.Builder builder;
                     builder = new SimpleDialog.Builder(R.style.SimpleDialog){
                         EditText etCourseCode;
                         Spinner spnCourseUnit,spnCourseGrade;
                         @Override
                         protected void onBuildDone(Dialog dialog) {
                             dialog.layoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                             etCourseCode=(EditText)dialog.findViewById(R.id.etCourseCode);
                             spnCourseUnit=(Spinner)dialog.findViewById(R.id.spn_cgpa_unit);
                             spnCourseUnit.setSelection(1);
                             spnCourseGrade=(Spinner)dialog.findViewById(R.id.spn_cgpa_grade);
                             etCourseCode.addTextChangedListener(new TextWatcher() {
                                 @Override
                                 public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                 }

                                 @Override
                                 public void onTextChanged(CharSequence s, int start, int before, int count) {
                                     String input=s.toString();
                                     if(twoUnitList.contains(input))
                                     {
                                         spnCourseUnit.setSelection(2);
                                     }
                                     else if(threeUnitList.contains(input))
                                     {
                                         spnCourseUnit.setSelection(3);
                                     }
                                     else if(oneUnitList.contains(input))
                                     {
                                         spnCourseUnit.setSelection(1);
                                     }
                                     else
                                     switch (s.toString())
                                     {
                                         case "GNS":
                                             spnCourseUnit.setSelection(2);
                                             break;
                                     }

                                 }

                                 @Override
                                 public void afterTextChanged(Editable s) {

                                 }
                             });

                             String[] units={"0", "1", "2", "3","4","5","6"};
                             ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(),R.layout.row_spn,units);
                             spnCourseUnit.setAdapter(arrayAdapter);
                             String[] grades={"F","E","D","C","B","A"};
                             ArrayAdapter<String> gradesAdapter = new ArrayAdapter<>(getActivity(),R.layout.row_spn,grades);
                             spnCourseGrade.setAdapter(gradesAdapter);
                             spnCourseGrade.setSelection(3);
                         }

                         @Override
                         public void onPositiveActionClicked(DialogFragment fragment) {
                             String courseCode, courseUnit,courseGrade;
                             courseCode=etCourseCode.getText().toString();
                             courseUnit=(String)spnCourseUnit.getSelectedItem();
                             courseGrade=(String)spnCourseGrade.getSelectedItem();


                             if(TextUtils.isEmpty(courseCode)||TextUtils.isEmpty(courseUnit)||TextUtils.isEmpty(courseGrade))
                             {
                                 new Manager(getActivity()).shortToast("Invalid course");
                             }
                             else{
                                 addCourse(courseCode,Integer.valueOf(courseUnit),courseGrade.charAt(0));
                             }
                             super.onPositiveActionClicked(fragment);
                         }

                         @Override
                         public void onNegativeActionClicked(DialogFragment fragment) {

                             super.onNegativeActionClicked(fragment);
                         }
                     };

                     builder.title("Add course for calculation")
                             .positiveAction("Add")
                             .negativeAction("Cancel")
                             .contentView(R.layout.cgpa_dialog);
                     DialogFragment fragment = DialogFragment.newInstance(builder);
                     fragment.show(fragmentManager,null);


                 }
             });
             fabCalculate.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     Hashtable<String,Integer> coursesTable= new Hashtable<>();
                     char[] grades= new char[courses.size()];
                     int currentElement=0;
                     for(CourseItem courseItem :courses)
                     {
                         String courseCode= courseItem.courseCode;
                         int courseUnit=courseItem.courseUnit;
                         char courseGrade=courseItem.courseGrade;
                         grades[currentElement]=courseGrade;
                         coursesTable.put(courseCode,courseUnit);

                         currentElement++;
                     }

                     if(currentElement!=0)
                     {
                         final CGPA cgpa = new CGPA(coursesTable,grades);
                         double calculatedCgpa=cgpa.getCGPA();
                         String result="The GPA is "+String.format(Locale.ENGLISH,"%4.3f", calculatedCgpa)+". This is a "+determineClass(calculatedCgpa)+" grade.";

                         new MaterialDialog.Builder(instance)
                                 .title("The result")
                                 .content(result)
                                 .positiveText(R.string.okay)
                                 .neutralText("Add to current CGPA")
                                 .onNeutral(new MaterialDialog.SingleButtonCallback() {
                                     @Override
                                     public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                         new MaterialDialog.Builder(instance).title("Your current CGPA")
                                                 .content("Please input your current CGPA to get the new one.")
                                                 .inputType(InputType.TYPE_NUMBER_FLAG_DECIMAL)
                                                 .inputRange(3, 4)
                                                 .neutralText("Use current CGPA " + academicsManager.getCurrentCGPA())
                                                 .positiveText("Calculate")
                                                 .onNeutral(new MaterialDialog.SingleButtonCallback() {
                                                     @Override
                                                     public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                         dialog.dismiss();
                                                         double cumulatedCGPA=cgpa.cumulate(academicsManager.getCurrentCGPA());
                                                         double cgpa=Double.valueOf(String.format(Locale.ENGLISH,"%4.3f", cumulatedCGPA));

                                                         dialogWizard.showSimpleDialog("Final result","The cumulative GPA is "+cgpa+". This has been automatically set as your current CGPA");

                                                         academicsManager.storeLatestCGPA(new Float(cgpa));


                                                     }
                                                 })
                                                 .input("Current CGPA", null, false, new MaterialDialog.InputCallback() {
                                                     @Override
                                                     public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                                         dialog.dismiss();
                                                         float currentCGPA= academicsManager.getFloatFromUser(input.toString());
                                                         double cumulatedCGPA= cgpa.cumulate(currentCGPA);
                                                         dialogWizard.showSimpleDialog("Final result","The cumulative GPA is "+String.format(Locale.ENGLISH,"%4.3f", cumulatedCGPA));
                                                     }
                                                 }).show();
                                     }
                                 })
                                 .show();
                     }

                     else {
                         academicsManager.shortToast("List is empty");
                     }



                 }
             });
             return rootView;
         }



         private String determineClass(double cgpa)
         {
            if(cgpa>=4.5) return "First Class";
            if(cgpa>=3.5) return "Second Class Upper Division";
            if(cgpa>=2.5) return "Second Class Lower Division";
            if(cgpa>=1.5) return "Third class";
            if(cgpa<1.5)  return "Failure. You will not be allowed to graduate with this CGPA";

             else{
                return "(An error occurred)";
            }
         }
         protected void addCourse(final String courseCode,int courseUnit, char courseGrade)
         {

             if(courseCodes.contains(courseCode))
             {
                 academicsManager.shortToast("The course is already on the list");
                 return;
             }
             tvEmpty.setVisibility(View.GONE);
             final ViewGroup newView = (ViewGroup) LayoutInflater.from(getActivity()).inflate(
                     R.layout.cgpa_calculator_item, mContainerView, false);
             ((TextView) newView.findViewById(R.id.tvCourseCode)).setText(courseCode);
             ((TextView) newView.findViewById(R.id.tvCourseGrade)).setText(String.valueOf(courseGrade));
             ((TextView) newView.findViewById(R.id.tvCourseUnit)).setText(String.valueOf(courseUnit));
             ImageButton ibRemove=(ImageButton) newView.findViewById(R.id.bt_cgpa_delete);

             CourseItem item = new CourseItem(courseCode, courseUnit, courseGrade);
                 courseCodes.add(courseCode);
                 courses.add(item);
                 newView.setTag(item);

             // Set a click listener for the "X" button in the row that will remove the row.
             ibRemove.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     // Remove the row from its parent (the container view).
                     // Because mContainerView has android:animateLayoutChanges set to true,
                     // this removal is automatically animated.
                     CourseItem removedTag=(CourseItem)newView.getTag();
                     courses.remove(removedTag);
                     courseCodes.remove(courseCode);
                     mContainerView.removeView(newView);



                     // If there are no rows remaining, show the empty view.
                     if (mContainerView.getChildCount() == 0) {
                         tvEmpty.setVisibility(View.VISIBLE);
                     }
                 }
             });



             mContainerView.addView(newView, 0);
         }


     }

    public static class CGPAPredictor extends CGPACalculator{

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

          List<String> courses = academicsManager.getUserCourses();
            for(String course: courses)
            {

              //  addCourse(course,0,'C');
            }


            return super.onCreateView(inflater, container, savedInstanceState);
        }
    }
    
   static class CourseItem{
        String courseCode;
        int courseUnit;
        char courseGrade;

        public CourseItem(String courseCode,int courseUnit,char courseGrade)
        {
            this.courseCode=courseCode;
            this.courseUnit=courseUnit;
            this.courseGrade=courseGrade;
        }
    }

}
