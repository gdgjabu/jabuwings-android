package com.jabuwings.views.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.encryption.Crypt;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.image.ImageProcessor;
import com.jabuwings.image.SiliCompressor;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.Item;
import com.jabuwings.models.ProfileRecycler;
import com.jabuwings.ultra.draw.Draw;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.hooks.ImageViewer;
import com.jabuwings.widgets.CircleImage;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.jabuwings.widgets.contextmenu.ContextMenuDialogFragment;
import com.jabuwings.widgets.contextmenu.MenuObject;
import com.jabuwings.widgets.contextmenu.MenuParams;
import com.jabuwings.widgets.contextmenu.interfaces.OnMenuItemClickListener;
import com.jabuwings.widgets.contextmenu.interfaces.OnMenuItemLongClickListener;
import com.jabuwings.widgets.croperino.Croperino;
import com.jabuwings.widgets.croperino.CroperinoConfig;
import com.jabuwings.widgets.croperino.CroperinoFileUtil;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MyProfile extends JabuWingsActivity implements View.OnClickListener  ,OnMenuItemClickListener, OnMenuItemLongClickListener {

    private static final String LOG_TAG = MyProfile.class.getSimpleName();

    private static final int USER_OPTIONS_ANIMATION_DELAY = 300;
    private static final Interpolator INTERPOLATOR = new DecelerateInterpolator();
    private static boolean refreshed=false;
    private FragmentManager fragmentManager;
    CircleImage ivUserProfilePhoto;
    ImageView ivUserDisplayPhoto;
    TextView userDepartment;
    TextView userName;
    TextView userUsername;
    ParseObject cardObject;

    TextView userCredit, userHubs;
    TextView userFriendCount;
    MaterialDialog inputDialog;
    SweetAlertDialog progressDialog;
    CardView cardView;
    Manager manager;
    Toolbar toolbar;
    RecyclerView recyclerView;
    View profileRoot, userDetails, userStats;
    DatabaseManager databaseManager;
    ImageView ivUserVerifiedBadge;
    private ContextMenuDialogFragment mMenuDialogFragment;
    String status,matricNo,medicalNo,level,email;
    public static void startUserProfileFromLocation(int[] startingLocation, Activity startingActivity) {
        final String ARG_REVEAL_START_LOCATION = "reveal_start_location";
        Intent intent = new Intent(startingActivity, MyProfile.class);
        intent.putExtra(ARG_REVEAL_START_LOCATION, startingLocation);
        startingActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        setUpToolbar(null, "");
        manager = new Manager(context);
        fragmentManager=getSupportFragmentManager();
        profileRoot = findViewById(R.id.vUserProfileRoot);
        ivUserVerifiedBadge=(ImageView)findViewById(R.id.user_verified_badge);
        databaseManager = manager.getDatabaseManager();
        userDetails = findViewById(R.id.vUserDetails);
        userStats = findViewById(R.id.vUserStats);
        userCredit = (TextView) findViewById(R.id.user_profile_credit);
        userHubs=(TextView) findViewById(R.id.user_profile_hubs);
        userFriendCount = (TextView) findViewById(R.id.user_profile_friends_count);
        userName = (TextView) findViewById(R.id.user_profile_name);
        recyclerView = (RecyclerView) findViewById(R.id.rvUserProfile);
        userDepartment = (TextView) findViewById(R.id.user_profile_department);
        userUsername = (TextView) findViewById(R.id.user_profile_username);
        setUpTextViews(this,userCredit,userFriendCount,userName,userDepartment,userUsername,userHubs);
        initMenuFragment();

        if(manager.isVerified())
        {
            ivUserVerifiedBadge.setVisibility(View.VISIBLE);
            ivUserVerifiedBadge.setOnClickListener(this);
        }
        //ivUserDisplayPhoto = (ImageView)findViewById(R.id.ivUserProfileDisplayPhoto);
        ivUserProfilePhoto = (CircleImage) findViewById(R.id.ivUserProfilePhoto);
        recyclerView.setHorizontalScrollBarEnabled(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        List<Item> items = new ArrayList<>();
        status=manager.getStatus();
        matricNo=manager.getMatricNo();
        medicalNo=manager.getMedicalNo();
        email=manager.getEmail();
        level=manager.getLevel();
        if(status!=null)
        items.add(new Item(getString(R.string.status),status));
        if(matricNo!=null)
        items.add(new Item(getString(R.string.matric_no),matricNo));
        if(level!=null)
            items.add(new Item(getString(R.string.level),level));
        if(medicalNo!=null)
        items.add(new Item(getString(R.string.med_no), medicalNo));
        if(email!=null)
        items.add(new Item(getString(R.string.email),email));
        ProfileRecycler profileRecycler = new ProfileRecycler(items,this);
        recyclerView.setAdapter(profileRecycler);
        ivUserProfilePhoto.setOnClickListener(this);
//        ivUserDisplayPhoto.setOnClickListener(this);

        userName.setText(manager.getName());
        userFriendCount.setText(String.valueOf(databaseManager.getFriendsCount()));
        userCredit.setText(manager.getCredit());
        userDepartment.setText(manager.getDepartment());
        userUsername.setText("@" + manager.getUsername());
        userHubs.setText(String.valueOf(databaseManager.getHubsCount()));
        if(!refreshed)
        ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if(e==null)
                {
                    manager=new Manager(context);
                    refreshed=true;
                    refresh();
                    //userCredit.setText(manager.getCredit());
                    parseUser.pinInBackground();
                    manager.shortToast("Profile Updated");
                }
            }
        });
        new ImageProcessor(this).loadBitmap(Const.PROFILE_PICTURE_NAME, ivUserProfilePhoto);


    }


    private void refresh()
    {
        try{
        recreate();} catch (Exception e){e.printStackTrace();}

    }


    private void animateUserProfileHeader() {
        profileRoot.setTranslationY(-profileRoot.getHeight());
           ivUserProfilePhoto.setTranslationY(-ivUserProfilePhoto.getHeight());
           userDetails.setTranslationY(-cardView.getHeight());
           userStats.setAlpha(0);

        profileRoot.animate().translationY(0).setDuration(300).setInterpolator(INTERPOLATOR);
           ivUserProfilePhoto.animate().translationY(0).setDuration(300).setStartDelay(100).setInterpolator(INTERPOLATOR);
           userDetails.animate().translationY(0).setDuration(300).setStartDelay(200).setInterpolator(INTERPOLATOR);
           userStats.animate().alpha(1).setDuration(200).setStartDelay(400).setInterpolator(INTERPOLATOR).start();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ivUserProfilePhoto:

                String[] options = {"Change Profile Picture","View Profile Picture"};

                new MaterialDialog.Builder(MyProfile.this)
                        .items(options)
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                dialog.dismiss();

                                switch (which)
                                {
                                    case 0:
                                        //Initialize on every usage
                                        new CroperinoConfig("IMG_" + System.currentTimeMillis() + ".jpg", "/JabuWings/Photos", "/sdcard/JabuWings/Pictures");
                                        CroperinoFileUtil.verifyStoragePermissions(MyProfile.this);
                                        CroperinoFileUtil.setupDirectory(MyProfile.this);

                                        //Prepare Chooser (Gallery or Camera)
                                        Croperino.prepareChooser(MyProfile.this, "Capture photo...", ContextCompat.getColor(MyProfile.this, android.R.color.background_dark));

                                        //Prepare Camera
                                        //   try {
                                        //     Croperino.prepareCamera(MyProfile.this);
                                        //} catch(Exception e) {
                                        //   e.printStackTrace();
                                        //}

                                        //Prepare Gallery
                                        //Croperino.prepareGallery(MyProfile.this);
                               /* FishBun.with(MyProfile.this)
                                        .setAlbumThumnaliSize(150)//you can resize album thumnail size
                                        .setActionBarColor((Color.BLUE), Color.BLUE) // actionBar and StatusBar color
                                        //        .setActionBarColor(Color.BLACK)           // only actionbar color
                                        .setPickerCount(1)//you can restrict photo count
                                        // .setArrayPaths(path)//you can choice again.
                                        .setPickerSpanCount(5)
                                        // .setRequestCode(11) //request code is 11. default == Define.ALBUM_REQUEST_CODE(27)
                                        .setCamera(true)//you can use camera
                                        .textOnImagesSelectionLimitReached("Limit Reached!")
                                        .textOnNothingSelected("Nothing Selected")
                                        .setButtonInAlbumActiviy(true)
                                        .setReachLimitAutomaticClose(true)
                                        .startAlbum();*/
                                        break;
                                    case 1:
                                        startActivity(new Intent(MyProfile.this, ImageViewer.class)
                                                .putExtra(Const.VIEW_THIS_IMAGE,getString(R.string.profile_picture))
                                                .putExtra(Const.IS_USER_PROFILE,true)
                                                .putExtra(Const.IMAGE_VIEWER_URI,Const.PROFILE_PICTURE_NAME));
                                        break;
                                    case 2:
                                        startActivity(new Intent(context, Draw.class));
                                        break;
                                }
                            }
                        })
                        .show();



                break;
            case R.id.user_verified_badge:
                switch (manager.getVerifiedAs())
                {
                    case "0":
                        manager.shortToast(getString(R.string.you_verified_as)+" a student");
                        break;
                    case "1":
                        manager.shortToast(getString(R.string.you_verified_as)+" a staff");
                        break;
                    case "2":
                        manager.shortToast(getString(R.string.you_verified_as)+" an alumini");
                        break;
                    default:
                        manager.shortToast(getString(R.string.you_verified_as)+" "+(manager.getVerifiedAs()));
                }
                break;


        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_OK && data!=null)
        {
            switch (requestCode) {
                case CroperinoConfig.REQUEST_TAKE_PHOTO:
                    if (resultCode == Activity.RESULT_OK) {
                    /* Parameters of runCropImage = File, Activity Context, Image is Scalable or Not, Aspect Ratio X, Aspect Ratio Y, Button Bar Color, Background Color */
                        Croperino.runCropImage(CroperinoFileUtil.getmFileTemp(), MyProfile.this, true, 1, 1, 0, 0);
                    }
                    break;
                case CroperinoConfig.REQUEST_PICK_FILE:
                    if (resultCode == Activity.RESULT_OK) {
                        CroperinoFileUtil.newGalleryFile(data, MyProfile.this);
                        Croperino.runCropImage(CroperinoFileUtil.getmFileTemp(), MyProfile.this, true, 1, 1, 0, 0);
                    }
                    break;
                case CroperinoConfig.REQUEST_CROP_PHOTO:
                    if (resultCode == Activity.RESULT_OK) {
                        Uri i = Uri.fromFile(CroperinoFileUtil.getmFileTemp());
                        //ivUserProfilePhoto.setImageURI(i);
                        String p = SiliCompressor.with(this).compress(i.toString());
                        upload(p);

                        //Do saving / uploading of photo method here.
                        //The image file can always be retrieved via CroperinoFileUtil.getmFileTemp()
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void upload(final String path)
    {

        final ParseFile file = new ParseFile(new File(path));

        final SweetAlertDialog dialog = new DialogWizard(MyProfile.this).showProgress("","Uploading your new picture",false);
        file.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                dialog.dismiss();
                if(e==null)
                {

                    HashMap<String,Object> params = new HashMap<String, Object>();
                    params.put(Const.VERSION,manager.getVersion());
                    params.put("picture",file);
                    //params.put(Const.PROFILE_PICTURE_URL, file.getUrl());
                    params.put(Const.TYPE,1);
                    ParseCloud.callFunctionInBackground("userUpdater", params, new FunctionCallback<String>() {
                        @Override
                        public void done(String s, ParseException e) {
                                if(e==null)
                                {
                                    manager.successToast("Profile Picture Updated");

                                    try{
                                        ImageProcessor processor=new ImageProcessor(context);
                                        FileOutputStream pp = context.openFileOutput(Const.PROFILE_PICTURE_NAME, Context.MODE_PRIVATE);
                                        pp.write(processor.getByteFromFile(new File(path)));
                                        pp.close();
                                        processor.removeBitmapFromMemoryCache(Const.PROFILE_PICTURE_NAME);
                                        processor.loadBitmap(Const.PROFILE_PICTURE_NAME, ivUserProfilePhoto);
                                        Domicile.refreshPicture();
                                    }
                                    catch (IOException er)
                                    {
                                        er.printStackTrace();
                                    }

                                }

                                else{
                                    ErrorHandler.handleError(context,e);

                                }

                        }
                    });




                }
                else{
                    ErrorHandler.handleError(MyProfile.this,e);
                }

            }
        }, new ProgressCallback() {
            @Override
            public void done(Integer integer) {

            }
        });




    }

    public void rechargeChrimata()
    {
      inputDialog=  new MaterialDialog.Builder(MyProfile.this)
                .title("Recharge your account")
                .content("Input the recharge pin")
                .inputType(InputType.TYPE_CLASS_NUMBER)
                .inputRange(10, 10)
                .positiveText("Recharge")
                .input("recharge pin", null, false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {


                        inputDialog.dismiss();
                        progressDialog=new DialogWizard(MyProfile.this).showSimpleProgress("Just hold on","We are recharging your account");
                        finalizeRecharge(input.toString());




                }
    } ).show();
    }

    public void finalizeRecharge(String rechargeCard)
    {
        HashMap<String,Object> params = new HashMap<>();
        params.put(Const.USERNAME,manager.getUsername());
        params.put(Const.PIN,rechargeCard);
        params.put(Const.VERSION,context.getString(R.string.version_number));

        ParseCloud.callFunctionInBackground("creditRecharger", params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {
                progressDialog.dismiss();
                if(e==null)
                {
                    try {
                        String[] response = s.split("&");

                        new DialogWizard(MyProfile.this).showSimpleDialog(response[0], response[1]);
                        int newAmount = Integer.valueOf(response[2]);
                        userCredit.setText(response[2]);

                        if (newAmount != 0) {
                            ParseUser user = manager.getUser();
                            user.put(Const.CHRIMATA, newAmount);
                            user.pinInBackground();
                        }
                    }
                    catch (Exception i)
                    {
                        new DialogWizard(MyProfile.this).showSimpleDialog("Result",s);
                    }
                }
                else{
                    ErrorHandler.handleError(context, e);
                }
            }
        });


    }



    public void changeStatus()
    {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.status))
                .content(getString(R.string.new_status))
                .inputType(InputType.TYPE_CLASS_TEXT)
                .inputRange(1, 100)
                .positiveText(getString(R.string.update))
                .negativeText(getString(R.string.cancel))
                .input(getString(R.string.status), manager.getStatus(), false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog,final  CharSequence input) {
                        manager.shortToast("Your status will be updated soon");

                        HashMap<String,Object> params= new HashMap<String, Object>();
                        params.put(Const.STATUS,input.toString());
                        params.put(Const.TYPE,0);

                        ParseCloud.callFunctionInBackground("userUpdater", params, new FunctionCallback<String>() {
                            @Override
                            public void done(String s, ParseException e) {
                                if(e==null)
                                {
                                    ParseUser user =ParseUser.getCurrentUser();
                                    user.put(Const.STATUS,input.toString());
                                    manager.successToast(getString(R.string.status_updated));
                                    refresh();
                                }
                                else{
                                    manager.errorToast("Your status was not updated. Please try again");
                                    //ErrorHandler.handleError(context,e);
                                }

                            }
                        });



                    }
                }).show();
    }
    private void changePassword()
    {
        Dialog.Builder builder;
        builder= new SimpleDialog.Builder(R.style.SimpleDialog){
            EditText etCurrentPassword,etNewPassword,etConfirmNewPassword;
            String password,newPassword,confirmNewPassword;
            @Override
            protected void onBuildDone(Dialog dialog) {
                super.onBuildDone(dialog);

                etCurrentPassword= (EditText)dialog.findViewById(R.id.cpd_password);
                etNewPassword=(EditText)dialog.findViewById(R.id.cpd_new_password);
                etConfirmNewPassword=(EditText)dialog.findViewById(R.id.cpd_confirm_new_password);




            }

            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                password=etCurrentPassword.getText().toString();
                newPassword=etNewPassword.getText().toString();
                confirmNewPassword=etConfirmNewPassword.getText().toString();
                if (!newPassword.equals(confirmNewPassword))
                {
                    manager.shortToast(R.string.error_password_mismatch);
                }
                else if(newPassword.length()<4)
                {
                    manager.shortToast(R.string.error_invalid_password);
                }
                else
                {
                super.onPositiveActionClicked(fragment);
                    SweetAlertDialog dialog=    new DialogWizard(MyProfile.this).showSimpleProgress("Change Password","Attempting to change password");
                    finaliseChangePassword(password, newPassword,dialog);
                }


            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };
        builder.title("Change Password")
                .positiveAction("Change")
                .negativeAction(getString(R.string.cancel))
                .contentView(R.layout.change_password_dialog);
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);
    }

    private void finaliseChangePassword(String currentPassword, final String newPassword, final SweetAlertDialog dialog)
    {
        String flaggedPasswordHash = Crypt.getSHA256(currentPassword);
        String currentPasswordHash=manager.getSHA();
        if(flaggedPasswordHash.equals(currentPasswordHash))
        {
            HashMap<String,Object> params= new HashMap<>();
            params.put(Const.PASSWORD,newPassword);
            params.put("current_password",currentPassword);
            params.put(Const.TYPE,3);

            ParseCloud.callFunctionInBackground(Const.USER_UPDATER, params, new FunctionCallback<String>() {
                @Override
                public void done(String s, ParseException e) {
                    if (e == null) {
                        ParseUser.logInInBackground(manager.getUsername(), newPassword, new LogInCallback() {
                            @Override
                            public void done(ParseUser parseUser, ParseException e) {
                                dialog.dismiss();
                                if (parseUser != null) {
                                    manager.successToast("Password Successfully changed");
                                } else {
                                    manager.shortToast("Please login with your new password");
                                    manager.outrightLogOut();
                                }
                            }
                        });

                    } else
                    {
                        dialog.dismiss();
                        ErrorHandler.handleError(getApplicationContext(), e);


                    }
                }
            });

        }

        else{
            dialog.dismiss();
            manager.shortToast(R.string.error_incorrect_password);
        }

    }
    public void changeMatricNo()
    {

    }

    private void initMenuFragment() {
        MenuParams menuParams = new MenuParams();
        menuParams.setActionBarSize((int) getResources().getDimension(R.dimen.tool_bar_height));
        menuParams.setMenuObjects(getMenuObjects());
        menuParams.setClosableOutside(true);
        mMenuDialogFragment = ContextMenuDialogFragment.newInstance(menuParams);
        mMenuDialogFragment.setItemClickListener(this);
        mMenuDialogFragment.setItemLongClickListener(this);
    }

    @Override
    public void onMenuItemClick(View clickedView, int position) {
        switch (position)
        {
            case 1:
                changeStatus();
                break;
            case 2:
                changePassword();
                break;
            case 3:
                rechargeChrimata();
                break;
            case 4:
                verifyAccount();
                break;
        }
    }
    private void verifyAccount()
    {
        new MaterialDialog.Builder(MyProfile.this).title("Verify your account").content("Please enter your password on JABU portal for "+manager.getMatricNo())
                .input("Jabu Portal password", null, false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                        final SweetAlertDialog sweetAlertDialog=new DialogWizard(MyProfile.this).showProgress(getString(R.string.please_wait),"We are verifying you.",false);
                        String password = input.toString();
                        HashMap<String,Object> params = manager.getDefaultCloudParams();
                        params.put("password",password);
                        ParseCloud.callFunctionInBackground("verifyWithJabuPortal", params, new FunctionCallback<String>() {
                            @Override
                            public void done(String object, ParseException e) {
                                sweetAlertDialog.dismiss();
                                if(e==null)
                                {
                                    manager.successToast("You have been verified");
                                    ParseUser user = manager.getUser();
                                    user.put(Const.VERIFIED_AS, "0");
                                    user.pinInBackground();
                                    ivUserVerifiedBadge.setVisibility(View.VISIBLE);
                                }
                                else ErrorHandler.handleError(context,e);
                            }
                        });
                    }
                }).show();
    }

    @Override
    public void onMenuItemLongClick(View clickedView, int position) {

    }

    private List<MenuObject> getMenuObjects() {
        List<MenuObject> menuObjects = new ArrayList<>();

        MenuObject close = new MenuObject();
        close.setResource(R.drawable.icn_close);

        MenuObject changeStatus = new MenuObject("Change Status");
        changeStatus.setResource(R.drawable.icn_1);


        MenuObject changePassword = new MenuObject("Change Password");
        BitmapDrawable bd = new BitmapDrawable(getResources(),
                BitmapFactory.decodeResource(getResources(), R.drawable.icn_3));
        changePassword.setDrawable(bd);

        MenuObject purchaseChrimata = new MenuObject("Purchase Chrimata");
        MenuObject verifyAcccount = new MenuObject("Verify your account with JABU portal");


        menuObjects.add(close);
        menuObjects.add(changeStatus);
        menuObjects.add(changePassword);
        menuObjects.add(purchaseChrimata);
        if(manager.getUserType()==0 && !manager.isVerified())menuObjects.add(verifyAcccount);
        return menuObjects;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;
            case R.id.context_menu:
                if (fragmentManager.findFragmentByTag(ContextMenuDialogFragment.TAG) == null) {
                    mMenuDialogFragment.show(fragmentManager, ContextMenuDialogFragment.TAG);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_context_menu, menu);
        return true;
    }
}
