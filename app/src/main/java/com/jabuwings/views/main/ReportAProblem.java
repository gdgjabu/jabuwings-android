package com.jabuwings.views.main;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.Utils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class ReportAProblem extends JabuWingsActivity {
EditText etProblemTitle, etProblemDetails;
Button btSubmit;
DialogWizard dialogWizard;
Manager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_aproblem);
        setUpToolbar(null,R.string.report_problem);
        dialogWizard= new DialogWizard(ReportAProblem.this);
        manager= new Manager(ReportAProblem.this);
        etProblemTitle=(EditText)findViewById(R.id.et_problem_title);
        etProblemDetails=(EditText)findViewById(R.id.et_problem_details);
        btSubmit=(Button)findViewById(R.id.bt_submit);
        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(etProblemDetails.getText().toString())|| TextUtils.isEmpty(etProblemTitle.getText().toString()))

                {
                    dialogWizard.showSimpleDialog(getString(R.string.field_missing),getString(R.string.error_report_a_problem));
                }

                else{


                    ParseObject submitObject= new ParseObject(Const.REPORT_PROBLEM_CLASS);
                            submitObject.put(Const.SENDER, ParseUser.getCurrentUser());
                            submitObject.put(Const.TITLE,etProblemTitle.getText().toString());
                            submitObject.put(Const.DETAILS,etProblemDetails.getText().toString());
                            submitObject.put("deviceType",Const.DEVICE_TYPE);
                            submitObject.put(Const.DEVICE_NAME,Utils.getDeviceName());
                            submitObject.put(Const.VERSION,getString(R.string.version_number));
                            submitObject.put(Const.MANUFACTURER, Utils.getManufacturer());
                            submitObject.put(Const.MODEL,Utils.getModel());
                            submitObject.put(Const.API_LEVEL,Utils.getApi());

                            submitObject.saveEventually(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if(e==null)
                                    {

                                    }

                                    else{

                                        Toast.makeText(ReportAProblem.this, getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                    Toast.makeText(ReportAProblem.this, getString(R.string.feedback_thanks), Toast.LENGTH_LONG).show();
                    finish();




                }



            }
        });

    }

}
