package com.jabuwings.views.main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.intelligence.RealTimeMessagingService;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.hooks.ChatBackgroundSelector;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;


public class Control extends JabuWingsActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);
        setUpToolbar(null, R.string.title_activity_control);
        getFragmentManager().beginTransaction()
                .replace(R.id.control_container, new Settings())
                .commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class Settings extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener{
        Manager manager;
        CheckBoxPreference chpLectureNotifications;
        ListPreference lpMessageDeliveryType;
        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            manager=new Manager(getActivity());

            addPreferencesFromResource(R.xml.prefs);

            ListPreference preference=(ListPreference)getPreferenceScreen().findPreference("home_screen");
            preference.setSummary(getString(R.string.pref_home_screen_desc));
            Preference preference1 =getPreferenceScreen().findPreference("chat_background");
            Preference preference2=getPreferenceScreen().findPreference("preferred_typeface");
            Preference instagram=getPreferenceScreen().findPreference("instagram");
            preference2.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(final Preference preference, Object newValue) {
                    int v= Integer.valueOf((String)newValue);

                    Typeface typeface= new Manager(getActivity()).getTypeFace();
                    new MaterialDialog.Builder(getActivity()).typeface(null,typeface).title("Sample").content("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG. the quick brown fox jumps over the lazy dog. 1234567890").positiveText(R.string.confirm).negativeText(R.string.cancel).onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString(Manager.KEY_PREFERRED_TYPEFACE,"0").commit();
                        }
                    }).onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            manager.longToast("You need to restart the app for the changes to be effective.");
                        }
                    }).show();
                    return true;
                }
            });

            instagram.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    if(manager.isInstagramAuthenticated())
                    {
                        String[] options ={"Check Instagram Authentication"};
                        new MaterialDialog.Builder(getActivity()).title("Instagram").items(options).itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                                switch (position)
                                {
                                    case 0:
                                        final SweetAlertDialog sweetAlertDialog=new DialogWizard(getActivity()).showSimpleProgress(getString(R.string.please_wait),"Checking Instagram");
                                        ParseCloud.callFunctionInBackground("check_instagram", manager.getDefaultCloudParams(), new FunctionCallback<String>() {
                                            @Override
                                            public void done(String s, ParseException e) {
                                                sweetAlertDialog.dismiss();
                                                if(e==null)
                                                {
                                                    String[] a= s.split("&");

                                                    if(a[0].equals("1"))
                                                    new DialogWizard(getActivity()).showSuccessDialog("Instagram",a[1]);
                                                    else{
                                                         new SweetAlertDialog(getActivity(),SweetAlertDialog.ERROR_TYPE).setTitleText("Instagram").setContentText(a[1]).setCancelText(getString(R.string.no)).setConfirmText(getString(R.string.yes)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                             @Override
                                                             public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                                 manager.authenticateInstagram();
                                                             }
                                                         }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                             @Override
                                                             public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                                 sweetAlertDialog.dismiss();
                                                             }
                                                         }).show();
                                                    }
                                                }
                                            }
                                        });
                                        break;
                                }
                            }
                        }).show();
                    }
                    else{




                        new SweetAlertDialog(getActivity(),SweetAlertDialog.NORMAL_TYPE).setTitleText("Instagram").setContentText("Add your instagram account so that your instagram posts will automatically appear on JabuWings").setConfirmText(getString(R.string.okay)).setCancelText(getString(R.string.cancel)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                                manager.authenticateInstagram();
                            }
                        }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        }).show();

                    }

                    return true;
                }
            });
            preference1.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {

                    String[] options = {"Choose Background","Remove Background","Custom Background"};

                    new MaterialDialog.Builder(getActivity())
                            .items(options)
                            .itemsCallback(new MaterialDialog.ListCallback() {
                                @Override
                                public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                    dialog.dismiss();
                                    switch (which)
                                    {
                                        case 0:
                                            startActivity(new Intent(getActivity(), ChatBackgroundSelector.class));
                                            break;
                                        case 1:
                                            manager.setCustomBackground(false);
                                            manager.shortToast("Background removed");
                                        case 2:
                                            choosePicture();
                                            break;

                                    }
                                }
                            })
                            .show();
                    return true;
                }
            });
            lpMessageDeliveryType=(ListPreference)getPreferenceScreen().findPreference(Manager.KEY_MESSAGE_DELIVERY_TYPE);
            chpLectureNotifications=(CheckBoxPreference)getPreferenceScreen().findPreference(Manager.KEY_LECTURE_NOTIFICATIONS);
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

        }



        @Override
        public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, String key) {

            switch(key){

                case Manager.KEY_LECTURE_NOTIFICATIONS:
                    if(sharedPreferences.getBoolean(Manager.KEY_LECTURE_NOTIFICATIONS,false))
                    {
                       manager.shortToast("Lecture Notifications enabled");
                       manager.activateLectureReminderService();

                    }
                    else {
                        manager.deactivateLectureReminderService();
                        manager.shortToast("Lecture Notifications disabled, you will no longer receive lecture notifications");
                    }
                    break;
                case Manager.KEY_MESSAGE_DELIVERY_TYPE:
                    if(sharedPreferences.getString(Manager.KEY_MESSAGE_DELIVERY_TYPE,"0").equals("1"))
                    {
                        new DialogWizard(getActivity()).showConfirmationDialog("Review Action",
                                "Real time messaging is for users who can't receive messages using the normal \"Push Messaging\" method. This can be due to the fact that the device does not support our push delivery method. Such devices include BlackBerry 10 and other hybrid android devices that are not fully android.\n Please note that this option costs more data and requires that your phone be connected to our servers at all times.",
                                new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        dialog.dismiss();
                                        manager.longToast("Real time messaging successfully activated");
                                        getActivity().startService(new Intent(getActivity(), RealTimeMessagingService.class));
                                    }
                                }, new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        sharedPreferences.edit().putString(Manager.KEY_PREFERRED_TYPEFACE,"0").commit();
                                        manager.shortToast("Nothing was changed");
                                    }
                                });


                    }

                    else{
                        manager.longToast("You have chosen push messaging, the best option if you are using an android phone.");
                    }
                    break;
                case Manager.KEY_PREFERRED_TYPEFACE:
                    break;
                case Manager.KEY_COLLEGE:
                    manager.setCollege(sharedPreferences.getString(Manager.KEY_COLLEGE,"Natural Sciences"));
                    manager.shortToast("College changed successfully");
                    break;


            }


        }


        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if(resultCode==RESULT_OK && data!=null &&requestCode==Const.PICK_CHAT_BACKGROUND)
            {
                try {
                    //TODO solve plausible out of memory error add crop
                    Bitmap bitmap;
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                    //covert  to bytes[]
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    if (bitmap != null)
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                    FileOutputStream fos = getActivity().openFileOutput(Const.CHAT_BACKGROUND_NAME, Context.MODE_PRIVATE);
                    fos.write(stream.toByteArray());
                    fos.close();
                    manager.setCustomBackground(true);
                    manager.shortToast("Your chat background has been successfully changed");
                }
                catch (IOException | OutOfMemoryError e)
                {
                    manager.errorToast("Unable to set image as background");
                }
            }
            else{
                if(data==null) manager.log("data was null");
                manager.log("result code "+resultCode+" request code"+requestCode);
            }


        }

        public void choosePicture()
        {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Choose Chat background"), Const.PICK_CHAT_BACKGROUND);
        }
    }

}
