package com.jabuwings.views.main;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.database.realm.Friend;
import com.jabuwings.database.realm.Message;
import com.jabuwings.image.SiliCompressor;
import com.jabuwings.intelligence.lisa.Lisa;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.management.Const;
import com.jabuwings.management.CreditManager;
import com.jabuwings.management.Manager;
import com.jabuwings.models.ChatAdapter;
import com.jabuwings.models.JabuWingsFriend;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.FileManager;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.AudioManager;
import com.jabuwings.views.hooks.ProfileOfFriend;
import com.jabuwings.widgets.SwitchButton;
import com.jabuwings.widgets.audiorecorder.AndroidAudioRecorder;
import com.jabuwings.widgets.audiorecorder.model.AudioChannel;
import com.jabuwings.widgets.audiorecorder.model.AudioSampleRate;
import com.jabuwings.widgets.audiorecorder.model.AudioSource;
import com.jabuwings.widgets.filepicker.FilePickerActivity;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.TimePickerDialog;
import com.sangcomz.fishbun.define.Define;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class Chat extends Converse implements View.OnClickListener {
    protected Toolbar toolbar;

    /** The Conversation list. */


    /**
     * The database id of friend.
     */
    private String id;
    String username;

    /**
     * Flag to hold if the activity is running or not.
     */
    private boolean isRunning;
    protected Manager manager;
    FileManager fileManager;
    Time time;
    boolean friendShipActive = true;
    private static final String LOG_TAG = Chat.class.getSimpleName();
    SQLiteDatabase db;
    private static String profileUsername, profileDatabaseId;
    String profileName;
    AudioManager recorder;
    protected boolean privateChat = true;
    DialogWizard dialogWizard;
    static Intent shareIntent;
    static boolean share=false;
    String audioFilePath;
    JabuWingsFriend localFriend;
    ChatAdapter adapter;
    protected Friend friend;
    boolean typing;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        setListener(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        setUpToolbar(this);
        init();
        initViews(getWindow());
        initEditText(getWindow());
        loadPicture(profileUsername,profileName);
        // this makes the smileys ready
        setUpEmoji(getWindow());
        restoreMessage();
        getFriend();
        getShareIntent();
        setUpTyping();
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        //linearLayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(linearLayoutManager);

        final RealmResults<Message> realmResults = realm.where(Message.class)
                .beginGroup()
                .equalTo(Const.RECEIVER,username)
                .equalTo(Const.SENDER,manager.getUsername())
                .endGroup()
                .or()
                .beginGroup()
                .equalTo(Const.RECEIVER,manager.getUsername())
                .equalTo(Const.SENDER,username)
                .endGroup()
                .findAllAsync();
        //realmResults.sort("msgTime");
        /*List<Message> messages=new ArrayList<>();
        for(Message message: realmResults)
        {
            messages.add(message);
        }*/
        
/*
        RealmChangeListener realmListener = new RealmChangeListener() {
            @Override
            public void onChange(Object o) {
                if(adapter!=null){
                    manager.log("adapter change");
                    adapter.notifyDataSetChanged();
                    recyclerView.scrollToPosition(realmResults.size()-1);
                    manager.log("first visible "+linearLayoutManager.findFirstVisibleItemPosition());
                    manager.log("last visible "+linearLayoutManager.findLastVisibleItemPosition());
                    manager.log("adapter size "+adapter.getItemCount());


                }

            }

        };
*/

        OrderedRealmCollectionChangeListener<RealmResults<Message>> callback = new OrderedRealmCollectionChangeListener<RealmResults<Message>>() {
            @Override
            public void onChange(RealmResults<Message> results, OrderedCollectionChangeSet changeSet) {
                if (changeSet == null) {
                    // The first time async returns with an null changeSet.
                    manager.log("Message results "+results.size());
                    adapter= new ChatAdapter(Chat.this, results.sort("msgTime"), realm, friend, new OnChatItemInserted() {
                        @Override
                        public void itemInserted() {
                            manager.log("message adapter change");
                            manager.log("first visible "+linearLayoutManager.findFirstVisibleItemPosition());
                            manager.log("last visible "+linearLayoutManager.findLastVisibleItemPosition());
                            manager.log("adapter size "+adapter.getItemCount());
                            //adapter.notifyDataSetChanged();

                            //If the last chat item at the bottom of the page is visible, scroll to the new item
                            if(adapter.getItemCount() - linearLayoutManager.findLastVisibleItemPosition() <=2)
                                recyclerView.scrollToPosition(realmResults.size()-1);
                        }
                    });
                    recyclerView.setAdapter(adapter);
                    recyclerView.scrollToPosition(realmResults.size()-1);
                } else {
                    // Called on every update.
                   
                }
            }
        };


        //realm.addChangeListener(realmListener);
        realmResults.addChangeListener(callback);

    }

    private void setUpTyping()
    {

        final int TYPING_TIMEOUT = 5000; // 5 seconds timeout
        final Handler timeoutHandler = new Handler();
        final Runnable typingTimeout = new Runnable() {
            public void run() {
                typing =false;
                HotSpot.sendUserTyping(Chat.this,username);
            }
        };

        txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // reset the timeout
                timeoutHandler.removeCallbacks(typingTimeout);

                if (txt.getText().toString().trim().length() > 0) {
                    // schedule the timeout
                    timeoutHandler.postDelayed(typingTimeout, TYPING_TIMEOUT);

                    if (!typing) {
                        typing = true;
                        HotSpot.sendUserTyping(Chat.this,username);
                    }
                }
                else {
                    typing = false;
                    HotSpot.sendUserTyping(Chat.this,username);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void getShareIntent()
    {
        if(share&&shareIntent!=null)
        {
            Intent intent = shareIntent.getParcelableExtra("intent");
            String action = intent.getAction();
            String type = intent.getType();
            if (Intent.ACTION_SEND.equals(action) && type != null) {
                if ("text/plain".equals(type)) {
                    handleSendText(intent); // Handle text being sent
                } else if (type.startsWith("image/")) {
                    handleSendImage(intent); // Handle single image being sent
                }
            } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
                if (type.startsWith("image/")) {
                    handleSendMultipleImages(intent); // Handle multiple images being sent
                }
            } else {
                manager.errorToast("Not supported");
            }
        }

        share=false;
        shareIntent=null;

    }


    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            txt.setText(sharedText);
        }
    }

    void handleSendImage(Intent intent) {
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            String p = SiliCompressor.with(this).compress(imageUri.toString());
            saveAndSendFileToServer("",profileUsername,new File(p),p, DataProvider.MessageType.OUTGOING_IMAGE.ordinal());
        }
    }

    void handleSendMultipleImages(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (imageUris != null) {
            for(Uri uri: imageUris)
            {
                String p = SiliCompressor.with(this).compress(uri.toString());
                saveAndSendFileToServer("",profileUsername,new File(p),p, DataProvider.MessageType.OUTGOING_IMAGE.ordinal());
            }
//           manager.errorToast("Multiple images are currently not supported");
        }
    }
    private void getFriend() {
        JabuWingsFriend.getQuery().whereEqualTo(Const.USERNAME, profileUsername).getFirstInBackground(new GetCallback<JabuWingsFriend>() {
            @Override
            public void done(JabuWingsFriend friend, ParseException e) {
                if (e == null) {
                    friend.pinInBackground();
                    if (!friend.isFriendOf(manager.getUsername())) {
                        manager.getDatabaseManager().updateFriendshipStatus(profileUsername, Const.FRIENDSHIP_STATUS.REMOVED.ordinal());
                    } else {
                        manager.getDatabaseManager().updateFriendshipStatus(profileUsername, Const.FRIENDSHIP_STATUS.ACTIVE.ordinal());
                    }
                    if (friend.isOnline()) {
                        setSubtitle("online");
                    } else {
                        try {
                            if(friend.getLastSeen()!=null)
                            setSubtitle("Last seen " + time.parseTime(friend.getLastSeen()));
                        } catch (NullPointerException er) {
                            er.printStackTrace();
                        }

                    }

                }

            }
        });

        JabuWingsFriend.getQuery().fromLocalDatastore().whereEqualTo(Const.USERNAME,profileUsername).getFirstInBackground(new GetCallback<JabuWingsFriend>() {
            @Override
            public void done(JabuWingsFriend friend, ParseException e) {
                if(e==null)
                {
                    localFriend = friend;
                }
                else e.printStackTrace();
            }
        });
    }


    private void restoreMessage() {
        String msg = manager.getEntityLastDraft(profileUsername);
        if (!TextUtils.isEmpty(msg)) {
            txt.setText(msg);
        }

        txt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    processSend();
                    handled = true;
                }
                return handled;
            }
        });

    }


    private void init() {
        manager = new Manager(context);
        db = new DatabaseManager(this).getWritableDatabase();
        fileManager = new FileManager(getApplicationContext());
        dialogWizard=new DialogWizard(Chat.this);
        time = new Time();

        if (privateChat) {
            username = getIntent().getStringExtra(Const.FRIEND_ID);
            friend = realm.where(Friend.class).equalTo(Const.USERNAME,username).findFirst();

            friend.addChangeListener(new RealmChangeListener<Friend>() {
                @Override
                public void onChange(Friend friend) {
                    if(friend.isTyping())
                    {
                        typingText.setText("Typing");
                    }

                }
            });
            id = friend.getUsername();
            profileUsername = friend.getUsername();
            profileName = friend.getName();
            int friendshipStatus = friend.getFriendShipStatus();
            if (friendshipStatus != Const.FRIENDSHIP_STATUS.ACTIVE.ordinal()) {
                friendShipActive = false;
            }
            setTitle(profileName);
            setSubtitle(friend.getStatus());

        }

    }


    /* (non-Javadoc)
         * @see android.support.v4.app.FragmentActivity#onResume()
         */
    @Override
    protected void onResume() {
        super.onResume();
        isRunning = true;

    }

    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onPause()
     */
    @Override
    protected void onPause() {
        String msg = txt.getText().toString();
        manager.storeLastEntityDraft(profileUsername, msg);
//        overridePendingTransition(R.anim.fade_scale_in, R.anim.slide_to_right);
        overridePendingTransition(0, R.anim.slide_down1);
       /* ContentValues values = new ContentValues(1);
        values.put(DataProvider.COL_COUNT, 0);
        getContentResolver().update(Uri.withAppendedPath(DataProvider.CONTENT_URI_FRIENDS, id), values, null, null);
        getContentResolver().notifyChange(DataProvider.CONTENT_URI_FRIENDS, null);*/

        isRunning = false;
        super.onPause();
    }


    private void send(final String msgToBeSent) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {


                    HotSpot.sendPersonalMessage(context, manager.getUsername(),
                            profileUsername, msgToBeSent.trim(), true);

                } catch (Exception ex) {
                    ex.printStackTrace();
                    msg = "Message could not be sent" + ex;
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (!TextUtils.isEmpty(msg)) {
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                }
            }
        }.execute(null, null, null);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnSend:
                if (send.getState() == SwitchButton.FIRST_STATE) {
                    if (friendShipActive) {
                       // audioFilePath = Environment.getExternalStorageDirectory() + "/JabuWings/JabuWings Audio/"+username+"/"+Lisa.generateAudioFileName(username)+".wav";
                        new File(Environment.getExternalStorageDirectory() + "/JabuWings/JabuWings Audio/"+username+"/").mkdirs();
                        audioFilePath = Environment.getExternalStorageDirectory() + "/JabuWings/JabuWings Audio/"+username+"/"+Lisa.generateAudioFileName(username)+".wav";

                        int color = getResources().getColor(R.color.colorPrimaryDark);
                        int requestCode = 234;
                        AndroidAudioRecorder.with(this)
                                // Required
                                .setFilePath(audioFilePath)
                                .setColor(color)
                                .setRequestCode(requestCode)

                                // Optional
                                .setSource(AudioSource.MIC)
                                .setTitle("Send to "+profileUsername)
                                .setChannel(AudioChannel.STEREO)
                                .setSampleRate(AudioSampleRate.HZ_8000)

                                // Start recording
                                .record();
                        //recorder = AudioManager.getInstance().initialise(Chat.this);
//                        recorder.initialiseRecorder(profileUsername, getSupportFragmentManager());
                    } else {
                        flagFriendship();
                    }


                } else {
                    processSend();
                }
                break;
            case R.id.toolbar:
                startActivity(new Intent(this, ProfileOfFriend.class).putExtra(Const.USERNAME, profileUsername));
                break;
            case R.id.iv_chat_share_pic:
                if (friendShipActive)
                    shareImage();
                else flagFriendship();
                break;
            case R.id.iv_chat_share_cam:
                if (friendShipActive)
                    shareCameraPhoto();
                else flagFriendship();
                break;
            case R.id.iv_chat_send_scheduled:
                if (friendShipActive) {
                    Dialog.Builder builder;
                    builder = new TimePickerDialog.Builder(time.getHour(), time.getMinute()) {
                        @Override
                        public void onPositiveActionClicked(DialogFragment fragment) {
                            TimePickerDialog dialog = (TimePickerDialog) fragment.getDialog();
                            String msg = txt.getText().toString();
                            if (TextUtils.isEmpty(msg)) {
                                manager.shortToast("Please type your message first");
                            } else {
                                txt.setText(null);
                                HotSpot.sendPersonalMessage(context, manager.getUsername(), profileUsername, msg, false, dialog.getHour(), dialog.getMinute());
                                new DialogWizard(Chat.this).showSimpleDialog("Timed message", "Your message will be sent at " + time.parseTime(dialog.getFormattedTime(time.getFormat())) + ". Please note that this feature is still under review and we cannot guarantee that the message will be sent at the exact time");
                            }
                            super.onPositiveActionClicked(fragment);
                        }

                        @Override
                        public void onNegativeActionClicked(DialogFragment fragment) {
                            super.onNegativeActionClicked(fragment);
                        }
                    };

                    builder.positiveAction("OK")
                            .title("Timed message")
                            .negativeAction("CANCEL");
                    DialogFragment fragment = DialogFragment.newInstance(builder);
                    fragment.show(getSupportFragmentManager(), null);
                } else flagFriendship();
                break;
            case R.id.iv_chat_share_others:
                if (friendShipActive) {

                    String[] options = {"Share File"};
                    new MaterialDialog.Builder(Chat.this)
                            .title(R.string.share_others)
                            .items(options)
                            .itemsCallback(new MaterialDialog.ListCallback() {
                                @Override
                                public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                    dialog.dismiss();
                                    switch (which)
                                    {
                                        case 0:
                                            sendFile();
                                            break;
                                        case 1:
                                            shareFriend();
                                            break;
                                        case 2:
                                            shareChrimata();
                                            break;
                                    }
                                }
                            })
                            .show();


                } else flagFriendship();
                break;


        }
    }

    private void sendFile()
    {
        // This always works
        Intent i = new Intent(context, FilePickerActivity.class);
        // This works if you defined the intent filter
        // Intent i = new Intent(Intent.ACTION_GET_CONTENT);

        // Set these depending on your use case. These are the defaults.
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
        i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);

        // Configure initial directory by specifying a String.
        // You could specify a String like "/storage/emulated/0/", but that can
        // dangerous. Always use Android's API calls to get paths to the SD-card or
        // internal memory.
        i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());

        startActivityForResult(i, Const.PICK_FILE_SHARE);
    }

    private void shareFriend()
    {


    }

    private void shareChrimata()
    {
       new CreditManager(Chat.this).share(profileUsername);
    }

    private void processSend()
    {
        String s = txt.getText().toString();
        if (TextUtils.isEmpty(s)){
            return;
        }
        txt.setText(null);
        if(friendShipActive)
            send(s);
        else
        {
            flagFriendship();
        }
    }

    private void flagFriendship()
    {
        new DialogWizard(Chat.this).showErrorDialog("Removed","You are no longer friends with "+profileUsername);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri=null;
        if(data!=null)
        {uri = data.getData();}
        switch (requestCode)
        {
            case 234:
                if (resultCode == RESULT_OK) {
                    final File file= new File(audioFilePath);
                    saveAndSendFileToServer(null,username,file,audioFilePath, DataProvider.MessageType.OUTGOING_VOICE_NOTE.ordinal());
                } else if (resultCode == RESULT_CANCELED) {
                    // Oops! User has canceled the recording
                }
                break;
            case Const.PICK_FILE_SHARE:
                if(resultCode==RESULT_OK && data!=null) {
                    File file = new File(uri.getPath());
                    if (file.length() > 10485760L)
                        dialogWizard.showSimpleDialog("File too large", "The file must not be more than 10 megabytes");
                    else
                        saveAndSendFileToServer(txt.getText().toString(),username,file,file.getPath(),DataProvider.MessageType.OUTGOING_FILE.ordinal());
                }
                break;
            case Define.ALBUM_REQUEST_CODE:
                if(resultCode==RESULT_OK && data!=null) {
                    ArrayList<Uri> path =   data.getParcelableArrayListExtra(Define.INTENT_PATH);
                    if (path.size() > 0) {
                        String p = SiliCompressor.with(this).compress(path.get(0).toString());
                        saveAndSendFileToServer(txt.getText().toString(), profileUsername, new File(p), p, DataProvider.MessageType.OUTGOING_IMAGE.ordinal());
                    }
                }
                break;
            case Const.PICK_CAMERA_SHARE_ID:
                File image;
                File sd = Environment.getExternalStorageDirectory();
                File dir;
                try{
                    dir = new File(sd.getAbsolutePath()+Const.PUBLIC_IMAGE_DIRECTORY);
                    dir.mkdirs();
                }
                catch (Exception e)
                {
                    manager.shortToast(R.string.error_phone_storage);
                    return;
                }

                image = new File(dir, Lisa.generateImageFileName(profileUsername)+".jpg");

               //manager.log(LOG_TAG,"URI "+data.toString());
                //  new PictureUploader(context,null,null,Const.MEDIA_UPLOAD_TYPE.MESSAGE_PICTURE.ordinal(),profileUsername).execute(uri);

                try{
                    Bitmap bitmap;
                        bitmap=(Bitmap) data.getExtras().get(Const.DATA);
                    //covert  to bytes[]
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    if(bitmap!=null) {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                        String path= MediaStore.Images.Media.insertImage(context.getContentResolver(),bitmap,Lisa.generateImageFileName(manager.getUsername()),null);
                        Uri u= Uri.parse(path);
                        Cursor cursor = getContentResolver().query(u,null,null,null,null);
                        cursor.moveToFirst();
                        path=cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA));
                        cursor.close();
                        String p=SiliCompressor.with(context).compress(path);
                        saveAndSendFileToServer(txt.getText().toString(), profileUsername, new File(p), p, DataProvider.MessageType.OUTGOING_IMAGE.ordinal());
                    }



                /*    FileOutputStream fos = new FileOutputStream
                            (image);

                    fos.write(stream.toByteArray());
                    fos.close();
                    // saveAndSendFileToServer(txt.getText().toString(),profileUsername,stream.toByteArray(),image.getPath());*/
                    txt.setText(null);


                }
                catch (OutOfMemoryError e){
                    e.printStackTrace();
                    String error= e.getMessage();
                    Toast.makeText(getApplicationContext(), "Memory error: " + error, Toast.LENGTH_LONG).show();
                }
                catch (NullPointerException e)
                {
                    manager.errorToast("No image obtained");
                }
                break;
            default:
                if(resultCode==RESULT_OK && data!=null)
                {

                    if(requestCode==Const.PICK_IMAGE_SHARE_ID || requestCode==Const.PICK_CAMERA_SHARE_ID )
                    {


                    }


                }
                else {
                    manager.shortToast(R.string.error_nothing_was_obtained);
                }

        }




    }

    public  void saveAndSendFileToServer(final String msg, final String receiver, final  File image, final String path, final int type)
    {

      final   ParseFile file= new ParseFile(image);
      final Message message=HouseKeeping.storeOutgoingPersonalMedia(context, path, msg, receiver, type);
      final String messageId = message.getMsgId();





        file.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                int messageType=0;
                if (e == null) {
                    manager.log("File Message uploaded");
                    if(type==DataProvider.MessageType.OUTGOING_IMAGE.ordinal())messageType=1;
                    else if(type==DataProvider.MessageType.OUTGOING_VOICE_NOTE.ordinal()) messageType=2;
                    else if(type==DataProvider.MessageType.OUTGOING_FILE.ordinal()) messageType=3;
                    HotSpot.sendPersonalMedia(messageType, file, image.length(),receiver, getApplicationContext(), messageId,msg,message);
                } else {
                    manager.shortToast(e.getMessage());
                    e.printStackTrace();
                    realm.beginTransaction();
                    message.setDeliveryStatus(Const.MESSAGE_FAILED);
                    realm.commitTransaction();
                }

            }
        }, new ProgressCallback() {
            @Override
            public void done(final Integer percentDone) {
                realm.beginTransaction();
                com.jabuwings.database.realm.File file = message.getFile();
                file.setProgress(percentDone);
                realm.commitTransaction();

                manager.log(LOG_TAG, "Progress " + percentDone);
            }
        });


    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {

        String currentMessage=txt.getText().toString();
        if(TextUtils.isEmpty(currentMessage))
        {
            outState.putString(Const.MSG,currentMessage);
        }

        super.onSaveInstanceState(outState);
    }
 

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
               startActivity(new Intent(this,Domicile.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(this,Domicile.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

    }

}





