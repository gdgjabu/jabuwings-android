package com.jabuwings.views.main;

public interface OnBadgeChangedListener{
        void onBadgeChanged(int position,String newBadge);
    }