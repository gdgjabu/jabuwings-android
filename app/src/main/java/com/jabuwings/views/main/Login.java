package com.jabuwings.views.main;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.jabuwings.R;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.encryption.Crypt;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.hooks.SettingThingsUp;
import com.jabuwings.views.registration.Verification;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseACL;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

import java.io.File;
import java.util.HashMap;
import java.util.Locale;

public class Login extends JabuWingsActivity implements View.OnClickListener {
    Button btLogin;
    EditText etUsername;
    EditText etPassword;
    Manager manager;
    DatabaseManager helper;
    SQLiteDatabase db;
    TextView tvRegister,tvForgotPassword;
    ImageView ivLogo;
    private String LOG_TAG= Login.class.getSimpleName();
    boolean showTutorial=false;
    int logoTouch=0;
    AnimationDrawable anim;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            findViewById(android.R.id.content).setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        }

        manager = new Manager(this);
        mAuth = FirebaseAuth.getInstance();

        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        setContentView(R.layout.activity_login);
        setUpToolbar(null,R.string.login);
        showTutorial=getIntent().getBooleanExtra(Const.TUTORIAL,false);
        ivLogo=(ImageView)findViewById(R.id.iv_logo);
        /*Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        ivLogo.startAnimation(slideUp);*/


        ivLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoTouch++;

                    if(logoTouch==15)
                    {
                        manager.shortToast("Seems you like our logo");
                    }

            }
        });
        helper = new DatabaseManager(this);
        db = helper.getWritableDatabase();


        btLogin = (Button) findViewById(R.id.btLogin_login);
        btLogin.setOnClickListener(this);
        etUsername = (EditText) findViewById(R.id.etLogin_username);
        etPassword = (EditText) findViewById(R.id.etLogin_password);
        etPassword.setImeActionLabel("Login",KeyEvent.KEYCODE_ENTER);

        etPassword.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        etPassword.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(event.getAction()==KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_ENTER:
                            attemptLogin();
                            break;
                    }
                }
                return false;
            }
        });
        tvForgotPassword = (TextView) findViewById(R.id.tvForgotPassword);
        tvForgotPassword.setOnClickListener(this);


     /*   if(!manager.isGooglePlayServicesAvailable(this))
        {
            new DialogWizard(Login.this).showSimpleDialog("No Google Play Services","Because you don't have Google Play Services, JabuWings has switched to Real Time Messaging(In place of push messaging) for delivering your messages. You can change this in the settings once you've logged in.");

        }*/


        RelativeLayout container = (RelativeLayout) findViewById(R.id.container);

        anim = (AnimationDrawable) container.getBackground();
        anim.setEnterFadeDuration(3000);
        anim.setExitFadeDuration(1000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (anim != null && !anim.isRunning())
            anim.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (anim != null && anim.isRunning())
            anim.stop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btLogin_login:
                attemptLogin();
                break;
            case R.id.tvForgotPassword:
                new MaterialDialog.Builder(Login.this)
                        .title("Forgot Password")
                        .content("Enter the email associated with your account")
                        .inputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
                        .inputRange(7,100)
                        .positiveText("Reset Password")
                        .input("email", null, false, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(@NonNull MaterialDialog dialog, final CharSequence input) {

                                final SweetAlertDialog sweetAlertDialog=new DialogWizard(Login.this).showProgress(getString(R.string.please_wait),"Requesting a password reset",false);
                                manager.longToast(R.string.please_wait);
                                ParseUser.requestPasswordResetInBackground(input.toString(), new RequestPasswordResetCallback() {
                                    public void done(ParseException e) {
                                        sweetAlertDialog.dismiss();
                                        if (e == null) {
                                            new DialogWizard(Login.this).showSimpleDialog("Password Reset","An email was successfully sent to " + input.toString() + " with reset instructions.");
                                        } else {
                                            ErrorHandler.handleError(context,e);
                                        }
                                    }
                                });

                            }
                        }).show();


                break;

        }
    }

    public void attemptLogin() {
        String id, code;
        clearErrors();

        // Store values at the time of the login attempt.
        id = etUsername.getText().toString().toUpperCase();
        code = etPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password.
        if (TextUtils.isEmpty(code)) {
            etPassword.setError(getString(R.string.error_field_required));
            focusView = etPassword;
            cancel = true;
        }
        // Check for a valid username.
        if (TextUtils.isEmpty(id)) {
            etUsername.setError(getString(R.string.error_field_required));
            focusView = etUsername;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // perform the user login attempt.
            if(manager.getLastUsername()!=null)
            {
                char firstChar=id.charAt(0);

                if(TextUtils.isDigitsOnly(String.valueOf(firstChar)) || firstChar=='F'|| firstChar=='C')
                {
                    tryMatricNo(id,code);
                }
                else
                if(!id.matches("[a-zA-Z0-9_]*")){ tryEmail(id.toLowerCase(),code);}else
                check(id.toLowerCase(Locale.getDefault()), code);
            }
            else{
                char firstChar=id.charAt(0);
                if((TextUtils.isDigitsOnly(String.valueOf(firstChar)) || firstChar=='F'|| firstChar=='C') && id.length()>=10)
                {
                    tryMatricNo(id,code);
                }
                else
                login(id.toLowerCase(),code);
            }
        }
    }



    public void clearErrors() {
        etUsername.setError(null);
        etPassword.setError(null);

    }

    private void check(final String id, final String code)
    {
        String currentUsername=manager.getLastUsername();
        manager.log(LOG_TAG, "Current username " + currentUsername);
        if (currentUsername!=null && !id.equals(currentUsername))

        {

            new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Are you sure?")
                    .setContentText("Logging in will delete all the chat history and settings of the previous user! A backup will be stored on the device storage for retrieval later")
                    .setConfirmText(getString(R.string.login))
                    .setCancelText(getString(R.string.cancel))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            boolean success=manager.backupAndEncryptDatabase();
                            if(success)
                            {
                                ParseObject.unpinAllInBackground();
                                manager.getDatabaseManager().deleteDatabase();
                                manager.deletePreferences();
                                login(id,code);
                            }
                            else{
                                new DialogWizard(Login.this).showSimpleDialog("Backup","We were unable to create the backup. Make sure your device storage is available and then try again.");
                            }

                        }
                    })
                    .show();

        }

        else{
            login(id,code);
        }
    }

    public void login(final String id, final String code) {
        ParseObject.unpinAllInBackground();

        if(manager.isDeviceOnline(Login.this))
        {
            final SweetAlertDialog dialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            dialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            dialog.setTitleText("We are signing you in");
            dialog.setContentText(getString(R.string.please_wait));
            //dialog.showCancelButton(true);
            // dialog.setCancelText(getString(R.string.dismiss));
            //dialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {@Overridepublic void onClick(SweetAlertDialog sweetAlertDialog) {sweetAlertDialog.dismiss();}});
            dialog.setCancelable(false);
            dialog.show();





            ParseUser.logInInBackground(id, code, new LogInCallback() {

                @Override
                public void done(final ParseUser parseUser, ParseException e) {
                    if (e == null) {

                        checkUser(parseUser,code);

                        if (!parseUser.getBoolean(Const.BLOCKED)) {
                            {
                                if (parseUser.getBoolean(Const.EMAIL_VERIFIED) || parseUser.getBoolean(Const.PHONE_VERIFIED)) {

                                    HashMap<String, Object> params = new HashMap<>();
                                    params.put(Const.VERSION, getString(R.string.version_number));
                                    params.put(Const.USERNAME, id);
                                    ParseCloud.callFunctionInBackground("versionCheck", params, new FunctionCallback<String>() {
                                        @Override
                                        public void done(String s, ParseException e) {

                                            if (e == null) {
                                                if (s.equals(Const.POSITIVE)) {
                                                    try {
                                                        authenticateWithFirebase();
                                                        //Delete the picture of the previous user, if any
                                                        context.deleteFile(Const.PROFILE_PICTURE_NAME);
                                                        if(manager.getLastUsername()==null) showTutorial=true;
                                                        manager.setUserSessionBlocked(false);
                                                        manager.setSHA(Crypt.getSHA256(code));

                                                        String path = Environment.getExternalStorageDirectory().toString()+"/JabuWings/Backups/"+id;
                                                        File directory=new File(path);
                                                        boolean fileFound=false;
                                                        //TODO remove backups
                                                        if(directory.exists() && directory.listFiles()!=null && directory.listFiles().length!=0)
                                                        {
                                                            File[] files= directory.listFiles();

                                                            for(final File file: files)
                                                            {
                                                                if(file.getName().contains(".jwbackup"))
                                                                {
                                                                    fileFound=true;
                                                                    dialog.dismiss();
                                                                    new SweetAlertDialog(Login.this,SweetAlertDialog.NORMAL_TYPE).setTitleText("Restore").setContentText("We found a previous backup. Will you like to restore").setConfirmText("Restore").setCancelText("Don't restore").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                        @Override
                                                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                                            if(manager.restoreEncryptedDatabase(file.getAbsolutePath(),id))
                                                                            {
                                                                                manager.successToast("Backup restored");
                                                                            }

                                                                            else{
                                                                                manager.errorToast("Unable to restore backup");
                                                                            }
                                                                            startActivity(new Intent(Login.this, SettingThingsUp.class).putExtra(Const.TUTORIAL,showTutorial));
                                                                            finish();
                                                                        }
                                                                    }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                        @Override
                                                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                                            startActivity(new Intent(Login.this, SettingThingsUp.class).putExtra(Const.TUTORIAL,showTutorial));
                                                                            finish();
                                                                        }
                                                                    }).show();

                                                                    break;
                                                                }
                                                            }
                                                            if(!fileFound)
                                                            {
                                                                dialog.dismiss();
                                                                startActivity(new Intent(Login.this, SettingThingsUp.class).putExtra(Const.TUTORIAL,showTutorial));
                                                                finish();
                                                            }
                                                        }
                                                        else{
                                                            dialog.dismiss();
                                                            startActivity(new Intent(Login.this, SettingThingsUp.class).putExtra(Const.TUTORIAL,showTutorial));
                                                            finish();
                                                        }





                                                    } catch (Exception i) {
                                                        i.printStackTrace();
                                                        try {
                                                            dialog.dismiss();
                                                        } catch (Exception ee) {
                                                            ee.printStackTrace();
                                                        }
                                                    }


                                                } else {
                                                    dialog.dismiss();
                                                    new DialogWizard(Login.this).showWarningDialog("", s);

                                                }
                                            } else {
                                                try {
                                                    dialog.dismiss();
                                                } catch (Exception ee) {
                                                    ee.printStackTrace();
                                                }
                                                e.printStackTrace();
                                                ErrorHandler.handleError(context,e);
                                            }


                                        }
                                    });


                                } else {
                                    finish();
                                    startActivity(new Intent(Login.this, Verification.class).putExtra("request",true));
                                }
                            }
                        } else {
                            ParseUser.logOutInBackground();
                            new DialogWizard(Login.this).showWarningDialog("Account Blocked!",
                                    "This account has been blocked due to a pending or reported case, please contact our customer care");
                        }


                    } else {
                        //TODO remove in production
                        e.printStackTrace();
                        try {
                            dialog.dismiss();
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                        if(e.getCode()==ParseException.CONNECTION_FAILED)
                        {

                            manager.errorToast("We were unable to get to our servers. Make sure your data connection is okay and try again");
                        }
                        else
                            new DialogWizard(Login.this).showErrorDialog("",e.getMessage());

                    }
                }
            });
        }



    }
    private void authenticateWithFirebase()
    {
        ParseCloud.callFunctionInBackground("firebaseAuth", manager.getDefaultCloudParams(), new FunctionCallback<String>() {
            @Override
            public void done(String token, ParseException e) {
                if(e==null)
                {
                    signInWithFirebase(token);
                }
                else{

                }
            }
        });
    }

    private void signInWithFirebase(String token)
    {
        mAuth.signInWithCustomToken(token)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            manager.successToast("Am in Firebase");
                            // Sign in success, update UI with the signed-in user's information

                        } else {
                            // If sign in fails, display a message to the user.

                        }
                    }
                });
    }


private void handleUnexpected()
{
    new DialogWizard(this).showErrorDialog("Oops...",getString(R.string.error_unexpected));
}

private void checkUser(ParseUser user , String password)
{
    //TODO Check for data2
    if(user.getParseObject(Const.DATA)==null)
    {
        ParseObject data=new ParseObject(Const.DATA);
        data.put(Const.USER,user);
        data.put(Const.SHA,Crypt.getSHA256(password));
        data.setACL(new ParseACL(user));
        user.put(Const.DATA,data);
    }

    user.saveInBackground();
}

private void tryEmail(String email, final String password)
{

    final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
    pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
    pDialog.setTitleText("Verifying your details");
    pDialog.setCancelable(false);
    pDialog.showCancelButton(true);
    pDialog.setCancelText(getString(R.string.dismiss));
    pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
        @Override
        public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
        }
    });
    pDialog.show();

    ParseUser.getQuery().whereEqualTo(Const.EMAIL,email).getFirstInBackground(new GetCallback<ParseUser>() {
        @Override
        public void done(ParseUser parseUser, ParseException e) {
            pDialog.dismiss();
            if (e == null) {
                check(parseUser.getUsername(), password);
            } else {
                if(e.getCode()==ParseException.OBJECT_NOT_FOUND)
                {
                    manager.shortToast("No account is associated with this email.");
                }
                else
                    ErrorHandler.handleError(Login.this, e);
            }
        }
    });
}
private void tryMatricNo(final String matricNo, final String password)
{
    final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
    pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
    pDialog.setTitleText("Verifying your details");
    pDialog.setCancelable(false);
    pDialog.showCancelButton(true);
    pDialog.setCancelText(getString(R.string.dismiss));
    pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
        @Override
        public void onClick(SweetAlertDialog sweetAlertDialog) {
            sweetAlertDialog.dismiss();
        }
    });
    pDialog.show();

        ParseUser.getQuery().whereEqualTo(Const.MATRIC_NO,matricNo).getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                pDialog.dismiss();
                if (e == null) {
                    check(parseUser.getUsername(), password);
                } else {
                    if(e.getCode()==ParseException.OBJECT_NOT_FOUND)
                    {
                        // Assume that the matric no is just a username and try.
                        login(matricNo,password);
                    }
                    else
                    ErrorHandler.handleError(Login.this, e);
                }
            }
        });
}
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                startActivity(new Intent(this,LoginHook.class));
                finish();
                break;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(this,LoginHook.class));
        finish();

    }
}