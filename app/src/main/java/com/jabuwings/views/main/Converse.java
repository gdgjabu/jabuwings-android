package com.jabuwings.views.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.text.Text;
import com.jabuwings.R;
import com.jabuwings.image.ImageProcessor;
import com.jabuwings.management.App;
import com.jabuwings.management.Const;
import com.jabuwings.views.hooks.ImageViewer;
import com.jabuwings.widgets.CircleImage;
import com.jabuwings.widgets.SwitchButton;
import com.sangcomz.fishbun.FishBun;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;



/**
 * Created by Falade James on 3/27/2016 All Rights Reserved.
 */
public class Converse extends JabuWingsActivity{
    ImageView shareImage, shareCameraPic;
    ImageView timeMessage,shareOthers;
    View.OnClickListener listener;
    CircleImage civProfilePicture;
    protected SwitchButton send;
    protected EmojiconEditText txt;
    EmojIconActions emojIcon;
    RecyclerView recyclerView;
    TextView typingText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.slide_up1, 0);


    }

    protected void initViews(Window window)
    {
        if(!manager.isCustomBackground())
        try {
            switch (manager.getChatBackground())
            {
                case 0:
                    getWindow().setBackgroundDrawableResource(R.drawable.chat_background0);
                    break;
                case 1:
                    getWindow().setBackgroundDrawableResource(R.drawable.chat_background1);
                    break;
                case 2:
                    getWindow().setBackgroundDrawableResource(R.drawable.chat_background2);
                    break;
                case 3:
                    getWindow().setBackgroundDrawableResource(R.drawable.chat_background3);
                    break;
                case 4:
                    getWindow().setBackgroundDrawableResource(R.drawable.chat_background4);
                    break;
                case 5:
                    getWindow().setBackgroundDrawableResource(R.drawable.chat_background5);
                    break;
                case 6:
                    getWindow().setBackgroundDrawableResource(R.drawable.chat_background6);
                    break;
                case 7:
                    getWindow().setBackgroundDrawableResource(R.drawable.chat_background7);
                    break;

            }

        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
        else{
            try{
            getWindow().setBackgroundDrawable(new BitmapDrawable(getResources(),new  ImageProcessor(this).decodeBitmapFromPrivateStorage(Const.CHAT_BACKGROUND_NAME)));}
            catch (OutOfMemoryError e)
            {
                e.printStackTrace();
            }
        }
        shareImage = (ImageView) window.findViewById(R.id.iv_chat_share_pic);
        shareImage.setOnClickListener(listener);
        shareCameraPic = (ImageView)window.findViewById(R.id.iv_chat_share_cam);
        shareCameraPic.setOnClickListener(listener);
        timeMessage = (ImageView)window.findViewById(R.id.iv_chat_send_scheduled);
        timeMessage.setOnClickListener(listener);
        shareOthers=(ImageView)window.findViewById(R.id.iv_chat_share_others);
        shareOthers.setOnClickListener(listener);
        civProfilePicture = (CircleImage)window.findViewById(R.id.civ_chat);
        recyclerView = (RecyclerView) window.findViewById(R.id.rvChat);
        typingText = (TextView)window.findViewById(R.id.chat_typing);
        send = (SwitchButton)window.findViewById(R.id.btnSend);
        send.setOnClickListener(listener);

    }
    public void shareCameraPhoto()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {

            //  intent.putExtra(MediaStore.EXTRA_OUTPUT,
            //        Uri.fromFile(fileManager.getFriendTransferPicture(getProfileUsername())));
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, Const.PICK_CAMERA_SHARE_ID);
            }
        }

        else{
            manager.errorToast(getString(R.string.error_no_application));
        }

    }
    public void shareImage()
    {
        FishBun.with(this)
                .MultiPageMode()
                .setAlbumThumbnailSize(150)
                .setActionBarColor(Color.BLUE, Color.BLUE) // actionBar and StatusBar color
                //        .setActionBarColor(Color.BLACK)           // only actionbar color
                .setMaxCount(1)
                .setMinCount(1)
                // .setArrayPaths(path)//you can choice again.
                .setPickerSpanCount(5)
                // .setRequestCode(11) //request code is 11. default == Define.ALBUM_REQUEST_CODE(27)
                .setCamera(true)//you can use camera
                .textOnImagesSelectionLimitReached("Limit Reached!")
                .textOnNothingSelected("Nothing Selected")
                .setButtonInAlbumActivity(true)
                .setReachLimitAutomaticClose(true)
                .startAlbum();
       /* Intent intent= new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Choose Picture to share"), Const.PICK_IMAGE_SHARE_ID);*/
    }

    public void setListener(View.OnClickListener listener)
    {
        this.listener=listener;
    }
    public void loadPicture(final String entity,final String name)
    {
        final String profilePictureFileName = manager.getFriendOrHubProfilePictureName(entity);
        new ImageProcessor(this).loadBitmap(profilePictureFileName, civProfilePicture);
        civProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, ImageViewer.class)
                        .putExtra(Const.VIEW_THIS_IMAGE, name)
                        .putExtra(Const.IMAGE_VIEWER_URI, profilePictureFileName));
            }
        });
    }

    protected void initEditText(Window window)
    {
        txt=(EmojiconEditText)window.findViewById(R.id.txt);



        txt.setTypeface(manager.getTypeFace());
        txt.setInputType(InputType.TYPE_CLASS_TEXT
                | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        txt.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);

        txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                send.goToState(text.length() == 0 ? SwitchButton.FIRST_STATE : SwitchButton.SECOND_STATE);
            }
        });

    }


    public void setUpEmoji(Window window){
        final View rootView = window.findViewById(R.id.lv_chat);
        final ImageView emojiButton = (ImageView) window.findViewById(R.id.emoji_btn);

        emojIcon=new EmojIconActions(this,rootView,txt,emojiButton);
        emojIcon.ShowEmojIcon();
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                Log.e("Keyboard","open");
             //   changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_keyboard);
            }

            @Override
            public void onKeyboardClose() {
                Log.e("Keyboard","close");
                changeEmojiKeyboardIcon(emojiButton, R.drawable.smiley);
            }
        });

       /* // Give the topmost view of your activity layout hierarchy. This will be used to measure soft keyboard height
        final EmojiconsPopup popup = new EmojiconsPopup(rootView, this);

        //Will automatically set size according to the soft keyboard size
        popup.setSizeForSoftKeyboard();

        //If the emoji popup is dismissed, change emojiButton to smiley icon
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                changeEmojiKeyboardIcon(emojiButton, R.drawable.smiley);
            }
        });

        //If the text keyboard closes, also dismiss the emoji popup
        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {

            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if(popup.isShowing())
                    popup.dismiss();
            }
        });

        //On emoji clicked, add it to edittext
        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                if (txt == null || emojicon == null) {
                    return;
                }

                int start = txt.getSelectionStart();
                int end = txt.getSelectionEnd();
                if (start < 0) {
                    txt.append(emojicon.getEmoji());
                } else {
                    txt.getText().replace(Math.min(start, end),
                            Math.max(start, end), emojicon.getEmoji(), 0,
                            emojicon.getEmoji().length());
                }
            }
        });

        //On backspace clicked, emulate the KEYCODE_DEL key event
        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                txt.dispatchKeyEvent(event);
            }
        });

        // To toggle between text keyboard and emoji keyboard keyboard(Popup)
        emojiButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //If popup is not showing => emoji keyboard is not visible, we need to show it
                if(!popup.isShowing()){

                    //If keyboard is visible, simply show the emoji popup
                    if(popup.isKeyBoardOpen()){
                        popup.showAtBottom();
                        changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_keyboard);
                    }

                    //else, open the text keyboard first and immediately after that show the emoji popup
                    else{
                        txt.setFocusableInTouchMode(true);
                        txt.requestFocus();
                        popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(txt, InputMethodManager.SHOW_IMPLICIT);
                        changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_keyboard);
                    }
                }

                //If popup is showing, simply dismiss it to show the undelying text keyboard
                else{
                    popup.dismiss();
                }
            }
        });*/

    }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId){
        iconToBeChanged.setImageResource(drawableResourceId);
    }

}
