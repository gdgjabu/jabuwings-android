package com.jabuwings.views.main;

/**
 * Created by jamesfalade on 23/09/2017.
 */

public interface OnChatItemInserted {
    void itemInserted();
}
