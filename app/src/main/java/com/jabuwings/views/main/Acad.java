package com.jabuwings.views.main;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.github.mikephil.charting.utils.Utils;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.intelligence.lisa.Lisa;
import com.jabuwings.management.AcademicsManager;
import com.jabuwings.management.Const;
import com.jabuwings.models.BannerAdapter;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.classroom.ConvertTextMaterial;
import com.jabuwings.views.classroom.CourseRegistration;
import com.jabuwings.views.classroom.LecturerConsole;
import com.jabuwings.views.classroom.LoggedInToPortal;
import com.jabuwings.views.classroom.StudentConsole;
import com.jabuwings.views.hooks.Browse;
import com.jabuwings.views.classroom.CourseMaterials;
import com.jabuwings.views.timetable.TimeTable2;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.jabuwings.widgets.pdf.PdfActivity;
import com.jabuwings.widgets.recyclerview.EasyRecyclerView;
import com.jabuwings.widgets.recyclerview.adapter.BaseViewHolder;
import com.jabuwings.widgets.recyclerview.adapter.RecyclerArrayAdapter;
import com.jabuwings.widgets.recyclerview.decoration.DividerDecoration;
import com.jabuwings.widgets.rollviewpager.RollPagerView;
import com.jabuwings.widgets.rollviewpager.Util;
import com.jabuwings.widgets.rollviewpager.hintview.ColorPointHintView;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseCloud;
import com.parse.ParseConfig;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.ProgressCallback;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.rey.material.widget.Spinner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class Acad extends JabuWingsFragment {
Context context;
View rootView;
    EasyRecyclerView recyclerView;
LayoutInflater inflater;
OnBadgeChangedListener listener;
DialogWizard dialogWizard;
AcademicsManager academicsManager;
public Acad() {
        // Required empty public constructor
        }

    public void setBadgeChangedListener(OnBadgeChangedListener listener)
    {
        this.listener=listener;
    }

    private void createListener()
    {
        if(listener==null)
        {
            Domicile core = Domicile.getInstance();
            if(core!=null)
            {
                listener= Domicile.getInstance().badgeChangedListener;
            }
        }
    }
@Override
public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context= getActivity();
        dialogWizard = new DialogWizard(getActivity());
        academicsManager = new AcademicsManager(getContext());
        createListener();

        }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.inflater=inflater;
        rootView=inflater.inflate(R.layout.sight_acad, container, false);
        setUpFragment(rootView);
         recyclerView =(EasyRecyclerView)rootView.findViewById(R.id.acad_recycler);

        //recyclerView.setAdapter(adapter = new PersonAdapter(this));

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        DividerDecoration itemDecoration = new DividerDecoration(Color.GRAY, Util.dip2px(context,0.5f), Util.dip2px(context,72),0);
        itemDecoration.setDrawLastItem(true);
        itemDecoration.setDrawHeaderFooter(true);
        recyclerView.addItemDecoration(itemDecoration);


        recyclerView.getRecyclerView().addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                    if(dy>0 || dy<0 && Domicile.boom.isShown()) Domicile.boom.setVisibility(View.GONE);
            }
        });
         createAdapter(recyclerView);
        return rootView;
    }


    public void onItemClicked(int position) {
        switch (position) {
            case 0:
                startActivity(new Intent(context, TimeTable2.class));
                break;
            case 1:
                startActivity(new Intent(context,AcademicManagerActivity.class));
                break;
            case 2:
                Intent courseReg = new Intent(context, Browse.class);
                courseReg.putExtra(Const.ACAD_CHOICE, "courseReg");
                startActivity(courseReg);
                break;
            case 3:
                Intent resultChecker = new Intent(context, Browse.class);
                resultChecker.putExtra(Const.ACAD_CHOICE, "resultChecker");
                startActivity(resultChecker);
                break;
            case 4:
                startActivity(new Intent(context, CourseMaterials.class));
                break;
            case 5:
                Intent studentReg = new Intent(context, Browse.class);
                studentReg.putExtra(Const.ACAD_CHOICE, "studentReg");
                startActivity(studentReg);
                break;
            case 6:
                Intent officialWebsite = new Intent(context, Browse.class);
                officialWebsite.putExtra(Const.ACAD_CHOICE, "officialWebsite");
                startActivity(officialWebsite);
                break;
        }
    }

    private void createAdapter(EasyRecyclerView recyclerView) {
        final List<AcadItem> content = new ArrayList<>();
        content.add(new AcadItem("Classroom","Get connected to your class.",R.drawable.ic_assignment_turned_in_black_48dp,0));
        content.add(new AcadItem("Timetable","Check when you have lectures.",R.drawable.ic_timer_black_48dp,1));
        content.add(new AcadItem("Academics Manager","Manage your CGPA and much more",R.drawable.ic_school_black_48dp,2));
        content.add(new AcadItem("Course Registration","Register your courses",R.drawable.ic_public_black_48dp,3));
        content.add(new AcadItem("Download  Course Form","Download your course form for this current session and semester.",R.drawable.ic_public_black_48dp,4));
        content.add(new AcadItem("Download Exam card","Download exam card",R.drawable.ic_public_black_48dp,5));
        content.add(new AcadItem("Check result","Check your previous results",R.drawable.ic_public_black_48dp,10));
        content.add(new AcadItem("Download Bio data","Download your student bio data",R.drawable.ic_public_black_48dp,6));
        content.add(new AcadItem("JABU Web","Visit the Official JABU website",R.drawable.ic_public_black_48dp,7));

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        PersonAdapter adapter= new PersonAdapter(getContext());
        recyclerView.setAdapter(adapter);

        adapter.addAll(content);
        adapter.addHeader(new RecyclerArrayAdapter.ItemView() {
            @Override
            public View onCreateView(ViewGroup parent) {
                RollPagerView header = new RollPagerView(getActivity());
                header.setHintView(new ColorPointHintView(getActivity(), Color.YELLOW,Color.GRAY));
                header.setHintPadding(0, 0, 0, (int) Utils.convertDpToPixel(8));
                header.setPlayDelay(2000);
                header.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) Utils.convertDpToPixel(200)));
                header.setAdapter(new BannerAdapter(getActivity(),content));
                return header;
            }

            @Override
            public void onBindView(View headerView) {

            }
        });



    }

    public class PersonAdapter extends RecyclerArrayAdapter<AcadItem> {
        public PersonAdapter(Context context) {
            super(context);
        }

        @Override
        public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            return new PersonViewHolder(parent);
        }
    }

    public class PersonViewHolder extends BaseViewHolder<AcadItem> {
        private TextView mTv_name;
        private ImageView mImg_face;
        private TextView mTv_sign;
        private RelativeLayout acadLayout;




        public PersonViewHolder(ViewGroup parent) {
            super(parent, R.layout.item_person);
            mTv_name = $(R.id.person_name);
            mTv_sign = $(R.id.person_sign);
            mImg_face = $(R.id.person_face);
            acadLayout=$(R.id.acadLayout);
        }

        @Override
        public void setData(final AcadItem data) {
            JabuWingsActivity.setUpTextViews(context,mTv_name,mTv_sign);
            mTv_name.setText(data.title);
            mTv_sign.setText(data.desc);
            Glide.with(getContext())
                    .load(data.resId)
                    .into(mImg_face);
            acadLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ParseConfig config= ParseConfig.getCurrentConfig();

                    switch (data.position)
                    {
                        case 0:
                           classroom();
                            break;
                        case 1:
                            startActivity(new Intent(getActivity(), TimeTable2.class));
                            break;
                        case 2:
                            startActivity(new Intent(getActivity(), AcademicManagerActivity.class));
                            break;
                        case 3:
                            startActivity(new Intent(getActivity(), CourseRegistration.class));
                            break;
                        case 4:
                            academicsManager.requireJABUPortalLoggedIn((JabuWingsActivity) getActivity(), new LoggedInToPortal() {
                                @Override
                                public void loggedIn() {
                                    downloadCourseForm();
                                }
                            });
                            break;
                        case 5:
                            academicsManager.requireJABUPortalLoggedIn((JabuWingsActivity) getActivity(), new LoggedInToPortal() {
                                @Override
                                public void loggedIn() {
                                    downloadExamCard();
                                }
                            });
                            break;
                        case 6:
                            academicsManager.requireJABUPortalLoggedIn((JabuWingsActivity) getActivity(), new LoggedInToPortal() {
                                @Override
                                public void loggedIn() {
                                    downloadBiodata();
                                }
                            });
                            break;
                        case 10:
                            academicsManager.requireJABUPortalLoggedIn((JabuWingsActivity) getActivity(), new LoggedInToPortal() {
                                @Override
                                public void loggedIn() {
                                    checkResult();
                                }
                            });
                            break;
                       /* case 3:
                            String portalUrl=config.getString("jabu_portal");
                            if(portalUrl==null)
                                portalUrl= Const.DEFAULT_PORTAL_URL;
                                manager.openInChrome(Uri.parse(portalUrl));
                            //startActivity(new Intent(getActivity(), Browse.class).putExtra(Const.ACAD_CHOICE,"portal"));
                            break;*/
                        case 7:
                            manager.openInChrome(Uri.parse(Const.DEFAULT_JABU_WEB_URL));
                            //startActivity(new Intent(getActivity(), Browse.class).putExtra(Const.ACAD_CHOICE,"web"));
                            break;

                    }
                }
            });
        }

        private void checkError(ParseException e)
        {
            manager.errorToast(academicsManager.getPortalError(e));
            academicsManager.checkPortalError(e);
        }
        private void downloadCourseForm()
        {
            final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                    .title(R.string.please_wait)
                    .content("Downloading your course form.")
                    .progress(false, 100, true)
                    .cancelable(false)
                    .negativeText(R.string.close)
                    .show();

            ParseCloud.callFunctionInBackground(Const.GET_COURSE_FORM, manager.getDefaultCloudParams(), new FunctionCallback<ParseFile>() {
                @Override
                public void done(final ParseFile file, ParseException e) {
                    if(e==null)
                     file.getDataInBackground(new GetDataCallback() {
                         @Override
                         public void done(byte[] data, ParseException e) {

                             if(e==null)
                             {
                                 try {
                                     String uri = file.getUrl();
                                     String extension =uri.substring(uri.lastIndexOf("."));
                                     File sd = Environment.getExternalStorageDirectory();
                                     File dir = new File(sd.getPath(),Const.PUBLIC_COURSE_FORM_DIRECTORY);
                                     dir.mkdir();
                                     String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.US).format(new Date());
                                     File courseFormFile = new File(dir,"Course Form_"+timeStamp+extension);
                                     FileOutputStream outputStream = new FileOutputStream(courseFormFile);
                                     outputStream.write(data);

                                     if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                         dialog.setTitle(R.string.done);
                                         dialog.setContent("The course form has been saved to "+courseFormFile.getPath());
                                     }
                                     else{
                                         dialog.dismiss();
                                         manager.longToast("Course form saved to "+courseFormFile.getPath());
                                         startActivity(new Intent(getActivity(),PdfActivity.class).putExtra(Const.PATH,courseFormFile.getAbsolutePath()).putExtra(Const.TITLE,"Your course form"));
                                     }




                                 }
                                 catch (IOException e1)
                                 {
                                     e1.printStackTrace();
                                     manager.errorToast("Download failed. Could not save file");
                                 }

                             }
                             else manager.errorToast("unable to download");
                         }
                     }, new ProgressCallback() {
                         @Override
                         public void done(Integer percentDone) {
                             dialog.setProgress(percentDone);

                         }
                     });
                    else {
                        dialog.dismiss();
                        checkError(e);
                    }
                }
            });
        }

        private void downloadBiodata()
        {

            final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                    .title(R.string.please_wait)
                    .content("Downloading your bio data")
                    .progress(false, 100, true)
                    .cancelable(false)
                    .negativeText(R.string.close)
                    .show();

            ParseCloud.callFunctionInBackground(Const.GET_BIO_DATA, manager.getDefaultCloudParams(), new FunctionCallback<ParseFile>() {
                @Override
                public void done(final ParseFile file, ParseException e) {
                    if(e==null)
                        file.getDataInBackground(new GetDataCallback() {
                            @Override
                            public void done(byte[] data, ParseException e) {

                                if(e==null)
                                {
                                    try {
                                        String uri = file.getUrl();
                                        String extension =uri.substring(uri.lastIndexOf("."));
                                        File sd = Environment.getExternalStorageDirectory();
                                        File dir = new File(sd.getPath(),Const.PUBLIC_BIO_DATA_DIRECTORY);
                                        dir.mkdir();
                                        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.US).format(new Date());
                                        File bioDataFile = new File(dir,"Bio data_"+timeStamp+extension);
                                        FileOutputStream outputStream = new FileOutputStream(bioDataFile);
                                        outputStream.write(data);

                                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                            dialog.setTitle(R.string.done);
                                            dialog.setContent("The bio data form has been saved to "+bioDataFile.getPath());
                                        }
                                        else{
                                            dialog.dismiss();
                                            manager.longToast("Bio data form saved to "+bioDataFile.getPath());
                                            startActivity(new Intent(getActivity(),PdfActivity.class).putExtra(Const.PATH,bioDataFile.getAbsolutePath()).putExtra(Const.TITLE,"Your bio data"));
                                        }




                                    }
                                    catch (IOException e1)
                                    {
                                        e1.printStackTrace();
                                        manager.errorToast("Download failed. Could not save file");
                                    }

                                }
                                else manager.errorToast("unable to download");
                            }
                        }, new ProgressCallback() {
                            @Override
                            public void done(Integer percentDone) {
                                dialog.setProgress(percentDone);

                            }
                        });
                    else {
                        dialog.dismiss();
                        checkError(e);
                    }
                }
            });

        }

        private void downloadExamCard()
        {

            final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                    .title(R.string.please_wait)
                    .content("Downloading your exam card.")
                    .progress(false, 100, true)
                    .cancelable(false)
                    .negativeText(R.string.close)
                    .show();

            ParseCloud.callFunctionInBackground(Const.GET_EXAM_CARD, manager.getDefaultCloudParams(), new FunctionCallback<ParseFile>() {
                @Override
                public void done(final ParseFile file, ParseException e) {
                    if(e==null)
                        file.getDataInBackground(new GetDataCallback() {
                            @Override
                            public void done(byte[] data, ParseException e) {

                                if(e==null)
                                {
                                    try {
                                        String uri = file.getUrl();
                                        String extension =uri.substring(uri.lastIndexOf("."));
                                        File sd = Environment.getExternalStorageDirectory();
                                        File dir = new File(sd.getPath(),Const.PUBLIC_EXAM_CARD_DIRECTORY);
                                        dir.mkdir();
                                        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.US).format(new Date());
                                        File examCardFile = new File(dir,"Exam_card_"+timeStamp+extension);
                                        FileOutputStream outputStream = new FileOutputStream(examCardFile);
                                        outputStream.write(data);

                                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                            dialog.setTitle(R.string.done);
                                            dialog.setContent("The exam card has been saved to "+examCardFile.getPath());
                                        }
                                        else{
                                            dialog.dismiss();
                                            manager.longToast("Exam card saved to "+examCardFile.getPath());
                                            startActivity(new Intent(getActivity(),PdfActivity.class).putExtra(Const.PATH,examCardFile.getAbsolutePath()).putExtra(Const.TITLE,"Your Exam card."));
                                        }




                                    }
                                    catch (IOException e1)
                                    {
                                        e1.printStackTrace();
                                        manager.errorToast("Download failed. Could not save file");
                                    }

                                }
                                else manager.errorToast("unable to download");
                            }
                        }, new ProgressCallback() {
                            @Override
                            public void done(Integer percentDone) {
                                dialog.setProgress(percentDone);

                            }
                        });
                    else {
                        dialog.dismiss();
                        checkError(e);
                    }
                }
            });

        }

        private void checkResult()
        {
            ParseConfig config = ParseConfig.getCurrentConfig();
            List<String> sessions = config.getList("school_sessions");

            new MaterialDialog.Builder(getActivity()).title(R.string.session).items(sessions).itemsCallback(new MaterialDialog.ListCallback() {
                @Override
                public void onSelection(MaterialDialog dialog, View itemView, int position, final CharSequence session) {
                    new MaterialDialog.Builder(getActivity()).title(R.string.level).items(Const.ALL_LEVELS).itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View itemView, int position, final CharSequence level) {

                            new MaterialDialog.Builder(getActivity()).title(R.string.semester).items(Const.ALL_SEMESTERS).itemsCallback(new MaterialDialog.ListCallback() {
                                @Override
                                public void onSelection(MaterialDialog _dialog, View itemView, int position, CharSequence semester) {
                                    final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                                            .title(R.string.please_wait)
                                            .content("Downloading your result slip.")
                                            .progress(false, 100, true)
                                            .cancelable(false)
                                            .negativeText(R.string.close)
                                            .show();
                                    HashMap<String,Object> params = manager.getDefaultCloudParams();
                                    params.put(Const.SESSION, session.toString());
                                    params.put(Const.SEMESTER, semester.toString());
                                    params.put(Const.LEVEL, level.toString());
                                    ParseCloud.callFunctionInBackground("getStudentResult", params, new FunctionCallback<ParseFile>() {
                                        @Override
                                        public void done(final ParseFile file, ParseException e) {
                                            if(e==null)
                                                file.getDataInBackground(new GetDataCallback() {
                                                    @Override
                                                    public void done(byte[] data, ParseException e) {

                                                        if(e==null)
                                                        {
                                                            try {
                                                                String uri = file.getUrl();
                                                                String extension =uri.substring(uri.lastIndexOf("."));
                                                                File sd = Environment.getExternalStorageDirectory();
                                                                File dir = new File(sd.getPath(),Const.PUBLIC_RESULTS_DIRECTORY);
                                                                dir.mkdir();
                                                                String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.US).format(new Date());
                                                                File resultFile = new File(dir,"Result_"+timeStamp+extension);
                                                                FileOutputStream outputStream = new FileOutputStream(resultFile);
                                                                outputStream.write(data);

                                                                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                                                    dialog.setTitle(R.string.done);
                                                                    dialog.setContent("The result has been saved to "+resultFile.getPath());
                                                                }
                                                                else{
                                                                    dialog.dismiss();
                                                                    manager.longToast("Result slip saved to "+resultFile.getPath());
                                                                    startActivity(new Intent(getActivity(),PdfActivity.class).putExtra(Const.PATH,resultFile.getAbsolutePath()).putExtra(Const.TITLE,"Your Result"));
                                                                }




                                                            }
                                                            catch (IOException e1)
                                                            {
                                                                e1.printStackTrace();
                                                                manager.errorToast("Download failed. Could not save file");
                                                            }

                                                        }
                                                        else manager.errorToast("unable to download");
                                                    }
                                                }, new ProgressCallback() {
                                                    @Override
                                                    public void done(Integer percentDone) {
                                                        dialog.setProgress(percentDone);

                                                    }
                                                });
                                            else {
                                                dialog.dismiss();
                                                checkError(e);
                                            }

                                        }
                                    });
                                }
                            }).show();
                        }
                    }).show();
                }
            }).show();
        }
        private void classroom()
        {
            int type= manager.getUserType();

            if(type==0)
            {
                academicsManager.requireJABUPortalLoggedIn((JabuWingsActivity) getActivity(), new LoggedInToPortal() {
                    @Override
                    public void loggedIn() {
                        startActivity(new Intent(context,StudentConsole.class));
                    }
                });
            }

            else if (type==1)
            {
                if(manager.getAcademicDepartment()!=null)
                {
                    startActivity(new Intent(context,LecturerConsole.class));
                }
                else{


                    new MaterialDialog.Builder(getActivity()).content("Only academic staff are allowed here").positiveText(R.string.ok).neutralText("I am an academic staff").onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    }).onNeutral(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull final MaterialDialog dialog, @NonNull DialogAction which) {

                            Dialog.Builder builder = new SimpleDialog.Builder(R.style.SimpleDialog){
                                Spinner spinner;
                                String department;
                                @Override
                                protected void onBuildDone(Dialog dialog) {
                                    dialog.layoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    spinner= (Spinner) dialog.findViewById(R.id.spAcademicDepartment);
                                    String[] departments= getResources().getStringArray(R.array.departments);
                                    ArrayAdapter<String> adapter = new ArrayAdapter<>(context,R.layout.row_spn,departments);
                                    adapter.setDropDownViewResource(R.layout.row_spn_dropdown);
                                    spinner.setAdapter(adapter);
                                }

                                @Override
                                public void onPositiveActionClicked(DialogFragment fragment) {
                                    department=(String) spinner.getSelectedItem();
                                    if(!department.equals(Const.CHOOSE)){
                                        final SweetAlertDialog sweetAlertDialog=dialogWizard.showSimpleProgress(getString(R.string.please_wait),"Effecting the change");
                                        HashMap<String,Object> params = manager.getDefaultCloudParams();
                                        params.put(Const.DEPARTMENT,department);
                                        ParseCloud.callFunctionInBackground("turnAcademicStaff", manager.getDefaultCloudParams(), new FunctionCallback<String>() {
                                            @Override
                                            public void done(final String s, ParseException e) {

                                                if(e==null)
                                                {
                                                    ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseUser>() {
                                                        @Override
                                                        public void done(ParseUser u, ParseException e) {
                                                            sweetAlertDialog.dismiss();
                                                            if(e==null){
                                                                u.pinInBackground();
                                                                dialogWizard.showSimpleDialog("Result",s);
                                                            }
                                                            else
                                                                ErrorHandler.handleError(context,e);
                                                        }
                                                    });

                                                }

                                                else{
                                                    sweetAlertDialog.dismiss();
                                                    ErrorHandler.handleError(context,e);
                                                }
                                            }
                                        });
                                    }
                                    else{
                                        manager.shortToast("Please select a department");
                                    }

                                }

                                @Override
                                public void onNegativeActionClicked(DialogFragment fragment) {
                                    super.onNegativeActionClicked(fragment);
                                }
                            };

                            builder.title("What is your department?")
                                    .contentView(R.layout.turn_academic_staff).positiveAction(getString(R.string.change)).negativeAction(getString(R.string.cancel));

                            DialogFragment rateFragment = DialogFragment.newInstance(builder);
                            rateFragment.show(getActivity().getSupportFragmentManager(), null);


                        }
                    }).show();
                }
            }
            else{
                manager.longToast("Classroom only available for students and academic staff");
            }
        }




    }



    public class AcadItem{
        String title,desc; int resId,position;
        public AcadItem(String title, String desc, int resId,int position)
        {
            this.title =title;
            this.desc=desc;
            this.resId=resId;
            this.position=position;
        }
    }



    public interface AcadClicks{
        void onClick(View v);
    }

}
