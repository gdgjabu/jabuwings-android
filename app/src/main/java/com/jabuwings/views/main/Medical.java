package com.jabuwings.views.main;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListItem;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.Const;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.GetFileCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class Medical extends JabuWingsActivity {
@InjectView(R.id.medicalInfo)
TextView tvInfo;
@InjectView(R.id.btRetrieveMedical)
Button retrieveMedicalNumber;
@InjectView(R.id.etMedName)
AutoCompleteTextView etMedName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical);
        ButterKnife.inject(this);
        setUpToolbar(null,R.string.medical);

        etMedName.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    retrieve();
                    return true;
                }
                return false;
            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                call(view);
            }
        });




       // String medicalNo=manager.getMedicalNo();
        tvInfo.setText("You can retrieve a student's medical number by filling the form below.");
        processSuggestions();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_medical,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.action_refresh)
        {
            new DownloadMedicalData().execute("");
        }
        return super.onOptionsItemSelected(item);
    }

    private void processSuggestions()
    {


        try {
            FileInputStream imageInput;
            imageInput = context.openFileInput(Const.MEDICAL_DATA);


            int size = imageInput.available();
            byte[] buffer = new byte[size];
            imageInput.read(buffer);
            imageInput.close();
            String json = new String(buffer, "UTF-8");


            manager.log(json);
            JSONArray array = new JSONArray(json);
            List<String> s= new ArrayList<>();
            for(int i=0; i<array.length(); i++)
            {
                s.add(array.getJSONObject(i).getString(Const.TITLE));
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_dropdown_item_1line, s);

            etMedName.setAdapter(adapter);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            new DownloadMedicalData().execute("");
        }
    }

    public  void getSuggestions()
    {
        new ParseQuery<>("App").whereEqualTo(Const.NAME,"medical").getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(e==null)
                {
                    ParseFile file = parseObject.getParseFile(Const.FILE);

                    file.getFileInBackground(new GetFileCallback() {
                        @Override
                        public void done(File file, ParseException e) {
                            if(e==null)
                            {

                            }
                            else
                            {}
                        }
                    });
                }
            }
        });
    }

    @OnClick(R.id.btRetrieveMedical)
    public void retrieve()
    {
        String name =etMedName.getText().toString();
        if(TextUtils.isEmpty(name))
        {
            manager.errorToast("Please enter a name first");
        }
        else{
            final HashMap<String,Object> params= manager.getDefaultCloudParams();
            params.put(Const.NAME,name);
            completeRetrieve(params);
        }


        /*new MaterialDialog.Builder(Medical.this).title("Retrieve").items(options).itemsCallback(new MaterialDialog.ListCallback() {
            @Override
            public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                    switch (position)
                    {
                        case 0:
                            params.put("personal",true);
                            completeRetrieve(params);
                            break;
                        case 1:
                            new MaterialDialog.Builder(Medical.this).title("Name").input("Enter the person's name(Surname first)", null, false, new MaterialDialog.InputCallback() {
                                @Override
                                public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                    params.put("personal",false);
                                    params.put("name",input.toString());
                                    completeRetrieve(params);
                                }
                            }).show();

                            break;
                    }
            }
        }).show();*/

    }

    private void completeRetrieve(HashMap<String,Object> params)
    {
        final DialogWizard dialogWizard=  new DialogWizard(Medical.this);
        final SweetAlertDialog dialog= dialogWizard.showProgress("Please wait","Getting your medical number",false);

        ParseCloud.callFunctionInBackground("get_medical_no", params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {
                dialog.dismiss();
                if(e==null)
                {
                    try {
                        JSONObject json = new JSONObject(s);
                        String sex= json.getString(Const.SEX).equals("F")? "Female" : "Male";
                        dialogWizard.showSimpleDialog("Medical Details", "Name: "+json.getString(Const.NAME)+"\n"+"Department: "+json.getString(Const.DEPARTMENT)+"\n"+"Sex: "+sex+"\n"+"Medical No: "+json.getString("no"));
                    }
                    catch (JSONException i)
                    {
                        i.printStackTrace();
                        dialogWizard.showSimpleDialog("Medical Details", s);
                    }

                }
                else{
                    ErrorHandler.handleError(context,e);
                }
            }
        });

    }
    private void call(final View view)

    {


        switch ((int)(Math.random()*2) +1)
        {
            case 1: manager.dial("08113991900"); break;
            case 2: manager.dial("08147386057"); break;
            default: manager.dial("08113991900");
        }

        Snackbar.make(view, "Attempting to call the JABU medical center", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();


    }


    private class DownloadMedicalData extends AsyncTask<String, Void, String> {

        String result;
        SweetAlertDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog=dialogWizard.showSimpleProgress(getString(R.string.please_wait),"Setting up for a first time use.");
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                URL url = new URL("https://www.jabuwings.com/medical_suggestions.json");


                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();


                // Convert response to string using String Builder
                try {
                    BufferedReader bReader = new BufferedReader(new InputStreamReader(input, "utf-8"), 8);
                    StringBuilder sBuilder = new StringBuilder();

                    String line = null;
                    while ((line = bReader.readLine()) != null) {
                        sBuilder.append(line + "\n");
                    }

                    input.close();
                    return sBuilder.toString();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;

            } catch (NullPointerException e) {
                e.printStackTrace();
                return null;
            }

            return  null;
        }

        /**
         * The system calls this to perform work in the UI thread and delivers
         * the result from doInBackground()
         */
        @Override
        protected void onPostExecute(String  r) {


            dialog.dismiss();


            if(r==null)
            {
                new MaterialDialog.Builder(Medical.this)
                        .title("Oops")
                        .content("Unable to download medical data. Please try again")
                        .positiveText(context.getString(R.string.download))
                        .negativeText(context.getString(R.string.cancel))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                                new DownloadMedicalData().execute("");
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                                finish();
                            }
                        })
                        .show();
            }
            else
            try {
                FileOutputStream pp = openFileOutput(Const.MEDICAL_DATA, Context.MODE_PRIVATE);
                pp.write(r.getBytes("UTF-8"));
                processSuggestions();

            } catch (Exception e) {
                manager.errorToast("Unable to process file "+e.getMessage());
                e.printStackTrace();
                Log.e("JSONException", "Error: " + e.toString());
            } // catch (JSONException e)
        } // protected void onPostExecute(Void v)


        }



    }


