package com.jabuwings.views.main;



import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.jabuwings.error.ExceptionHandler;
import com.jabuwings.management.Manager;

import io.realm.Realm;

/**
 * Created by Falade James on 11/28/2015 All Rights Reserved.
 */

/**
 * This fragment is the basis fragment for most JabuWings fragments. Most of the fragments extend this fragment
 * to inherit some features so as to avoid keep repeating the same thing across fragments
 */
@SuppressWarnings("ConstantConditions")
public class JabuWingsFragment extends Fragment {
   View rootView;
   Manager manager;
   Realm realm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        manager=new Manager(getActivity());
        //Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(getActivity()));
    }

    public void setUpFragment(View rootView)
   {
       this.rootView=rootView;
   }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();

    }

    protected View findViewById(int resId)
  {
      return rootView.findViewById(resId);
  }

}
