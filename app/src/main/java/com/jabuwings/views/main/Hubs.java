package com.jabuwings.views.main;


import android.content.Context;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.database.realm.Hub;
import com.jabuwings.management.Manager;
import com.jabuwings.models.HubsAdapter;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * A simple {@link Fragment} subclass.
 */
public class Hubs extends JabuWingsFragment implements OrderedRealmCollectionChangeListener<RealmResults<Hub>> {
View rootView;
RecyclerView rvHubs;
HubsAdapter hubsAdapter;
Context context;
TextView tvHubTitle;
Manager manager;
public static TextView lonely;
RealmResults<Hub> hubs;
DatabaseManager databaseManager;
    public Hubs() {
        // Required empty public constructor
    }

    @Override
    public void onChange(RealmResults<Hub> hubs, OrderedCollectionChangeSet changeSet) {
        hubsAdapter.notifyDataSetChanged();

    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=getActivity();

        manager= new Manager(context);
        databaseManager = manager.getDatabaseManager();
        hubs = realm.where(Hub.class).findAll();
       // hubs.addChangeListener(this);


            realm.addChangeListener(new RealmChangeListener<Realm>() {
            @Override
            public void onChange(Realm realm) {
                if(hubsAdapter !=null) hubsAdapter.notifyDataSetChanged();
            }
        });


    }

    private void setUpAdapter(RealmResults<Hub> hubs)
    {
        this.hubs=hubs;
        hubsAdapter = new HubsAdapter(getActivity(),hubs.sort("lastMessageDate", Sort.DESCENDING));
        rvHubs.setAdapter(hubsAdapter);
        rvHubs.setLayoutManager(new LinearLayoutManager(context));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_hubs, container, false);
        lonely=(TextView)rootView.findViewById(R.id.tv_lonely_hubs);
        rvHubs=(RecyclerView)rootView.findViewById(R.id.rv_hubs);




        int hubCount=hubs.size();
        if(hubCount<=0)
        {
            lonely.setVisibility(View.VISIBLE);
        }
        setUpAdapter(hubs);
      /*  manager._getUserHubs().getQuery().fromLocalDatastore().findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e==null)
                {
                    if(list.size()==0) lonely.setVisibility(View.VISIBLE);
                }

                else{
                    if(list==null) lonely.setVisibility(View.VISIBLE);
                    else{
                        if(list.size()==0) lonely.setVisibility(View.VISIBLE);
                    }
                }


            }
        });
*/

        return rootView;
    }




}
