package com.jabuwings.views.main;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.jabuwings.R;

/**
 * Created by Falade James on 11/28/2015 All Rights Reserved.
 */
@SuppressWarnings("ConstantConditions")
public class JabuWingsTooleap extends JabuWingsActivity {
protected Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * This is used to initialise the toolbar and should only be used when the id of the toolbar is "R.id.toolbar"
     */
    protected Toolbar setUpToolbar(View.OnClickListener listener)
    {
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        toolbar.setOnClickListener(listener);
        toolbar.setNavigationIcon(R.drawable.btn_back);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return toolbar;
    }

    /**
     * This is used to initialise the toolbar and should only be used when the id of the toolbar is "R.id.toolbar"
     */
    protected Toolbar setUpToolbar(View.OnClickListener listener,int titleResId)
    {
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(titleResId);
        toolbar.setOnClickListener(listener);
        toolbar.setNavigationIcon(R.drawable.btn_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return toolbar;
    }

    protected Toolbar setUpToolbar(View.OnClickListener listener,String title)
    {
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        toolbar.setOnClickListener(listener);
        toolbar.setNavigationIcon(R.drawable.btn_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return toolbar;
    }
}
