package com.jabuwings.views.main;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.feed.FeedContextMenu;
import com.jabuwings.feed.FeedItem;
import com.jabuwings.feed.FeedItemAnimator;
import com.jabuwings.feed.FeedPost;
import com.jabuwings.intelligence.FeedService;
import com.jabuwings.intelligence.OnFeedChanged;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.CircleTransform;
import com.jabuwings.models.CommentsAdapter;
import com.jabuwings.models.JabuWingsFirebaseUser;
import com.jabuwings.models.JabuWingsUser;
import com.jabuwings.models.MultipleUsersAdapter;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.Comments;
import com.jabuwings.views.hooks.ImageViewer;
import com.jabuwings.views.hooks.MultipleUsersSearch;
import com.jabuwings.views.hooks.ProfileOfFriend;
import com.jabuwings.widgets.JabuWingsTextView;
import com.jabuwings.widgets.SendCommentButton;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.OnBoomListener;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.rey.material.widget.ProgressView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class Feed extends JabuWingsFragment implements SwipeRefreshLayout.OnRefreshListener,FeedContextMenu.OnFeedContextMenuItemClickListener,OnFeedChanged{
	private static final String TAG = Feed.class.getSimpleName();
	RecyclerView rvFeedRecycler;
    public  TextView tvLonely;
	View rootView;
	Context context;
	Manager manager;
    SimpleExoPlayer player;
//	public FeedAdapter feedAdapter;
	public OnFeedChanged onFeedChanged;
    int pastVisibleItems,visibleItemCount,totalItemCount;
    public static final int VIEW_TYPE_DEFAULT = 1;
    public static final int VIEW_TYPE_NO_IMAGE = 2;
    public static final String ACTION_LIKE_BUTTON_CLICKED = "action_like_button_button";
    public static final String ACTION_LIKE_IMAGE_CLICKED = "action_like_image_button";
    FirestoreRecyclerAdapter adapter;
    FirebaseFirestore db;


    public Feed()
	{

	}
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.context=getActivity();
        manager=new Manager(context);
        db = FirebaseFirestore.getInstance();
		/*feedAdapter = new FeedAdapter(getActivity(), this);
		feedAdapter.setOnFeedItemClickListener(this);*/

        onFeedChanged=this;
        player = ExoPlayerFactory.newSimpleInstance(
                new DefaultRenderersFactory(getActivity()),
                new DefaultTrackSelector(), new DefaultLoadControl());
		//getLoaderManager().initLoader(0,null,this);
	}

    @Override
    public void feedChanged() {
        /*//manager.shortToast("Feed changed");
        if(feedAdapter!=null){feedAdapter.updateItems(false);}*/
    }



	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView= inflater.inflate(R.layout.fragment_feed,container,false);
		rvFeedRecycler = (RecyclerView)rootView. findViewById(R.id.rv_feed);
        tvLonely=(TextView)rootView.findViewById(R.id.tv_lonely_feeds);
//        feedAdapter.updateItems(true);
		//feedDatabase =  new FeedDatabase(getActivity(),null);

		final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context) {
			@Override
			protected int getExtraLayoutSpace(RecyclerView.State state) {
				return 300;
			}
		};

        rvFeedRecycler.setLayoutManager(linearLayoutManager);

        rvFeedRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(dy>0)
                {
                    visibleItemCount=linearLayoutManager.getChildCount();
                    totalItemCount=linearLayoutManager.getItemCount();
                    pastVisibleItems=linearLayoutManager.findFirstVisibleItemPosition();
                    linearLayoutManager.findLastVisibleItemPosition();

                    
                    //Remove -2 to get the last item
                    if((visibleItemCount+pastVisibleItems)>=totalItemCount-2)
                    {
                        manager.log("Almost last");
//                        feedAdapter.updateFromLast();

                    }
                }
            }
        });

        startDatabase();

//        rvFeedRecycler.setAdapter(feedAdapter);
//        rvFeedRecycler.setItemAnimator(new FeedItemAnimator());
		return rootView;
	}


    public void setLonely(boolean a)
    {
        if(tvLonely!=null)
        {
            if(a)
                tvLonely.setVisibility(View.VISIBLE);
            else
                tvLonely.setVisibility(View.GONE);
        }
    }


    public void onCommentsClick(View v, FeedViewHolder viewHolder) {
       final Intent intent = new Intent(getActivity(), Comments.class);
        int[] startingLocation = new int[2];
        v.getLocationOnScreen(startingLocation);
        intent.putExtra(Comments.ARG_DRAWING_START_LOCATION, startingLocation[1]);
//		intent.putExtra("object",viewHolder.getFeedItem());
        startActivity(intent.putExtra(Const.Feed.COMMENTS,viewHolder.getFeedItem()).putExtra(Const.ID, viewHolder.getSnapshot().getId()));
        getActivity().overridePendingTransition(0, 0);
//		showComments(v,viewHolder.getFeedItem());
    }


    public void onLikesClick(FeedViewHolder viewHolder) {
        FeedPost item =viewHolder.getFeedItem();
        //              TODO uncomment    MultipleUsersSearch.query=item.getLikes().getQuery();
        MultipleUsersSearch.title="Likes";
        MultipleUsersSearch.error="No likes found";
        startActivity(new Intent(context,MultipleUsersSearch.class));


    /*    LayoutInflater inflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view =inflater.inflate(R.layout.pop_likes,null,false);
        final ListView lvLikes =(ListView) view.findViewById(R.id.lvLikes);
        final ProgressView pvLikes=(ProgressView)view.findViewById(R.id.pvLikes);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size= new Point();
        display.getSize(size);
        PopupWindow popupWindow = new PopupWindow(view,size.x-50,size.y-50,true);
        popupWindow.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.pop_up_bg));
        popupWindow.setOutsideTouchable(false);
        popupWindow.showAtLocation(viewHolder.tsLikesCounter, Gravity.TOP,0,400);


        item.getLikes().getQuery().findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> list, ParseException e) {
                pvLikes.stop();
                if(e==null)
                {
                    Activity activity = Domicile.getInstance();
                    if(activity!=null)
                        lvLikes.setAdapter(new MultipleUsersAdapter(activity,list, new BoomMenuButton.OnSubButtonClickListener() {
                        @Override
                        public void onClick(int buttonIndex, BoomMenuButton boomMenuButton) {

                            final JabuWingsUser user=(JabuWingsUser) boomMenuButton.getTag();
                            switch (buttonIndex)
                            {
                                case 0:
                                    Intent intent= new Intent(context,ImageViewer.class);
                                    intent.putExtra(Const.PRIVATE,false);
                                    intent.putExtra(Const.ONLINE,true);
                                    intent.putExtra(Const.VIEW_THIS_IMAGE,user.getName());
                                    intent.putExtra(Const.IMAGE_VIEWER_URI,user.getProfilePictureUrl());
                                    startActivity(intent);
                                    break;
                                case 1:
                                    ProfileOfFriend.jabuWingsUser=user;
                                    startActivity(new Intent(context,ProfileOfFriend.class).putExtra(Const.ONLINE,true));
                                    break;
                                case 2:
                                    new MaterialDialog.Builder(getActivity())
                                            .title("Add friend")
                                            .content("Send request to @"+user.getUsername()+" ")
                                            .positiveText(getString(R.string.okay))
                                            .negativeText(getString(R.string.cancel))
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                    manager.shortToast("Attempting to send request to @"+user.getUsername());
                                                    HotSpot.sendFriendRequest(context,user.getUsername(),"Feed");
                                                }
                                            }).show();
                                    break;

                            }
                        }
                    }));
                }
            }
        });*/
    }




    private boolean validateComment(String comment,SendCommentButton btnSendComment) {
        if (TextUtils.isEmpty(comment)) {
            btnSendComment.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake_error));
            return false;
        }

        return true;
    }

    public void onMoreClick(View v,final int position) {
        //FeedContextMenuManager.getInstance().toggleContextMenuFromView(v, position, this);
        final FeedPost feedItem=(FeedPost) v.getTag();
        String postId = feedSnapshot.get(position).getId();
        switch (position)
        {
            case 0:
                startActivity(Intent.createChooser(new Intent().setAction(Intent.ACTION_SEND).putExtra(Intent.EXTRA_TEXT,"Check out this JabuWings Feed \n"+feedItem.getShareUrl(manager.getWebUrl(), postId)).setType("text/plain"),"Select an app to share with."));
                break;
            case 1:
                manager.copyToClipboard(feedItem.getShareUrl(manager.getWebUrl(), feedSnapshot.get(position).getId()));
                manager.successToast("Feed url copied.");
                break;
            case 2:
                new MaterialDialog.Builder(getActivity())
                        .title("Report feed")
                        .content("What to you think is wrong with this feed")
                        .input("State the problem", null, false, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                dialog.dismiss();
                                final SweetAlertDialog sweetAlertDialog=new DialogWizard(getActivity()).showProgress(getString(R.string.please_wait),"Reporting the feed",true);

                                HashMap<String,Object> params =new HashMap<>();
                                params.put(Const.Feed.FEED_ID,feedSnapshot.get(position).getId());
                                params.put(Const.VERSION,manager.getVersion());

                                ParseCloud.callFunctionInBackground("feedReport", params, new FunctionCallback<String>() {
                                    @Override
                                    public void done(String s, ParseException e) {
                                        sweetAlertDialog.dismiss();
                                        if(e==null)
                                        {
                                            if(s!=null)
                                            manager.successToast(s);
                                        }
                                        else
                                            ErrorHandler.handleError(context,e);
                                    }
                                });

                            }
                        }).show();

                break;
        }
    }


    public void onReadMoreClick(FeedViewHolder viewHolder) {
        onCommentsClick(viewHolder.btnComments,viewHolder);
    }

    public void onProfileClick(View v) {

    }


    @Override
    public void onReportClick(int feedItem) {

    }

    @Override
    public void onSharePhotoClick(int feedItem) {

    }

    @Override
    public void onCopyShareUrlClick(int feedItem) {

    }

    @Override
    public void onCancelClick(int feedItem) {

    }



	@Override
	public void onRefresh() {

        context.startService(new Intent(context,FeedService.class));
	}


	public void updateFeed()

	{
        feedChanged();
                /*
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        updateFeed();
                    }
                }, 12000);
*/

	}

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    List<DocumentSnapshot> feedSnapshot;
    private void startDatabase(){

        Query query = db
                .collection("Feed")
                .orderBy(Const.TIMESTAMP, Query.Direction.DESCENDING)
                .limit(50);
        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                if(e==null)
                 feedSnapshot = documentSnapshots.getDocuments();
            }
        });


        // Configure recycler adapter options:
//  * query is the Query object defined above.
//  * Chat.class instructs the adapter to convert each DocumentSnapshot to a Chat object
        FirestoreRecyclerOptions<FeedPost> options = new FirestoreRecyclerOptions.Builder<FeedPost>()
                .setQuery(query, FeedPost.class)
                .build();


        adapter = new FirestoreRecyclerAdapter<FeedPost, FeedViewHolder>(options) {
            @Override
            public void onBindViewHolder(FeedViewHolder holder, int position, FeedPost model) {
                manager.log("FeedItem "+model.getContent());
                manager.log("FeedItem "+model.getPicture());
                manager.log("FeedItem "+model.toString());


                DocumentSnapshot snapshot = (DocumentSnapshot)adapter.getSnapshots().getSnapshot(0);
                snapshot.toObject(FeedPost.class);
                if(model.getPicture()!=null)
                {
                    (holder).bindView(model);
                }
                else
                    (holder).bindNoImageView(model);


            }





            @Override
            public FeedViewHolder onCreateViewHolder(ViewGroup group, int i) {
                // Create a new instance of the ViewHolder, in this case we are using a custom
                // layout called R.layout.message for each item

                View view = LayoutInflater.from(context).inflate(R.layout.item_feed, group, false);
                final FeedViewHolder feedViewHolder = new FeedViewHolder(view);

                setupClickableViews(view, feedViewHolder);

                return feedViewHolder;
            }
        };
        rvFeedRecycler.setAdapter(adapter);
    }

    private void setupClickableViews(final View view, final FeedViewHolder feedViewHolder) {
        feedViewHolder.btnComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCommentsClick(view, feedViewHolder);
            }
        });
        feedViewHolder.tvFeedComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onCommentsClick(view,feedViewHolder);
            }
        });
        feedViewHolder.tsLikesCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLikesClick(feedViewHolder);
            }
        });
        feedViewHolder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMoreClick(v, feedViewHolder.getAdapterPosition());
            }
        });
        feedViewHolder.ivFeedCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int adapterPosition = feedViewHolder.getAdapterPosition();
                /*FeedItem item =feedItems.get(adapterPosition);//              TODO uncomment
                if(!item.isLiked())
                {
                    item.like(context);
                    item.likesCount++;

//                    animatePhotoLike(cellFeedViewHolder);
//                    animateHeartButton(cellFeedViewHolder);
//                    updateLikesCounter(cellFeedViewHolder, cellFeedViewHolder.getFeedItem().likesCount);

                    adapter.notifyItemChanged(adapterPosition, ACTION_LIKE_IMAGE_CLICKED);
                }
*/

            }
        });

        feedViewHolder.btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!feedViewHolder.getFeedItem().isLiked(feedViewHolder.getSnapshot().getId()))
                {
                    ((ImageButton)v).setImageResource( R.drawable.ic_heart_red);
                    manager.log("snapshots.....");
                    String  postId = feedSnapshot.get(feedViewHolder.getAdapterPosition()).getId();
                    manager.log(postId);
                    Map<String, Object> like = new HashMap<>();
//                    like.put(Const.ID, manager.getFirebaseUserId());
                    like.put(Const.TIMESTAMP, FieldValue.serverTimestamp());
                    db.collection("Feed")
                            .document(postId)
                            .collection(Const.Feed.LIKES)
                            .document(manager.getFirebaseUserId())
                            .set(like).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            manager.log("done");
                            feedViewHolder.getFeedItem().like(feedViewHolder.getSnapshot().getId());
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            manager.log("cannot update ");
                            e.printStackTrace();
                        }
                    });
                }



            }
        });
        feedViewHolder.ivUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onProfileClick(view);
            }
        });
        feedViewHolder.tvFeedReadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onReadMoreClick(feedViewHolder);
            }
        });
    }



    public class FeedViewHolder extends RecyclerView.ViewHolder implements OnBoomListener {
        @InjectView(R.id.ivFeedCenter)
        ImageView ivFeedCenter;
        @InjectView(R.id.tvFeedContent)
        JabuWingsTextView tvFeedContent;
        @InjectView(R.id.tvFeedReadMore)
        TextView tvFeedReadMore;
        @InjectView(R.id.tvFeedUsername)
        TextView tvFeedUsername;
        @InjectView(R.id.tvFeedDate)
        TextView tvFeedDate;
        @InjectView(R.id.btnComments)
        ImageButton btnComments;
        @InjectView(R.id.btnLike)
        public ImageButton btnLike;
        @InjectView(R.id.btnMore)
        BoomMenuButton btnMore;
        @InjectView(R.id.vBgLike)
        public View vBgLike;
        @InjectView(R.id.ivLike)
        public ImageView ivLike;
        @InjectView(R.id.tsLikesCounter)
        public TextSwitcher tsLikesCounter;
        @InjectView(R.id.tvFeedComments)
        public TextView tvFeedComments;
        @InjectView(R.id.ivUserProfile)
        ImageView ivUserProfile;
        @InjectView(R.id.vImageRoot)
        FrameLayout vImageRoot;
        /*@InjectView(R.id.video_view)
        SimpleExoPlayerView playerView;*/
        FeedPost feedItem;
        DocumentSnapshot snapshot;
        public FeedViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
            final String[] circleSubButtonTexts={"Share","Copy","Report"};
            int[] drawablesResource = new int[]{
                    R.drawable.share,
                    R.drawable.copy,
                    R.drawable.report_problem
            };
            btnMore.clearBuilders();
            for (int i = 0; i < btnMore.getPiecePlaceEnum().pieceNumber(); i++)
                btnMore.addBuilder(new TextInsideCircleButton.Builder().normalImageRes(drawablesResource[i]).normalText(circleSubButtonTexts[i]).imagePadding(new Rect(30,30,30,30)).typeface(new Manager(context).getTypeFace()));



            btnMore.setOnBoomListener(this);

        }


        @Override
        public void onClicked(int index, BoomButton boomButton, BoomMenuButton menuButton) {
            onMoreClick(menuButton,index);
        }

        @Override
        public void onBackgroundClick() {

        }

        @Override
        public void onBoomWillHide() {

        }

        @Override
        public void onBoomDidHide() {

        }

        @Override
        public void onBoomWillShow() {

        }

        @Override
        public void onBoomDidShow() {

        }

        public void bindVideoView()
        {
//            playerView.requestFocus();
//            playerView.setPlayer(player);


          /*  MediaSource mediaSource = new HlsMediaSource(Uri.parse("https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"),
                    mediaDataSourceFactory, mainHandler, null);

            player.prepare(mediaSource);
            player.setPlayWhenReady(true);*/
        }

        public void bindNoImageView(final FeedPost feedItem)
        {
            this.feedItem=feedItem;
            btnMore.setTag(feedItem);
            vImageRoot.setVisibility(View.GONE);
            int adapterPosition = getAdapterPosition();adapterPosition = getAdapterPosition();
            tvFeedDate.setText(new Time().parseTime(new Date()));

            tvFeedContent.set(feedItem.getContent());
            tvFeedContent.setMaxLines(20);

            Layout l = tvFeedContent.getLayout();
            if(l!=null)
            {
                int lines = l.getLineCount();
                if(lines>20)
                {
                    tvFeedReadMore.setVisibility(View.VISIBLE);
                }
                else{
                    tvFeedReadMore.setVisibility(View.GONE);
                }

            }

            bindGeneral(feedItem);


        }
        private void bindGeneral(final FeedPost feedItem)
        {
            snapshot = feedSnapshot.get(getAdapterPosition());
            JabuWingsActivity.setUpTextViews(context,tvFeedComments,tvFeedContent,tvFeedUsername,tvFeedDate);

            final DocumentReference docRef = db.collection("users").document(feedItem.getAuthor());
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document != null && document.exists()) {
                            final JabuWingsFirebaseUser user = document.toObject(JabuWingsFirebaseUser.class);
                            tvFeedUsername.setText(user.getName());

                            if(user.hasPicture())
                            {
                                Picasso.with(context)
                                        .load(user.getPicture())
                                        .networkPolicy(NetworkPolicy.OFFLINE)
                                        .transform(new CircleTransform())
                                        .into(ivUserProfile, new Callback() {
                                            @Override
                                            public void onSuccess() {

                                            }

                                            @Override
                                            public void onError() {
                                                Picasso.with(context)
                                                        .load(user.getPicture())
                                                        .transform(new CircleTransform())
                                                        .into(ivUserProfile);
                                            }
                                        });
                            }
                            Log.d(TAG, "DocumentSnapshot data: " + task.getResult().getData());
                        } else {
                            Log.d(TAG, "No such document");
                        }
                    } else {
                        Log.d(TAG, "get failed with ", task.getException());
                    }
                }
            });

            btnLike.setImageResource(feedItem.isLiked(snapshot.getId()) ? R.drawable.ic_heart_red : R.drawable.ic_heart_outline_grey);
            tsLikesCounter.setCurrentText(vImageRoot.getResources().getQuantityString(
                    R.plurals.likes_count, feedItem.getLikesCount(), feedItem.getLikesCount()
            ));

            tvFeedComments.setText(vImageRoot.getResources().getQuantityString(
                    R.plurals.comments_count, feedItem.getCommentsCount(), feedItem.getCommentsCount()
            ));
            tvFeedDate.setText(new Time().parseTime(feedItem.getTimestamp()));

        }
        public void bindView(final FeedPost feedItem) {
            this.feedItem=feedItem;
            btnMore.setTag(feedItem);
            int adapterPosition = getAdapterPosition();adapterPosition = getAdapterPosition();
//            tvFeedUsername.setText(feedItem.getName());
//            tvFeedDate.setText(new Time().parseTime(feedItem.getDate()));


            if(feedItem.getContent()!=null)
                tvFeedContent.set(feedItem.getContent());
            Layout l = tvFeedContent.getLayout();
            if(l!=null)
            {
                int lines = l.getLineCount();
                if(lines>10)
                {
                    tvFeedReadMore.setVisibility(View.VISIBLE);
                }
                else{
                    tvFeedReadMore.setVisibility(View.GONE);
                }

          /*      if(lines>0)
                {
                    if(l.getEllipsisCount(lines-1)>0)
                    {
                        tvFeedReadMore.setVisibility(View.VISIBLE);

                    }
                }*/
            }



            if(feedItem.hasPicture())
            {


                
                Glide.with(context)
                        .using(new FirebaseImageLoader())
                        .load(FirebaseStorage.getInstance().getReference().child(feedItem.getPicture()))
                        .into(ivFeedCenter);
                /*Picasso.with(context)
                        .load(feedItem.getPicture())
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .into(ivFeedCenter, new Callback() {
                            @Override
                            public void onSuccess() {
                                //new Manager(context).shortToast("Picture load success");
                            }

                            @Override
                            public void onError() {
                                //new Manager(context).shortToast("Picture load error");
                                //Try again online if cache failed
                                Picasso.with(context)
                                        .load(feedItem.getPicture())
                                        .error(R.drawable.default_avatar)
                                        .into(ivFeedCenter, new Callback() {
                                            @Override
                                            public void onSuccess() {
                                            }

                                            @Override
                                            public void onError() {

                                            }
                                        });
                            }
                        });*/
            }

            else{
                vImageRoot.setVisibility(View.GONE);
            }
            bindGeneral(feedItem);


        }



        public FeedPost getFeedItem() {
            return feedItem;
        }
        public DocumentSnapshot getSnapshot() {
            return snapshot;
        }
    }




}
