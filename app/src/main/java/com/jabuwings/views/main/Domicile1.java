package com.jabuwings.views.main;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.intelligence.lisa.ChatWithLisa;
import com.jabuwings.management.Const;
import com.jabuwings.management.SessionManager;
import com.jabuwings.widgets.CircleImage;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.jabuwings.widgets.spacenavigation.SpaceItem;
import com.jabuwings.widgets.spacenavigation.SpaceNavigationView;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.nightonke.boommenu.BoomMenuButton;
import com.parse.ParseConfig;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class Domicile1 extends JabuWingsActivity {
    @InjectView(R.id.space)
    SpaceNavigationView space;
    @InjectView(R.id.domicile_toolbar_title)
    TextView tvTitle;
//    @InjectView(R.id.boomDomicileMenu)
//    BoomMenuButton menuButton;

    static Toolbar toolbar;
    static ViewPager viewPager;
    public static boolean newSignIn;
    public static CircleImage profilePicture;
    static Domicile instance;
    MaterialSearchView searchView;
    public static BoomMenuButton boom;
    BoomMenuButton boom2;

    boolean showTutorial=false,share=false;
    public static boolean showDownloadNewVersion=false;
    public OnBadgeChangedListener badgeChangedListener;
    public static boolean openCustom=false;
    public static int customPosition=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_domicile);
        setUpToolbar(null);
        ButterKnife.inject(this);
        //space.initWithSaveInstanceState(savedInstanceState);
        space.addSpaceItem(new SpaceItem("Acad", R.drawable.ic_school_white_24dp));
        space.addSpaceItem(new SpaceItem("Feed", R.drawable.ic_view_headline_white_24dp));
        space.addSpaceItem(new SpaceItem("Friends", R.drawable.ic_chat_bubble_outline_white_24dp));
        space.addSpaceItem(new SpaceItem("Hubs", R.drawable.ic_forum_white_24dp));



    }

    private void showDownloadNewVersion()
    {
        final SessionManager sessionManager=new SessionManager(this);

        ParseConfig config=ParseConfig.getCurrentConfig();
        try {
            new SweetAlertDialog(Domicile1.this, SweetAlertDialog.WARNING_TYPE).setTitleText("New Version Available: ")
                    .setConfirmText(context.getString(R.string.update)).setCancelText("Not Now?").setContentText("Version " + config.getString(Const.VERSION) + "\n " +
                    "Having the latest version guarantees you the maximum security and the latest features. Please Download now").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sessionManager.setUpdateNoRemind(0);
                    sweetAlertDialog.dismiss();
                    sessionManager.directToGooglePlay();
                }
            }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.dismiss();
                    sessionManager.setUpdateNoRemind(System.currentTimeMillis());
                    sessionManager.longToast("You will be reminded later. To turn off update dialogs, go to the settings page");
                }
            }).show();
        }
        catch(Exception e )
        {
            e.printStackTrace();
        }
        showDownloadNewVersion=false;
    }

    private void showRate()
    {
        manager.showRateUsDialog();

    }
    private void showTutorial()
    {

        String m="Welcome to JabuWings. A social network designed for the Joseph Ayo Babalola University and beyond. Let's learn something.";
        final String feed="JabuWings feed contains all posts across the social network. Note that when you post on the feed, everyone on JabuWings can see it.";
        final String notification="All notifications associated to your account will appear here.";
        final String friends="All your friends on JabuWings wil appear her. Note that Lisa is a chat robot designed by JabuWings.";
        final String hubs="Hubs are like groups where you can chat and socialise together.";
        final String acad="Here. You have access to the extra academic benefits of the social network.";
        final String other="To access your profile, click the profile image on the toolbar above. \n" +
                " To perform several actions like adding friends, and so on, press the button below. And finally, to get help, and change somethings, press the button at the left top corner";
        SweetAlertDialog dialog =new SweetAlertDialog(this,SweetAlertDialog.NORMAL_TYPE);
        dialog.setTitle(R.string.lisa_hello);
        dialog.setContentText(m);
        dialog.setConfirmText(getString(R.string.ok));
        dialog.setCancelText(getString(R.string.skip));
        dialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                viewPager.setCurrentItem(0);
                sweetAlertDialog.setTitle(R.string.notifications);
                sweetAlertDialog.setContentText(notification);
                sweetAlertDialog.setConfirmText(getString(R.string.next));
                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        viewPager.setCurrentItem(1);
                        sweetAlertDialog.setTitle(R.string.title_activity_feed);
                        sweetAlertDialog.setContentText(feed);
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                viewPager.setCurrentItem(2);
                                sweetAlertDialog.setTitle(R.string.friends);
                                sweetAlertDialog.setContentText(friends);
                                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        viewPager.setCurrentItem(3);
                                        sweetAlertDialog.setTitle(R.string.hubs);
                                        sweetAlertDialog.setContentText(hubs);
                                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                viewPager.setCurrentItem(4);
                                                sweetAlertDialog.setTitle(R.string.acad);
                                                sweetAlertDialog.setContentText(acad);
                                                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                        viewPager.setCurrentItem(2);
                                                        sweetAlertDialog.setTitleText("That didn't take long.");
                                                        sweetAlertDialog.setContentText(other);
                                                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                                sweetAlertDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                                                sweetAlertDialog.setTitleText("Ahah");
                                                                sweetAlertDialog.setContentText("Once again. Welcome to JabuWings.\n Press that button and let's get started. :)");
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
        dialog.show();





    }

    private void initViews()
    {
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        Typeface font= Typeface.createFromAsset(context.getAssets(),"fonts/DancingScript-Regular.otf");
        tvTitle.setTypeface(font);
        tvTitle.setText(R.string.app_name);

       // setUpBoom();

        //searchView = (MaterialSearchView) findViewById(R.id.search_view);

        List<String> friends=manager.getDatabaseManager().getFriends();

        String[] suggestions=new String[friends.size()];
        for(int i=0; i<=friends.size()-1; i++) suggestions[i]=friends.get(i);

        searchView.setSuggestions(suggestions);
        searchView.setHint("Enter Username or Id");

        searchView.setSubmitOnClick(true);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String friendUsername=query;
                if (friendUsername.equals("lisa")) {
                    startActivity(new Intent(context,
                            ChatWithLisa.class).putExtra(
                            Const.FRIEND_ID, friendUsername));
                } else {
                    startActivity(new Intent(context,
                            Chat.class).putExtra(
                            Const.FRIEND_ID, friendUsername));
                }

                //Do some magic
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });

        if(share) {
            manager.longToast("Select Friend or hub to share with");
            Chat.shareIntent=getIntent();
            Chat.share=true;
            HubChat.shareIntent=getIntent();
            HubChat.share=true;
            share=false;
        }

    }
}
