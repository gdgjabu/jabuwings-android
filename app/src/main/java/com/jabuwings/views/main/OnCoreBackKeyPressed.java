package com.jabuwings.views.main;

/**
 * Created by Falade James on 9/16/2016 All Rights Reserved.
 */
public interface OnCoreBackKeyPressed {
    void onBackKeyPressed();
}
