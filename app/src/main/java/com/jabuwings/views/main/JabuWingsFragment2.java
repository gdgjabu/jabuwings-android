package com.jabuwings.views.main;



import android.app.Fragment;
import android.view.View;

/**
 * Created by Falade James on 11/28/2015 All Rights Reserved.
 */

/**
 * This fragment is the basis fragment for most JabuWings fragments. Most of the fragments extend this fragment
 * to inherit some features so as to avoid keep repeating the same thing across fragments
 */
@SuppressWarnings("ConstantConditions")
public class JabuWingsFragment2 extends Fragment {
   View rootView;

   public void setUpFragment(View rootView)
   {
       this.rootView=rootView;
   }

  protected View findViewById(int resId)
  {
      return rootView.findViewById(resId);
  }

}
