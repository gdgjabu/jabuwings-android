package com.jabuwings.views.main;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.encryption.Crypt;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.image.ImageProcessor;
import com.jabuwings.image.PictureUploader;
import com.jabuwings.management.Const;
import com.jabuwings.models.HubMember;
import com.jabuwings.models.HubMembersRecycler;
import com.jabuwings.models.JabuWingsHub;
import com.jabuwings.models.JabuWingsPublicUser;
import com.jabuwings.models.JabuWingsUser;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.FileManager;
import com.jabuwings.views.hooks.ImageViewer;
import com.jabuwings.widgets.CircleImage;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.jabuwings.widgets.contextmenu.ContextMenuDialogFragment;
import com.jabuwings.widgets.contextmenu.MenuObject;
import com.jabuwings.widgets.contextmenu.MenuParams;
import com.jabuwings.widgets.contextmenu.interfaces.OnMenuItemClickListener;
import com.jabuwings.widgets.contextmenu.interfaces.OnMenuItemLongClickListener;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.rey.material.widget.ProgressView;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class HubProfile extends JabuWingsActivity implements View.OnClickListener,OnMenuItemClickListener, OnMenuItemLongClickListener {

    @InjectView(R.id.ivHubProfilePhoto)
    CircleImage civProfilePicture;

    @InjectView(R.id.tvHubTitle)
    TextView tvHubTitle;

    @InjectView(R.id.tvHubMembers)
    TextView tvHubMembers;

    @InjectView(R.id.tvHubChiefAdmin)
    TextView tvHubChiefAdmin;

    @InjectView(R.id.tvHubDesc)
    TextView tvHubDesc;

    @InjectView(R.id.ivHubVerified)
    ImageView ivVerifiedBadge;

    @InjectView(R.id.rvHubProfile)
    RecyclerView rvMembers;

    @InjectView(R.id.pvHubProfile)
    ProgressView pvLoading;


    DatabaseManager databaseManager;
    private FragmentManager fragmentManager;
    private ContextMenuDialogFragment mMenuDialogFragment;
    SQLiteDatabase db;
    String hubTitle, hubStatus,hubChiefAdmin,hubDesc,profilePictureFileName,verifiedAs,hubId,hubMembers;
    ParseRelation<JabuWingsPublicUser> members;
    JabuWingsHub hub;
    String pin;
    DialogWizard dialogWizard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hub_profile);
        setUpToolbar(this);
        fragmentManager=getSupportFragmentManager();
        dialogWizard=new DialogWizard(HubProfile.this);
        ButterKnife.inject(this);
        databaseManager = new DatabaseManager(context);
        db= databaseManager.getWritableDatabase();
        hubId= getIntent().getStringExtra(Const.HUB_ID);
        //manager.shortToast("id "+hubId);

        //REVISIT TECHNIQUES WITH LOCAL DATASTORE IN MIND
        profilePictureFileName=manager.getFriendOrHubProfilePictureName(hubId);
        loadPhoto(profilePictureFileName);
        init();








    }

    private void init()
    {
        ParseQuery.getQuery(Const.HUB_CLASS_NAME).fromLocalDatastore().getInBackground(hubId, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(e==null) {
                    hub = new JabuWingsHub(parseObject);
                    hubTitle = hub.getTitle();
                    hubStatus = hub.getStatus();
                    hubChiefAdmin = hub.getCreatedBy();
                    hubDesc = hub.getDescription();
                    members = hub.getMembers();
                    tvHubTitle.setText(hubTitle);
                    tvHubDesc.setText(hubDesc);
                    tvHubChiefAdmin.setText("Directed by @"+hubChiefAdmin);
                    //tvHubMembers.setText(members.size() +" Members");

                    boolean isVerified= hub.isVerified();
                    if(isVerified) {
                        ivVerifiedBadge.setVisibility(View.VISIBLE);
                        verifiedAs=hub.isVerifiedAs();
                    }
                    getMembersFromDataStore();
                    if(hub.getLocalAdmins().size()==0)
                    {
                        manager.errorToast("Unable to get admins");
                    }
                    initMenuFragment();

                    //Get hub from local database to compare the instance
                    String where = DataProvider.COL_HUB_ID + "=?";
                    String[] whereArgs = new String[]{hubId};
                    Cursor cursor = db.query(true, DataProvider.TABLE_HUBS, null, where, whereArgs, null, null, null, null);
                    if(cursor.moveToFirst())
                    {

                        long ins=cursor.getLong(cursor.getColumnIndex(DataProvider.COL_HUB_INSTANCE_TIME));
                        if(hub.getHub().getUpdatedAt().getTime()!=ins)
                        {
                            getMembers();
                        }
                        cursor.close();

                    }
                }
                else{
                    ParseQuery<ParseObject> hubQuery= new ParseQuery<>(Const.HUB_CLASS_NAME);
                    hubQuery.getInBackground(hubId, new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject parseObject, ParseException e) {
                                if(e==null)
                                {
                                    recreate();
                                }
                        }
                    });
                    manager.shortToast(getString(R.string.please_wait));
                    e.printStackTrace();
                }
            }
        });
    }
    private void toggleLoading(boolean a)
    {
     if(a) pvLoading.start();
        else pvLoading.stop();
    }
    private void loadPhoto(String fileName) {
        new ImageProcessor(this).loadBitmap(fileName, civProfilePicture);
    }

    @OnClick(R.id.ivHubProfilePhoto)
    public void onHubProfilePhotoClick()
    {
        startActivity(new Intent(this, ImageViewer.class)
                .putExtra(Const.VIEW_THIS_IMAGE, hubTitle)
                .putExtra(Const.IMAGE_VIEWER_URI, profilePictureFileName));
    }

    @OnClick(R.id.ivHubVerified)
    public void onHubVerifiedClick()
    {
        switch (verifiedAs)
        {
            case "0":
                manager.shortToast(getString(R.string.hub_verified)+" a student hub");
                break;
            case "1":
                manager.shortToast(getString(R.string.hub_verified)+" a staff hub");
                break;
            case "2":
                manager.shortToast(getString(R.string.hub_verified)+" an alumini hub");
                break;
            default:
                manager.shortToast(getString(R.string.hub_verified)+" "+verifiedAs);
        }

    }


    private void getMembers()
    {
        toggleLoading(true);
        Log.d("Hub", "Getting members online");
            hub.getMembers().getQuery().findInBackground(new FindCallback<JabuWingsPublicUser>() {
                @Override
                public void done(List<JabuWingsPublicUser> list, ParseException e) {
                    if (e == null) {
                        //Update the instance
                        manager.getDatabaseManager().getWritableDatabase().execSQL("update hubs set instance_time=" + hub.getHub().getUpdatedAt().getTime() + " where hub_id=?",
                                new Object[]{hubId});
                        context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_MESSAGES, null);
                        ParseUser.pinAllInBackground(hubId + "_" + Const.MEMBERS, list);
                        getMembersFromDataStore(); //TODO a better method
                    }
                    else{
                        e.printStackTrace();
                    }
                }
            });
                //TODO if a member of the hub no longer exists in the hub

    }





    private void getMembersFromDataStore()
    {
        Log.d("Hub", "Getting members");
        hub.getMembers().getQuery().fromLocalDatastore().findInBackground(new FindCallback<JabuWingsPublicUser>() {
            @Override
            public void done(List<JabuWingsPublicUser> list, ParseException e) {

                if (e == null) {
                    if (list.size() == 0) {

                        getMembers();
                    } else
                    {
                        toggleLoading(false);



                        List<HubMember> hubMembers = new ArrayList<HubMember>();


                    //     if (list.size() < hub.getMembers().size()) {
                    //       getMembers();
                    // }

                    for (JabuWingsPublicUser jabuWingsUser : list) {
                        HubMember member = new HubMember(jabuWingsUser, jabuWingsUser.getUsername(), jabuWingsUser.getId(), jabuWingsUser.getName(),
                                jabuWingsUser.getProfilePictureUrl(), jabuWingsUser.getDepartment(), "", jabuWingsUser.isVerified());
                        hubMembers.add(member);
                    }
                    HubMembersRecycler hubMembersRecycler = new HubMembersRecycler(hubMembers, hubId, HubProfile.this, hub);
                    rvMembers.setLayoutManager(new LinearLayoutManager(HubProfile.this));
                    rvMembers.setAdapter(hubMembersRecycler);
                }
                } else {


                    //No members where found check online
                    e.printStackTrace();
                    getMembers();
                }
            }
        });

    }

    private void initMenuFragment() {
        MenuParams menuParams = new MenuParams();
        menuParams.setActionBarSize((int) getResources().getDimension(R.dimen.tool_bar_height));
        menuParams.setMenuObjects(getMenuObjects());
        menuParams.setClosableOutside(true);
        mMenuDialogFragment = ContextMenuDialogFragment.newInstance(menuParams);
        mMenuDialogFragment.setItemClickListener(this);
        mMenuDialogFragment.setItemLongClickListener(this);
    }
    private List<MenuObject> getMenuObjects() {
        List<MenuObject> menuObjects = new ArrayList<>();

        MenuObject close = new MenuObject();
        close.setResource(R.drawable.icn_close);


        MenuObject viewPin = new MenuObject("View hub QR code");
        viewPin.setResource(R.drawable.barcode);

        MenuObject changeHubPicture = new MenuObject("Change hub display picture");


        MenuObject generateNewHubPin = new MenuObject("Generate new hub pin");
        BitmapDrawable bd = new BitmapDrawable(getResources(),BitmapFactory.decodeResource(getResources(), R.drawable.icn_3));
        generateNewHubPin.setDrawable(bd);

        String authentication = hub.getCreatedBy().equals(manager.getUsername()) ? "View Authentication code" : "Authenticate";
        MenuObject viewAuthenticationCode = new MenuObject(authentication);

        MenuObject copyLink= new MenuObject("Copy Join Link");


        menuObjects.add(close);
        menuObjects.add(viewPin);
        menuObjects.add(changeHubPicture);
        menuObjects.add(generateNewHubPin);
        menuObjects.add(viewAuthenticationCode);
        menuObjects.add(copyLink);
        return menuObjects;
    }
    @Override
    public void onClick(View v) {

    }

    @Override
    public void onMenuItemClick(View clickedView, int position) {
        switch (position)
        {
            case 1:
                viewPin();
                break;
            case 2:
                changePicture();
                break;
            case 3:
                generateHubPin();
                break;

            case 4:
                if(hub.getCreatedBy().equals(manager.getUsername()))
                    new MaterialDialog.Builder(HubProfile.this).title("Hub Authentication code").negativeText(R.string.cancel).content("Anyone with the authentication code of this hub can claim the  ownership at any point in time. Please use it with caution. Enter your account password to confirm").inputType(InputType.TYPE_TEXT_VARIATION_PASSWORD).input("password", null, false, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                            String password = input.toString();
                            if(Crypt.getSHA256(password).equals(manager.getSHA()))
                            dialogWizard.showSimpleDialog("Hub Authentication Code",hub.getAuthenticationNo());
                            else manager.shortToast(R.string.error_incorrect_password);
                        }
                    }).onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    }).neutralText(R.string.forgot_password).onNeutral(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                            dialogWizard.requestForgottenPassword();
                        }
                    }).show();
                else{
                    new MaterialDialog.Builder(HubProfile.this).title(R.string.authenticate).content("Authenticating makes you the primary administrator of this hub. To authenticate, you need the authentication number of this hub. This can only be obtained from the creator/director of the hub").inputType(InputType.TYPE_CLASS_NUMBER).input(getString(R.string.auth_no), null, false, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(@NonNull final MaterialDialog dialog, CharSequence input) {
                            int authenticationNo;
                            try{
                            authenticationNo= Integer.valueOf(input.toString());
                            HashMap<String,Object> params=new HashMap<>();
                            params.put(Const.AUTHENTICATION_ID,authenticationNo);
                                params.put(Const.VERSION,context.getString(R.string.version_number));
                            ParseCloud.callFunctionInBackground("hubAuthenticator", params, new FunctionCallback<String>() {
                                @Override
                                public void done(String s, ParseException e) {
                                    try{
                                        String[] response=s.split("&");
                                        dialogWizard.showSimpleDialog(response[0],response[1]);
                                        hub.getHub().fetchInBackground();

                                    }
                                    catch (Exception i){
                                        dialogWizard.showSimpleDialog("Result",s);
                                    }
                                }
                            });

                            }
                            catch (Exception e)
                            {
                                manager.shortToast(R.string.error_invalid_character);
                            }
                        }
                    }).show();
                }
                break;
            case 5:
                String url= "https://www.jabuwings.com/addHub?id="+hub.getPin();
                manager.copyToClipboard(url);
                manager.successToast("Copied");
                dialogWizard.showSuccessDialog("Info","You can share the link with anyone. To use the link, just paste it in a browser or click the link");
                break;
        }
    }


    private void generateHubPin()
    {
        if(hub.getCreatedBy().equals(manager.getUsername()))
        {

            new MaterialDialog.Builder(HubProfile.this).title("Please confirm").content("Note that generating a new pin will only create a new pin. All the members of the hub will remain. To confirm, please input your account password").inputType(InputType.TYPE_TEXT_VARIATION_PASSWORD).input("password", null, false, new MaterialDialog.InputCallback() {
                @Override
                public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                    String password = input.toString();
                    if(Crypt.getSHA256(password).equals(manager.getSHA()))
                    {
                        generate();
                    }

                    else{
                        manager.shortToast("Invalid password");
                    }
                }
            }).show();
        }

        else{
            manager.shortToast("Feature only available for the director of the hub.");
        }
    }

    private void generate()
    {
        final SweetAlertDialog progress= dialogWizard.showProgress("Please wait","Requesting for a new pin",false);
        HashMap<String,Object> params= new HashMap<>();
        params.put(Const.USERNAME,manager.getUsername());
        params.put(Const.HUB_ID,hub.getId());
        params.put(Const.VERSION,context.getString(R.string.version_number));

        ParseCloud.callFunctionInBackground("hubPinChanger", params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {
                progress.dismiss();
                if(e==null)
                {

                    try{
                        String[] response = s.split("&");
                        String newPin=response[2];
                        dialogWizard.showSimpleDialog(response[0],response[1]);
                        ParseObject hubObject= hub.getHub();
                        hubObject.put(Const.PIN,newPin);
                        hubObject.pinInBackground();
                    }
                    catch (Exception i)
                    {
                        dialogWizard.showSimpleDialog("Result",s);
                    }

                }
                else
                {
                    ErrorHandler.handleError(HubProfile.this,e);
                }
            }
        });
    }
    private void changePicture()
    {
        Intent intent= new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Hub Picture"), Const.PICK_HUB_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK && data!=null)
        {
            switch (requestCode)
            {
                case Const.PICK_HUB_IMAGE:
                    startCropActivity(data.getData());
                    break;
                case Crop.REQUEST_CROP:
                    Uri uri= Crop.getOutput(data);
                    try{
                        MaterialDialog dialog = new MaterialDialog.Builder(HubProfile.this)
                                .title(getString(R.string.please_wait))
                                .content("Uploading the hub display picture")
                                .cancelable(false)
                                .progress(true, 0)
                                .show();
                        PictureUploader pictureUploader = new PictureUploader(HubProfile.this,dialog,null, Const.MEDIA_UPLOAD_TYPE.HUB_PICTURE.ordinal(),hubTitle,hubId);
                        pictureUploader.execute(uri);}
                    catch (Exception e)
                    {
                        Toast.makeText(getApplicationContext(), "error: " + e.toString(), Toast.LENGTH_LONG).show();}
                    break;
            }

        }
        else{
            manager.shortToast(R.string.error_nothing_was_obtained);
        }


    }

    private void startCropActivity(@NonNull Uri uri) {
        final File pictureFile = new FileManager(context).getProfilePictureFile();
        Uri mDestinationUri =Uri.fromFile(pictureFile);
        Crop.of(uri, mDestinationUri).asSquare().start(this);
    }
    private void viewPin()
    {
        //TODO barcode view

        pin=hub.getPin();
        if(pin!=null)
        {
            new MaterialDialog.Builder(HubProfile.this).title(hubTitle).content("The hub pin is \n "+pin).positiveText(R.string.okay).neutralText(R.string.copy).onNeutral(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    manager.copyToClipboard(pin);
                    manager.shortToast(R.string.pin_copied);
                }
            }).show();
        }
        else{
           generate();
        }


    }


    @Override
    public void onMenuItemLongClick(View clickedView, int position) {

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;
            case R.id.context_menu:
                if (fragmentManager.findFragmentByTag(ContextMenuDialogFragment.TAG) == null) {
                    mMenuDialogFragment.show(fragmentManager, ContextMenuDialogFragment.TAG);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_context_menu, menu);
        return true;
    }
    @Override
    protected void onPause() {
        super.onPause();
    }
}
