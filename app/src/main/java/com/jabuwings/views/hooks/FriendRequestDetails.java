package com.jabuwings.views.hooks;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.JabuWingsUser;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.main.Domicile;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.rey.material.widget.Button;
import com.rey.material.widget.ProgressView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class FriendRequestDetails extends JabuWingsActivity implements View.OnClickListener {

    TextView tvUsername;
    TextView tvLevel;
    TextView tvDepartment;
    TextView tvRemark;
    ImageView ivProfilePicture;
    ProgressView pvProgress;
    String name;
    String username;
    Manager manager;
    Button btAccept;
    Button btReject;
    String department;
    String level;
    String remark; String databaseId;
    String profilePictureUrl;
   public  SweetAlertDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager= new Manager(context);
        setContentView(R.layout.activity_friend_request_details);
        setUpToolbar(null);
        final SweetAlertDialog dialog=new DialogWizard(FriendRequestDetails.this).showProgress(getString(R.string.please_wait), "Loading profile", false);
        tvUsername= (TextView)findViewById(R.id.frd_username);
        tvLevel= (TextView)findViewById(R.id.frd_level);
        tvDepartment= (TextView)findViewById(R.id.frd_department);
        tvRemark= (TextView)findViewById(R.id.frd_remarks);
        pvProgress=(ProgressView)findViewById(R.id.frd_loading);
        ivProfilePicture=(ImageView)findViewById(R.id.frd_image);
        final RelativeLayout rvDepartment= (RelativeLayout)findViewById(R.id.rv_friend_request_details_department);
        final RelativeLayout rvLevel= (RelativeLayout)findViewById(R.id.rv_friend_request_details_level);
        btAccept= (Button)findViewById(R.id.frd_accept); btAccept.setOnClickListener(this);
        btReject= (Button)findViewById(R.id.frd_reject); btReject.setOnClickListener(this);


        username=String.valueOf(getIntent().getStringExtra(Const.USERNAME));
        tvUsername.setText(username);
        ParseUser.getQuery()
                .whereEqualTo(Const.USERNAME, username)
                .getFirstInBackground(new GetCallback<ParseUser>() {
                    @Override
                    public void done(ParseUser user, ParseException e) {
                        dialog.dismiss();
                        if(e==null)
                        {
                            JabuWingsUser jabuWingsUser= new JabuWingsUser(user);
                            name=jabuWingsUser.getName();
                            department =jabuWingsUser.getDepartment();
                            level=jabuWingsUser.getLevel();
                            pvProgress.start();


                            toolbar.setTitle(name);

                            if (department != null) {
                                tvDepartment.setText(department);
                            }
                            else{
                                rvDepartment.setVisibility(View.GONE);
                            }
                            if (level != null) {
                                tvLevel.setText(level);
                            }
                            else{
                                rvLevel.setVisibility(View.GONE);
                            }


                            if(!TextUtils.isEmpty(remark))
                            {
                                tvRemark.setText(remark);
                            }

                            profilePictureUrl = jabuWingsUser.getProfilePictureUrl();
                            new DownloadImageTask().execute(profilePictureUrl);
                        }
                        else{
                            manager.shortToast("Unable to load Profile");
                            ErrorHandler.handleError(FriendRequestDetails.this,e);
                        }

                    }
                });










        // ImageDownloader imageDownloader= new ImageDownloader();
        //ivProfilePicture.setImageBitmap( imageDownloader.downloadImageWithUrl(profilePictureUrl));

    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.frd_accept:
                //dialog=new DialogWizard(FriendRequestDetails.this).showProgress(getString(R.string.setting_friends),getString(R.string.just_hold_on),false);
                acceptRequest(username);
                break;
            case R.id.frd_reject:
                reject(username);
                break;

        }




    }


    public void acceptRequest(String username)
    {
        HotSpot.sendAcceptFriendRequest(context, username, manager.getUsername(),true);
    }


    public void close()
    {
        onBackPressed();
    }


    public  void reject(final String username)
    {
        new MaterialDialog.Builder(FriendRequestDetails.this)
                .title("Please confirm")
                .content("Do you want delete the request of "+username)
                .positiveText(R.string.delete_request)
                .negativeText(R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        manager.removeRequest(username);
                        manager.shortToast("Request rejected");
                        finish(); startActivity(new Intent(FriendRequestDetails.this,Domicile.class));

                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();

        //HouseKeeping.deleteNotification(this,username);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        /**
         * The system calls this to perform work in a worker thread and
         * delivers it the parameters given to AsyncTask.execute()
         */

        String FILENAME;
        String username;
        ImageView view;
        @Override
        protected Bitmap doInBackground(String... urls) {
            try {

                URL url = new URL(urls[0]);




                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                return BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
                return null;

            }


        }

        /**
         * The system calls this to perform work in the UI thread and delivers
         * the result from doInBackground()
         */
        @Override
        protected void onPostExecute(Bitmap result) {
            if(result!=null)
            ivProfilePicture.setImageBitmap(result);
            pvProgress.stop();


        }

    }

}
