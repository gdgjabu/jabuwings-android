package com.jabuwings.views.hooks;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.management.Const;
import com.jabuwings.models.JabuWingsHub;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.main.HubChat;
import com.jabuwings.views.main.Hubs;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseConfig;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.rey.material.widget.Button;

import java.util.HashMap;

/**
 * Created by Falade James on 10/10/2015 All Rights Reserved.
 */
public class HubCreationWizard extends JabuWingsActivity implements View.OnClickListener {
    Button create;
    EditText etGroupName;
    EditText etGroupDesc;
    LinearLayout layout;
    MaterialDialog dialog;

    DialogWizard dialogWizard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hub_creation_wizard);
        dialogWizard=new DialogWizard(HubCreationWizard.this);
        setUpToolbar(null, getString(R.string.create_hub));

        layout= (LinearLayout)findViewById(R.id.lv_hub_creation);
        create = (Button)findViewById(R.id.hub_create);
        create.setOnClickListener(this);
        etGroupName= (EditText)findViewById(R.id.et_hub_name);
        etGroupDesc=(EditText)findViewById(R.id.et_hub_desc);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.hub_create:
                String hubTitle=etGroupName.getText().toString().trim();
                if(TextUtils.isEmpty(etGroupName.getText().toString()) || TextUtils.isEmpty(etGroupDesc.getText().toString()))
            {
                new MaterialDialog.Builder(HubCreationWizard.this)
                        .title("All fields are required")
                        .show();
                return;
            }
                if(!hubTitle.matches("[a-zA-Z0-9 '\"-_]*"))

                {
                    dialogWizard.showSimpleDialog("Invalid Hub Name","You entered an invalid character");
                    return;
                }

                if(hubTitle.toLowerCase().contains("jabuwings"))
                {
                    dialogWizard.showSimpleDialog("Invalid Hub Name","The Hub title is reserved");
                    return;
                }
                checkAvailability(hubTitle.replaceAll("\\s+", " "));
                break;

        }





        }

private void checkAvailability(final String groupName)

{
             dialog = new MaterialDialog.Builder(this)
            .title(getString(R.string.just_hold_on))
            .content("We are checking for availability")
            .progress(true,0)
       //     .progressIndeterminateStyle(true)
            .cancelable(false)
            .show();

    ParseQuery<ParseObject> query = new ParseQuery<>(Const.HUB_CLASS_NAME);
    query.whereEqualTo(Const.HUB_TITLE, groupName);
    query.getFirstInBackground(new GetCallback<ParseObject>() {
        @Override
        public void done(ParseObject parseObject, ParseException e) {
            dialog.dismiss();
            if (e == null)
            {
                Toast.makeText(HubCreationWizard.this, getString(R.string.error_hub_creation), Toast.LENGTH_SHORT).show();
            }

            else

            if (e.getCode()==ParseException.OBJECT_NOT_FOUND){
                createHub(groupName);
            }

            else{

                ErrorHandler.handleError(getApplicationContext(),e);
            }







        }
    });






}

    private void createHub(final String name)

    {
        ParseConfig config = ParseConfig.getCurrentConfig();
        int hubPriceTag = config.getInt("hubCreationBill", 100);
        new MaterialDialog.Builder(HubCreationWizard.this).title("Review").content("Please note that creating a hub is subject to a chrimata fee of "+hubPriceTag+" We may choose not to deduct this fee and we may subsidise it. After creation, you will find out how much was deducted").positiveText(R.string.confirm).negativeText(R.string.cancel).onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog1, @NonNull DialogAction which) {
                HashMap<String,Object> params= new HashMap<>();
                params.put(Const.HUB_TITLE,name);
                params.put(Const.HUB_DESC,etGroupDesc.getText().toString());
                params.put(Const.VERSION,context.getString(R.string.version_number));
                dialog = new MaterialDialog.Builder(HubCreationWizard.this)
                        .title(getString(R.string.just_hold_on))
                        .content("We are creating the hub")
                        .progress(true,0)
                        //            .progressIndeterminateStyle(true)
                        .cancelable(false)
                        .show();

                ParseCloud.callFunctionInBackground("hubCreator", params, new FunctionCallback<String>() {
                    @Override
                    public void done(String s, ParseException e) {
                        dialog.dismiss();
                        if(e==null)
                            try{
                                final String[] response = s.split("&");
                                final String hubObjectId=response[2];
                                int balance=Integer.valueOf(response[3]);
                                ParseUser user = manager.getUser();
                                user.put(Const.CHRIMATA,balance);
                                user.pinInBackground();


                                final SweetAlertDialog materialDialog = dialogWizard.showSimpleProgress("Please wait","We are obtaining the hub");
                                new ParseQuery<>(Const.HUB_CLASS_NAME).getInBackground(hubObjectId, new GetCallback<ParseObject>() {
                                    @Override
                                    public void done(ParseObject parseObject, ParseException e) {
                                        materialDialog.dismiss();
                                        if(e==null)
                                        {
                                            try{
                                            parseObject.pin();}catch (ParseException ee){ee.printStackTrace(); manager.errorToast("Unable to save hub on device"); }
                                            HouseKeeping.storeHub(context,new JabuWingsHub(parseObject));
                                            manager.addHub(parseObject.getObjectId());
                                            manager.successToast(getString(R.string.hub_creation_successful));
                                            if(Hubs.lonely!=null)
                                                Hubs.lonely.setVisibility(View.GONE);
                                            startActivity(new Intent(HubCreationWizard.this, HubChat.class).putExtra(Const.HUB_ID,hubObjectId));
                                            finish();

                                        }

                                        else{
                                            e.printStackTrace();
                                            dialogWizard.showSimpleDialog("Something went wrong","We can confirm that your hub was created but something went wrong while trying to connect you to the hub. This is usually due to a network connection failure. Please check your network and restart the app. If your still can't see the hub, log out and log in again");
                                            dialogWizard.showSimpleDialog(response[0],response[1]);
                                        }
                                    }
                                });


                            }


                            catch (Exception e1)
                            {
                                new DialogWizard(HubCreationWizard.this).showSimpleDialog("Result",s);
                            }
                        else{
                            ErrorHandler.handleError(context,e);
                        }
                    }

                });



            }
        }).show();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }



}
