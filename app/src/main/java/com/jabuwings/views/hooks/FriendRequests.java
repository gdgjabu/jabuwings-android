package com.jabuwings.views.hooks;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.JabuWingsPublicUser;
import com.jabuwings.models.JabuWingsUser;
import com.jabuwings.models.MultipleUsersAdapter;
import com.jabuwings.views.main.JabuWingsActivity;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.OnBoomListener;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.rey.material.widget.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class FriendRequests extends JabuWingsActivity {
    List<JabuWingsPublicUser> friendRequests;
    List<String> requests;
    Manager manager;
    ProgressView progressView;
    TextView textView;
    MultipleUsersAdapter adapter; OnBoomListener listener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_requests);
        progressView=(ProgressView)findViewById(R.id.pvRequests);
        textView=(TextView)findViewById(R.id.tvRequests);
        progressView.start();
        manager=new Manager(context);
        setUpToolbar(null,"Friend Requests");
        friendRequests= new ArrayList<>();
        requests= new ArrayList<>();
        final ListView lvFriendRequests= (ListView)findViewById(R.id.lv_friend_requests);

        manager._getFriendRequests().getQuery().findInBackground(new FindCallback<JabuWingsPublicUser>() {
            @Override
            public void done(final List<JabuWingsPublicUser> list, ParseException e) {
                progressView.stop();
                progressView.setVisibility(View.GONE);
                if(e==null)
                {
                    if(list.size()==0)
                    {
                        //manager.errorToast("No friend Requests");
                        textView.setVisibility(View.VISIBLE);
                    }
                    else {
                        listener=new OnBoomListener() {
                            @Override
                            public void onClicked(int index, BoomButton b,BoomMenuButton boomButton) {
                                final JabuWingsPublicUser user=(JabuWingsPublicUser) boomButton.getTag();
                                switch (index)
                                {
                                    case 0:
                                        Intent intent= new Intent(context,ImageViewer.class);
                                        intent.putExtra(Const.PRIVATE,false);
                                        intent.putExtra(Const.ONLINE,true);
                                        intent.putExtra(Const.VIEW_THIS_IMAGE,user.getName());
                                        intent.putExtra(Const.IMAGE_VIEWER_URI,user.getProfilePictureUrl());
                                        startActivity(intent);
                                        break;
                                    case 1:
                                        startActivity(new Intent(context,ProfileOfFriend.class).putExtra(Const.ONLINE,true).putExtra(Const.FRIEND,user));
                                        break;
                                    case 2:
                                        list.remove(user);
                                        adapter.notifyDataSetChanged();
                                        //SweetAlertDialog dialog = new DialogWizard(FriendRequests.this).showProgress(getString(R.string.please_wait),getString(R.string.setting_friends),false);
                                        HotSpot.sendAcceptFriendRequest(context,user.getUsername(),manager.getUsername(),false);
                                        break;

                                }
                            }

                            @Override
                            public void onBackgroundClick() {

                            }

                            @Override
                            public void onBoomWillHide() {

                            }

                            @Override
                            public void onBoomDidHide() {

                            }

                            @Override
                            public void onBoomWillShow() {

                            }

                            @Override
                            public void onBoomDidShow() {

                            }
                        };
                        adapter=new MultipleUsersAdapter(FriendRequests.this, list, listener);
                        lvFriendRequests.setAdapter(adapter);


                    }

                }
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_friends,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.menu_check:
                check();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void check()
    {
        recreate();

    }

}
