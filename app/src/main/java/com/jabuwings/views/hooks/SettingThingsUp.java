package com.jabuwings.views.hooks;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.jabuwings.R;
import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.JabuWingsFriend;
import com.jabuwings.models.JabuWingsHub;
import com.jabuwings.models.JabuWingsPublicUser;
import com.jabuwings.views.main.Domicile;
import com.jabuwings.widgets.ProgressWheel;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.util.List;

import io.realm.Realm;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

public class SettingThingsUp extends AppCompatActivity {
Manager manager;
private static String LOG_TAG=SettingThingsUp.class.getSimpleName();
int hubCount=0;int friendsCount=0;
ProgressWheel progressWheel; boolean through;
boolean tutorial=false;
AnimationDrawable anim;
    boolean incomplete=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            findViewById(android.R.id.content).setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        }
        setContentView(R.layout.activity_settings_things_up);
        tutorial=getIntent().getBooleanExtra(Const.TUTORIAL,false);
        progressWheel=(ProgressWheel)findViewById(R.id.pw_spinner);
        progressWheel.spin();

        manager= new Manager(this);
        getFriends(manager.getUsername());

        RelativeLayout container = (RelativeLayout) findViewById(R.id.container);

        anim = (AnimationDrawable) container.getBackground();
        anim.setEnterFadeDuration(3000);
        anim.setExitFadeDuration(1000);

    }



    @Override
    protected void onResume() {
        super.onResume();
        if (anim != null && !anim.isRunning())
            anim.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (anim != null && anim.isRunning())
            anim.stop();
    }

    @Override
    public void onBackPressed() {

    }
    
    private void getFriends(final String id) {
        final SettingThingsUp activity = this;
        synchronized (activity)
        {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try{
                        progressWheel.setText("Getting started");
                        Thread.sleep(5000);
                    }
                    catch (InterruptedException e){e.printStackTrace();}
                    _getFriends(ParseUser.getCurrentUser());
                    _getHubs(ParseUser.getCurrentUser());
                    if(incomplete)
                    {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                manager.shortToast("An error occurred. Restarting...");
                                activity.recreate();
                            }
                        });
                    }
                    try {
                        progressWheel.setText("Just a moment");
                        Thread.sleep(2000);
                        progressWheel.setText("On your marks...");
                        Thread.sleep(2000);
                        progressWheel.setText("Get set...");
                        Thread.sleep(2000);
                        progressWheel.setText("Go...");
                        Thread.sleep(2000);
                    } catch (InterruptedException ee){
                        ee.printStackTrace();}
                    finalise(id);
                }
            }).start();
        }


    }

    private void finalise(String id)
    {
        manager.createLoginSession(id);
        finish();
        startActivity(new Intent(SettingThingsUp.this, Domicile.class).putExtra(Const.NEW_LOGIN, true).putExtra(Const.TUTORIAL,tutorial));
    }


    
   

    private void _getFriends(ParseUser user)
    {
        progressWheel.setText("Getting your friends");
        ParseRelation<JabuWingsFriend> relation = user.getRelation(Const.USER_FRIENDS);
        try {
            List<JabuWingsFriend> friends = relation.getQuery().find();
            if(friends.size()!=0)
            for(JabuWingsFriend friend: friends)
            {
                HouseKeeping.storeFriend(friend, getApplicationContext());
            }


        }
        catch (ParseException e)
        {
            incomplete=true;
            e.printStackTrace();

        }

        catch (RealmPrimaryKeyConstraintException e)
        {
            e.printStackTrace();
        }

    }

    private void _getHubs(ParseUser user)
    {
        progressWheel.setText("Getting Hubs");
        ParseRelation<ParseObject> relation =user.getRelation("Hubs");
        try{
            List<ParseObject> hubs=relation.getQuery().find();
            for(ParseObject hub: hubs)
            {
                hub.pin();
                JabuWingsHub jabuWingsHub=new JabuWingsHub(hub);
                List<ParseUser> admins=jabuWingsHub.getAdmins().getQuery().find();
                List<JabuWingsPublicUser> members=jabuWingsHub.getMembers().getQuery().find();
                ParseUser.pinAll(hub.getObjectId() + "_" + Const.ADMIN, admins);
                ParseUser.pinAll(hub.getObjectId() + "_" + Const.MEMBERS, members);
                HouseKeeping.storeHub(getApplicationContext(),jabuWingsHub);
            }
			
        }
        catch (ParseException e)
        {
            incomplete=true;
            e.printStackTrace();
        }
    }
    

    


}
