package com.jabuwings.views.hooks;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.jabuwings.R;
import com.jabuwings.error.SessionDeactivated;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.management.SessionManager;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.chapel.Chapel;
import com.jabuwings.views.intro.Intro;

import com.jabuwings.views.main.Domicile;
import com.jabuwings.views.main.Domicile1;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.main.LoginHook;
import com.jabuwings.widgets.ProgressWheel;

public class Splash extends JabuWingsActivity {
Manager manager;
boolean isLogin,isBlocked;
//ProgressWheel progressWheel;
private int sleep=1000;
Button openChapel;
    boolean chapel=false;


    private void chapelVisible(){
        openChapel.setVisibility(View.VISIBLE);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        openChapel=(Button)findViewById(R.id.openChapel);
        manager= new Manager(this);
        openChapel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chapel=true;
            }
        });
        Time time= new Time();
        int day=time.getDay();
        int hour =time.getHour();

        if(manager.isLoggedIn())
        switch (day)
        {
            //An hour was added in case of irregularities.

            //Sunday service 8:00am to 11:00am
            case 1:
                if(hour>=8 && hour <=12) chapelVisible();
                break;
            //Wednesday service 3:00pm to 4:00pm
            case 4:
                if(hour>=15 && hour <=17)  chapelVisible();
                break;
            //Friday service 7:00pm to 8:30pm
            case 6:
                if(hour>=19 && hour<=21)  chapelVisible();
                break;
        }
       // progressWheel=(ProgressWheel)findViewById(R.id.pw_spinner);
        //progressWheel.spin();
        final boolean pending=getIntent().getBooleanExtra(Const.PENDING,false);
        Thread timer= new Thread (){
            @Override
            public void run() {
                try{
                    if(manager.isLoggedIn())
                    {
//                        Thread.sleep(sleep);
                        new SessionManager(Splash.this).check();
                        isLogin = manager.isLoggedIn();
                        isBlocked=manager.isBlocked()||manager.isSessionBlocked();
                        if (isLogin &&manager.getUsername()!=null) {
                            if (!manager.isUserEmailVerified()) {
                                startActivity(new Intent(Splash.this, EmailVerificationHook.class));
                                finish();
                                return;
                            }
                            if (isBlocked) {
                                startActivity(new Intent(Splash.this, SessionDeactivated.class));
                                finish();
                                return;
                            }
                            manager.login();

                            if(chapel)
                            {
                                startActivity(new Intent(Splash.this, Chapel.class));
                                finish();
                                return;
                            }

                            //If pending is true, it means an activity is requested to be opened instead of the default activity. This is usually due to a notification click
                            if(pending)
                            {
                                Class a=(Class)getIntent().getSerializableExtra("class");
                                startActivity(new Intent(Splash.this, a).putExtras(getIntent()));
                            }
                            else
                            startActivity(new Intent(Splash.this, Domicile.class));
                            finish();
                        }
                        else{ startActivity(new Intent(getApplicationContext(), LoginHook.class)); finish();}
                    }

                    else
                    {
                        if(manager.isIntroViewed())
                        {
                            startActivity(new Intent(getApplicationContext(), LoginHook.class));
                        }
                        else{
                            startActivity(new Intent(getApplicationContext(), Intro.class));
//                            startActivity(new Intent(getApplicationContext(), LoginHook.class));
                        }
                    }



                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    startActivity(new Intent(getApplicationContext(), LoginHook.class));
                }
            }
        };
        timer.start();

    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        finish();
    }
}
