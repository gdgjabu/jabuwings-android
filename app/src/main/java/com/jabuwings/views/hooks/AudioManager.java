package com.jabuwings.views.hooks;

import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.intelligence.lisa.Lisa;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.Utils;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;



/**
 * Created by Falade James on 10/29/2015 All Rights Reserved.
 */
public class AudioManager implements View.OnClickListener, MediaPlayer.OnCompletionListener,SeekBar.OnSeekBarChangeListener {
private static String LOG_TAG = AudioManager.class.getSimpleName();
private Context context;
private MediaRecorder mRecorder;
MediaPlayer player;
ImageButton ibBackward, ibForward,ibPlay;
SeekBar seekBar;
File file;
String path;
String username;
SQLiteDatabase db;
Chronometer chronometer;
TextView tvCurrentDuration, tvTotalDuration;
private Handler mHandler = new Handler();
boolean play=false;
DialogFragment fragment,recordFragment;
Manager manager;
    View.OnClickListener listener= this;
    public static AudioManager audioManager=new AudioManager();
    public static AudioManager getInstance()
    {

        return audioManager;
    }
    private AudioManager()
    {}

    public AudioManager initialise(Context context)
    {
        this.context=context;
        manager=new Manager(context);
        db=new DatabaseManager(context).getWritableDatabase();
        return audioManager;

    }

    public void initialiseRecorder(String username, FragmentManager fragmentManager)
    {
        this.username=username;
        File sd = Environment.getExternalStorageDirectory();
        File dir;
        try{
            dir= new File(sd.getAbsolutePath()+"/JabuWings/JabuWings Audio/"+username);
            dir.mkdirs();
        }
        catch (Exception e)
        {
            new Manager(context).shortToast("Unable to access phone storage");
            return;
        }

        file = new File(dir, Lisa.generateAudioFileName(username)+".amr");
        generateNoMediaFile(dir);


        Dialog.Builder builder;
        builder = new SimpleDialog.Builder(R.style.SimpleDialog){

            @Override
            protected void onBuildDone(Dialog dialog) {
                dialog.layoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                ImageButton record=(ImageButton)dialog.findViewById(R.id.ib_record);
                chronometer=(Chronometer)dialog.findViewById(R.id.record_chronometer);

                record.setOnClickListener(listener);
            }

            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {

                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {

                super.onNegativeActionClicked(fragment);
            }
        };

        builder.title("Send Audio")
                .contentView(R.layout.voice_recording);
        recordFragment = DialogFragment.newInstance(builder);
        recordFragment.show(fragmentManager, null);



    }

    private void record()

    {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
        mRecorder.setOutputFile(file.getAbsolutePath());
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
            mRecorder.start();
            chronometer.setBase(SystemClock.elapsedRealtime());
            chronometer.start();

        }

        catch (Exception e) {
        Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        Log.e(LOG_TAG, "prepare() failed");
    }


    }


    public void stopRecording() {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        final String msgId=HouseKeeping.storeOutgoingPersonalMedia(context, file.getAbsolutePath(), null, username, DataProvider.MessageType.OUTGOING_VOICE_NOTE.ordinal()).getMsgId();
        final ParseFile parseFile = new ParseFile(file);

        parseFile.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e==null)
                {
//                    HotSpot.sendPersonalMedia(1,parseFile,username,context,msgId,null);
                }else {
                    manager.shortToast(e.getMessage());
                    e.printStackTrace();
                    db.execSQL("update messages set delivery_status='" + Const.MESSAGE_FAILED + "' where msgId=?",
                            new Object[]{msgId});
                    context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_MESSAGES, null);
                }
            }
        }, new ProgressCallback() {
            @Override
            public void done(Integer integer) {
                Log.d(LOG_TAG,"Progress "+integer);
                db.execSQL("update messages set file_progress='" + integer + "' where msgId=?",
                        new Object[]{msgId});
                context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_MESSAGES,null);
            }
        });


    }


    public void playAudio(String path, FragmentManager fragmentManager)
    {
        this.path=path;
        player= new MediaPlayer();


        Dialog.Builder builder;
        builder = new SimpleDialog.Builder(R.style.SimpleDialog){

            @Override
            protected void onBuildDone(Dialog dialog) {
                dialog.layoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                ibBackward=(ImageButton)dialog.findViewById(R.id.btnBackward);
                ibForward=(ImageButton)dialog.findViewById(R.id.btnForward);
                ibPlay=(ImageButton)dialog.findViewById(R.id.btnPlay);
                tvCurrentDuration= (TextView)dialog.findViewById(R.id.songCurrentDurationLabel);
                tvTotalDuration=(TextView)dialog.findViewById(R.id.songTotalDurationLabel);
                seekBar=(SeekBar)dialog.findViewById(R.id.audioProgressBar);
                ibBackward.setOnClickListener(listener);
                ibForward.setOnClickListener(listener);
                ibPlay.setOnClickListener(listener);
                playAudio();
            }

            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {

                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {

                super.onNegativeActionClicked(fragment);
            }

            @Override
            public void onDismiss(DialogInterface dialog) {
                super.onDismiss(dialog);
                player.stop();
            }
        };

        builder.title("Play audio")
                .contentView(R.layout.audio_player);
        fragment = DialogFragment.newInstance(builder);
        fragment.show(fragmentManager, null);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnPlay:
                // check for already playing
                if(player.isPlaying()){

                        player.pause();
                        // Changing button image to play button
                        ibPlay.setImageResource(R.drawable.btn_play);

                }else{
                    // Resume song

                        player.start();
                        // Changing button image to pause button
                        ibPlay.setImageResource(R.drawable.btn_pause);

                }
                break;
            case R.id.ib_record:
                play= !play;
                if(play)
                {
                    record();
                    Toast.makeText(context, "Record started", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    stopRecording();
                    recordFragment.dismiss();


                    Toast.makeText(context, "Record stopped", Toast.LENGTH_SHORT).show();
                }



                break;
        }
    }


    private void  playAudio(){

        try {
            player.reset();
            player.setDataSource(path);
            player.prepare();
            player.start();
            // Displaying Song title

            // Changing Button Image to pause image
            ibPlay.setImageResource(R.drawable.btn_pause);

            // set Progress bar values
            seekBar.setProgress(0);
            seekBar.setMax(100);
            // Updating progress bar
            updateProgressBar();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * When user starts moving the progress handler
     * */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = player.getDuration();
        int currentPosition = Utils.progressToTimer(seekBar.getProgress(), totalDuration);

        // forward or backward to certain seconds
        player.seekTo(currentPosition);

        // update timer progress again
        updateProgressBar();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        ibPlay.setImageResource(R.drawable.btn_play);

    }


    /**
     * Background Runnable thread
     * */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = player.getDuration();
            long currentDuration = player.getCurrentPosition();

            // Displaying Total Duration time
            tvTotalDuration.setText(""+ Utils.milliSecondsToTimer(totalDuration));
            // Displaying time completed playing
            tvCurrentDuration.setText(""+Utils.milliSecondsToTimer(currentDuration));

            // Updating progress bar
            int progress = (Utils.getProgressPercentage(currentDuration, totalDuration));
            //Log.d("Progress", ""+progress);
            seekBar.setProgress(progress);

            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };


    /**
     * Update timer on seekbar
     * */
    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }


    public void generateNoMediaFile(File root) {
        try {
            File noMediaFile = new File(root, ".nomedia");
            FileWriter writer = new FileWriter(noMediaFile);
            writer.append("");
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
