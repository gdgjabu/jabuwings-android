package com.jabuwings.views.hooks;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.database.realm.Friend;
import com.jabuwings.image.ImageProcessor;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.management.Const;
import com.jabuwings.management.CreditManager;
import com.jabuwings.management.Manager;
import com.jabuwings.models.Item;
import com.jabuwings.models.JabuWingsFriend;
import com.jabuwings.models.JabuWingsUser;
import com.jabuwings.models.ProfileRecycler;
import com.jabuwings.views.main.Chat;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.CircleImage;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.jabuwings.widgets.contextmenu.ContextMenuDialogFragment;
import com.jabuwings.widgets.contextmenu.MenuObject;
import com.jabuwings.widgets.contextmenu.MenuParams;
import com.jabuwings.widgets.contextmenu.interfaces.OnMenuItemClickListener;
import com.jabuwings.widgets.contextmenu.interfaces.OnMenuItemLongClickListener;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ProfileOfFriend extends JabuWingsActivity implements View.OnClickListener, OnMenuItemClickListener, OnMenuItemLongClickListener {

    CircleImage profilePicture;
    Manager manager;
    String username;
    String profilePictureUri;
    String displayPictureUri;
    RecyclerView recycler;
    TextView tvName,tvUsername,tvDepartment,tvStatus;
    ImageView ivVerifiedBadge;
    String verifiedAs;
    public JabuWingsFriend jabuWingsFriend;
    boolean isOnline,showFragment=true;

    private FragmentManager fragmentManager;
    private ContextMenuDialogFragment mMenuDialogFragment;
    Friend friend;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_of_friend);
        setUpToolbar(this);
        isOnline = getIntent().getBooleanExtra(Const.ONLINE, false);
        username = getIntent().getStringExtra(Const.USERNAME);
        fragmentManager = getSupportFragmentManager();
        profilePicture = (CircleImage) findViewById(R.id.ivFriendProfilePic);
        ivVerifiedBadge = (ImageView) findViewById(R.id.user_verified_badge);
        ivVerifiedBadge.setOnClickListener(this);
        tvName = (TextView) findViewById(R.id.tvFriendProfileName);
        tvName.setOnClickListener(this);
        tvUsername = (TextView) findViewById(R.id.tvFriendProfileUsername);
        tvDepartment = (TextView) findViewById(R.id.tvFriendProfileDepartment);
        tvStatus = (TextView) findViewById(R.id.tvFriendProfileStatus);
        recycler = (RecyclerView) findViewById(R.id.rvProfileOfFriend);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(linearLayoutManager);
        recycler.setHorizontalScrollBarEnabled(true);
        profilePicture.setOnClickListener(this);
        //      displayPicture= (ImageView)findViewById(R.id.ivFriendDisplayPicture);


        manager = new Manager(this);


        if (isOnline) {
            jabuWingsFriend = getIntent().getParcelableExtra(Const.FRIEND);
            if (jabuWingsFriend != null) {
                profilePictureUri = jabuWingsFriend.getProfilePictureUrl();
                username = jabuWingsFriend.getUsername();
                setValues(jabuWingsFriend.getName(), jabuWingsFriend.getDepartment(), jabuWingsFriend.getStatus(), jabuWingsFriend.getLevel(), jabuWingsFriend.getStateOfOrigin(), jabuWingsFriend.isVerified(), jabuWingsFriend.isVerifiedAs());

            } else {
                tvUsername.setText("@" + username);

            }
            jabuWingsFriend = null;
        } else {

            username = getIntent().getStringExtra(Const.USERNAME);
            friend = realm.where(Friend.class).equalTo(Const.USERNAME, username).findFirst();
            String name = friend.getName();
            String username = friend.getUsername();
            String department = friend.getDepartment();
            String level = friend.getLevel();
            String state = friend.getState();
            String status = friend.getStatus();
            boolean isVerified = friend.getVerifiedAs() != null;
            setValues(name, department, status, level, state, isVerified, friend.getVerifiedAs());
            profilePictureUri =
                    manager.getFriendOrHubProfilePictureName(username);
            //toolbar.setTitle(username);

            JabuWingsUser currentUser = new JabuWingsUser(ParseUser.getCurrentUser());
            if (profilePictureUri != null)
                loadThumbnailPhoto();
            if (!isOnline)
                initMenuFragment();
            else {
                showFragment = false;
                if (jabuWingsFriend != null && jabuWingsFriend.isFriendOf(manager.getUsername()) && new DatabaseManager(this).getFriends().contains(jabuWingsFriend.getUsername())) {

                    initMenuFragment();

                }
            }


        }
    }

    private void setValues(String name,String department, String status,String level,String state, boolean isVerified,String verified)
    {
        if(isVerified) {
            ivVerifiedBadge.setVisibility(View.VISIBLE);
            if(isOnline) verifiedAs=verified;
            else
            verifiedAs=friend.getVerifiedAs();
        }
        List<Item> list = new ArrayList<>();
        list.add(new Item(getString(R.string.username),username));
        if(status!=null)
            list.add(new Item(getString(R.string.status),status));
        if(department!=null)
            list.add(new Item(getString(R.string.department),department));
        if(level!=null) list.add(new Item(getString(R.string.level),level));
        list.add(new Item(getString(R.string.state), state));
        tvName.setText(name);
        tvUsername.setText("@"+username);
        tvDepartment.setText(department);
        tvStatus.setText(status);
        ProfileRecycler profileRecycler= new ProfileRecycler(list,this);
        recycler.setAdapter(profileRecycler);
    }


    private void initMenuFragment() {
        MenuParams menuParams = new MenuParams();
        menuParams.setActionBarSize((int) getResources().getDimension(R.dimen.tool_bar_height));
        menuParams.setMenuObjects(getMenuObjects());
        menuParams.setClosableOutside(true);
        mMenuDialogFragment = ContextMenuDialogFragment.newInstance(menuParams);
        mMenuDialogFragment.setItemClickListener(this);
        mMenuDialogFragment.setItemLongClickListener(this);
    }

    private List<MenuObject> getMenuObjects() {

        List<MenuObject> menuObjects = new ArrayList<>();

        MenuObject close = new MenuObject();
        close.setResource(R.drawable.icn_close);

        MenuObject send = new MenuObject("Send message");
        send.setResource(R.drawable.icn_1);

        MenuObject shareChrimata = new MenuObject("Share Chrimata");


        MenuObject block = new MenuObject("Remove friend");
        block.setResource(R.drawable.icn_5);

        menuObjects.add(close);
        menuObjects.add(send);
        menuObjects.add(shareChrimata);
        menuObjects.add(block);
        return menuObjects;
    }


    private void loadThumbnailPhoto() {

            if(!isOnline)
            new ImageProcessor(this).loadBitmap(profilePictureUri, profilePicture);
            else{
                Picasso.with(context).load(profilePictureUri).into(profilePicture);
            }
         /*   profilePicture.animate()
                    .scaleX(1.f).scaleY(1.f)
                    .setInterpolator(new OvershootInterpolator())
                    .setDuration(400)
                    .setStartDelay(200)
                    .start();*/

    }
    @Override
    public void onBackPressed() {
        if (mMenuDialogFragment != null && mMenuDialogFragment.isAdded()) {
            mMenuDialogFragment.dismiss();
        } else{
            finish();
        }
    }
    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.ivFriendProfilePic:
                if(!isOnline)
                startActivity(new Intent(this, ImageViewer.class)
                        .putExtra(Const.VIEW_THIS_IMAGE, username)
                        .putExtra(Const.IMAGE_VIEWER_URI, profilePictureUri));
                else
                    startActivity(new Intent(this, ImageViewer.class)
                            .putExtra(Const.VIEW_THIS_IMAGE, username)
                            .putExtra(Const.PRIVATE,false)
                            .putExtra(Const.ONLINE,true)
                            .putExtra(Const.IMAGE_VIEWER_URI, profilePictureUri));

                break;
            case R.id.tvFriendProfileName:
                manager.shortToast(tvName.getText().toString());
                break;
            case R.id.user_verified_badge:
                switch (verifiedAs)
                {
                    case "0":
                        manager.shortToast(Const.AT+username+Const.SPACE+getString(R.string.is_verified_as)+" a student");
                        break;
                    case "1":
                        manager.shortToast(Const.AT+username+Const.SPACE+getString(R.string.is_verified_as)+" a staff");
                        break;
                    case "2":
                        manager.shortToast(Const.AT+username+Const.SPACE+getString(R.string.is_verified_as)+" an alumini");
                        break;
                    default:
                        manager.shortToast(Const.AT+username+Const.SPACE+getString(R.string.is_verified_as)+Const.SPACE+verifiedAs);
                }

                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_context_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;
            case R.id.context_menu:
                if(showFragment)
                if (fragmentManager.findFragmentByTag(ContextMenuDialogFragment.TAG) == null) {
                    mMenuDialogFragment.show(fragmentManager, ContextMenuDialogFragment.TAG);
                }
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMenuItemClick(View clickedView, int position) {
        switch (position)
        {
            case 1:
                startActivity(new Intent(ProfileOfFriend.this, Chat.class).putExtra(Const.FRIEND_ID, username));
                break;
            case 2:
                shareChrimata();
                break;
            case 3:
                new SweetAlertDialog(ProfileOfFriend.this,SweetAlertDialog.WARNING_TYPE).setTitleText("Remove "+username).setContentText("Are you sure? "+username+" will no longer be able to send you messages").setConfirmText("Remove "+username).setCancelText(context.getString(R.string.cancel)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        HotSpot.removeFriend(ProfileOfFriend.this, username);
                    }
                }).show();
                break;
        }
    }

    private void shareChrimata()
    {
        new CreditManager(ProfileOfFriend.this).share(username);
    }

    @Override
    public void onMenuItemLongClick(View clickedView, int position) {

    }
}
