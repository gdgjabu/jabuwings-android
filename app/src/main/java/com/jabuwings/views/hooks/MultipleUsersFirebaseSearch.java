package com.jabuwings.views.hooks;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.JabuWingsPublicUser;
import com.jabuwings.models.JabuWingsUser;
import com.jabuwings.models.MultipleUsersAdapter;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.main.JabuWingsActivity;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.OnBoomListener;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.rey.material.widget.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class MultipleUsersFirebaseSearch extends JabuWingsActivity implements View.OnClickListener ,OnBoomListener {
Manager manager;
List<JabuWingsPublicUser> departmentMates;
DialogWizard dialogWizard;
ListView listView;
ProgressView progressView;
TextView textView;
MultipleUsersAdapter friendSearchAdapter;
View.OnClickListener clickListener;
OnBoomListener onSubButtonClickListener;
public static ParseQuery<JabuWingsPublicUser> query;
public static String title="Results";
public static String error="No result was found";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_department_friend_search);
        manager=new Manager(this);
        setUpToolbar(null, title);
        departmentMates= new ArrayList<>();
        clickListener=this;
        onSubButtonClickListener=this;
        friendSearchAdapter= new MultipleUsersAdapter(this,departmentMates,onSubButtonClickListener);
        listView=(ListView)findViewById(R.id.lv_department_friend_search);
        progressView=(ProgressView)findViewById(R.id.pvDepartmentSearch);
        textView=(TextView)findViewById(R.id.tvDepartmentSearch);
        textView.setText(error);
        progressView.start();
        listView.setAdapter(friendSearchAdapter);

        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            ParseQuery<JabuWingsPublicUser>  q= JabuWingsPublicUser.getQuery();
            q.whereContains(Const.NAME,query);
            q.whereNotContainedIn(Const.USERNAME,manager.getFriendUsernames());
            q.whereNotEqualTo(Const.USERNAME,manager.getUsername());
            search(q);
        }

        search(query);
        query=null;




    }

    private void search(ParseQuery<JabuWingsPublicUser> query)
    {
        if(query!=null)
            query
                    .findInBackground(new FindCallback<JabuWingsPublicUser>() {
                        @Override
                        public void done(List<JabuWingsPublicUser> list, ParseException e) {
                            progressView.stop();
                            progressView.setVisibility(View.GONE);
                            if (e == null) {
                                friendSearchAdapter.update(list);
                                friendSearchAdapter.notifyDataSetChanged();

                                String userCount;
                                int size = list.size();
                                // If no user exists, terminate the activity
                                if(size==0)
                                {
                                    textView.setVisibility(View.VISIBLE);
                                    //   finish();
                                    //manager.errorToast("We couldn't find any user from your department");
                                }
                                userCount= size==1? "user": "users";
                                //manager.successToast("We found " + size + " " + userCount + " from the department of " + manager.getDepartment()+" who are not your friend");



                                //getDepartmentMates(list);
                            } else {
                                ErrorHandler.handleError(getApplicationContext(), e);
                                finish();
                            }
                        }
                    });

    }

    public void getDepartmentMates(List<ParseUser> list)
    {
        String userCount;
        int size = list.size();
        // If no user exists, terminate the activity
        if(size==0)
        {
            finish();
            manager.longToast("We couldn't find any user from your department");
        }
        userCount= size==1? "user": "users";
        manager.longToast("We found " + size + " " + userCount + " from the department of " + manager.getDepartment()+" who are not your friend");

        for(ParseUser parseUser: list)
        {
            JabuWingsUser user= new JabuWingsUser(parseUser);
            FriendItem friendItem=new FriendItem(user.getName(),user.getUsername(),user.getProfilePictureUrl());
           // departmentMates.add(friendItem);
            friendSearchAdapter.notifyDataSetChanged();
            Log.d("DepartmentFriendSearch",friendItem.toString());

        }








    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

        }
    }

    @Override
    public void onClicked(int index, BoomButton boomButton, BoomMenuButton menuButton) {
        final JabuWingsPublicUser user=(JabuWingsPublicUser) menuButton.getTag();
        switch (index)
        {
            case 0:
                Intent intent= new Intent(context,ImageViewer.class);
                intent.putExtra(Const.PRIVATE,false);
                intent.putExtra(Const.ONLINE,true);
                intent.putExtra(Const.VIEW_THIS_IMAGE,user.getName());
                intent.putExtra(Const.IMAGE_VIEWER_URI,user.getProfilePictureUrl());
                startActivity(intent);
                break;
            case 1:
                startActivity(new Intent(context,ProfileOfFriend.class).putExtra(Const.ONLINE,true).putExtra(Const.FRIEND,user));
                break;
            case 2:
                if(!user.getUsername().equals(manager.getUsername()))
                    new MaterialDialog.Builder(MultipleUsersFirebaseSearch.this)
                            .title("Add friend")
                            .content("Send request to @"+user.getUsername()+" ")
                            .positiveText(getString(R.string.okay))
                            .negativeText(getString(R.string.cancel))
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    manager.shortToast("Attempting to send request to @"+user.getUsername());
                                    HotSpot.sendFriendRequest(getApplicationContext(),user.getUsername(),"Department");
                                }
                            }).show();
                else manager.errorToast("Unfortunately, you cannot add yourself.");
                break;

        }
    }



    @Override
    public void onBackgroundClick() {

    }

    @Override
    public void onBoomWillHide() {

    }

    @Override
    public void onBoomDidHide() {

    }

    @Override
    public void onBoomWillShow() {

    }

    @Override
    public void onBoomDidShow() {

    }








    public class FriendItem{
        public String name;
        public String username;
        public String profilePictureUrl;

        public FriendItem(String name, String username, String profilePictureUrl)
        {
            this.name=name;
            this.username=username;
            this.profilePictureUrl=profilePictureUrl;
        }


        @Override
        public String toString() {
            return "Name is "+name+", "+username+" and profilePictureUrl is "+profilePictureUrl;
        }
    }
}
