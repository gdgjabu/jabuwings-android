package com.jabuwings.views.hooks;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.image.SiliCompressor;
import com.jabuwings.intelligence.FeedService;
import com.jabuwings.intelligence.lisa.Lisa;
import com.jabuwings.management.Const;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.main.Feed;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.SaveCallback;

import com.sangcomz.fishbun.FishBun;
import com.sangcomz.fishbun.define.Define;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class FeedMonger2 extends JabuWingsActivity {

    @InjectView(R.id.tbFeedFriends)
    ToggleButton tbFriends;
    @InjectView(R.id.tbFeedPublic)
    ToggleButton tbPublic;
    @InjectView(R.id.etFeedContent)
    EditText etFeedContent;
    @InjectView(R.id.ivFeedPhoto)
    ImageView ivFeedPhoto;
    @InjectView(R.id.feed_monger_hint)
    TextView tvHint;
    String _friends="Your post will only be visible to your friends";
    String _public="Your post will be visible to everyone on JabuWings";
    boolean friends,pub=false;
    String content;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_monger_2);
        setUpToolbar(null);
        ButterKnife.inject(this);

        tvHint.setText(_friends);
        tbFriends.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                friends=isChecked;
                if(friends)
                {
                    pub=false;
                    tbPublic.setChecked(false);
                    tvHint.setText(_friends);
                }
            }
        });

        tbPublic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                pub=isChecked;
                if(pub)
                {
                    friends=false;
                    tbFriends.setChecked(false);
                    tvHint.setText(_public);
                }

            }
        });
    }




    @OnClick(R.id.ivFeedPhoto)
    public void onPhotoClick()
    {
        FishBun.with(FeedMonger2.this)
                .MultiPageMode()
                .setAlbumThumbnailSize(150)//you can resize album thumnail size
                .setActionBarColor(Color.BLACK, Color.BLUE) // actionBar and StatusBar color
                //        .setActionBarColor(Color.BLACK)           // only actionbar color
                .setPickerCount(1)//you can restrict photo count
                // .setArrayPaths(path)//you can choice again.
                .setPickerSpanCount(3)
                // .setRequestCode(11) //request code is 11. default == Define.ALBUM_REQUEST_CODE(27)
                .setCamera(true)//you can use camera
                .textOnImagesSelectionLimitReached("Limit Reached!")
                .textOnNothingSelected("Nothing Selected")
                .setButtonInAlbumActivity(true)
                .setReachLimitAutomaticClose(true)
                .startAlbum();

    }
String p;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_OK && data!=null)
        {
            if(requestCode== Define.ALBUM_REQUEST_CODE)
            {

                ArrayList<Uri> path = data.getParcelableArrayListExtra(Define.INTENT_PATH);
                if(path.size()>0)
                {
                    p = SiliCompressor.with(this).compress(path.get(0).toString());
                    try {
                        ivFeedPhoto.setImageBitmap(SiliCompressor.with(this).getCompressBitmap(path.get(0).toString()));
                    }
                    catch (IOException e){e.printStackTrace();}

                }
            }
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_feed_monger, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_publish) {
            content=etFeedContent.getText().toString();
            upload(p);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void upload(String path)
    {
        if(TextUtils.isEmpty(content) && path==null)
        {
            new DialogWizard(FeedMonger2.this).showWarningDialog("","The content cannot be empty");
        }
        else
        {
            if(path!=null)
            uploadToFirebase(Uri.fromFile(new File(path)));
            else savePost(null);
            finish();



        }
    }

    private void proceed(ParseFile file,String content)
    {
        HashMap<String, Object> params = new HashMap<>();
        params.put(Const.Feed.AUTHOR, manager.getUsername());
       // params.put(Const.Feed.TITLE, FeedMonger.title);
        params.put(Const.Feed.CONTENT, content);
        params.put("public",pub);
        if(file!=null) {
            params.put(Const.Feed.PICTURE_FILE, file);
        }
        //params.put(Const.Feed.FEED_ID, Lisa.generateFeedId(manager.getUsername()));
        //params.put(Const.Feed.EXTERNAL_URL, FeedMonger.feedUrl);
        params.put(Const.VERSION,getString(R.string.version_number));

        ParseCloud.callFunctionInBackground(Const.FEED_MONGER_CLASS, params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {
                if (e == null) {
                    if (s.equals(Const.POSITIVE)) {
                        manager.successToast(getString(R.string.success_feed));
                    } else {
                        manager.shortToast(s);
                    }

                } else {
                    // manager.storeFeedDraft(title,content,feedUrl);
                    ErrorHandler.handleError(context,e);
                }


            }
        });


    }

    private void uploadToFirebase(Uri uri)
    {
        // Create a storage reference from our app
        StorageReference storageRef = FirebaseStorage.getInstance().getReference();

        // Points to "images"
        StorageReference imagesRef = storageRef.child("feed_images");

        final String fileName = UUID.randomUUID()+".jpg";
        final StorageReference feedImageRef = imagesRef.child(fileName);

        UploadTask uploadTask = feedImageRef.putFile(uri);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                exception.printStackTrace();
                manager.errorToast("firebase oops "+ exception.getMessage());
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                manager.shortToast("File Saved to firebase");
                savePost(feedImageRef.getPath());

            }
        });
    }

    private void savePost(String pictureName)
    {

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        // Create a new user with a first and last name
        Map<String, Object> feed = new HashMap<>();
        feed.put("content", content);
        if(pictureName!=null)
        feed.put("picture", pictureName);
        feed.put(Const.Feed.AUTHOR, FirebaseAuth.getInstance().getCurrentUser().getUid());
        feed.put(Const.TIMESTAMP, FieldValue.serverTimestamp());

// Add a new document with a generated ID
        db.collection("Feed")
                .add(feed)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        manager.successToast("Your post has been added");

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                        manager.errorToast("Unable to add your post "+e.getMessage());
                    }
                });
    }
}
