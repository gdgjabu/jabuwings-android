package com.jabuwings.views.hooks;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.jabuwings.R;
import com.jabuwings.feed.FeedItem;
import com.jabuwings.feed.FeedPost;
import com.jabuwings.management.Const;
import com.jabuwings.models.CommentsAdapter;
import com.jabuwings.utilities.Utils;
import com.jabuwings.views.main.Domicile;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.registration.StaffRegistration;
import com.jabuwings.widgets.JabuWingsTextView;
import com.jabuwings.widgets.SendCommentButton;
import com.rey.material.widget.ProgressView;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class Comments extends JabuWingsActivity implements SendCommentButton.OnSendClickListener {
    public static final String ARG_DRAWING_START_LOCATION = "arg_drawing_start_location";

    @InjectView(R.id.contentRoot)
    LinearLayout contentRoot;
    @InjectView(R.id.rvComments)
    RecyclerView rvComments;
    @InjectView(R.id.llAddComment)
    LinearLayout llAddComment;
    @InjectView(R.id.etComment)
    EditText etComment;
    @InjectView(R.id.btnSendComment)
    SendCommentButton btnSendComment;
    @InjectView(R.id.tvComments)
    JabuWingsTextView tvComment;
    @InjectView(R.id.ivComments)
    ImageView ivComment;
    @InjectView(R.id.pvComments)
    ProgressView pvComments;
    @InjectView(R.id.sv_comments)
    ScrollView svComments;

    private CommentsAdapter commentsAdapter;
    private int drawingStartLocation;

    public FeedPost feedPost;
    String id;
    private static Comments instance;

    public static Comments getInstance()
    {
        return instance;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        setUpToolbar(null,R.string.comments);
        instance=this;
        ButterKnife.inject(this);
        feedPost = (FeedPost) getIntent().getSerializableExtra(Const.Feed.COMMENTS);
        id = (String)getIntent().getStringExtra(Const.ID);
        setUpTextViews(context,tvComment);
//        feedItem=(FeedItem) getIntent().getSerializableExtra("object");

        svComments.post(new Runnable() {
            @Override
            public void run() {
                svComments.fullScroll(View.FOCUS_DOWN);
            }
        });
        setupComments();
        setupSendCommentButton();

        drawingStartLocation = getIntent().getIntExtra(ARG_DRAWING_START_LOCATION, 0);
        if (savedInstanceState == null) {
            contentRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    contentRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                    startIntroAnimation();
                    return true;
                }
            });
        }
    }

    public static void stopLoading()
    {
        Comments instance=getInstance();
        if(instance!=null) {
            if (instance.pvComments != null) instance.pvComments.stop();
            if (instance.svComments != null) instance.svComments.fullScroll(View.FOCUS_DOWN);
        }
    }

    private void setupComments() {
        pvComments.start();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvComments.setLayoutManager(linearLayoutManager);
        rvComments.setHasFixedSize(true);

        commentsAdapter = new CommentsAdapter(this,feedPost,id);
        rvComments.setAdapter(commentsAdapter);
        rvComments.setOverScrollMode(View.OVER_SCROLL_NEVER);
        rvComments.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    commentsAdapter.setAnimationsLocked(true);
                }
            }
        });

        tvComment.set(feedPost.getContent());
        if(feedPost.getPicture()!=null)
        Picasso.with(context).load(feedPost.getPicture()).into(ivComment);
        else
            ivComment.setVisibility(View.GONE);
    }

    private void setupSendCommentButton() {
        btnSendComment.setOnSendClickListener(this);
    }

    private void startIntroAnimation() {
        final Toolbar toolbar= Domicile.getToolbar();
        if(toolbar!=null)
        ViewCompat.setElevation(toolbar, 0);
        contentRoot.setScaleY(0.1f);
        contentRoot.setPivotY(drawingStartLocation);
        llAddComment.setTranslationY(200);

        contentRoot.animate()
                .scaleY(1)
                .setDuration(200)
                .setInterpolator(new AccelerateInterpolator())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if(toolbar!=null)
                        ViewCompat.setElevation(toolbar, Utils.dpToPx(8));
                        animateContent();
                    }
                })
                .start();
    }

    private void animateContent() {
        commentsAdapter.updateItems();
        llAddComment.animate().translationY(0)
                .setInterpolator(new DecelerateInterpolator())
                .setDuration(200)
                .start();
    }

    @Override
    public void onBackPressed() {
        Toolbar toolbar= Domicile.getToolbar();
        if(toolbar!=null)
        ViewCompat.setElevation(toolbar, 0);
        contentRoot.animate()
                .translationY(Utils.getScreenHeight(this))
                .setDuration(200)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        Comments.super.onBackPressed();
                        overridePendingTransition(0, 0);
                    }
                })
                .start();
    }

    @Override
    public void onSendClickListener(View v) {
        if (validateComment()) {
            String comment=etComment.getText().toString();
            //TODO feedPost.comment(context,comment);

            FirebaseFirestore db = FirebaseFirestore.getInstance();
            HashMap<String,Object> map = new HashMap<>();
            map.put(Const.Feed.AUTHOR,manager.getFirebaseUserId());
            map.put(Const.TIMESTAMP, FieldValue.serverTimestamp());
            map.put(Const.CONTENT, comment);
            db.collection(Const.FEED).document(id).collection(Const.Feed.COMMENTS).add(map).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
            commentsAdapter.addItem(comment);
            commentsAdapter.setAnimationsLocked(false);
            commentsAdapter.setDelayEnterAnimation(false);
           // rvComments.smoothScrollBy(0, rvComments.getChildAt(0).getHeight() * commentsAdapter.getItemCount());

            etComment.setText(null);
            btnSendComment.setCurrentState(SendCommentButton.STATE_DONE);
        }
    }

    private boolean validateComment() {
        if (TextUtils.isEmpty(etComment.getText())) {
            btnSendComment.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_error));
            return false;
        }

        return true;
    }
}
