package com.jabuwings.views.hooks;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;

import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.realm.HubMessage;
import com.jabuwings.database.realm.Message;
import com.jabuwings.management.Const;
import com.jabuwings.models.ImageSliderAdapter;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.CirclePageIndicator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;


public class ImageSlider extends JabuWingsActivity{

	private ImageSliderAdapter adapter;
	private ViewPager viewPager;
    CirclePageIndicator mIndicator;
	String where;
	String[] whereArgs;
	Cursor cursor;
	String path;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fullscreen_view);
		setUpToolbar(null);
		viewPager = (ViewPager) findViewById(R.id.pager);
		String a= getIntent().getStringExtra(Const.USERNAME);
		String hubId=getIntent().getStringExtra(Const.HUB_ID);
		path=getIntent().getStringExtra(Const.PATH);
		boolean _private=getIntent().getBooleanExtra(Const.PRIVATE,false);
        // ViewPager Indicator
        mIndicator = (CirclePageIndicator) findViewById(R.id.image_slider_indicator);


		if(!_private){
			RealmResults<HubMessage> realmResults = realm.where(HubMessage.class)
					.equalTo(Const.HUB_ID,hubId)
					.equalTo(Const.MSG_TYPE,DataProvider.MessageType.INCOMING_IMAGE.ordinal())
					.or()
					.equalTo(Const.MSG_TYPE,DataProvider.MessageType.OUTGOING_IMAGE.ordinal())
					.findAllAsync();


			OrderedRealmCollectionChangeListener<RealmResults<HubMessage>> callback = new OrderedRealmCollectionChangeListener<RealmResults<HubMessage>>() {
				@Override
				public void onChange(RealmResults<HubMessage> results, OrderedCollectionChangeSet changeSet) {
					if (changeSet == null) {
						List<String> messages= new ArrayList<>();
						for(HubMessage message: results)
						{
							String url = message.getFile().getLocalUrl();
                            String onlineUrl = message.getFile().getAddress();

                            if((!TextUtils.isEmpty(url) && new File(url).exists()) || onlineUrl!=null)
                                messages.add(url==null? onlineUrl : url);
						}
						setUpAdapter(messages);


					} else {
						// Called on every update.
						//adapter.notifyDataSetChanged();
					}
				}
			};
			realmResults.addChangeListener(callback);
		}
		else{
			RealmResults<Message> realmResults = realm.where(Message.class)
                    .equalTo(Const.MSG_TYPE,DataProvider.MessageType.INCOMING_IMAGE.ordinal())
                    .or()
                    .equalTo(Const.MSG_TYPE,DataProvider.MessageType.OUTGOING_IMAGE.ordinal())
					.beginGroup()
					.equalTo(Const.RECEIVER,a)
					.equalTo(Const.SENDER,manager.getUsername())
					.endGroup()
					.or()
					.beginGroup()
					.equalTo(Const.RECEIVER,manager.getUsername())
					.equalTo(Const.SENDER,a)
					.endGroup()
					.findAllAsync();

			OrderedRealmCollectionChangeListener<RealmResults<Message>> callback = new OrderedRealmCollectionChangeListener<RealmResults<Message>>() {
				@Override
				public void onChange(RealmResults<Message> results, OrderedCollectionChangeSet changeSet) {
					if (changeSet == null) {
						List<String> messages= new ArrayList<>();
						for(Message message: results)
						{
							if(message.getFile()!=null)
							{
								String url = message.getFile().getLocalUrl();
								String onlineUrl = message.getFile().getAddress();

								if((!TextUtils.isEmpty(url) && new File(url).exists()) || onlineUrl!=null)
									messages.add(url==null? onlineUrl : url);
							}

						}
						setUpAdapter(messages);


					} else {
						// Called on every update.
						//adapter.notifyDataSetChanged();
					}
				}
			};
			realmResults.addChangeListener(callback);

		}
	}
	private void setUpAdapter(List<String> messages)
	{
		adapter = new ImageSliderAdapter(ImageSlider.this,
				messages);

		viewPager.setAdapter(adapter);

		int position=0;
		for(int i=0; i<=messages.size()-1; i++)
		{
			if(messages.get(i).equals(path))
			{
				position=i;
				break;
			}
		}
		// displaying selected image first
		viewPager.setCurrentItem(position);

		mIndicator.setCentered(true);
		mIndicator.setFillColor(getResources().getColor(R.color.default_title_indicator_footer_color));
		mIndicator.setPageColor(getResources().getColor(R.color.default_page_indicator));
		mIndicator.setViewPager(viewPager);
	}
}
