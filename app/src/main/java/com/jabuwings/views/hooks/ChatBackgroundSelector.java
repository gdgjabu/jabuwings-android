package com.jabuwings.views.hooks;

import android.support.v4.view.ViewPager;
import android.os.Bundle;

import com.jabuwings.R;
import com.jabuwings.models.ChatBackgroundAdapter;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.CirclePageIndicator;

public class ChatBackgroundSelector extends JabuWingsActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_background_selector);
        setUpToolbar(null,"Select Chat Background");
        ViewPager pager = (ViewPager)findViewById(R.id.pager);
        pager.setAdapter(new ChatBackgroundAdapter(this));
        CirclePageIndicator mIndicator=(CirclePageIndicator)findViewById(R.id.chat_background_indicator);
        mIndicator.setCentered(true);
        mIndicator.setFillColor(getResources().getColor(R.color.default_title_indicator_footer_color));
        mIndicator.setPageColor(getResources().getColor(R.color.default_page_indicator));
        mIndicator.setViewPager(pager);



    }
}
