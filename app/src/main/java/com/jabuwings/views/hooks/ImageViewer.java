 package com.jabuwings.views.hooks;

 import android.content.Intent;
 import android.graphics.Bitmap;
 import android.graphics.Color;
 import android.net.Uri;
 import android.os.Bundle;
 import android.os.Environment;
 import android.support.annotation.NonNull;

 import android.support.v4.view.MenuItemCompat;
 import android.support.v7.widget.ShareActionProvider;
 import android.util.Log;
 import android.view.Menu;
 import android.view.MenuItem;
 import android.view.View;
 import android.widget.ImageView;

 import android.widget.Toast;

 import com.afollestad.materialdialogs.MaterialDialog;
 import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
 import com.afollestad.materialdialogs.simplelist.MaterialSimpleListItem;
 import com.jabuwings.R;
 import com.jabuwings.image.GalleryUploader;
 import com.jabuwings.image.ImageProcessor;
 import com.jabuwings.image.PictureUploader;
 import com.jabuwings.image.SiliCompressor;
 import com.jabuwings.management.Const;
 import com.jabuwings.management.Manager;
 import com.jabuwings.models.CircleTransform;
 import com.jabuwings.ultra.draw.Draw;
 import com.jabuwings.utilities.FileManager;
 import com.jabuwings.views.main.JabuWingsActivity;
 import com.jabuwings.widgets.ZoomableImageView;
 import com.parse.ParseUser;
 import com.soundcloud.android.crop.Crop;
 import com.squareup.picasso.Callback;
 import com.squareup.picasso.NetworkPolicy;
 import com.squareup.picasso.Picasso;

 import java.io.File;
 import java.io.IOException;
 import java.text.SimpleDateFormat;
 import java.util.Date;
 import java.util.Locale;

 public class ImageViewer extends JabuWingsActivity {
     private static final String LOG_TAG = ImageViewer.class.getSimpleName();
     Bitmap bitmap;
     ParseUser user;
     Manager manager;
     boolean isUserProfile;
     ZoomableImageView image;
     String profilePictureURL;
     String title;
     private ShareActionProvider mShareActionProvider;
     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_image_viewer);
         title = getIntent().getStringExtra(Const.VIEW_THIS_IMAGE);
         setUpToolbar(null, title);
         manager = new Manager(this);
         image = (ZoomableImageView) findViewById(R.id.image_viewer);
         isUserProfile = getIntent().getBooleanExtra(Const.IS_USER_PROFILE, false);



         user = ParseUser.getCurrentUser();
         profilePictureURL = getIntent().getStringExtra(Const.IMAGE_VIEWER_URI);
         //TODO checks if the image is from the private storage or the public external storage
         boolean isPrivate=getIntent().getBooleanExtra(Const.PRIVATE,true);
         boolean isOnline=getIntent().getBooleanExtra(Const.ONLINE,false);

         if(!isPrivate)
         {
             try {
                 if(isOnline) Picasso.with(this).load(profilePictureURL).placeholder(R.drawable.default_avatar).networkPolicy(NetworkPolicy.OFFLINE).into(image, new Callback() {
                     @Override
                     public void onSuccess() {

                     }

                     @Override
                     public void onError() {
                         Picasso.with(context)
                                 .load(profilePictureURL)
                                 .into(image);
                     }
                 });
                 else
                 image.setImageBitmap(SiliCompressor.with(this).getCompressBitmap(Uri.fromFile(new File(profilePictureURL)).toString()));
             }
             catch (IOException e)
             {
                e.printStackTrace();
             }
//             new ImageProcessor(this,false).loadBitmap(profilePictureURL, image);
         }
         else
         if(profilePictureURL!=null)
                 new ImageProcessor(this).loadBitmap(profilePictureURL, image);


         else {
             image.setImageDrawable(getResources().getDrawable(R.drawable.default_avatar));
         }
         image.setMaxZoom(6f);
     }

     @Override
     protected void onResume() {
         super.onResume();
//         if(profilePictureURL!=null)
//       new  ImageProcessor(this).loadBitmap(profilePictureURL, image);

     }

     @Override
     public boolean onCreateOptionsMenu(Menu menu) {
         if(!isUserProfile)
         {getMenuInflater().inflate(R.menu.menu_image_viewer, menu);  // Locate MenuItem with ShareActionProvider
             MenuItem item = menu.findItem(R.id.menu_item_share);

             // Fetch and store ShareActionProvider
             mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);}
         return super.onCreateOptionsMenu(menu);
     }

     // Call to update the share intent
     private void setShareIntent(Intent shareIntent) {
         if (mShareActionProvider != null) {
             mShareActionProvider.setShareIntent(shareIntent);
         }
     }
     @Override
     public boolean onOptionsItemSelected(MenuItem item) {
         switch (item.getItemId()) {
             case R.id.action_save_to_gallery:
                saveToGallery();
                 break;
         }
         return super.onOptionsItemSelected(item);
     }

     private void draw()
     {
         //startActivity(new Intent(this, Draw.class));
     }

     private void saveToGallery()
     {
       if(profilePictureURL!=null)
       {
           Bitmap bitmap= new ImageProcessor(this).getBitmapFromMemCache(profilePictureURL);
           if(bitmap!=null)
           new GalleryUploader(this,title,bitmap).execute();
           else{
               //TODO change to xml error toast
               Toast.makeText(this, "Unable to save to gallery due to a missing picture", Toast.LENGTH_SHORT).show();
           }
       }
     }


     @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK && data!=null)
        switch (requestCode)
        {
            case Const.PICK_PROFILE_IMAGE_ID:
                startCropActivity(data.getData());
                break;
            case Crop.REQUEST_CROP:
                final Uri resultUri = Crop.getOutput(data);

                try{
                    MaterialDialog dialog =       new MaterialDialog.Builder(ImageViewer.this)
                            .title(getString(R.string.please_wait))
                            .content("Uploading your picture")
                            .cancelable(false)
                            .progress(true, 0)
                            .show();


                    PictureUploader pictureUploader = new PictureUploader(ImageViewer.this,dialog,image, Const.MEDIA_UPLOAD_TYPE.PROFILE_PICTURE.ordinal());
                    pictureUploader.execute(resultUri);}
                catch (Exception e)
                {Toast.makeText(getApplicationContext(), "error: "+e.toString(), Toast.LENGTH_LONG).show();}
                break;
            default:
                manager.log(LOG_TAG,"Default"+requestCode);
                Toast.makeText(getApplicationContext(),"No image obtained", Toast.LENGTH_SHORT).show();

        }
        else{
            manager.log(LOG_TAG,"Check failed ");
            Toast.makeText(getApplicationContext(),"No image obtained", Toast.LENGTH_SHORT).show();
        }

    }

     private void startCropActivity(@NonNull Uri uri) {
         final File pictureFile = new FileManager(context).getProfilePictureFile();
         Uri mDestinationUri =Uri.fromFile(pictureFile);
         Crop.of(uri, mDestinationUri).asSquare().start(this);
     }

    public void pickImage() {

        Intent intent= new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Const.PICK_PROFILE_IMAGE_ID);

    }


    public static File getOutputMediaFile(){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        String mediaCardState=   Environment.getExternalStorageState();
        if(mediaCardState.equals("MEDIA_UNMOUNTED")|| mediaCardState.equals("MEDIA_REMOVED"))
        {}

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), Const.APP_NAME);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("jabuwings", "failed to create directory");

                return null;
            }
        }

        // Create a media file name
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "Profile Picture"+ ".jpg");

        return mediaFile;
    }


    public static File getOutputMediaFile(String username){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        String mediaCardState=   Environment.getExternalStorageState();
        if(mediaCardState.equals("MEDIA_UNMOUNTED")|| mediaCardState.equals("MEDIA_REMOVED"))
        {}

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), Const.APP_NAME);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("JabuWings", "failed to create directory");

                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                username+"_"+timeStamp+ ".jpg");

        return mediaFile;
    }





}
