package com.jabuwings.views.hooks;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.jabuwings.R;

public class Binoculars extends AppCompatActivity {
TextView tvQuery;
private static String LOG_TAG=Binoculars.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG,"Binoculars called");
        setContentView(R.layout.activity_binoculars);
        tvQuery=(TextView)findViewById(R.id.tv_binoculars);
        handleIntent(getIntent());

    }



    private void handleIntent(Intent intent) {
        Log.d(LOG_TAG,"Handle intent called");
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            Log.d(LOG_TAG,"Search called");
            String query = intent
                    .getStringExtra(SearchManager.QUERY);
            tvQuery.setText(query);

            //use the query to search your data somehow
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }


}
