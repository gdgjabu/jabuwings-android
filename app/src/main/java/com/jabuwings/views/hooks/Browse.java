package com.jabuwings.views.hooks;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.jabuwings.R;
import com.jabuwings.download.DownloadService;
import com.jabuwings.download.ResultManager;
import com.jabuwings.management.Const;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.Web;
import com.parse.ParseConfig;
import com.parse.ParseFile;
import com.rey.material.widget.ProgressView;


@Deprecated
public class Browse extends JabuWingsActivity implements Web.Listener {
    Web web;
    String choice;
    ProgressView progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse);
        toolbar=(Toolbar)findViewById(R.id.toolbar);

        web = (Web)findViewById(R.id.browse);
        web.setListener(this,this);
        progress = (ProgressView)findViewById(R.id.pv_browse);
        progress.start();

        choice = getIntent().getStringExtra(Const.ACAD_CHOICE);
        ParseConfig config= ParseConfig.getCurrentConfig();


        if(choice!=null)
        switch (choice)
        {
            case "portal":
                toolbar.setTitle("JABU Portal");
                String portalUrl=config.getString("jabu_portal");
                if(portalUrl==null)
                web.loadUrl("http://portal.jabu.edu.ng");
                else{
                    web.loadUrl(portalUrl);
                }
                break;
            case "web":
                toolbar.setTitle("JABU Web");
                web.loadUrl("http://jabu.edu.ng");
                break;
            case "studentReg":
                toolbar.setTitle("Student Registration");
                web.loadUrl("http://jabu-portal.com.ng");
                break;
            case "officialWebsite":
                toolbar.setTitle("Jabu.edu.ng");
                web.loadUrl("http://www.jabu.edu.ng");
                break;



        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        progress.setVisibility(ProgressView.VISIBLE);
        progress.start();
    }

    @Override
    public void onPageFinished(String url) {
        progress.stop();
        progress.setVisibility(ProgressView.GONE);

    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        getSupportActionBar().setTitle(failingUrl);
        web.loadUrl("file:///android_asset/browser/page_error.html");
        progress.stop();
    }

    @Override
    public void onDownloadRequested(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
        Log.d("Browse", "Url: " + url + " userAgent: " + userAgent +
                " contentDisposition: " + contentDisposition + " mimetype: " + mimetype + "contentLength: " + contentLength);
        Intent intent = new Intent(this, DownloadService.class);
        intent.putExtra("url", url);
        intent.putExtra("filename","document.pdf");
        intent.putExtra("receiver", new ResultManager(new Handler(),getApplicationContext()));
        startService(intent);
    }

    @Override
    public void onExternalPageRequest(String url) {

    }

    public void setValue(float progress){
       // this.progress.setProgress(progress);
    }

}
