package com.jabuwings.views.hooks;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.views.main.Domicile;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.timetable.TimeTable2;


import butterknife.ButterKnife;
import butterknife.InjectView;

public class LectureDetails extends JabuWingsActivity implements View.OnClickListener {

Button btCheck;
@InjectView(R.id.ald_course_code)
TextView tvCourseCode;
@InjectView(R.id.ald_course_venue)
TextView tvVenue;
@InjectView(R.id.ald_course_time_start)
TextView tvTimeStart;
@InjectView(R.id.ald_course_time_stop)
TextView tvTimeStop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lecture_details);
        setUpToolbar(null,"Lecture Details");
        ButterKnife.inject(this);
        String courseTag= getIntent().getStringExtra(Const.TAG);
        String[] courseInfo=courseTag.split("_");
        tvCourseCode.setText(courseInfo[0]);
        tvTimeStart.setText(courseInfo[1]);
        tvTimeStop.setText(courseInfo[2]);
        tvVenue.setText(courseInfo[3]);
        //tvContent.setText(getIntent().getStringExtra("msg"));
        btCheck= (Button)findViewById(R.id.bt_lecture_check);
        btCheck.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.bt_lecture_check:
                startActivity(new Intent(LectureDetails.this, TimeTable2.class));
                //startService(new Intent(LectureDetails.this,LectureService.class).putExtra("msg","No more lectures today"));
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
        {
            finish();
            startActivity(new Intent(this,Domicile.class));
        }
        return true;
    }
}
