package com.jabuwings.views.hooks;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.main.JabuWingsActivity;

import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class NotificationDetails extends JabuWingsActivity {
    @InjectView(R.id.tv_notification_details)
    TextView tvDetails;
    @InjectView(R.id.tv_notifications_date)
    TextView tvDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_details);
        setUpToolbar(null,R.string.notifications);
        ButterKnife.inject(this);
        setUpTextViews(tvDate,tvDetails);
        String content=getIntent().getStringExtra(Const.CONTENT);
        long d=getIntent().getLongExtra(Const.TIME,0);
        Date date = new Date(d);
        tvDate.setText(new Time().parseTime(date));
        tvDetails.setText(Html.fromHtml(content));

    }
}
