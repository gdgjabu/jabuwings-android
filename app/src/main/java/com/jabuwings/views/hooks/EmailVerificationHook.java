package com.jabuwings.views.hooks;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.main.Domicile;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.registration.Verification;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.HashMap;

public class EmailVerificationHook extends JabuWingsActivity {
TextView tvEmailSubmitted;
Button btProceed,btChangeEmail;
ParseUser parseUser;
Manager manager;

    public void verifyNumber(View v)
    {
        startActivity(new Intent(this, Verification.class).putExtra("request",true));
        finish();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_verification_hook);
        setUpToolbar(null, "Email Verification");
        parseUser=ParseUser.getCurrentUser();
        parseUser.fetchIfNeededInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser user, ParseException e) {
                if(e==null)
                {
                    user.pinInBackground();
                    tvEmailSubmitted.setText(user.getEmail());
                }

            }
        });
        tvEmailSubmitted=(TextView)findViewById(R.id.evh_email_submitted);
        btProceed=(Button)findViewById(R.id.evh_proceed);
        btChangeEmail=(Button)findViewById(R.id.evh_change_email);
        manager=new Manager(this);

        btChangeEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(EmailVerificationHook.this)
                        .title(R.string.change_email)
                        .content("Please enter your new email. Please note that you must have access to this email to complete the process")
                        .inputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
                        .input("email", null, false, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                    final String newEmail= input.toString();
                                if(!newEmail.contains("@")|| !newEmail.contains(".") || newEmail.length()<5)
                                {
                                    manager.shortToast("Invalid email");
                                }

                                else{
                                    dialog.dismiss();
                                    final SweetAlertDialog materialDialog= new DialogWizard(EmailVerificationHook.this).showSimpleProgress("Please wait","We are changing your email");


                                    HashMap<String,Object> params= manager.getDefaultCloudParams();
                                    params.put(Const.EMAIL,newEmail);
                                    ParseCloud.callFunctionInBackground("userUpdateEmail", manager.getDefaultCloudParams(), new FunctionCallback<String>() {
                                        @Override
                                        public void done(String s, ParseException e) {
                                            materialDialog.dismiss();
                                            if(e==null)
                                            {
                                                new DialogWizard(EmailVerificationHook.this).showSimpleDialog("Email changed","Please check your email \n"+newEmail+"\n"+"for instructions to verify this account");
                                                tvEmailSubmitted.setText(newEmail);

                                            }
                                            else{
                                                new DialogWizard(EmailVerificationHook.this).showErrorDialog("An error occurred",e.getMessage());
                                            }
                                        }
                                    });

                                }
                            }
                        }).show();
            }
        });

        btProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SweetAlertDialog dialog= new DialogWizard(EmailVerificationHook.this).showSimpleProgress(R.string.please_wait,R.string.making_sure);
                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.whereEqualTo(Const.USERNAME,parseUser.getUsername()).getFirstInBackground(new GetCallback<ParseUser>() {
                    @Override
                    public void done(ParseUser parseUser, ParseException e) {
                        dialog.dismiss();
                        if(e==null)
                        if(parseUser.getBoolean(Const.EMAIL_VERIFIED))
                        {
                            parseUser.pinInBackground();
                            Toast.makeText(EmailVerificationHook.this, "Welcome aboard", Toast.LENGTH_SHORT).show();
                            manager.createLoginSession(parseUser.getUsername());
                            manager.setUpPush();
                            manager.enableBootService();
                            startActivity(new Intent(EmailVerificationHook.this,Domicile.class).putExtra(Const.TUTORIAL,true));
                            finish();

                        }

                        else{
                            Toast.makeText(EmailVerificationHook.this, "The email has not been verified.", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            ErrorHandler.handleError(getApplicationContext(),e);
                        }
                    }
                });


            }
        });
        tvEmailSubmitted.setText(parseUser.getEmail());



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_with_logout_option, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_logout:
                new DialogWizard(EmailVerificationHook.this).showSimpleProgress("Please wait","Logging out");
                new Manager(EmailVerificationHook.this).outrightLogOut();
                break;


        }
        return super.onOptionsItemSelected(item);
    }
}
