package com.jabuwings.views.hooks;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jabuwings.R;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.CircleImage;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class ContactsList extends JabuWingsActivity {

    @InjectView(R.id.lvContacts)
    ListView listView;
    @InjectView(R.id.btContact)
    Button submit;
    List<Contact> contacts= new ArrayList<>();
    List<String> numbers=new ArrayList<>();

    ContactsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_list);
        manager.errorToast("Not completed");
        ButterKnife.inject(this);
        setUpToolbar(null,"Contacts");
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        getContacts();
    }

    private void getContacts(){
        final int ID = 0;
        final  int LOOKUP_KEY = 1;
        final  int DISPLAY_NAME = 2;
        final  int PHOTO_THUMBNAIL_DATA = 3;
        final  int SORT_KEY = 4;
        Cursor cur = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,null, null);

        if(cur!=null)
        {
            while(cur.moveToNext())
            {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                final Uri contactUri = ContactsContract.Contacts.getLookupUri(
                        cur.getLong(ID),
                        cur.getString(LOOKUP_KEY));

                Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);

                if(phones!=null) {
                    while (phones.moveToNext()) {
                        String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        addContact(new Contact(name, number, contactUri));

                    }
                    phones.close();
                }
            }
            cur.close();
        }



        manager.log("Contacts size "+contacts.size());









        adapter=new ContactsAdapter(context,contacts);
        listView.setAdapter(adapter);
    }

    private void addContact(Contact contact)
    {
        contacts.add(contact);
       /* if(!numbers.contains(contact.no)) {
            contacts.add(contact);
            numbers.add(contact.no);
        }*/
    }


    @OnClick(R.id.btContact)
    public void onSubmit()
    {
        SparseBooleanArray checked = listView.getCheckedItemPositions();
        ArrayList<Contact> selectedItems = new ArrayList<Contact>();
        for (int i = 0; i < checked.size(); i++) {
            // Item position in adapter
            int position = checked.keyAt(i);
            // Add sport if it is checked i.e.) == TRUE!
            if (checked.valueAt(i))
                selectedItems.add((Contact) adapter.getItem(position));
        }

       /* String[] outputStrArr = new String[selectedItems.size()];

        for (int i = 0; i < selectedItems.size(); i++) {
            outputStrArr[i] = selectedItems.get(i);
        }

        Intent intent = new Intent(getApplicationContext(),
                ResultActivity.class);*/

        // Create a bundle object
       /* Bundle b = new Bundle();
        b.putStringArray("selectedItems", outputStrArr);

        // Add the bundle to the intent.
        intent.putExtras(b);

        // start the ResultActivity
        startActivity(intent);*/
    }
    public class ContactsAdapter extends BaseAdapter {

        LayoutInflater inflater;
        Context context;
        List<Contact> contacts;
        public ContactsAdapter(Context context, List<Contact>list)

        {
            this.context=context;
            inflater = LayoutInflater.from(context);
            contacts=list;

        }
        @Override
        public int getCount() {
            return contacts.size();
        }

        @Override
        public Object getItem(int position) {
            return contacts.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.contact_item, null);
                holder = new ViewHolder();
                holder.tvName = (TextView) convertView.findViewById(R.id.contactName);
                holder.tvNumber = (TextView) convertView.findViewById(R.id.contactNo);
                holder.image = (CircleImage) convertView.findViewById(R.id.contactImage);

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            Contact contact = contacts.get(position);
            if (contact != null) {
                holder.tvName.setText(contact.name);
                holder.tvNumber.setText(contact.no);
                if(contact.uri!=null)
                holder.image.setImageURI(contact.uri);
            }







            return  convertView;
        }
    }

    public class  ViewHolder {
        TextView tvName,tvNumber;
        CircleImage image;

    }
    public class Contact
    {
        String name, no;
        Uri uri;
        public Contact(String name, String no, Uri uri)
        {
            this.name=name;
            this.no=no;
            this.uri=uri;


        }
    }
}
