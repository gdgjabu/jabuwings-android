package com.jabuwings.views.hooks;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.image.PictureUploader;
import com.jabuwings.intelligence.lisa.Lisa;
import com.jabuwings.management.Const;
import com.jabuwings.management.CreditManager;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.Utils;
import com.jabuwings.views.main.JabuWingsActivity;
import com.parse.ConfigCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseConfig;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.lang.reflect.Array;
import java.util.HashMap;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;


public class FeedMonger extends JabuWingsActivity {
EditText etTitle;
EditText etFeedURL;
EmojiconEditText etContent;
EmojIconActions emojIcon;
FloatingActionButton btPost;
ImageView ivFeed;
Button btAddImage;
Uri _uri;
CreditManager creditManager;
public static String title, content, feedUrl;
public static int feedPriceTag;
DialogWizard dialogWizard;
MaterialDialog dialog;
static FeedMonger instance;
boolean storeFeed=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_monger);
        setUpEmoji();
        setUpToolbar(null);
        instance=this;
        creditManager = new CreditManager(getApplicationContext());
        dialogWizard=new DialogWizard(FeedMonger.this);
        etTitle=(EditText)findViewById(R.id.et_feed_title);
        etContent=(EmojiconEditText)findViewById(R.id.etFeedContent);
        ivFeed=(ImageView)findViewById(R.id.feed_image);
        etFeedURL=(EditText)findViewById(R.id.et_feed_url);
        btAddImage=(Button)findViewById(R.id.btFeed_add_image);
        btPost=(FloatingActionButton)findViewById(R.id.bt_feed_post);



        restoreFeed();
        btPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValues();
            }
        });

        btAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImage();
            }
        });
    }

    private void restoreFeed()
    {
       String feedData=manager.getFeedDraft();

        if(!TextUtils.isEmpty(feedData))
        {
            String [] feedDetails= feedData.split(Utils.strSeparator);
            if(feedDetails.length==3)
            {
                etTitle.setText(feedDetails[0]);
                etContent.setText(feedDetails[1]);
                etFeedURL.setText(feedDetails[2]);
            }

            else if(feedDetails.length==2)
            {
                etTitle.setText(feedDetails[0]);
                etContent.setText(feedDetails[1]);
            }
        }


    }
    public static FeedMonger getInstance()
    {
        return instance;
    }

    public void checkFunds()
    {

        //dialog= dialogWizard.showSimpleProgress("Just hold on","Posting your feed");
        postFeed();
        creditManager.shortToast("Your article will be posted shortly");
        storeFeed=false;
        finish();
     /*   ParseConfig.getInBackground(new ConfigCallback() {
            @Override
            public void done(ParseConfig config, ParseException e) {
                if (e == null) {
                    feedPriceTag = config.getInt("feedPriceTag", 20);

                } else {
                    config = ParseConfig.getCurrentConfig();
                    feedPriceTag = config.getInt("feedPriceTag", 20);
                }

                ParseUser.getQuery().whereEqualTo(Const.USERNAME,creditManager.getUsername())
                        .getFirstInBackground(new GetCallback<ParseUser>() {
                            @Override
                            public void done(ParseUser parseUser, ParseException e) {
                                if(e==null) {
                                    int balance = parseUser.getInt(Const.CHRIMATA);
                                    if (feedPriceTag > balance) {
                                        dialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "You have insufficient funds", Toast.LENGTH_SHORT).show();
                                    } else {
                                        parseUser.put(Const.CHRIMATA, balance - feedPriceTag);
                                        parseUser.pinInBackground();
                                        parseUser.saveInBackground(new SaveCallback() {
                                            @Override
                                            public void done(ParseException e) {

                                            }
                                        });
                                    }
                                }

                                else{
                                    dialog.dismiss();
                                    ErrorHandler.handleError(FeedMonger.this,e);
                                }
                            }
                        });

            }
        });

*/






    }

    public void checkValues()

    {
        title=etTitle.getText().toString();
        content=etContent.getText().toString();
        feedUrl=etFeedURL.getText().toString();

        if(TextUtils.isEmpty(title)|| TextUtils.isEmpty(content))

        {
            new MaterialDialog.Builder(FeedMonger.this)
                    .content("You must have a title and a content")
                    .show();

        }

        else if(!title.matches("[a-zA-Z0-9_ ]*"))
        {
            dialogWizard.showSimpleDialog("","You entered an invalid title");
        }

        else{
            checkFunds();
        }
    }


    public void postFeed()

    {

            if(_uri!=null) {
                new PictureUploader(this, dialog, ivFeed, Const.MEDIA_UPLOAD_TYPE.FEED_PICTURE.ordinal()).execute(_uri);
            }
            else {
                HashMap<String, Object> params = new HashMap<>();
                params.put(Const.Feed.AUTHOR, creditManager.getUsername());
                params.put(Const.Feed.TITLE, FeedMonger.title);
                params.put(Const.Feed.CONTENT, FeedMonger.content);
                params.put(Const.Feed.FEED_ID, Lisa.generateFeedId(creditManager.getUsername()));
                params.put(Const.Feed.EXTERNAL_URL, FeedMonger.feedUrl);
                params.put(Const.VERSION,getString(R.string.version_number));

                ParseCloud.callFunctionInBackground(Const.FEED_MONGER_CLASS, params, new FunctionCallback<String>() {
                    @Override
                    public void done(String s, ParseException e) {
                        if (e == null) {
                            if (s.equals(Const.POSITIVE)) {
                                creditManager.shortToast(R.string.success_feed);
                                finish();
                            } else {
                                creditManager.shortToast(s);
                            }

                        } else {
                            manager.storeFeedDraft(title,content,feedUrl);
                            ErrorHandler.handleError(context,e);
                        }


                    }
                });


            }
    }


    @Override
    protected void onDestroy() {
        String title,content,url;title=etTitle.getText().toString();content=etContent.getText().toString();url=etFeedURL.getText().toString();
        if(storeFeed)
        manager.storeFeedDraft(title,content,url);
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==Const.PICK_FEED_IMAGE&&data!=null)
        try {
            _uri = data.getData();
            ivFeed.setImageURI(_uri);

        }

        catch (Exception e)
        {
            creditManager.longToast("Please proceed with posting the feed, your image will be attached");
        }

        else{
            creditManager.shortToast("No image Obtained");
        }

    }

    public void addImage()
    {
        Intent intent= new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Feed Picture"), Const.PICK_FEED_IMAGE);
    }

    public void setUpEmoji(){


        final View rootView = findViewById(R.id.cor_feed_monger);
        final ImageView emojiButton = (ImageView) findViewById(R.id.emoji_btn);

        emojIcon=new EmojIconActions(this,rootView,etContent,emojiButton);
        emojIcon.ShowEmojIcon();
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                Log.e("Keyboard","open");
                changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_keyboard);
            }

            @Override
            public void onKeyboardClose() {
                Log.e("Keyboard","close");
                changeEmojiKeyboardIcon(emojiButton, R.drawable.smiley);
            }
        });

        /*final View rootView = findViewById(R.id.cor_feed_monger);
        final ImageView emojiButton = (ImageView) findViewById(R.id.feed_smiley);


        // Give the topmost view of your activity layout hierarchy. This will be used to measure soft keyboard height
        final EmojiconsPopup popup = new EmojiconsPopup(rootView, this);

        //Will automatically set size according to the soft keyboard size
        popup.setSizeForSoftKeyboard();

        //If the emoji popup is dismissed, change emojiButton to smiley icon
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                changeEmojiKeyboardIcon(emojiButton, R.drawable.smiley);
            }
        });

        //If the text keyboard closes, also dismiss the emoji popup
        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {

            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if(popup.isShowing())
                    popup.dismiss();
            }
        });

        //On emoji clicked, add it to edittext
        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                if (etContent == null || emojicon == null) {
                    return;
                }

                int start = etContent.getSelectionStart();
                int end = etContent.getSelectionEnd();
                if (start < 0) {
                    etContent.append(emojicon.getEmoji());
                } else {
                    etContent.getText().replace(Math.min(start, end),
                            Math.max(start, end), emojicon.getEmoji(), 0,
                            emojicon.getEmoji().length());
                }
            }
        });

        //On backspace clicked, emulate the KEYCODE_DEL key event
        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                etContent.dispatchKeyEvent(event);
            }
        });

        // To toggle between text keyboard and emoji keyboard keyboard(Popup)
        emojiButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //If popup is not showing => emoji keyboard is not visible, we need to show it
                if(!popup.isShowing()){

                    //If keyboard is visible, simply show the emoji popup
                    if(popup.isKeyBoardOpen()){
                        popup.showAtBottom();
                        changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_keyboard);
                    }

                    //else, open the text keyboard first and immediately after that show the emoji popup
                    else{
                        etContent.setFocusableInTouchMode(true);
                        etContent.requestFocus();
                        popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(etContent, InputMethodManager.SHOW_IMPLICIT);
                        changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_keyboard);
                    }
                }

                //If popup is showing, simply dismiss it to show the undelying text keyboard
                else{
                    popup.dismiss();
                }
            }
        });*/

    }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId){
        iconToBeChanged.setImageResource(drawableResourceId);
    }

}
