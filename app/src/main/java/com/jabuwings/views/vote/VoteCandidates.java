package com.jabuwings.views.vote;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Falade James on 2/9/2017 All Rights Reserved.
 */

 class VoteCandidates implements Serializable {
     List<VoteCandidate> candidates;
    VoteCandidates(List<VoteCandidate> c)
    {
        this.candidates=c;
    }
}
