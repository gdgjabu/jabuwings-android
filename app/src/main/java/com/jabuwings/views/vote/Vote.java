package com.jabuwings.views.vote;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.main.JabuWingsActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class Vote extends JabuWingsActivity {

    @InjectView(R.id.rvVoteChannels)
    RecyclerView recyclerView;
    static VoteChannels channels;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vote);
        ButterKnife.inject(this);
        boolean linked= getIntent().getBooleanExtra("linked",false);

        if(!linked)
        {
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(new ChannelsAdapter(channels));
            //channels=null;
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    public class ChannelsAdapter extends RecyclerView.Adapter<ChannelsAdapter.ViewHolder>
    {

        VoteChannels channels;
         ChannelsAdapter(VoteChannels channels)
        {
            this.channels=channels;
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.vote_channel_item, parent, false);




            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            try {
                JSONObject jsonObject = channels.channels.getJSONObject(position);
                VoteChannel channel=new VoteChannel(jsonObject);

                holder.view.setTag(channel);

                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(v.getId()==R.id.vote_channel_container)
                        {
                            VoteChannel channel=(VoteChannel)v.getTag();
                            try {
                                VoteForChannel.categories=new VoteCategories(channel.getCategories());
                                startActivity(new Intent(Vote.this, VoteForChannel.class).putExtra(Const.NAME, channel.name).putExtra(Const.ID,channel.id));
                            }
                            catch (JSONException e)
                            {
                                manager.errorToast("Oops, There was an error");
                                e.printStackTrace();
                            }
                        }
                        else{
                            manager.log("clicked "+v.getId());
                        }

                    }
                });
                holder.title.setText(channel.name);
                holder.desc.setText(channel.desc);
                Time time=new Time();
                holder.time.setText("From "+time.parseTime(new Date(channel.start))+"\nTo "+time.parseTime(new Date(channel.end)));

                String d="Categories\n";
                for(VoteCategory category : channel.getCategories())
                {
                    d+=category.getName()+"\n";
                }

                holder.categories.setText(d);





            }
            catch (JSONException e)
            {
                manager.errorToast("Oops an error "+e.getMessage());
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return channels.channels.length();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            @InjectView(R.id.vote_channel_title)
            TextView title;
            @InjectView(R.id.vote_channel_desc)
            TextView desc;
            @InjectView(R.id.vote_channel_categories)
            TextView categories;
            @InjectView(R.id.vote_channel_time)
            TextView time;
            @InjectView(R.id.vote_channel_container)
            LinearLayout view;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.inject(this,itemView);

            }
        }
    }

}
