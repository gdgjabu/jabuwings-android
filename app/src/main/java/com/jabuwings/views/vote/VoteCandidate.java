package com.jabuwings.views.vote;

import com.jabuwings.management.Const;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Falade James on 2/11/2017 All Rights Reserved.
 */

public class VoteCandidate {
    JSONObject jsonObject;
     VoteCandidate(JSONObject object)
    {
        this.jsonObject=object;
    }

    public String getId() throws JSONException
    {
     return jsonObject.getString(Const.ID);
    }
    public String getName() throws JSONException{
        return jsonObject.getString(Const.NAME);
    }
    public String getMeta() throws JSONException{
        return jsonObject.getString(Const.META);
    }
     String getProfilePicture() throws JSONException{
        return jsonObject.getString(Const.PROFILE_PICTURE);
    }
}
