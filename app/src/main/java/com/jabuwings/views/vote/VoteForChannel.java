package com.jabuwings.views.vote;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.Const;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.stepstone.stepper.StepperLayout;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class VoteForChannel extends JabuWingsActivity implements VoteForCategory.OnFragmentInteractionListener {

    static     VoteCategories categories;
    @InjectView(R.id.vote_stepper)
    StepperLayout stepperLayout;
    String id;
    List<VotedCandidate> votedCandidates=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vote_for_channel);
        ButterKnife.inject(this);
        String name=getIntent().getStringExtra(Const.NAME);
        setUpToolbar(null,name);
        id=getIntent().getStringExtra(Const.ID);
        stepperLayout.setAdapter(new VoteStepAdapter(getSupportFragmentManager(),this,name,categories.cats));
        categories=null;
    }


    @Override
    public void removeCandidate(VoteCategory category, VoteCandidate candidate) {
        //remove(candidate);
       // votedCandidates.remove(new VotedCandidate(candidate,category,false));
    }


    private void remove(VoteCandidate voteCandidate)
    {
        for(VotedCandidate v: votedCandidates)
        {
            try {
                if (v.candidate.getId().equals(voteCandidate.getId())) {
                    votedCandidates.remove(v);
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void remitCandidate(VoteCategory category, VoteCandidate candidate) {
        addCandidate(new VotedCandidate(candidate,category,false));
    }

    private void addCandidate(VotedCandidate votedCandidate)
    {


        /*try {
            manager.shortToast("to add votedCandidate " + votedCandidate.candidate.getId() + " category " + votedCandidate.category.getId());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        if(!votedCandidates.isEmpty())
        {
            VoteCategory voteCategory=votedCandidate.category;

            boolean exists=false; VotedCandidate toRemove=null;
            for(int i=0; i<votedCandidates.size(); i++)
            {
                VotedCandidate v=votedCandidates.get(i);

                try {
                    if (v.category.getId().equals(voteCategory.getId())) {
                        exists=true;
                        //The category has been voted for before


                        //Remove previous vote
                        toRemove=v;
                        //Add new vote
                        votedCandidates.add(votedCandidate);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
            if(!exists)
            {
                votedCandidates.add(votedCandidate);
            }
            else{
                votedCandidates.remove(toRemove);
            }

        }
        else*/

        votedCandidates.add(votedCandidate);

    }

    @Override
    public void voteCompleted(boolean done) {
        if(done)
        {
            if(votedCandidates.isEmpty())
            {
                manager.errorToast("You have to vote for at least one category");
            }
            else{

                try{
                    String a="";
                    String s="";
                    String cats="";

                    manager.log("size "+votedCandidates.size());
                    for(int i=0; i<votedCandidates.size(); i++)
                    {
                        VotedCandidate candidate=votedCandidates.get(i);
                        if(!cats.contains(candidate.category.getId()))
                        {
                            a+=candidate.category.getName()+": "+candidate.candidate.getName()+"\n\n";

                            manager.log("Position "+i);

                            if(i==votedCandidates.size()-1) //last item
                                s+=candidate.candidate.getId();
                            else{
                                s+=candidate.candidate.getId()+"$";
                            }
                            cats+=candidate.category.getId();
                        }



                    }

                    final String j=s;

                    a+="\n\nPlease note that this action is irreversible";

                    new MaterialDialog.Builder(VoteForChannel.this).title("Please Confirm your vote").content(a).positiveText(R.string.confirm).negativeText(R.string.check_again).onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull final MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                            HashMap<String,Object> params=manager.getDefaultCloudParams();
                            params.put(Const.OBJECT,j);
                            params.put(Const.CHANNEL_ID,id);
                            final SweetAlertDialog sweetAlertDialog=dialogWizard.showProgress(getString(R.string.please_wait),"Casting your vote",false);
                            ParseCloud.callFunctionInBackground("vote_for_multiple", params, new FunctionCallback<String>() {
                                @Override
                                public void done(String s, ParseException e) {
                                    sweetAlertDialog.dismiss();
                                    if(e==null)
                                    {
                                        SweetAlertDialog dialog=new SweetAlertDialog(VoteForChannel.this,SweetAlertDialog.SUCCESS_TYPE).setTitleText(getString(R.string.success)).setContentText(s).setConfirmText(getString(R.string.okay)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    sweetAlertDialog.cancel();
                                                    finish();
                                            }
                                        });
                                        dialog.setCancelable(false);
                                        dialog.show();
                                    }
                                    else{
                                        if(e.getCode()==ParseException.CONNECTION_FAILED)
                                        {
                                            dialogWizard.showErrorDialog("An error occurred",getString(R.string.error_network));
                                        }
                                        else if(e.getCode()==ParseException.SCRIPT_ERROR)
                                        {
                                            dialogWizard.showErrorDialog("An error occurred",e.getMessage());
                                        }
                                        else{
                                            ErrorHandler.handleError(context,e);
                                        }


                                    }
                                }
                            });
                        }
                    }).onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    }).show();

                }
                catch (JSONException e)
                {
                    manager.errorToast("Unable to process votes");
                    e.printStackTrace();
                }

            }

        }

    }

    class VotedCandidate
    {
        VoteCategory category;
        VoteCandidate candidate;
        boolean fin=false;
         VotedCandidate(VoteCandidate candidate, VoteCategory category,boolean fin )
        {
            this.fin=fin;
            this.candidate=candidate;
            this.category=category;
        }
    }
}
