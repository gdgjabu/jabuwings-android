package com.jabuwings.views.vote;

import com.jabuwings.management.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Falade James on 2/9/2017 All Rights Reserved.
 */

public class VoteChannel implements Serializable {
    public String id,name,desc;
    public boolean verified;
    public long start,end;
    JSONObject jsonObject;
    public VoteChannel(JSONObject jsonObject) throws JSONException
    {
            this.jsonObject=jsonObject;
            this.id = jsonObject.getString(Const.ID);
            this.name = jsonObject.getString(Const.NAME);
            this.desc = jsonObject.getString(Const.DESC);
            this.verified = jsonObject.getBoolean(Const.IS_VERIFIED);
            this.start = jsonObject.getLong(Const.START);
            this.end = jsonObject.getLong(Const.STOP);



    }

    public List<VoteCategory> getCategories() throws JSONException
    {
        JSONArray jsonArray=jsonObject.getJSONArray(Const.CATEGORIES);

        List<VoteCategory> categories=new ArrayList<>();
        for(int i=0; i<jsonArray.length(); i++)
        {
            VoteCategory category=new VoteCategory(jsonArray.getJSONObject(i));
            categories.add(category);
        }

        return categories;
    }
    public VoteChannel(String id,String name,String desc, boolean verified,long start, long end)
    {
        this.id=id;
        this.name=name;
        this.desc=desc;
        this.verified=verified;
        this.start=start;
        this.end=end;
    }
}
