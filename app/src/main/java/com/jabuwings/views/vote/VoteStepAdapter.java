package com.jabuwings.views.vote;

import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.jabuwings.R;
import com.jabuwings.views.registration.AcademicDetails;
import com.jabuwings.views.registration.AccountDetails;
import com.jabuwings.views.registration.PersonalDetails;
import com.jabuwings.views.registration.StudentDisclaimer;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import org.json.JSONException;

import java.util.List;

 class VoteStepAdapter extends AbstractFragmentStepAdapter {

    List<VoteCategory> categories;
    String electionName;
     VoteStepAdapter(@NonNull FragmentManager fm, @NonNull Context context, String electionName, List<VoteCategory> cats) {
        super(fm, context);
         Log.d("VoteStep","voteStep cats no "+cats.size());
        this.categories=cats;
        this.electionName=electionName;
    }

    @Override
    public Step createStep(int position) {

        VoteCategory category= categories.get(position);
        try {
            boolean last=position==categories.size()-1;

            return VoteForCategory.newInstance(category.getName(),category.getCandidatesArray().toString(), last,category.getJsonObject().toString());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            return null;

        }



    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        StepViewModel.Builder builder = new StepViewModel.Builder(context)
                .setTitle(electionName);

        VoteCategory category=categories.get(position);

        try {
            if(categories.size()==1)
            {
                return builder.setTitle(category.getName()).setNextButtonLabel("Vote").create();
            }
            else
            if (position == 0)
                return builder.setTitle(category.getName()).setNextButtonLabel(categories.get(position + 1).getName()).create();
            else if (position == categories.size() - 1)
                return builder.setTitle(category.getName()).setBackButtonLabel(categories.get(position - 1).getName()).setNextButtonLabel("Vote").create();
            else
                return builder.setTitle(category.getName()).setNextButtonLabel(categories.get(position + 1).getName()).setBackButtonLabel(categories.get(position - 1).getName()).create();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            return builder.create();
        }


    }
}