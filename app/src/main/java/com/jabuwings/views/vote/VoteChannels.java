package com.jabuwings.views.vote;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Falade James on 2/9/2017 All Rights Reserved.
 */

public class VoteChannels implements Serializable {
    public JSONArray channels;
    public VoteChannels(JSONArray c)
    {
        this.channels=c;
    }
}
