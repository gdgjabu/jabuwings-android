package com.jabuwings.views.vote;

/**
 * Created by Falade James on 2/10/2017 All Rights Reserved.
 */

public class VoteSingle {
    String id,channelId,name,post,picture,channelName;
    public VoteSingle(String id,String name, String post,String picture, String channelId, String channelName)
    {
        this.id=id;
        this.name=name;
        this.post=post;
        this.picture=picture;
        this.channelName=channelName;
        this.channelId=channelId;

    }
}
