package com.jabuwings.views.vote;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.vision.barcode.Barcode;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.Const;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.CircleImage;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.jabuwings.widgets.barcode.MaterialBarcodeScanner;
import com.jabuwings.widgets.barcode.MaterialBarcodeScannerBuilder;
import com.jabuwings.widgets.crystalpreloaders.widgets.CrystalPreloader;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.PatternSyntaxException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class VotePre extends JabuWingsActivity {

    @InjectView(R.id.vote_pre_text)
    TextView text;
    @InjectView(R.id.vote_pre_info)
    TextView tvInfo;
    @InjectView(R.id.vote_pre_load)
    CrystalPreloader load;
    @InjectView(R.id.vote_pre_button)
    Button button;
    @InjectView(R.id.ib_vote_pre_barcode)
    ImageButton imageButton;
    boolean voting=false;

    String loadingMessage="";
    boolean working=true;

    boolean single=false;

    //The two variables are used in case an attempt to load a channel after a vote failed
    boolean cus=false;
    HashMap<String,Object> p;
    DialogFragment voteFragment;
    private void pr()
    {
        boolean linked=getIntent().getBooleanExtra("linked",true);
        single=getIntent().getBooleanExtra("single",false);
       // String id=getIntent().getStringExtra("id");



        loadingMessage= linked?"Processing Link": "Loading Channels";
        tvInfo.setText(loadingMessage);

        if(linked) {
            HashMap<String,Object> params= manager.getDefaultCloudParams();
            try {
                final String id = getIntent().getData().getQueryParameter("id");
                single = id.length()==11;
                params.put(Const.ID,id);


                if (single)
                    ParseCloud.callFunctionInBackground("vote_get_single", params, new FunctionCallback<String>() {
                        @Override
                        public void done(String s, ParseException e) {
                            if (e == null) {

                                try
                                {
                                    JSONObject json = new JSONObject(s);
                                    final VoteSingle single=new VoteSingle(json.getString(Const.ID),json.getString(Const.NAME),json.getString(Const.POSITION),json.getString(Const.PROFILE_PICTURE),json.getString(Const.CHANNEL_ID),json.getString( Const.CHANNEL_NAME));

                                    Dialog.Builder builder;
                                    builder=new SimpleDialog.Builder(R.style.SimpleDialog){

                                        @Override
                                        protected void onBuildDone(final Dialog dialog) {
                                            CircleImage image=(CircleImage)dialog.findViewById(R.id.vote_single_picture);
                                            Picasso.with(VotePre.this).load(single.picture.replace("localhost","192.168.43.99")).placeholder(R.drawable.default_avatar).into(image);
                                            TextView name=(TextView)dialog.findViewById(R.id.vote_single_name);
                                            name.setText(single.name);
                                            TextView position=(TextView)dialog.findViewById(R.id.vote_single_post);
                                            position.setText(single.post);
                                            Button yes=(Button)dialog.findViewById(R.id.vote_single_yes);
                                            Button no=(Button)dialog.findViewById(R.id.vote_single_no);


                                            yes.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();
                                                    tvInfo.setText(null);
                                                    setText("Casting your vote");
                                                    HashMap<String,Object> params= manager.getDefaultCloudParams();
                                                    params.put("sId",single.id);
                                                    ParseCloud.callFunctionInBackground("vote_cast_single", params, new FunctionCallback<String>() {
                                                        @Override
                                                        public void done(String s, ParseException e) {
                                                            if(e==null)
                                                            {
                                                                setText("");
                                                                SweetAlertDialog d=new SweetAlertDialog(VotePre.this,SweetAlertDialog.SUCCESS_TYPE).setTitleText("You have voted").setContentText(s).setConfirmText(getString(R.string.yes)).setCancelText(getString(R.string.no)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                    @Override
                                                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                                        loadChannel(single.id);
                                                                    }
                                                                }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                    @Override
                                                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                                        finish();
                                                                        manager.openHome();
                                                                    }
                                                                });
                                                                d.setCancelable(false);
                                                                d.show();
                                                            }
                                                            else
                                                                handleError(e);
                                                        }
                                                    });
                                                }
                                            });
                                            no.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    if(voteFragment!=null)
                                                    {
                                                        voteFragment.dismiss();
                                                    }

                                                    loadChannel(single.id);
                                                }
                                            });
                                        }
                                    };

                                    builder.title(single.channelName)
                                            .contentView(R.layout.vote_single);
                                    voteFragment = DialogFragment.newInstance(builder);
                                    voteFragment.setCancelable(false);
                                    voteFragment.show(getSupportFragmentManager(), null);

                                }
                                catch (JSONException q)
                                {
                                    handleError(new ParseException(999,"Invalid Single Vote Data"));
                                    q.printStackTrace();
                                }


                            } else {
                                handleError(e);

                            }

                        }
                    });
                else
                    handleC(params);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                handleError(new ParseException(999,"Invalid link"));
            }



        }
        else
        {
            ParseCloud.callFunctionInBackground("vote_get_channels", manager.getDefaultCloudParams(), new FunctionCallback<String>() {
                @Override
                public void done(String s, ParseException e) {
                    if(e==null)
                    {

                        try {

                            manager.log(s);
                            JSONArray jsonArray= new JSONObject(s).getJSONArray("channels");
                            Vote.channels=new VoteChannels(jsonArray);
                            startActivity(new Intent(VotePre.this, Vote.class).putExtra("linked", false));
                            finish();
                        }
                        catch (Exception e1)
                        {
                            e1.printStackTrace();
                            handleError(new ParseException(999,"Invalid Vote data"));
                        }
                    }
                    else{
                       handleError(e);
                    }
                }
            });
        }
    }

    private void loadChannel(String channelId)
    {
        tvInfo.setText("Loading Channel");
        HashMap<String,Object> params= manager.getDefaultCloudParams();
        params.put(Const.ID,channelId);
        cus=true;
        p=params;
        handleC(params);
    }
    private void handleC(HashMap<String,Object> params)
    {
        ParseCloud.callFunctionInBackground("vote_get_channel", params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {
                if (e == null) {
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        VoteChannel channel = new VoteChannel(jsonObject);
                        VoteForChannel.categories = new VoteCategories(channel.getCategories());
                        startActivity(new Intent(VotePre.this, VoteForChannel.class).putExtra(Const.ID, channel.id).putExtra(Const.NAME, channel.name));
                        finish();
                    }
                    catch (JSONException e1)
                    {
                        handleError(new ParseException(999,"Invalid Channel Data"));
                        e1.printStackTrace();
                    }
                } else {
                    handleError(e);
                }

            }
        });
    }
    private void handleError(ParseException e)
    {
        toggle();
        if(e.getCode()==ParseException.SCRIPT_ERROR)
        {
            setText(e.getMessage());
        }
        else {
            if(e.getCode()==ParseException.CONNECTION_FAILED)
            {
                setText(getString(R.string.error_network));
            }
            else if(e.getCode()==999)
            {
                setText(e.getMessage());
            }
            else
                ErrorHandler.handleError(context, e);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vote_pre);
        ButterKnife.inject(this);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                working=true;
                load.setVisibility(View.VISIBLE);
                button.setVisibility(View.GONE);
                text.setText(null);
                if(cus)
                {
                    tvInfo.setText("Loading Channel");
                    handleC(p);

                }
                else
                pr();
            }
        });

        pr();


    }



    private void toggle()
    {
        if(working)
        {
            tvInfo.setText(null);
            button.setVisibility(View.VISIBLE);
            load.setVisibility(View.GONE);
            text.setText(null);
        }
        else{
            tvInfo.setText(loadingMessage);
            button.setVisibility(View.GONE);
            load.setVisibility(View.VISIBLE);
        }
        working=!working;

    }


    @Override
    public void onBackPressed() {
        if(!voting)
        super.onBackPressed();
    }

    private void setText(String t)
    {
        text.setVisibility(View.VISIBLE);
        text.setText(t);
    }

    @OnClick(R.id.ib_vote_pre_barcode)
    public void scanBarcode()
    {
        final MaterialBarcodeScanner materialBarcodeScanner = new MaterialBarcodeScannerBuilder()
                .withActivity(VotePre.this)
                .withEnableAutoFocus(true)
                .withBleepEnabled(true)
                .withBackfacingCamera()
                .withText("Scan the vote channel's QR code")
                .withResultListener(new MaterialBarcodeScanner.OnResultListener() {
                    @Override
                    public void onResult(Barcode barcode) {
                        String code = barcode.rawValue;
                        loadChannel(code);



                    }
                })
                .build();
        materialBarcodeScanner.startScan();

    }
}
