package com.jabuwings.views.vote;

import com.jabuwings.management.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Falade James on 2/11/2017 All Rights Reserved.
 */

 class VoteCategory {
    private JSONObject jsonObject;
     VoteCategory(JSONObject jsonObject)
    {
        this.jsonObject=jsonObject;

    }

     JSONObject getJsonObject()
    {
        return jsonObject;
    }

    public String getId()throws JSONException
    {
        return jsonObject.getString(Const.ID);
    }

    public String getName() throws JSONException
    {
        return jsonObject.getString(Const.NAME);
    }

    public String getDesc() throws JSONException
    {
        return jsonObject.getString(Const.DESC);
    }

     JSONArray getCandidatesArray() throws JSONException{
        return jsonObject.getJSONArray(Const.CANDIDATES);
    }
    public List<VoteCandidate> getCandidates() throws JSONException
    {
        JSONArray candidates=jsonObject.getJSONArray(Const.CANDIDATES);

        List<VoteCandidate> v=new ArrayList<>();

        for(int i=0; i<candidates.length(); i++)
        {
            VoteCandidate candidate=new VoteCandidate(candidates.getJSONObject(i));
            v.add(candidate);

        }
        return v;
    }

}
