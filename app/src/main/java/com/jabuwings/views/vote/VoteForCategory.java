package com.jabuwings.views.vote;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.management.Manager;
import com.jabuwings.widgets.CircleImage;
import com.squareup.picasso.Picasso;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VoteForCategory.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VoteForCategory#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VoteForCategory extends Fragment implements Step {
    private static final String NAME = "param1";
    private static final String CANDIDATES = "param2";
    private static final String LAST = "last";
    private static final String CATEGORY = "category";

    private String name;

    private String candidates;
    private String cat;
    private boolean last;

    VoteCategory category;
     OnFragmentInteractionListener mListener;



    public VoteForCategory() {
        // Required empty public constructor
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(chosenCandidate!=null)
        {
            try {
                outState.putString("candidate", chosenCandidate.getId());
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
        super.onSaveInstanceState(outState);

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param name Parameter 1.
     * @param candidates Parameter 2.
     * @return A new instance of fragment VoteForCandidate.
     */
    public static VoteForCategory newInstance(String name, String candidates,boolean last,String category) {
        VoteForCategory fragment = new VoteForCategory();
        Bundle args = new Bundle();
        args.putString(NAME, name);
        args.putBoolean(LAST, last);
        args.putString(CANDIDATES, candidates);
        args.putString(CATEGORY, category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public VerificationError verifyStep() {
        if(last)
        {
            mListener.voteCompleted(true);
        }
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
    String chosenCandidateId;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(NAME);
            candidates = getArguments().getString(CANDIDATES);
            last=getArguments().getBoolean(LAST);
            cat=getArguments().getString(CATEGORY);
            try {
                category = new VoteCategory(new JSONObject(cat));
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

    }



    static VoteCandidate chosenCandidate;
    public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder>{

        List<VoteCandidate> candidates;
        List<CheckBox> checkBoxes=new ArrayList<>();
        String chosenCandidateId;
        public Adapter(List<VoteCandidate> cats,String chosenCandidateId)
        {
            this.chosenCandidateId=chosenCandidateId;
            candidates=cats;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.vote_for_category_item,parent,false);
            return new ViewHolder(v);
        }

        private void checkCheck(int position)
        {
            for (int i=0; i<checkBoxes.size(); i++)
            {
                if(i!=position)
                {
                    checkBoxes.get(i).setChecked(false);
                }
            }
        }
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {




            final VoteCandidate candidate= candidates.get(position);

            try {
                //check if the candidate has been selected before
                if (chosenCandidateId!=null && chosenCandidateId.equals(candidate.getId())) {
                    holder.checkBox.setChecked(true);
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            try {
                holder.name.setText(candidate.getName());
                holder.meta.setText(candidate.getMeta());
                holder.checkBox.setTag(position);
                checkBoxes.add(holder.checkBox);
                Picasso.with(getContext()).load(candidate.getProfilePicture().replace("localhost","192.168.43.99")).placeholder(R.drawable.default_avatar).into(holder.image);
            }
            catch (JSONException e)
            {
                new Manager(getContext()).errorToast("There was an error");
                e.printStackTrace();
            }

            holder.view.setTag(candidate);
            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int position = (int) buttonView.getTag();
                    if(isChecked) {
                        checkCheck(position);
                        chosenCandidate=candidates.get(position);
                        mListener.remitCandidate(category,candidates.get(position));
                    }
                    else{
                        mListener.removeCandidate(category,candidates.get(position));
                    }
                }
            });
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    VoteCandidate candidate=(VoteCandidate)v.getTag();
                    mListener.remitCandidate(category,candidate);
                    chosenCandidate=candidate;

                    CheckBox checkBox =(CheckBox)v.findViewById(R.id.vote_for_category_checkbox);
                    if(checkBox.isChecked())
                    {
                      checkBox.setChecked(false);
                      mListener.removeCandidate(category,candidate);
                    }
                    else
                    checkBox.setChecked(true);
                    int position=(int)checkBox.getTag();
                    checkCheck(position);


                }
            });



        }

        @Override
        public int getItemCount() {
            return candidates.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder{@InjectView(R.id.vote_for_category_name)
        TextView name;
            @InjectView(R.id.vote_for_category_picture)
            CircleImage image;
            @InjectView(R.id.vote_for_category_meta)
            TextView meta;
            View view;
            @InjectView(R.id.vote_for_category_checkbox)
            CheckBox checkBox;
            public ViewHolder(View itemView)
            {
                 super(itemView);
                view=itemView;
                ButterKnife.inject(this,itemView);
            }

        }
    }



    @InjectView(R.id.vote_category_name)
    TextView tvName;
    @InjectView(R.id.rvVoteForCategory)
    RecyclerView recyclerView;
    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.fragment_vote_for_category, container, false);
        ButterKnife.inject(this,rootView);
        Manager manager=new Manager(getContext());

        try {
            tvName.setText(category.getDesc());

            JSONArray cats = new JSONArray(candidates);
            List<VoteCandidate> candidates = new ArrayList<>();
            for (int i = 0; i < cats.length(); i++) {
                VoteCandidate cand = new VoteCandidate(cats.getJSONObject(i));
                manager.log("cat "+cand.getName());
                candidates.add(cand);
            }
            manager.log("cant "+candidates.size());
            if(chosenCandidate!=null)
            chosenCandidateId=chosenCandidate.getId();
            Adapter adapter=new Adapter(candidates,chosenCandidateId);

            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(adapter);
        }catch (JSONException e)
        {
            e.printStackTrace();
        }
        return rootView;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void voteCompleted(boolean done);
        void remitCandidate(VoteCategory category, VoteCandidate candidate);
        void removeCandidate(VoteCategory category, VoteCandidate candidate);
    }
}
