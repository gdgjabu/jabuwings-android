package com.jabuwings.views.football;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jabuwings.R;
import com.jabuwings.views.main.JabuWingsFragment;

import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class Live extends FootballFragment {


    public Live() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_football_live, container, false);
        ButterKnife.inject(this,view);
        setUp(view);
        return view;
    }

}
