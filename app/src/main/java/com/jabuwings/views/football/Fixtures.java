package com.jabuwings.views.football;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.views.chapel.Announcements;
import com.jabuwings.views.football.models.FootballFixture;
import com.jabuwings.views.main.JabuWingsFragment;
import com.jabuwings.widgets.section.SimpleSectionedAdapter;
import com.parse.FindCallback;
import com.parse.ParseException;

import java.util.List;

import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fixtures extends FootballFragment {


    public Fixtures() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_football_fixtures, container, false);
        ButterKnife.inject(this,view);
        setUp(view);
        return view;
    }

    private void getFixtures()
    {
        FootballFixture.getQuery().findInBackground(new FindCallback<FootballFixture>() {
            @Override
            public void done(List<FootballFixture> fixtures, ParseException e) {
                if(e==null)
                {

                }
                else {
                    ErrorHandler.handleError(getContext(), e);
                    getFixtures();
                }
            }
        });
    }

    
    class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(View v)
        {
            super(v);
        }
    }
    public class Adapter extends SimpleSectionedAdapter<ViewHolder>{
        @Override
        protected String getSectionHeaderTitle(int section) {
            return null;
        }

        @Override
        protected int getSectionCount() {
            return 0;
        }

        @Override
        protected int getItemCountForSection(int section) {
            return 0;
        }

        @Override
        protected ViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
            return null;
        }

        @Override
        protected void onBindItemViewHolder(ViewHolder holder, int section, int position) {

        }
    }
}
