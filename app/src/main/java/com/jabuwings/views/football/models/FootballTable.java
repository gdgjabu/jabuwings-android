package com.jabuwings.views.football.models;

import com.jabuwings.management.Const;
import com.jabuwings.models.ClassroomCourse;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;

/**
 * Created by Falade James on 7/18/2016 All Rights Reserved.
 */
@ParseClassName("JABU_FOOTBALL_LEAGUE_TABLE")
public class FootballTable extends ParseObject {

    public int getPoints()
    {
        return getInt("points");
    }

    public int getGoalDifference()
    {
        return getGoalsFor()-getGoalsAgainst();
    }

    public int getGoalsFor()
    {
        return getInt("gf");
    }


    public int getGoalsAgainst()
    {
        return getInt("ga");
    }
    public int getPayed()
    {
        return getInt("played");
    }

    public String getName(){return  getString(Const.NAME);}

    



    public static ParseQuery<FootballTable> getQuery(){return ParseQuery.getQuery(FootballTable.class);}


}
