package com.jabuwings.views.football;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import com.jabuwings.R;
import com.jabuwings.models.ViewPagerAdapter;
import com.jabuwings.views.main.JabuWingsActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class Football extends JabuWingsActivity {

    @InjectView(R.id.footballPager)
    ViewPager pager;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.football_table:
                    setTitle(R.string.table);
                    pager.setCurrentItem(0);
                    return true;
                case R.id.football_fixtures:
                    setTitle(R.string.fixtures);
                    pager.setCurrentItem(1);
                    return true;
                case R.id.football_videos:
                    setTitle(R.string.videos);
                    pager.setCurrentItem(2);
                    return true;
                case R.id.football_live:
                    setTitle(R.string.live);
                    pager.setCurrentItem(3);
                    return true;


            }
            return false;
        }

    };

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Table(), "Table");
        adapter.addFragment(new Fixtures(), "Fixtures");
        adapter.addFragment(new Videos(), "Videos");
        adapter.addFragment(new Live(), "Live");
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_football);
        setUpToolbar(null,R.string.football);
        ButterKnife.inject(this);
        setupViewPager(pager);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}
