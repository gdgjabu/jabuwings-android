package com.jabuwings.views.football;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.Manager;
import com.jabuwings.views.football.models.FootballTable;
import com.jabuwings.views.main.JabuWingsFragment;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.codecrafters.tableview.SortableTableView;
import de.codecrafters.tableview.TableHeaderAdapter;
import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.model.TableColumnWeightModel;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class Table extends FootballFragment {


    public Table() {
        // Required empty public constructor
    }


    List<FootballTable> table;
    SortableTableView tableView;
    Manager manager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FrameLayout view =(FrameLayout) inflater.inflate(R.layout.fragment_football_table, container, false);
        setUp(view);
        setUpFragment(view);
        
        getTable();
        manager = new Manager(getContext());
        
        return view;
    }

    private void getTable()
    {
        FootballTable.getQuery().addDescendingOrder("points").findInBackground(new FindCallback<FootballTable>() {
            @Override
            public void done(List<FootballTable> list, ParseException e) {
                if(e==null)
                {
                    table = list;
                    processTable();
                }
                else{
                    ErrorHandler.handleError(getContext(),e);
                    getTable();
                }
            }
        });
    }

    private static class FootballTableComparator implements Comparator<String[]> {
        @Override
        public int compare(String[] club1, String[] club2) {
            //compare points
            if(Integer.parseInt(club1[4])==Integer.parseInt(club2[4]))
            {
                //return goal difference
                return Integer.parseInt(club2[3])- Integer.parseInt(club1[3]);
            }
            else{
                //return point difference
                return Integer.parseInt(club2[4])-Integer.parseInt(club1[4]);
            }
        }
    }

    private void processTable()
    {
        tableView = (SortableTableView)findViewById(R.id.tableView); 
        stopLoad();
        List<String[]> data = new ArrayList<>();
        
        for(int i =0; i<table.size(); i++)
        {
            FootballTable club = table.get(i);
            String[] a= {String.valueOf(i+1),club.getName(),String.valueOf(club.getPayed()),String.valueOf(club.getGoalDifference()),String.valueOf(club.getPoints())};
            data.add(a);
        }


        Collections.sort(data,new FootballTableComparator());
        
        
        //Reposition the club's position after sort
        for(int i=0; i<data.size(); i++)
        {
            String[] a = data.get(i);
            a[0]= String.valueOf(i+1);
        }
       
        

        TableColumnWeightModel columnModel = new TableColumnWeightModel(5);
        columnModel.setColumnWeight(1, 2);
        tableView.setColumnModel(columnModel);
        tableView.setHeaderAdapter(new TableHeaderAdapter(getContext(),5) {
            @Override
            public View getHeaderView(int columnIndex, ViewGroup parentView) {

                TextView textView = new TextView(getContext());

                switch (columnIndex)
                {
                    case 0:
                        textView.setText("#") ;
                        break;
                    case 1:
                        textView.setText("Name") ;
                        break;
                    case 2:
                        textView.setText("P");
                        break;
                    case 3:
                        textView.setText("GD") ;
                        break;
                    case 4:
                        textView.setText("Pts") ;
                        break;


                }
                return textView;
            }
        });
        tableView.setDataAdapter(new SimpleTableDataAdapter(getContext(), data));

    }

}
