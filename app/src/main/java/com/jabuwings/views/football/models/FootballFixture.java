package com.jabuwings.views.football.models;

import com.jabuwings.management.Const;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;

import java.util.Date;
import java.util.List;

/**
 * Created by jamesfalade on 29/08/2017.
 */

@ParseClassName("JABU_FOOTBALL_LEAGUE_FIXTURES")
public class FootballFixture extends ParseObject{
    public FootballTable getFirstFootballClub()
    {
        return (FootballTable) getParseObject("club1");
    }

    public FootballTable getSecondFootballClub()
    {
        return (FootballTable) getParseObject("club2");
    }

    public Date getMatchDate()
    {
        return getDate(Const.DATE);
    }

    public boolean isPlayed()
    {
        return getBoolean("played");
    }

    public int goalsByFirstFootballClub()
    {
        return getInt("goals1");
    }

    public int goalsBySecondFootballClub()
    {
        return getInt("goals2");
    }

    public ParseRelation<FootballPlayer> getScorers()
    {
        return getRelation("scorers");
    }

    public static ParseQuery<FootballFixture> getQuery(){return ParseQuery.getQuery(FootballFixture.class);}



}
