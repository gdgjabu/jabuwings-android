package com.jabuwings.views.football;

import android.view.View;

import com.jabuwings.R;
import com.jabuwings.views.main.JabuWingsFragment;
import com.jabuwings.widgets.crystalpreloaders.widgets.CrystalPreloader;

/**
 * Created by jamesfalade on 27/08/2017.
 */

public class FootballFragment extends JabuWingsFragment {
    CrystalPreloader loader;
    protected String FOOTBALL_TABLE = "JABU_FOOTBALL_LEAGUE_TABLE";
    protected String FOOTBALL_FIXTURES = "JABU_FOOTBALL_LEAGUE_FIXTURES";
    protected String FOOTBALL_VIDEOS = "JABU_FOOTBALL_LEAGUE_VIDEOS";
    protected String FOOTBALL_LIVE = "JABU_FOOTBALL_LEAGUE_LIVE";
    protected void setUp(View rootView)
    {
        loader =(CrystalPreloader) rootView.findViewById(R.id.football_load);
    }

    protected void stopLoad()
    {
        loader.setVisibility(View.GONE);
    }

    protected void startLoad()
    {
        loader.setVisibility(View.VISIBLE);
    }
}
