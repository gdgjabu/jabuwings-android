package com.jabuwings.views.intro;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.jabuwings.R;

/**
 * Created by Falade James on 11/22/2015 All Rights Reserved.
 */
public class Intro1 extends IntroFragment {

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        background.setBackgroundColor(Color.parseColor("#222222"));
        title.setText("Welcome to JabuWings");
        imageBig.setVisibility(View.GONE);
        imageSmall.setImageResource(R.drawable.logo);
        imageSmall.setVisibility(View.VISIBLE);
        desc.setText("Thank you for downloading JabuWings. Connect with Entrepreneurs");


    }
}
