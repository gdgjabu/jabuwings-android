package com.jabuwings.views.intro;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.jabuwings.R;

/**
 * Created by Falade James on 11/22/2015 All Rights Reserved.
 */
public class Intro3 extends IntroFragment {


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        background.setBackgroundColor(Color.parseColor("#8bc34a"));
        title.setText("Chat with your friends");
        imageBig.setImageResource(R.drawable.intro1);
        desc.setText("Chat with people. Your course mates, friends, lecturers, and the entire community");


    }
}
