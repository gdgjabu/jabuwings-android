package com.jabuwings.views.intro;


import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jabuwings.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntroFragment extends Fragment {
View background;
TextView title, desc;
ImageView imageSmall, imageBig;


    public IntroFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_intro, container, false);
        background=v.findViewById(R.id.intro_background);
        title=(TextView)v.findViewById(R.id.intro_title);
        desc=(TextView)v.findViewById(R.id.intro_description);
        imageSmall=(ImageView)v.findViewById(R.id.intro_image_small);
        imageBig=(ImageView)v.findViewById(R.id.intro_image);
        return v;
    }


}
