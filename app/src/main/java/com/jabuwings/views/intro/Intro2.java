package com.jabuwings.views.intro;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.jabuwings.R;

/**
 * Created by Falade James on 11/22/2015 All Rights Reserved.
 */
public class Intro2 extends IntroFragment {


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        background.setBackgroundColor(Color.parseColor("#f44336"));
        title.setText("Navigate");
        imageBig.setImageResource(R.drawable.intro4);
        desc.setText("The navigation menu allows you to move through options ");

    }
}
