package com.jabuwings.views.intro;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.jabuwings.R;

/**
 * Created by Falade James on 11/22/2015 All Rights Reserved.
 */
public class Intro5 extends IntroFragment {


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        background.setBackgroundColor(Color.parseColor("#9c27b0"));
        title.setText("Get serious, do something official.");
        imageBig.setImageResource(R.drawable.intro5);
        desc.setText("Do your course registration, check your results, check lecture timetables, download course materials and much more");


    }
}
