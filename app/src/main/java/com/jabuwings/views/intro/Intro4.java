package com.jabuwings.views.intro;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.jabuwings.R;

/**
 * Created by Falade James on 11/22/2015 All Rights Reserved.
 */
public class Intro4 extends IntroFragment {


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        background.setBackgroundColor(Color.parseColor("#2196f3"));
        title.setText("A Smiley is worth a hundred words");
        imageBig.setImageResource(R.drawable.intro3);
        desc.setText("Words may not be enough to express your feelings, try our smileys");


    }
}
