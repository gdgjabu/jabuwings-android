package com.jabuwings.views.intro;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.github.paolorotolo.appintro.AppIntro2;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.views.main.Login;
import com.jabuwings.views.main.LoginHook;

/**
 * Created by Falade James on 11/22/2015 All Rights Reserved.
 */
public class Intro extends AppIntro2 {
    public static boolean newSignIn=false;
    @Override
    public void init(@Nullable Bundle savedInstanceState) {
        addSlide(new Intro1());
        addSlide(new Intro2());
        addSlide(new Intro3());
        addSlide(new Intro4());
        addSlide(new Intro5());
        setFlowAnimation();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        newSignIn=true;
        new Manager(this).setIntroViewed();
        startActivity(new Intent(Intro.this, LoginHook.class).putExtra(Const.TUTORIAL,true));
        finish();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        onDonePressed(currentFragment);
    }
}
