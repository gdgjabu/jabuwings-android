package com.jabuwings.views.timetable;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.management.AcademicsManager;
import com.jabuwings.management.App;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.TimeTableItem;
import com.jabuwings.models.TimeTableRecycler;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.main.JabuWingsActivity;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.rey.material.app.TimePickerDialog;
import com.rey.material.widget.Spinner;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by Falade James on 10/20/2015 All Rights Reserved.
 */
public class TimeKeeper {
String day;
private String level;
private int college;
private Context context;
boolean filter, AS,ES,HS,HM,LW,MS,NS,SS;
private static String LOG_TAG=TimeKeeper.class.getSimpleName();
List<String> userCourses;
Manager manager;
Document document;
Document documentEditing;
Time time;
    public TimeKeeper (String day, int college, String level, boolean filter,AppCompatActivity context)
    {
        this.day=day;
        this.context=context;
        this.college=college;
        this.level=level;
        time=new Time();
        this.filter=filter;
        manager=new Manager(context);
        userCourses= manager.getUserCourses();
        LOG_TAG=TimeKeeper.class.getSimpleName()+" "+day;

    }

    public TimeKeeper(Context context)
    {
        this.context=context;
        manager=new Manager(context);
    }

    /**
     *
     * @param manager The Manager object
     * @param refresh If true, the timetable document will be refreshed
     * @return the document
     */
    private Document getDocument(Manager manager, boolean refresh) {



        if(document==null ||refresh)
        {
            try {
                DocumentBuilderFactory factory =
                        DocumentBuilderFactory.newInstance();
                //Get the DOM Builder
                DocumentBuilder builder = factory.newDocumentBuilder();
                if (manager.isCustomTimeTable()) {
                    FileInputStream imageInput;
                    imageInput = manager.getContext().openFileInput("timetable.xml");
                    document = builder.parse(imageInput);
                } else {
                    document = builder.parse(manager.getContext().getResources().openRawResource(R.raw.timetable));
                }
                return document;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }
        else
            return document;

    }

    public  String getTimetableVersion()
    {
        Document doc= getDocument(manager,false);
        Element e=doc.getDocumentElement();
        return e.getAttributes().getNamedItem(Const.ID).getNodeValue();
    }
    public  String getTimeTableSemester()
    {
        Document doc= getDocument(manager,false);
        Element e=doc.getDocumentElement();
        return e.getAttributes().getNamedItem(Const.SEMESTER).getNodeValue();
    }

    public static List<TimeTableItem> getNextLectures(Context context, String courseCode)
    {
        Manager manager = new Manager(context);
        Document document=new TimeKeeper(context).getDocument(manager,false);

        try{
            NodeList nodeList = document.getDocumentElement().getElementsByTagName("college");
            Element collegeElement;
            String preferredCollege=manager.getCollege();
            String college=preferredCollege==null? "NS": preferredCollege;

            collegeElement= findElement(college,nodeList);
            NodeList coursesNode=collegeElement.getElementsByTagName("course");
            List<TimeTableItem> courses=new ArrayList<>();

            for(int a=0; a<coursesNode.getLength(); a++)
            {
                Element courseElement = (Element) coursesNode.item(a);
                NamedNodeMap attributes = courseElement.getAttributes();
                if(attributes.getNamedItem(Const.COURSE_CODE).getNodeValue().equals(courseCode))
                {
                    courses.add(new TimeTableItem(attributes.getNamedItem(Const.COURSE_CODE).getNodeValue(),
                            attributes.getNamedItem(Const.VENUE).getNodeValue(),
                            attributes.getNamedItem(Const.TIME_START).getNodeValue(),
                            attributes.getNamedItem(Const.TIME_STOP).getNodeValue(),
                            attributes.getNamedItem(Const.LEVEL).getNodeValue()
                            ,attributes.getNamedItem(Const.DAY).getNodeValue(),
                            collegeElement.getAttributes().getNamedItem(Const.NAME).getNodeValue()));
                }

            }

            return courses;
        }

        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    public static List<TimeTableItem> getNextLectures(Context context){
        Time time= new Time();
        int hour= time.getHour();
        String day = time.getTimetableDay();
        Manager manager = new Manager(context);
        List<TimeTableItem> items = new ArrayList<>();
        List<String> userCourses= manager.getUserCourses();
        Document document=new TimeKeeper(context).getDocument(manager,false);
        try
        {


            NodeList nodeList = document.getDocumentElement().getElementsByTagName("college");
            Element collegeElement=null;
            String preferredCollege=manager.getCollege();
            String college=preferredCollege==null? Const.NATURAL_SCIENCES: preferredCollege;

            collegeElement= findElement(college,nodeList);
            NodeList coursesNode=collegeElement.getElementsByTagName("course");
            for(int a=0; a<coursesNode.getLength(); a++) {

                Element courseElement = (Element) coursesNode.item(a);
                NamedNodeMap attributes = courseElement.getAttributes();
                TimeTableItem course=new TimeTableItem(attributes.getNamedItem(Const.COURSE_CODE).getNodeValue(),
                        attributes.getNamedItem(Const.VENUE).getNodeValue(),
                        attributes.getNamedItem(Const.TIME_START).getNodeValue(),
                        attributes.getNamedItem(Const.TIME_STOP).getNodeValue(),
                        attributes.getNamedItem(Const.LEVEL).getNodeValue()
                        ,attributes.getNamedItem(Const.DAY).getNodeValue(),
                        collegeElement.getAttributes().getNamedItem(Const.NAME).getNodeValue());
                if(userCourses.contains(course.getCourseCode()) &&
                        course.getDay().equals(day)&& time.getHour(course.getTimeStart())>hour)
                {
                    items.add(course);
                    Log.d(LOG_TAG,"Course added for notification: "+course.toString());
                }
                else {
                    Log.d(LOG_TAG, course.getCourseCode()+" did not meet the requirements for notification");
                }

            }

            Collections.sort(items);
            return items;
        }
        catch ( Exception e )
        {
            Log.d(LOG_TAG,"Error occurred while parsing timetable for notification courses ");
            e.printStackTrace();
            //Toast.makeText(context, context.getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
            return items;
        }
    }

    public void showTimeTable(final RecyclerView recyclerView)
    {
        new AsyncTask<Void,Void,TimeTableRecycler>(){
            @Override
            protected TimeTableRecycler doInBackground(Void... params) {
                try {
                    getDocument(manager,false);
                    Element documentElement=document.getDocumentElement();
                    String id=documentElement.getAttribute("id");
                    Log.d(LOG_TAG,"ID "+id);
                    manager.setTimeTableVersion(id);
                    NodeList nodeList = document.getDocumentElement().getElementsByTagName("college");
                    Element collegeElement=null;

                    switch (college)
                    {
                        case 0:
                            AS=true;
                            break;
                        case 1:
                            ES=true;
                            break;
                        case 2:
                            HS=true;
                            break;
                        case 3:
                            HM=true;
                            break;
                        case 4:
                            LW=true;
                            break;
                        case 5:
                            MS=true;
                            break;
                        case 6:
                            NS=true;
                            break;
                        case 7:
                            SS = true;
                            break;
                    }

                    if(AS)
                    {
                        collegeElement= findElement(Const.AGRICULTURAL_SCIENCES,nodeList);
                        Log.d(LOG_TAG,"AS "+collegeElement.getChildNodes().getLength());
                    }

                    if(ES)
                    {
                        collegeElement= findElement(Const.ENVIRONMENTAL_SCIENCES,nodeList);
                        Log.d(LOG_TAG,"ES "+collegeElement.getChildNodes().getLength());
                    }

                    if(HS)
                    {
                        collegeElement= findElement(Const.HEALTH_SCIENCES,nodeList);
                        Log.d(LOG_TAG,"HS "+collegeElement.getChildNodes().getLength());
                    }

                    if(HM)
                    {
                        collegeElement= findElement(Const.HUMANITIES,nodeList);
                        Log.d(LOG_TAG,"HM "+collegeElement.getChildNodes().getLength());
                    }

                    if(LW)
                    {
                        collegeElement= findElement(Const.LAW,nodeList);
                        Log.d(LOG_TAG,"LW "+collegeElement.getChildNodes().getLength());

                    }

                    if(MS)
                    {
                        collegeElement= findElement(Const.MANAGEMENT_SCIENCES,nodeList);
                        Log.d(LOG_TAG,"MS "+collegeElement.getChildNodes().getLength());

                    }

                    if(NS)
                    {
                        collegeElement= findElement(Const.NATURAL_SCIENCES,nodeList);
                        Log.d(LOG_TAG,"NS "+collegeElement.getChildNodes().getLength());

                    }

                    if(SS)
                    {
                        collegeElement= findElement(Const.SOCIAL_SCIENCES,nodeList);
                        Log.d(LOG_TAG,"SS "+collegeElement.getChildNodes().getLength());

                    }

                    if(collegeElement==null)
                    {
                        Log.d(LOG_TAG,"College Element is null");
                        return null;
                    }

                    NodeList coursesNode=collegeElement.getElementsByTagName("course");
                    List<TimeTableItem> items = new ArrayList<>();
                    for(int a=0; a<coursesNode.getLength(); a++)
                    {

                        Element courseElement=(Element)coursesNode.item(a);
                        NamedNodeMap attributes= courseElement.getAttributes();
                        if(attributes==null) Log.d(LOG_TAG,"Attributes null");
                        TimeTableItem course=new TimeTableItem(attributes.getNamedItem("courseCode").getNodeValue(),
                                attributes.getNamedItem("venue").getNodeValue(),
                                attributes.getNamedItem("timeStart").getNodeValue(),
                                attributes.getNamedItem("timeStop").getNodeValue(),
                                attributes.getNamedItem("level").getNodeValue()
                                ,attributes.getNamedItem("day").getNodeValue(),
                                collegeElement.getAttributes().getNamedItem("name").getNodeValue());


                        if(filter) {
                            if(userCourses!=null)
                                if (course.getLevel().equals(String.valueOf(level))
                                        && course.getDay().equals(day) && userCourses.contains(course.getCourseCode())) {
                                    items.add(course);
                                    Log.d(LOG_TAG, "Course added: " + course.toString());
                                } else {
                                    //  Log.d(LOG_TAG, course.getCourseCode() + " did not meet the filter requirements ");
                                }
                        }

                        else{
                            if (course.getLevel().equals(String.valueOf(level)) && course.getDay().equals(day)) {
                                items.add(course);
                                Log.d(LOG_TAG, "Course added: " + course.toString());
                            } else {
                                //Log.d(LOG_TAG, course.getCourseCode() + " did not meet the requirements ");
                            }
                        }



                    }

                    Collections.sort(items);
                    return new TimeTableRecycler(items,context);



                }
                catch (Exception e)

                {
                    e.printStackTrace();
                    //     Toast.makeText(context, context.getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();

                    return   null;
                }

            }

            @Override
            protected void onPostExecute(TimeTableRecycler timeTableRecycler) {
                recyclerView.setAdapter(timeTableRecycler);
            }
        }.execute(null,null,null);


    }


    public void addPeriod()
    {
        Dialog.Builder builder;

        builder= new SimpleDialog.Builder(R.style.SimpleDialog){
            EditText etCourseVenue;
            Spinner spCourseCode;
            Button btTimeStart,btTimeStop;
            @Override
            protected void onBuildDone(Dialog dialog) {
                dialog.layoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                spCourseCode=(Spinner) dialog.findViewById(R.id.spnCourseCode);
                List<String> userCourses=new AcademicsManager(context).getPlainUserCourses();
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(TimeTable2.getInstance(),R.layout.row_spn,userCourses);
                spCourseCode.setAdapter(arrayAdapter);
                btTimeStart=(Button)dialog.findViewById(R.id.bt_time_start);
                btTimeStop=(Button)dialog.findViewById(R.id.bt_time_stop);
                etCourseVenue=(EditText)dialog.findViewById(R.id.et_course_venue);

                btTimeStart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        handleTime(btTimeStart,"8:00");
                    }
                });

                btTimeStop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        handleTime(btTimeStop,"10:00");
                    }
                });

            }

            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                String newTimeStart=btTimeStart.getText().toString();
                String newTimeStop=btTimeStop.getText().toString();
                String newCourseCode= spCourseCode.getSelectedItem().toString();
                String newCourseVenue=etCourseVenue.getText().toString();
                //TODO check that all the values are valid

                if(TextUtils.isEmpty(newCourseCode) || TextUtils.isEmpty(newCourseVenue) || TextUtils.isEmpty(newTimeStart) || TextUtils.isEmpty(newCourseVenue))
                {
                    manager.shortToast(R.string.field_missing);
                    return;
                }
                String[] newDetails=new String[6];
                newDetails[0]=newCourseCode;
                newDetails[1]=newTimeStart;
                newDetails[2]=newTimeStop;
                newDetails[3]=newCourseVenue;
                newDetails[4]=String.valueOf(level);
                newDetails[5]=day;


                addPeriod(newDetails,manager.getCollege());
                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };

        builder.title("Add period")
                .positiveAction(context.getString(R.string.save_changes))
                .negativeAction(context.getString(R.string.cancel))
                .contentView(R.layout.edit_timetable_dialog);
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(TimeTable2.getInstance().getSupportFragmentManager(), null);




    }

    public  void editPeriod(String period_id, final String college)
    {
        final String[] periodDetails= period_id.split("_");



        Dialog.Builder builder;

        builder= new SimpleDialog.Builder(R.style.SimpleDialog){
            EditText etCourseVenue;
            Button btTimeStart,btTimeStop;
            String courseCode,timeStart,timeStop,courseVenue,level,day;
            Spinner spCourseCode;
            @Override
            protected void onBuildDone(Dialog dialog) {
                dialog.layoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                courseCode = periodDetails[0];
                timeStart = periodDetails[1];
                timeStop = periodDetails[2];
                courseVenue = periodDetails[3];
                level = periodDetails[4];
                day = periodDetails[5];

                //etCourseCode=(EditText)dialog.findViewById(R.id.et_course_code);
                btTimeStart = (Button) dialog.findViewById(R.id.bt_time_start);
                btTimeStop = (Button) dialog.findViewById(R.id.bt_time_stop);
                etCourseVenue = (EditText) dialog.findViewById(R.id.et_course_venue);
                spCourseCode = (Spinner) dialog.findViewById(R.id.spnCourseCode);
                List<String> userCourses = new AcademicsManager(context).getPlainUserCourses();
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(TimeTable2.getInstance(), R.layout.row_spn, userCourses);
                spCourseCode.setAdapter(arrayAdapter);
                int position=0;

                for (int a = 0; a < userCourses.size(); a++)
                {
                    if(userCourses.get(a).equals(courseCode))
                    {
                        position=a;
                        break;
                    }
                }

                spCourseCode.setSelection(position);
                btTimeStart.setText(timeStart);
                btTimeStop.setText(timeStop);
                etCourseVenue.setText(courseVenue);

                btTimeStart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    handleTime(btTimeStart,timeStart);

                    }
                });

                btTimeStop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                      handleTime(btTimeStop,timeStop);
                    }
                });

            }

            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                String newTimeStart=btTimeStart.getText().toString();
                String newTimeStop=btTimeStop.getText().toString();
                String newCourseCode= (String)spCourseCode.getSelectedItem();
                String newCourseVenue=etCourseVenue.getText().toString();
                if(TextUtils.isEmpty(newCourseCode) || TextUtils.isEmpty(newCourseVenue) || TextUtils.isEmpty(newTimeStart) || TextUtils.isEmpty(newCourseVenue))
                {
                    manager.shortToast(R.string.field_missing);
                    return;
                } else
                    if(!newTimeStart.equals(timeStart) || !newTimeStop.equals(timeStop) || !newCourseCode.equals(courseCode) || !newCourseVenue.equals(courseVenue))
                    {
                        String[] newDetails=new String[6];
                        newDetails[0]=newCourseCode;
                        newDetails[1]=newTimeStart;
                        newDetails[2]=newTimeStop;
                        newDetails[3]=newCourseVenue;
                        newDetails[4]=day;
                        newDetails[5]=level;
                        editPeriod(periodDetails,newDetails,college);
                    }

                else{
                        manager.shortToast("Nothing was changed");
                    }
                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };

        builder.title("Edit period")
                .positiveAction("Save changes")
                .negativeAction(context.getString(R.string.cancel))
                .contentView(R.layout.edit_timetable_dialog);
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(TimeTable2.getInstance().getSupportFragmentManager(), null);







    }


    private void addPeriod(String [] periodDetails,String college)
    {
        String courseCode,timeStart,timeStop,courseVenue,level,day;
        courseCode=periodDetails[0];
        if(!courseCode.contains(" ") && courseCode.length()==6){ courseCode=courseCode.substring(0,3)+" "+courseCode.substring(3,6);}
        timeStart=periodDetails[1];
        timeStop=periodDetails[2];
        courseVenue=periodDetails[3];
        level=periodDetails[4];
        day=periodDetails[5];



        try {
            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();
            //Get the DOM Builder
            DocumentBuilder builder = factory.newDocumentBuilder();
            if (manager.isCustomTimeTable()) {
                FileInputStream imageInput;
                imageInput = context.openFileInput(Const.TIMETABLE_XML);
                documentEditing = builder.parse(imageInput);
            } else {
                documentEditing = builder.parse(context.getResources().openRawResource(R.raw.timetable));
            }

            NodeList colleges = documentEditing.getElementsByTagName("college");
            Element collegeElement = findElement(college, colleges);
            Element age = documentEditing.createElement("course");
            age.setAttribute(Const.COURSE_CODE,courseCode);
            age.setAttribute(Const.VENUE,courseVenue);
            age.setAttribute(Const.TIME_START,timeStart);
            age.setAttribute(Const.TIME_STOP,timeStop);
            age.setAttribute(Const.LEVEL,level);
            age.setAttribute(Const.DAY,day);
            collegeElement.appendChild(age);

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(documentEditing);
            FileOutputStream fileOutputStream =context.openFileOutput(Const.TIMETABLE_XML, Context.MODE_PRIVATE);
            StreamResult result = new StreamResult(fileOutputStream);
            transformer.transform(source, result);
            manager.setCustomTimetable(true);
            manager.shortToast("The period has been successfully added");
            TimeTable2.getInstance().reset();
        }

        catch (Exception e){
            e.printStackTrace();
            manager.shortToast(R.string.error_unable_to_change_timetable);
        }
    }
    private void editPeriod(String[] periodDetails,String[]newDetails,String college)
    {
        String courseCode,timeStart,timeStop,courseVenue,level,day;
        String newCourseCode,newTimeStart,newTimeStop,newCourseVenue,newCourseLevel,newCourseDay;
        courseCode=periodDetails[0];
        timeStart=periodDetails[1];
        timeStop=periodDetails[2];
        courseVenue=periodDetails[3];
        level=periodDetails[4];
        day=periodDetails[5];


        newCourseCode=newDetails[0];
        newTimeStart=newDetails[1];
        newTimeStop=newDetails[2];
        newCourseVenue=newDetails[3];
        newCourseLevel=newDetails[4];
        newCourseDay=newDetails[5];


        try{
          Element course=getCourse(courseCode,timeStart,timeStop,courseVenue,day,college);
            if(course==null)
            {
                manager.shortToast(R.string.error_unable_to_change_timetable);
                return;
            }

            // update course attribute
            NamedNodeMap attr = course.getAttributes();
            Node courseCodeAttr=attr.getNamedItem(Const.COURSE_CODE);
            courseCodeAttr.setTextContent(newCourseCode);
            Node timeStartAttr = attr.getNamedItem(Const.TIME_START);
            timeStartAttr.setTextContent(newTimeStart);
            Node timeStopAttr=attr.getNamedItem(Const.TIME_STOP);
            timeStopAttr.setTextContent(newTimeStop);
            Node venueAttr=attr.getNamedItem(Const.VENUE);
            venueAttr.setTextContent(newCourseVenue);

            // append a new node to course
            /*
            Element age = doc.createElement("age");
            age.appendChild(doc.createTextNode("28"));
            course.appendChild(age);*/
            /*
            // loop the course child node
            NodeList list = course.getChildNodes();

            for (int i = 0; i < list.getLength(); i++) {

                Node node = list.item(i);

                // get the salary element, and update the value
                if ("salary".equals(node.getNodeName())) {
                    node.setTextContent("2000000");
                }

                //remove firstname
                if ("firstname".equals(node.getNodeName())) {
                    course.removeChild(node);
                }

            }
            */

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(documentEditing);
            FileOutputStream fileOutputStream =context.openFileOutput("timetable.xml", Context.MODE_PRIVATE);
            StreamResult result = new StreamResult(fileOutputStream);
            transformer.transform(source, result);
            manager.setCustomTimetable(true);
            manager.shortToast("The timetable has been successfully changed");
            TimeTable2.getInstance().reset();
        } catch (TransformerException | IOException e) {
            e.printStackTrace();
        }
    }

    public void deletePeriod(String period_id,String college)
    {
        String[] periodDetails=period_id.split("_");
        String courseCode,timeStart,timeStop,courseVenue,level,day;
        courseCode=periodDetails[0];
        Log.d(LOG_TAG,"Course code "+courseCode);
        timeStart=periodDetails[1];
        timeStop=periodDetails[2];
        courseVenue=periodDetails[3];
        level=periodDetails[4];
        day=periodDetails[5];
        Element course=getCourse(courseCode, timeStart, timeStop, courseVenue, day, college);

        if(course==null)
        {
            manager.shortToast("Unable to delete item");
        }
        NodeList colleges = documentEditing.getElementsByTagName("college");
        Element collegeElement = findElement(college, colleges);
        collegeElement.removeChild(course);


        try{
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(documentEditing);
        FileOutputStream fileOutputStream =context.openFileOutput(Const.TIMETABLE_XML, Context.MODE_PRIVATE);
        StreamResult result = new StreamResult(fileOutputStream);
        transformer.transform(source, result);
        manager.setCustomTimetable(true);
        manager.shortToast("The course has been deleted");
        TimeTable2.getInstance().reset();
        }
        catch(Exception e)
        {
            e.printStackTrace();


        }


    }

    public void importTimeTable(JabuWingsActivity jabuWingsActivity)
    {

    }
    public  void exportTimeTable(JabuWingsActivity jabuWingsActivity)
    {
        final DialogWizard dialogWizard= new DialogWizard(jabuWingsActivity);
        dialogWizard.showConfirmationDialog("Export to File", "Exporting will save the timetable in a file on your phone storage", new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                continueExport(dialogWizard);
            }
        },null);
    }

    private void continueExport(DialogWizard dialogWizard)
    {
        try{
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(getDocument(manager,true));
        File sd = Environment.getExternalStorageDirectory();
        File dir;
        dir = new File(sd.getAbsolutePath()+Const.PUBLIC_FILE_DIRECTORY);
        dir.mkdirs();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        File file =new File(dir,"TimeTable_"+timeStamp+".xml");
            FileOutputStream fos = new FileOutputStream
                    (file);
        StreamResult result = new StreamResult(fos);
        transformer.transform(source, result);
        fos.close();
        dialogWizard.showSimpleDialog("Export Successful","The timetable has been successfully exported to "+file.getPath());
        }
        catch (Exception e)
        {
            dialogWizard.showSimpleDialog("Oops","An error occurred while exporting the timetable. Possible error is your phone storage is unavailable. The error was: "+e.getMessage());
            e.printStackTrace();
        }
    }
    private Element getCourse(String courseCode,String timeStart,String timeStop,String courseVenue,String day,String college)
    {
        try {


                DocumentBuilderFactory factory =
                        DocumentBuilderFactory.newInstance();
                //Get the DOM Builder
                DocumentBuilder builder = factory.newDocumentBuilder();
                if (manager.isCustomTimeTable()) {
                    FileInputStream imageInput;
                    imageInput = context.openFileInput(Const.TIMETABLE_XML);
                    documentEditing = builder.parse(imageInput);
                } else {
                    documentEditing = builder.parse(context.getResources().openRawResource(R.raw.timetable));
                }


            NodeList colleges = documentEditing.getElementsByTagName("college");
            Element collegeElement = findElement(college, colleges);
            NodeList courses = collegeElement.getElementsByTagName("course");

            Log.d(LOG_TAG, "Courses length " + courses.getLength());

            for (int i = 0; i < courses.getLength(); i++) {
                Element courseElement = (Element) courses.item(i);

                String nodeCourseCode = courseElement.getAttribute(Const.COURSE_CODE);

                if (nodeCourseCode.equals(courseCode)) {
                    String nodeTimeStart = courseElement.getAttribute(Const.TIME_START);
                    String nodeTimeStop = courseElement.getAttribute(Const.TIME_STOP);
                    String nodeCourseVenue = courseElement.getAttribute(Const.VENUE);
                    String nodeCourseDay = courseElement.getAttribute(Const.DAY);
                    Log.d(LOG_TAG, "TimeStart " + timeStart + " " + "Day " + nodeCourseDay);
                    if (nodeTimeStart.equals(timeStart) && nodeTimeStop.equals(timeStop) && nodeCourseVenue.equals(courseVenue) && nodeCourseDay.equals(day)) {
                        Log.d(LOG_TAG, "Course successfully found " + nodeCourseCode);
                        return courseElement;
                    }

                } else {
                    Log.d(LOG_TAG, "false for " + nodeCourseCode);
                }


            }

            return null;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    private void handleTime(final Button timeButton, String time)
    {

        Dialog.Builder builder ;
        builder = new TimePickerDialog.Builder(new Time().getHour(time), 0){
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                TimePickerDialog dialog = (TimePickerDialog)fragment.getDialog();
                timeButton.setText(String.valueOf(dialog.getHour())+":00");
                if(dialog.getMinute()!=0)
                {
                    manager.shortToast("Timetable is hourly based, minutes discarded");
                }
                super.onPositiveActionClicked(fragment);


            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {

                super.onNegativeActionClicked(fragment);
            }
        };

        builder.positiveAction("OK")
                .negativeAction("CANCEL");
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(TimeTable2.getInstance().getSupportFragmentManager(), null);


    }


    private static Element findElement(String name,NodeList list)
    {
        Element element;
        Log.d(LOG_TAG, "Length of list "+list.getLength());
        for(int i=0; i<list.getLength(); i++)
        {
            element=(Element)list.item(i);
            if(element.getAttribute("name").equals(name))
            {
                Log.d(LOG_TAG, "Element "+name+" returned");
               return element;
            }
        }
        return null;
    }


}
