package com.jabuwings.views.timetable;

import com.jabuwings.management.Const;
import com.jabuwings.models.TimeTableItem;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Falade James on 12/3/2015 All Rights Reserved.
 */
class Handler extends DefaultHandler {

    List<TimeTableItem> courses = new ArrayList<>();
    TimeTableItem course = null;
    int college;

    public Handler(int college) {
        this.college = college;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        switch (qName) {

            case "course":
                String collegeName = "";
                switch (college) {
                    case 0:
                        collegeName = Const.AS;
                        break;
                    case 1:
                        collegeName = Const.ES;
                        break;
                    case 2:
                        collegeName = Const.HM;
                        break;
                    case 3:
                        collegeName = Const.LW;
                        break;
                    case 4:
                        collegeName = Const.MS;
                        break;
                    case 5:
                        collegeName = Const.NS;
                        break;
                    case 6:
                        collegeName = Const.SS;
                        break;


                }
                course = new TimeTableItem(attributes.getValue("courseCode"),
                        attributes.getValue("venue"),
                        attributes.getValue("timeStart"), attributes.getValue("timeStop"), attributes.getValue("level"),
                        attributes.getValue("day"), collegeName);
                courses.add(course);
                break;


        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
    }
}
