package com.jabuwings.views.timetable;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.Time;
import com.jabuwings.widgets.filter.adapter.MenuAdapter;
import com.jabuwings.widgets.filter.adapter.SimpleTextAdapter;
import com.jabuwings.widgets.filter.interfaces.OnFilterDoneListener;
import com.jabuwings.widgets.filter.interfaces.OnFilterItemClickListener;
import com.jabuwings.widgets.filter.typeview.SingleListView;
import com.jabuwings.widgets.filter.util.UIUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Falade James on 9/27/2016 All Rights Reserved.
 */
public class TimeTableAdapter implements MenuAdapter {
    Context context; OnFilterDoneListener onFilterDoneListener;
    String[] titles;
    Manager manager;
    public TimeTableAdapter(Context context, OnFilterDoneListener onFilterDoneListener, String[] titles)
    {
        this.context=context; this.onFilterDoneListener=onFilterDoneListener;this.titles=titles; this.manager=new Manager(context);

    }
    private View getFilterGroup() {

        SingleListView<String> singleListView = new SingleListView<String>(context)
                .adapter(new SimpleTextAdapter<String>(null, context) {
                    @Override
                    public String provideText(String string) {
                        return string;
                    }
                })
                .onItemClick(new OnFilterItemClickListener<String>() {
                    @Override
                    public void onItemClick(String item) {

                        if (onFilterDoneListener != null) {
                            onFilterDoneListener.onFilterDone(0,item, item);
                        }
                    }
                });

        List<String> list = new ArrayList<>();
        list.add("My Courses");
        list.add("All Courses");
        singleListView.setList(list, 0);
        return singleListView;
    }

    private View getLevelGroup() {

        SingleListView<String> singleListView = new SingleListView<String>(context)
                .adapter(new SimpleTextAdapter<String>(null, context) {
                    @Override
                    public String provideText(String string) {
                        return string;
                    }
                })
                .onItemClick(new OnFilterItemClickListener<String>() {
                    @Override
                    public void onItemClick(String item) {

                        if (onFilterDoneListener != null) {
                            onFilterDoneListener.onFilterDone(1, item, item);
                        }
                    }
                });

        List<String> list = new ArrayList<>();
        list.add("100");
        list.add("200");
        list.add("300");
        list.add("400");
        list.add("500");

        String level= manager.getLevel();
        int levelPosition=0;
        if(level!=null)
        {
            switch (level) {
                case "100":
                    levelPosition=0;
                    break;
                case "200":
                    levelPosition=1;
                    break;
                case "300":
                    levelPosition=2;
                    break;
                case "400":
                    levelPosition=3;
                    break;
                case "500":
                    levelPosition=4;
                    break;
            }

        }

        singleListView.setList(list, levelPosition);
        return singleListView;
    }

    private View getCollegeGroup() {

        SingleListView<String> singleListView = new SingleListView<String>(context)
                .adapter(new SimpleTextAdapter<String>(null, context) {
                    @Override
                    public String provideText(String string) {
                        return string;
                    }
                })
                .onItemClick(new OnFilterItemClickListener<String>() {
                    @Override
                    public void onItemClick(String item) {

                        if (onFilterDoneListener != null) {
                            onFilterDoneListener.onFilterDone(2, item, item);
                        }
                    }
                });

        String college = manager.getCollege();
        int collegePosition =0;
        if(college!=null)
            switch (college)
            {
                case Const.AGRICULTURAL_SCIENCES:
                    collegePosition=0;
                    break;
                case Const.ENVIRONMENTAL_SCIENCES:
                    collegePosition=1;
                    break;
                case Const.HEALTH_SCIENCES:
                    collegePosition=2;
                    break;
                case Const.HUMANITIES:
                    collegePosition=3;
                    break;
                case Const.LAW:
                    collegePosition=4;
                    break;
                case Const.MANAGEMENT_SCIENCES:
                    collegePosition=5;
                    break;
                case Const.NATURAL_SCIENCES:
                    collegePosition=6;
                    break;
                case Const.SOCIAL_SCIENCES:
                    collegePosition=7;
                    break;


            }

        List<String> list = Arrays.asList(context.getResources().getStringArray(R.array.colleges_names));

        singleListView.setList(list, collegePosition);
        return singleListView;
    }

    private View getDayGroup() {

        SingleListView<String> singleListView = new SingleListView<String>(context)
                .adapter(new SimpleTextAdapter<String>(null, context) {
                    @Override
                    public String provideText(String string) {
                        return string;
                    }
                })
                .onItemClick(new OnFilterItemClickListener<String>() {
                    @Override
                    public void onItemClick(String item) {

                        if (onFilterDoneListener != null) {
                            onFilterDoneListener.onFilterDone(3, item,item);
                        }
                    }
                });

        List<String> list = new ArrayList<>();
        list.add("Monday");
        list.add("Tuesday");
        list.add("Wednesday");
        list.add("Thursday");
        list.add("Friday");


        singleListView.setList(list,  new Time().getDay()-2);
        return singleListView;
    }
    @Override
    public int getMenuCount() {
        return titles.length;
    }

    @Override
    public String getMenuTitle(int position) {
        return titles[position];
    }

    @Override
    public int getBottomMargin(int position) {
        return UIUtil.dp(context, 140);
    }

    @Override
    public View getView(int position, FrameLayout parentContainer) {
        View view = parentContainer.getChildAt(position);

        switch (position) {
            case 0:
                view = getFilterGroup();
                break;
            case 1:
                view = getLevelGroup();
                break;
            case 2:
                view = getCollegeGroup();
                break;
            case 3:
                // view = createDoubleGrid();
                view = getDayGroup();
                break;
        }

        return view;
    }
}
