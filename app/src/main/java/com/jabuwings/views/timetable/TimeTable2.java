package com.jabuwings.views.timetable;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListItem;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.Const;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.jabuwings.widgets.button.FloatingActionButton;
import com.jabuwings.widgets.button.FloatingActionsMenu;
import com.jabuwings.widgets.filter.DropDownMenu;
import com.jabuwings.widgets.filter.adapter.SimpleTextAdapter;
import com.jabuwings.widgets.filter.interfaces.OnFilterDoneListener;
import com.jabuwings.widgets.filter.interfaces.OnFilterItemClickListener;
import com.jabuwings.widgets.filter.typeview.SingleListView;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseConfig;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class TimeTable2 extends JabuWingsActivity  implements OnFilterDoneListener{

    @InjectView(R.id.mFilterContentView)
    RecyclerView rvTimeTable;
    @InjectView(R.id.dropDownMenu)
    DropDownMenu dropDownMenu;
    @InjectView(R.id.fabTimeTable)
    FloatingActionsMenu fab;
    TimeKeeper timeKeeper;
    static TimeTable2 instance;
    DialogWizard dialogWizard;


    Time time; String defaultLevel="100"; int college;public String collegeName; String day;boolean filter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_table2);
        instance=this;
        setUpToolbar(null,R.string.title_activity_time_table);
        ButterKnife.inject(this);
        rvTimeTable.setLayoutManager(new LinearLayoutManager(this));
        time = new Time();
        dialogWizard= new DialogWizard(TimeTable2.this);
        if(manager.getLevel()!=null)
        defaultLevel=manager.getLevel();
        college=getCollege(null);
        collegeName=manager.getCollege();
        day=time.getTimetableDay();
        filter=true;
        timeKeeper= new TimeKeeper(day,college,defaultLevel,filter,this);
        timeKeeper.showTimeTable(rvTimeTable);
        initFilterDropDownView();
        initFab();
        update();

    }

    private void update()
    {
        ParseConfig config = ParseConfig.getCurrentConfig();
        //check for explicit update intent
        boolean update = getIntent().getBooleanExtra(Const.UPDATE,false);
        if(update)
        {
            proceedUpdate();
        }
        else{
            if(config.getString(Const.TIMETABLE_VERSION)!=null && !config.getString(Const.TIMETABLE_VERSION).equals(manager.getTimeTableVersion()))
            {
                proceedUpdate();
            }
        }

    }

    private void proceedUpdate()
    {
        new SweetAlertDialog(TimeTable2.this).setTitleText("New Version").setContentText("An update is available for the TimeTable. Please note that all current changes to this current timetable will hence be reverted").setConfirmText(getString(R.string.download)).setCancelText(getString(R.string.cancel)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
                final SweetAlertDialog progressDialog= dialogWizard.showProgress("Please wait","Updating timetable",true);
                ParseQuery<ParseObject> query= new ParseQuery<>(Const.APP_CLASS);
                query.whereEqualTo(Const.NAME,"timetable");

                query.getFirstInBackground(new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        if(e==null)
                        {
                            parseObject.getParseFile(Const.FILE).getDataInBackground(new GetDataCallback() {
                                @Override
                                public void done(byte[] bytes, ParseException e) {
                                    progressDialog.dismiss();
                                    if(e==null)
                                    {
                                        try{
                                            FileOutputStream fileOutputStream =context.openFileOutput(Const.TIMETABLE_XML, Context.MODE_PRIVATE);
                                            fileOutputStream.write(bytes);
                                            manager.setCustomTimetable(true);
                                            dialogWizard.showSuccessDialog("Timetable updated","The timetable has been successfully updated.");
                                            reset();

                                        }
                                        catch (IOException i)
                                        {
                                            dialogWizard.showSimpleDialog("Oops","For some reasons, we were unable to save the new timetable. Please make sure your phone storage is accessible and try again");
                                        }

                                    }
                                    else{
                                        ErrorHandler.handleError(TimeTable2.this,e);
                                    }
                                }
                            });

                        }
                        else{
                            progressDialog.dismiss();
                            ErrorHandler.handleError(context,e);

                        }
                    }
                });

            }
        }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
            }
        }).show();
    }


    public void reset()
    {
        timeKeeper= new TimeKeeper(day,college,defaultLevel,filter,this);
        timeKeeper.showTimeTable(rvTimeTable);
    }
    public static TimeTable2 getInstance()
    {
        return instance;

    }

    private void initFab()
    {
        FloatingActionButton fabAddCourse= new FloatingActionButton(this);
        fabAddCourse.setTitle("Add course");
        fabAddCourse.setSize(FloatingActionButton.SIZE_MINI);
        fabAddCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.collapse();
                addCourse();
            }
        });
        fab.addButton(fabAddCourse);


        FloatingActionButton fabExportImport= new FloatingActionButton(this);
        fabExportImport.setTitle("Export/Import");
        fabExportImport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.collapse();
                exportImport();
            }
        });
        fab.addButton(fabExportImport);

        FloatingActionButton fabTimeTableInfo= new FloatingActionButton(this);
        fabTimeTableInfo.setTitle("Timetable info");
        fabTimeTableInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.collapse();
                timetableInfo();
            }
        });
        fab.addButton(fabTimeTableInfo);



        FloatingActionButton fabDelete= new FloatingActionButton(this);
        fabDelete.setTitle("Reset timetable");
        fabDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.collapse();

                new MaterialDialog.Builder(TimeTable2.this).title("Reset timetable").content("Resetting the timetable will revert all the changes you have made to this timetable. This action is irreversible").positiveText(R.string.confirm).negativeText(R.string.cancel).onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if(manager.isCustomTimeTable())
                        {
                            deleteFile(Const.TIMETABLE_XML);
                            manager.setCustomTimetable(false);

                            manager.shortToast("Timetable reset successful");
                        }

                        else{
                            manager.shortToast("No need to reset timetable.");
                        }

                    }
                }).show();




            }
        });
        fab.addButton(fabDelete);
    }




    private void addCourse()
    {
        timeKeeper.addPeriod();
    }
    private void timetableInfo()
    {

        TimeKeeper timeKeeper =new TimeKeeper(context);
        dialogWizard.showSimpleDialog("Timetable Info","Version : "+timeKeeper.getTimetableVersion()+"\n Semester: "+timeKeeper.getTimeTableSemester());

    }

    private void exportImport()
    {
        String[] options={"Import to file","Export to file"};

        new MaterialDialog.Builder(getInstance())
                .title("Export/Import")
                .items(options)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        TimeKeeper timeKeeper = new TimeKeeper(getInstance());
                        switch (which)
                        {

                            case 0:
                                timeKeeper.importTimeTable(getInstance());
                                break;
                            case 1:
                                timeKeeper.exportTimeTable(getInstance());
                                break;
                        }
                    }
                })
                .show();

    }
    public  String getConfiguration()
    {
        String s="\n";
        String filter= this.filter? "Mine" : "All";
        return "Day: "+day+s+
                "Filter: "+filter+s+
                "Level: "+defaultLevel+s+
                "College: "+collegeName;
    }

    private int getCollege(String c)
    {
        int collegePosition=0;
        String college= c==null? manager.getCollege() : c;

        if(college!=null)
            switch (college)
            {
                case Const.AGRICULTURAL_SCIENCES:
                    collegePosition=0;
                    break;
                case Const.ENVIRONMENTAL_SCIENCES:
                    collegePosition=1;
                    break;
                case Const.HEALTH_SCIENCES:
                    collegePosition=2;
                    break;
                case Const.HUMANITIES:
                    collegePosition=3;
                    break;
                case Const.LAW:
                    collegePosition=4;
                    break;
                case Const.MANAGEMENT_SCIENCES:
                    collegePosition=5;
                    break;
                case Const.NATURAL_SCIENCES:
                    collegePosition=6;
                    break;
                case Const.SOCIAL_SCIENCES:
                    collegePosition=7;
                    break;

            }
        return collegePosition;
    }

    private void initFilterDropDownView() {
        String[] titleList = new String[]{"Filter", "Level", "College", "Day"};
        dropDownMenu.setMenuAdapter(new TimeTableAdapter(this, this, titleList));
    }


    @Override
    public void onFilterDone(int position, String positionTitle, String urlValue) {
        dropDownMenu.setPositionIndicatorText(position,positionTitle);

        switch (position)
        {
            case 0:
                filter = urlValue.equals("Mine");
                break;
            case 1:
                defaultLevel=urlValue;
                break;
            case 2:
                college=getCollege(urlValue);
                collegeName=urlValue;
                break;
            case 3:
                day=urlValue.substring(0,3).toUpperCase();
                break;
        }
        timeKeeper= new TimeKeeper(day,college,defaultLevel,filter,this);
        timeKeeper.showTimeTable(rvTimeTable);



    dropDownMenu.close();


    }
}
