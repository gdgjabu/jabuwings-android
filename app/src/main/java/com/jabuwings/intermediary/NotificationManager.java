package com.jabuwings.intermediary;

/**
 * Created by Falade James on 25/07/2015.
 */

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Icon;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.RemoteInput;
import android.support.v4.app.TaskStackBuilder;
import android.text.TextUtils;

import com.jabuwings.R;
import static com.jabuwings.management.Const.*;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.views.main.Domicile;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NotificationManager {

    private String TAG = NotificationManager.class.getSimpleName();
    Manager manager;
    private Context mContext;
    private static List<String> notifications= new ArrayList<>();
    int icon = R.drawable.not_icon;
    public static String PERSONAL_MESSAGES_CHANNEL_KEY="personal_messages";
    public static String HUB_MESSAGES_CHANNEL_KEY="hub_messages";
    public static String GENERAL_NOTIFICATIONS_CHANNEL_KEY="general_notifications";
    private static final String KEY_PERSONAL_MESSAGE_REPLY = "personal_message_reply";
    

    public static void clearNotifications()
    {
        notifications.clear();
    }
    public NotificationManager(Context mContext) {
        this.mContext = mContext;
        manager= new Manager(mContext);

    }

  /*  @TargetApi(Build.VERSION_CODES.O)
    public void setUpNotificationChannels()
    {
        android.app.NotificationManager mNotificationManager =
                ( android.app.NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
// The id of the channel.
        String id = "my_channel_01";
// The user-visible name of the channel.
        CharSequence name = mContext.getString(R.string.channel_name);
// The user-visible description of the channel.
        String description = getString(R.string.channel_description);
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);
// Configure the notification channel.
        mChannel.setDescription(description);
        mChannel.enableLights(true);
// Sets the notification light color for notifications posted to this
// channel, if the device supports this feature.
        mChannel.setLightColor(Color.RED);
        mChannel.enableVibration(true);
        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        mNotificationManager.createNotificationChannel(mChannel);
    }*/
    public void showNotification(String title, String alert, Intent intent)
    {
        int notificationId=intent.getIntExtra(NOT_ID,0);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
        stackBuilder.addParentStack(Domicile.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                mContext);


        Notification notification = mBuilder.setSmallIcon(icon).setTicker(alert)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(alert))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentText(alert)
                .build();

        notification.flags=Notification.FLAG_SHOW_LIGHTS;
        notification.ledARGB= Color.MAGENTA;
        notification.ledOnMS=1000;
        notification.ledOffMS=300;





        android.app.NotificationManager mNotificationManager =
                (android.app.NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        mNotificationManager.notify(notificationId, notification);


    }
    public void showNotificationMessage(String title, String message, Intent intent) {
        if(!manager.isBlocked()&& !manager.isSessionBlocked())
        {
            intent.putExtra(Const.CLEAR_NOTIFICATIONS,true);
            notifications.add(message);


            // Check for empty push message
            if (TextUtils.isEmpty(message))
                return;
            if (isAppInBackground(mContext)) {
                // notification icon

                int mNotificationId = Const.MESSAGE_NOTIFICATION;


                NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

                inboxStyle.setBigContentTitle("JabuWings");
                inboxStyle.setSummaryText("Messages");

                for(int i= 0; i<=notifications.size()-1; i++)
                {
                    inboxStyle.addLine(notifications.get(i));
                }








// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
// Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(Domicile.class);
// Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(intent);
                PendingIntent resultPendingIntent =
                        stackBuilder.getPendingIntent(
                                0,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                Uri url= Uri.parse("android.resource://" + mContext.getPackageName() + "/" + R.raw.msg_tone);

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        mContext);

                // Key for the string that's delivered in the action's intent.

                String replyLabel = mContext.getResources().getString(R.string.type);
               
                if(Build.VERSION.SDK_INT>=24)
                    notifyWithReply(resultPendingIntent,mNotificationId,inboxStyle,title,message);
                else{
                    Notification notification = mBuilder.setSmallIcon(icon).setTicker(title)
                            .setAutoCancel(true)
                            .setContentTitle(title)
                            .setStyle(inboxStyle)
                            .setNumber(notifications.size())
                            .setContentIntent(resultPendingIntent)
                            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                            //.setLargeIcon(getEntityPicture(intent.getStringExtra(Const.SENDER)))
                            .setContentText(message)
                            .build();





                    android.app.NotificationManager mNotificationManager =
                            (android.app.NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
                    mNotificationManager.notify(mNotificationId, notification);
                }


            } else {
//                intent.putExtra("title", title);
//                intent.putExtra("message", message);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                mContext.startActivity(intent);
            }
        }

    }

    private  void notifyWithReply(PendingIntent pendingIntent, int mNotificationId, NotificationCompat.InboxStyle inboxStyle, String title,String message)
    {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                mContext);
        
        RemoteInput remoteInput = new RemoteInput.Builder(KEY_PERSONAL_MESSAGE_REPLY)
                .setLabel(mContext.getString(R.string.reply))
                .build();

        // Create the reply action and add the remote input.
        NotificationCompat.Action action =
                new NotificationCompat.Action.Builder(R.drawable.ic_send,
                        mContext.getString(R.string.reply), pendingIntent)
                        .addRemoteInput(remoteInput)
                        .build();

        Notification notification = mBuilder.setSmallIcon(icon).setTicker(title)
                .setAutoCancel(true)
                .setContentTitle(title)
                .addAction(action)
                .setStyle(inboxStyle)
                .setNumber(notifications.size())
                .setContentIntent(pendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                //.setLargeIcon(getEntityPicture(intent.getStringExtra(Const.SENDER)))
                .setContentText(message)
                .build();

        android.app.NotificationManager mNotificationManager =
                (android.app.NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        mNotificationManager.notify(mNotificationId, notification);
    }
    /**
     * Method checks if the app is in background or not
     *
     * @param context
     * @return
     */
    public static boolean isAppInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    private Bitmap getEntityPicture(String username)
    {
        //TODO change to fileName
      String  fileName=null;
        if(fileName!=null)

        try {
            FileInputStream imageInput;
            imageInput = mContext.openFileInput(fileName);
            return BitmapFactory.decodeStream(imageInput);


        } catch (IOException |OutOfMemoryError e) {
            e.printStackTrace();
            return BitmapFactory.decodeResource(mContext.getResources(),R.drawable.default_avatar);
        }

        else{
            return BitmapFactory.decodeResource(mContext.getResources(),R.drawable.default_avatar);
        }
    }
}
