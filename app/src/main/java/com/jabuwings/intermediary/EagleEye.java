package com.jabuwings.intermediary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.jabuwings.intelligence.LectureService;
import com.jabuwings.management.Manager;

/**
 * Created by Falade James on 6/16/2016 All Rights Reserved.
 */
public class EagleEye extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        Manager manager= new Manager(context);

        if(manager.isTimetableNotificationEnabled())
        if (action.equals(Intent.ACTION_TIME_CHANGED) ||
                action.equals(Intent.ACTION_TIMEZONE_CHANGED))
        {
            context.stopService(new Intent(context, LectureService.class));
            context.startService(new Intent(context,LectureService.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }
}
