package com.jabuwings.intermediary;

/**
 * Created by Falade James on 25/07/2015.
 *
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.database.realm.HubMessage;
import com.jabuwings.database.realm.Message;
import com.jabuwings.encryption.Friendship;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.intelligence.MessageService;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.JabuWingsHub;
import com.jabuwings.models.MatricNo;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.Parrot;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.FriendRequests;
import com.jabuwings.views.main.Domicile;
import com.jabuwings.views.main.HubChat;
import com.jabuwings.views.main.Hubs;
import com.jabuwings.views.main.Notifications;
import com.jabuwings.views.timetable.TimeTable2;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParsePushBroadcastReceiver;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;

import static com.jabuwings.management.Const.ALERT;
import static com.jabuwings.management.Const.CHATS_CLASS;
import static com.jabuwings.management.Const.DEPARTMENT;
import static com.jabuwings.management.Const.FILE;
import static com.jabuwings.management.Const.FRIEND;
import static com.jabuwings.management.Const.FRIENDS_RADAR;
import static com.jabuwings.management.Const.FRIEND_AUTHENTICATOR;
import static com.jabuwings.management.Const.FROM;
import static com.jabuwings.management.Const.HUBS_CHAT_CLASS;
import static com.jabuwings.management.Const.HUB_ID;
import static com.jabuwings.management.Const.ID;
import static com.jabuwings.management.Const.LEVEL;
import static com.jabuwings.management.Const.MATRIC_NO;
import static com.jabuwings.management.Const.MESSAGE_DELIVERY_SERVICE;
import static com.jabuwings.management.Const.MESSAGE_FAILED;
import static com.jabuwings.management.Const.MESSAGE_SENT;
import static com.jabuwings.management.Const.META;
import static com.jabuwings.management.Const.MSG;
import static com.jabuwings.management.Const.MSG_ID;
import static com.jabuwings.management.Const.NAME;
import static com.jabuwings.management.Const.NEGATIVE;
import static com.jabuwings.management.Const.NOTIFICATION_INTENT;
import static com.jabuwings.management.Const.NOT_ID;
import static com.jabuwings.management.Const.OBJECT_ID;
import static com.jabuwings.management.Const.PENDING;
import static com.jabuwings.management.Const.PIN;
import static com.jabuwings.management.Const.POSITIVE;
import static com.jabuwings.management.Const.RECEIVER;
import static com.jabuwings.management.Const.RELATIONSHIP_MEDIATOR;
import static com.jabuwings.management.Const.REMARK;
import static com.jabuwings.management.Const.SENDER;
import static com.jabuwings.management.Const.STATUS;
import static com.jabuwings.management.Const.TIME;
import static com.jabuwings.management.Const.TITLE;
import static com.jabuwings.management.Const.TO;
import static com.jabuwings.management.Const.TYPE;
import static com.jabuwings.management.Const.TYPING_WATCHDOG;
import static com.jabuwings.management.Const.UPDATE;
import static com.jabuwings.management.Const.USER;
import static com.jabuwings.management.Const.USERNAME;


/**
 * This class is responsible for receiving all push messages and notifications from the server.
 * It also communicates with the server to send messages and other network manipulations.
 */
public class HotSpot extends ParsePushBroadcastReceiver{
    private static final String  TAG = HotSpot.class.getSimpleName();
    private NotificationManager notificationManager;
    Manager manager;
    DatabaseManager helper;

    private Intent intent;

    JSONObject json;
   static Time time;
    private static boolean sent;

    public HotSpot() {
        super();

    }


    @Override
    protected void onPushReceive(Context context, Intent intent) {
        time = new Time();
        helper = new DatabaseManager(context);
        manager=new Manager(context);
        if (intent == null || manager.isBlocked() || !manager.isUserEmailVerified())
            return;

        try {
                json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
            Log.i(TAG, "Push received: " + json); //TODO remove for production
//            manager.shortToast(json.toString());
            this.intent = intent;

            String intendedReceiver;
            int not_id = json.getInt(NOT_ID);





                switch (not_id){
                    // A friend request is being requested or a request has been accepted
                    case 333777:
                        intendedReceiver= json.getString(TO);
                        if(intendedReceiver!=null)
                        {
                            if(!intendedReceiver.equals(manager.getUsername()))
                                return;
                        }
                        handleRequest333777(json,context);
                        break;
                    // A new message from a friend
                    case 677774:
                        intendedReceiver= json.getString(TO);
                        if(intendedReceiver!=null)
                        {
                            if(!intendedReceiver.equals(manager.getUsername()))
                                return;
                        }
//                        handleRequest677774(json,context);
                        break;
                    // A message Retraction from a friend
                    case 738728:
                        intendedReceiver= json.getString(TO);
                        if(intendedReceiver!=null)
                        {
                            if(!intendedReceiver.equals(manager.getUsername()))
                                return;
                        }
                        handleRequest738728(json,context);
                        break;
                    // A request has been made to set a message has either delivered or read
                    case 335554:
                        intendedReceiver= json.getString(TO);
                        if(intendedReceiver!=null)
                        {
                            if(!intendedReceiver.equals(manager.getUsername()))
                                return;
                        }
                        handleRequest335554(json,context);
                        break;
                    // A friend has updated his or her profile details.
                    case 873283:
                        handleRequest873283(context);
                        break;
                    // A hub message was received
                    case 476887:
                        handleRequest476887(json,context);
                        break;
                    case 236467:
                        handleRequest236467(json,context);
                        break;
                    // A special event
                    case 522894:
                        handle522894(json,context);
                        break;
                    // A notification message has been sent
                    case 608434:
                        handle608434(json,context);
                        break;
                    //A birthday wish notification
                    case 247843:
                        handle247843(json,context);
                        break;
                    // Friend typing notification
                    case 389746:
                        handle389746(json,context);
                        break;
                    // Hub typing notification
                    case 489746:
                        break;
                    // Timetable update
                    case 846382:
                        handle846382(json,context);
                        break;
                    default:
                        break;

                }








        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPushDismiss(Context context, Intent intent) {
        super.onPushDismiss(context, intent);
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        super.onPushOpen(context, intent);
    }






    public static void makeAdmin(final Activity context, final String hubId, final String usernameToAdd)
    {
        final Manager manager= new Manager(context);
        new SweetAlertDialog(context,SweetAlertDialog.WARNING_TYPE).setTitleText(context.getString(R.string.confirm)).setContentText("Add "+usernameToAdd+" to the hub admins? "+usernameToAdd+" will have the priviledges as you but won't be able to remove you.").setConfirmText(context.getString(R.string.confirm)).setCancelText(context.getString(R.string.cancel)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
                final SweetAlertDialog dialog = new DialogWizard(context).showProgress(context.getString(R.string.please_wait),"Making "+usernameToAdd+" an admin",false);
                HashMap<String,Object> params=manager.getDefaultCloudParams();
                params.put(Const.HUB_ID,hubId);
                params.put(Const.USERNAME,usernameToAdd);
                ParseCloud.callFunctionInBackground("hubAdminAdd", params, new FunctionCallback<String>() {
                    @Override
                    public void done(String s, ParseException e) {
                        dialog.dismiss();
                        if(e==null)
                        {
                            manager.successToast(s);
                        }
                        else{
                            ErrorHandler.handleError(context,e);
                        }
                    }
                });
            }
        }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
            }
        }).show();

    }

    public static void joinHub(final Activity context,final String pin)
    {
     final MaterialDialog dialog= new DialogWizard(context).
              showCancellableProgress
                      (context.getString(R.string.just_hold_on),"Attaching you to the hub","Do in background",null);

        final Manager manager = new Manager(context);
        HashMap<String, Object> params = new HashMap<>();
        params.put(USERNAME, manager.getUsername());
        params.put(PIN,pin);
        params.put(Const.VERSION,context.getString(R.string.version_number));
        ParseCloud.callFunctionInBackground("hubAdder", params, new FunctionCallback<String>() {
            @Override
            public void done(final String s, ParseException e) {

                if (e == null) {

                    JabuWingsHub.getQuery().getInBackground(s, new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject parseObject, ParseException e) {

                            if(e==null)
                            {
                                manager.addHub(s);
                                HouseKeeping.storeHub(context, new JabuWingsHub(parseObject));
                                if(dialog.isShowing())
                                {
                                    dialog.dismiss();
                                    if(Hubs.lonely!=null)
                                        Hubs.lonely.setVisibility(View.GONE);
                                    context.startActivity(new Intent(context, HubChat.class).putExtra(Const.HUB_ID,s));
                                    context.finish();

                                }
                            }
                            else{
                                dialog.dismiss();
                                ErrorHandler.handleError(context,e);
                            }
                        }
                    });


                } else {
                    dialog.dismiss();
                    ErrorHandler.handleError(context, e);
                }
            }
        });






    }






    public static void sendFriendRequest(final Context context,final String friend,String remark){


        final Manager manager= new Manager(context);
        if(manager.getUserFriends()!=null && manager.getUserFriends().contains(friend)){
            manager.successToast("This user is already your friend.");
        }
        else {
            HashMap<String, Object> params = new HashMap<>();
            params.put(FRIEND, friend);
            params.put(USER, manager.getUsername());
            params.put(LEVEL, manager.getLevel());
            params.put(DEPARTMENT, manager.getDepartment());
            params.put(NAME, manager.getName());
            params.put("url", manager.getProfilePictureUrl());
            params.put(REMARK, remark);
            params.put(Const.VERSION, context.getString(R.string.version_number));


            ParseCloud.callFunctionInBackground(FRIEND_AUTHENTICATOR, params, new FunctionCallback<String>() {
                public void done(String response, ParseException e) {
                    if (e == null) {

                        if (response.equals("positive")) {
                            Toast.makeText(context, "The request was successfully sent to @" + friend, Toast.LENGTH_LONG).show();
                        } else {
                            manager.shortToast(response);
                        }


                    } else {
                        ErrorHandler.handleError(context, e);
                    }
                }
            });

        }

    }

    public static void sendFriendRequestWithMatricNo(final Context context,final MatricNo friendMatricNo,final String user,
                                        final String name,final String department,final String level,final String remark){

        ParseUser.getQuery()
                .whereEqualTo(MATRIC_NO,friendMatricNo.toString())
                .getFirstInBackground(new GetCallback<ParseUser>() {
                    @Override
                    public void done(ParseUser parseUser, ParseException e) {

                        if (e == null)

                        {
                            final String friend = parseUser.getUsername();


                            HashMap<String, Object> params = new HashMap<>();
                            params.put(FRIEND, friend);
                            params.put(USER, user);
                            params.put(LEVEL, level);
                            params.put(DEPARTMENT, department);
                            params.put(NAME, name);
                            params.put(REMARK, remark);
                            params.put(Const.VERSION,context.getString(R.string.version_number));

                            ParseCloud.callFunctionInBackground(FRIEND_AUTHENTICATOR, params, new FunctionCallback<String>() {
                                public void done(String response, ParseException e) {
                                    if (e == null) {

                                        if (response.equals("positive")) {
                                            Toast.makeText(context, "The request was successfully sent to @" + friend, Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(context,
                                                    "An error occurred", Toast.LENGTH_SHORT).show();
                                        }


                                    } else {
                                        ErrorHandler.handleError(context, e);
                                    }
                                }
                            });


                        } else {
                            ErrorHandler.handleError(context, e);

                        }

                    }
                });


    }



    public static void sendAcceptFriendRequest(final Context context, final String friend, String user, final boolean openCore)
    {

        HashMap<String, Object> params = new HashMap<>();
        params.put("friend",friend);
        params.put("user", user);
        params.put(Const.VERSION,context.getString(R.string.version_number));


        ParseCloud.callFunctionInBackground(RELATIONSHIP_MEDIATOR, params, new FunctionCallback<String>() {
            public void done(String response, ParseException e) {
                if (e == null) {

                    if (response.equals(POSITIVE)) {
                        UserQuery.getUserFriend(friend, context);
                        List<String> newFriend = new ArrayList<>();
                        newFriend.add(friend);

                        ParseUser parseUser = ParseUser.getCurrentUser();
                        parseUser.fetchInBackground(new GetCallback<ParseUser>() {
                            @Override
                            public void done(ParseUser parseUser1, ParseException e) {
                                if(e==null)  parseUser1.pinInBackground();

                            }
                        });
//                        parseUser.addUnique(FRIENDS,friend);
//                        parseUser.removeAll(REQUESTS, newFriend);
//                        parseUser.saveInBackground();
//                        parseUser.pinInBackground();
                        new Manager(context).successToast(friend+"'s "+context.getString(R.string.friend_request_accepted));
                 /*       if (openCore)
                            context.startActivity(new Intent(context, Domicile.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        Domicile.openFriend();*/
                    }
                    if (response.equals(NEGATIVE)) {
                        Toast.makeText(context, context.getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }
                    if (response.equals("already")) {
                        Toast.makeText(context, context.getString(R.string.friend_request_already), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    ErrorHandler.handleError(context, e);

                }
            }
        });


    }
    public static void retractPersonalMessage(final Activity context, String receiver, String msgId, final String databaseId)
    {
        final SweetAlertDialog dialog= new DialogWizard(context).showProgress(context.getString(R.string.please_wait),"Retracting your Message",false);
        final Manager manager=new Manager(context);
        HashMap<String,Object> params=manager.getDefaultCloudParams();
        params.put(MSG_ID,msgId);
        params.put(RECEIVER,receiver);

        ParseCloud.callFunctionInBackground("retractMessage", params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {
                dialog.dismiss();

                if(e==null)
                {
                  new DialogWizard(context).showSimpleDialog("Retraction",s);
                  HouseKeeping.deleteMessage(context,databaseId);
                }
                else{
                    manager.errorToast("Unable to retract message");
                }
            }
        });
    }

    public static void sendPersonalMessage(final Context context,String sender, String receiver, String msg, boolean sendNow,int... timeDetails){
     final String msgId =   HouseKeeping.storeOutgoingMessage(context,msg,receiver,DataProvider.MessageType.OUTGOING.ordinal());
        if(sendNow) {

            if(!TextUtils.isEmpty(msg))
            sendMSG(context, msg,msgId, sender, receiver,true);
        }

        else{
            MessageService.sendTimedMessage(context,timeDetails[0], timeDetails[1], msg,msgId,sender,receiver);
        }

    }

    private static void cachePersonalMSG(String... p)
    {
        ParseObject cache = new ParseObject(PENDING);
        cache.put("t",0);
        cache.put(FROM,p[0]);
        cache.put(TO,p[1]);
        cache.put(MSG,p[2]);
        cache.put(MSG_ID,p[3]);
        cache.put("mId",p[3]);
        cache.put(TYPE,Integer.valueOf(p[4]));
        cache.pinInBackground();
    }

    public static void sendMSG(final Context context,final String msg, final String id,final String sender, final String receiver,final boolean cache)
    {
        final ParseObject chatItem=new ParseObject(CHATS_CLASS);
        chatItem.put(FROM,sender);
        chatItem.put(TO, receiver);
        chatItem.put(MSG,new Friendship(sender,receiver).encodeMessage(msg));
        chatItem.put(MSG_ID,id);
        chatItem.put(TYPE,0);
        //get Local Friend
        /*try{
            ParseUser user = ParseUser.getQuery().fromLocalDatastore().whereEqualTo(Const.USERNAME,receiver).getFirst();
            chatItem.put(SENDER , ParseUser.getCurrentUser());
            chatItem.put(RECEIVER, user);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }*/

        final Realm realm= Realm.getDefaultInstance();
        Message message=realm.where(Message.class).equalTo(Const.ID,id).findFirst();
        try{
            chatItem.save();
            realm.beginTransaction();
            message.setDeliveryStatus(Const.MESSAGE_SENT);
            message.setMsgId(chatItem.getObjectId());
            realm.commitTransaction();

        }
        catch (ParseException e)
        {
            ErrorHandler.handleError(context,e);
            if (cache && new Manager(context).getResendMessagePreference())
                cachePersonalMSG(sender,receiver,msg,id,"0");
            realm.beginTransaction();
            message.setDeliveryStatus(Const.MESSAGE_FAILED);
            realm.commitTransaction();
        }
        realm.close();
    }

    public static void sendHubMessage(final Context context, String msg, String sender, final String hubId, int type,String... extra)
    {

        final String  msgId = HouseKeeping.storeOutgoingHub(context, msg, hubId, sender, type,extra);
        final ParseObject chatItem = new ParseObject(HUBS_CHAT_CLASS);
        chatItem.put(SENDER,sender);
        chatItem.put(HUB_ID,hubId);
        if(extra.length>0)
        {
            chatItem.put("replyId",extra[0]);
        }
        //chatItem.put("hub",ParseObject.createWithoutData(HUB_CLASS_NAME,hubId));
        //chatItem.put(HUB_TITLE,hubTitle);
        if(msg!=null)
        chatItem.put(MSG,msg);
        else
        chatItem.put(MSG,"");

        chatItem.put(MSG_ID,msgId);
        chatItem.put(TYPE,0);
        chatItem.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                final Realm realm = Realm.getDefaultInstance();
                HubMessage hubMessage = realm.where(HubMessage.class).equalTo(Const.MSG_ID, msgId).findFirst();
                realm.beginTransaction();
                if(e==null)
                {
                    hubMessage.setDeliveryStatus(Const.MESSAGE_SENT);
                    hubMessage.setMsgId(chatItem.getObjectId());
                }
                else{
                    ErrorHandler.handleError(context,e);
                    hubMessage.setDeliveryStatus(Const.MESSAGE_FAILED);
                    hubMessage.setMsgId(chatItem.getObjectId());
                }
                realm.commitTransaction();
                realm.close();
            }
        });

    }


    public static void sendHubMedia(final int type,final ParseFile file, long size,final String hubId, final Context context, final String msgId,@Nullable final String msg)
    {
        final  SQLiteDatabase db= new DatabaseManager(context).getWritableDatabase();
        final ParseObject transferObject= new ParseObject(HUBS_CHAT_CLASS);
        //transferObject.put("hub",ParseObject.createWithoutData(HUB_CLASS_NAME,hubId));
        transferObject.put("hubId",hubId);
        transferObject.put(MSG,msg);
        //transferObject.put(MSG_ID,msgId);
        transferObject.put(FILE,file);
        transferObject.put(TYPE,type);
        transferObject.put("size",size);

        transferObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null)
                {
                    db.execSQL("update hub_messages set delivery_status= '" +
                                    MESSAGE_SENT + "' where msgId=?",
                            new Object[]{msgId});
                    db.execSQL("update hub_messages set msgId= '" +
                                    transferObject.getObjectId() + "' where msgId=?",
                            new Object[]{msgId});
                    context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_HUB_MESSAGES, null);
                    Toast.makeText(context, "File sent", Toast.LENGTH_SHORT).show();
                }
                else {


                    db.execSQL("update hub_messages set delivery_status= '" +
                                    MESSAGE_FAILED + "' where msgId=?",
                            new Object[]{msgId});
                    context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_HUB_MESSAGES, null);
                    e.printStackTrace();

                }
            }
        });


    }
    public static void sendPersonalMedia(final int type,final ParseFile file, long size,final String receiver, final Context context, final String msgId,@Nullable final String msg,final Message message)
    {
        final  SQLiteDatabase db= new DatabaseManager(context).getWritableDatabase();
        final ParseObject transferObject= new ParseObject(CHATS_CLASS);
        transferObject.put(FROM,ParseUser.getCurrentUser().getUsername());
        transferObject.put(TO,receiver);
        if(msg!=null)
        transferObject.put(MSG,msg);
        //transferObject.put(MSG_ID,msgId);
        transferObject.put(FILE,file);
        transferObject.put(Const.SIZE,size);
        transferObject.put(TYPE,type);
        transferObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Realm realm = Realm.getDefaultInstance();

                if (e == null)

                {
                    realm.beginTransaction();
                    message.setDeliveryStatus(Const.MESSAGE_SENT);
                    message.setMsgId(transferObject.getObjectId());
                    realm.commitTransaction();

                } else {
                    ErrorHandler.handleError(context,e);
                    realm.beginTransaction();
                    message.setDeliveryStatus(Const.MESSAGE_FAILED);
                    realm.commitTransaction();

                }
                realm.close();


            }
        });





    }

    @Deprecated
    public static void updateProfile(final Context context,int id, String update)
    {

        switch (id) {
            case 1:
                HashMap<String, Object> params = new HashMap<>();
                params.put(USERNAME, new Manager(context).getUsername());
                params.put("id",id);
                params.put("url",update);
                params.put(Const.VERSION,context.getString(R.string.version_number));
                ParseCloud.callFunctionInBackground(FRIENDS_RADAR, params, new FunctionCallback<String>() {
                    @Override
                    public void done(String s, ParseException e) {
                        if (e == null) {
                            //   Toast.makeText(context, context.getString(R.string.status_updated), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                break;
            case 2:
                HashMap<String, Object> params1 = new HashMap<>();
                params1.put(USERNAME, new Manager(context).getUsername());
                params1.put("id", id);
                params1.put(STATUS,update);
                params1.put(Const.VERSION,context.getString(R.string.version_number));
                ParseUser user=  ParseUser.getCurrentUser();
                user.put(STATUS,update);
                user.saveEventually();


                ParseCloud.callFunctionInBackground(FRIENDS_RADAR, params1, new FunctionCallback<String>() {
                    @Override
                    public void done(String s, ParseException e) {
                        if (e == null) {
                            Toast.makeText(context, context.getString(R.string.status_updated), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();

                        }
                    }
                });
                break;
        }
    }

    public static void setMessageRead(String friend,final String msgId, final Context context)
    {
        HashMap<String,Object> params= new HashMap<>();
        params.put(SENDER, new Manager(context).getUsername());
        params.put(RECEIVER, friend);
        params.put(MSG_ID, msgId);
        params.put(ID,2);
        params.put(Const.VERSION,context.getString(R.string.version_number));
        ParseCloud.callFunctionInBackground(MESSAGE_DELIVERY_SERVICE, params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {

                if (e == null) {
                    new DatabaseManager(context).setMessageStatus(msgId,Const.MESSAGE_READ);
                }


            }
        });




    }


    public static void removeFriend(final Activity context, final String username)
    {
        final SweetAlertDialog materialDialog = new DialogWizard(context).showSimpleProgress("Please wait","Removing "+username);
        Manager manager;
        manager= new Manager(context);
        HashMap<String,Object> params = new HashMap<>();
        params.put(USERNAME,manager.getUsername());
        params.put(FRIEND,username);
        params.put(Const.VERSION,context.getString(R.string.version_number));

        ParseCloud.callFunctionInBackground("friendRemover", params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {

                if(e==null)
                {
                    ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseUser>() {
                        @Override
                        public void done(ParseUser parseObject, ParseException e) {
                            materialDialog.dismiss();
                            HouseKeeping.deleteFriend(context, username);
                            context.startActivity(new Intent(context, Domicile.class));
                        }
                    });

                }

                else{
                    materialDialog.dismiss();
                    ErrorHandler.handleError(context, e);
                }
            }
        });
    }

    public static void removeHub(final Activity activity, String hubId)
    {
        //TODO implement
    }
    //TODO Review
    public static void sendUserTyping(Context context, String username)

    {
        HashMap<String,Object> params= new Manager(context).getDefaultCloudParams();
        params.put(RECEIVER,username);
        ParseCloud.callFunctionInBackground(TYPING_WATCHDOG, params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {

            }
        });



    }


        private void handleRequest335554(JSONObject json,Context context)
        {
            try {

                int id=json.getInt("id");
                String msgId= json.getString("msgId");

                DatabaseManager databaseManager = new DatabaseManager(context);
                switch (id)

                {
                    case 1:
                        databaseManager.setMessageStatus(msgId,Const.MESSAGE_DELIVERED);
                        break;
                    case 2:
                        databaseManager.setMessageStatus(msgId,Const.MESSAGE_READ);
                        break;


                }
                context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_MESSAGES,null);



            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

        }


    private void handleRequest333777(JSONObject json,Context context){

        try{


        int id = json.getInt("id");

            if (id==1) {

               // Toast.makeText(context, "JabuWings friend request received", Toast.LENGTH_SHORT).show();
                String title = json.getString(TITLE);
                String not_message = json.getString(ALERT);
                String username=json.getString(USERNAME);
                HouseKeeping.storeNotification(0,new Date(json.getLong(TIME)), not_message, username, username);


                Intent resultIntent = new Intent(context, FriendRequests.class);
                notificationManager = new NotificationManager(context);
                resultIntent.putExtras(intent.getExtras());
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                notificationManager.showNotificationMessage(title, not_message, resultIntent);
            }

            else if (id==2){


                String username=json.getString(USERNAME);
                String title= json.getString(TITLE);
                String not_message= json.getString(ALERT);
                UserQuery.getUserFriend(username,context);


                Intent resultIntent = new Intent(context, Domicile.class);
                notificationManager = new NotificationManager(context);
                resultIntent.putExtras(intent.getExtras());
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                notificationManager.showNotificationMessage(title, not_message, resultIntent);
            }

        }
        catch (JSONException e){
                e.printStackTrace();
        }

    }



    private void handleRequest738728(JSONObject json, final  Context context)
    {
        try{
            String msgId= json.getString(MSG_ID);
            HouseKeeping.retractPersonalMessage(context,msgId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }
  //  private void handleRequest677774(JSONObject json,final Context context)
//    {
//        try {
//            String title = json.getString(TITLE);
//            String not_message=json.getString("alert");
//            String msgSender= json.getString(SENDER);
//            String msg= json.getString(MSG);
//            String receiver= json.getString("receiver");
//            final   String msgId= json.getString("msgId");
//            int type=json.getInt(TYPE);
//            String fileUrl;
//            long size;
//            String objectId;
//            switch(type)
//            {
//                //Text message
//                case 0:
////                    HouseKeeping.storeIncomingMessage(context, msg, msgSender, receiver,msgId);
//                    break;
//                //Picture Message
//                case 1:
//                    size=json.getLong(SIZE);
//                    objectId=json.getString(OBJECT_ID);
//                    fileUrl=json.getString(FILE_URL);
//                    manager.shortToast("incoming file "+fileUrl+" id"+objectId);
//                    HouseKeeping.storeIncomingPersonalMedia(context, fileUrl, null,size, msg, msgSender, DataProvider.MessageType.INCOMING_IMAGE.ordinal(),objectId);
//                    HouseKeeping.getMedia(context,objectId,msgSender,0);
//                    //TODO if file is null
//                    break;
//                //Audio Message
//                case 2:
//                    size=json.getLong(SIZE);
//                    objectId=json.getString(OBJECT_ID);
//                    fileUrl=json.getString(FILE_URL);
//                    manager.shortToast("incoming file "+fileUrl+" id"+objectId);
//                    HouseKeeping.storeIncomingPersonalMedia(context, fileUrl, null,size, msg, msgSender, DataProvider.MessageType.INCOMING_VOICE_NOTE.ordinal(),objectId);
//                    HouseKeeping.getMedia(context,objectId,msgSender,0);
//                    break;
//                //File message
//                case 3:
//                    size=json.getLong(SIZE);
//                    objectId=json.getString(OBJECT_ID);
//                    fileUrl=json.getString(FILE_URL);
//                    manager.shortToast("incoming file "+fileUrl+" id"+objectId);
//                    HouseKeeping.storeIncomingPersonalMedia(context, fileUrl, null,size, msg, msgSender, DataProvider.MessageType.INCOMING_FILE.ordinal(),objectId);
//                    HouseKeeping.getMedia(context,objectId,msgSender,0);
//                    break;
//            }
//
//
//              setMessageDelivered(context,msgSender,msgId);
//
//            Intent resultIntent = new Intent(context, Chat.class);
//            notificationManager = new NotificationManager(context);
//            resultIntent.putExtra(FRIEND_ID, String.valueOf(msgSender));
//            resultIntent.putExtras(intent.getExtras());
//            resultIntent.putExtra(SENDER,msgSender);
//            resultIntent.putExtra(NOTIFICATION_INTENT,true);
//            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            notificationManager.showNotificationMessage(title, not_message, resultIntent);
//
//
//        }
//        catch (JSONException e){
//            e.printStackTrace();
//        }
//
//
//
//    }
    public static void setMessageDelivered(final  Context context,String msgSender,final String msgId )
    {
        HashMap<String, Object> params = new HashMap<>();
        params.put(SENDER, new Manager(context).getUsername());
        params.put(RECEIVER, msgSender);
        params.put(MSG_ID, msgId);
        params.put(ID, 1);
        params.put(Const.VERSION,context.getString(R.string.version_number));
        ParseCloud.callFunctionInBackground(MESSAGE_DELIVERY_SERVICE, params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {

                if (e == null) {
                    new DatabaseManager(context).setMessageStatus(msgId,Const.MESSAGE_DELIVERED);

                } else {
                    new DatabaseManager(context).setMessageStatus(msgId,Const.MESSAGE_FAILED);
                }



            }
        });
    }


    private void handleRequest236467(JSONObject json,Context context)
    {
        try{
            String hubId= json.getString(Const.HUB_ID);

            JabuWingsHub.getQuery().getInBackground(hubId, new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    if(e==null)
                    {
                        parseObject.pinInBackground();
                    }

                }
            });
        }
        catch (JSONException e)
        {
            e.printStackTrace();

        }
    }
    private void handleRequest476887(JSONObject json,Context context)

    {

        try {
            String title = json.getString(TITLE);
            int type =json.getInt(Const.TYPE);
            String not_message = json.getString("alert");
            String sender = json.getString("sender");
            String msg = json.getString("msg");
            String hubId=json.getString("hubId");
            final String msgId = json.getString("msgId");
            String fileUrl,objectId;int size;

            if(type==0)
            {
                //Text message
//                HouseKeeping.storeHubIncomingMessage(context, msg, sender, hubId, msgId);
            }
            else{
                fileUrl=json.getString(Const.FILE_URL);
                size=json.getInt("size");
                objectId=json.getString(OBJECT_ID);

                switch (type)
                {
                    //A picture message
                    case 1:
                        HouseKeeping.storeIncomingHubMedia(context,fileUrl,null,size,msg,sender,hubId,DataProvider.MessageType.INCOMING_IMAGE.ordinal(),msgId);
                        //TODO change sender to hubtitle+sender
                        break;
                    //An audio message
                    case 2:
                        HouseKeeping.storeIncomingHubMedia(context,fileUrl,null,size,msg,sender,hubId,DataProvider.MessageType.INCOMING_VOICE_NOTE.ordinal(),msgId);
                        //TODO change sender to hubtitle+sender
                        break;
                    case 3:
                        HouseKeeping.storeIncomingHubMedia(context,fileUrl,null,size,msg,sender,hubId,DataProvider.MessageType.INCOMING_VOICE_NOTE.ordinal(),msgId);
                        //TODO change sender to hubtitle+sender
                        break;

                }
//                HouseKeeping.getMedia(context,objectId,sender,1);

            }




            Intent resultIntent = new Intent(context, HubChat.class);
            notificationManager = new NotificationManager(context);
            resultIntent.putExtras(intent.getExtras());
            resultIntent.putExtra(HUB_ID,hubId);
            resultIntent.putExtra(NOTIFICATION_INTENT,true);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            notificationManager.showNotificationMessage(title, not_message, resultIntent);

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param context the context of the request received
     * @deprecated This method is no longer used.
     * The regular check of user's profile changes method using the ProfileWatchDog is now used.
     */
    @Deprecated
    private void handleRequest873283(Context context)
    {
        SQLiteDatabase db= helper.getWritableDatabase();

        try {


        int id= json.getInt("id");
        String newUrl=json.getString("url");
        String newStatus=json.getString("status");
        String friend= json.getString("username");


        switch (id)
        {
            case 1:
                manager.storeFriendProfilePictureUrl(friend,newUrl);
                break;
            case 2:
                db.execSQL("update profile set status='" +newStatus+ "' where username=?",
                        new Object[]{friend});
                break;





        }




        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }






    }


    private void handle389746(JSONObject json, Context c)
    {
        try{
            String friend = json.getString(USERNAME);
            HouseKeeping.storeFriendTyping(friend);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void handle846382(JSONObject json, Context context)
    {
        try{
            int id= 846382;
            String title=json.getString(TITLE);
            String alert=json.getString(ALERT);
            Intent resultIntent = new Intent(context, TimeTable2.class);
            notificationManager = new NotificationManager(context);
            resultIntent.putExtras(intent.getExtras());
            resultIntent.putExtra(NOT_ID,id);
            resultIntent.putExtra(UPDATE, true);
            resultIntent.putExtra(NOTIFICATION_INTENT,true);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            notificationManager.showNotification(title, alert, resultIntent);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void handle247843(JSONObject json,Context c)
    {
        try{
            String title=json.getString(TITLE);
            String alert=json.getString(ALERT);
            String msg=json.getString(MSG);
            Intent resultIntent = new Intent(c, Domicile.class);
            resultIntent.putExtra(Const.NOT_ID,247843);
            notificationManager = new NotificationManager(c);
            resultIntent.putExtras(intent.getExtras());
            notificationManager.showNotification(title, alert, resultIntent);
            new Parrot(c).speak(msg);
        }
        catch (JSONException e){e.printStackTrace();}

    }

    private void handle608434(JSONObject json,Context context)
    {
         try{
             String sender= json.getString(SENDER);
             String msg= json.getString(MSG);
             //String header= json.getString(HEADER);
             String title=json.getString(TITLE);
             String alert=json.getString(ALERT);
             Date date =new Date(json.getLong(TIME));
             int type=json.getInt(TYPE);
             int id=json.getInt("m_id");
             String meta= json.getString(META);
             HouseKeeping.storeNotification(type,date,msg,sender,meta);


             Intent resultIntent = new Intent(context, Notifications.class);
             notificationManager = new NotificationManager(context);
             resultIntent.putExtras(intent.getExtras());
             resultIntent.putExtra(NOT_ID,id);
             resultIntent.putExtra(NOTIFICATION_INTENT,true);
             resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
             notificationManager.showNotification(title, alert, resultIntent);

         }
         catch (JSONException e)
         {
            e.printStackTrace();
         }
    }
        private void handle522894(JSONObject json, Context context)
    {
        try {
            String msg = json.getString(MSG);
            switch (json.getInt("code")) {
                case 925024:
                    // A special notification to update the watchword
                    PreferenceManager.getDefaultSharedPreferences(context).edit().putString("watchword",msg).commit();
                break;
                case 394448:
                    // A special notification to force the app to stop
                    System.exit(0);
                break;
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

    }









}
