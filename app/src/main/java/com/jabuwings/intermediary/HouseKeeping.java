package com.jabuwings.intermediary;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import com.jabuwings.database.DataProvider;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.database.realm.Friend;
import com.jabuwings.database.realm.Hub;
import com.jabuwings.database.realm.HubMessage;
import com.jabuwings.database.realm.Message;
import com.jabuwings.encryption.Friendship;
import com.jabuwings.intelligence.lisa.Lisa;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.JabuWingsFriend;
import com.jabuwings.models.JabuWingsHub;
import com.jabuwings.utilities.Time;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import io.realm.Realm;


/**
 * Created by Falade James on 8/16/2015.
 */
public class HouseKeeping {
private static final String LOG_TAG= HouseKeeping.class.getSimpleName();
    public static void storeFriend(JabuWingsFriend jabuWingsFriend, final Context context) {



        final String username= jabuWingsFriend.getUsername();
        final String name = jabuWingsFriend.getName();
        final String department = jabuWingsFriend.getDepartment();
        final String status = jabuWingsFriend.getStatus();
        final String level = jabuWingsFriend.getLevel();
        final String birthday = jabuWingsFriend.getBirthDay();
        final String state = jabuWingsFriend.getStateOfOrigin();
        final int type= jabuWingsFriend.getAccountType();
        final String verifiedString= jabuWingsFriend.isVerifiedAs();
        final long profilePictureDate=jabuWingsFriend.getProfilePictureDate();

        final ParseFile parseFile;
        parseFile=jabuWingsFriend.getParseFile(Const.PROFILE_PICTURE);
        jabuWingsFriend.pinInBackground();
        if(parseFile!=null)
            parseFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] bytes, ParseException e) {
                    if (e == null) {
                        try {
                            FileOutputStream outputStream = context.openFileOutput(username + "_" + Const.PROFILE_PICTURE, Context.MODE_PRIVATE);
                            outputStream.write(bytes);
                            new Manager(context).storeFriendProfilePictureFileDate(username, profilePictureDate);
                        } catch (IOException ee) {
                            ee.printStackTrace();
                        }
                    }

                }
            });
        Realm realm = Realm.getDefaultInstance();
        try{
            realm.beginTransaction();
            Friend friend  = realm.createObject(Friend.class,username);
            friend.setName(name);
            if(department!=null)
                friend.setDepartment(department);
            friend.setBirthday(birthday);
            friend.setStatus(status);
            if(level!=null)
                friend.setLevel(level);
            friend.setState(state);
            friend.setType(type);
            friend.setFriendShipStatus(Const.FRIENDSHIP_STATUS.ACTIVE.ordinal());
            if(parseFile!=null)
                friend.setPicture(parseFile.getUrl());
            if(verifiedString!=null)
                friend.setVerifiedAs(verifiedString);
                 /*
            If No profile picture is available.
            We are setting the profilePictureDate to the current date to avoid collision while trying to check if a profilePicture was never gotten.
            This not being empty makes it possible for us to update it in the future and to separate it from profile pictures that had errors while obtaining them.
             */
            friend.setPictureDate(parseFile==null ? new Date().getTime(): profilePictureDate);
            realm.commitTransaction();
            realm.close();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            realm.close();
        }
        


    }

    public static void storeHub(final Context context,final JabuWingsHub jabuWingsHub)
    {
        final String hubId=jabuWingsHub.getId();
        final long profilePictureDate= jabuWingsHub.getProfilePictureDate();
        Realm realm = Realm.getDefaultInstance();
        try{

            realm.beginTransaction();
            Hub hub = realm.createObject(Hub.class,jabuWingsHub.getId());
            hub.initHub(jabuWingsHub);
            realm.commitTransaction();
            realm.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            realm.close();
        }
        try{
            ParseFile parseFile;
            parseFile=jabuWingsHub.getHub().getParseFile(Const.PROFILE_PICTURE);
            if(parseFile!=null)
                parseFile.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] bytes, ParseException e) {
                        if (e == null) {
                            try {
                                FileOutputStream outputStream = context.openFileOutput(hubId + "_" + Const.PROFILE_PICTURE, Context.MODE_PRIVATE);
                                outputStream.write(bytes);
                                new Manager(context).storeHubProfilePictureFileDate(hubId, profilePictureDate);
                            } catch (IOException ee) {
                                ee.printStackTrace();
                            }
                        }

                    }
                });
            else{
            /*
            No profile picture is available.
            We are setting the profilePictureDate to the current date to avoid collision while trying to check if a profilePicture was never gotten.
            This not being empty makes it possible for us to update it in the future and to separate it from profile pictures that had errors while obtaining them.
             */
                new Manager(context).storeHubProfilePictureFileDate(hubId, new Date().getTime());
            }


        }
        catch (SQLiteConstraintException e)
        {
            e.printStackTrace();
        }
    }

    public static void getProfilePicture(final Context context,final String username,ParseFile profilePictureFile,final long profilePictureDate)
    {
        profilePictureFile.getDataInBackground(new GetDataCallback() {
            @Override
            public void done(byte[] bytes, ParseException e) {
                if (e == null)
                    try {
                        Manager manager = new Manager(context);
                        FileOutputStream outputStream = context.openFileOutput(manager.getFriendOrHubProfilePictureName(username), Context.MODE_PRIVATE);
                        outputStream.write(bytes);
                        manager.storeFriendProfilePictureFileDate(username, profilePictureDate);
                    } catch (IOException ee) {
                        ee.printStackTrace();
                    }
            }
        });
    }

    public static void getHubPicture(final Context context,final String hubId,ParseFile profilePictureFile,final long profilePictureDate)
    {
        profilePictureFile.getDataInBackground(new GetDataCallback() {
            @Override
            public void done(byte[] bytes, ParseException e) {
                if (e == null)
                    try {
                        Manager manager = new Manager(context);
                        FileOutputStream outputStream = context.openFileOutput(manager.getFriendOrHubProfilePictureName(hubId), Context.MODE_PRIVATE);
                        outputStream.write(bytes);
                        manager.storeHubProfilePictureFileDate(hubId, profilePictureDate);
                    } catch (IOException ee) {
                        ee.printStackTrace();
                    }
            }
        });
    }

    public static void storeNotification(int type, Date date, String notification_msg,String sender,String meta) {

        ParseObject o = new ParseObject(Const.LOCAL_NOTIFICATIONS);
        o.put(Const.MSG,notification_msg);
        //o.put(Const.TITLE,title);
        o.put(Const.TYPE,type);
        o.put(Const.VALID,true);
        o.put(Const.SENDER,sender);
        o.put(Const.TIME,date);
        o.put(Const.META,meta);
        o.put(Const.USER,ParseUser.getCurrentUser());
        o.put(Const.MESSAGE_READ,false);
        o.pinInBackground();
    }

    public static String storeOutgoingMessage(final Context context, final String msg, final String receiver, final int type)
    {

        final String msgId = UUID.randomUUID().toString();


        final Realm realm = Realm.getDefaultInstance();


        final Friend friend = realm.where(Friend.class).equalTo(Const.USERNAME, receiver).findFirst();

        realm.beginTransaction();
        Manager manager= new Manager(context);
        Time time= new Time();
        Message message = realm.createObject(Message.class,msgId);
        message.setMsg(msg);
        message.setSender(manager.getUsername());
        message.setReceiver(receiver);
        message.setMsgTime(time.getDisplayTimeDate());
        message.setMsgType(type);
        message.setDeliveryStatus(Const.MESSAGE_SENDING);
        friend.setLastMessage(message);
        friend.setLastMessageDate(System.currentTimeMillis());
        realm.commitTransaction();

        realm.close();





        return  msgId;
    }

    public static void storeFriendTyping(final String username)
    {

        new Thread(new Runnable() {
            @Override
            public void run() {
                final Realm realm = Realm.getDefaultInstance();
                Friend friend = realm.where(Friend.class).equalTo(Const.USERNAME, username).findFirst();
                realm.beginTransaction();
                friend.setLastTypingDate(System.currentTimeMillis());
                realm.commitTransaction();
            }
        }).start();


    }


    public static String  storeOutgoingHub(Context context, String msg, String hubId,String sender, int type,String[] extra)
    {

        String reply="";
        if(extra.length>0)
        {
            reply=extra[1];
        }
        String messageId =Lisa.generateHubMessageId(hubId);
        Manager manager= new Manager(context);
        Time time= new Time();
        Realm realm = Realm.getDefaultInstance();

        Hub hub = realm.where(Hub.class).equalTo(Const.HUB_ID,hubId).findFirst();

        realm.beginTransaction();;
        HubMessage hubMessage = realm.createObject(HubMessage.class,UUID.randomUUID().toString());
        hubMessage.setHubId(hubId);
        hubMessage.setMsg(msg);
        hubMessage.setMsgId(messageId);
        hubMessage.setSender(sender);
        hubMessage.setType(type);
        if(!TextUtils.isEmpty(reply))
        {
            hubMessage.setReply(reply);
        }
        hubMessage.setDeliveryStatus(Const.MESSAGE_SENDING);
        hubMessage.setTime(time.getDisplayTimeDate());
        hub.setLastMessage(hubMessage);
        hub.setLastMessageDate(System.currentTimeMillis());
        realm.commitTransaction();
        realm.close();

        return messageId;
    }

    public static HubMessage storeOutgoingHubMedia(Context context, String fileUrl, String msg, String hubId, int type )

    {
        Manager manager= new Manager(context);
        Time time= new Time();
        String messageId= Lisa.generateMessageId();

        Realm realm = Realm.getDefaultInstance();
        Hub hub = realm.where(Hub.class).equalTo(Const.HUB_ID,hubId).findFirst();
        realm.beginTransaction();
        com.jabuwings.database.realm.File file =realm.createObject(com.jabuwings.database.realm.File.class,UUID.randomUUID().toString());
        file.setLocalUrl(fileUrl);
        file.setProgress(0);
        HubMessage message = realm.createObject(HubMessage.class, UUID.randomUUID().toString());
        if(msg!=null)
        message.setMsg(msg);
        message.setFile(file);
        message.setSender(manager.getUsername());
        message.setType(type);
        message.setDeliveryStatus(Const.MESSAGE_SENDING);
        message.setMsgId(messageId);
        message.setHubId(hubId);
        message.setTime(time.getDisplayTimeDate());
        hub.setLastMessage(message);
        hub.setLastMessageDate(System.currentTimeMillis());
        realm.commitTransaction();
        return message;
    }
    public static Message storeOutgoingPersonalMedia(final Context context,final  String fileUrl,final String msg,final String receiver,final int type )

    {

        Manager manager= new Manager(context);
        Time time= new Time();
        final String msgId= Lisa.generateMessageId();

        Realm realm = Realm.getDefaultInstance();
        final Friend friend = realm.where(Friend.class).equalTo(Const.USERNAME, receiver).findFirst();
        realm.beginTransaction();
        com.jabuwings.database.realm.File file = realm.createObject(com.jabuwings.database.realm.File.class, UUID.randomUUID().toString());
        file.setLocalUrl(fileUrl);
        file.setProgress(0);
        Message message=realm.createObject(Message.class, UUID.randomUUID().toString());
        if(msg!=null)
        message.setMsg(msg);
        message.setFile(file);
        message.setSender(manager.getUsername());
        message.setReceiver(receiver);
        message.setMsgTime(time.getDisplayTimeDate());
        message.setMsgType(type);
        message.setDeliveryStatus(Const.MESSAGE_SENDING);
        message.setMsgId(msgId);
        friend.setLastMessageDate(System.currentTimeMillis());
        friend.setLastMessage(message);
        realm.commitTransaction();



        return message;
    }

    public static void storeIncomingPersonalFile(Context context, String fileUrl, String fileAddress,String name,long size, String message, String sender,String messageId)
    {
        try {
            Manager manager = new Manager(context);
            Time time = new Time();
            ContentValues values = new ContentValues(11);
            values.put(DataProvider.COL_MSG, message);
            values.put(DataProvider.COL_MESSAGE_ID, messageId);
            values.put(DataProvider.COL_TO, manager.getUsername());
            values.put(DataProvider.COL_FROM, sender);
            values.put(DataProvider.COL_TIME, time.getDisplayTimeDate());
            values.put(DataProvider.COL_FILE_LOCAL_URL, fileUrl);
            values.put(DataProvider.COL_FILE_SIZE,size);
            values.put(DataProvider.COL_FILE_NAME,name);
            values.put(DataProvider.COL_FILE_ADDRESS, fileAddress);
            values.put(DataProvider.COL_TYPE, DataProvider.MessageType.INCOMING_FILE.ordinal());
            values.put(DataProvider.COL_DELIVERY_STATUS, Const.MESSAGE_FAILED);
            new Manager(context).storeLatestMessageDate(new Date().getTime());
            context.getContentResolver().insert(DataProvider.CONTENT_URI_MESSAGES, values);
            context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_MESSAGES, null);
        }
        catch (SQLiteConstraintException e){e.printStackTrace();}
    }
    @Deprecated
    public static void storeIncomingPersonalMedia(Context context, String fileUrl, String fileAddress, long size, String message, String sender, int type, String messageId )

    {

        try {
            Manager manager = new Manager(context);
            Time time = new Time();
            ContentValues values = new ContentValues(10);

            values.put(DataProvider.COL_MSG, message);
            values.put(DataProvider.COL_MESSAGE_ID, messageId);
            values.put(DataProvider.COL_TO, manager.getUsername());
            values.put(DataProvider.COL_FROM, sender);
            values.put(DataProvider.COL_TIME, time.getDisplayTimeDate());
            values.put(DataProvider.COL_FILE_LOCAL_URL, fileUrl);
            values.put(DataProvider.COL_FILE_SIZE,size);
            values.put(DataProvider.COL_FILE_ADDRESS, fileAddress);
            values.put(DataProvider.COL_TYPE, type);
            values.put(DataProvider.COL_DELIVERY_STATUS, Const.MESSAGE_FAILED);
            new Manager(context).storeLatestMessageDate(new Date().getTime());
            context.getContentResolver().insert(DataProvider.CONTENT_URI_MESSAGES, values);
            context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_MESSAGES, null);
        }
        catch (SQLiteConstraintException e){e.printStackTrace();}

    }

    @Deprecated
    public static void storeIncomingHubMedia(Context context, String fileUrl, String fileAddress, long size, String message, String sender,String hubId, int type, String messageId )

    {

        try {
            Manager manager = new Manager(context);
            Time time = new Time();
            ContentValues values = new ContentValues(10);

            values.put(DataProvider.COL_MSG, message);
            values.put(DataProvider.COL_MESSAGE_ID, messageId);
            values.put(DataProvider.COL_HUB_ID,hubId);
            values.put(DataProvider.COL_HUB_MESSAGE_SENDER, sender);
            values.put(DataProvider.COL_TIME, time.getDisplayTimeDate());
            values.put(DataProvider.COL_FILE_LOCAL_URL, fileUrl);
            values.put(DataProvider.COL_FILE_SIZE,size);
            values.put(DataProvider.COL_FILE_ADDRESS, fileAddress);
            values.put(DataProvider.COL_TYPE, type);
            values.put(DataProvider.COL_DELIVERY_STATUS, Const.MESSAGE_FAILED);
            new Manager(context).storeLatestMessageDate(new Date().getTime());
            context.getContentResolver().insert(DataProvider.CONTENT_URI_HUB_MESSAGES, values);
            context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_HUB_MESSAGES, null);
        }
        catch (SQLiteConstraintException e){e.printStackTrace();}

    }

    public static void getHubMedia(final Context context,final String objectId,final String senderUsername)
    {
        final Manager manager =new Manager(context);
        manager.shortToast("Attempting to get hub Media");
        final ParseQuery<ParseObject> fileQuery=new ParseQuery<>(Const.HUBS_CHAT_CLASS);
        fileQuery.getInBackground(objectId, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) {
                    manager.shortToast("Media Object gotten");
                    ParseFile file = parseObject.getParseFile(Const.FILE);

                    file.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] bytes, ParseException e) {
                            if (e == null) {

                                Log.d(LOG_TAG,"FIle gotten");
                                manager.shortToast("File gotten");
                                try{
                                    manager.shortToast("Attempting to save after download");
                                    File sd = Environment.getExternalStorageDirectory();
                                    File mediaDir = new File(sd.getAbsolutePath(),Const.PUBLIC_IMAGE_DIRECTORY);
                                    File media= new File(mediaDir,Lisa.generateImageFileName(senderUsername));
                                    FileOutputStream stream = new FileOutputStream(media);
                                    stream.write(bytes);
                                    SQLiteDatabase db = new Manager(context).getDatabaseManager().getWritableDatabase();
                                    ContentValues contentValues = new ContentValues();
                                    contentValues.put(DataProvider.COL_DELIVERY_STATUS,Const.MESSAGE_READ);
                                    contentValues.put(DataProvider.COL_FILE_LOCAL_URL,media.getAbsolutePath());
                                    db.update(DataProvider.TABLE_HUB_MESSAGES, contentValues, "msgId = ?", new String[]{objectId});
                                    context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_HUB_MESSAGES, null);
                                }
                                catch (IOException f)
                                {
                                    new Manager(context).shortToast("IO exception");
                                    SQLiteDatabase db = new Manager(context).getDatabaseManager().getWritableDatabase();
                                    ContentValues contentValues = new ContentValues();
                                    contentValues.put(DataProvider.COL_DELIVERY_STATUS,Const.MESSAGE_FAILED);
                                    db.update(DataProvider.TABLE_HUB_MESSAGES, contentValues, "msgId = ?", new String[]{objectId});
                                    context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_HUB_MESSAGES, null);
                                    f.printStackTrace();
                                }
                            }

                            else{
                                new Manager(context).shortToast(e.getMessage());
                                e.printStackTrace();
                                SQLiteDatabase db = new Manager(context).getDatabaseManager().getWritableDatabase();
                                db.execSQL("update messages set delivery_status ='" + Const.MESSAGE_FAILED + "' where msgId=?", new Object[]{objectId});
                                context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_HUB_MESSAGES, null);
                            }

                        }
                    });
                }

                else{
                    e.printStackTrace();
                    new Manager(context).shortToast("Error downloading image");
                }
            }
        });

    }

    /**
     *
     * @param context
     * @param object
     * @param senderUsername
     * @param type 0 for friends 1 for hubs
     */
    public static void getMedia(final Context context, final ParseObject object, final String senderUsername, final int type)
    {
        final Manager manager =new Manager(context);
        manager.log("Attempting to get Media "+object.getObjectId());
        manager.log("Media Object gotten");
        ParseFile file = object.getParseFile(Const.FILE);
        final String objectId = object.getObjectId();

        file.getDataInBackground(new GetDataCallback() {
            @Override
            public void done(byte[] bytes, ParseException e) {
                if (e == null) {

                    Log.d(LOG_TAG,"FIle gotten");
                    manager.shortToast("File gotten");
                    try{
                        manager.shortToast("Attempting to save after download");
                        File sd = Environment.getExternalStorageDirectory();
                        File mediaDir = new File(sd.getPath(),Const.PUBLIC_IMAGE_DIRECTORY);
                        File media= new File(mediaDir,Lisa.generateImageFileName(senderUsername)+".jpg");
                        FileOutputStream stream = new FileOutputStream(media);
                        stream.write(bytes);

                        Realm realm = Realm.getDefaultInstance();
                        if(type==0)
                        {
                            Message message = realm.where(Message.class).equalTo(Const.MSG_ID,objectId).findFirst();
                            com.jabuwings.database.realm.File file = message.getFile();
                            realm.beginTransaction();
                            message.setDeliveryStatus(Const.MESSAGE_READ);
                            file.setLocalUrl(media.getPath());
                            manager.shortToast("Setting path "+media.getPath());
                            realm.commitTransaction();
                        }
                        else{
                            HubMessage hubMessage= realm.where(HubMessage.class).equalTo(Const.MSG_ID,objectId).findFirst();
                            com.jabuwings.database.realm.File file = hubMessage.getFile();
                            realm.beginTransaction();
                            hubMessage.setDeliveryStatus(Const.MESSAGE_READ);
                            file.setLocalUrl(media.getAbsolutePath());
                            realm.commitTransaction();
                        }
                        realm.close();
                        object.unpinInBackground();
                    }
                    catch (IOException f)
                    {
                        new Manager(context).shortToast("IO exception");
                        f.printStackTrace();
                        setMessageFailed(objectId,type);
                    }
                }

                else{
                    object.pinInBackground();
                    new Manager(context).shortToast(e.getMessage());
                    e.printStackTrace();
                    setMessageFailed(objectId,type);

                }

            }
        });
    }

    private static void setMessageFailed(String msgId, int type)
    {
        Realm realm = Realm.getDefaultInstance();
        if(type==0)
        {
            Message message = realm.where(Message.class).equalTo(Const.MSG_ID,msgId).findFirst();
            realm.beginTransaction();
            message.setDeliveryStatus(Const.MESSAGE_FAILED);
            realm.commitTransaction();
        }
        else{
            HubMessage hubMessage = realm.where(HubMessage.class).equalTo(Const.MSG_ID,msgId).findFirst();
            realm.beginTransaction();
            hubMessage.setDeliveryStatus(Const.MESSAGE_FAILED);
            realm.commitTransaction();
        }
    }

    /**
     * Delete the message from the device if it hasn't been read
     * @param msgId The universal message id
     */
    public static void retractPersonalMessage(Context context,String msgId){
        String where = DataProvider.COL_MESSAGE_ID + "=?";
        String[] whereArgs = new String[]{msgId};
        Cursor c = new Manager(context).getDatabaseManager().getReadableDatabase().query(true, DataProvider.TABLE_MESSAGES, null, where, whereArgs, null, null, null, null);

        if (c != null && c.moveToFirst()){
            String status= c.getString(c.getColumnIndex(DataProvider.COL_DELIVERY_STATUS));
            if (!status.equals(Const.MESSAGE_READ))
            {
                //delete the message from the device
                deleteMessage(context,String.valueOf(c.getInt(c.getColumnIndex(DataProvider.COL_ID))));
            }
        }
    }
    public static void storeIncomingMessage
            (final Context context, final ParseObject object)
    {
        final Realm realm = Realm.getDefaultInstance();

        final String from = object.getString(Const.FROM);
        final String to = object.getString(Const.TO);
        final String msg = object.getString(Const.MSG);
        final String msgId = object.getObjectId();
        final String fileUrl = object.getString(Const.FILE_URL);
        final long size = object.getLong(Const.SIZE);


        try {
            final Time time = new Time();
            realm.beginTransaction();
            Friendship friendship = new Friendship(from,to);
            Message message = realm.createObject(Message.class, UUID.randomUUID().toString());
            //TODO remove duo
            message.setMsgId(msgId);
            message.setSender(from);
            message.setReceiver(to);
            if(!TextUtils.isEmpty(msg))
                message.setMsg(friendship.decodeMessage(msg));
            message.setMsgTime(time.getDisplayTimeDate());
            message.setDeliveryStatus(Const.MESSAGE_FAILED);
            int type = 0;
            switch (object.getInt(Const.TYPE))
            {
                case 0:
                    type= DataProvider.MessageType.INCOMING.ordinal();
                    break;
                case 1:
                    type = DataProvider.MessageType.INCOMING_IMAGE.ordinal();
                    break;
                case 2:
                    type = DataProvider.MessageType.INCOMING_VOICE_NOTE.ordinal();
                    break;
                case 3:
                    type = DataProvider.MessageType.INCOMING_FILE.ordinal();
                    break;

            }
            if(type>0)
            {
                com.jabuwings.database.realm.File file = realm.createObject(com.jabuwings.database.realm.File.class,UUID.randomUUID().toString());
                file.setAddress(fileUrl);
                file.setSize(size);
                message.setFile(file);
            }
            message.setMsgType(type);
            new Manager(context).storeLatestMessageDate(new Date().getTime());
            realm.commitTransaction();
            if(type>0)
                getMedia(context,object,from,0);
            realm.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void storeHubIncomingMessage
            (final Context context,final ParseObject object)
    {
        try {


            final Time time = new Time();
            final Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    int type = object.getInt(Const.TYPE);
                    String not_message = "A new Message";
                    String sender = object.getString(Const.SENDER);
                    String msg = object.getString(Const.MSG);
                    String hubId=object.getString(Const.HUB_ID);
                    final String msgId = object.getObjectId();
                    String fileUrl=null;int size;
                    if(object.getParseFile(Const.FILE)!=null)
                        fileUrl = object.getParseFile(Const.FILE).getUrl();
                    size = object.getInt(Const.SIZE);

                    switch (type)
                    {
                        case 0:
                            type = DataProvider.MessageType.INCOMING.ordinal();
                            break;
                        case 1:
                            type = DataProvider.MessageType.INCOMING_IMAGE.ordinal();
                            break;
                        case 2:
                            type = DataProvider.MessageType.INCOMING_VOICE_NOTE.ordinal();
                            break;
                        case 3:
                            type = DataProvider.MessageType.INCOMING_FILE.ordinal();
                            break;
                    }

                    HubMessage hubMessage = realm.createObject(HubMessage.class, UUID.randomUUID().toString());
                    hubMessage.setHubId(hubId);
                    hubMessage.setMsg(msg);
                    hubMessage.setSender(sender);
                    hubMessage.setMsgId(msgId);
                    hubMessage.setTime(time.getDisplayTimeDate());
                    hubMessage.setDeliveryStatus(Const.MESSAGE_FAILED);
                    hubMessage.setType(type);
                    if(type>0)
                    {
                        com.jabuwings.database.realm.File file = realm.createObject(com.jabuwings.database.realm.File.class,UUID.randomUUID().toString());
                        file.setAddress(fileUrl);
                        file.setSize(size);
                        hubMessage.setFile(file);
                        getMedia(context,object,sender,1);
                    }
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void storeFeedItem(Context context, String author, String title, String content, String feedUrl, String feedPictureUrl)
    {
        ContentValues values= new ContentValues();
        values.put(DataProvider.COL_FEED_AUTHOR,author);
        values.put(DataProvider.COL_FEED_TITLE,title);
        values.put(DataProvider.COL_FEED_CONTENT,content);
        values.put(DataProvider.COL_FEED_URL,feedUrl);
        values.put(DataProvider.COL_FEED_PICTURE_URL,feedPictureUrl);
        context.getContentResolver().insert(DataProvider.CONTENT_URI_FEEDS, values);
        context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_FEEDS,null);

    }

    public static void deleteNotification(Context context,String notificationID)

    {

        ParseQuery.getQuery(Const.LOCAL_NOTIFICATIONS).fromLocalDatastore().getInBackground(notificationID, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(e==null)
                {
                    parseObject.unpinInBackground();
                }

            }
        });

        //context.getContentResolver().delete(Uri.withAppendedPath(DataProvider.CONTENT_URI_NOTIFICATIONS, notificationID), null, null);


    }

    public static void deleteHub(Context context,String hubId)
        {
        String where = DataProvider.COL_HUB_ID + "=?";
        String[] whereArgs = new String[]{hubId};
        Cursor c = new DatabaseManager(context).getWritableDatabase().query(true, DataProvider.TABLE_HUBS, null, where, whereArgs, null, null, null, null);
        if(c!=null)
            if(c.moveToFirst())
            {
                context.getContentResolver().delete(Uri.withAppendedPath(DataProvider.CONTENT_URI_HUBS, String.valueOf(c.getInt(c.getColumnIndex(DataProvider.COL_ID)))),null, null);
                new DatabaseManager(context).deleteHubMessages(hubId);
                c.close();
            }

    }
    public static void deleteFriend(Context context,String username)
    {
        Realm realm = Realm.getDefaultInstance();
        Friend friend = realm.where(Friend.class).equalTo(Const.USERNAME,username).findFirst();
        realm.beginTransaction();
        friend.deleteFromRealm();
        //TODO delete all messages
        realm.commitTransaction();
        realm.close();

    }

    @Deprecated
    public static void deleteMessage(Context context, String msgDatabaseId)
    {
        context.getContentResolver().delete(Uri.withAppendedPath(DataProvider.CONTENT_URI_MESSAGES, msgDatabaseId),null, null);
        context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_MESSAGES,null);
    }

    public static void deleteMessage(final Message message)
    {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        message.deleteFromRealm();
        realm.commitTransaction();
        realm.close();
    }

    public static void deleteHubMessage(Context context, String msgDatabaseId)
    {
        context.getContentResolver().delete(Uri.withAppendedPath(DataProvider.CONTENT_URI_HUB_MESSAGES, msgDatabaseId),null, null);
        context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_HUB_MESSAGES,null);
    }



}

