package com.jabuwings.intermediary;

import android.content.Context;
import android.util.Log;

import com.jabuwings.database.DataProvider;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.database.realm.Friend;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.JabuWingsFriend;
import com.jabuwings.models.JabuWingsHub;
import com.jabuwings.models.JabuWingsUser;
import com.jabuwings.models.MatricNo;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import io.realm.Realm;

/**
 * Created by Falade James on 9/26/2015 All Rights Reserved.
 */
public class UserQuery {
private ParseUser theUser;
boolean done;
    
    public UserQuery(String username){
        Log.d("Query","username is "+username);


try{
      theUser= ParseUser.getQuery().
                whereEqualTo(Const.USERNAME,username)
                .getFirst();
}
catch (ParseException e)
{
    e.getMessage();

    e.printStackTrace();


}


    }

    public UserQuery(MatricNo matricNo)

    {

        try{
        theUser=ParseUser.getQuery().
                whereEqualTo(Const.MATRIC_NO,matricNo.toString())
                .getFirst();}
        catch (ParseException e)
        {
            e.printStackTrace();
            e.getMessage();
        }


        }



  public static void getUserFriend(final String username, final Context context)
  {
      JabuWingsFriend.getQuery().whereEqualTo(Const.USERNAME,username)
              .getFirstInBackground(new GetCallback<JabuWingsFriend>() {
                  @Override
                  public void done(JabuWingsFriend friend, ParseException e) {
                            if(e==null) {
                                //Store friend on the local database
                                HouseKeeping.storeFriend(friend, context);
                                //Store friend on the local user object
                                new Manager(context).addFriend(username);
                            }
                      else{
                                e.printStackTrace();
                            }
                  }
              });
  }

  public static void getHub(String hubId, final Context context)
  {
      ParseQuery<ParseObject> query = new ParseQuery<>(Const.HUB_CLASS_NAME);
      query.getInBackground(hubId, new GetCallback<ParseObject>() {
          @Override
          public void done(ParseObject parseObject, ParseException e) {
              if (e == null) HouseKeeping.storeHub(context, new JabuWingsHub(parseObject));
          }
      });
  }


    public static void getUserWithMatricNo(MatricNo matricNo)

    {
        ParseUser.getQuery()
                .whereEqualTo(Const.MATRIC_NO,matricNo.toString())
                .getFirstInBackground(new GetCallback<ParseUser>() {
                    @Override
                    public void done(ParseUser parseUser, ParseException e) {

                    }
                });

    }






  

    
    public  String getUserDepartment(){
        return theUser.getString(Const.DEPARTMENT);
    }
    public  String getUserStatus()
    {return theUser.getString(Const.STATUS);}
    
    public  String getUserName()
    {return theUser.getString(Const.NAME);}
    public String getUsername()
    {return theUser.getString(Const.USERNAME);}
    public  String getUserState()
    {return theUser.getString(Const.STATE);}
    
    public  String getUserBirthday()
    {return theUser.getString(Const.BIRTHDAY);}
    
    public  String getUserLevel()
    {return theUser.getString(Const.LEVEL);}

    public String getUserProfilePictureUrl()
    {

        return
                theUser.getString(Const.PROFILE_PICTURE_URL);

       }
    
    public  String getUserMatricNo()
    {return theUser.getString(Const.MATRIC_NO);}
    
    
    private boolean makeSureTheUserIsValid()
    {


        if(theUser!=null)
        {
            return true;
        }

        else{
            return false;
        }

    }
    
    
    public static void queryUserPresentState(final Context context, final String username)
        
    {
       final Manager manager = new Manager(context);
        Realm realm = Realm.getDefaultInstance();
        Friend friend = realm.where(Friend.class).equalTo(Const.USERNAME, username).findFirst();

       final DatabaseManager databaseManager = manager.getDatabaseManager();
       final String presentStatus= friend.getStatus();
       final String isVerifiedAs=friend.getVerifiedAs();

        try {
            ParseUser.getQuery()
                    .whereEqualTo(Const.USERNAME, username)
                    .getFirstInBackground(new GetCallback<ParseUser>() {
                        @Override
                        public void done(ParseUser parseUser, ParseException e) {

                            if (e == null) {
                                JabuWingsUser jabuWingsUser = new JabuWingsUser(parseUser);
                                String currentStatus =jabuWingsUser.getStatus();
                                String currentProfilePictureURL = jabuWingsUser.getProfilePictureUrl();
                                long currentProfilePictureDate= jabuWingsUser.getProfilePictureDate();
                                ParseFile parseFile= jabuWingsUser.getProfilePicture();

                                if (currentProfilePictureURL != null) {
                                    //Log.d("UserQuery ","Profile Url obtained from manager for "+username+" is "+manager.getFriendProfilePictureUrl(username));
                                    if(currentProfilePictureDate!=manager.getFriendProfilePictureDate(username))
                                    {
                                        HouseKeeping.getProfilePicture(context,username,parseFile,currentProfilePictureDate);
                                    }
                                }
                                if(currentStatus!=null)
                                if (!currentStatus.equals(presentStatus)) {
                                    databaseManager.updateFriendStatus(username, currentStatus);
                                }

                                    if(jabuWingsUser.isVerified())
                                    {
                                        if(!jabuWingsUser.isVerifiedAs().equals(isVerifiedAs))
                                        databaseManager.setFriendVerified(username,jabuWingsUser.isVerifiedAs());
                                    }

                            }

                            else{
                                if(e.getCode()==ParseException.OBJECT_NOT_FOUND)
                                {
                                    //The User no longer exits.
                                    databaseManager.updateFriendshipStatus(username,Const.FRIENDSHIP_STATUS.GONE.ordinal());

                                }
                            }


                        }
                    });

        }
        catch (Exception e)
            {
                e.printStackTrace();
            }
            realm.close();
    }
    
    public static void queryHubState(final Context context, final String hubId)
    {
        final Manager manager = new Manager(context);
        final DatabaseManager databaseManager = manager.getDatabaseManager();
        final long presentProfilePictureDate=databaseManager.getHubProfilePictureDate(hubId);
        ParseQuery<ParseObject> hubQuery = new ParseQuery<>(Const.HUB_CLASS_NAME);
        hubQuery.getInBackground(hubId, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                    if(e==null)
                    {
                        parseObject.pinInBackground();

                        JabuWingsHub jabuWingsHub = new JabuWingsHub(parseObject);
                        ParseFile parseFile = jabuWingsHub.getProfilePicture();

                        long currentProfilePictureDate=jabuWingsHub.getProfilePictureDate();

                        if(currentProfilePictureDate!=presentProfilePictureDate && parseFile!=null)
                        {
                            HouseKeeping.getHubPicture(context,hubId,parseFile,currentProfilePictureDate);

                        }
                      context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_HUBS,null);
                    }


            }
        });
    }
    
    
    
    
}
