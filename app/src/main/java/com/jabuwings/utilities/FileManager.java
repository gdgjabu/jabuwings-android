package com.jabuwings.utilities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.FileUriExposedException;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.jabuwings.intelligence.lisa.Lisa;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Falade James on 10/29/2015 All Rights Reserved.
 */
public class FileManager {
    Context context;
    public FileManager(Context context)
    {
        this.context=context;
    }

    public boolean isStorageAvailable()

    {
        String mediaCardState=   Environment.getExternalStorageState();
        if(mediaCardState.equals(Environment.MEDIA_MOUNTED))


        {
            return true;

        }

        else{
            return false;
        }
    }


    public File getFriendPictureFile(String username)
    {
        File image;

        File sd = Environment.getExternalStorageDirectory();
        File dir = new File(sd.getAbsolutePath()+Const.PUBLIC_IMAGE_DIRECTORY);
        try{

        dir.mkdirs();
            image = new File(dir, Lisa.generateImageFileName(username)+".jpg");
            return image;
        }

        catch (Exception e )
        {
            e.printStackTrace();
            return null;
        }


    }

    public  File getFriendTransferPicture(String username){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM)	, Const.APP_NAME);
        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("jabuwings", "failed to create directory");
                Toast.makeText(context,"Failed to create directory",Toast.LENGTH_SHORT).show();

                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator + Const.PICTURES_DIRECTORY_NAME + File.separator +
                username+"_"+timeStamp+ ".jpg");

        return mediaFile;
    }



    public static String getPictureShareDirectory(String username)
    {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM)	, Const.APP_NAME);
        return mediaStorageDir.getAbsolutePath()+File.separator+Const.PICTURES_DIRECTORY_NAME+File.separator+username;
    }

    public File getProfilePictureFile()
    {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), Const.APP_NAME);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("jabuwings", "failed to create directory");

                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "Profile Picture_"+timeStamp+ ".jpg");

        return mediaFile;
    }

    public void openFile(File file)
    {
        //TODO implement FileProvider to fix android 7 bug
        MimeTypeMap mimeTypeMap=MimeTypeMap.getSingleton();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String mimeType=mimeTypeMap.getMimeTypeFromExtension(fileExt(file.getAbsolutePath()).substring(1));
        intent.setDataAndType(Uri.fromFile(file),mimeType);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        try{
            context.startActivity(intent);
        }
        catch (ActivityNotFoundException e)
        {
            new Manager(context).errorToast("No installed application can open this file.");
        }

    }

    private String fileExt(String url)
    {
        if(url.indexOf("?")>-1)
        {
            url=url.substring(0,url.indexOf("?"));
        }
        if(url.lastIndexOf(".")==-1)
            return null;
        else{
            String ext=url.substring(url.lastIndexOf(".")+1);
            if(ext.indexOf("%")>-1)
            {
                ext=ext.substring(0,ext.indexOf("%"));
            }
            if(ext.contains("/"))
            {
                ext=ext.substring(0,ext.indexOf("/"));
            }
            return ext.toLowerCase();
        }
    }

    }


