package com.jabuwings.utilities;

import java.util.Enumeration;
import java.util.Formatter;
import java.util.Hashtable;

/**
 * @author Falade James
 * This class was created by Falade James.
 * This is a proprietary software and may not be transferred by any means without the consent of the author.
 */
public class CGPA{
public Hashtable<String,Integer> courseDetails;
Enumeration enumCourses;
String[][] courses;
String key;
    char[] grades;
    int totalCourses;
    double calculatedCGPA=0;
    public CGPA(Hashtable<String,Integer> courseDetails,char[] grades)
    {
        this.courseDetails=courseDetails;
        this.grades=grades;
        totalCourses=grades.length;
        courses= new String[totalCourses][3];
    }
    public double getCGPA()
    {
        enumCourses= courseDetails.keys();
        int position=0;
        while(enumCourses.hasMoreElements())
        {
            key= (String)enumCourses.nextElement();
            String courseCode=key;
            int courseUnit=courseDetails.get(key);
            char courseGrade=grades[position];
            addCourse(courseCode,courseUnit,courseGrade,position);
            position++;
        }
        return calculate();
    }



    private  void addCourse(String courseCode, int courseUnit, char courseGrade, int position)
    {
        courses[position][0]=courseCode;
        courses[position][1]=String.valueOf(courseUnit);
        courses[position][2]=String.valueOf(courseGrade);
    }


    private double calculate()
    {
        double totalUnit=0;
        double totalGrade=0;
        for(int i= 0; i<=totalCourses-1;i++)
        {
            totalUnit+=Integer.valueOf(courses[i][1]);
        }

        for(int i=0; i<=totalCourses-1; i++)
        {
            totalGrade+= Double.valueOf(courses[i][1])  * getIntegerGrade(courses[i][2].charAt(0));
        }
        double cgpa= totalGrade/totalUnit;
        calculatedCGPA=cgpa;
        return cgpa;
    }

    public int getIntegerGrade(char letterGrade)
    {
        switch (letterGrade)
        {
            case 'A':
                return 5;
            case 'B':
                return 4;
            case 'C':
                return 3;
            case 'D':
                return 2;
            case 'E':
                return 1;
            case 'F':
                return 0;
            default:
                    return 0;
        }

    }

    public double cumulate(double newCGPA)
    {
        return (calculatedCGPA+newCGPA)/2;
    }

    @SuppressWarnings("UnusedDeclaration")
    public static double average(double currentCGPA,double newGPA)
    {
    	return (currentCGPA+newGPA)/2;
    }


   public enum AcademicClasses{
        FIRST_CLASS,SECOND_CLASS_UPPER_DIVISION,SECOND_CLASS_LOWER_DIVISION,THIRD_CLASS
    }
}
