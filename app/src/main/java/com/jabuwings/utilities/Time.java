package com.jabuwings.utilities;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Falade James on 9/2/2015.
 */

public  class Time {
String s= "28-12-2015 19:10:47";
Calendar calendar;
SimpleDateFormat sdf;
String LOG_TAG=Time.class.getSimpleName();
    public Time(){
        calendar = Calendar.getInstance();
        sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);
    }

    public SimpleDateFormat getFormat()
    {
        return sdf;
    }
    public String parseTime(Date time)
    {
      return  parseTime(sdf.format(time));
    }
    public String  parseTime(String dateTime)

    {
        switch (isTodayOrYesterday(dateTime))
        {
            case 0:
                return getTimeFromDateTime(dateTime);
            case 1:
                return "YESTERDAY, "+getTimeFromDateTime(dateTime);


            default: return getMonthDate(dateTime);
        }


    }

    //bug found use calendar
    @Deprecated
    public String getDisplayTime() {
        String date = DateFormat.getTimeInstance().format(new Date());
        StringBuilder builder = new StringBuilder(date);
        return  builder.delete(5,date.charAt(date.length()-1)).toString();
    }

    public String getDisplayTimeDate()
    {
        //String date = DateFormat.getDateTimeInstance().format(sdf);
    return  sdf.format(new Date());

    }

    /**
     * Used to get the hour in a String like "9:00","12:00"
     * @param time The time in the above format
     * @return The hour in integers. 9,12
     */
    public int getHour(String time)
    {
        if(time.length()==5)
        {
            return Integer.valueOf( new StringBuilder(time).delete(2,time.length()).toString());
        }
        else{
            return Integer.valueOf( new StringBuilder(time).delete(1,time.length()).toString());
        }

    }


    public int getHour()
    {
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public int getMinute()

    {
        return calendar.get(Calendar.MINUTE);
    }



    private String getDayFromDateTime(String dateTime)

    {
        StringBuilder builder;
        String[] date_time=dateTime.split(" ");
        String date=date_time[0];
        builder=new StringBuilder(date);
        String dayOfMonth =builder.delete(2,10).toString();
        Log.d(LOG_TAG, "Day of the month is " + dayOfMonth);
        return dayOfMonth;

    }

    private String getTimeFromDateTime(String dateTime)
    {
        String[] date_time=dateTime.split(" ");
        String timeSeconds=date_time[1];
        StringBuffer builder= new StringBuffer(timeSeconds);
        builder.delete(5, 8);
        return   builder.toString();

    }

    private String getMonthDate(String dateTime)
    {
        String[] date_time=dateTime.split(" ");
        String date=date_time[0];
        String timeSeconds=date_time[1];
        StringBuilder builder = new StringBuilder(timeSeconds);
        builder.delete(5, 8);
        String time=  builder.toString();





        builder=new StringBuilder(date);
        String dayOfMonth =builder.delete(2,10).toString();



        builder=new StringBuilder(date);
        builder.delete(0, 3);
        builder.delete(2, 7);
        int month=Integer.valueOf(builder.toString());



        return getMonth(month)+" "+dayOfMonth+", "+time;

    }



    public String getMonth(int number)
    {
        switch (number)
        {
            case 1:
                return "JAN";
            case 2:
                return "FEB";
            case 3:
                return "MAR";
            case 4:
                return "APR";
            case 5:
                return "MAY";
            case 6:
                return "JUN";
            case 7:
                return "JUL";
            case 8:
                return "AUG";
            case 9:
                return "SEP";
            case 10:
                return "OCT";
            case 11:
                return "NOV";
            case 12:
                return "DEC";
            default:
                return "";


        }
    }


    public String getFeedTime(Date date)
    {
        return sdf.format(date);
    }
    private int isTodayOrYesterday(String dateTime)
    {
        String[] date_time=dateTime.split(" ");

        String date=date_time[0];
        String timeSeconds=date_time[1];
        StringBuilder builder;
        builder=new StringBuilder(date);
        int dayOfMonth = Integer.valueOf(builder.delete(2,10).toString());

        builder=new StringBuilder(date);
        builder.delete(0,3); builder.delete(2, 7);

        int month=Integer.valueOf(builder.toString());


        builder=new StringBuilder(date);
        builder.delete(0, 6);
        Integer year=Integer.valueOf(builder.toString());





        int nowDay=calendar.get(Calendar.DAY_OF_MONTH);
        int nowMonth=calendar.get(Calendar.MONTH)+1;
        int nowYear=calendar.get(Calendar.YEAR);


        if(dayOfMonth==nowDay&& month==nowMonth&&year==nowYear)
        {
            return 0;
        }

        if(dayOfMonth==nowDay-1&& month==nowMonth&&year==nowYear)
        {
            return 1;
        }

        else{
            return 2;
        }



    }


    public void pTime(String dateTime)
    {
        String[] date_time=dateTime.split(" ");
        String date=date_time[0];
        String timeSeconds=date_time[1];



        StringBuilder builder = new StringBuilder(timeSeconds);
        builder.delete(5, 8);
        String time=  builder.toString();




        builder=new StringBuilder(date);
       String dayOfMonth =builder.delete(2,10).toString();

        builder=new StringBuilder(date);
        builder.delete(0, 3);
                 builder.delete(2,7);
                String month=builder.toString();

        builder=new StringBuilder(date);
        builder.delete(0, 6);





    }


    public int getDay()
    {
       return calendar.get(Calendar.DAY_OF_WEEK);
    }

    public int getMonth()
    {
        return calendar.get(Calendar.MONTH);
    }
    public int getYear()
    {
        return calendar.get(Calendar.YEAR);
    }


    public String getCalendarDay()
    {
        int day =calendar.get(Calendar.DAY_OF_WEEK);
        switch (day)
        {
            case 1:
                return "SUNDAY";
            case 2:
                return "MONDAY";
            case 3:
                return "TUESDAY";
            case 4:
                return "WEDNESDAY";
            case 5:
                return "THURSDAY";
            case 6:
                return "FRIDAY";
            case 7:
                return "SATURDAY";
            default:return "";


        }
    }
    public String getTimetableDay()
    {
        int day =calendar.get(Calendar.DAY_OF_WEEK);
        switch (day)
        {
            case 1:
                return "SUN";
            case 2:
                return "MON";
            case 3:
                return "TUE";
            case 4:
                return "WED";
            case 5:
                return "THU";
            case 6:
                return "FRI";
            case 7:
                return "SAT";
            default:return "";


        }
    }

}


