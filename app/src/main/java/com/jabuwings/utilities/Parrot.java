package com.jabuwings.utilities;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.util.Log;

import com.jabuwings.management.Manager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Falade James on 6/5/2016 All Rights Reserved.
 */
public class Parrot {
    boolean initialised=false;
    TextToSpeech tts;
    List<String> queue=new ArrayList<>();
    Manager manager;
    Context c;
    private String singleQueue;
    public Parrot(final Context c)
    {
        manager=new Manager(c);
        this.c=c;
        tts= new TextToSpeech(c, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                initialised= status==TextToSpeech.SUCCESS;
                manager.log("Parrot","Initialised");
                if(singleQueue!=null)
                tts.speak(singleQueue,TextToSpeech.QUEUE_FLUSH,null);

            }
        });
    }

    public void speak(final String text)
    {
        if(initialised)
        if(tts.speak(text,TextToSpeech.QUEUE_FLUSH,null)==TextToSpeech.SUCCESS)
        {
            manager.log("Parrot","Spoken "+text);
        }else singleQueue=text;

    }
    
    public void shutdown()
    {
        tts.shutdown();
    }
    
}
