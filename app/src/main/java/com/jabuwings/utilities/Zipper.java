package com.jabuwings.utilities;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Created by Falade James on 10/15/2016 All Rights Reserved.
 */
public class Zipper {

    int BUFFER = 2048;

    public void zipFile(String[] _files, String zipFileName) {
        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(zipFileName);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
            byte data[] = new byte[BUFFER];

            for (int i = 0; i < _files.length; i++) {
                FileInputStream fi = new FileInputStream(_files[i]);
                origin = new BufferedInputStream(fi, BUFFER);
                ZipEntry entry = new ZipEntry(_files[i].substring(_files[i].lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;
                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }
            out.close();
        } catch (Exception c) {
            c.printStackTrace();
        }
    }

    public void unzip(String _zipFile, String _targetLocation)
    {
        //TODO check if _targetLocation exists

        try{
            FileInputStream fin = new FileInputStream(_zipFile);
            ZipInputStream zin= new ZipInputStream(fin);
            ZipEntry ze=null;
            while ((ze=zin.getNextEntry())!=null){
                if(ze.isDirectory()){
                    //check
                    ze.getName();
                }
                else{
                    FileOutputStream fout= new FileOutputStream(_targetLocation+ze.getName());
                    for(int c =zin.read(); c!=-1; c=zin.read())
                    {
                        fout.write(c);
                    }

                    zin.closeEntry();
                    fout.close();
                }
            }
            zin.close();
        }
        catch (Exception e)
        {}
    }
}
