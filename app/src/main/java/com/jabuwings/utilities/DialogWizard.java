package com.jabuwings.utilities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.Manager;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

/**
 * Created by Falade James on 11/21/2015 All Rights Reserved.
 */
public class DialogWizard
{
Activity context;


    public DialogWizard(Activity context)

    {
        this.context=context;
    }





    public MaterialDialog showConfirmationDialog(String title, String content, MaterialDialog.SingleButtonCallback onPositiveClick, @Nullable MaterialDialog.SingleButtonCallback onNegativeClick)
    {

        boolean a=true;
        return new MaterialDialog.Builder(context)
                .title(title)
                .content(content)
                .positiveText(context.getString(R.string.confirm))
                .negativeText(context.getString(R.string.cancel))
                .onPositive(onPositiveClick)
                .onNegative(onNegativeClick==null? new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                } : onNegativeClick)
                .show();
    }

    public SweetAlertDialog showSimpleDialog(String title, String content)
    {

        SweetAlertDialog dialog =new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
        dialog.setTitleText(title)
                .setContentText(content)
                .show();
        return dialog;
    }

    public SweetAlertDialog showErrorDialog(String title,String content)
    {
        SweetAlertDialog dialog =new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                dialog.setTitleText(title)
                .setContentText(content)
                .show();
        return dialog;
    }

    public SweetAlertDialog showSuccessDialog(String title,String content)
    {
        SweetAlertDialog dialog =new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        dialog.setTitleText(title)
                .setContentText(content)
                .show();
        return dialog;
    }

    public SweetAlertDialog showWarningDialog(String title,String content) {
        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        dialog.setTitleText(title)
                .setContentText(content)
                .show();
        return dialog;
    }






    public SweetAlertDialog showSimpleProgress(CharSequence title, CharSequence content)
    {
        return showProgress(title,content,false);
    }


    public SweetAlertDialog showSweetProgress(CharSequence content)
    {
       SweetAlertDialog pDialog = new SweetAlertDialog(context,SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(content.toString());
        pDialog.setCancelable(false);
        pDialog.show();
        return pDialog;
    }


    public SweetAlertDialog showProgress(CharSequence title, final CharSequence content,boolean cancellable)
    {
        final SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        dialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        dialog.setTitleText(title.toString());
        dialog.setContentText(content.toString());
        dialog.showCancelButton(cancellable);
        if(cancellable)
        dialog.setCancelText(context.getString(R.string.dismiss));
        dialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
            }
        });
        dialog.setCancelable(cancellable);
        dialog.show();
        return dialog;

    }

    public MaterialDialog showCancellableProgress(CharSequence title, CharSequence content,String positive, String negative)
    {
        return new MaterialDialog.Builder(context)
                .title(title)
                .content(content)
                .cancelable(false)
                .progress(true, 0)
                .positiveText(positive)
                .negativeText(negative)
                .show();
    }



    public SweetAlertDialog showSimpleProgress(int titleResId, int contentResId)
    {

        return showProgress(context.getString(titleResId),context.getString(contentResId),false);
    }

    public void requestForgottenPassword()
    {
        final SweetAlertDialog progress= showSimpleProgress(R.string.please_wait,R.string.requesting_password_change);
        final Manager manager=new Manager(context);
        ParseUser.requestPasswordResetInBackground(manager.getEmail(), new RequestPasswordResetCallback() {
            @Override
            public void done(ParseException e) {
                progress.dismiss();
                if(e==null)
                {
                    showSuccessDialog(context.getString(R.string.success),"Reset Instructions has been sent to this email: \n"+manager.getEmail());
                }
                else{
                    ErrorHandler.handleError(context,e);
                }

            }
        });
    }
}