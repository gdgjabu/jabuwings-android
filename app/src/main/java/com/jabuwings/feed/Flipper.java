package com.jabuwings.feed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.jabuwings.R;
import com.jabuwings.models.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Falade James on 8/6/2016 All Rights Reserved.
 */
public class Flipper extends BaseAdapter {

    LayoutInflater inflater;
    List<Item> feedItems;
    Context context;
    public Flipper(Context context)
    {
        inflater = LayoutInflater.from(context);
        this.context=context;
        feedItems=new ArrayList<>();
        feedItems.add(new Item("Hello",context.getString(R.string.lorem_ipsum)));
        feedItems.add(new Item("Hey",context.getString(R.string.lorem_ipsum)));
        feedItems.add(new Item("Hmm",context.getString(R.string.lorem_ipsum)));
        feedItems.add(new Item("Hi",context.getString(R.string.lorem_ipsum)));
        feedItems.add(new Item("Hulo",context.getString(R.string.lorem_ipsum)));

    }
    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    /* @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View layout = convertView;
        if (convertView == null) {
            layout = inflater.inflate(R.layout.complex1, null);
            AphidLog.d("created new view from adapter: %d", position);
        }

        final Item data = feedItems.get(position % feedItems.size());

        UI
                .<TextView>findViewById(layout, R.id.title)
                .setText(AphidLog.format("%d. %s", position, data.item));

        UI
                .<ImageView>findViewById(layout, R.id.photo);

        UI
                .<TextView>findViewById(layout, R.id.description)
                .setText(data.value);

        UI
                .<Button>findViewById(layout, R.id.wikipedia)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

        return layout;
    }*/
}
