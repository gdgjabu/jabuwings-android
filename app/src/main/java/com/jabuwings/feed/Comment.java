package com.jabuwings.feed;

/**
 * Created by Falade James on 9/18/2016 All Rights Reserved.
 */
public class Comment {
    private String username,comment,pictureUrl;
    public Comment(String username, String comment, String pictureUrl)
    {
        this.username=username;this.comment=comment;this.pictureUrl=pictureUrl;
    }

    public String getUsername(){return username;}
    public String getComment(){return comment;}
    public String getPictureUrl(){return pictureUrl;}
}
