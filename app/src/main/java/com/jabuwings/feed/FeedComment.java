package com.jabuwings.feed;

import java.util.Date;

/**
 * Created by Falade James on 9/18/2016 All Rights Reserved.
 */
public class FeedComment {
    private String content,author;
    private Date timeStamp;

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
    public String getContent(){return content;}

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
