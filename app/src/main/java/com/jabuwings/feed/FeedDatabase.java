package com.jabuwings.feed;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.CursorRecyclerViewAdapter;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.main.JabuWingsActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

/**
 * Created by Falade James on 11/16/2015 All Rights Reserved.
 */
@Deprecated
public class FeedDatabase extends CursorRecyclerViewAdapter<FeedDatabase.ViewHolder>{
    View itemLayout;
    Context context;
    Manager manager;
    LayoutInflater inflater;
    public FeedDatabase(Context context, Cursor c)
    {
        super(context, c);
        this.context=context;
        manager=new Manager(context);
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, Cursor cursor) {
        String title,author,time,content,authorPictureUrl, feedPictureUrl,feedLinkUrl,feedId;
        int id;
        long createdAt;
        id=cursor.getInt(cursor.getColumnIndex(DataProvider.COL_ID));
        title=cursor.getString(cursor.getColumnIndex(DataProvider.COL_FEED_TITLE));
        author=cursor.getString(cursor.getColumnIndex(DataProvider.COL_FEED_AUTHOR));
        content=cursor.getString(cursor.getColumnIndex(DataProvider.COL_FEED_CONTENT));
        time=cursor.getString(cursor.getColumnIndex(DataProvider.COL_FEED_TIME));
        authorPictureUrl=cursor.getString(cursor.getColumnIndex(DataProvider.COL_FEED_AUTHOR_PICTURE_URL));
        feedPictureUrl=cursor.getString(cursor.getColumnIndex(DataProvider.COL_FEED_PICTURE_URL));
        feedLinkUrl=cursor.getString(cursor.getColumnIndex(DataProvider.COL_FEED_URL));
        createdAt=cursor.getLong(cursor.getColumnIndex(DataProvider.COL_FEED_CREATED_AT));
        feedId=cursor.getString(cursor.getColumnIndex(DataProvider.COL_FEED_ID));
        JabuWingsActivity.setUpTextViews(context,holder.tvAuthor,holder.tvTitle,holder.tvContent,holder.tvTimeStamp,holder.tvFeedUrl);
        manager.storeFeedDatabaseId(feedId, id);
//        manager.storeLatestFeed(id, createdAt);

        //Log.d("FeedDb", "Feed Item " + "author: " + author);
        holder.tvAuthor.setText(author);
        holder.tvTitle.setText(title);
        holder.tvContent.setText(content);
        holder.tvTimeStamp.setText(new Time().parseTime(time));
        holder.tvFeedUrl.setText(feedLinkUrl);
        holder.tvFeedDbId.setText(String.valueOf(id));

        if(feedPictureUrl==null)  holder.ivFeedPicture.setVisibility(View.GONE);
        loadImage(feedPictureUrl, authorPictureUrl,holder);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView =inflater.inflate(R.layout.feed_item,parent,false);
        return new ViewHolder(itemView, new FeedClicks() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, FeedDetails.class).putExtra(Const.ID,Integer.valueOf(((TextView)v.findViewById(R.id.feed_db_id)).getText().toString())));
            }
        });
    }


    private void loadImage(final String feedPictureUrl, final String authorProfilePictureURl, final ViewHolder holder)
    {
        Picasso.with(context)
                .load(feedPictureUrl)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.ivFeedPicture, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(context)
                                .load(feedPictureUrl)
                                .error(R.drawable.default_avatar)
                                .into(holder.ivFeedPicture, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError() {
                                        Log.v("Picasso", "Could not fetch image");
                                    }
                                });
                    }
                });

        Picasso.with(context)
                .load(authorProfilePictureURl)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.ivAuthorPicture, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(context)
                                .load(authorProfilePictureURl)
                                .error(R.drawable.default_avatar)
                                .into(holder.ivAuthorPicture, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                    }

                                    @Override
                                    public void onError() {

                                    }
                                });
                    }
                });

    }
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTitle;
        TextView tvAuthor;
        ImageView ivAuthorPicture;
        ImageView ivFeedPicture;
        TextView tvContent;
        TextView tvFeedUrl;
        TextView tvTimeStamp;
        TextView tvFeedDbId;
        FeedClicks feedClicks;
        public ViewHolder(View v, FeedClicks feedClicks)
        {
            super(v);
            this.feedClicks=feedClicks;
            v.setOnClickListener(this);
           tvTitle=(TextView) v.findViewById(R.id.feed_title);
           tvAuthor=(TextView) v.findViewById(R.id.author);
           tvContent=(TextView) v.findViewById(R.id.content);
           tvFeedUrl=(TextView) v.findViewById(R.id.feedURl);
           tvTimeStamp=(TextView) v.findViewById(R.id.timestamp);
           ivAuthorPicture=(ImageView) v.findViewById(R.id.profilePic);
           ivFeedPicture=(ImageView) v.findViewById(R.id.feedImage);
           tvFeedDbId=(TextView)v.findViewById(R.id.feed_db_id);
        }


        @Override
        public void onClick(View v) {
            feedClicks.onClick(v);
        }
    }



    public interface FeedClicks{
        void onClick(View v);
    }



}
