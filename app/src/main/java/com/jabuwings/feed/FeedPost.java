package com.jabuwings.feed;

import android.content.Context;
import android.text.TextUtils;

import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.JabuWingsPublicUser;
import com.jabuwings.utilities.Utils;
import com.parse.FunctionCallback;
import com.parse.ParseClassName;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class FeedPost implements Serializable{


	private String id, title, content, picture, author;
	private int likes, comments;
	private Date timestamp;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public int getComments() {
		return this.comments;
	}

	public void setComments(int comments) {
		this.comments = comments;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public boolean hasPicture()
	{
		return !TextUtils.isEmpty(getPicture());
	}
	public boolean isLiked(String id)
	{
		try {
			ParseQuery.getQuery(Const.Feed.LIKES).fromLocalDatastore().whereEqualTo(Const.ID, id).getFirst();
			return true;
		}
		catch (ParseException e)
		{
			return false;
		}



	}

	public int getLikesCount()
	{
		return likes;
	}

	public String getShareUrl(String baseUrl, String id)
	{
		return baseUrl+"/feed?id="+id;
	}
	public int getCommentsCount()
	{
		return comments;
	}

	public void like(String id )
	{
		ParseObject object = new ParseObject(Const.Feed.LIKES);
		object.put(Const.ID, id);
		object.pinInBackground();
	}
}
