package com.jabuwings.feed;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.utilities.Time;
import com.squareup.picasso.Picasso;




public class MainFragment extends Fragment {
TextView tvFeedContent;
ImageView ivFeedPicture;
TextView tvFeedDate,tvFeedAuthor,tvFeedLink;
FloatingActionButton fabFeed;
CollapsingToolbarLayout toolbarLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.feed_details_fragment, container, false);
        tvFeedContent=(TextView)rootView.findViewById(R.id.feed_content);
        ivFeedPicture=(ImageView)rootView.findViewById(R.id.feed_picture);
        tvFeedAuthor=(TextView) rootView.findViewById(R.id.feed_author);
        tvFeedDate=(TextView) rootView.findViewById(R.id.feed_date);
        tvFeedLink=(TextView) rootView.findViewById(R.id.feed_url);
        fabFeed=(FloatingActionButton)rootView.findViewById(R.id.feed_fab);
        toolbarLayout=(CollapsingToolbarLayout)rootView.findViewById(R.id.toolbar_layout);
        toolbarLayout.setTitle(FeedDetails.feedTitle);
        tvFeedContent.setText(FeedDetails.feedContent);
        tvFeedAuthor.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/AlexBrush-Regular.ttf"));
        tvFeedAuthor.setText(FeedDetails.feedAuthor);
        tvFeedLink.setText(FeedDetails.feedUrl);
        tvFeedDate.setText(new Time().parseTime(FeedDetails.feedDate));
        String feedPictureUrl=FeedDetails.feedPictureUrl;
        if(feedPictureUrl!=null)
        Picasso.with(getActivity())
                .load(FeedDetails.feedPictureUrl)
                .placeholder(R.drawable.logo)
                .into(ivFeedPicture);

        return rootView;
    }
}
