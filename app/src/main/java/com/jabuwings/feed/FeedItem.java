package com.jabuwings.feed;

import android.content.Context;

import com.jabuwings.error.ErrorHandler;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.JabuWingsPublicUser;
import com.jabuwings.utilities.Utils;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@ParseClassName(Const.FEED_CLASS_NAME)
public class FeedItem extends ParseObject implements Serializable{

    public int likesCount=0,commentsCount=0; boolean liked=false;
	public String getId() {
		return getObjectId();
	}


	public String getTitle() {
		return getString(Const.TITLE);
	}

	public String getContent() {
		return getString(Const.Feed.CONTENT);
	}

	public int getLikesCount(){ return getInt(Const.Feed.LIKES_COUNT);}

    public void incrementLikesCount(){ likesCount++;}

	public int getCommentsCount(){ return getInt(Const.Feed.COMMENTS_COUNT);}

	public String getUsername(){return getString(Const.USERNAME);}

	public String getName(){return getString(Const.NAME);}

    public int getPostedBy(){return getInt(Const.Feed.POSTED_BY);}

	public ParseObject getAuthor(){return getParseObject(Const.Feed.AUTHOR_PUBLIC);}

    public String getAuthorUrl(){return getString(Const.Feed.AUTHOR_URL);}

    public String getLocalAuthorUrl(String baseUrl){return getString(Const.Feed.AUTHOR_URL).replace("localhost",baseUrl);}


	public ParseRelation<JabuWingsPublicUser> getLikes(){return getRelation(Const.Feed.LIKES);}



	public String getImageUrl(){return getString(Const.Feed.PICTURE_URL);}

	public String getLocalUrl(){return Utils.getDeviceName().equals("SAMSUNG")? getImageUrl().replace("localhost","10.0.2.2") : getImageUrl().replace("localhost","192.168.1.11");}

	public ParseFile getImage(){return getParseFile(Const.Feed.PICTURE_FILE);}

	public Date getDate(){return getCreatedAt();}

    public String getShareUrl(String baseUrl)
    {
       return baseUrl+"/feed?id="+getObjectId();
    }

	public boolean isPicture(){return getImageUrl()!=null;}

	public List<String> getLikesId()
	{
		return getList("Likes");
	}
    public boolean isLiked(){
		return getLikesId()!=null && getLikesId().contains(ParseUser.getCurrentUser().getObjectId());
		}



	public void like(final Context context){
		liked=true;
		put(Const.Feed.IS_LIKED,true);
        increment(Const.Feed.LIKES_COUNT);
		pinInBackground();
		HashMap<String,Object> params= new HashMap<>();
		params.put(Const.Feed.FEED_ID,getObjectId());
		ParseCloud.callFunctionInBackground("feedLikesAdd", params, new FunctionCallback<String>() {
			@Override
			public void done(String s, ParseException e) {
				if(e==null)
				{
					new Manager(context).log("Like saved");
				}
				else{
					liked=false;
					put(Const.Feed.IS_LIKED,false);
					pinInBackground();

				}
			}
		});

    }

    public void comment(final Context context,String comment){

		HashMap<String,Object> params= new HashMap<>();
		params.put(Const.Feed.FEED_ID,getObjectId());
        params.put(Const.CONTENT,comment);
		ParseCloud.callFunctionInBackground("feedCommentsAdd", params, new FunctionCallback<String>() {
			@Override
			public void done(String s, ParseException e) {
				if(e==null)
				{
					new Manager(context).log("Comment saved");
				}
				else{
					ErrorHandler.handleError(context,e);

				}
			}
		});



    }



    public static ParseQuery<FeedItem> getQuery(){return ParseQuery.getQuery(FeedItem.class);}


}
