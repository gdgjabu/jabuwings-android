package com.jabuwings.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.jabuwings.management.Manager;
import com.parse.CountCallback;

/**
 * Created by Falade James on 23/07/2015.
 */
public class DataProvider extends ContentProvider {

    public static final String COL_ID = "_id";


    /**
     * The name of the table that stores messages
     */
    public static final String TABLE_MESSAGES = "messages";

    /**
     * The unique id of the message provided by the server
     */
    public static final String COL_MESSAGE_ID="msgId";

    /**
     * The message sent/received
     */
    public static final String COL_MSG = "msg";

    /**
     * The username of the user sending the message
     */

    public static final String COL_FROM = "username";

    /**
     * The username of the user the message was sent to
     */
    public static final String COL_TO = "username2";


    /**
     * The time the message was sent
     */
    public static final String COL_TIME = "time";
    public static final String COL_AT = "at";

    /**
     * The status of a message being sent @code{MessageType}
     */
    public static final String COL_DELIVERY_STATUS="delivery_status";

    /**
     * The URL of the file stored in the phone
     */
    public static final String COL_FILE_LOCAL_URL ="file_url";

    /**
     * The URL of the file stored online.
     * The File may be deleted online immediately the user downloads the file.
     * Hence, the address may point to an invalid file #404 error.
     */
    public static final String COL_FILE_ADDRESS="file_address";

    /**
     * The progress of a file being sent/received
     */
    public static final String COL_FILE_PROGRESS="file_progress";
    public static final String COL_FILE_SIZE="file_size";
    public static final String COL_FILE_NAME="file_name";
/********************End of Messages Table********************************************************************************/

    public static final String COL_TYPE	= "type";

    public static final String TABLE_FRIENDS = "friends";
    public static final String COL_NAME = "name";
    public static final String COL_USERNAME = "username";
    public static final String COL_FRIEND_OF="friend_of";
    public static final String COL_MATRIC_NO="matric_no";
    public static final String COL_TYPING="typing";
    public static final String COL_VERIFIED_AS="verified_as";
    public static final String COL_COUNT = "count";
    public static final String COL_DEPARTMENT="department";
    public static final String COL_STATUS="status";
    public static final String COL_STATE="state";
    public static final String COL_LEVEL= "level";
    public static final String COL_BIRTHDAY="birthday";
    public static final String COL_LAST_MESSAGE="last_message";
    public static final String COL_LAST_MESSAGE_TIME_STAMP ="last_message_time_stamp";
    public static final String COL_LAST_MESSAGE_STATUS="last_message_status";
    public static final String COL_LAST_MESSAGE_TYPE="last_message_type";
    public static final String COL_PROFILE_PICTURE_URL="profile_picture_url";
    public static final String COL_PROFILE_PICTURE_DATE="profile_picture_date";
    public static final String COL_FRIENDSHIP_STATUS="friendship_status";



    public static final String TABLE_HUBS="hubs";
    /**
     * The display title of the hub subject to change
     */
    public static final String COL_HUB_TITLE ="hub_title";

    /**
     * The unique id of the hub which normally does not change
     */
    public static final String COL_HUB_ID ="hub_id";
    public static final String COL_HUB_PROFILE_PICTURE_URL="profile_picture_url";
    public static final String COL_HUB_PROFILE_PICTURE_FILE_URL="profile_picture_file_url";
    public static final String COL_HUB_DESC="hub_desc";
    public static final String COL_HUB_ADMINS="hub_admins";
    public static final String COL_HUB_MEMBERS="hub_members";
    public static final String COL_HUB_CHIEF_ADMIN="hub_chief_admin";
    public static final String COL_HUB_LAST_MESSAGE="last_message";
    public static final String COL_HUB_LAST_MESSAGE_TIME_STAMP ="last_message_time_stamp";
    public static final String COL_HUB_LAST_MESSAGE_TYPE ="last_message_type";
    public static final String COL_HUB_INSTANCE_TIME="instance_time";


    public static final String TABLE_NOTIFICATIONS ="notifications";
    public static final String COL_NOT_MESSAGE="msg";
    public static final String COL_NOT_TITLE= "not_title";
    public static final String COL_NOT_SENDER ="not_username";
    public static final String COL_NOT_TIME="time";


    public static final String TABLE_HUB_MESSAGES = "hub_messages";
    public static final String COL_HUB_MESSAGE ="hub_msg";
    public static final String COL_HUB_REPLY ="hub_reply";
    public static final String COL_HUB_MESSAGE_SENDER ="hub_msg_sender";



    public static final String TABLE_FEEDS="feeds";
    public static final String COL_FEED_AUTHOR="author";
    public static final String COL_FEED_TITLE="title";
    public static final String COL_FEED_CONTENT="content";
    public static final String COL_FEED_AUTHOR_PICTURE_URL ="author_picture";
    public static final String COL_FEED_PICTURE_URL="picture_url";
    public static final String COL_FEED_URL="feed_url";
    public static final String COL_FEED_TIME="feed_time";
    public static final String COL_FEED_CREATED_AT="createdAt";
    public static final String COL_FEED_ID="feed_id";
    public static final String COL_POSTED_BY="posted_by";







    private DatabaseManager databaseManager;

    public static int [] RECEIVED_MESSAGE_TYPES={0,2,4,6,8};
    public static int [] SENT_MESSAGE_TYPES={1,3,5,7,9};


    public enum MessageType {

        INCOMING, OUTGOING, INCOMING_IMAGE, OUTGOING_IMAGE, INCOMING_VOICE_NOTE,OUTGOING_VOICE_NOTE,INCOMING_REPLY,OUTGOING_REPLY,INCOMING_FILE,OUTGOING_FILE
    }



    @Override
    public boolean onCreate() {
        databaseManager = new DatabaseManager(getContext());

        return true;
    }
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder){
        SQLiteDatabase db = databaseManager.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        switch(uriMatcher.match(uri)) {
            case MESSAGES_ALL_ROWS:
                qb.setTables(TABLE_MESSAGES);
                break;

            case MESSAGES_SINGLE_ROW:
                qb.setTables(TABLE_MESSAGES);
                qb.appendWhere("_id = " + uri.getLastPathSegment());
                break;
            case HUB_MESSAGES_ALL_ROWS:
                qb.setTables(TABLE_HUB_MESSAGES);
                break;

            case HUB_MESSAGES_SINGLE_ROW:
                qb.setTables(TABLE_HUB_MESSAGES);
                qb.appendWhere("_id = " + uri.getLastPathSegment());
                break;

            case FRIENDS_ALL_ROWS:
                qb.setTables(TABLE_FRIENDS);
                break;

            case FRIENDS_SINGLE_ROW:
                qb.setTables(TABLE_FRIENDS);
                qb.appendWhere("_id = " + uri.getLastPathSegment());
                break;

            case HUBS_ALL_ROWS:
                qb.setTables(TABLE_HUBS);
                break;

            case HUBS_SINGLE_ROW:
                qb.setTables(TABLE_HUBS);
                qb.appendWhere("_id = " + uri.getLastPathSegment());
                break;


            case NOTIFICATION_ALL_ROWS:
                qb.setTables(TABLE_NOTIFICATIONS);
                break;

            case NOTIFICATION_SINGLE_ROW:
                qb.setTables(TABLE_NOTIFICATIONS);
                qb.appendWhere("_id = " + uri.getLastPathSegment());
                break;

            case FEEDS_ALL_ROWS:
                qb.setTables(TABLE_FEEDS);
                break;

            case FEEDS_SINGLE_ROW:
                qb.setTables(TABLE_FEEDS);
                qb.appendWhere("_id = " + uri.getLastPathSegment());
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        long id;
        switch(uriMatcher.match(uri)) {
            case MESSAGES_ALL_ROWS:
                id = db.insertOrThrow(TABLE_MESSAGES, null, values);
                if (values.get(COL_TO) != null && ! values.get(COL_FROM).equals(new Manager(getContext()).getUsername())) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(COL_LAST_MESSAGE,values.getAsString(COL_MSG));
                    contentValues.put(COL_LAST_MESSAGE_TIME_STAMP, values.getAsString(COL_TIME));
                    contentValues.put(COL_LAST_MESSAGE_STATUS,"");
                    db.update(TABLE_FRIENDS, contentValues, "username = ?", new String[]{values.getAsString(COL_FROM)});
                    db.execSQL("update friends set count=count+1 where username = ?", new Object[]{values.get(COL_FROM)});
                   // db.execSQL("update friends set last_message='"+values.getAsString(COL_MSG)+"' where username = ?", new Object[]{values.get(COL_FROM)});
                    //db.execSQL("update friends set last_message_time_stamp='"+values.getAsString(COL_TIME)+"' where username = ?", new Object[]{values.get(COL_FROM)});

                    getContext().getContentResolver().notifyChange(CONTENT_URI_FRIENDS, null);

                } else
                if(values.get(COL_FROM).equals(new Manager(getContext()).getUsername()) && values.get(COL_TO)!=null) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(COL_LAST_MESSAGE, values.getAsString(COL_MSG));
                    contentValues.put(COL_LAST_MESSAGE_TIME_STAMP, values.getAsString(COL_TIME));
                    contentValues.put(COL_LAST_MESSAGE_TYPE,values.getAsInteger(COL_TYPE));
                    contentValues.put(COL_LAST_MESSAGE_STATUS,values.getAsString(COL_DELIVERY_STATUS));
                    db.update(TABLE_FRIENDS, contentValues, "username = ?", new String[]{values.getAsString(COL_TO)});
                    getContext().getContentResolver().notifyChange(CONTENT_URI_FRIENDS, null);
                   // db.execSQL("update friends set last_message='" + values.getAsString(COL_MSG) + "' where username = ?", new Object[]{values.get(COL_TO)});
                    //db.execSQL("update friends set last_message_time_stamp='" + values.getAsString(COL_TIME) + "' where username = ?", new Object[]{values.get(COL_TO)});
                }

                break;
            case HUB_MESSAGES_ALL_ROWS:
                id = db.insertOrThrow(TABLE_HUB_MESSAGES, null, values);
                String sender=values.getAsString(COL_HUB_MESSAGE_SENDER);
                if(sender != null ) {
                    if (sender.equals(new Manager(getContext()).getUsername())) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(COL_LAST_MESSAGE, values.getAsString(COL_HUB_MESSAGE));
                        contentValues.put(COL_LAST_MESSAGE_TIME_STAMP, values.getAsString(COL_TIME));
                        contentValues.put(COL_LAST_MESSAGE_STATUS, values.getAsString(COL_DELIVERY_STATUS));
                        db.update(TABLE_HUBS, contentValues, "hub_id = ?", new String[]{values.getAsString(COL_HUB_ID)});
                        getContext().getContentResolver().notifyChange(CONTENT_URI_HUB_MESSAGES, null);
                    } else {

                        ContentValues contentValues = new ContentValues();
                        contentValues.put(COL_LAST_MESSAGE, values.getAsString(COL_HUB_MESSAGE));
                        contentValues.put(COL_LAST_MESSAGE_TIME_STAMP, values.getAsString(COL_TIME));
                        contentValues.put(COL_LAST_MESSAGE_STATUS, "");
                        db.update(TABLE_HUBS, contentValues, "hub_id = ?", new String[]{values.getAsString(COL_HUB_ID)});
                        db.execSQL("update hubs set count=count+1 where hub_id = ?", new Object[]{values.get(COL_HUB_ID)});
                        getContext().getContentResolver().notifyChange(CONTENT_URI_HUB_MESSAGES, null);

                    }
                }
                break;


            case FRIENDS_ALL_ROWS:
                id = db.insertOrThrow(TABLE_FRIENDS, null, values);
                break;
            case HUBS_ALL_ROWS:
                id = db.insertOrThrow(TABLE_HUBS, null, values);
                break;

            case NOTIFICATION_ALL_ROWS:
                id = db.insertOrThrow(TABLE_NOTIFICATIONS, null, values);
                break;
            case FEEDS_ALL_ROWS:
                id = db.insertOrThrow(TABLE_FEEDS, null, values);
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        Uri insertUri = ContentUris.withAppendedId(uri, id);
        getContext().getContentResolver().notifyChange(insertUri, null);
        return insertUri;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = databaseManager.getWritableDatabase();

        int count;
        switch(uriMatcher.match(uri)) {
            case MESSAGES_ALL_ROWS:
                count = db.update(TABLE_MESSAGES, values, selection, selectionArgs);
                break;

            case MESSAGES_SINGLE_ROW:
                count = db.update(TABLE_MESSAGES, values, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;

            case HUB_MESSAGES_ALL_ROWS:
                count = db.update(TABLE_HUB_MESSAGES, values, selection, selectionArgs);
                break;

            case HUB_MESSAGES_SINGLE_ROW:
                count = db.update(TABLE_HUB_MESSAGES, values, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;

            case FRIENDS_ALL_ROWS:
                count = db.update(TABLE_FRIENDS, values, selection, selectionArgs);
                break;

            case FRIENDS_SINGLE_ROW:
                count = db.update(TABLE_FRIENDS, values, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;

            case HUBS_ALL_ROWS:
                count = db.update(TABLE_HUBS, values, selection, selectionArgs);
                break;

            case HUBS_SINGLE_ROW:
                count = db.update(TABLE_HUBS, values, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;


            case NOTIFICATION_ALL_ROWS:
                count = db.update(TABLE_NOTIFICATIONS, values, selection, selectionArgs);
                break;

            case NOTIFICATION_SINGLE_ROW:
                count = db.update(TABLE_NOTIFICATIONS, values, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;

            case FEEDS_ALL_ROWS:
                count = db.update(TABLE_FEEDS, values, selection, selectionArgs);
                break;

            case FEEDS_SINGLE_ROW:
                count = db.update(TABLE_FEEDS, values, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = databaseManager.getWritableDatabase();

        int count;
        switch(uriMatcher.match(uri)) {
            case MESSAGES_ALL_ROWS:
                count = db.delete(TABLE_MESSAGES, selection, selectionArgs);
                break;

            case MESSAGES_SINGLE_ROW:
                count = db.delete(TABLE_MESSAGES, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;

            case HUB_MESSAGES_ALL_ROWS:
                count = db.delete(TABLE_HUB_MESSAGES, selection, selectionArgs);
                break;

            case HUB_MESSAGES_SINGLE_ROW:
                count = db.delete(TABLE_HUB_MESSAGES, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;

            case FRIENDS_ALL_ROWS:
                count = db.delete(TABLE_FRIENDS, selection, selectionArgs);
                break;

            case FRIENDS_SINGLE_ROW:
                count = db.delete(TABLE_FRIENDS, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;

            case HUBS_ALL_ROWS:
                count = db.delete(TABLE_HUBS, selection, selectionArgs);
                break;

            case HUBS_SINGLE_ROW:
                count = db.delete(TABLE_HUBS, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;
            case NOTIFICATION_ALL_ROWS:
                count = db.delete(TABLE_NOTIFICATIONS, selection, selectionArgs);
                break;

            case NOTIFICATION_SINGLE_ROW:
                count = db.delete(TABLE_NOTIFICATIONS, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;

            case FEEDS_ALL_ROWS:
                count = db.delete(TABLE_NOTIFICATIONS, selection, selectionArgs);
                break;

            case FEEDS_SINGLE_ROW:
                count = db.delete(TABLE_NOTIFICATIONS, "_id = ?", new String[]{uri.getLastPathSegment()});
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    private String getTableName(Uri uri) {
        switch(uriMatcher.match(uri)) {
            case MESSAGES_ALL_ROWS:
            case MESSAGES_SINGLE_ROW:
                return TABLE_MESSAGES;

            case FRIENDS_ALL_ROWS:
            case FRIENDS_SINGLE_ROW:
                return TABLE_FRIENDS;
        }
        return null;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    public static final Uri CONTENT_URI_MESSAGES = Uri.parse("content://com.jabuwings.provider/messages");
    public static final Uri CONTENT_URI_FRIENDS = Uri.parse("content://com.jabuwings.provider/friends");
    public static final Uri CONTENT_URI_HUBS = Uri.parse("content://com.jabuwings.provider/hubs");
    public static final Uri CONTENT_URI_NOTIFICATIONS = Uri.parse("content://com.jabuwings.provider/notifications");
    public static final Uri CONTENT_URI_HUB_MESSAGES =Uri.parse("content://com.jabuwings.provider/hub_messages");
    public static final Uri CONTENT_URI_FEEDS =Uri.parse("content://com.jabuwings.provider/feeds");

    private static final int MESSAGES_ALL_ROWS = 1;
    private static final int MESSAGES_SINGLE_ROW = 2;
    private static final int FRIENDS_ALL_ROWS = 3;
    private static final int FRIENDS_SINGLE_ROW = 4;
    private static final int NOTIFICATION_ALL_ROWS = 5;
    private static final int NOTIFICATION_SINGLE_ROW = 6;
    private static final int HUB_MESSAGES_ALL_ROWS =7;
    private static final int HUB_MESSAGES_SINGLE_ROW =8;
    private static final int HUBS_ALL_ROWS = 9;
    private static final int HUBS_SINGLE_ROW = 10;
    private static final int FEEDS_ALL_ROWS = 11;
    private static final int FEEDS_SINGLE_ROW = 12;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI("com.jabuwings.provider", "messages", MESSAGES_ALL_ROWS);
        uriMatcher.addURI("com.jabuwings.provider", "messages/#", MESSAGES_SINGLE_ROW);
        uriMatcher.addURI("com.jabuwings.provider","hub_messages", HUB_MESSAGES_ALL_ROWS);
        uriMatcher.addURI("com.jabuwings.provider","hub_messages/#", HUB_MESSAGES_SINGLE_ROW);
        uriMatcher.addURI("com.jabuwings.provider", "friends", FRIENDS_ALL_ROWS);
        uriMatcher.addURI("com.jabuwings.provider", "friends/#", FRIENDS_SINGLE_ROW);
       // uriMatcher.addURI("com.jabuwings.provider", "notifications", NOTIFICATION_ALL_ROWS);
        //uriMatcher.addURI("com.jabuwings.provider", "notifications/#", NOTIFICATION_SINGLE_ROW);
        uriMatcher.addURI("com.jabuwings.provider", "hubs", HUBS_ALL_ROWS);
        uriMatcher.addURI("com.jabuwings.provider", "hubs/#", HUBS_SINGLE_ROW);
        //uriMatcher.addURI("com.jabuwings.provider", "feeds/", FEEDS_ALL_ROWS);
        //uriMatcher.addURI("com.jabuwings.provider", "feeds/#", FEEDS_SINGLE_ROW);
    }

}

