package com.jabuwings.database.realm;

import android.os.AsyncTask;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by jamesfalade on 10/08/2017.
 */

public class RealmHelper {
    private Realm realm;
    public RealmHelper()
    {
        realm= Realm.getDefaultInstance();
    }
    
    public void saveInBackground(RealmObject... objects)
    {
        new AsyncTask<RealmObject,Void,Void>(){
            @Override
            protected Void doInBackground(final RealmObject... params) {
                realm= Realm.getDefaultInstance();
                realm.beginTransaction();
                for(RealmObject o: params)
                {
                    realm.copyToRealmOrUpdate(o);
                }
                realm.commitTransaction();
                realm.close();
                return null;
            }
        }.execute(objects);
    }
    
    
    public <E extends RealmObject> RealmQuery<E> setQueryFinishedListener(final RealmQuery<E> query, final QueryFinished queryFinishedListener)
    {
        new AsyncTask<Void,Void,RealmResults<E>>(){
            @Override
            protected RealmResults<E> doInBackground(Void... params) {
                return query.findAll();
            }

            @Override
            protected void onPostExecute(RealmResults<E> results) {
                super.onPostExecute(results);
                queryFinishedListener.onFinish(results);
            }
        }.execute();

        return null;
    }
    
    
    
    
}
