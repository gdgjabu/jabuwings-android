package com.jabuwings.database.realm;

import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by jamesfalade on 12/08/2017.
 */

public interface QueryFinished <E extends RealmObject>{
    void onFinish(RealmResults<Message> results);
}
