package com.jabuwings.database.realm;


import io.realm.FriendRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by jamesfalade on 14/08/2017.
 */
public class Friend extends RealmObject {
    @PrimaryKey
    private String username;

    private String name,matricNo,picture,department,state,level,verifiedAs,birthday,status;

    private Message lastMessage;

    private long lastTypingDate;

    private long pictureDate;

    private int type, friendShipStatus;

    private long lastMessageDate;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMatricNo() {
        return matricNo;
    }

    public void setMatricNo(String matricNo) {
        this.matricNo = matricNo;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getVerifiedAs() {
        return verifiedAs;
    }

    public void setVerifiedAs(String verifiedAs) {
        this.verifiedAs = verifiedAs;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Message getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(Message lastMessage) {
        this.lastMessage = lastMessage;
    }


    public long getPictureDate() {
        return pictureDate;
    }

    public void setPictureDate(long pictureDate) {
        this.pictureDate = pictureDate;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getFriendShipStatus() {
        return friendShipStatus;
    }

    public void setFriendShipStatus(int friendShipStatus) {
        this.friendShipStatus = friendShipStatus;
    }

    public long getLastMessageDate() {
        return lastMessageDate;
    }

    public void setLastMessageDate(long lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }

    public long getLastTypingDate() {
        return lastTypingDate;
    }

    public void setLastTypingDate(long lastTypingDate) {
        this.lastTypingDate = lastTypingDate;
    }

    public boolean isTyping()
    {
        return System.currentTimeMillis() - lastTypingDate <=5000;
    }
}
