package com.jabuwings.database.realm;

import com.jabuwings.models.JabuWingsHub;
import com.parse.ParseObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by jamesfalade on 14/08/2017.
 */

public class Hub extends RealmObject {

    public Hub()
    {

    }
    public void initHub(JabuWingsHub hub)
    {
        if(hub.getProfilePictureUrl()!=null)
            setPicture(hub.getProfilePictureUrl());
        setDesc(hub.getDescription());
        if(hub.isVerifiedAs()!=null)
        setVerifiedAs(hub.isVerifiedAs());
        setChiefAdmin(hub.getCreatedBy());
        setInstanceTime(hub.getHub().getUpdatedAt().getTime());
        setTitle(hub.getTitle());

    }
    @PrimaryKey
    private String hubId;

    private String picture, desc, verifiedAs, chiefAdmin, members, admins, title;

    private long instanceTime, pictureDate;

    private HubMessage lastMessage;

    private long lastMessageDate;



    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getVerifiedAs() {
        return verifiedAs;
    }

    public void setVerifiedAs(String verifiedAs) {
        this.verifiedAs = verifiedAs;
    }

    public String getChiefAdmin() {
        return chiefAdmin;
    }

    public void setChiefAdmin(String chiefAdmin) {
        this.chiefAdmin = chiefAdmin;
    }

    public String getMembers() {
        return members;
    }

    public void setMembers(String members) {
        this.members = members;
    }

    public String getAdmins() {
        return admins;
    }

    public void setAdmins(String admins) {
        this.admins = admins;
    }

    public long getInstanceTime() {
        return instanceTime;
    }

    public void setInstanceTime(long instanceTime) {
        this.instanceTime = instanceTime;
    }

    public long getPictureDate() {
        return pictureDate;
    }

    public void setPictureDate(long pictureDate) {
        this.pictureDate = pictureDate;
    }

    public HubMessage getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(HubMessage lastMessage) {
        this.lastMessage = lastMessage;
    }

    public long getLastMessageDate() {
        return lastMessageDate;
    }

    public void setLastMessageDate(long lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
