package com.jabuwings.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.jabuwings.database.realm.Friend;
import com.jabuwings.database.realm.Hub;
import com.jabuwings.database.realm.Message;
import com.jabuwings.feed.FeedItem;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.models.MessageItem;
import com.jabuwings.utilities.Utils;
import com.parse.ParseException;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;

/**
 * Created by Falade James on 23/07/2015.
 */
public class DatabaseManager extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "jabuwings.db";
    private static final int DATABASE_VERSION = 1;
    private Context context;
    public DatabaseManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table messages ("
                + "_id integer primary key autoincrement, "
                + DataProvider.COL_TYPE        	  +" integer, "
                + DataProvider.COL_MSG         +" text, "
                + DataProvider.COL_FROM 	  +" text, "
                + DataProvider.COL_TO  +" text, "
                + DataProvider.COL_AT  +" datetime default current_timestamp,"
                + DataProvider.COL_DELIVERY_STATUS 	  +" text, "
                + DataProvider.COL_MESSAGE_ID +" text unique, "
                + DataProvider.COL_FILE_LOCAL_URL +" text, "
                + DataProvider.COL_FILE_SIZE +" long, "
                + DataProvider.COL_FILE_NAME +" text, "
                + DataProvider.COL_FILE_ADDRESS +" text, "
                + DataProvider.COL_FILE_PROGRESS +" integer, "
                + DataProvider.COL_TIME +" text);");

        db.execSQL("create table hub_messages ("
                + "_id integer primary key autoincrement, "
                + DataProvider.COL_TYPE        	  +" integer, "
                + DataProvider.COL_HUB_MESSAGE +" text, "
                + DataProvider.COL_HUB_REPLY +" text, "
                + DataProvider.COL_DELIVERY_STATUS 	  +" text, "
                + DataProvider.COL_MESSAGE_ID +" text unique, "
                + DataProvider.COL_HUB_MESSAGE_SENDER +" text, "
                + DataProvider.COL_AT  +" datetime default current_timestamp,"
                + DataProvider.COL_HUB_ID +" text, "
                + DataProvider.COL_FILE_LOCAL_URL +" text, "
                + DataProvider.COL_FILE_SIZE +" long, "
                + DataProvider.COL_FILE_NAME +" text, "
                + DataProvider.COL_FILE_ADDRESS +" text, "
                + DataProvider.COL_FILE_PROGRESS +" integer, "
                + DataProvider.COL_TIME +" text);");


        db.execSQL("create table friends("
                + "_id integer primary key autoincrement, "
                + DataProvider.COL_NAME 	  +" text, "
                + DataProvider.COL_USERNAME   +" text unique, "
                + DataProvider.COL_MATRIC_NO   +" text unique, "
                + DataProvider.COL_FRIEND_OF   +" text, "
                + DataProvider.COL_TYPING   +" boolean, "
                + DataProvider.COL_PROFILE_PICTURE_URL   +" text, "
                + DataProvider.COL_PROFILE_PICTURE_DATE   +" long, "
                + DataProvider.COL_DEPARTMENT   +" text, "
                + DataProvider.COL_STATE   +" text, "
                + DataProvider.COL_LEVEL   +" text, "
                + DataProvider.COL_TYPE        	  +" integer, "
                + DataProvider.COL_VERIFIED_AS        	  +" text, "
                + DataProvider.COL_BIRTHDAY   +" text, "
                + DataProvider.COL_STATUS   +" text, "
                + DataProvider.COL_FRIENDSHIP_STATUS   +" integer default 0, "
                + DataProvider.COL_LAST_MESSAGE   +" text, "
                + DataProvider.COL_LAST_MESSAGE_STATUS   +" text, "
                + DataProvider.COL_LAST_MESSAGE_TIME_STAMP +" text, "
                + DataProvider.COL_LAST_MESSAGE_TYPE +" integer default 0, "
                + DataProvider.COL_COUNT   +" integer default 0);");

        db.execSQL("create table hubs ("
                + "_id integer primary key autoincrement, "
                + DataProvider.COL_HUB_TITLE +" text, "
                + DataProvider.COL_HUB_ID         +" text unique, "
                + DataProvider.COL_HUB_PROFILE_PICTURE_URL 	  +" text, "
                + DataProvider.COL_HUB_PROFILE_PICTURE_FILE_URL 	  +" text, "
                + DataProvider.COL_PROFILE_PICTURE_DATE   +" long, "
                + DataProvider.COL_HUB_DESC 	  +" text, "
                + DataProvider.COL_VERIFIED_AS        	  +" text, "
                + DataProvider.COL_HUB_CHIEF_ADMIN 	  +" text, "
                + DataProvider.COL_HUB_LAST_MESSAGE  +" text, "
                + DataProvider.COL_HUB_INSTANCE_TIME +" long, "
                + DataProvider.COL_HUB_LAST_MESSAGE_TIME_STAMP  +" text, "
                + DataProvider.COL_LAST_MESSAGE_STATUS   +" text, "
                + DataProvider.COL_HUB_LAST_MESSAGE_TYPE +" integer default 0, "
                + DataProvider.COL_COUNT  +" integer default 0, "
                + DataProvider.COL_HUB_MEMBERS  +" text, "
                + DataProvider.COL_HUB_ADMINS  +" text);");

   /*     db.execSQL("create table notifications("
                + "_id integer primary key autoincrement, "
                + DataProvider.COL_NOT_MESSAGE + " text, "
                + DataProvider.COL_NOT_TITLE + " text, "
                + DataProvider.COL_NOT_SENDER + " text, "
                + DataProvider.COL_NOT_TIME + " long);");

        db.execSQL("create table feeds("
                + "_id integer primary key autoincrement, "
                + DataProvider.COL_POSTED_BY + " integer, "
                + DataProvider.COL_FEED_AUTHOR + " text, "
                + DataProvider.COL_FEED_TITLE + " text, "
                + DataProvider.COL_FEED_CREATED_AT + " long, "
                + DataProvider.COL_FEED_CONTENT + " text, "
                + DataProvider.COL_FEED_AUTHOR_PICTURE_URL + " text, "
                + DataProvider.COL_FEED_URL + " text, "
                + DataProvider.COL_FEED_TIME + " text, "
                + DataProvider.COL_FEED_ID + " text unique, "
                + DataProvider.COL_FEED_PICTURE_URL + " text);");*/


    }

    public List<String> getFriends()
    {
        Realm realm = Realm.getDefaultInstance();
        List<Friend> friends=realm.where(Friend.class).findAll();
        List<String> usernames= new ArrayList<>();
        for (Friend friend: friends)
        {
            usernames.add(friend.getUsername());
        }
        realm.close();
        return usernames;
    }

    public int getFriendsNewMessagesCount()
    {
        //TODO
        return 0;
    }

    public int getHubsNewMessagesCount() {
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM " + DataProvider.TABLE_HUBS, null);
        int count = 0;
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                count += cursor.getInt(cursor.getColumnIndex(DataProvider.COL_COUNT));
                cursor.moveToNext();
            }
        }
        cursor.close();
        return count;
    }
    public List<String> getHubs()
    {
        Realm realm = Realm.getDefaultInstance();
        List<Hub> hubs=realm.where(Hub.class).findAll();
        List<String> hubIds= new ArrayList<>();
        for (Hub hub: hubs)
        {
            hubIds.add(hub.getHubId());
        }
        realm.close();

        return hubIds;
    }
    public int getFriendsCount() {

        Realm realm = Realm.getDefaultInstance();
        int count = realm.where(Friend.class).findAll().size();
        realm.close();
        return count;
    }

    public int getHubsCount() {
        Realm realm = Realm.getDefaultInstance();
        int count = realm.where(Hub.class).findAll().size();
        realm.close();
        return count;
    }

    /**
     * Must be called on a background thread
     * @return the numbers of local feeds
     */
    public int getFeedCount() {
        try{
        return FeedItem.getQuery().fromLocalDatastore().count();
        }
        catch (ParseException e)
        {

        }
        return 0;
    }


    public void updateFriendStatus(String username, String newStatus)
    {
        Realm realm = Realm.getDefaultInstance();
        Friend friend = realm.where(Friend.class).equalTo(Const.USERNAME, username).findFirst();
        realm.beginTransaction();
        friend.setStatus(newStatus);
        realm.commitTransaction();
        realm.close();

    }
    public void updateFriendshipStatus(String username, int newStatus)
    {
        //TODO remove condition when lisa account is active
        if(!username.equals("lisa"))
        {
            Realm realm = Realm.getDefaultInstance();
            Friend friend = realm.where(Friend.class).equalTo(Const.USERNAME, username).findFirst();
            realm.beginTransaction();
            friend.setFriendShipStatus(newStatus);
            realm.commitTransaction();
            realm.close();
        }


    }

    public void setFriendVerified(String username, String verifiedAs)
    {
        Realm realm = Realm.getDefaultInstance();
        Friend friend = realm.where(Friend.class).equalTo(Const.USERNAME, username).findFirst();
        realm.beginTransaction();
        if(verifiedAs==null) {
            friend.setVerifiedAs("1");
        }
        else {
            friend.setVerifiedAs(verifiedAs);
        }
        realm.commitTransaction();

    }

    @Deprecated
    public void updateFriendFileProgress(String msgId, int newProgress)
    {
        SQLiteDatabase db= this.getWritableDatabase();
        db.execSQL("update messages set file_progress ='"+newProgress+"' where msgId=?",new Object[]{msgId});
        context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_MESSAGES,null);

    }

    public void updateFriendProfilePictureUrl(String username, String newURL)
    {
        Realm realm = Realm.getDefaultInstance();
        Friend friend = realm.where(Friend.class).equalTo(Const.USERNAME, username).findFirst();
        realm.beginTransaction();
        friend.setPicture(newURL);
        realm.commitTransaction();
        realm.close();
    }

    public void updateFriendProfilePictureDate(String username, long newDate)
    {
        Realm realm = Realm.getDefaultInstance();
        Friend friend = realm.where(Friend.class).equalTo(Const.USERNAME, username).findFirst();
        realm.beginTransaction();
        friend.setPictureDate(newDate);
        realm.commitTransaction();
        realm.close();
    }

    public void updateHubProfilePictureDate(String hub_id, long newDate)
    {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Hub hub = realm.where(Hub.class).equalTo(Const.HUB_ID, hub_id).findFirst();
        hub.setPictureDate(newDate);
        realm.commitTransaction();
        realm.close();

    }

    @Deprecated
    public void updateHubAdmins(String hub_id, String newAdmins)
    {
        SQLiteDatabase db= this.getWritableDatabase();
        db.execSQL("update hubs set hub_admins ='"+newAdmins+"' where hub_id=?",new Object[]{hub_id});
        context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_HUBS,null);
    }

    @Deprecated
    public void updateHubProfilePictureUrl(String title, String newURL)
    {
        SQLiteDatabase db= this.getWritableDatabase();
        db.execSQL("update friends set profile_picture_url ='"+newURL+"' where hub_title=?",new Object[]{title});
        context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_HUBS,null);

    }

    @Deprecated
    public String getFriendProfilePictureURL(String username)
    {

        String where = DataProvider.COL_USERNAME + "=?";
        String[] whereArgs = new String[]{username};
        Cursor cursor = this.getWritableDatabase().query(true, DataProvider.TABLE_FRIENDS, null, where, whereArgs, null, null, null, null);
        if(cursor.moveToFirst())
        {
            String profilePictureURL=cursor.getString(cursor.getColumnIndex(DataProvider.COL_PROFILE_PICTURE_URL));
            cursor.close();
            return profilePictureURL;
        }

        else{
            return null;
        }
    }
    public long getFriendProfilePictureDate(String username)
    {
        Realm realm = Realm.getDefaultInstance();
        Friend friend = realm.where(Friend.class).equalTo(Const.USERNAME, username).findFirst();
        long date = friend.getPictureDate();
        realm.close();
        return date;
    }

    public long getHubProfilePictureDate(String hub_id)
    {
        Realm realm = Realm.getDefaultInstance();
        Hub hub = realm.where(Hub.class).equalTo(Const.HUB_ID, hub_id).findFirst();
        long date = hub.getPictureDate();
        realm.close();
        return date;
    }


    public String getFriendWithMatricNo(String matricNo)
    {
        Realm realm = Realm.getDefaultInstance();
        Friend friend = realm.where(Friend.class).equalTo(Const.MATRIC_NO, matricNo).findFirst();
        String username = null;
        if(friend!=null)
        {
            username = friend.getUsername();
        }
        realm.close();
        return  username;
    }

    public void deleteHubMessages(String hubId)
    {
        getWritableDatabase().execSQL("delete from messages where hub_id='"+hubId+"'");
    }



    public void setMessageStatus(final String msgId,final String status)
    {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Message message = realm.where(Message.class).equalTo(Const.MSG_ID, msgId).findFirst();
                if(message!=null)
                message.setDeliveryStatus(status);
                realm.close();
            }
        });
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
        //db.execSQL("ALTER TABLE FRIENDS ADD COLUMN new_column INTEGER DEFAULT ");

    }
    public void deleteDatabase()
    {
         //delete realm
         Realm realm = Realm.getDefaultInstance();
         realm.beginTransaction();
         realm.deleteAll();
         realm.commitTransaction();
         realm.close();
    }
}

