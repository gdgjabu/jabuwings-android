package com.jabuwings.models;

import com.jabuwings.management.Const;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by Falade James on 10/21/2016 All Rights Reserved.
 */
@ParseClassName("HubTopics")
public class HubTopic extends ParseObject {

    public String getMessage()
    {
        return getString(Const.MSG);
    }

    public String getSender()
    {
        return getString(Const.SENDER);
    }

    public String getStatus()
    {
        return getString(Const.STATUS);
    }

    public ParseQuery getReplies()
    {
        return ParseQuery.getQuery("HubReplies").whereEqualTo(Const.TOPIC,this);
    }
    public static ParseQuery<HubTopic> getQuery()
    {
        return ParseQuery.getQuery(HubTopic.class);
    }

}
