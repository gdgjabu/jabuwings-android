package com.jabuwings.models;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.jabuwings.R;
import com.jabuwings.widgets.GestureImageView;
import com.jabuwings.widgets.ZoomableImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class ImageSliderAdapter extends PagerAdapter {

	private Activity _activity;
	private LayoutInflater inflater;
    private List<String> _imagePaths;

	// constructor
	public ImageSliderAdapter(Activity activity, List<String> _imagePaths) {
		this._activity = activity;
        this._imagePaths=_imagePaths;

	}

	@Override
	public int getCount() {
		return this._imagePaths.size();
	}

	@Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }
	
	@Override
    public Object instantiateItem(ViewGroup container, int position) {
        ZoomableImageView imgDisplay;
 
        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.background_image, container,
                false);
        viewLayout.findViewById(R.id.btChoose).setVisibility(View.GONE);
 
        imgDisplay = (ZoomableImageView) viewLayout.findViewById(R.id.img);


        /*BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(_imagePaths.get(position), options);*/
        //imgDisplay.setImageBitmap(bitmap);
        String path = _imagePaths.get(position);
        if(path.contains("http")) Picasso.with(_activity).load(path).into(imgDisplay);
        else
        Picasso.with(_activity).load(Uri.fromFile(new File(_imagePaths.get(position)))).into(imgDisplay);


        ((ViewPager) container).addView(viewLayout);
 
        return viewLayout;
	}
	
	@Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
 
    }

}
