package com.jabuwings.models;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.feed.Comment;
import com.jabuwings.feed.FeedComment;
import com.jabuwings.feed.FeedItem;
import com.jabuwings.feed.FeedPost;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.views.hooks.Comments;
import com.jabuwings.views.main.JabuWingsActivity;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class CommentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private FeedPost feedPost;
    private String id;
    private int itemsCount = 0;
    private int lastAnimatedPosition = -1;
    private int avatarSize;

    private boolean animationsLocked = false;
    private boolean delayEnterAnimation = true;
    Manager manager;
    private List<Comment> comments=new ArrayList<>();

    public CommentsAdapter(Context context,FeedPost feedPost, String id) {
        this.context = context;
        this.id=id;
        this.feedPost = feedPost;
        avatarSize = context.getResources().getDimensionPixelSize(R.dimen.comment_avatar_size);
        manager= new Manager(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(context).inflate(R.layout.item_comment, parent, false);
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        runEnterAnimation(viewHolder.itemView, position);
        CommentViewHolder holder = (CommentViewHolder) viewHolder;
        holder.bindView(comments.get(position));

    }


    public void updateItems()
    {
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("Feed").document(id).collection("comments").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                if (task.isSuccessful()) {
                    for (final DocumentSnapshot document : task.getResult()) {
                        final FeedComment feedComment = document.toObject(FeedComment.class);
                        db.collection("users").document(feedComment.getAuthor()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                DocumentSnapshot documentSnapshot = task.getResult();
                                if(documentSnapshot != null && documentSnapshot.exists())
                                {
                                    JabuWingsFirebaseUser jabuWingsFirebaseUser = documentSnapshot.toObject(JabuWingsFirebaseUser.class);
                                    comments.add(new Comment(jabuWingsFirebaseUser.getName(),feedComment.getContent(), jabuWingsFirebaseUser.getPicture()));
                                    Comments.stopLoading();
                                    notifyDataSetChanged();

                                }
                            }
                        });
                    }
                } else {
                   manager.log("Error getting comments "+task.getException().getMessage());
                }
            }
        });

    }
    private void runEnterAnimation(View view, int position) {
        if (animationsLocked) return;

        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position;
            view.setTranslationY(100);
            view.setAlpha(0.f);
            view.animate()
                    .translationY(0).alpha(1.f)
                    .setStartDelay(delayEnterAnimation ? 20 * (position) : 0)
                    .setInterpolator(new DecelerateInterpolator(2.f))
                    .setDuration(300)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            animationsLocked = true;
                        }
                    })
                    .start();
        }
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }



    public void addItem(String comment) {
        comments.add(new Comment("You",comment,manager.getProfilePictureUrl()));
        notifyDataSetChanged();
        //notifyItemInserted(itemsCount - 1);
    }

    public void setAnimationsLocked(boolean animationsLocked) {
        this.animationsLocked = animationsLocked;
    }

    public void setDelayEnterAnimation(boolean delayEnterAnimation) {
        this.delayEnterAnimation = delayEnterAnimation;
    }

    public  class CommentViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.ivUserAvatar)
        ImageView ivUserAvatar;
        @InjectView(R.id.tvComment)
        TextView tvComment;
        @InjectView(R.id.tvCommentUsername)
        TextView tvUsername;

        public CommentViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
            JabuWingsActivity.setUpTextViews(context,tvComment,tvUsername);
        }
        public void bindView(Comment comment)
        {
            tvComment.setText(comment.getComment());
            tvUsername.setText(comment.getUsername());
            Picasso.with(context).load(comment.getPictureUrl()).transform(new CircleTransform()).into(ivUserAvatar);
        }
    }
}
