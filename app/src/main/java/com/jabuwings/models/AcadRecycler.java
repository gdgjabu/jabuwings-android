package com.jabuwings.models;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.views.hooks.Browse;
import com.jabuwings.views.classroom.CourseMaterials;
import com.jabuwings.views.main.AcademicManagerActivity;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.timetable.TimeTable2;

import java.util.List;

/**
 * Created by Falade James on 10/15/2015 All Rights Reserved.
 */
public class AcadRecycler extends RecyclerView.Adapter<AcadRecycler.ViewHolder> {
    List<String> items;
    Context context;
    public AcadRecycler(List<String> items, Context context) {
        this.items = items;
        this.context=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.picker_item, parent, false);


        return new ViewHolder(v, new AcadClicks() {
            @Override
            public void onClick(View v) {

                switch ((int)v.getTag())
                {
                    case 0:
                        context.startActivity(new Intent(context, TimeTable2.class));
                        break;
                    case 1:
                        context.startActivity(new Intent(context,AcademicManagerActivity.class));
                        break;
                    case 2:
                        Intent courseReg = new Intent(context, Browse.class);
                        courseReg.putExtra(Const.ACAD_CHOICE, "courseReg");
                        context.startActivity(courseReg);
                        break;
                    case 3:
                        Intent resultChecker = new Intent(context, Browse.class);
                        resultChecker.putExtra(Const.ACAD_CHOICE, "resultChecker");
                        context.startActivity(resultChecker);
                        break;
                    case 4:
                        context.startActivity(new Intent(context, CourseMaterials.class));
                        break;
                    case 5:
                        Intent studentReg = new Intent(context, Browse.class);
                        studentReg.putExtra(Const.ACAD_CHOICE, "studentReg");
                        context.startActivity(studentReg);
                        break;
                    case 6:
                        Intent officialWebsite = new Intent(context, Browse.class);
                        officialWebsite.putExtra(Const.ACAD_CHOICE, "officialWebsite");
                        context.startActivity(officialWebsite);
                        break;
                }
            }
        });
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemName.setText(items.get(position));
        holder.itemName.setTag(position);


    }

    @Override
    public int getItemCount() {
        return items.size();

    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemName;



        ViewHolder(View itemView, final AcadClicks acadClicks) {
            super(itemView);
            itemName = (TextView) itemView.findViewById(R.id.tv_picker);
            itemName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    acadClicks.onClick(v);
                }
            });
            JabuWingsActivity.setUpTextViews(context,itemName);

        }

    }

    public interface AcadClicks{
        void onClick(View v);
    }
}
