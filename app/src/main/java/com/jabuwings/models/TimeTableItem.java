package com.jabuwings.models;

import android.support.annotation.NonNull;

import com.jabuwings.utilities.Time;

/**
 * Created by Falade James on 10/17/2015 All Rights Reserved.
 */
public class TimeTableItem implements Comparable<TimeTableItem> {
    String courseCode;
    String courseVenue;
    String timeStart;
    String timeStop;
    String level;
    String day;
    String college;
    Time time;
    public TimeTableItem(String courseCode, String venue, String timeStart, String timeStop, String  level, String day, String college)
    {
        this.courseCode =courseCode;
        this.courseVenue=venue;
        this.timeStart=timeStart;
        this.timeStop=timeStop;
        this.level=level;
        this.day=day;
        this.college=college;
        time=new Time();
    }

    @Override
    public int compareTo(@NonNull TimeTableItem another) {
        int thisHour=time.getHour(timeStart);
        int thatHour=time.getHour(another.getTimeStart());

        if(thisHour==thatHour) return 1;

        if(thisHour>thatHour)
        {
            return 1;
        }

        else{
            return -1;
        }

    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof TimeTableItem) {
            TimeTableItem t = (TimeTableItem) o;
            return t.getCourseCode().equals(getCourseCode()) && t.getCourseVenue().equals(getCourseVenue()) && t.getTimeStart().equals(getTimeStart()) && t.getTimeStop().equals(getTimeStop()) && t.getCollege().equals(getCollege()) && t.getLevel().equals(getLevel());
        }
        return false;

    }

    @Override
    public String toString() {
        return "courseCode: "+ courseCode +", day is "+day+", venue is "+courseVenue+", timeStart is: "+timeStart
                 +" timeStop is: "+timeStop+ ", college is "+college+", level is "+level;
    }

    public String getCourseCode()
    {return courseCode;}
    public String  getCourseVenue()
    {return courseVenue;}
    public String  getTimeStart()
    {return timeStart;}
    public int getStartingHour()
    {return time.getHour(getTimeStart());}

    public String getReadableStartHour()
    {return String.valueOf(time.getHour(getTimeStart()))+":00";}
    public int getEndingHour()
    {return time.getHour(getTimeStop());}
    public String getReadableEndHour()
    {return String.valueOf(time.getHour(getTimeStop()))+":00";}
    public String  getTimeStop()
    {return timeStop;}
    public String  getLevel()
    {return level;}
    public String  getDay()
    {return day;}
    public String getCollege()
    {return college;}
}
