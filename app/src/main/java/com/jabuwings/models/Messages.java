package com.jabuwings.models;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.realm.Message;
import com.jabuwings.image.ImageProcessor;
import com.jabuwings.image.PictureUploader;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.FileManager;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.AudioManager;
import com.jabuwings.views.hooks.ImageSlider;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.jabuwings.widgets.progressbutton.CircularProgressButton;
import com.squareup.picasso.Picasso;

import java.io.File;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

import static com.jabuwings.management.Const.MESSAGE_FAILED;
import static com.jabuwings.management.Const.MESSAGE_READ;

/**
 * Created by Falade James on 8/3/2015.
 */
@Deprecated
public class Messages extends ListFragment{
    private OnFragmentInteractionListener mListener;
    private ChatCursorAdapter adapter;
    private String TAG= Messages.class.getSimpleName();
    Time time;
    Manager manager;
    ImageProcessor imageProcessor;
    DialogWizard dialogWizard;
    Realm realm;




    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();







    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getActivity().getMenuInflater().inflate(R.menu.context_menu_chat, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info= (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        View view= info.targetView;
        TextView tvMsgId;
        tvMsgId = (TextView)view.findViewById(R.id.tv_message_id);
        TextView msg= (TextView)view.findViewById(R.id.msg);
        String id= tvMsgId.getText().toString();


        switch (item.getItemId())
        {
            case R.id.contextual_action_copy_message:
                if(msg!=null){
                manager.copyToClipboard(msg.getText().toString());
                manager.shortToast(R.string.message_copied);}
                else manager.errorToast("Cannot copy empty message");
                break;
            case R.id.contextual_action_forward_message:
                break;
            case R.id.contextual_action_delete_message:
                HouseKeeping.deleteMessage(getActivity(), id);
                manager.successToast(getString(R.string.message_deleted));
                break;
            case R.id.contextual_action_resend_message:
                if(msg!=null && !TextUtils.isEmpty(msg.getText()))
                HotSpot.sendPersonalMessage(getContext(),manager.getUsername(),mListener.getProfileUsername(),msg.getText().toString(),true);
                else
                manager.errorToast("Only applies to text messages");
                break;


        }



        return super.onContextItemSelected(item);

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        manager= new Manager(getActivity());
        getListView().setDivider(getResources().getDrawable(R.drawable.chat_divider));
        //getListView().setOverscrollHeader(getResources().getDrawable(R.drawable.btn_add));
        /*
        LinearLayout linearLayout=(LinearLayout) getLayoutInflater(savedInstanceState).inflate(R.layout.load_more,null);
        Button load=(Button)linearLayout.findViewById(R.id.msg_load_more);

        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manager.shortToast("I was clicked");
            }
        });
*/
        getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
        //getListView().addHeaderView(linearLayout);
        getListView().setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        getListView().setStackFromBottom(true);
       // registerForContextMenu(getListView());
        FrameLayout.LayoutParams params= new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getListView().setLayoutParams(params);
        Bundle args = new Bundle();
        args.putString(DataProvider.COL_USERNAME, mListener.getProfileUsername());

        Realm realm =Realm.getDefaultInstance();
        RealmResults<Message> realmResults = realm.where(Message.class)
                 .beginGroup()
                     .equalTo(Const.RECEIVER,mListener.getProfileUsername())
                     .equalTo(Const.SENDER,manager.getUsername())
                 .endGroup()
                .or()
                 .beginGroup()
                     .equalTo(Const.RECEIVER,manager.getUsername())
                     .equalTo(Const.SENDER,mListener.getProfileUsername())
                 .endGroup()
                .findAllAsync();
        //realmResults.sort("msgTime");
        /*List<Message> messages=new ArrayList<>();
        for(Message message: realmResults)
        {
            messages.add(message);
        }*/

        dialogWizard=new DialogWizard(getActivity());
        imageProcessor=new ImageProcessor(getActivity(),false);
        RealmChangeListener realmListener = new RealmChangeListener() {
            @Override
            public void onChange(Object o) {
                if(adapter!=null){
                    manager.log("adapter change");
                    adapter.notifyDataSetChanged();
                }

            }

           };

        OrderedRealmCollectionChangeListener<RealmResults<Message>> callback = new OrderedRealmCollectionChangeListener<RealmResults<Message>>() {
            @Override
            public void onChange(RealmResults<Message> results, OrderedCollectionChangeSet changeSet) {
                if (changeSet == null) {
                    // The first time async returns with an null changeSet.
                    manager.log("Message results "+results.size());
                    adapter= new ChatCursorAdapter(getActivity(), results.sort("msgTime"));
                } else {
                    // Called on every update.
                    adapter.notifyDataSetChanged();
                }
            }
        };


        realm.addChangeListener(realmListener);
        realmResults.addChangeListener(callback);







    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
         String getProfileUsername();

    }



    public class ChatCursorAdapter extends BaseAdapter implements View.OnClickListener {

        RealmResults<Message> messages;
        Context context;
        public ChatCursorAdapter(Context context, RealmResults<Message> messages) {
            this.messages=messages;
            this.context=context;
        }

        @Override public int getCount() {
            return messages.size();
        }

        @Override
        public int getViewTypeCount() {
            return 10;
        }



        private int getItemViewType(Cursor _cursor) {
            int typeIdx = _cursor.getColumnIndex(DataProvider.COL_TYPE);
            return  _cursor.getInt(typeIdx);
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Message message= messages.get(position);
            ViewHolder holder = new ViewHolder();
            View itemLayout = null;

            switch (message.getMsgType())
            {
                case 0:
                    itemLayout = LayoutInflater.from(context).inflate(R.layout.chat_bubble_receive_text, parent, false);
                    break;
                case 1:
                    itemLayout = LayoutInflater.from(context).inflate(R.layout.chat_bubble_send_text, parent, false);
                    break;
                case 2:
                    itemLayout=  LayoutInflater.from(context).inflate(R.layout.bubble_receive_image, parent, false);
                    break;
                case 3:
                    itemLayout=  LayoutInflater.from(context).inflate(R.layout.bubble_send_image, parent, false);
                    break;
                case 4:
                    itemLayout=  LayoutInflater.from(context).inflate(R.layout.bubble_receive_audio, parent, false);
                    break;
                case 5:
                    itemLayout=  LayoutInflater.from(context).inflate(R.layout.bubble_send_audio, parent, false);
                    break;
                case 8:
                    itemLayout=  LayoutInflater.from(context).inflate(R.layout.bubble_receive_file, parent, false);
                    break;
                case 9:
                    itemLayout=  LayoutInflater.from(context).inflate(R.layout.bubble_send_file, parent, false);
                    break;
            }
            itemLayout.setTag(holder);
            holder.id= (TextView)itemLayout.findViewById(R.id.tv_message_id);
            holder.friendUsername=(TextView)itemLayout.findViewById(R.id.friend_username);
            holder.tvTime = (TextView) itemLayout.findViewById(R.id.date_msg);
            holder.msg = (EmojiconTextView) itemLayout.findViewById(R.id.msg);
            if(holder.msg!=null)
                holder.msg.setTag(holder);
            holder.sendImage=(ImageView)itemLayout.findViewById(R.id.bubble_image);
            holder.receiveImage=(ImageView)itemLayout.findViewById(R.id.iv_bubble_receive_image);
            holder.status= (ImageView)itemLayout.findViewById(R.id.msg_status);
            holder.fileProgress =(CircularProgressButton)itemLayout.findViewById(R.id.cpb_file_download_progress);
            holder.playAudio=(ImageButton)itemLayout.findViewById(R.id.iv_bubble_send_audio);
            holder.openFile=(ImageButton)itemLayout.findViewById(R.id.ivOpenFile);
            holder.tvFileSize=(TextView) itemLayout.findViewById(R.id.tvFileSize);
            holder.tvFileName=(TextView) itemLayout.findViewById(R.id.tvFileName);
            holder.audioLayout=itemLayout.findViewById(R.id.lv_bubble_audio);
            JabuWingsActivity.setUpTextViews(context,holder.msg,holder.tvTime,holder.tvFileSize,holder.tvFileName,holder.fileProgress);
            

            return bindView(holder,itemLayout,message);
        }
        
        private View bindView(ViewHolder holder, View itemLayout, Message message)
        {

            String sender_username = message.getSender();
            String time= new Time().parseTime(message.getMsgTime());
//            Date date = new Date(cursor.getString(cursor.getColumnIndex(DataProvider.COL_AT)));
            String msg=message.getMsg();
            String id = message.getId();
            String status = message.getDeliveryStatus();
            String msgId= message.getMsgId();
            com.jabuwings.database.realm.File messageFile = message.getFile();
            String fileUrl=null;
            long fileSize=0;
            String name=null;
            int fileProgress=0;
            
            if(messageFile!=null)
            {
                fileUrl=messageFile.getLocalUrl();
                fileSize=messageFile.getSize();
                name=messageFile.getName();
                fileProgress=messageFile.getProgress();
            }

       /*    if(System.currentTimeMillis()-date.getTime()>60000)
            {
                SQLiteDatabase db = new DatabaseManager(context).getWritableDatabase();
                db.execSQL("update messages set delivery_status= '" +
                                MESSAGE_FAILED + "' where msgId=?",
                        new Object[]{msgId});
            }
*/

            switch(message.getMsgType()) {
                case 0: //Received text
                    holder.friendUsername.setText(sender_username);
                    holder.id.setText(id);
                    setText(holder.msg,msg);
                    holder.tvTime.setText(time);

                    if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                    {
                        HotSpot.setMessageRead(sender_username,msgId,getActivity());
                    }
                    break;
                case 1: //Sent text
                    holder.id.setText(id);
                    setText(holder.msg, msg);
                    holder.tvTime.setText(time);
                    switch (status)
                    {
                        case Const.MESSAGE_SENDING:
                            holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_message_sending));
                            break;
                        case Const.MESSAGE_SENT:
                            holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_message_sent));
                            break;
                        case Const.MESSAGE_DELIVERED:
                            holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_item_message_delivered));
                            break;
                        case Const.MESSAGE_READ:
                            holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_message_delivered));
                            break;
                        case Const.MESSAGE_FAILED:
                            holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_item_message_fail));
                            break;
                    }
                    break;
                case 2: //Received image
                    holder.id.setText(id);
                    holder.fileProgress.setOnClickListener(this);
                    holder.fileProgress.setTag(message);
                    if(fileUrl!=null)
                        Picasso.with(context).load(Uri.fromFile(new File(fileUrl))).into(holder.receiveImage);
                    //Glide.with(context).load(fileUrl).into(holder.receiveImage);
                    //imageProcessor.loadBitmap(fileUrl, holder.receiveImage);
                    holder.receiveImage.setOnClickListener(this);
                    holder.receiveImage.setTag(id);

                    setText(holder.msg,msg);
                    holder.tvTime.setText(time);
                    if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                    {
                        HotSpot.setMessageRead(sender_username,msgId,getActivity());
                    }
                    handleIncomingFileProgress(holder, status, fileProgress);
                    break;
                case 3: //Sent image
                    holder.id.setText(id);
                    holder.fileProgress.setOnClickListener(this);
                    holder.fileProgress.setTag(message);
                    if(fileUrl!=null)
                        Picasso.with(context).load(Uri.fromFile(new File(fileUrl))).into(holder.sendImage);
                    //Glide.with(context).load(fileUrl).into(holder.sendImage);
                    //imageProcessor.loadBitmap(fileUrl, holder.sendImage);
                    holder.sendImage.setOnClickListener(this);
                    holder.sendImage.setTag(message);
                    holder.tvTime.setText(time);
                    setText(holder.msg,msg);
                    handleMessageStatus(holder,status,fileProgress);
                    break;
                case 4: //Received audio
                    holder.id.setText(id);
                    holder.fileProgress.setOnClickListener(this);
                    holder.fileProgress.setTag(message);
                    holder.fileProgress.setProgress(fileProgress);
                    holder.playAudio.setTag(message);
                    holder.playAudio.setOnClickListener(this);
                    holder.tvTime.setText(time);
                    if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                    {
                        HotSpot.setMessageRead(sender_username,msgId,getActivity());
                    }
                    handleIncomingFileProgress(holder, status, fileProgress);
                    break;
                case 5: //Sent audio
                    holder.id.setText(id);
                    holder.fileProgress.setOnClickListener(this);
                    holder.fileProgress.setTag(message);
                    holder.tvTime.setText(time);
                    holder.playAudio.setTag(id);
                    holder.playAudio.setOnClickListener(this);
                    handleMessageStatus(holder, status, fileProgress);
                    break;
                case 8: //received file
                    holder.id.setText(id);
                    holder.fileProgress.setOnClickListener(this);
                    holder.fileProgress.setTag(message);
                    holder.tvTime.setText(time);
                    holder.openFile.setTag(id);
                    holder.openFile.setOnClickListener(this);
                    holder.tvFileName.setText(name);
                    setFileSize(holder.tvFileSize,fileSize);
                    handleIncomingFileProgress(holder, status, fileProgress);
                    break;
                case 9: //Sent file
                    File file =new File(fileUrl);
                    holder.id.setText(id);
                    holder.fileProgress.setOnClickListener(this);
                    holder.fileProgress.setTag(message);
                    holder.tvTime.setText(time);
                    holder.openFile.setTag(fileUrl);
                    holder.openFile.setOnClickListener(this);
                    if(file.exists())
                    {  holder.tvFileName.setText(file.getName());
                        setFileSize(holder.tvFileSize,file.length());}
                    handleMessageStatus(holder, status, fileProgress);
                    if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                    {
                        HotSpot.setMessageRead(sender_username,msgId,getActivity());
                    }
                    break;

            }


            View.OnLongClickListener onLongClickListener=new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    ViewHolder holder = (ViewHolder) v.getTag();
                    final String id = holder.id.getText().toString();

                    final Message message = realm.where(Message.class).equalTo(Const.ID,id).findFirst();

                    if(message!=null)
                    {
                        int type = message.getMsgType();
                        final String msg= message.getMsg();
                        final String msgId=message.getMsgId();
                        final String receiver=message.getReceiver();
                        final String status=message.getDeliveryStatus();
                        int [] r={0,2,4,6,8};
                        //int [] s ={1,3,5,7,9};
                        String copy=getString(R.string.contextual_action_copy_message);
                        String forward=getString(R.string.contextual_action_forward_message);
                        String resend=getString(R.string.contextual_action_resend_message);
                        final String delete=getString(R.string.contextual_action_delete_message);
                        String retract=getString(R.string.contextual_action_retract_message);

                        if(contains(type,r))
                        {
                            //Received message
                            String[] options= {copy,forward,resend,delete};
                            new MaterialDialog.Builder(getActivity()).items(options).itemsCallback(new MaterialDialog.ListCallback() {
                                @Override
                                public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                                    switch (position)
                                    {
                                        case 0:
                                            if(msg!=null){
                                                manager.copyToClipboard(msg);
                                                manager.shortToast(R.string.message_copied);}
                                            else manager.errorToast("Cannot copy empty message");
                                            break;
                                        case 1:
                                            break;
                                        case 3:
                                            HouseKeeping.deleteMessage(message);
                                            manager.successToast(getString(R.string.message_deleted));
                                            break;
                                        case 2:
                                            if(msg!=null && !TextUtils.isEmpty(msg))
                                                HotSpot.sendPersonalMessage(getContext(),manager.getUsername(),mListener.getProfileUsername(),msg,true);
                                            else
                                                manager.errorToast("Only applies to text messages");
                                            break;
                                    }
                                }
                            }).show();
                        }
                        else{
                            //sent message
                            String[] options= {copy,forward,resend,delete,retract};

                            new MaterialDialog.Builder(getActivity()).items(options).itemsCallback(new MaterialDialog.ListCallback() {
                                @Override
                                public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                                    switch (position)
                                    {
                                        case 0:
                                            if(msg!=null){
                                                manager.copyToClipboard(msg);
                                                manager.shortToast(R.string.message_copied);}
                                            else manager.errorToast("Cannot copy empty message");
                                            break;
                                        case 1:
                                            break;
                                        case 3:
                                            HouseKeeping.deleteMessage(message);
                                            manager.successToast(getString(R.string.message_deleted));
                                            break;
                                        case 2:
                                            if(msg!=null && !TextUtils.isEmpty(msg))
                                                HotSpot.sendPersonalMessage(getContext(),manager.getUsername(),mListener.getProfileUsername(),msg,true);
                                            else
                                                manager.errorToast("Only applies to text messages");
                                            break;
                                        case 4:
                                            switch (status)
                                            {
                                                case MESSAGE_READ:
                                                    manager.errorToast("You cannot retract a message that has been read.");
                                                    break;
                                                case MESSAGE_FAILED:
                                                    manager.shortToast("No need to retract. Message was not sent");
                                                    break;
                                                default:
                                                    new SweetAlertDialog(getActivity(),SweetAlertDialog.WARNING_TYPE).setTitleText(getString(R.string.warning)).setContentText("Retraction will delete the message from the recipient's device only if the recipient has not read the particular message").setConfirmText(getString(R.string.continue_)).setCancelText(getString(R.string.cancel)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                        @Override
                                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                            sweetAlertDialog.dismiss();
                                                            HotSpot.retractPersonalMessage(getActivity(),receiver,msgId,id);
                                                        }
                                                    }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                        @Override
                                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                            sweetAlertDialog.dismiss();
                                                        }
                                                    }).show();

                                            }



                                            break;

                                    }
                                }
                            }).show();

                        }
                    }

                    return false;
                }
            };
            itemLayout.setOnLongClickListener(onLongClickListener);
            if(holder.msg!=null)
                holder.msg.setOnLongClickListener(onLongClickListener);
            return itemLayout;
        }

    

        @Override
        public void onClick(View v) {
            Message message;
            switch (v.getId())
            {
                case R.id.bubble_image:
                    message= (Message)v.getTag();

                    startActivity(new Intent(context, ImageSlider.class).putExtra(Const.PATH,message.getFile().getLocalUrl()).putExtra(Const.USERNAME,message.getReceiver()).putExtra(Const.PRIVATE,true));
                    //startActivity(new Intent(mContext, ImageViewer.class).putExtra(Const.IMAGE_VIEWER_URI,messageItem.fileLocalUrl).putExtra(Const.VIEW_THIS_IMAGE,messageItem.receiver).putExtra(Const.PRIVATE,false));
                    break;
                case R.id.iv_bubble_receive_image:
                    message= (Message)v.getTag();

                    startActivity(new Intent(context, ImageSlider.class).putExtra(Const.PATH,message.getFile().getLocalUrl()).putExtra(Const.USERNAME,message.getReceiver()).putExtra(Const.PRIVATE,true));
                    //startActivity(new Intent(mContext, ImageViewer.class).putExtra(Const.IMAGE_VIEWER_URI,messageItem.fileLocalUrl).putExtra(Const.VIEW_THIS_IMAGE,messageItem.receiver).putExtra(Const.PRIVATE,false));
                    break;
                case R.id.iv_bubble_send_audio:
                    message= (Message)v.getTag();
                    AudioManager
                            .getInstance()
                            .initialise(context)
                            .playAudio(message.getFile().getLocalUrl(),getFragmentManager());

                    break;
                case R.id.ivOpenFile:
                    String url=(String)v.getTag();
                    File file= new File(url);
                    if(file.exists())
                    new FileManager(context).openFile(file);
                    else
                    manager.errorToast("File not found");
                    break;
                case R.id.cpb_file_download_progress:
                    message= (Message)v.getTag();

                    String status=message.getDeliveryStatus();

                    /**
                     * If true, it means the file is meant to be uploaded. Otherwise, download;
                     */
                    boolean upload=message.getSender().equals(manager.getUsername());

                    switch (status)
                    {
                        case Const.MESSAGE_SENDING:
                            dialogWizard.showSimpleDialog("Info","The file is being sent");
                            break;
                        case Const.MESSAGE_SENT:
                            dialogWizard.showSimpleDialog("Info","The file has been sent");
                            break;
                        case Const.MESSAGE_DELIVERED:
                            dialogWizard.showSimpleDialog("Info","The file has been delivered");
                            break;
                        case Const.MESSAGE_READ:
                            dialogWizard.showSimpleDialog("Info","The file has been read");
                            break;
                        case Const.MESSAGE_FAILED:
                            if(upload)
                            {


                                manager.shortToast("Attempting to resend message");
                                if(message.getMsgType()==DataProvider.MessageType.OUTGOING_IMAGE.ordinal())
                                new PictureUploader(getContext(),null,null,Const.MEDIA_UPLOAD_TYPE.MESSAGE_PICTURE.ordinal(),message.getReceiver(),message.getMsg()).execute(Uri.parse(message.getFile().getLocalUrl()));
                                else ; //TODO implement for audio messages
                                HouseKeeping.deleteMessage(message);
                            }

                            else{
                                manager.retryFileDownload(message);
                            }

                            break;
                    }




                    break;
            }

        }



      




        }

    private boolean contains(int n, int[]a)
    {
        for(int x:a)
        {
            if(x==n) return true;
        }
        return false;
    }


    public void setFileSize(TextView tvFileSize, long size)
    {
        if(size<1048576)
        {
            long kb=size/1024;
            tvFileSize.setText(kb+"KB");
        }
        else{
            long mb=size/1024/1024;
            tvFileSize.setText(mb+"MB");
        }
    }

    private void handleIncomingFileProgress(ViewHolder holder, String status,int fileProgress)
    {
        switch (status)
        {
            case Const.MESSAGE_SENT:
                holder.fileProgress.setVisibility(View.GONE);
                break;
            case Const.MESSAGE_FAILED:
                holder.fileProgress.setProgress(-1);
                break;
            case Const.MESSAGE_DELIVERED:
                holder.fileProgress.setVisibility(View.GONE);
                break;
            case Const.MESSAGE_READ:
                holder.fileProgress.setVisibility(View.GONE);
                break;

        }

    }
    private void handleMessageStatus( ViewHolder holder, String status,int fileProgress)
    {
        if(status.equals(Const.MESSAGE_SENDING))
        {
            holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_message_sending));
            holder.fileProgress.setProgress(fileProgress);
            manager.log("Messages", "Sending" + fileProgress);
        }
        else
            switch (status)
            {
                case Const.MESSAGE_SENT:
                    holder.fileProgress.setVisibility(View.GONE);
                    holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_message_sent));
                    break;
                case Const.MESSAGE_FAILED:
                    holder.fileProgress.setProgress(-1);
                    holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_item_message_fail));
                    break;
                case Const.MESSAGE_DELIVERED:
                    holder.fileProgress.setVisibility(View.GONE);
                    holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_item_message_delivered));
                    break;
                case Const.MESSAGE_READ:
                    holder.fileProgress.setVisibility(View.GONE);
                    holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_message_delivered));
                    break;

            }


    }

    private void setText(EmojiconTextView msgTextView, String msg)
    {
        if(msg!=null) msgTextView.setText(msg);
        else msgTextView.setText("");
    }

    private static class ViewHolder {
        TextView id;
        TextView tvTime;
        EmojiconTextView msg;
        TextView friendUsername;
        ImageView sendImage;
        ImageView receiveImage;
        ImageView status;
        View audioLayout;
        ImageView tvUserImage;
        ImageButton playAudio;
        ImageButton openFile;
        TextView tvFileSize;
        TextView tvFileName;
        CircularProgressButton fileProgress;


    }


}
