package com.jabuwings.models;

import android.app.Activity;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.Utils;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.CircleImage;
import com.nightonke.boommenu.BoomButtons.SimpleCircleButton;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.Util;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MultipleUsersAdapter extends BaseAdapter


    {


        List<JabuWingsPublicUser> users;
        OnBoomListener onSubButtonClickListener;
        Activity context;
        public MultipleUsersAdapter(Activity context,List<JabuWingsPublicUser> users,OnBoomListener onSubButtonClickListener)

        {
            this.users=users;
            this.context=context;
            this.onSubButtonClickListener=onSubButtonClickListener;

        }

        public void update(List<JabuWingsPublicUser> users)
        {
            this.users=users;
        }
        @Override
        public int getCount() {
            return users.size();
        }

        @Override
        public JabuWingsPublicUser getItem(int position) {
            return users.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            if(convertView==null) {
                convertView = context.getLayoutInflater().inflate(R.layout.friend_search_item, parent,false);
                holder= new ViewHolder();
                holder.name = (TextView) convertView.findViewById(R.id.friend_search_name);
                holder.username = (TextView) convertView.findViewById(R.id.friend_search_username);
                holder.profilePicture=(CircleImage)convertView.findViewById(R.id.friend_search_profile_picture);
                holder.btAdd=(BoomMenuButton) convertView.findViewById(R.id.boomFriendItem);
                convertView.setTag(holder);
            }
            else {
                holder=(ViewHolder)convertView.getTag();
            }


            JabuWingsPublicUser jabuWingsUser =getItem(position);

            String profileUrl=jabuWingsUser.getProfilePictureUrl();
            if(profileUrl!=null)
                Picasso.with(context)
                        .load(profileUrl)
                        .placeholder(R.drawable.default_avatar)
                        .into(holder.profilePicture);

            holder.name.setText(jabuWingsUser.getName());
            holder.username.setText(jabuWingsUser.getUsername());
            holder.btAdd.setTag(jabuWingsUser);

            int[] drawablesResource = new int[]{
                    R.drawable.photo,
                    R.drawable.requests,
                    R.drawable.add_friend
            };
            holder.btAdd.setOnBoomListener(onSubButtonClickListener);
            holder.btAdd.clearBuilders();
            final String[] circleSubButtonTexts={"Picture","Profile","Add"};
            for (int i = 0; i <  holder.btAdd.getPiecePlaceEnum().pieceNumber(); i++)
                holder.btAdd.addBuilder(new TextInsideCircleButton.Builder().normalImageRes(drawablesResource[i]).normalText(circleSubButtonTexts[i]).imagePadding(new Rect(30,30,30,30)).typeface(new Manager(context).getTypeFace()));

           /* final Drawable[] circleSubButtonDrawables = new Drawable[3];

            for (int i = 0; i < 3; i++)
                circleSubButtonDrawables[i]
                        = ContextCompat.getDrawable(context, drawablesResource[i]);
            final int[][] subButtonColors = new int[3][2];
            for (int i = 0; i < 3; i++) {
                subButtonColors[i][1] = Utils.GetRandomColor();
                subButtonColors[i][0] = Util.getInstance().getPressedColor(subButtonColors[i][1]);
            }
            final String[] circleSubButtonTexts={"Picture","Profile","Add"};
            holder.btAdd.postDelayed(new Runnable() {
                @Override
                public void run() {
                    new BoomMenuButton.Builder()
                            .subButtons(circleSubButtonDrawables, subButtonColors, circleSubButtonTexts)
                            .button(ButtonType.CIRCLE)
                            .boom(BoomType.PARABOLA)
                            .place(PlaceType.CIRCLE_3_1)
                            .subButtonsShadow(Util.getInstance().dp2px(2), Util.getInstance().dp2px(2))
                            .onSubButtonClick(onSubButtonClickListener)
                            .init(holder.btAdd);
                }
            },1);
*/




            return convertView;
        }


      public class  ViewHolder {
          TextView name,username;
          CircleImage profilePicture;
          BoomMenuButton btAdd;
        }
    }