package com.jabuwings.models;

import com.jabuwings.management.Const;
import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;

/**
 * Created by Falade James on 10/3/2016 All Rights Reserved.
 */
@ParseClassName("ClassroomMaterials")
public class ClassroomMaterial extends ParseObject {

    boolean downloaded=false;
    public String getTitle()
    {
        return getString(Const.TITLE);
    }

    public String getDetails()
    {
        return getString(Const.DETAILS);
    }

    public ParseFile getFile()
    {
        return getParseFile(Const.FILE);
    }
    public static ParseQuery<ClassroomMaterial> getQuery(){
        return ParseQuery.getQuery(ClassroomMaterial.class);
    }
}
