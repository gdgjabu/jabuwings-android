package com.jabuwings.models;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.realm.Hub;
import com.jabuwings.database.realm.HubMessage;
import com.jabuwings.image.ImageProcessor;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.ImageViewer;
import com.jabuwings.views.main.Domicile;
import com.jabuwings.views.main.HubChat;
import com.jabuwings.views.main.HubProfile;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.CircleImage;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.OnBoomListener;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;


public class HubsAdapter extends RecyclerView.Adapter<HubsAdapter.ViewHolder> {

        View itemLayout;

        String title, profilePictureURL;
        String id;
        boolean newSignIn;
        static Activity context ;
        Manager manager;

        ImageProcessor imageProcessor;
        List<Hub> hubs;


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hubs_grid, parent, false);
        ViewHolder holder= new ViewHolder(itemView, new HubClicks() {
            @Override
            public void onClick(View v) {
                if(v.getId()==R.id.hub_picture){
                    if(v.getTag()!=null)
                    {
                    String hubId = (String)v.getTag();
                        String where = DataProvider.COL_HUB_ID + "=?";
                        String[] whereArgs = new String[]{hubId};
                        Cursor c = manager.getDatabaseManager().getReadableDatabase().query(true, DataProvider.TABLE_HUBS, null, where, whereArgs, null, null, null, null);

                        if (c != null && c.moveToFirst()) {
                            String title= c.getString(c.getColumnIndex(DataProvider.COL_HUB_TITLE));
                            context.startActivity(new Intent(context, ImageViewer.class).putExtra(Const.VIEW_THIS_IMAGE, title)
                                    .putExtra(Const.IMAGE_VIEWER_URI, manager.getFriendOrHubProfilePictureName(hubId)));
                            c.close();
                        }




                    }

                }
                else{
              TextView  tvHubId=(TextView)v.findViewById(R.id.hub_id);
              String id=tvHubId.getText().toString();
                context.startActivity(new Intent(context, HubChat.class).
                        putExtra(Const.HUB_ID, id));
                }
            }

            @Override
            public void onBoomClick(int buttonIndex,BoomMenuButton menuButton) {
                try{
                    final JabuWingsHub hub = (JabuWingsHub) menuButton.getTag();
                    String title = hub.getTitle();
                    final String hubId=hub.getId();
                    switch (buttonIndex) {
                        case 0:
                            context.startActivity(new Intent(context, ImageViewer.class).putExtra(Const.VIEW_THIS_IMAGE, title)
                                    .putExtra(Const.IMAGE_VIEWER_URI, manager.getFriendOrHubProfilePictureName(hubId)));
                            break;
                        case 1:
                            context.startActivity(new Intent(context, HubProfile.class).putExtra(Const.HUB_ID, hubId));
                            break;
                        case 2:
                            new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE).setTitleText("Remove " + title).setContentText("Are you sure? " + "You will no longer be able to communicate with the hub.").setConfirmText("Remove").setCancelText(context.getString(R.string.cancel)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                    HotSpot.removeFriend(context, hubId);
                                }
                            }).show();

                            break;
                    }

                } catch (Exception  e){e.printStackTrace();}
            }

            @Override
            public void onLongClick(View v) {
                BoomMenuButton boom = (BoomMenuButton)v.getTag();
                boom.boom();
            }
        });

        return holder;
    }

    @Override
    public int getItemCount() {
        return hubs.size();
    }

    public HubsAdapter(Activity context, List<Hub> hubs) {
            this.context=context;
            manager = new Manager(context);
            imageProcessor=new ImageProcessor(context);
            this.newSignIn= Domicile.newSignIn;
            this.hubs=hubs;
        }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Hub hub =hubs.get(position);
        title = hub.getTitle();
        id=hub.getHubId();
        int lastMessageType=0;
        String timestamp =null, lastMessageStatus=null;
        HubMessage lastMessage = hub.getLastMessage();
        if(lastMessage!=null){
            timestamp=lastMessage.getTime();
            lastMessageType = lastMessage.getType();
            lastMessageStatus = lastMessage.getDeliveryStatus();
        }

        // TODO
        int count=0;
        HubMessage last = hub.getLastMessage();
        String lastMsg=last==null? null : last.getMsg();
        holder.title.setText(title);
        holder.hubId.setText(id);
        if(timestamp!=null)
        holder.timestamp.setText(new Time().parseTime(timestamp));

        if(lastMsg!=null)
        holder.lastMessage.setText(lastMsg.replaceAll("[\\t\\n\\r]"," "));
        else holder.lastMessage.setText("");
        getAdminRank(id,holder,holder.status);
        holder.hubPicture.setTag(id);
        JabuWingsActivity.setUpTextViews(context,holder.title,holder.timestamp,holder.count,holder.lastMessage,holder.membershipStatus);
        profilePictureURL= hub.getPicture();
        if (count > 0) {

            holder.count.setVisibility(View.VISIBLE);
            holder.count.setText(String.valueOf(count));
        } else
            holder.count.setVisibility(View.GONE);
        //holder.boom.setTag(id);
        imageProcessor.loadBitmap(manager.getFriendOrHubProfilePictureName(id),holder.hubPicture);

        if(lastMessage!=null)

        if(lastMessageType==DataProvider.MessageType.INCOMING_VOICE_NOTE.ordinal() || lastMessageType==DataProvider.MessageType.OUTGOING_VOICE_NOTE.ordinal())
        {
            holder.lastMessageMedia.setVisibility(View.VISIBLE);
            holder.lastMessageMedia.setImageDrawable(context.getResources().getDrawable(R.drawable.emoji_1f3b5));
        }
        else
        if(lastMessageType==DataProvider.MessageType.OUTGOING_IMAGE.ordinal() || lastMessageType==DataProvider.MessageType.INCOMING_IMAGE.ordinal())
        {
            holder.lastMessageMedia.setVisibility(View.VISIBLE);
            holder.lastMessageMedia.setImageDrawable(context.getResources().getDrawable(R.drawable.emoji_1f5fb));
        }

        if(manager.arrayContains(DataProvider.SENT_MESSAGE_TYPES,lastMessageType))

            switch (lastMessageStatus)
            {
                case Const.MESSAGE_SENDING:
                    holder.lastMessageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_message_sending));
                    break;
                case Const.MESSAGE_SENT:
                    holder.lastMessageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_message_sent));
                    break;
                case Const.MESSAGE_DELIVERED:
                    holder.lastMessageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_item_message_delivered));
                    break;
                case Const.MESSAGE_READ:
                    holder.lastMessageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_message_delivered));
                    break;
                case Const.MESSAGE_FAILED:
                    holder.lastMessageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_item_message_fail));
                    break;
            }

        else{
            holder.lastMessageStatus.setImageDrawable(null);
        }
    }

    private void getAdminRank(String hub_id, final ViewHolder holder, final TextView status)
    {
        ParseQuery<ParseObject> query = new ParseQuery<>(Const.HUB_CLASS_NAME);
        query.fromLocalDatastore().getInBackground(hub_id, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                    if(e==null)
                    {
                        JabuWingsHub hub = new JabuWingsHub(parseObject);
                        holder.boom.setTag(hub);
                        if(hub.getCreatedBy().equals(manager.getUsername()))
                        {
                            holder.membershipStatus.setText(R.string.chief_admin);
                            return;
                        }
                        if(hub.adminContains(manager.getUsername()))
                        {
                            holder.membershipStatus.setText(R.string.admin);
                        }

                        else{
                            holder.membershipStatus.setText(R.string.member);
                        }

                        status.setText(hub.getStatus());
                    }
            }
        });
    }



    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,OnBoomListener,View.OnLongClickListener {
        TextView title;
        TextView hubId;
        TextView count;
        TextView timestamp;
        EmojiconTextView lastMessage;
        TextView status,membershipStatus;
        CircleImage hubPicture;
        HubClicks hubClicks;
        BoomMenuButton boom;
        ImageView lastMessageStatus;
        ImageView lastMessageMedia;
        Manager manager;
        public ViewHolder(View view, HubClicks listener)
        {
            super(view);
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
            this.hubClicks=listener;
            manager= new Manager(context);
            title = (TextView) view.findViewById(R.id.hub_title);
            hubPicture = (CircleImage) view.findViewById(R.id.hub_picture);
            hubPicture.setOnClickListener(this);
            hubId=(TextView)view.findViewById(R.id.hub_id);
            count=(TextView)view.findViewById(R.id.hub_msg_count);
            timestamp=(TextView)view.findViewById(R.id.hub_timestamp);
            lastMessage=(EmojiconTextView)view.findViewById(R.id.hub_last_msg);
            status=(TextView)view.findViewById(R.id.hub_status);
            membershipStatus=(TextView)view.findViewById(R.id.hub_membership_status);
            boom= (BoomMenuButton)view.findViewById(R.id.hub_boom);
            lastMessageStatus = (ImageView)view.findViewById(R.id.hub_last_msg_status);
            lastMessageMedia = (ImageView)view.findViewById(R.id.hub_last_media);
            view.setTag(boom);

            JabuWingsActivity.setUpTextViews(context,title,count,timestamp,status,lastMessage,membershipStatus);



            int[] drawablesResource = new int[]{
                    R.drawable.photo,
                    R.drawable.requests,
                    R.drawable.remove
            };
            String viewProfile=context.getString(R.string.profile);
            String viewPicture=context.getString(R.string.picture);
            String remove=context.getString(R.string.remove);
            final String[] circleSubButtonTexts={viewPicture,viewProfile,remove};
            boom.clearBuilders();
            for (int i = 0; i < boom.getPiecePlaceEnum().pieceNumber(); i++)
                boom.addBuilder(new TextInsideCircleButton.Builder().normalImageRes(drawablesResource[i]).normalText(circleSubButtonTexts[i]).imagePadding(new Rect(30,30,30,30)).typeface(new Manager(context).getTypeFace()));


           /* final Drawable[] circleSubButtonDrawables = new Drawable[3];

            for (int i = 0; i < 3; i++)
                circleSubButtonDrawables[i]
                        = ContextCompat.getDrawable(context, drawablesResource[i]);
            final int[][] subButtonColors = new int[3][2];
            for (int i = 0; i < 3; i++) {
                subButtonColors[i][1] = Utils.GetRandomColor();
                subButtonColors[i][0] = Util.getInstance().getPressedColor(subButtonColors[i][1]);
            }
            String viewProfile=context.getString(R.string.profile);
            String viewPicture=context.getString(R.string.picture);
            String remove=context.getString(R.string.remove);
            final String[] circleSubButtonTexts={viewPicture,viewProfile,remove};
            final BoomMenuButton.OnSubButtonClickListener onSubButtonClickListener=this;
            boom.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Now with Builder, you can init BMB more convenient
                    new BoomMenuButton.Builder()
                            .subButtons(circleSubButtonDrawables, subButtonColors, circleSubButtonTexts)
                            .button(ButtonType.CIRCLE)
                            .boom(BoomType.PARABOLA)
                            .place(PlaceType.CIRCLE_3_1)
                            .subButtonsShadow(Util.getInstance().dp2px(2), Util.getInstance().dp2px(2))
                            .onSubButtonClick(onSubButtonClickListener)
                            .init(boom);
                }
            }, 1);
*/
            boom.setOnBoomListener(this);

        }

        @Override
        public void onClicked(int index, BoomButton boomButton,BoomMenuButton menuButton) {
            hubClicks.onBoomClick(index,menuButton);
        }

        @Override
        public void onBackgroundClick() {

        }

        @Override
        public void onBoomWillHide() {

        }

        @Override
        public void onBoomDidHide() {

        }

        @Override
        public void onBoomWillShow() {

        }

        @Override
        public void onBoomDidShow() {

        }



        @Override
        public void onClick(View v) {
            hubClicks.onClick(v);
        }

        @Override
        public boolean onLongClick(View v) {
            hubClicks.onLongClick(v);
            return true;
        }
    }

    private interface HubClicks{
       void onClick(View v);
        void onLongClick(View v);
        void onBoomClick(int buttonIndex, BoomMenuButton boomMenuButton);
    }


    }