package com.jabuwings.models;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.management.Manager;
import com.jabuwings.views.timetable.TimeKeeper;
import com.jabuwings.views.timetable.TimeTable2;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.OnBoomListener;

import java.util.List;

/**
 * Created by Falade James on 10/17/2015 All Rights Reserved.
 */
public class TimeTableRecycler extends RecyclerView.Adapter<TimeTableRecycler.ViewHolder> {
List<TimeTableItem> items;
Context context;


    public TimeTableRecycler(List<TimeTableItem> items,Context context)
    {
        this.context=context;
        this.items=items;
        TimeTable2 timetable = TimeTable2.getInstance();
        if(timetable!=null)
        if(items.size()==0) items.add(new TimeTableItem("No courses available for: \n"+timetable.getConfiguration(),null,null,null,null,null,null));
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.time_table_item, parent, false);

        return new ViewHolder(v, new TimeTableClicks() {
            @Override
            public void onClick(final View v) {


            }
        });
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TimeTableItem item =items.get(position);
        /**
         * If true it means the list is empty and a message is shown
         */
        if(position==0 && item.getTimeStart()==null)
        {
            holder.tvCourseVenue.setVisibility(View.GONE);
            holder.tvTimeStart.setVisibility(View.GONE);
            holder.tvTimeStop.setVisibility(View.GONE);
            holder.ibMenu.setVisibility(View.GONE);
            holder.divider.setVisibility(View.GONE);

        }
        char separator='_';
        holder.tvCourseCode.setText(item.courseCode);
        holder.tvCourseVenue.setText(item.courseVenue);
        holder.tvTimeStart.setText(item.timeStart);
        holder.tvTimeStop.setText(item.timeStop);
        holder.ibMenu.setTag(item.courseCode+separator+item.timeStart+separator+item.timeStop+separator+item.courseVenue+separator+item.level+separator+item.day);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,OnBoomListener {
        TextView tvCourseCode;
        TextView tvCourseVenue;
        TextView tvTimeStart;
        TextView tvTimeStop;
        BoomMenuButton ibMenu;
        View divider;
        TimeTableClicks listener;

        ViewHolder(View itemView, final TimeTableClicks listener) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.listener = listener;
            tvCourseCode = (TextView) itemView.findViewById(R.id.tvTime_table_code);
            tvCourseVenue = (TextView) itemView.findViewById(R.id.tvItem_venue);
            tvTimeStart = (TextView) itemView.findViewById(R.id.tvTime_table_start);
            tvTimeStop = (TextView) itemView.findViewById(R.id.tvTime_table_stop);
            divider=itemView.findViewById(R.id.divider);
            ibMenu = (BoomMenuButton) itemView.findViewById(R.id.ib_time_table_menu);

            int[] drawablesResource = new int[]{
                    R.drawable.ic_edit_white_48dp,
                    R.drawable.remove
            };
            ibMenu.clearBuilders();
            final String[] circleSubButtonTexts={"Edit","Delete"};
            for (int i = 0; i < ibMenu.getPiecePlaceEnum().pieceNumber(); i++)
                ibMenu.addBuilder(new TextInsideCircleButton.Builder().normalImageRes(drawablesResource[i]).normalText(circleSubButtonTexts[i]).imagePadding(new Rect(30,30,30,30)).typeface(new Manager(context).getTypeFace()));
            /*final Drawable[] circleSubButtonDrawables = new Drawable[2];

            for (int i = 0; i < 2; i++)
                circleSubButtonDrawables[i]
                        = ContextCompat.getDrawable(context, drawablesResource[i]);
            final int[][] subButtonColors = new int[2][2];
            for (int i = 0; i < 2; i++) {
                subButtonColors[i][1] = Utils.GetRandomColor();
                subButtonColors[i][0] = Util.getInstance().getPressedColor(subButtonColors[i][1]);
            }
            final String[] circleSubButtonTexts={"Edit","Delete"};
            final BoomMenuButton.OnSubButtonClickListener onSubButtonClickListener=this;
            ibMenu.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Now with Builder, you can init BMB more convenient
                    new BoomMenuButton.Builder()
                            .subButtons(circleSubButtonDrawables, subButtonColors, circleSubButtonTexts)
                            .button(ButtonType.CIRCLE)
                            .boom(BoomType.PARABOLA)
                            .place(PlaceType.CIRCLE_2_1)
                            .subButtonsShadow(Util.getInstance().dp2px(2), Util.getInstance().dp2px(2))
                            .onSubButtonClick(onSubButtonClickListener)
                            .init(ibMenu);
                }
            }, 1);
*/
            ibMenu.setOnBoomListener(this);

        }

        @Override
        public void onClicked(int index, BoomButton b,BoomMenuButton v) {
            switch (index)
            {
                case 0:
                    new TimeKeeper(context).editPeriod((String)v.getTag(),TimeTable2.getInstance().collegeName);
                    break;
                case 1:
                    new TimeKeeper(context).deletePeriod((String)v.getTag(),TimeTable2.getInstance().collegeName);
                    break;
            }
        }

        @Override
        public void onBackgroundClick() {

        }

        @Override
        public void onBoomWillHide() {

        }

        @Override
        public void onBoomDidHide() {

        }

        @Override
        public void onBoomWillShow() {

        }

        @Override
        public void onBoomDidShow() {

        }


        @Override
        public void onClick(final View v) {
            switch (v.getId())
            {
                default:
                    listener.onClick(v);
                    break;

            }

        }
    }

    public interface TimeTableClicks{
        void onClick(View v);
    }
}
