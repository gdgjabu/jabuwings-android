package com.jabuwings.models;

import android.util.Log;

import com.jabuwings.management.Const;
import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.Date;
import java.util.List;

/**
 * Created by Falade James on 7/18/2016 All Rights Reserved.
 */
@ParseClassName("UserFriend")
public class JabuWingsFriend extends ParseObject {


    public String getName()
    {
        return getString(Const.NAME);
    }
    public String getUsername()
    {
        return getString(Const.USERNAME);
    }

    public String getDepartment()
    {
        return getString(Const.DEPARTMENT);
    }

    public String getStatus()
    {
        return getString(Const.STATUS);
    }

    public String getLevel()
    {
        return getString(Const.LEVEL);
    }
    public String getBirthday()
    {
        return getString(Const.BIRTHDAY);
    }

    public String getStateOfOrigin()
    {
        return getString(Const.STATE);
    }

    public String getPicture()
    {
        return getString(Const.PROFILE_PICTURE_URL);
    }


    public String getBirthDay()

    {
        return getString(Const.BIRTHDAY);
    }



    public String getId(){return  getObjectId();}

    public Date getLastSeen()
    {
        return getDate(Const.LAST_SEEN);
    }

    public int getAccountType()
    {
        return getInt(Const.ACCOUNT_TYPE);
    }

    public JabuWingsPublicUser getPublicFriend()
    {
        return (JabuWingsPublicUser) getParseObject(Const.USER_PUBLIC);
    }


    public List<String> getFriends()
    {
        return getList(Const.FRIENDS);
    }

    public boolean isFriendOf(String username)
    {
        return getFriends()!=null &&getFriends().contains(username);
    }

    public boolean isOnline(){
        Date lastSeen= getLastSeen();
        if(lastSeen!=null)
        {long difference = System.currentTimeMillis()-lastSeen.getTime();
            return difference<=180000;}
        else return false;
    }


    public String isVerifiedAs(){
        return getString(Const.VERIFIED_AS);
    }

    public boolean isVerified()
    {
        return getString(Const.VERIFIED_AS)!=null;
    }



    public ParseFile getProfilePicture()
    {return getParseFile(Const.PROFILE_PICTURE);}
    public String getProfilePictureUrl()

    {
        ParseFile parseFile = getProfilePicture();
        if(parseFile!=null)
        return parseFile.getUrl();
        return null;
    }

    public long getProfilePictureDate(){
        Date date= getDate(Const.PROFILE_PICTURE_DATE);
        if(date == null) return 0;
        else return date.getTime();}
    public static ParseQuery<JabuWingsFriend> getQuery(){return ParseQuery.getQuery(JabuWingsFriend.class);}

}
