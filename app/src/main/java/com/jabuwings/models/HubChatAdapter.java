package com.jabuwings.models;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.realm.Friend;
import com.jabuwings.database.realm.Hub;
import com.jabuwings.database.realm.HubMessage;
import com.jabuwings.database.realm.Message;
import com.jabuwings.image.PictureUploader;
import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.FileManager;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.AudioManager;
import com.jabuwings.views.hooks.ImageSlider;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.progressbutton.CircularProgressButton;
import com.squareup.picasso.Picasso;

import java.io.File;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by jamesfalade on 25/08/2017.
 */

public class HubChatAdapter extends RealmRecyclerViewAdapter<HubMessage,HubChatAdapter.ViewHolder> implements View.OnClickListener{

    JabuWingsActivity context;
    OrderedRealmCollection<HubMessage> messages;
    private Realm realm;
    Hub hub;
    Time time;
    Manager manager;
    public HubChatAdapter(JabuWingsActivity context, OrderedRealmCollection<HubMessage> messages, Realm realm, Hub hub)
    {
        super(messages,true);
        this.realm = realm;
        this.hub = hub;
        this.context = context;
        time = new Time();
        this.messages = messages;
        manager = new Manager(context);
    }


    @Override
    public int getItemViewType(int position) {
        return messages.get(position).getType();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemLayout = null;
        switch(viewType){
            case 0:
                itemLayout = LayoutInflater.from(context).inflate(R.layout.bubble_hub_receive_text, parent, false);
                break;
            case 1:
                itemLayout = LayoutInflater.from(context).inflate(R.layout.bubble_hub_send_text1, parent, false);
                break;
            case 2:
                itemLayout = LayoutInflater.from(context).inflate(R.layout.bubble_hub_receive_image, parent, false);
                break;
            case 3:
                itemLayout= LayoutInflater.from(context).inflate(R.layout.bubble_send_image, parent, false);
                break;
            case 4:
                itemLayout= LayoutInflater.from(context).inflate(R.layout.bubble_hub_receive_audio, parent, false);
                break;
            case 5:
                itemLayout= LayoutInflater.from(context).inflate(R.layout.bubble_send_audio, parent, false);
                break;
            case 6: //incoming reply
                itemLayout = LayoutInflater.from(context).inflate(R.layout.bubble_hub_receive_text_reply, parent, false);
                break;
            case 7: //outgoing reply
                itemLayout = LayoutInflater.from(context).inflate(R.layout.bubble_hub_send_text_reply, parent, false);
                break;
            case 8:
                itemLayout=  LayoutInflater.from(context).inflate(R.layout.bubble_hub_receive_file, parent, false);
                break;
            case 9:
                itemLayout=  LayoutInflater.from(context).inflate(R.layout.bubble_send_file, parent, false);
                break;
        }
        ViewHolder holder = new ViewHolder(itemLayout);
        itemLayout.setTag(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        HubMessage hubMessage = messages.get(position);
        String status = hubMessage.getDeliveryStatus();
        String databaseId= hubMessage.getId();
        String msgId=hubMessage.getMsgId();
        String reply= hubMessage.getReply();


        //TODO to be reconsidered
            /*if(reply!=null) {
                String where = DataProvider.COL_ID + "=?";
                String[] whereArgs = new String[]{reply};
                Cursor c = manager.getDatabaseManager().getReadableDatabase().query(true, DataProvider.TABLE_HUB_MESSAGES, null, where, whereArgs, null, null, null, null);

                if (c != null && c.moveToFirst()) {
                    reply = c.getString(cursor.getColumnIndex(DataProvider.COL_HUB_MESSAGE));
                    c.close();
                }
            }*/
        String fileUrl=null;
        int fileProgress=0;
        long fileSize=0;
        String name= null;
        if(hubMessage.getFile()!=null)
        {
            fileUrl = hubMessage.getFile().getLocalUrl();
            fileProgress = hubMessage.getFile().getProgress();
            fileSize = hubMessage.getFile().getSize();
        }

        time = new Time();
        holder.date.setText(time.parseTime(hubMessage.getTime()));
        if(holder.msg!=null)
            holder.msg.setText(hubMessage.getMsg());
        holder.msgId.setText(msgId);
        holder.databaseId.setText(databaseId);
        if(holder.sender!=null) holder.sender.setText(hubMessage.getSender());

        switch(hubMessage.getType()) {
            case 0:
                if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                {


                    //    HotSpot.setMessageRead(sender_username, msgId, getActivity());


                }



                break;
            case 1:
                // holder.id.setText(id);
                //  holder.msg.setText(msg);
                // holder.tvTime.setText(time);
                handleSending(holder,status);
                break;
            case 2:

                holder.fileProgress.setOnClickListener(this);
                holder.fileProgress.setTag(hubMessage);
                Picasso.with(context).load(Uri.fromFile(new File(fileUrl))).into(holder.image);
                holder.image.setOnClickListener(this);
                holder.image.setTag(hubMessage);

                if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                {
//                        HotSpot.setMessageRead(sender_username,msgId,getActivity());
                }
                handleMessageStatus(holder, status, fileProgress);
                break;
            case 3:
                handleSending(holder,status);
                handleMessageStatus(holder, status, fileProgress);
                holder.fileProgress.setOnClickListener(this);
                holder.fileProgress.setTag(hubMessage);
                Picasso.with(context).load(Uri.fromFile(new File(fileUrl))).into(holder.image);
                holder.image.setOnClickListener(this);
                holder.image.setTag(hubMessage);
                break;
            case 4: //Received audio
                handleMessageStatus(holder, status, fileProgress);
                holder.fileProgress.setOnClickListener(this);
                holder.fileProgress.setTag(hubMessage);
                holder.fileProgress.setProgress(fileProgress);
                holder.playAudio.setTag(hubMessage);
                holder.playAudio.setOnClickListener(this);

                if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                {
//                        HotSpot.setMessageRead(sender_username,msgId,getActivity());
                }
                break;
            case 5: //Sent audio
                handleSending(holder,status);
                holder.fileProgress.setOnClickListener(this);
                holder.fileProgress.setTag(hubMessage);
                holder.playAudio.setTag(hubMessage);
                holder.playAudio.setOnClickListener(this);
                handleMessageStatus(holder, status, fileProgress);
                break;
            case 6:
                holder.reply.setText(reply);


                break;
            case 7:
                handleSending(holder,status);
                holder.reply.setText(reply);
                break;
            case 9: //Sent file
                File file =new File(fileUrl);
                holder.fileProgress.setOnClickListener(this);
                holder.fileProgress.setTag(hubMessage);

                holder.openFile.setTag(hubMessage);
                holder.openFile.setOnClickListener(this);
                if(file.exists())
                {  holder.tvFileName.setText(file.getName());
                    setFileSize(holder.tvFileSize,file.length());}
                handleMessageStatus(holder, status, fileProgress);
                if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                {
//                        HotSpot.setMessageRead(sender_username,msgId,getActivity());
                }
                break;
            case 8: //received file
                holder.fileProgress.setOnClickListener(this);
                holder.fileProgress.setTag(hubMessage);
                holder.openFile.setTag(hubMessage);
                holder.openFile.setOnClickListener(this);
                holder.tvFileName.setText(name);
                setFileSize(holder.tvFileSize,fileSize);

                break;









        }

    }

    public void setFileSize(TextView tvFileSize, long size)
    {
        if(size<1048576)
        {
            long kb=size/1024;
            tvFileSize.setText(kb+"KB");
        }
        else{
            long mb=size/1024/1024;
            tvFileSize.setText(mb+"MB");
        }
    }
    protected void handleMessageStatus(ViewHolder holder, String status, int fileProgress)
    {
        if(status.equals(Const.MESSAGE_SENDING))
        {
            holder.fileProgress.setProgress(fileProgress);
            Log.d("Messages", "Sending" + fileProgress);
        }
        else
            switch (status)
            {
                case Const.MESSAGE_SENT:
                    holder.fileProgress.setVisibility(View.GONE);
                    break;
                case Const.MESSAGE_FAILED:
                    holder.fileProgress.setProgress(-1);
                    break;
                case Const.MESSAGE_DELIVERED:
                    holder.fileProgress.setVisibility(View.GONE);
                    break;
                case Const.MESSAGE_READ:
                    holder.fileProgress.setVisibility(View.GONE);
                    break;

            }


    }

    @Override
    public void onClick(View v) {
        HubMessage hubMessage = (HubMessage)v.getTag();
        switch (v.getId())
        {
            case R.id.bubble_image:
                context.startActivity(new Intent(context, ImageSlider.class).putExtra(Const.PATH,hubMessage.getFile().getLocalUrl()).putExtra(Const.HUB_ID,hubMessage.getHubId()));
                //startActivity(new Intent(context, ImageViewer.class).putExtra(Const.IMAGE_VIEWER_URI,messageItem.fileLocalUrl).putExtra(Const.VIEW_THIS_IMAGE,messageItem.receiver).putExtra(Const.PRIVATE,false));
                break;
            case R.id.iv_bubble_send_audio:
                String databaseId= String.valueOf(v.getTag());

                Cursor c = context.getContentResolver().query(Uri.withAppendedPath(DataProvider.CONTENT_URI_MESSAGES, databaseId), null, null, null, null);

                if (c.moveToFirst()) {
                    AudioManager
                            .getInstance()
                            .initialise(context)
                            .playAudio(c.getString(c.getColumnIndex(DataProvider.COL_FILE_LOCAL_URL)),context.getSupportFragmentManager());
                }



                c.close();


                break;
            case R.id.ivOpenFile:
                String url=(String)v.getTag();
                File file= new File(url);
                if(file.exists())
                    new FileManager(context).openFile(file);
                else
                    manager.errorToast("File not found");
                break;
            case R.id.cpb_file_download_progress:
                String id=(String) v.getTag();
                String status=hubMessage.getDeliveryStatus();

                /**
                 * If true, it means the file is meant to be uploaded. Otherwise, download;
                 */
                boolean upload=hubMessage.getSender().equals(manager.getUsername());

                switch (status)
                {
                    case Const.MESSAGE_FAILED:
                        if(upload)
                        {


                            manager.shortToast("Attempting to resend message");
                            if(hubMessage.getType()==DataProvider.MessageType.OUTGOING_IMAGE.ordinal())
                            {
                                //TODO replace                                 new PictureUploader(context,null,null,Const.MEDIA_UPLOAD_TYPE.MESSAGE_PICTURE.ordinal(),messageItem.receiver,messageItem.msg).execute(Uri.parse(messageItem.fileLocalUrl));
                            }
                            else ; //TODO implement for audio messages
                            //TODO HouseKeeping.deleteMessage(context,messageItem.databaseId);
                        }

                        else{
                            //TODO
                            //manager.retryFileDownload(messageItem);
                        }

                        break;
                }




                break;
        }
    }

    private void handleSending(ViewHolder holder, String status)
    {
        switch (status)
        {
            case Const.MESSAGE_SENDING:
                holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_message_sending));
                break;
            case Const.MESSAGE_SENT:
                holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_message_sent));
                break;
            case Const.MESSAGE_DELIVERED:
                holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_item_message_delivered));
                break;
            case Const.MESSAGE_READ:
                holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_message_delivered));
                break;
            case Const.MESSAGE_FAILED:
                holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_item_message_fail));
        }

    }




      class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v)
        {
            super(v);
            v.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    context.getMenuInflater().inflate(R.menu.context_menu_hub_chat, menu);
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            manager.shortToast("Context click");
                        }
                    });
                }


            });


            date = (TextView) v.findViewById(R.id.date_msg);
            msg = (EmojiconTextView) v.findViewById(R.id.msg);
            sender= (TextView)v.findViewById(R.id.msg_sender);
            status=(ImageView)v.findViewById(R.id.msg_status);
            databaseId=(TextView) v.findViewById(R.id.tv_database_id);
            msgId=(TextView) v.findViewById(R.id.tv_message_id);
            reply=(EmojiconTextView) v.findViewById(R.id.msg_reply);
            image=(ImageView)v.findViewById(R.id.bubble_image);
            playAudio=(ImageButton)v.findViewById(R.id.iv_bubble_send_audio);
            openFile=(ImageButton)v.findViewById(R.id.ivOpenFile);
            tvFileSize=(TextView) v.findViewById(R.id.tvFileSize);
            tvFileName=(TextView) v.findViewById(R.id.tvFileName);
            fileProgress =(CircularProgressButton)v.findViewById(R.id.cpb_file_download_progress);
        }
        TextView date;
        EmojiconTextView msg,reply;
        TextView databaseId,msgId;
        TextView sender;
        ImageView status;
        ImageView image;
        ImageButton playAudio;
        ImageButton openFile;
        TextView tvFileSize;
        TextView tvFileName;
        CircularProgressButton fileProgress;
    }
}
