package com.jabuwings.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.jabuwings.R;
import com.jabuwings.utilities.Utils;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class FriendContextMenu extends LinearLayout {
    private static final int CONTEXT_MENU_WIDTH = Utils.dpToPx(150);

    private String username = "";

    private OnContextMenuItemClickListener onItemClickListener;

    public FriendContextMenu(Context context) {
        super(context);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.friends_context_menu, this, true);
        setBackgroundResource(R.drawable.bg_container_shadow);
        setOrientation(VERTICAL);
        setLayoutParams(new LayoutParams(CONTEXT_MENU_WIDTH, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    public void bindToItem(String username) {
        this.username = username;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        ButterKnife.inject(this);
    }

    public void dismiss() {
        ((ViewGroup) getParent()).removeView(FriendContextMenu.this);
    }



    @OnClick(R.id.btnRemove)
    public void onRemoveClick() {
        if (onItemClickListener != null) {
            onItemClickListener.onRemoveClick(username);
        }
    }



    
    @OnClick(R.id.btnViewProfilePicture)
    public void onViewProfilePictureClick() {
        if (onItemClickListener != null) {
            onItemClickListener.onViewProfilePictureClick(username);
        }
    }

    @OnClick(R.id.btnViewProfile)
    public void onViewProfileClick() {
        if (onItemClickListener != null) {
            onItemClickListener.onViewProfileClick(username);
        }
    }


    public void setOnFeedMenuItemClickListener(OnContextMenuItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnContextMenuItemClickListener {
        void onReportClick(String username);

        void onRemoveClick(String username);

        void onViewProfileClick(String username);

        void onViewProfilePictureClick(String username);
        

        void onBlockClick(String username);

    }
}