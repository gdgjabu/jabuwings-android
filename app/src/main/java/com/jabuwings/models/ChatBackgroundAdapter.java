package com.jabuwings.models;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.jabuwings.R;
import com.jabuwings.management.Manager;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.GestureImageView;
import com.rey.material.widget.Button;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ChatBackgroundAdapter extends PagerAdapter {

	private JabuWingsActivity _activity;
	private LayoutInflater inflater;

	// constructor
	public ChatBackgroundAdapter(JabuWingsActivity activity) {
		this._activity = activity;

	}

	@Override
	public int getCount() {
		return 8;
	}

	@Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        ImageView imgDisplay;

        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.background_image, container,
                false);
        Button chooseButton;
        imgDisplay = (ImageView) viewLayout.findViewById(R.id.img);
        chooseButton=(Button)viewLayout.findViewById(R.id.btChoose);
        chooseButton.setText("Choose this image");
        chooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Manager manager=new Manager(_activity);
                manager.storeChatBackground(position);
                manager.setCustomBackground(false);
                manager.shortToast("Chat background changed successfully");
                _activity.finish();

            }
        });
        Drawable drawable=null;
        int resId=0;
        switch (position)
        {
            case 0:
                //drawable =_activity.getResources().getDrawable(R.drawable.chat_background0);
                resId=R.drawable.chat_background0;
                break;
            case 1:
                //drawable =_activity.getResources().getDrawable(R.drawable.chat_background1);
                resId=R.drawable.chat_background1;
                break;
            case 2:
                //drawable =_activity.getResources().getDrawable(R.drawable.chat_background2);
                resId=R.drawable.chat_background2;
                break;
            case 3:
                //drawable =_activity.getResources().getDrawable(R.drawable.chat_background3);
                resId=R.drawable.chat_background3;
                break;
            case 4:
                //drawable =_activity.getResources().getDrawable(R.drawable.chat_background4);
                resId=R.drawable.chat_background4;
                break;
            case 5:
                //drawable =_activity.getResources().getDrawable(R.drawable.chat_background5);
                resId=R.drawable.chat_background5;
                break;
            case 6:
                //drawable =_activity.getResources().getDrawable(R.drawable.chat_background6);
                resId=R.drawable.chat_background6;
                break;
            case 7:
                //drawable =_activity.getResources().getDrawable(R.drawable.chat_background7);
                resId=R.drawable.chat_background7;
                break;
            case 8:
                resId=R.drawable.welcome_background;
                break;
            case 9:
                resId=R.drawable.welcome_background2;
                break;
        }
        ((ViewPager) container).addView(viewLayout);

        //Picasso.with(_activity).load(resId).into(imgDisplay);
        Glide.with(_activity).load(resId).into(imgDisplay);
        //imgDisplay.setImageDrawable(drawable);

        return viewLayout;
    }
	
	@Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
 
    }

}
