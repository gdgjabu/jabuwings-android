package com.jabuwings.models;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.realm.Friend;
import com.jabuwings.database.realm.Message;
import com.jabuwings.image.PictureUploader;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.FileManager;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.AudioManager;
import com.jabuwings.views.hooks.ImageSlider;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.main.OnChatItemInserted;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.jabuwings.widgets.progressbutton.CircularProgressButton;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;

import static com.jabuwings.management.Const.MESSAGE_FAILED;
import static com.jabuwings.management.Const.MESSAGE_READ;

/**
 * Created by jamesfalade on 23/08/2017.
 */

public class ChatAdapter extends RealmRecyclerViewAdapter<Message,ChatAdapter.ViewHolder> implements View.OnClickListener {

    List<Message> messages;
    JabuWingsActivity context;
    Manager manager;
    DialogWizard dialogWizard;
    Realm realm;
    String username;
    public ChatAdapter(JabuWingsActivity context, OrderedRealmCollection<Message> messages, Realm realm, Friend friend, final OnChatItemInserted onChatItemInserted)
    {
        super(messages,true);
        this.context= context;
        setHasStableIds(true);
        this.messages = messages;
        dialogWizard = new DialogWizard(context);
        manager = new Manager(context);
        this.realm = realm;
        this.username = friend.getUsername();
        registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                onChatItemInserted.itemInserted();
                
            }
        });
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout=null;
        switch (viewType)
        {
            case 0:
                layout = context.getLayoutInflater().inflate(R.layout.chat_bubble_receive_text, parent, false);
                break;
            case 1:
                layout = context.getLayoutInflater().inflate(R.layout.chat_bubble_send_text, parent, false);
                break;
            case 2:
                layout = context.getLayoutInflater().inflate(R.layout.bubble_receive_image, parent, false);
                break;
            case 3:
                layout = context.getLayoutInflater().inflate(R.layout.bubble_send_image, parent, false);
                break;
            case 4:
                layout = context.getLayoutInflater().inflate(R.layout.bubble_receive_audio, parent, false);
                break;
            case 5:
                layout = context.getLayoutInflater().inflate(R.layout.bubble_send_audio, parent, false);
                break;
            case 6:

            case 7:

            case 8:
                layout = context.getLayoutInflater().inflate(R.layout.bubble_receive_file, parent, false);
                break;
            case 9:
                layout = context.getLayoutInflater().inflate(R.layout.bubble_send_file, parent, false);
                break;
        }

        ViewHolder holder = new ViewHolder(layout);
        layout.setTag(holder);


        return holder;
    }

    
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Message message = messages.get(position);
        String sender_username = message.getSender();
        String time= new Time().parseTime(message.getMsgTime());
//            Date date = new Date(cursor.context.getString(cursor.getColumnIndex(DataProvider.COL_AT)));
        String msg=message.getMsg();
        String id = message.getId();
        String status = message.getDeliveryStatus();
        String msgId= message.getMsgId();
        com.jabuwings.database.realm.File messageFile = message.getFile();
        String fileUrl=null;
        String onlineUrl  = null;
        long fileSize=0;
        String name=null;
        int fileProgress=0;

        if(messageFile!=null)
        {
            fileUrl=messageFile.getLocalUrl();
            fileSize=messageFile.getSize();
            name=messageFile.getName();
            fileProgress=messageFile.getProgress();
            onlineUrl = messageFile.getAddress();
        }

       /*    if(System.currentTimeMillis()-date.getTime()>60000)
            {
                SQLiteDatabase db = new DatabaseManager(context).getWritableDatabase();
                db.execSQL("update messages set delivery_status= '" +
                                MESSAGE_FAILED + "' where msgId=?",
                        new Object[]{msgId});
            }
*/

        if(holder.msg!=null)
            holder.msg.setTag(holder);
        switch(message.getMsgType()) {
            case 0: //Received text
                holder.friendUsername.setText(sender_username);
                holder.id.setText(id);
                setText(holder.msg,msg);
                holder.tvTime.setText(time);

                if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                {
                    HotSpot.setMessageRead(sender_username,msgId,context);
                }
                break;
            case 1: //Sent text
                holder.id.setText(id);
                setText(holder.msg, msg);
                holder.tvTime.setText(time);
                switch (status)
                {
                    case Const.MESSAGE_SENDING:
                        holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_message_sending));
                        break;
                    case Const.MESSAGE_SENT:
                        holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_message_sent));
                        break;
                    case Const.MESSAGE_DELIVERED:
                        holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_item_message_delivered));
                        break;
                    case Const.MESSAGE_READ:
                        holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_message_delivered));
                        break;
                    case Const.MESSAGE_FAILED:
                        holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_item_message_fail));
                        break;
                }
                break;
            case 2: //Received image
                holder.id.setText(id);
                holder.fileProgress.setOnClickListener(this);
                holder.fileProgress.setTag(message);
                if(fileUrl!=null)
                    Picasso.with(context).load(Uri.fromFile(new File(fileUrl))).into(holder.receiveImage);
                else Picasso.with(context).load(onlineUrl).into(holder.receiveImage);
                manager.log("File Url "+fileUrl);
                //Glide.with(context).load(fileUrl).into(holder.receiveImage);
                //imageProcessor.loadBitmap(fileUrl, holder.receiveImage);
                holder.receiveImage.setOnClickListener(this);
                holder.receiveImage.setTag(message);

                setText(holder.msg,msg);
                holder.tvTime.setText(time);
                if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                {
                    HotSpot.setMessageRead(sender_username,msgId,context);
                }
                handleIncomingFileProgress(holder, status, fileProgress);
                break;
            case 3: //Sent image
                holder.id.setText(id);
                holder.fileProgress.setOnClickListener(this);
                holder.fileProgress.setTag(message);
                if(fileUrl!=null)
                    Picasso.with(context).load(Uri.fromFile(new File(fileUrl))).into(holder.sendImage);
                //Glide.with(context).load(fileUrl).into(holder.sendImage);
                //imageProcessor.loadBitmap(fileUrl, holder.sendImage);
                holder.sendImage.setOnClickListener(this);
                holder.sendImage.setTag(message);
                holder.tvTime.setText(time);
                setText(holder.msg,msg);
                handleMessageStatus(holder,status,fileProgress);
                break;
            case 4: //Received audio
                holder.id.setText(id);
                holder.fileProgress.setOnClickListener(this);
                holder.fileProgress.setTag(message);
                holder.fileProgress.setProgress(fileProgress);
                holder.playAudio.setTag(message);
                holder.playAudio.setOnClickListener(this);
                holder.tvTime.setText(time);
                if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                {
                    HotSpot.setMessageRead(sender_username,msgId,context);
                }
                handleIncomingFileProgress(holder, status, fileProgress);
                break;
            case 5: //Sent audio
                holder.id.setText(id);
                holder.fileProgress.setOnClickListener(this);
                holder.fileProgress.setTag(message);
                holder.tvTime.setText(time);
                holder.playAudio.setTag(id);
                holder.playAudio.setOnClickListener(this);
                handleMessageStatus(holder, status, fileProgress);
                break;
            case 8: //received file
                holder.id.setText(id);
                holder.fileProgress.setOnClickListener(this);
                holder.fileProgress.setTag(message);
                holder.tvTime.setText(time);
                holder.openFile.setTag(id);
                holder.openFile.setOnClickListener(this);
                holder.tvFileName.setText(name);
                setFileSize(holder.tvFileSize,fileSize);
                handleIncomingFileProgress(holder, status, fileProgress);
                break;
            case 9: //Sent file
                File file =new File(fileUrl);
                holder.id.setText(id);
                holder.fileProgress.setOnClickListener(this);
                holder.fileProgress.setTag(message);
                holder.tvTime.setText(time);
                holder.openFile.setTag(fileUrl);
                holder.openFile.setOnClickListener(this);
                if(file.exists())
                {  holder.tvFileName.setText(file.getName());
                    setFileSize(holder.tvFileSize,file.length());}
                handleMessageStatus(holder, status, fileProgress);
                if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                {
                    HotSpot.setMessageRead(sender_username,msgId,context);
                }
                break;

        }


        View.OnLongClickListener onLongClickListener=new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ViewHolder holder = (ViewHolder) v.getTag();
                final String id = holder.id.getText().toString();

                final Message message = realm.where(Message.class).equalTo(Const.ID,id).findFirst();

                if(message!=null)
                {
                    int type = message.getMsgType();
                    final String msg= message.getMsg();
                    final String msgId=message.getMsgId();
                    final String receiver=message.getReceiver();
                    final String status=message.getDeliveryStatus();
                    int [] r={0,2,4,6,8};
                    //int [] s ={1,3,5,7,9};
                    String copy=context.getString(R.string.contextual_action_copy_message);
                    String forward=context.getString(R.string.contextual_action_forward_message);
                    String resend=context.getString(R.string.contextual_action_resend_message);
                    final String delete=context.getString(R.string.contextual_action_delete_message);
                    String retract=context.getString(R.string.contextual_action_retract_message);

                    if(contains(type,r))
                    {
                        //Received message
                        String[] options= {copy,forward,resend,delete};
                        new MaterialDialog.Builder(context).items(options).itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                                switch (position)
                                {
                                    case 0:
                                        if(msg!=null){
                                            manager.copyToClipboard(msg);
                                            manager.shortToast(R.string.message_copied);}
                                        else manager.errorToast("Cannot copy empty message");
                                        break;
                                    case 1:
                                        break;
                                    case 3:
                                        HouseKeeping.deleteMessage(message);
                                        manager.successToast(context.getString(R.string.message_deleted));
                                        break;
                                    case 2:
                                        if(msg!=null && !TextUtils.isEmpty(msg))
                                            new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    HotSpot.sendPersonalMessage(context,manager.getUsername(),username,msg,true);
                                                }
                                            }).start();
                                        else
                                            manager.errorToast("Only applies to text messages");
                                        break;
                                }
                            }
                        }).show();
                    }
                    else{
                        //sent message
                        String[] options= {copy,forward,resend,delete,retract};

                        new MaterialDialog.Builder(context).items(options).itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                                switch (position)
                                {
                                    case 0:
                                        if(msg!=null){
                                            manager.copyToClipboard(msg);
                                            manager.shortToast(R.string.message_copied);}
                                        else manager.errorToast("Cannot copy empty message");
                                        break;
                                    case 1:
                                        break;
                                    case 3:
                                        HouseKeeping.deleteMessage(message);
                                        manager.successToast(context.getString(R.string.message_deleted));
                                        break;
                                    case 2:
                                        if(msg!=null && !TextUtils.isEmpty(msg))
                                        {
                                            new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    HotSpot.sendPersonalMessage(context,manager.getUsername(),username,msg,true);
                                                }
                                            }).start();
                                        }
                                        else
                                            manager.errorToast("Only applies to text messages");
                                        break;
                                    case 4:
                                        switch (status)
                                        {
                                            case MESSAGE_READ:
                                                manager.errorToast("You cannot retract a message that has been read.");
                                                break;
                                            case MESSAGE_FAILED:
                                                manager.shortToast("No need to retract. Message was not sent");
                                                break;
                                            default:
                                                new SweetAlertDialog(context,SweetAlertDialog.WARNING_TYPE).setTitleText(context.getString(R.string.warning)).setContentText("Retraction will delete the message from the recipient's device only if the recipient has not read the particular message").setConfirmText(context.getString(R.string.continue_)).setCancelText(context.getString(R.string.cancel)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                        sweetAlertDialog.dismiss();
                                                        HotSpot.retractPersonalMessage(context,receiver,msgId,id);
                                                    }
                                                }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                        sweetAlertDialog.dismiss();
                                                    }
                                                }).show();

                                        }



                                        break;

                                }
                            }
                        }).show();

                    }
                }

                return false;
            }
        };
        holder.itemView.setOnLongClickListener(onLongClickListener);
        if(holder.msg!=null)
            holder.msg.setOnLongClickListener(onLongClickListener);

    }

    @Override
    public void onClick(View v) {
        Message message;
        switch (v.getId())
        {
            case R.id.bubble_image:
                message= (Message)v.getTag();
                context.startActivity(new Intent(context, ImageSlider.class).putExtra(Const.PATH,message.getFile().getLocalUrl()).putExtra(Const.USERNAME,message.getReceiver()).putExtra(Const.PRIVATE,true));
                //startActivity(new Intent(mContext, ImageViewer.class).putExtra(Const.IMAGE_VIEWER_URI,messageItem.fileLocalUrl).putExtra(Const.VIEW_THIS_IMAGE,messageItem.receiver).putExtra(Const.PRIVATE,false));
                break;
            case R.id.iv_bubble_receive_image:
                message= (Message)v.getTag();
                String localUrl = message.getFile().getLocalUrl();
                String onlineUrl = message.getFile().getAddress();
                String _url = localUrl==null? onlineUrl : localUrl;

                context.startActivity(new Intent(context, ImageSlider.class).putExtra(Const.PATH,_url).putExtra(Const.USERNAME,message.getReceiver()).putExtra(Const.PRIVATE,true));
                //startActivity(new Intent(mContext, ImageViewer.class).putExtra(Const.IMAGE_VIEWER_URI,messageItem.fileLocalUrl).putExtra(Const.VIEW_THIS_IMAGE,messageItem.receiver).putExtra(Const.PRIVATE,false));
                break;
            case R.id.iv_bubble_send_audio:
                message= (Message)v.getTag();
                AudioManager
                        .getInstance()
                        .initialise(context)
                        .playAudio(message.getFile().getLocalUrl(),context.getSupportFragmentManager());

                break;
            case R.id.ivOpenFile:
                String url=(String)v.getTag();
                File file= new File(url);
                if(file.exists())
                    new FileManager(context).openFile(file);
                else
                    manager.errorToast("File not found");
                break;
            case R.id.cpb_file_download_progress:
                message= (Message)v.getTag();

                String status=message.getDeliveryStatus();

                /**
                 * If true, it means the file is meant to be uploaded. Otherwise, download;
                 */
                boolean upload=message.getSender().equals(manager.getUsername());

                switch (status)
                {
                    case Const.MESSAGE_SENDING:
                        dialogWizard.showSimpleDialog("Info","The file is being sent");
                        break;
                    case Const.MESSAGE_SENT:
                        dialogWizard.showSimpleDialog("Info","The file has been sent");
                        break;
                    case Const.MESSAGE_DELIVERED:
                        dialogWizard.showSimpleDialog("Info","The file has been delivered");
                        break;
                    case Const.MESSAGE_READ:
                        dialogWizard.showSimpleDialog("Info","The file has been read");
                        break;
                    case Const.MESSAGE_FAILED:
                        if(upload)
                        {


                            manager.shortToast("Attempting to resend message");
                            if(message.getMsgType()== DataProvider.MessageType.OUTGOING_IMAGE.ordinal())
                                new PictureUploader(context,null,null,Const.MEDIA_UPLOAD_TYPE.MESSAGE_PICTURE.ordinal(),message.getReceiver(),message.getMsg()).execute(Uri.parse(message.getFile().getLocalUrl()));
                            else ; //TODO implement for audio messages
                            HouseKeeping.deleteMessage(message);
                        }

                        else{
                            manager.retryFileDownload(message);
                        }

                        break;
                }




                break;
        }

    }
    private boolean contains(int n, int[]a)
    {
        for(int x:a)
        {
            if(x==n) return true;
        }
        return false;
    }

    private void handleIncomingFileProgress(ViewHolder holder, String status, int fileProgress)
    {
        switch (status)
        {
            case Const.MESSAGE_SENT:
                holder.fileProgress.setVisibility(View.GONE);
                break;
            case Const.MESSAGE_FAILED:
                holder.fileProgress.setProgress(-1);
                break;
            case Const.MESSAGE_DELIVERED:
                holder.fileProgress.setVisibility(View.GONE);
                break;
            case Const.MESSAGE_READ:
                holder.fileProgress.setVisibility(View.GONE);
                break;

        }

    }
    public void setFileSize(TextView tvFileSize, long size)
    {
        if(size<1048576)
        {
            long kb=size/1024;
            tvFileSize.setText(kb+"KB");
        }
        else{
            long mb=size/1024/1024;
            tvFileSize.setText(mb+"MB");
        }
    }
    private void handleMessageStatus(ViewHolder holder, String status, int fileProgress)
    {
        if(status.equals(Const.MESSAGE_SENDING))
        {
            holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_message_sending));
            holder.fileProgress.setProgress(fileProgress);
            manager.log("Messages", "Sending" + fileProgress);
        }
        else
            switch (status)
            {
                case Const.MESSAGE_SENT:
                    holder.fileProgress.setVisibility(View.GONE);
                    holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_message_sent));
                    break;
                case Const.MESSAGE_FAILED:
                    holder.fileProgress.setProgress(-1);
                    holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_item_message_fail));
                    break;
                case Const.MESSAGE_DELIVERED:
                    holder.fileProgress.setVisibility(View.GONE);
                    holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_item_message_delivered));
                    break;
                case Const.MESSAGE_READ:
                    holder.fileProgress.setVisibility(View.GONE);
                    holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_message_delivered));
                    break;

            }


    }

    private void setText(EmojiconTextView msgTextView, String msg)
    {
        if(msg!=null) msgTextView.setText(msg);
        else msgTextView.setText("");
    }
    @Override
    public int getItemViewType(int position) {
        return messages.get(position).getMsgType();
    }

     class ViewHolder extends RecyclerView.ViewHolder {

         public ViewHolder(View v)
         {
             super(v);
             msg = (EmojiconTextView) v.findViewById(R.id.msg);
             id= (TextView) v.findViewById(R.id.tv_message_id);
             tvTime= (TextView) v.findViewById(R.id.date_msg);
             status = (ImageView) v.findViewById(R.id.msg_status);
             friendUsername = (TextView)v.findViewById(R.id.friend_username);
             playAudio = (ImageButton)v.findViewById(R.id.iv_bubble_send_audio);
             openFile = (ImageButton)v.findViewById(R.id.ivOpenFile);
             tvFileSize = (TextView) v.findViewById(R.id.tvFileSize);
             tvFileName = (TextView) v.findViewById(R.id.tvFileName);
             fileProgress= (CircularProgressButton)v.findViewById(R.id.cpb_file_download_progress);
             audioLayout = v.findViewById(R.id.lv_bubble_audio);
             sendImage = (ImageView)v.findViewById(R.id.bubble_image);
             receiveImage = (ImageView)v.findViewById(R.id.iv_bubble_receive_image);
             JabuWingsActivity.setUpTextViews(context,msg,tvTime,tvFileSize,tvFileName,fileProgress);
         }
        TextView id;
        TextView tvTime;

        EmojiconTextView msg;
        TextView friendUsername;
        ImageView sendImage;
        ImageView receiveImage;
        ImageView status;
        View audioLayout;
        ImageButton playAudio;
        ImageButton openFile;
        TextView tvFileSize;
        TextView tvFileName;
        CircularProgressButton fileProgress;


    }
}
