package com.jabuwings.models;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;



import com.jabuwings.database.DataProvider;

/**
 * Created by Falade James on 12/5/2015 All Rights Reserved.
 */

//This class is not yet implemented/complete
    //IT is meant to load the database before the app opens for faster accessibility
public class FriendsDatabaseManager implements LoaderManager.LoaderCallbacks<Cursor> {
Activity context;
DatabaseLoader databaseLoader;
    public FriendsDatabaseManager(Activity activity, DatabaseLoader databaseLoader)
    {
        this.context=activity;
        this.databaseLoader=databaseLoader;
        context.getLoaderManager().initLoader(0, null, this);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader loader; loader = new CursorLoader(context,
                DataProvider.CONTENT_URI_FRIENDS,
                new String[]{DataProvider.COL_ID, DataProvider.COL_NAME, DataProvider.COL_STATUS,
                        DataProvider.COL_DEPARTMENT, DataProvider.COL_COUNT,DataProvider.COL_USERNAME,DataProvider.COL_LAST_MESSAGE
                        ,DataProvider.COL_LAST_MESSAGE_STATUS,DataProvider.COL_LAST_MESSAGE_TIME_STAMP
                },
                null,
                null,
                DataProvider.COL_ID + " DESC");

        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
    databaseLoader.loadFinished(loader,data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    databaseLoader.loadReset(null);
    }

    public interface DatabaseLoader{

        Loader<Cursor> loadFinished(Loader<Cursor> loader, Cursor data);
        Loader<Cursor> loadReset(Loader<Cursor> loader);
    }
}
