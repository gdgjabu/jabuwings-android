package com.jabuwings.models;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.realm.Friend;
import com.jabuwings.database.realm.Message;
import com.jabuwings.image.ImageProcessor;
import com.jabuwings.intelligence.lisa.ChatWithLisa;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.ImageViewer;
import com.jabuwings.views.hooks.ProfileOfFriend;
import com.jabuwings.views.main.Chat;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.CircleImage;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.OnBoomListener;

import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;


/**
 * Created by Falade James on 12/5/2015 All Rights Reserved.
 */
public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder>  {
static Activity context;
static Manager manager;
static BoomMenuButton poppedBoom;
ImageProcessor imageProcessor;
static FriendsAdapter instance;
List<Friend> friends;
Time time;
    static String viewProfile,viewPicture,removeFriend;
    public FriendsAdapter(Activity context, List<Friend> friends)
    {
        this.context=context;
        manager= new Manager(context);
        imageProcessor =new ImageProcessor(context);
        instance=this;
        viewProfile=context.getString(R.string.profile);
        viewPicture=context.getString(R.string.picture);
        removeFriend=context.getString(R.string.remove);
        time=new Time();
        this.friends=friends;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener,OnBoomListener, FriendContextMenu.OnContextMenuItemClickListener
    {
        TextView name;
        TextView username;
        TextView messageCount;
        EmojiconTextView lastMessage;
        CircleImage displayPicture;
        TextView status;
        TextView timeStamp;
        ImageView lastMessageStatus,lastMessageMedia;
        BoomMenuButton contextMenuButton;
        FriendClicks listener;



        @Override
        public void onViewProfileClick(String username) {
            closeMenu();
          context.startActivity(new Intent(context, ProfileOfFriend.class).putExtra(Const.USERNAME, username));
        }

        @Override
        public void onReportClick(String username) {
            closeMenu();
        }

        @Override
        public void onRemoveClick(final String username) {
            closeMenu();
            new MaterialDialog.Builder(context)
                    .title("Remove "+username)
                    .content("Are you sure? "+username+" will no longer be able to send you messages")
                    .positiveText("Remove "+username)
                    .negativeText(R.string.cancel)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            HotSpot.removeFriend(context, username);
                        }
                    }).show();
        }

        @Override
        public void onViewProfilePictureClick(String username) {
            closeMenu();
            context.startActivity(new Intent(context, ImageViewer.class).putExtra(Const.VIEW_THIS_IMAGE, username)
            .putExtra(Const.IMAGE_VIEWER_URI,manager.getFriendOrHubProfilePictureName(username)));
        }

        @Override
        public void onBlockClick(String username) {
            closeMenu();
        }



        private void closeMenu()
        {
            FriendsContextMenuManager.getInstance().hideContextMenu();
        }
        public ViewHolder(View view, FriendClicks listener) {
            super(view);
            this.listener=listener;
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
            name = (TextView) view.findViewById(R.id.user_item_name);
            displayPicture = (CircleImage) view.findViewById(R.id.user_item_profile_pic);
            username= (TextView)view.findViewById(R.id.user_item_username);
            messageCount=(TextView)view.findViewById(R.id.user_item_msg_count);
            status=(TextView)view.findViewById(R.id.user_item_status);
            lastMessage=(EmojiconTextView)view.findViewById(R.id.user_item_last_msg);
            lastMessageMedia=(ImageView) view.findViewById(R.id.user_item_last_media);
            timeStamp=(TextView)view.findViewById(R.id.user_item_timestamp);

            /*
             Initialise TextViews typeface
             */

            JabuWingsActivity.setUpTextViews(context,name,username,messageCount,status,lastMessage,timeStamp);
            lastMessageStatus=(ImageView)view.findViewById(R.id.user_item_last_msg_status);
            contextMenuButton=(BoomMenuButton) view.findViewById(R.id.user_item_boom);
            view.setTag(contextMenuButton);

            displayPicture.setOnClickListener(this);

            int[] drawablesResource = new int[]{
                    R.drawable.photo,
                    R.drawable.requests,
                    R.drawable.remove
            };
            final String[] circleSubButtonTexts={viewPicture,viewProfile,removeFriend};
            contextMenuButton.clearBuilders();
            for (int i = 0; i < contextMenuButton.getPiecePlaceEnum().pieceNumber(); i++)
                contextMenuButton.addBuilder(new TextInsideCircleButton.Builder().normalImageRes(drawablesResource[i]).normalText(circleSubButtonTexts[i]).imagePadding(new Rect(30,30,30,30)).typeface(new Manager(context).getTypeFace()));
            /*final Drawable[] circleSubButtonDrawables = new Drawable[3];

            for (int i = 0; i < 3; i++)
                circleSubButtonDrawables[i]
                        = ContextCompat.getDrawable(context, drawablesResource[i]);
            final int[][] subButtonColors = new int[3][2];
            for (int i = 0; i < 3; i++) {
                subButtonColors[i][1] = Utils.GetRandomColor();
                subButtonColors[i][0] = Util.getInstance().getPressedColor(subButtonColors[i][1]);
            }
            final String[] circleSubButtonTexts={viewPicture,viewProfile,removeFriend};
            final BoomMenuButton.OnSubButtonClickListener onSubButtonClickListener=this;
            contextMenuButton.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Now with Builder, you can init BMB more convenient
                    new BoomMenuButton.Builder()
                            .subButtons(circleSubButtonDrawables, subButtonColors, circleSubButtonTexts)
                            .button(ButtonType.CIRCLE)
                            .boom(BoomType.PARABOLA)
                            .place(PlaceType.CIRCLE_3_1)
                            .subButtonsShadow(Util.getInstance().dp2px(2), Util.getInstance().dp2px(2))
                            .onSubButtonClick(onSubButtonClickListener)
                            .init(contextMenuButton);
                }
            }, 1);

            contextMenuButton.setOnSubButtonClickListener(this);*/
            contextMenuButton.setOnBoomListener(this);
        }
        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                default:
                    listener.onClick(v);
            }

        }

        @Override
        public boolean onLongClick(View v) {
            listener.onLongClick(v);
            return true;
        }

        @Override
        public void onClicked(int index, BoomButton boomButton, BoomMenuButton menuButton) {
            listener.onBoomClick(index, menuButton);
        }



        @Override
        public void onBackgroundClick() {

        }

        @Override
        public void onBoomWillHide() {

        }

        @Override
        public void onBoomDidHide() {

        }

        @Override
        public void onBoomWillShow() {

        }

        @Override
        public void onBoomDidShow() {

        }


    }

    public BoomMenuButton getPoppedBoom()
    {
        return poppedBoom;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Friend friend = friends.get(position);
        String username,name,status,lastMessage=null,lastMessageStatus=null,timeStamp=null;
        int lastMessageType=0;
        String id;
        username = friend.getUsername();
        name =friend.getName();
        status=friend.getStatus();
        Message lastMsg= friend.getLastMessage();
        if(lastMsg!=null)
        {
            lastMessage= lastMsg.getMsg();
            lastMessageStatus =lastMsg.getDeliveryStatus();
            lastMessageType=lastMsg.getMsgType();
            timeStamp= lastMsg.getMsgTime();
        }

        id= friend.getUsername();
        //manager.setUpFriendPush(username);


        holder.contextMenuButton.setTag(username);


        holder.name.setText(name);
        holder.username.setText(username);
        holder.username.setTag(id);
        holder.displayPicture.setTag(username);
        holder.status.setText(status);
        if(lastMessage!=null) {
            holder.lastMessage.setText(lastMessage.replaceAll("[\\t\\n\\r]"," "));
        }
        else{
            holder.lastMessage.setText("");
        }
        if(timeStamp!=null)
            holder.timeStamp.setText(time.parseTime(timeStamp));
        else{
            holder.timeStamp.setText("");
        }
        //Glide.with(context).load()
        imageProcessor.loadBitmap(username+"_"+Const.PROFILE_PICTURE,holder.displayPicture);


/*
        if (Core.newSignIn&&profilePictureURL != null) {
            if(profilePictureFileURL==null)
            new ImageDownloader(context,holder.displayPicture,manager.getFileNameFromUrl(profilePictureURL),username).execute(profilePictureURL);
        }

        else
        if (!Core.newSignIn&&profilePictureURL!=null)
        loadFriendImage(username,profilePictureFileURL,profilePictureURL,holder);*/


        if(lastMsg!=null)
        if(lastMessageType==DataProvider.MessageType.INCOMING_VOICE_NOTE.ordinal() || lastMessageType==DataProvider.MessageType.OUTGOING_VOICE_NOTE.ordinal())
        {
            holder.lastMessageMedia.setVisibility(View.VISIBLE);
            holder.lastMessageMedia.setImageDrawable(context.getResources().getDrawable(R.drawable.emoji_1f3b5));
        }
        else
        if(lastMessageType==DataProvider.MessageType.OUTGOING_IMAGE.ordinal() || lastMessageType==DataProvider.MessageType.INCOMING_IMAGE.ordinal())
        {
            holder.lastMessageMedia.setVisibility(View.VISIBLE);
            holder.lastMessageMedia.setImageDrawable(context.getResources().getDrawable(R.drawable.emoji_1f5fb));
        }







        if(manager.arrayContains(DataProvider.SENT_MESSAGE_TYPES,lastMessageType))

            switch (lastMessageStatus)
            {
                case Const.MESSAGE_SENDING:
                    holder.lastMessageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_message_sending));
                    break;
                case Const.MESSAGE_SENT:
                    holder.lastMessageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_message_sent));
                    break;
                case Const.MESSAGE_DELIVERED:
                    holder.lastMessageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_item_message_delivered));
                    break;
                case Const.MESSAGE_READ:
                    holder.lastMessageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_message_delivered));
                    break;
                case Const.MESSAGE_FAILED:
                    holder.lastMessageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_item_message_fail));
                    break;
            }

        else{
            holder.lastMessageStatus.setImageDrawable(null);
        }
        //TODO query to find count
        /*int count = cursor.getInt(cursor.getColumnIndex(DataProvider.COL_COUNT));
        if (count > 0) {

            holder.messageCount.setVisibility(View.VISIBLE);
            holder.messageCount.setText(String.valueOf(count));
            //holder.messageCount.setText(String.format("%d new message%s", count, count == 1 ? "" : "s"));
        } else*/
            holder.messageCount.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return friends.size();
    }




    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_item, parent, false);
        ViewHolder vh = new ViewHolder(itemView, new FriendClicks() {
            @Override
            public void onClick(View v) {
                if(v.getId()==R.id.user_item_profile_pic)
                {
                    context.startActivity(new Intent(context, ImageViewer.class)
                            .putExtra(Const.VIEW_THIS_IMAGE, (String)v.getTag())
                            .putExtra(Const.IMAGE_VIEWER_URI, manager.getFriendOrHubProfilePictureName((String)v.getTag())));
                }
                else
                {TextView tvFriendUsername;
              tvFriendUsername = (TextView) v.findViewById(R.id.user_item_username);
                if(tvFriendUsername!=null) {
                    String friendUsername = tvFriendUsername.getText().toString();

                    if (friendUsername.equals(Const.LISA)) {
                        context.startActivity(new Intent(context,
                                ChatWithLisa.class).putExtra(
                                Const.FRIEND_ID, friendUsername));
                    } else {
                        context.startActivity(new Intent(context,
                                Chat.class).putExtra(
                                Const.FRIEND_ID, friendUsername));
                    }
                }}
            }


            @Override
            public void onBoomClick(int buttonIndex, BoomMenuButton boomMenuButton) {
                final String username=(String)boomMenuButton.getTag();
                switch (buttonIndex)
                {
                    case 0:
                        context.startActivity(new Intent(context, ImageViewer.class).putExtra(Const.VIEW_THIS_IMAGE, username)
                                .putExtra(Const.IMAGE_VIEWER_URI,manager.getFriendOrHubProfilePictureName(username)));
                        break;
                    case 1:
                        context.startActivity(new Intent(context, ProfileOfFriend.class).putExtra(Const.USERNAME, username));
                        break;
                    case 2:
                        new SweetAlertDialog(context,SweetAlertDialog.WARNING_TYPE).setTitleText("Remove "+username).setContentText("Are you sure? "+username+" will no longer be able to send you messages").setConfirmText("Remove "+username).setCancelText(context.getString(R.string.cancel)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                                HotSpot.removeFriend(context, username);
                            }
                        }).show();

                        break;
                }


            }

            @Override
            public void onLongClick(View v) {
                BoomMenuButton boom = (BoomMenuButton)v.getTag();
                boom.boom();

            }
        });
        return vh;
    }

    public interface FriendClicks{
        void onClick(View v);
        void onLongClick(View v);
        void onBoomClick(int buttonIndex, BoomMenuButton boomMenuButton);
    }

}
