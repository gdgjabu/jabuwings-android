package com.jabuwings.models;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.views.classroom.Classroom;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ProgressCallback;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by Falade James on 5/13/2016 All Rights Reserved.
 */
public class CourseMaterialAdapter extends BaseAdapter {
    Context context;List<ClassroomMaterial> items;
    public CourseMaterialAdapter(List<ClassroomMaterial> items, Context context) {
         this.context=context;
         this.items=items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.course_material_item, null);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.tvCourseMaterialName);
            holder.tvInfo = (TextView) convertView.findViewById(R.id.tvCourseMaterialInfo);
            holder.btDownload = (Button) convertView.findViewById(R.id.btCourseMaterialDownload);
            holder.btDownload.setTag(items.get(position).getFile());
            ClassroomMaterial material =items.get(position);
            holder.tvName.setText(material.getFile().getName());
            holder.tvInfo.setText(material.getDetails());
            holder.btDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final ParseFile parseFile=(ParseFile) v.getTag();
                    getFile(parseFile);


                }
            });
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

            return convertView;
    }




    private void getFile(final ParseFile parseFile)
    {

        Manager manager= new Manager(context);
        manager.shortToast("Getting file");
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                context);

        Intent intent = new Intent(context, Classroom.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(Classroom.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        manager.shortToast("Called not");
        final Notification notification = mBuilder.setTicker("Downloading "+parseFile.getName())
                .setContentTitle("Downloading "+parseFile.getName())
                .setContentText("Download in progress")
                .setProgress(100,0,false)
                //  .setContentIntent(resultPendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .build();


        final android.app.NotificationManager mNotificationManager =
                (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(242, notification);
        parseFile.getDataInBackground(new GetDataCallback() {
            @Override
            public void done(byte[] bytes, ParseException e) {

                if(e==null)
                {

                    try {
                        File dir = new File(Const.PUBLIC_COURSE_MATERIALS_DIRECTORY);
                        dir.mkdir();
                        File courseMaterials = new File(dir, parseFile.getName());
                        FileOutputStream outputStream = new FileOutputStream(courseMaterials);
                        outputStream.write(bytes);
                        mBuilder.setContentText("Download complete")
                                // Removes the progress bar
                                .setProgress(0, 0, false);
                        mNotificationManager.notify(242, mBuilder.build());
                    }
                    catch (IOException e1)
                    {
                        e1.printStackTrace();
                        mBuilder.setContentText("Download failed. Could not save file")
                                // Removes the progress bar
                                .setProgress(0, 0, false);
                        mNotificationManager.notify(242, mBuilder.build());
                    }


                }

                else{
                    mBuilder.setContentText("Download failed")
                            .setProgress(0,0,false);
                    mNotificationManager.notify(242, mBuilder.build());
                }
            }

        }, new ProgressCallback() {
            @Override
            public void done(Integer integer) {
                mBuilder.setProgress(100,integer,false);
                mNotificationManager.notify(242,mBuilder.build()
                );
            }
        });
    }
    public class ViewHolder{
        TextView tvName,tvInfo;
        Button btDownload;

        public ViewHolder() {

        }


    }
    public interface DownloadClick extends View.OnClickListener{

        void onClick(View v);
    }
}
