package com.jabuwings.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.jabuwings.management.Const;
import com.jabuwings.views.classroom.Classroom;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;

import java.util.List;

/**
 * Created by Falade James on 7/18/2016 All Rights Reserved.
 */
@ParseClassName("ClassroomStudents")
public class ClassroomStudent extends ParseObject {


    public void getAssignments()
    {

    }
    
    public JsonArray getPortalCourses()
    {
        return new JsonParser().parse(getString("CoursesMap")).getAsJsonArray();
    }

    public ParseRelation<ClassroomCourse> getCourses()
    {
        return getRelation(Const.COURSES);
    }

    public static ParseQuery<ClassroomStudent> getQuery(){return ParseQuery.getQuery(ClassroomStudent.class);}


}
