package com.jabuwings.models;

import com.jabuwings.management.Const;
import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.Date;

/**
 * Created by Falade James on 7/18/2016 All Rights Reserved.
 */
@ParseClassName("UserPublic")
public class JabuWingsPublicUser extends ParseObject {


    public String getName()
    {
        return getString(Const.NAME);
    }
    public String getUsername()
    {
        return getString(Const.USERNAME);
    }

    public String getDepartment()
    {
        return getString(Const.DEPARTMENT);
    }




    public String getPicture()
    {
        return getString(Const.PROFILE_PICTURE_URL);
    }





    public String getId(){return  getObjectId();}


    public int getAccountType()
    {
        return getInt(Const.ACCOUNT_TYPE);
    }


    public String isVerifiedAs(){
        return getString(Const.VERIFIED_AS);
    }

    public boolean isVerified()
    {
        return getString(Const.VERIFIED_AS)!=null;
    }



    public ParseFile getProfilePicture()
    {return getParseFile(Const.PROFILE_PICTURE);}
    public String getProfilePictureUrl()

    {
        return getString(Const.PROFILE_PICTURE_URL);
    }

    public long getProfilePictureDate(){
        Date date= getDate(Const.PROFILE_PICTURE_DATE);
        if(date == null) return 0;
        else return date.getTime();}

    public static ParseQuery<JabuWingsPublicUser> getQuery(){return ParseQuery.getQuery(JabuWingsPublicUser.class);}

}
