package com.jabuwings.models;

import android.text.TextUtils;

import com.jabuwings.management.Const;
import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.Date;

/**
 * Created by Falade James on 7/18/2016 All Rights Reserved.
 */
public class JabuWingsFirebaseUser {


    private String name,username,department,picture, verifiedAs;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public boolean hasPicture()
    {
        return !TextUtils.isEmpty(getPicture());
    }

    public String getVerifiedAs() {
        return verifiedAs;
    }

    public void setVerifiedAs(String verifiedAs) {
        this.verifiedAs = verifiedAs;
    }
}
