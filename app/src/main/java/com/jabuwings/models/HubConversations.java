package com.jabuwings.models;

import android.content.Context;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.Time;
import com.jabuwings.widgets.JabuWingsTextView;

import java.util.List;

import butterknife.InjectView;


/**
 * Created by Falade James on 10/21/2016 All Rights Reserved.
 */
public class HubConversations extends ListFragment {



    public class Adapter extends BaseAdapter{
        Context context;
        List<HubTopic> topics;
        Manager manager;
        Time time;
        public Adapter(Context context, List<HubTopic> topics)
        {
            this.context=context;
            this.topics=topics;
            time=new Time();
            manager=new Manager(context);

        }
        @Override
        public int getCount() {
            return topics.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            HubTopic topic= topics.get(position);
            String status=topic.getStatus();
            if (convertView == null) {
                if(topic.getSender().equals(manager.getUsername()))
                convertView = LayoutInflater.from(context).inflate(R.layout.bubble_hub_send_text, null);
                else{
                    convertView = LayoutInflater.from(context).inflate(R.layout.bubble_hub_receive_text, null);
                }

                holder=new ViewHolder();



            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            convertView.setTag(holder);

            holder.msg=(JabuWingsTextView)convertView.findViewById(R.id.msg);
            holder.date=(TextView)convertView.findViewById(R.id.date_msg);
            holder.sender=(TextView)convertView.findViewById(R.id.msg_sender);
            holder.status=(ImageView) convertView.findViewById(R.id.msg_status);
            holder.replies=(RecyclerView)convertView.findViewById(R.id.rvHubMsg);

            holder.sender.setText(topic.getSender().equals(manager.getUsername())? context.getString(R.string.you): topic.getSender());
            holder.msg.set(topic.getMessage());
            holder.date.setText(time.parseTime(topic.getCreatedAt()));


            switch (status)
            {
                case Const.MESSAGE_SENDING:
                    holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_message_sending));
                    break;
                case Const.MESSAGE_SENT:
                    holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_message_sent));
                    break;
                case Const.MESSAGE_DELIVERED:
                    holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_item_message_delivered));
                    break;
                case Const.MESSAGE_READ:
                    holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_message_delivered));
                    break;
                case Const.MESSAGE_FAILED:
                    holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_item_message_fail));
            }




            return convertView;



        }
    }

    public class HubReplies extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return 0;
        }
    }

    public static class ReplyViewHolder extends RecyclerView.ViewHolder{

        @InjectView(R.id.msg)
        JabuWingsTextView msg;
        @InjectView(R.id.date_msg)
        TextView date;
        @InjectView(R.id.msg_sender)
        TextView sender;

        public ReplyViewHolder(View view)
        {
            super(view);
        }
    }

    public class ViewHolder{
        JabuWingsTextView msg;
        TextView date,sender;
        ImageView status;
        RecyclerView replies;

    }
}
