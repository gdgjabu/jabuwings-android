package com.jabuwings.models;

/**
 * Created by Falade James on 2/25/2016 All Rights Reserved.
 */
@Deprecated
public class MessageItem {
    public String sender,receiver,msg,databaseId,msgId,msgTime,deliveryStatus,fileLocalUrl, fileAddress;
    int fileProgress, msgType;
    public MessageItem(String sender, String receiver, String msg, String databaseId,
                       String msgId, String msgTime, String deliveryStatus,
                       String fileLocalUrl,String fileAddress,int fileProgress, int msgType)

    {

        this.sender=sender;
        this.receiver=receiver;
        this.msg=msg;
        this.databaseId=databaseId;
        this.msgId=msgId;
        this.msgTime=msgTime;
        this.deliveryStatus=deliveryStatus;
        this.fileLocalUrl=fileLocalUrl;
        this.fileAddress =fileAddress;
        this.fileProgress=fileProgress;
        this.msgType =msgType;

    }

}
