package com.jabuwings.models;

import com.jabuwings.management.Const;
import com.parse.ParseFile;
import com.parse.ParseUser;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Falade James on 12/27/2015 All Rights Reserved.
 */
public class JabuWingsUser implements Serializable{
ParseUser user;
    public JabuWingsUser(ParseUser user)
    {
        this.user=user;
    }

    public ParseUser getParseUser(){return user;}
    public String getName()

    {
        return user.getString(Const.NAME);
    }
    public String getMatricNo()
    {
       return user.getString(Const.MATRIC_NO);
    }

    public String getDepartment()

    {
        String department =user.getString(Const.DEPARTMENT);
        return department==null? "" : department;
    }


    public String getLevel()

    {
        return user.getString(Const.LEVEL);
    }

    public String getStateOfOrigin()

    {
        return user.getString(Const.STATE);
    }

    public ParseFile getProfilePicture()
    {return user.getParseFile(Const.PROFILE_PICTURE);}
    public String getProfilePictureUrl()

    {
        return user.getString(Const.PROFILE_PICTURE_URL);
    }

    public long getProfilePictureDate(){
        Date date= user.getDate(Const.PROFILE_PICTURE_DATE);
        if(date == null) return 0;
        else return date.getTime();}
    public String getStatus()

    {
        return user.getString(Const.STATUS);
    }

    public List<String> getFriends()
    {
        return user.getList(Const.FRIENDS);
    }

    public List<String> getFriendRequests()
    {
        return user.getList(Const.REQUESTS);
    }

    public int getCommits(){return user.getInt(Const.COMMITS);}

    public String getPhoneNo()

    {
        return user.getString(Const.PHONE_NO);
    }

    public String getMedicalNo()

    {
        return user.getString(Const.MEDICAL_NO);
    }

    public String getBirthDay()

    {
        return user.getString(Const.BIRTHDAY);
    }

    public String getUsername(){return  user.getUsername();}

    public String getEmail(){return  user.getEmail();}

    public String getId(){return  user.getObjectId();}

    public Date getLastSeen()
    {
        return user.getDate(Const.LAST_SEEN);
    }

    public int getAccountType()
    {
        return user.getInt(Const.ACCOUNT_TYPE);
    }


    public String isVerifiedAs(){
        return user.getString(Const.VERIFIED_AS);
    }

    public boolean isVerified()
    {
        return user.getString(Const.VERIFIED_AS)!=null;
    }

    public boolean isPhoneVerified(){return user.getBoolean(Const.PHONE_VERIFIED);}
    public boolean isOnline(){
        Date lastSeen= getLastSeen();
        if(lastSeen!=null)
        {long difference = System.currentTimeMillis()-lastSeen.getTime();
        return difference<=180000;}
        else return false;
    }

    public boolean isFriendOf(String username)
    {
        return getFriends()!=null &&getFriends().contains(username);
    }

}
