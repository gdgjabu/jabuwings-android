package com.jabuwings.models;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.realm.HubMessage;
import com.jabuwings.database.realm.Message;
import com.jabuwings.image.PictureUploader;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.FileManager;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.AudioManager;
import com.jabuwings.views.hooks.ImageSlider;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.SwitchButton;
import com.jabuwings.widgets.progressbutton.CircularProgressButton;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;


/**
 * Created by Falade James on 8/3/2015.
 */
@Deprecated
public class HubMessages extends ListFragment {
    private OnFragmentInteractionListener mListener;
    private HubCursorAdapter adapter;
    private String TAG= Messages.class.getSimpleName();
    Time time;
    Manager manager;
    Realm realm;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager= new Manager(getActivity());
        realm = Realm.getDefaultInstance();
       

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getActivity().getMenuInflater().inflate(R.menu.context_menu_hub_chat, menu);
    }
    
    

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info= (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        View view= info.targetView;
        TextView tvMsgId,tvDatabaseId;
        tvDatabaseId = (TextView)view.findViewById(R.id.tv_database_id);
        tvMsgId=(TextView)view.findViewById(R.id.tv_message_id);
        TextView msg= (TextView)view.findViewById(R.id.msg);
        String msgText="";
        if(msg!=null)
        msgText=msg.getText().toString();
        String databaseId= tvDatabaseId.getText().toString();
        String msgId=tvMsgId.getText().toString();


        switch (item.getItemId())
        {
            case R.id.contextual_action_copy_message:
                if(msg!=null && !TextUtils.isEmpty(msgText)){
                    manager.copyToClipboard(msg.getText().toString());
                    manager.shortToast(R.string.message_copied);}
                else manager.errorToast("Cannot copy empty message");
                break;
            case R.id.contextual_action_reply_message:
                if(msg!=null && !TextUtils.isEmpty(msgText))
                {
                    replyMessage(msgText,databaseId,msgId);
                }
                break;
            case R.id.contextual_action_delete_message:
                HouseKeeping.deleteHubMessage(getActivity(), databaseId);
                manager.successToast(getString(R.string.message_deleted));
                break;
            case R.id.contextual_action_resend_message:
                if(msg!=null && !TextUtils.isEmpty(msgText))
                    HotSpot.sendHubMessage(getContext(),msg.getText().toString(),manager.getUsername(),mListener.getHubId(),DataProvider.MessageType.OUTGOING.ordinal());
                else
                    manager.errorToast("Only applies to text messages");
                break;


        }



        return super.onContextItemSelected(item);

    }

    public void replyMessage(final String msg, final String databaseId, final String msgId)
    {



        String where = DataProvider.COL_ID + "=?";
        String[] whereArgs = new String[]{databaseId};
        Cursor c = manager.getDatabaseManager().getReadableDatabase().query(true, DataProvider.TABLE_HUB_MESSAGES, null, where, whereArgs, null, null, null, null);
        String status="";
        if (c != null && c.moveToFirst()) {
            status =c.getString(c.getColumnIndex(DataProvider.COL_DELIVERY_STATUS));
            c.close();
        }


        final Dialog.Builder builder;
        builder = new SimpleDialog.Builder(R.style.SimpleDialog){
            TextView textView;
            EmojiconEditText editText;
            SwitchButton send;
            @Override
            protected void onBuildDone(final Dialog dialog) {
                textView=(TextView)dialog.findViewById(R.id.hub_chat_reply);
                editText=(EmojiconEditText)dialog.findViewById(R.id.txt);
                send=(SwitchButton)dialog.findViewById(R.id.btnSend);
                textView.setText(msg);

                send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (send.getState() == SwitchButton.FIRST_STATE) {

                        }
                        else{
                            HotSpot.sendHubMessage(getActivity(),editText.getText().toString().trim(),manager.getUsername(),mListener.getHubId(),DataProvider.MessageType.OUTGOING_REPLY.ordinal(),msgId,databaseId);
                            dialog.dismiss();
                        }
                    }
                });

                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String text = s.toString();
                        send.goToState(text.length() == 0 ? SwitchButton.FIRST_STATE : SwitchButton.SECOND_STATE);
                    }
                });

            }

            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {

            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };

        builder.title("Reply")
                .negativeAction(getString(R.string.cancel))
                .contentView(R.layout.hub_chat_reply);
        DialogFragment rateFragment = DialogFragment.newInstance(builder);

        if(status.equals(Const.MESSAGE_SENDING) || status.equals(Const.MESSAGE_FAILED))
        {
            manager.errorToast("The message has not been sent");
        }
        else
        rateFragment.show(getActivity().getSupportFragmentManager(), null);
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setDivider(null);
        getListView().setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        getListView().setStackFromBottom(true);
        FrameLayout.LayoutParams params= new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getListView().setLayoutParams(params);
        registerForContextMenu(getListView());
        Bundle args = new Bundle();
        args.putString(DataProvider.COL_HUB_ID, mListener.getHubId());

        RealmResults<HubMessage> realmResults = realm.where(HubMessage.class)
                .equalTo(Const.HUB_ID, mListener.getHubId())
                .findAllAsync();


        OrderedRealmCollectionChangeListener<RealmResults<HubMessage>> callback =  new OrderedRealmCollectionChangeListener<RealmResults<HubMessage>>() {
            @Override
            public void onChange(RealmResults<HubMessage> messages, OrderedCollectionChangeSet changeSet) {
                if(changeSet==null)
                {
                    adapter= new HubCursorAdapter(getActivity(), messages);
                    setListAdapter(adapter);
                }
                else {
                    adapter.notifyDataSetChanged();
                }
            }
        };
        realmResults.addChangeListener(callback);

        realm.addChangeListener(new RealmChangeListener<Realm>() {
            @Override
            public void onChange(Realm realm) {
                if(adapter!=null)
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        String getHubId();
    }


    private class HubCursorAdapter extends BaseAdapter implements View.OnClickListener {
        List<HubMessage> messages;
        Context context;
        public HubCursorAdapter(Context context, List<HubMessage> messages) {
            this.messages=messages;
            this.context=context;
        }

        @Override public int getCount() {
            return messages.size();
        }

        @Override
        public int getViewTypeCount() {
            return 10;
        }

        @Override
        public int getItemViewType(int _position) {
            HubMessage hubMessage = (HubMessage) messages.get(_position);
            return hubMessage.getType();
        }

        @Override
        public Object getItem(int position) {
            
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = new ViewHolder();
            View itemLayout = null;
            HubMessage message = messages.get(position);


            holder.date = (TextView) itemLayout.findViewById(R.id.date_msg);
            holder.msg = (EmojiconTextView) itemLayout.findViewById(R.id.msg);
            holder.sender= (TextView)itemLayout.findViewById(R.id.msg_sender);
            holder.status=(ImageView)itemLayout.findViewById(R.id.msg_status);
            holder.databaseId=(TextView) itemLayout.findViewById(R.id.tv_database_id);
            holder.msgId=(TextView) itemLayout.findViewById(R.id.tv_message_id);
            holder.reply=(EmojiconTextView) itemLayout.findViewById(R.id.msg_reply);
            holder.image=(ImageView)itemLayout.findViewById(R.id.bubble_image);
            holder.playAudio=(ImageButton)itemLayout.findViewById(R.id.iv_bubble_send_audio);
            holder.openFile=(ImageButton)itemLayout.findViewById(R.id.ivOpenFile);
            holder.tvFileSize=(TextView) itemLayout.findViewById(R.id.tvFileSize);
            holder.tvFileName=(TextView) itemLayout.findViewById(R.id.tvFileName);
            holder.fileProgress =(CircularProgressButton)itemLayout.findViewById(R.id.cpb_file_download_progress);
            JabuWingsActivity.setUpTextViews(context,holder.msg,holder.date,holder.tvFileSize,holder.tvFileName,holder.fileProgress);
            bindView(itemLayout, message);


            return itemLayout;
        }
        

        @SuppressWarnings("all")
        public void bindView(View view, HubMessage hubMessage) {
            String status = hubMessage.getDeliveryStatus();
            String databaseId= hubMessage.getId();
            String msgId=hubMessage.getMsgId();
            String reply= hubMessage.getReply();


            //TODO to be reconsidered
            /*if(reply!=null) {
                String where = DataProvider.COL_ID + "=?";
                String[] whereArgs = new String[]{reply};
                Cursor c = manager.getDatabaseManager().getReadableDatabase().query(true, DataProvider.TABLE_HUB_MESSAGES, null, where, whereArgs, null, null, null, null);

                if (c != null && c.moveToFirst()) {
                    reply = c.getString(cursor.getColumnIndex(DataProvider.COL_HUB_MESSAGE));
                    c.close();
                }
            }*/
            String fileUrl=null;
            int fileProgress=0;
            long fileSize=0;
            String name= null;
            if(hubMessage.getFile()!=null)
            {
                fileUrl = hubMessage.getFile().getLocalUrl();
                fileProgress = hubMessage.getFile().getProgress();
                fileSize = hubMessage.getFile().getSize();
            }

                ViewHolder holder = (ViewHolder) view.getTag();
                time = new Time();
                holder.date.setText(time.parseTime(hubMessage.getTime()));
                if(holder.msg!=null)
                holder.msg.setText(hubMessage.getMsg());
                holder.msgId.setText(msgId);
                holder.databaseId.setText(databaseId);
                if(holder.sender!=null) holder.sender.setText(hubMessage.getSender());

            switch(hubMessage.getType()) {
                case 0:
                    if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                    {


                    //    HotSpot.setMessageRead(sender_username, msgId, getActivity());


                    }



                    break;
                case 1:
                   // holder.id.setText(id);
                  //  holder.msg.setText(msg);
                   // holder.tvTime.setText(time);
                    handleSending(holder,status);
                    break;
                case 2:

                    holder.fileProgress.setOnClickListener(this);
                    holder.fileProgress.setTag(databaseId);
                    Picasso.with(context).load(Uri.fromFile(new File(fileUrl))).into(holder.image);
                    holder.image.setOnClickListener(this);
                    holder.image.setTag(databaseId);

                    if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                    {
//                        HotSpot.setMessageRead(sender_username,msgId,getActivity());
                    }
                    handleMessageStatus(holder, status, fileProgress);
                    break;
                case 3:
                    handleSending(holder,status);
                    handleMessageStatus(holder, status, fileProgress);
                    holder.fileProgress.setOnClickListener(this);
                    holder.fileProgress.setTag(databaseId);
                    Picasso.with(context).load(Uri.fromFile(new File(fileUrl))).into(holder.image);
                    holder.image.setOnClickListener(this);
                    holder.image.setTag(databaseId);
                    break;
                case 4: //Received audio
                    handleMessageStatus(holder, status, fileProgress);
                    holder.fileProgress.setOnClickListener(this);
                    holder.fileProgress.setTag(databaseId);
                    holder.fileProgress.setProgress(fileProgress);
                    holder.playAudio.setTag(databaseId);
                    holder.playAudio.setOnClickListener(this);

                    if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                    {
//                        HotSpot.setMessageRead(sender_username,msgId,getActivity());
                    }
                    break;
                case 5: //Sent audio
                    handleSending(holder,status);
                    holder.fileProgress.setOnClickListener(this);
                    holder.fileProgress.setTag(databaseId);
                    holder.playAudio.setTag(databaseId);
                    holder.playAudio.setOnClickListener(this);
                    handleMessageStatus(holder, status, fileProgress);
                    break;
                case 6:
                    holder.reply.setText(reply);


                    break;
                case 7:
                    handleSending(holder,status);
                    holder.reply.setText(reply);
                    break;
                case 9: //Sent file
                    File file =new File(fileUrl);
                    holder.fileProgress.setOnClickListener(this);
                    holder.fileProgress.setTag(databaseId);

                    holder.openFile.setTag(fileUrl);
                    holder.openFile.setOnClickListener(this);
                    if(file.exists())
                    {  holder.tvFileName.setText(file.getName());
                        setFileSize(holder.tvFileSize,file.length());}
                    handleMessageStatus(holder, status, fileProgress);
                    if (status.equals(Const.MESSAGE_FAILED)|| status.equals(Const.MESSAGE_DELIVERED))
                    {
//                        HotSpot.setMessageRead(sender_username,msgId,getActivity());
                    }
                    break;
                case 8: //received file
                    holder.fileProgress.setOnClickListener(this);
                    holder.fileProgress.setTag(databaseId);
                    holder.openFile.setTag(databaseId);
                    holder.openFile.setOnClickListener(this);
                    holder.tvFileName.setText(name);
                    setFileSize(holder.tvFileSize,fileSize);

                    break;









            }
        }
        public void setFileSize(TextView tvFileSize, long size)
        {
            if(size<1048576)
            {
                long kb=size/1024;
                tvFileSize.setText(kb+"KB");
            }
            else{
                long mb=size/1024/1024;
                tvFileSize.setText(mb+"MB");
            }
        }
        protected void handleMessageStatus( ViewHolder holder, String status,int fileProgress)
        {
            if(status.equals(Const.MESSAGE_SENDING))
            {
                holder.fileProgress.setProgress(fileProgress);
                Log.d("Messages", "Sending" + fileProgress);
            }
            else
                switch (status)
                {
                    case Const.MESSAGE_SENT:
                        holder.fileProgress.setVisibility(View.GONE);
                        break;
                    case Const.MESSAGE_FAILED:
                        holder.fileProgress.setProgress(-1);
                        break;
                    case Const.MESSAGE_DELIVERED:
                        holder.fileProgress.setVisibility(View.GONE);
                        break;
                    case Const.MESSAGE_READ:
                        holder.fileProgress.setVisibility(View.GONE);
                        break;

                }


        }

        @Override
        public void onClick(View v) {
            MessageItem messageItem;
            switch (v.getId())
            {
                case R.id.bubble_image:
                    messageItem=manager.getHubMessageItem(String.valueOf(v.getTag()));
                    startActivity(new Intent(context, ImageSlider.class).putExtra(Const.PATH,messageItem.fileLocalUrl).putExtra(Const.HUB_ID,messageItem.receiver));
                    //startActivity(new Intent(context, ImageViewer.class).putExtra(Const.IMAGE_VIEWER_URI,messageItem.fileLocalUrl).putExtra(Const.VIEW_THIS_IMAGE,messageItem.receiver).putExtra(Const.PRIVATE,false));
                    break;
                case R.id.iv_bubble_send_audio:
                    String databaseId= String.valueOf(v.getTag());

                    Cursor c = context.getContentResolver().query(Uri.withAppendedPath(DataProvider.CONTENT_URI_MESSAGES, databaseId), null, null, null, null);

                    if (c.moveToFirst()) {
                        AudioManager
                                .getInstance()
                                .initialise(context)
                                .playAudio(c.getString(c.getColumnIndex(DataProvider.COL_FILE_LOCAL_URL)),getFragmentManager());
                    }



                    c.close();


                    break;
                case R.id.ivOpenFile:
                    String url=(String)v.getTag();
                    File file= new File(url);
                    if(file.exists())
                        new FileManager(context).openFile(file);
                    else
                        manager.errorToast("File not found");
                    break;
                case R.id.cpb_file_download_progress:
                    String id=(String) v.getTag();
                    messageItem=manager.getFriendMessageItem(id);
                    String status=messageItem.deliveryStatus;

                    /**
                     * If true, it means the file is meant to be uploaded. Otherwise, download;
                     */
                    boolean upload=messageItem.sender.equals(manager.getUsername());

                    switch (status)
                    {
                        case Const.MESSAGE_FAILED:
                            if(upload)
                            {


                                manager.shortToast("Attempting to resend message");
                                if(messageItem.msgType==DataProvider.MessageType.OUTGOING_IMAGE.ordinal())
                                    new PictureUploader(getContext(),null,null,Const.MEDIA_UPLOAD_TYPE.MESSAGE_PICTURE.ordinal(),messageItem.receiver,messageItem.msg).execute(Uri.parse(messageItem.fileLocalUrl));
                                else ; //TODO implement for audio messages
                                HouseKeeping.deleteMessage(context,messageItem.databaseId);
                            }

                            else{
//                                manager.retryFileDownload(messageItem);
                            }

                            break;
                    }




                    break;
            }
        }

        private void handleSending(ViewHolder holder, String status)
        {
            switch (status)
            {
                case Const.MESSAGE_SENDING:
                    holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_message_sending));
                    break;
                case Const.MESSAGE_SENT:
                    holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_message_sent));
                    break;
                case Const.MESSAGE_DELIVERED:
                    holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_item_message_delivered));
                    break;
                case Const.MESSAGE_READ:
                    holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_message_delivered));
                    break;
                case Const.MESSAGE_FAILED:
                    holder.status.setImageDrawable(getResources().getDrawable(R.drawable.ic_item_message_fail));
            }

        }


    }

    private static class ViewHolder {
        TextView date;
        EmojiconTextView msg,reply;
        TextView databaseId,msgId;
        TextView sender;
        ImageView status;
        ImageView image;
        ImageButton playAudio;
        ImageButton openFile;
        TextView tvFileSize;
        TextView tvFileName;
        CircularProgressButton fileProgress;
    }
}
