package com.jabuwings.models;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListItem;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.utilities.Utils;
import com.jabuwings.views.hooks.ImageViewer;
import com.jabuwings.views.hooks.ProfileOfFriend;
import com.jabuwings.views.main.Chat;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.main.MyProfile;
import com.jabuwings.widgets.CircleImage;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.SimpleCircleButton;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.Util;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Falade James on 10/15/2015 All Rights Reserved.
 */
public class HubMembersRecycler extends RecyclerView.Adapter<HubMembersRecycler.ViewHolder> {
    List<HubMember> hubMembers;
    Activity context;
    Manager manager;
    String hub_id;
    boolean one;
    JabuWingsHub jabuWingsHub;
    public HubMembersRecycler(List<HubMember> items,String hub_id,Activity context,JabuWingsHub jabuWingsHub) {
        this.hubMembers = items;
        this.context=context;
        manager= new Manager(context);
        this.hub_id=hub_id;
        this.jabuWingsHub=jabuWingsHub;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hub_member_item, parent, false);


        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final HubMember member= hubMembers.get(position);
        String username =member.username;
        if(!username.equals(manager.getUsername())) {
            holder.username.setText(member.username);
        }
        else holder.username.setText(R.string.you);
        holder.department.setText(member.department);
        holder.status.setText(member.status);
        holder.boom.setTag(member.username+"&"+position);
        holder.profilePicture.setTag(member.user);
        holder.profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JabuWingsUser user=(JabuWingsUser)v.getTag();
                Intent intent= new Intent(context,ImageViewer.class);
                intent.putExtra(Const.PRIVATE,false);
                intent.putExtra(Const.ONLINE,true);
                intent.putExtra(Const.VIEW_THIS_IMAGE,user.getName());
                intent.putExtra(Const.IMAGE_VIEWER_URI,user.getProfilePictureUrl());
                context.startActivity(intent);
            }
        });


        if(member.profilePictureUrl!=null)
        Picasso.with(context)
                .load(member.profilePictureUrl)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .placeholder(R.drawable.default_avatar)
                .into(holder.profilePicture, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        Picasso.with(context)
                                .load(member.profilePictureUrl)
                                .transform(new CircleTransform())
                                .into(holder.profilePicture);
                    }
                });


    }

    @Override
    public int getItemCount() {
        return hubMembers.size();

    }


    public class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener, OnBoomListener{

        TextView username,department,status;
        CircleImage profilePicture;
        BoomMenuButton boom;


        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            username = (TextView) itemView.findViewById(R.id.hub_member_username);
            department = (TextView) itemView.findViewById(R.id.hub_member_department);
            status = (TextView) itemView.findViewById(R.id.hub_member_status);

            profilePicture=(CircleImage)itemView.findViewById(R.id.hub_member_profile_picture);
            profilePicture.setOnClickListener(this);
            boom=(BoomMenuButton) itemView.findViewById(R.id.hub_member_menu);
            int[] drawablesResource = new int[]{
                    R.drawable.photo,
                    R.drawable.requests,
                    R.drawable.ic_more
            };
            String  viewProfile=context.getString(R.string.profile),viewPicture=context.getString(R.string.picture),removeFriend=context.getString(R.string.more);
            final String[] circleSubButtonTexts={viewPicture,viewProfile,removeFriend};

            boom.clearBuilders();
            for (int i = 0; i < boom.getPiecePlaceEnum().pieceNumber(); i++)
                boom.addBuilder(new TextOutsideCircleButton.Builder().normalImageRes(drawablesResource[i]).normalText(circleSubButtonTexts[i]).imagePadding(new Rect(30,30,30,30)).typeface(new Manager(context).getTypeFace()));
            /*final Drawable[] circleSubButtonDrawables = new Drawable[3];

            for (int i = 0; i < 3; i++)
                circleSubButtonDrawables[i]
                        = ContextCompat.getDrawable(context, drawablesResource[i]);
            final int[][] subButtonColors = new int[3][2];
            for (int i = 0; i < 3; i++) {
                subButtonColors[i][1] = Utils.GetRandomColor();
                subButtonColors[i][0] = Util.getInstance().getPressedColor(subButtonColors[i][1]);
            }

            final BoomMenuButton.OnSubButtonClickListener onSubButtonClickListener=this;
            boom.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Now with Builder, you can init BMB more convenient
                    new BoomMenuButton.Builder()
                            .subButtons(circleSubButtonDrawables, subButtonColors, circleSubButtonTexts)
                            .button(ButtonType.CIRCLE)
                            .boom(BoomType.PARABOLA)
                            .place(PlaceType.CIRCLE_3_1)
                            .subButtonsShadow(Util.getInstance().dp2px(2), Util.getInstance().dp2px(2))
                            .onSubButtonClick(onSubButtonClickListener)
                            .init(boom);
                }
            }, 1);*/
            boom.setOnBoomListener(this);

        }

        @Override
        public void onClicked(int index, BoomButton boomButton,BoomMenuButton menuButton) {
            String[] data =((String)menuButton.getTag()).split("&");
            final String username =data[0];
            final int position=Integer.valueOf(data[1]);
            HubMember hubMember= hubMembers.get(position);
            switch (index)
            {
                case 0:
                    Intent intent= new Intent(context,ImageViewer.class);
                    intent.putExtra(Const.PRIVATE,false);
                    intent.putExtra(Const.ONLINE,true);
                    intent.putExtra(Const.VIEW_THIS_IMAGE,hubMember.name);
                    intent.putExtra(Const.IMAGE_VIEWER_URI,hubMember.profilePictureUrl);
                    context.startActivity(intent);
                    break;
                case 1:

                    context.startActivity(new Intent(context,ProfileOfFriend.class).putExtra(Const.ONLINE,true).putExtra(Const.FRIEND,hubMember.user));
                    break;
                case 2:
                    boolean admin=jabuWingsHub.adminContains(manager.getUsername());
                    final boolean userAdmin=jabuWingsHub.adminContains(username);
                    List<String> options = new ArrayList<>();


                    if(admin)
                    {
                        if(!userAdmin)
                            options.add("Make hub admin");
                        options.add("Remove");

                        new MaterialDialog.Builder(context).items(options).itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                                switch (position)
                                {
                                    case 0:
                                        if(!userAdmin)
                                        {
                                            HotSpot.makeAdmin(context,jabuWingsHub.getId(),username);
                                        }
                                        else{
                                            if(!username.equals(manager.getUsername()))
                                                removeFromHub(username);
                                            else{
                                                removeSelfFromHub();
                                            }
                                        }
                                        break;
                                    case 1:
                                        if(!username.equals(manager.getUsername()))
                                            removeFromHub(username);
                                        else{
                                            removeSelfFromHub();
                                        }
                                        break;
                                }
                            }
                        }).show();
                    }
                    else{
                        manager.shortToast("Options only available for admins");
                    }

                    break;
            }
        }

        @Override
        public void onBackgroundClick() {

        }

        @Override
        public void onBoomWillHide() {

        }

        @Override
        public void onBoomDidHide() {

        }

        @Override
        public void onBoomWillShow() {

        }

        @Override
        public void onBoomDidShow() {

        }



        @Override
        public void onClick(final View v) {
            switch (v.getId())
            {
                case R.id.hub_member_menu:
                    /*
                    String[] data =((String)v.getTag()).split("&");
                    final String username =data[0];
                    final int position=Integer.valueOf(data[1]);

                    if(!username.equals(manager.getUsername())){
                        String[] options;
                        one=true;
                        if(!manager.getUserFriends().contains(v.getTag())) {
                            options={"Send Friend Request"};
                            listAdapter.add(new MaterialSimpleListItem.Builder(context)
                                    .content("Send Friend Request")
                                    .build());
                            one=false;
                        }



                        listAdapter.add(new MaterialSimpleListItem.Builder(context)
                                .content("View hub gist")
                                .build());

                        if(manager.isUserAdminOfHub(hub_id))
                            listAdapter.add(new MaterialSimpleListItem.Builder(context)
                                    .content("Dispatch from hub")
                                    .build());

                        String[] options = {"Share File"};
                        new MaterialDialog.Builder(context)
                                .title(R.string.share_others)
                                .items(options)
                                .itemsCallback(new MaterialDialog.ListCallback() {
                                    @Override
                                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                                    }
                                })
                                .show();

                        new MaterialDialog.Builder(context).adapter(listAdapter, new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                dialog.dismiss();
                                switch (which)
                                {
                                    case 0:
                                        if(!one) {
                                            HotSpot.sendFriendRequest(context, username, "Hub");
                                            manager.shortToast("Attempting to send request to @"+username);
                                        }
                                        else {
                                            viewGist(hubMembers.get(position));
                                        }

                                        break;
                                    case 1:
                                        if(!one) viewGist(hubMembers.get(position));
                                        else
                                        if(manager.isUserAdminOfHub(hub_id))
                                        {
                                            removeFromHub(username);
                                        }
                                        break;
                                }
                            }
                        })
                                .show();


                    }

                    else{
                        MaterialSimpleListAdapter listAdapter= new MaterialSimpleListAdapter(context);

                        listAdapter.add(new MaterialSimpleListItem.Builder(context)
                                .content("View hub gist")
                                .build());

                        if(manager.isUserAdminOfHub(hub_id))
                            listAdapter.add(new MaterialSimpleListItem.Builder(context)
                                    .content("Dispatch self from hub")
                                    .build());

                        new MaterialDialog.Builder(context).adapter(listAdapter, new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {

                                dialog.dismiss();
                                switch (which)
                                {
                                    case 0: viewGist(hubMembers.get(position)); break;
                                    case 1: removeSelfFromHub(); break;
                                }

                            }
                        }).show();
                    }
*/
                    break;
                default:
                    final String friendUsername=((TextView)itemView.findViewById(R.id.hub_member_username)).getText().toString();
                    if(!friendUsername.equals("You"))
                    if(manager.getDatabaseManager().getFriends().contains(friendUsername) && !friendUsername.equals(manager.getUsername()))
                    context.startActivity(new Intent(context, Chat.class).putExtra(Const.FRIEND_ID,friendUsername));
                    else{

                        new SweetAlertDialog(context,SweetAlertDialog.NORMAL_TYPE).setTitleText("Add Friend").setContentText(friendUsername +" is not your friend. Will you like to send a friend request?").setConfirmText(context.getString(R.string.ok)).setCancelText(context.getString(R.string.cancel)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                HotSpot.sendFriendRequest(context, friendUsername, "Hub");
                                manager.shortToast("Attempting to send request to @"+friendUsername);
                            }
                        }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();

                            }
                        }).show();
                    }
                    else  new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            int[] startingLocation = new int[2];
                            v.getLocationOnScreen(startingLocation);
                            startingLocation[0] += v.getWidth() / 2;
                            MyProfile.startUserProfileFromLocation(startingLocation,context);

                        }
                    }, 200);

                    break;
            }

        }

        public  void viewGist(HubMember member)
        {
            String lastSentence; String username =member.username;
            if (member.verified)
            {
                if(jabuWingsHub.adminContains(member.id))
                    lastSentence=username+" is an admin of this hub and is verified";
                else
                    lastSentence=username+ " is a member of this hub and is verified";
            }

            else{
                if(jabuWingsHub.adminContains(member.id))
                    lastSentence=username+" is an admin of this hub";
                else{
                    lastSentence=username+" is a member of this hub";
                }
            }
            String content="Name: \t "+member.name+"\n" +
                    "Username: \t "+member.username+"\n" +
                    "Department: \t "+member.department+"\n" +
                    "Status: \t"+member.status+"\n\n" +lastSentence;


            new MaterialDialog.Builder(context).title(member.username)
                    .content(content).show();
        }

        private void removeFromHub(final String username)
        {

            if(jabuWingsHub.adminContains(manager.getUsername()))
            new MaterialDialog.Builder(context).title("Remove "+username).content("If you remove "+username+","+username+" will be disconnected from the hub. Please note that unless you block the user, the user may still be able to join at a later time").positiveText("Block "+username).negativeText(R.string.cancel).onPositive(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    manager.shortToast(username+" will be removed soon");
                    HashMap<String,Object> params= new HashMap<>();
                    params.put(Const.USERNAME,username);
                    params.put(Const.HUB_ID,hub_id);
                    params.put(Const.VERSION,context.getString(R.string.version_number));

                    ParseCloud.callFunctionInBackground(Const.HUB_MEMBER_REMOVER, params, new FunctionCallback<String>() {
                        @Override
                        public void done(String s, ParseException e) {

                            if(e==null)
                            {
                                if(s.equals(Const.POSITIVE)) manager.shortToast(username+" was successfully removed");
                                else{
                                    manager.shortToast(s);
                                }
                            }

                            else{
                                ErrorHandler.handleError(context,e);
                            }
                        }
                    })      ;
                }
            }).show();
            else{
                //TODO correct
            manager.errorToast("You lack the priviledges to perform this action");
        }


        }

        private void removeSelfFromHub()
        {
            if(jabuWingsHub.getCreatedBy().equals(manager.getUsername()))
            {
                new DialogWizard(context).showSimpleDialog("Remove self","You are the director of this hub. You have to transfer this hub to another person before you can leave the  hub. View the authentication code and give it to another member to authenticate as the new director of the hub.");
            }
            else{

                    new MaterialDialog.Builder(context).title("Remove self").content("Removing yourself will disconnect you from all the activities of this hub").positiveText(R.string.proceed).neutralText(R.string.cancel).onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                            HashMap<String,Object> params= new HashMap<>();
                            params.put(Const.VERSION,context.getString(R.string.version_number));
                            params.put(Const.HUB_ID,hub_id);


                            final SweetAlertDialog loading=new DialogWizard(context).showSimpleProgress("Please wait","Removing you from this hub");
                            ParseCloud.callFunctionInBackground(Const.HUB_MEMBER_REMOVER, params, new FunctionCallback<String>() {
                                @Override
                                public void done(String s, ParseException e) {
                                    loading.dismiss();
                                    if(e==null)
                                    {
                                        if(s.equals(Const.POSITIVE))
                                        HouseKeeping.deleteHub(context,hub_id);
                                        else{
                                            new DialogWizard(context).showSimpleDialog("",s);
                                        }
                                    }

                                    else{
                                        ErrorHandler.handleError(context,e);
                                    }
                                }
                            });


                        }
                    }).show();

            }

        }

    }





}
