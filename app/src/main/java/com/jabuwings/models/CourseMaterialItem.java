package com.jabuwings.models;

import com.parse.ParseFile;

public class CourseMaterialItem {


            private   String item;
            private String value;
            private ParseFile parseFile;

            public CourseMaterialItem(String item, String value, ParseFile parseFile)
            {
                this.item=item;
                this.value=value;
                this.parseFile=parseFile;
            }

            public String getName()
            {
                return this.item;
            }
            public String getInfo()
            {
                return this.value;
            }

            public ParseFile getFile()
            {
                return this.parseFile;
            }






        }