package com.jabuwings.models;

import com.jabuwings.management.Const;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Falade James on 9/21/2016 All Rights Reserved.
 */
@ParseClassName("ClassroomAssignments")
public class ClassroomAssignment extends ParseObject{

    public String getContent(){return getString(Const.CONTENT);}
    public ParseRelation<ParseObject> getSubmitted(){return getRelation(Const.SUBMITTED);}
    public int getNoOfSubmitted(){return getInt("noOfSubmitted");}
    public String getDate(){return SimpleDateFormat.getDateInstance().format(getCreatedAt());}
    public Date submittedBefore(){return getDate(Const.BEFORE);}
    public String submittedBeforeText(){return SimpleDateFormat.getDateInstance().format(submittedBefore());}

    public ParseObject getCourse(){return getParseObject(Const.COURSE);}

    public int getObtainable(){return getInt("marks");}

    public static ParseQuery<ClassroomAssignment> getQuery(){ return ParseQuery.getQuery(ClassroomAssignment.class);}
}

