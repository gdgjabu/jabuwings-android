package com.jabuwings.models;

import com.jabuwings.management.Const;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;

import java.util.List;

/**
 * Created by Falade James on 7/18/2016 All Rights Reserved.
 */
@ParseClassName("ClassroomCourses")
public class ClassroomCourse extends ParseObject {



    public String getCourseCode()
    {
        return getString(Const.COURSE_CODE);
    }

    public String getCourseTitle(){return getString(Const.COURSE_TITLE);}

    public int getCourseUnits(){return getInt(Const.COURSE_UNIT);}

    public String getLecturerName()
    {
        return getString("lecturer_name");
    }


    public static ParseQuery<ClassroomCourse> getQuery(){return ParseQuery.getQuery(ClassroomCourse.class);}

    public ParseRelation<ClassroomStudent> getStudents()
    {
        return getRelation("students");
    }
    public List<String> _getStudents(){
        return getList("Students");
    }
}
