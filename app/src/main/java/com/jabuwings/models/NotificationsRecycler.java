package com.jabuwings.models;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.hooks.NotificationDetails;
import com.parse.ParseObject;

import java.util.List;

/**
 * Created by Falade James on 10/17/2015 All Rights Reserved.
 */
public class NotificationsRecycler extends RecyclerView.Adapter<NotificationsRecycler.ViewHolder> {
List<ParseObject> items;
Context context;


    public NotificationsRecycler(List<ParseObject> items, Context context)
    {
        this.context=context;
        this.items=items;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notifications_list_item, parent, false);

        return new ViewHolder(v, new NotificationClick() {
            @Override
            public void onClick(final View v) {

                ParseObject object=(ParseObject) v.findViewById(R.id.n_title).getTag();
                context.startActivity(new Intent(context, NotificationDetails.class).putExtra(Const.TITLE,object.getString(Const.TITLE)).putExtra(Const.CONTENT,object.getString(Const.CONTENT)));


            }
        });
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ParseObject item =items.get(position);

        holder.tvTitle.setText(item.getString(Const.TITLE));
        holder.tvMessage.setText(item.getString(Const.CONTENT));
        holder.tvTimeStamp.setText(new Time().parseTime(item.getUpdatedAt()));
        holder.tvTitle.setTag(item);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTitle;
        TextView tvMessage;
        TextView tvTimeStamp;
        NotificationClick listener;

        ViewHolder(View itemView, NotificationClick listener) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.listener = listener;
            tvTitle = (TextView) itemView.findViewById(R.id.n_title);
            tvMessage = (TextView) itemView.findViewById(R.id.n_msg);
            tvTimeStamp = (TextView) itemView.findViewById(R.id.n_timestamp);
        }

        @Override
        public void onClick(final View v) {
            listener.onClick(v);


        }
    }

    public interface NotificationClick {
        void onClick(View v);
    }
}
