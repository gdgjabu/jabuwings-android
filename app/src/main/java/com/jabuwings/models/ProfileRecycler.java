package com.jabuwings.models;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.main.MyProfile;

import java.util.List;

/**
 * Created by Falade James on 10/15/2015 All Rights Reserved.
 */
public class ProfileRecycler extends RecyclerView.Adapter<ProfileRecycler.ViewHolder> {
    List<Item> items;
    Context context;
    public ProfileRecycler(List<Item> items, Context context) {
        this.items = items;
        this.context=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.profile_card_item, parent, false);


        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemName.setText(items.get(position).item);
        holder.itemValue.setText(items.get(position).value);


    }

    @Override
    public int getItemCount() {
        return items.size();

    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemName;
        TextView itemValue;


        ViewHolder(View itemView) {
            super(itemView);
            itemName = (TextView) itemView.findViewById(R.id.tvItem_title);
            itemValue = (TextView) itemView.findViewById(R.id.tvItem_value);
            JabuWingsActivity.setUpTextViews(context,itemName,itemValue);

        }
    }

}
