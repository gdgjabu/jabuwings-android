package com.jabuwings.models;

import com.jabuwings.management.Const;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by Falade James on 7/18/2016 All Rights Reserved.
 */
@ParseClassName("ClassroomLecturers")
public class ClassroomLecturer extends ParseObject {



    public ParseRelation<ClassroomCourse> getCourses()
    {
        return getRelation(Const.COURSES);
    }


    public static ParseQuery<ClassroomLecturer> getQuery(){return ParseQuery.getQuery(ClassroomLecturer.class);}

}
