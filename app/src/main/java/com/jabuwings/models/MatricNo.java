package com.jabuwings.models;

/**
 * Created by Falade James on 11/17/2015 All Rights Reserved.
 */
public class MatricNo implements CharSequence {

    int length;
    String matricNo;
    public  MatricNo(String matricNo)
    {
        length=matricNo.length();
        this.matricNo=matricNo;
    }


    @Override
    public int length() {
        return length;
    }

    @Override
    public char charAt(int index) {
        return matricNo.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return null;
    }


    @Override
    public String toString() {
        return matricNo;
    }
}
