package com.jabuwings.models;

import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.feed.FeedItem;
import com.jabuwings.feed.LoadingFeedItemView;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.Time;
import com.jabuwings.views.main.Feed;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.widgets.JabuWingsTextView;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.OnBoomListener;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;


/**
 * 
 */
public class FeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String ACTION_LIKE_BUTTON_CLICKED = "action_like_button_button";
    public static final String ACTION_LIKE_IMAGE_CLICKED = "action_like_image_button";
    public static final int VIEW_TYPE_DEFAULT = 1;
    public static final int VIEW_TYPE_NO_IMAGE = 2;
    public static final int VIEW_TYPE_LOADER = 3;

    private final List<FeedItem> feedItems = new ArrayList<>();

    public Context context;
    private OnFeedItemClickListener onFeedItemClickListener;

    private boolean showLoadingView = false;
    Manager manager;
    static BoomMenuButton poppedBoom;
    private Feed feed;

    public FeedAdapter(Context context, Feed feed) {
        this.context = context;
        manager= new Manager(context);
        this.feed=feed;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_DEFAULT || viewType==VIEW_TYPE_NO_IMAGE) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_feed, parent, false);
            final CellFeedViewHolder cellFeedViewHolder = new CellFeedViewHolder(view, new OnFeedItemClickListener() {
                @Override
                public void onCommentsClick(View v, CellFeedViewHolder viewHolder) {

                }

                @Override
                public void onLikesClick(CellFeedViewHolder viewHolder) {
                    onFeedItemClickListener.onLikesClick(viewHolder);
                }


                @Override
                public void onMoreClick(View v, int position) {
                    onFeedItemClickListener.onMoreClick(v,position);
                }

                @Override
                public void onProfileClick(View v) {

                }

                @Override
                public void onReadMoreClick(CellFeedViewHolder viewHolder) {
                    onFeedItemClickListener.onReadMoreClick(viewHolder);
                }

            });
            setupClickableViews(view, cellFeedViewHolder);
            return cellFeedViewHolder;
        } else if (viewType == VIEW_TYPE_LOADER) {
            LoadingFeedItemView view = new LoadingFeedItemView(context);
            view.setLayoutParams(new LinearLayoutCompat.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)
            );
            return new LoadingCellFeedViewHolder(view);
        }

        return null;
    }

    private void setupClickableViews(final View view, final CellFeedViewHolder cellFeedViewHolder) {
        cellFeedViewHolder.btnComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFeedItemClickListener.onCommentsClick(view, cellFeedViewHolder);
            }
        });
        cellFeedViewHolder.tvFeedComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFeedItemClickListener.onCommentsClick(view,cellFeedViewHolder);
            }
        });
        cellFeedViewHolder.tsLikesCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFeedItemClickListener.onLikesClick(cellFeedViewHolder);
            }
        });
        cellFeedViewHolder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFeedItemClickListener.onMoreClick(v, cellFeedViewHolder.getAdapterPosition());
            }
        });
        cellFeedViewHolder.ivFeedCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int adapterPosition = cellFeedViewHolder.getAdapterPosition();
                FeedItem item =feedItems.get(adapterPosition);
                if(!item.isLiked())
                {
                    item.like(context);
                    item.likesCount++;

//                    animatePhotoLike(cellFeedViewHolder);
//                    animateHeartButton(cellFeedViewHolder);
//                    updateLikesCounter(cellFeedViewHolder, cellFeedViewHolder.getFeedItem().likesCount);

                    notifyItemChanged(adapterPosition, ACTION_LIKE_IMAGE_CLICKED);
                }


            }
        });

        cellFeedViewHolder.btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int adapterPosition = cellFeedViewHolder.getAdapterPosition();
                FeedItem feedItem = feedItems.get(adapterPosition);
                if(!feedItem.isLiked())
                {
                    feedItem.like(context);
                    feedItem.likesCount++;

//                    animateHeartButton(cellFeedViewHolder);
//                    updateLikesCounter(cellFeedViewHolder, cellFeedViewHolder.getFeedItem().likesCount);

                    notifyItemChanged(adapterPosition, ACTION_LIKE_BUTTON_CLICKED);
                }


            }
        });
        cellFeedViewHolder.ivUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFeedItemClickListener.onProfileClick(view);
            }
        });
        cellFeedViewHolder.tvFeedReadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFeedItemClickListener.onReadMoreClick(cellFeedViewHolder);
            }
        });
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        FeedItem item= feedItems.get(position);

        //refreshFeedItem(feedItems.get(position));

        if(getItemViewType(position)==VIEW_TYPE_DEFAULT)
        {
            ((CellFeedViewHolder) viewHolder).bindView(item);
        }

        if(getItemViewType(position)==VIEW_TYPE_NO_IMAGE)
        {
            ((CellFeedViewHolder) viewHolder).bindNoImageView(item);
        }


        if (getItemViewType(position) == VIEW_TYPE_LOADER) {
            bindLoadingFeedItem((LoadingCellFeedViewHolder) viewHolder);
        }
    }

    public void refreshFeedItem(FeedItem feedItem)
    {
        feedItem.fetchIfNeededInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(e==null)
                {
                    parseObject.pinInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null)
                            {
                                notifyDataSetChanged();

                            }
                        }
                    });
                }
            }
        });


    }

    private void bindLoadingFeedItem(final LoadingCellFeedViewHolder holder) {
        holder.loadingFeedItemView.setOnLoadingFinishedListener(new LoadingFeedItemView.OnLoadingFinishedListener() {
            @Override
            public void onLoadingFinished() {
                showLoadingView = false;
                notifyItemChanged(0);
            }
        });
        //holder.loadingFeedItemView.startLoading();
    }

    @Override
    public int getItemViewType(int position) {
        FeedItem item =feedItems.get(position);
        if(item.isPicture())
        {
            return VIEW_TYPE_DEFAULT;
        }
        else{
            return VIEW_TYPE_NO_IMAGE;
        }


    }

    @Override
    public int getItemCount() {
        return feedItems.size();
    }


    public BoomMenuButton getPoppedBoom()
    {
        return poppedBoom;
    }


    public void updateItems(final boolean fetchNew) {

        manager.log("Updated items "+fetchNew);
        final ParseQuery<FeedItem> feedQuery= FeedItem.getQuery();

        feedQuery.whereEqualTo(Const.Feed.VALID,true).orderByDescending(Const.CREATED_AT).fromLocalDatastore().findInBackground(new FindCallback<FeedItem>() {
            @Override
            public void done(List<FeedItem> list, ParseException e) {
                if(e==null && list!=null && list.size()>0)
                {
                    feed.setLonely(false);
                    if(feedItems.size()!=list.size()) {
                        feedItems.clear();
                        feedItems.addAll(list);
                        
                        notifyItemRangeInserted(0, feedItems.size());
                        feed.updateFeed();

                    }
                }
                else{
                    feed.setLonely(true);
                }


            }
        });

    }
    private ParseQuery<FeedItem> getFeedQuery()
    {
        return FeedItem.getQuery()
                .orderByDescending(Const.CREATED_AT)
                .setLimit(15)
                .include("UserPublic")
                .whereEqualTo("valid",true);
    }

    
    private boolean updatingFromLast = false;
    public void updateFromLast()
    {
        if(!updatingFromLast)
        {
            updatingFromLast=true;
            Date lastCreated = feedItems.get(0).getCreatedAt();

            getFeedQuery().whereLessThan(Const.CREATED_AT, lastCreated).findInBackground(new FindCallback<FeedItem>() {
                @Override
                public void done(List<FeedItem> objects, ParseException e) {
                    if(e==null && !objects.isEmpty())
                    {
                        ParseObject.pinAllInBackground(objects, new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                updateItems(false);
                            }
                        });
                    }
                }
            });
        }
        
        
        
        
    }


    public void setOnFeedItemClickListener(OnFeedItemClickListener onFeedItemClickListener) {
        this.onFeedItemClickListener = onFeedItemClickListener;
    }

    public void showLoadingView() {
        showLoadingView = true;
        notifyItemChanged(0);
    }

    public static class LoadingViewHolder extends RecyclerView.ViewHolder{
        public LoadingViewHolder(View view)
        {
            super(view);
        }
    }
    public class CellFeedViewHolder extends RecyclerView.ViewHolder implements OnBoomListener {
        @InjectView(R.id.ivFeedCenter)
        ImageView ivFeedCenter;
        @InjectView(R.id.tvFeedContent)
        JabuWingsTextView tvFeedContent;
        @InjectView(R.id.tvFeedReadMore)
        TextView tvFeedReadMore;
        @InjectView(R.id.tvFeedUsername)
        TextView tvFeedUsername;
        @InjectView(R.id.tvFeedDate)
        TextView tvFeedDate;
        @InjectView(R.id.btnComments)
        ImageButton btnComments;
        @InjectView(R.id.btnLike)
        public ImageButton btnLike;
        @InjectView(R.id.btnMore)
        BoomMenuButton btnMore;
        @InjectView(R.id.vBgLike)
        public View vBgLike;
        @InjectView(R.id.ivLike)
        public ImageView ivLike;
        @InjectView(R.id.tsLikesCounter)
        public TextSwitcher tsLikesCounter;
        @InjectView(R.id.tvFeedComments)
        public TextView tvFeedComments;
        @InjectView(R.id.ivUserProfile)
        ImageView ivUserProfile;
        @InjectView(R.id.vImageRoot)
        FrameLayout vImageRoot;
        OnFeedItemClickListener listener;
        FeedItem feedItem;

        public CellFeedViewHolder(View view,OnFeedItemClickListener listener) {
            super(view);
            ButterKnife.inject(this, view);
            this.listener=listener;
            final String[] circleSubButtonTexts={"Share","Copy","Report"};
            int[] drawablesResource = new int[]{
                    R.drawable.share,
                    R.drawable.copy,
                    R.drawable.report_problem
            };
            btnMore.clearBuilders();
            for (int i = 0; i < btnMore.getPiecePlaceEnum().pieceNumber(); i++)
                btnMore.addBuilder(new TextInsideCircleButton.Builder().normalImageRes(drawablesResource[i]).normalText(circleSubButtonTexts[i]).imagePadding(new Rect(30,30,30,30)).typeface(new Manager(context).getTypeFace()));





            /*for (int i = 0; i < 3; i++) {
                subButtonColors[i][1] = Utils.GetRandomColor();
                subButtonColors[i][0] = Util.getInstance().getPressedColor(subButtonColors[i][1]);
            }
            final String[] circleSubButtonTexts={"Share","Copy","Report"};
            final BoomMenuButton.OnSubButtonClickListener onSubButtonClickListener=this;
            btnMore.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Now with Builder, you can init BMB more convenient
                    new BoomMenuButton.Builder()
                            .subButtons(circleSubButtonDrawables, subButtonColors, circleSubButtonTexts)
                            .button(ButtonType.CIRCLE)
                            .boom(BoomType.PARABOLA)
                            .place(PlaceType.CIRCLE_3_1)
                            .subButtonsShadow(Util.getInstance().dp2px(2), Util.getInstance().dp2px(2))
                            .onSubButtonClick(onSubButtonClickListener)
                            .init(btnMore);
                }
            }, 1);

            btnMore.setOnSubButtonClickListener(this);*/
            btnMore.setOnBoomListener(this);

        }


        @Override
        public void onClicked(int index, BoomButton boomButton,BoomMenuButton menuButton) {
            listener.onMoreClick(menuButton,index);
        }

        @Override
        public void onBackgroundClick() {

        }

        @Override
        public void onBoomWillHide() {

        }

        @Override
        public void onBoomDidHide() {

        }

        @Override
        public void onBoomWillShow() {

        }

        @Override
        public void onBoomDidShow() {

        }


        public void bindNoImageView(final FeedItem feedItem)
        {
            this.feedItem=feedItem;
            btnMore.setTag(feedItem);
            vImageRoot.setVisibility(View.GONE);
            int adapterPosition = getAdapterPosition();adapterPosition = getAdapterPosition();
            tvFeedUsername.setText(feedItem.getName());
            tvFeedDate.setText(new Time().parseTime(feedItem.getDate()));

            tvFeedContent.set(feedItem.getContent());
            tvFeedContent.setMaxLines(20);

            Layout l = tvFeedContent.getLayout();
            if(l!=null)
            {
                int lines = l.getLineCount();
                if(lines>20)
                {
                    tvFeedReadMore.setVisibility(View.VISIBLE);
                }
                else{
                    tvFeedReadMore.setVisibility(View.GONE);
                }

            }

            bindGeneral(feedItem);


        }
        private void bindGeneral(final FeedItem feedItem)
        {
            JabuWingsActivity.setUpTextViews(context,tvFeedComments,tvFeedContent,tvFeedUsername,tvFeedDate);
            if(feedItem.isLiked())
            {
                btnLike.setImageResource(R.drawable.ic_heart_red);
            }
            btnLike.setImageResource(feedItem.isLiked() ? R.drawable.ic_heart_red : R.drawable.ic_heart_outline_grey);
            tsLikesCounter.setCurrentText(vImageRoot.getResources().getQuantityString(
                    R.plurals.likes_count, feedItem.getLikesCount(), feedItem.getLikesCount()
            ));

            tvFeedComments.setText(vImageRoot.getResources().getQuantityString(
                    R.plurals.comments_count, feedItem.getCommentsCount(), feedItem.getCommentsCount()
            ));

            if(feedItem.getAuthorUrl()!=null)
            {
                Picasso.with(context)
                        .load(feedItem.getAuthorUrl())
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .transform(new CircleTransform())
                        .into(ivUserProfile, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Picasso.with(context)
                                        .load(feedItem.getAuthorUrl())
                                        .transform(new CircleTransform())
                                        .into(ivUserProfile);
                            }
                        });
            }
        }
        public void bindView(final FeedItem feedItem) {
            this.feedItem=feedItem;
            btnMore.setTag(feedItem);
            int adapterPosition = getAdapterPosition();adapterPosition = getAdapterPosition();
            tvFeedUsername.setText(feedItem.getName());
            tvFeedDate.setText(new Time().parseTime(feedItem.getDate()));


            if(feedItem.getContent()!=null)
            tvFeedContent.set(feedItem.getContent());
            Layout l = tvFeedContent.getLayout();
            if(l!=null)
            {
                int lines = l.getLineCount();
                if(lines>10)
                {
                    tvFeedReadMore.setVisibility(View.VISIBLE);
                }
                else{
                    tvFeedReadMore.setVisibility(View.GONE);
                }

          /*      if(lines>0)
                {
                    if(l.getEllipsisCount(lines-1)>0)
                    {
                        tvFeedReadMore.setVisibility(View.VISIBLE);

                    }
                }*/
            }



            if(feedItem.getImage()!=null)
            {
                //TODO remove getLocalUrl in production


                Picasso.with(context)
                        .load(feedItem.getImageUrl())
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .into(ivFeedCenter, new Callback() {
                            @Override
                            public void onSuccess() {
                                //new Manager(context).shortToast("Picture load success");
                            }

                            @Override
                            public void onError() {
                                //new Manager(context).shortToast("Picture load error");
                                //Try again online if cache failed
                                Picasso.with(context)
                                        .load(feedItem.getImageUrl())
                                        .error(R.drawable.default_avatar)
                                        .into(ivFeedCenter, new Callback() {
                                            @Override
                                            public void onSuccess() {
                                            }

                                            @Override
                                            public void onError() {

                                            }
                                        });
                            }
                        });
            }

            else{
               vImageRoot.setVisibility(View.GONE);
            }
            bindGeneral(feedItem);


        }



        public FeedItem getFeedItem() {
            return feedItem;
        }
    }

     private class LoadingCellFeedViewHolder extends FeedAdapter.CellFeedViewHolder {

        LoadingFeedItemView loadingFeedItemView;

         LoadingCellFeedViewHolder(LoadingFeedItemView view) {
            super(view,null);
            this.loadingFeedItemView = view;
        }

        @Override
        public void bindView(FeedItem feedItem) {
            super.bindView(feedItem);
        }
    }



    public interface OnFeedItemClickListener {
        void onCommentsClick(View v, FeedAdapter.CellFeedViewHolder viewHolder);

        void onMoreClick(View v, int position);

        void onProfileClick(View v);

        void onReadMoreClick(FeedAdapter.CellFeedViewHolder viewHolder);

        void onLikesClick(FeedAdapter.CellFeedViewHolder viewHolder);
    }

}
