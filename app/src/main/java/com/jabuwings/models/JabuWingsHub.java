package com.jabuwings.models;

import com.jabuwings.management.Const;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by Falade James on 12/27/2015 All Rights Reserved.
 */
public class JabuWingsHub {
ParseObject hub;

List<ParseUser> members; List<ParseObject> _members;
    public JabuWingsHub(ParseObject hub)
    {
        this.hub = hub;
        getLocalData();
    }

    private void getLocalData()
    {
        try{
        _members=hub.getRelation(Const._MEMBERS).getQuery().fromLocalDatastore().find();}
        catch (ParseException e)
        {e.printStackTrace();}
    }




    public String getTitle()

    {
        return hub.getString(Const.HUB_TITLE);
    }

    public String getPin() {return hub.getString(Const.PIN);}
    public String getId()

    {
        return hub.getObjectId();
    }

    public ParseRelation<ParseUser> getAdmins()
    {
        return hub.getRelation(Const._ADMIN);
    }

    public List<String> getLocalAdmins()
    {
        return hub.getList(Const.ADMIN);
    }
    public ParseRelation<JabuWingsPublicUser> getMembers()
    {
        return hub.getRelation(Const.HUB_MEMBERS);
    }
    public List<ParseObject> getLocalMembers()
    {
        return _members;
    }

    public boolean adminContains(String id)
    {
        return getLocalAdmins()!=null && getLocalAdmins().contains(id);
    }


    public String getDescription()

    {
        return hub.getString(Const.HUB_DESC);
    }

    public String getAuthenticationNo()

    {
        return hub.getString(Const.HUB_AUTHENTICATION_NO);
    }

    public String getProfilePictureUrl()

    {
        return hub.getString(Const.PROFILE_PICTURE_URL);
    }

    public long getProfilePictureDate()

    {
        return hub.getLong(Const.PROFILE_PICTURE_DATE);
    }
    public ParseFile getProfilePicture()
    {return hub.getParseFile(Const.PROFILE_PICTURE);}

    public String getStatus() {return hub.getString(Const.STATUS);}

    public String getCreatedBy()

    {
        return hub.getString(Const.BY);
    }

    public ParseObject getHub(){return hub;}

    public String isVerifiedAs(){
        return hub.getString(Const.VERIFIED_AS);
    }

    public boolean isVerified()
    {
        return hub.getString(Const.VERIFIED_AS)!=null;
    }

    public static ParseQuery<ParseObject> getQuery(){return ParseQuery.getQuery(Const.HUB_CLASS_NAME);}



}

