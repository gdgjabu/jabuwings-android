package com.jabuwings.download;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.jabuwings.database.DataProvider;
import com.jabuwings.image.ImageProcessor;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.jabuwings.views.main.Domicile;
import com.jabuwings.widgets.CircleImage;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Falade James on 11/6/2015 All Rights Reserved.
 */
public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
private String LOG_TAG=ImageDownloader.class.getSimpleName();
CircleImage view;
String fileName;
Context context;
String username;
String url;
Manager manager;
    public ImageDownloader(Context context,CircleImage view, String fileName,String username) {
        this.view=view;
        this.fileName=fileName;
        this.context=context;
        this.username=username;
        manager= new Manager(context);
        Log.d(LOG_TAG,"Username "+username+" was initialised");
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        try {
            String[] urls = params;
            url=urls[0];
            URL Url = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) Url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap=BitmapFactory.decodeStream(input);
            storeFile(bitmap, fileName);
            return bitmap;
        } catch (OutOfMemoryError|IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {

        if(fileName.equals(Const.PROFILE_PICTURE_NAME))
        {
            manager.setProfilePictureDate(manager.getUserProfilePictureDate());
            Domicile.refreshPicture();
            return;
        }
        if(bitmap!=null) {
            String key=new Manager(context).getFriendOrHubProfilePictureName(username);
            ImageProcessor imageProcessor= new ImageProcessor(context);
            imageProcessor.addBitmapToMemoryCache(key,bitmap);
            view.setImageBitmap(imageProcessor.getBitmapFromMemCache(key));
            context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_FRIENDS,null);
            Log.d(LOG_TAG, "Download finished and bitmap obtained for " + username);
        }

        else {
            Log.d(LOG_TAG,"Download failed with a null bitmap for "+username);
        }
    }

    public static Bitmap openFile(Context context,String fileName) {
        try {
            FileInputStream imageInput;
            imageInput = context.openFileInput(fileName);
            return BitmapFactory.decodeStream(imageInput);


        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void storeFile(Bitmap bitmap, String Filename) {
        try {
            FileOutputStream pp =context.openFileOutput(Filename, Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, pp);
        } catch (IOException | OutOfMemoryError e) {
            e.printStackTrace();
        }
    }
}
