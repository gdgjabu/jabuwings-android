package com.jabuwings.download;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;

import com.jabuwings.database.DatabaseManager;
import com.jabuwings.management.Const;
import com.jabuwings.models.FeedAdapter;
import com.jabuwings.views.main.Feed;

/**
 * Created by Falade James on 11/5/2015 All Rights Reserved.
 */
@Deprecated
public class ResultManager extends ResultReceiver {
    static Handler handler1;
    DatabaseManager databaseManager;
    public ResultManager(Handler handler,Context context) {
        super(handler); handler1=handler;
        databaseManager = new DatabaseManager(context);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);
        if (resultCode == DownloadService.UPDATE_PROGRESS) {
            int progress = resultData.getInt("progress");
            int downloadType=resultData.getInt(Const.DOWNLOAD_TYPE);

            switch (downloadType)
            {
                case 0:
                    String messageId=resultData.getString(Const.MSG_ID);
                    databaseManager.updateFriendFileProgress(messageId,progress);
                    break;



            }
            if (progress == 100) {
 //               mProgressDialog.dismiss();
            }
        }

        if(resultCode==333)
        {
//            Feed.update();
        }
    }

    //A required field to implement Parcelable
    public static final Creator<ResultReceiver> CREATOR
            = new Creator<ResultReceiver>() {
        public ResultReceiver createFromParcel(Parcel in) {
            return new ResultReceiver(handler1);
        }
        public ResultReceiver[] newArray(int size) {
            return new ResultReceiver[size];
        }
    };
}
