package com.jabuwings.download;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;
import android.widget.Toast;

import com.jabuwings.intelligence.lisa.Lisa;
import com.jabuwings.management.Const;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Falade James on 11/5/2015 All Rights Reserved.
 */
public class DownloadService extends IntentService {
 String fileName;
private String type;
Context context;
    public static final int UPDATE_PROGRESS = 8344;
    public static enum DownloadType{ImageFile,AudioFile,BrowserFile};
    public DownloadService() {
        super("DownloadService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context=getApplicationContext();
        Toast.makeText(context, "Download service created", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // TODO: Include multi threads for downloading several files at at a time
        Toast.makeText(context, "Download started", Toast.LENGTH_SHORT).show();
        String urlToDownload = intent.getStringExtra("url");
        fileName=intent.getStringExtra("filename");
        int downloadType=intent.getIntExtra(Const.DOWNLOAD_TYPE, 0);
        String databaseId=intent.getStringExtra(Const.MSG_ID);

        ResultReceiver receiver =  intent.getParcelableExtra("receiver");
        try {
            URL url = new URL(urlToDownload);
            URLConnection connection = url.openConnection();
            connection.connect();
            // this will be useful so that you can show a typical 0-100% progress bar
            int fileLength = connection.getContentLength();

            File sd = Environment.getExternalStorageDirectory();
            // download the file
            InputStream input = new BufferedInputStream(connection.getInputStream());
            File dir;
            File file;

            switch (downloadType)
            {
                case 0:
                    dir = new File(sd.getAbsolutePath()+"/JabuWings/Images");
                    file = new File(dir, Lisa.generateImageFileName(fileName)+".jpg");
                    break;
                case 1:
                    dir = new File(sd.getAbsolutePath()+"/JabuWings/Audio");
                    file = new File(dir, Lisa.generateImageFileName(fileName)+".amr");
                    break;
                case 2:
                    dir = new File(sd.getAbsolutePath()+"/JabuWings/Downloads");
                    file = new File(dir, Lisa.generateImageFileName(fileName));
                    break;
                default:
                    dir = new File(sd.getAbsolutePath()+"/JabuWings/Downloads");
                    file = new File(dir, Lisa.generateImageFileName(fileName));
            }
            //OutputStream output = new FileOutputStream(sd.getAbsolutePath()+"/JabuWings/Downloads/"+fileName);
            OutputStream output= new FileOutputStream(file);

            byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                Bundle resultData = new Bundle();
                resultData.putInt("progress" ,(int) (total * 100 / fileLength));
                resultData.putInt(Const.DOWNLOAD_TYPE,downloadType);
                resultData.putString(Const.ID,databaseId);
                receiver.send(UPDATE_PROGRESS, resultData);
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

        Bundle resultData = new Bundle();
        resultData.putInt("progress", 100);
        receiver.send(UPDATE_PROGRESS, resultData);
        Toast.makeText(context, "Download complete", Toast.LENGTH_SHORT).show();
    }


    public void downloadFromPHP(String url)
    {
        /*
        byte[] buffer = new byte[1024];


        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse execute = client.execute(httpGet);
            InputStream content = execute.getEntity().getContent();

            filesize = execute.getEntity().getContentLength();
            fileOutput = new FileOutputStream(new File(FILE_PATH, "file_copyformserver.apk"));

            while ((len = content.read(buffer, 0, 1024)) > 0) {
                fileOutput.write(buffer, 0, len);
                Thread.sleep(100);
            }

            fileOutput.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
        */
    }

}