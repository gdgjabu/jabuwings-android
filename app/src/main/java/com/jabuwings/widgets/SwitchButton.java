package com.jabuwings.widgets;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageButton;

import com.jabuwings.R;


public class SwitchButton extends ImageButton {
    private static final Interpolator interpolator = new AccelerateDecelerateInterpolator();

    public static final int FIRST_STATE = 1;
    public static final int SECOND_STATE = 2;

    private Drawable firstDrawable;
    private Drawable secondDrawable;

    private int state = FIRST_STATE;
    private int total = 100;
    private int duration = 200;
    private boolean init = false;

    public SwitchButton(Context context) {
        super(context);
    }

    public SwitchButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public SwitchButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.SwitchButton, 0, 0);

        int first = array.getResourceId(R.styleable.SwitchButton_first, -1);
        int second = array.getResourceId(R.styleable.SwitchButton_second, -1);
        duration = array.getInteger(R.styleable.SwitchButton_duration, duration);

        if (array != null) {
            array.recycle();
        }

        if (first > 0 && second > 0) {
            init = true;

            Resources resources = context.getResources();
            firstDrawable = resources.getDrawable(first);
            secondDrawable = resources.getDrawable(second);

            setImageDrawable(firstDrawable);
        }
    }

    public void goToState(int state) {
        if (!init || this.state == state) return;

        switch (state) {
            case FIRST_STATE:
                setImageDrawable(firstDrawable);
                //animate(firstDrawable, secondDrawable);
                break;
            case SECOND_STATE:
                setImageDrawable(secondDrawable);
                //animate(secondDrawable, firstDrawable);
                break;
        }

        this.state = state;
    }

    public int getState()
    {
        return this.state;
    }

    private Drawable makeInsetDrawable(Drawable drawable, int inset) {
        return new InsetDrawable(drawable, inset, inset, inset, inset);
    }

    private void animate(final Drawable from, final Drawable to) {
        ValueAnimator animator = ValueAnimator.ofInt(0, total);
        animator.setDuration(duration);
        animator.setInterpolator(interpolator);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                int left = total - value;

                Drawable firstInset = makeInsetDrawable(from, left);
                Drawable secondInset = makeInsetDrawable(to, value);

                LayerDrawable layer = new LayerDrawable(new Drawable[]{firstInset, secondInset});
                setImageDrawable(layer);
            }
        });

        animator.start();
    }
}
