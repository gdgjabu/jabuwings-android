package com.jabuwings.widgets.progressbutton;

interface OnAnimationEndListener {

    public void onAnimationEnd();
}
