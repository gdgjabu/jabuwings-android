package com.jabuwings.widgets.video;

/**
 * @author Aidan Follestad (afollestad)
 */
public interface EasyVideoProgressCallback {

    void onVideoProgressUpdate(int position, int duration);
}
