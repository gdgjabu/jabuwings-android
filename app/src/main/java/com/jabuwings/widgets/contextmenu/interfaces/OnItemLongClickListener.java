package com.jabuwings.widgets.contextmenu.interfaces;

import android.view.View;

/**
 * Menu adapter item long click listener
 */
public interface OnItemLongClickListener {

    public void onLongClick(View v);
}
