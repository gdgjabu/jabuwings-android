package com.jabuwings.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

import com.jabuwings.R;

/**
 * Created by Falade James on 2/26/2016 All Rights Reserved.
 */
public class DigitalCircle extends View {


    int circleColor,labelColor,labelSize;
    CharSequence label;
    Paint textPaint,circlePaint;
    RectF rectF;
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    public DigitalCircle(Context context,AttributeSet attrs)
    {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.DigitalCircle,
                0, 0);

        try{
        label= a.getString(R.styleable.DigitalCircle_label);
        circleColor=a.getColor(R.styleable.DigitalCircle_circle_color, 0xFFE3EA1E);
        labelColor=a.getColor(R.styleable.DigitalCircle_label_color,0xffffff);
        labelSize=a.getInt(R.styleable.DigitalCircle_label_size,100);
            
        }
        finally {
            a.recycle();
        }
        init();


    }
    private void init() {
        textPaint = new Paint(); circlePaint= new Paint();

        textPaint.setColor(labelColor);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(labelSize);

        circlePaint.setStyle(Paint.Style.FILL);
        circlePaint.setAntiAlias(true);
        circlePaint.setColor(circleColor);
        rectF = new RectF(0,10,0,10);
    }

    public void setLabel(CharSequence label)
    {
        this.label=label;
        invalidate();
        requestLayout();
    }

    /**
     * This changes the current label and sets the color according to the corresponding color
     * @param cgpa The cgpa to change to
     */
    public void setCGPAColor(float cgpa)
    {
        this.label=String.valueOf(cgpa);
        if(cgpa>=4.5)
        {
            setCircleColor(Color.CYAN);
            return;
        }

        if(cgpa>=3.5)
        {
            setCircleColor(Color.BLUE);
            return;
        }
        if(cgpa>=2.5)
        {
            setCircleColor(Color.MAGENTA);
            return;
        }
        if(cgpa>=1.5)
        {
            setCircleColor(Color.LTGRAY);
            return;
        }
        if(cgpa<1.5)
        {
            setCircleColor(Color.RED);
        }


    }

    private void updateView()
    {
        invalidate();
        requestLayout();
    }
    public void setLabelColor(int color)
    {
        this.labelColor=color;
        updateView();
    }

    public void setCircleColor(int color)
    {
        this.circleColor=color;
        circlePaint.setColor(circleColor);
        invalidate();
        requestLayout();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int viewWidthHalf = this.getMeasuredWidth()/2;
        int viewHeightHalf = this.getMeasuredHeight()/2;
        int radius;
        if(viewWidthHalf>viewHeightHalf)
            radius=viewHeightHalf-10;
        else
            radius=viewWidthHalf-10;

        canvas.drawCircle(viewWidthHalf, viewHeightHalf, radius, circlePaint);


        canvas.drawText(label.toString(), viewWidthHalf, viewHeightHalf, textPaint);
    }
}
