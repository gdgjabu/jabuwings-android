package com.jabuwings.widgets.contactpicker.interfaces;


import com.jabuwings.widgets.contactpicker.Contact;

/**
 * Created by Carlos Reyna on 21/01/17.
 */

public interface ContactSelectionListener {
    void onContactSelected(Contact contact, String communication);
    void onContactDeselected(Contact contact, String communication);
}
