package com.jabuwings.widgets.spacenavigation;

import com.nightonke.boommenu.BoomMenuButton;

/**
 * Created by jamesfalade on 24/09/2017.
 */

public interface OnBoomReady {
    void onBoomReady(BoomMenuButton boomMenuButton);
}
