package com.jabuwings.error;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.views.hooks.Splash;
import com.jabuwings.views.main.JabuWingsActivity;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.SaveCallback;


import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class CrashReporter extends JabuWingsActivity {

    @InjectView(R.id.tvCrashReport)
    TextView tvCrashReport;
    @InjectView(R.id.btCrashReport)
    Button btCrashReport;
    String error;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crash_reporter);
        setUpToolbar(null,"Crash Reporter");
        ButterKnife.inject(this);
        error = getIntent().getStringExtra("error");
        Log.e("Error",error);
        tvCrashReport.setText(error);
    }

    @OnClick(R.id.btCrashReport)
    public void onClick()
    {
        final ParseFile file = new ParseFile(error.getBytes());
        file.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e==null)
                {
                    ParseObject object = new ParseObject("CrashReports");
                    object.put(Const.FILE,file);
                    object.put("trace",error);
                    object.saveEventually();
                }
            }
        });
        manager.shortToast("Thanks for submitting the error.");
        startActivity(new Intent(this, Splash.class));
        finish();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                startActivity(new Intent(this, Splash.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
