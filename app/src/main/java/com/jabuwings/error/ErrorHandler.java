package com.jabuwings.error;

import android.content.Context;
import android.util.Log;

import com.jabuwings.R;
import com.jabuwings.management.Const;
import com.jabuwings.management.Manager;
import com.parse.ParseException;

/**
 * Created by Falade James on 12/24/2015 All Rights Reserved.
 */
public class ErrorHandler {
    transient Context context;
    static Manager manager;

    public ErrorHandler(Context context) {
        this.context = context;
    }


    public static void handleError(Context context, ParseException e) {
        if(context!=null)
        {
            manager = new Manager(context);
            switch (e.getCode()) {
                case ParseException.INVALID_SESSION_TOKEN:
                    handleInvalidSessionToken();
                    break;
                case ParseException.OTHER_CAUSE:
                    handlerOtherCause(e);
                    break;
                case ParseException.INVALID_EMAIL_ADDRESS:
                    handleInvalidEmail();
                    break;
                case ParseException.CONNECTION_FAILED:
                    handleConnectionFailed();
                    break;
                case ParseException.OBJECT_NOT_FOUND:
                    handleObjectNotFound();
                    break;
                case ParseException.SCRIPT_ERROR:
                    handleScriptError(e);
                    break;
                case ParseException.OPERATION_FORBIDDEN:
                    handleOperationForbidden();
                    break;



                default:
                    Log.d("ErrorHandler", "Couldn't explicitly handle error code " + e.getCode());
                    handleGeneralCause(e);


            }
        }

    }

    private static void handleInvalidEmail(){manager.errorToast("Invalid email");}
    private static void handleInvalidSessionToken() {
       manager.outrightLogOut();
    }


    private static void handlerOtherCause(ParseException e) {
        e.printStackTrace();
        manager.errorToast(manager.getContext().getString(R.string.error_fatal));
    }

    private static void handleGeneralCause(ParseException e) {
        manager.errorToast(e.getMessage());
    }

    private static void handleObjectNotFound() {
        manager.errorToast("No results were found");
    }


    private static void handleConnectionFailed() {
        manager.errorToast(manager.getContext().getString(R.string.error_network));
    }

    private static void handleScriptError(ParseException e)
    {
        e.printStackTrace();
        String msg=e.getMessage();
        if(msg.equals(Const.NEGATIVE))
        manager.errorToast(manager.getContext().getString(R.string.error_internal));
        else manager.errorToast(msg);
    }

    private static void handleOperationForbidden()
    {
        manager.errorToast(manager.getContext().getString(R.string.access_denied));
    }
}