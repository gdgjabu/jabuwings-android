package com.jabuwings.error;

/**
 * Created by Falade James on 9/21/2016 All Rights Reserved.
 */
import java.io.PrintWriter;
import java.io.StringWriter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.jabuwings.management.Manager;

public class ExceptionHandler implements java.lang.Thread.UncaughtExceptionHandler {
        private  Context myContext;
        private  Activity activity;
        private final String LINE_SEPARATOR = "\n";

         public ExceptionHandler(Context context) {
          myContext = context;
         }

        public ExceptionHandler(Activity context) {
         activity = context;
         myContext=activity;
     }

         public void uncaughtException(Thread thread, Throwable exception) {
          exception.printStackTrace();
          StringWriter stackTrace = new StringWriter();
          exception.printStackTrace(new PrintWriter(stackTrace));
          StringBuilder errorReport = new StringBuilder();
          errorReport.append("************ CAUSE OF ERROR ************\n\n");
          errorReport.append(stackTrace.toString());
          Manager manager= new Manager(myContext);

          errorReport.append("************ JabuWings INFORMATION ************\n\n");
          errorReport.append("VERSION: ");
          errorReport.append(manager.getVersion());
          errorReport.append(LINE_SEPARATOR);

          errorReport.append("\n************ DEVICE INFORMATION ***********\n");
          errorReport.append("Brand: ");
          errorReport.append(Build.BRAND);
          errorReport.append(LINE_SEPARATOR);
          errorReport.append("Device: ");
          errorReport.append(Build.DEVICE);
          errorReport.append(LINE_SEPARATOR);
          errorReport.append("Model: ");
          errorReport.append(Build.MODEL);
          errorReport.append(LINE_SEPARATOR);
          errorReport.append("Id: ");
          errorReport.append(Build.ID);
          errorReport.append(LINE_SEPARATOR);
          errorReport.append("Product: ");
          errorReport.append(Build.PRODUCT);
          errorReport.append(LINE_SEPARATOR);
          errorReport.append("\n************ FIRMWARE ************\n");
          errorReport.append("SDK: ");
          errorReport.append(Build.VERSION.SDK);
          errorReport.append(LINE_SEPARATOR);
          errorReport.append("Release: ");
          errorReport.append(Build.VERSION.RELEASE);
          errorReport.append(LINE_SEPARATOR);
          errorReport.append("Incremental: ");
          errorReport.append(Build.VERSION.INCREMENTAL);
          errorReport.append(LINE_SEPARATOR);
          manager.reportError(errorReport.toString());

          /*Intent intent = new Intent(myContext, CrashReporter.class);
          intent.putExtra("error", errorReport.toString());
          exception.printStackTrace();
          //manager.errorToast(errorReport.toString());
          myContext.startActivity(intent);
          if(activity!=null)
           activity.finish();*/

         /* android.os.Process.killProcess(android.os.Process.myPid());
          System.exit(10);*/
         }

        }
