package com.jabuwings.error;

import android.os.Bundle;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.management.Manager;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.main.JabuWingsActivity;
import com.rey.material.widget.Button;

public class SessionDeactivated extends JabuWingsActivity {
Button btLogout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_deactivated);
        setUpToolbar(null,"Session Blocked");
        btLogout=(Button)findViewById(R.id.bt_session_deactivated_log_out);
        btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Manager(SessionDeactivated.this).outrightLogOut();
            }
        });

    }
}
