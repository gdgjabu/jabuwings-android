package com.jabuwings.management;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jabuwings.R;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.models.JabuWingsHub;
import com.jabuwings.views.main.JabuWingsActivity;
import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.List;

public class JabuWings extends JabuWingsActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jabu_wings);
        setUpToolbar(null,"Join Hub");
        Intent intent=getIntent();

        shortToast(intent.getDataString());

        String pin=intent.getData().getQueryParameter("id");
        boolean exist=false;
        try{
        List<ParseObject> hubs= JabuWingsHub.getQuery().fromLocalDatastore().find();
            for(ParseObject o : hubs)
            {
                JabuWingsHub hub=new JabuWingsHub(o);
                if(hub.getPin().equals(pin))
                {
                    exist=true;
                }

            }

            if(!exist)
            {
                HotSpot.joinHub(JabuWings.this,pin);
            }
            else{
                manager.successToast("Already a member");
                finish();
            }
        }
        catch (ParseException e)
        {
            manager.errorToast("An error occurred");
        }

    }
}
