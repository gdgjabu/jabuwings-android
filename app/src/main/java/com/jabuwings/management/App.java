package com.jabuwings.management;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v4.util.LruCache;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.jabuwings.database.DataProvider;
import com.jabuwings.feed.FeedItem;
import com.jabuwings.feed.LruBitmapCache;
import com.jabuwings.intelligence.lisa.Lisa;
import com.jabuwings.intermediary.HotSpot;
import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.models.ClassroomAssignment;
import com.jabuwings.models.ClassroomCourse;
import com.jabuwings.models.ClassroomLecturer;
import com.jabuwings.models.ClassroomMaterial;
import com.jabuwings.models.ClassroomStudent;
import com.jabuwings.models.HubReply;
import com.jabuwings.models.HubTopic;
import com.jabuwings.models.JabuWingsFriend;
import com.jabuwings.views.football.models.FootballFixture;
import com.jabuwings.views.football.models.FootballTable;
import com.parse.LiveQueryException;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseInstallation;
import com.parse.ParseLiveQueryClient;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SubscriptionHandling;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.net.URI;
import java.net.URISyntaxException;

import io.realm.Realm;

/** jjj
 * Created by Falade James on 10/3/2015 All Rights Reserved.
 */
public class App extends MultiDexApplication {
    public static final String TAG = App.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    LruBitmapCache mLruBitmapCache;
    public static volatile Handler applicationHandler = null;
    private static App mInstance;
    public static Long sessionId;
    public static LruCache<String,Bitmap> memoryCache;
    public static int chatBackground;
    private static String signalID;
    private static Context context;
    public static String url ="https://www.jabuwings.com/parse/";
    public static String liveUrl ="ws://www.jabuwings.com/live/";
    public static String wifiUrl ="http://192.168.43.71:1337/parse";
    public static String myWifiUrl ="http://192.168.43.71:1337/parse";
    public static String workWifiUrl ="http://172.28.99.230:1337/parse";
    public static  ParseLiveQueryClient parseLiveQueryClient;
    @Override
    public void onCreate() {
        super.onCreate();
        context=getApplicationContext();
        Picasso.Builder builder= new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this,Integer.MAX_VALUE));
        Picasso built = builder.build();
//        built.setIndicatorsEnabled(true);
//        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);


        String serverUrl=PreferenceManager.getDefaultSharedPreferences(this).getString("server_url","default");
        switch (serverUrl)
        {
            case "default":initialiseParseServer();break;
            case "parse": initialiseParse();break;
            default:initialiseCustomServer(serverUrl);break;
        }


        Log.d("App","Server url "+serverUrl);
        sessionId= Lisa.generateSessionId();
        chatBackground=(int)(Math.random()*7) +1;
        mInstance = this;
        applicationHandler = new Handler(getInstance().getMainLooper());
        initializeImageCache();
        Realm.init(context);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static Context getContext()
    {
        return context;
    }


    private void wifi()
    {
        WifiManager wifii= (WifiManager) getSystemService(Context.WIFI_SERVICE);
        DhcpInfo d=wifii.getDhcpInfo();

        String s_dns1="DNS 1: "+String.valueOf(d.dns1);
        String s_dns2="DNS 2: "+String.valueOf(d.dns2);
        String s_gateway="Default Gateway: "+String.valueOf(d.gateway);
        String s_ipAddress="IP Address: "+String.valueOf(d.ipAddress);
        String s_leaseDuration="Lease Time: "+String.valueOf(d.leaseDuration);
        String s_netmask="Subnet Mask: "+String.valueOf(d.netmask);
        String s_serverAddress="Server IP: "+String.valueOf(d.serverAddress);
    }



    private void initialiseCustomServer(String url)
    {
        ParseObject.registerSubclass(FeedItem.class);
        ParseObject.registerSubclass(ClassroomStudent.class);
        ParseObject.registerSubclass(ClassroomLecturer.class);
        ParseObject.registerSubclass(ClassroomAssignment.class);
        ParseObject.registerSubclass(ClassroomCourse.class);
        ParseObject.registerSubclass(ClassroomMaterial.class);
        ParseObject.registerSubclass(HubTopic.class);
        ParseObject.registerSubclass(HubReply.class);
        ParseObject.registerSubclass(FootballTable.class);
        ParseObject.registerSubclass(FootballFixture.class);
        ParseObject.registerSubclass(JabuWingsFriend.class);

       // Parse.addParseNetworkInterceptor(new ParseLogInterceptor());

        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("be4a2836d3d1cbad5cc0911fa2147d02e25b277c")
                .clientKey("abgsv2734839dfdjHJGH83832jdas")
                .server(url)
                .enableLocalDataStore()
                .build());
        Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
        //TODO remove in production
        ParseUser.enableAutomaticUser();
        ParseInstallation parseInstallation=ParseInstallation.getCurrentInstallation();
        if(signalID!=null)
        parseInstallation.put("signal",signalID);
        parseInstallation.saveInBackground();
        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(false);
        ParseACL.setDefaultACL(defaultACL, true);
        try{
            parseLiveQueryClient = ParseLiveQueryClient.Factory.getClient(new URI(liveUrl));
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }





    }
    private void initialiseParse()
    {
        Parse.enableLocalDatastore(this);
        Parse.initialize(this);
        ParseUser.enableAutomaticUser();
        ParseInstallation.getCurrentInstallation().saveInBackground();
        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(false);
        ParseACL.setDefaultACL(defaultACL, true);
    }
    private void initialiseParseServer()
    {
       // String url="http://10.0.2.2:1337/parse/";
        //String wifiUrl= url +"/parse/";



        initialiseCustomServer(url);

        //TODO remove below lines in production. {@code jabuwingsURL} should be used
//        if(Utils.getDeviceName().equals("SAMSUNG"))
//        initialiseCustomServer(url);
//        else initialiseCustomServer(wifiUrl);
    }


    public static synchronized App getInstance() {
        return mInstance;
    }

    public static synchronized LruCache<String, Bitmap> getImageCache()
    {
        return memoryCache;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    private void initializeImageCache(){
        //int cacheSize = 60*1024*1024;
        int maxMemory =  (int) (Runtime.getRuntime().maxMemory());
        int cacheSize=(int) (maxMemory*0.8);
        memoryCache	 = new LruCache<String, Bitmap>(cacheSize){
            @Override
            protected int sizeOf(String key, Bitmap value) {
                if(android.os.Build.VERSION.SDK_INT>=12){
                    return value.getByteCount();
                }
                else{
                    return value.getRowBytes()*value.getHeight();
                }
            }
        };
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            getLruBitmapCache();
            mImageLoader = new ImageLoader(this.mRequestQueue, mLruBitmapCache);
        }

        return this.mImageLoader;
    }

    public LruBitmapCache getLruBitmapCache() {
        if (mLruBitmapCache == null)
            mLruBitmapCache = new LruBitmapCache();
        return this.mLruBitmapCache;
    }

}
