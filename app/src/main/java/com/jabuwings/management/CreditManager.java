package com.jabuwings.management;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.HashMap;

/**
 * Created by Falade James on 11/5/2015 All Rights Reserved.
 */
public final class CreditManager extends Manager {

    int balance;
    Activity activity;
    public CreditManager(Activity context)
    {
        super(context);
        this.activity=context;
        balance=   parseUser.getInt(Const.CHRIMATA);
    }

    public CreditManager(Context context)
    {
        super(context);

        balance=   parseUser.getInt(Const.CHRIMATA);
    }

    public int getBalance()

    {
       return parseUser.getInt(Const.CHRIMATA);

    }

    public void deduct(final int amount)
    {
        ParseUser.getQuery().whereEqualTo(Const.USERNAME,getUsername())
                .getFirstInBackground(new GetCallback<ParseUser>() {
                    @Override
                    public void done(ParseUser parseUser, ParseException e) {
                        int balance=parseUser.getInt(Const.CHRIMATA);
                        if(amount>balance)
                        {
                            Toast.makeText(context, "You have insufficient funds", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            parseUser.put(Const.CHRIMATA,balance-amount);
                            parseUser.pinInBackground();
                            parseUser.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    Toast.makeText(context, "Alert: We have deducted "+amount+" chrimatas", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }



                    }
                });
    }


    public void add(final int amount)
    {


        ParseUser.getQuery().whereEqualTo(Const.USERNAME,getUsername())
                .getFirstInBackground(new GetCallback<ParseUser>() {
                    @Override
                    public void done(ParseUser parseUser, ParseException e) {
                        int balance=parseUser.getInt(Const.CHRIMATA);

                            parseUser.put(Const.CHRIMATA,balance+amount);
                            parseUser.pinInBackground();
                            parseUser.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if(e==null){
                                        Toast.makeText(context, "Alert: You have been credited with "+amount+" chrimatas", Toast.LENGTH_SHORT).show();
                                    }

                                    else{
                                        Toast.makeText(context, "Unable to complete transaction, check your network connection", Toast.LENGTH_SHORT).show();
                                    }



                                }
                            });




                    }
                });
    }

    public void add(final int amount, final MaterialDialog dialog)
    {


        ParseUser.getQuery().whereEqualTo(Const.USERNAME,getUsername())
                .getFirstInBackground(new GetCallback<ParseUser>() {
                    @Override
                    public void done(ParseUser parseUser, ParseException e) {
                        int balance=parseUser.getInt(Const.CHRIMATA);

                        parseUser.put(Const.CHRIMATA,balance+amount);
                        parseUser.pinInBackground();
                        parseUser.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if(e==null){
                                    dialog.dismiss();
                                   new MaterialDialog.Builder(context).title("Success")
                                            .positiveText("Ok")
                                            .cancelable(true)
                                            .content("Alert! You have been credited with " + amount + " chrimata. Enjoy")
                                            .show();

                                }
                                else{
                                    dialog.dismiss();
                                    new MaterialDialog.Builder(context).title("Success")
                                            .positiveText("Ok")
                                            .cancelable(true)
                                            .content("Unable to complete transaction due to a network error, Please request for a chrimata refund if you are not automatically credited in the next 24 hours")
                                            .show();
                                }



                            }
                        });




                    }
                });
    }




    public void share(final String username)
    {
        new MaterialDialog.Builder(activity).title("Share Chrimata").input("Amount", null, false, new MaterialDialog.InputCallback() {
            @Override
            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                int amount;
                try {
                    amount = Integer.valueOf(input.toString());
                    if(amount<=0)
                    {
                     shortToast("Invalid amount");
                    }
                    else
                    share(amount,username);
                }
                catch (Exception e) {
                    shortToast("Invalid amount");
                }
            }
        }).inputType(InputType.TYPE_CLASS_NUMBER)
                .show();
    }
    private void share(int amount, String username)
    {
        final DialogWizard dialogWizard= new DialogWizard(activity);
        final SweetAlertDialog dialog=dialogWizard.showSimpleProgress("Share", "Transferring credit");
        HashMap<String,Object> params = new HashMap<>();
        params.put(Const.FRIEND,username);
        params.put(Const.USERNAME,getUsername());
        params.put(Const.AMOUNT,amount);
        params.put(Const.VERSION,context.getString(R.string.version_number));

        ParseCloud.callFunctionInBackground(Const.CREDIT_TRANSFER_MANAGER, params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {
                dialog.dismiss();
                if(e==null)
                {
                    try{
                        String[] response = s.split("&");
                        int newAmount = Integer.valueOf(response[2]);
                        dialogWizard.showSimpleDialog(response[0],response[1]);
                        ParseUser user = getUser();
                        user.put(Const.CHRIMATA,Integer.valueOf(response[2]));
                        user.pinInBackground();
                    }
                    catch (Exception i)
                    {
                        dialogWizard.showSimpleDialog("Result",s);
                    }

                }
                else{
                    ErrorHandler.handleError(context,e);
                }
            }
        });






    }



}
