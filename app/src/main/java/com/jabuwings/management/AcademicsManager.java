package com.jabuwings.management;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jabuwings.R;
import com.jabuwings.error.ErrorHandler;
import com.jabuwings.models.ClassroomStudent;
import com.jabuwings.utilities.DialogWizard;
import com.jabuwings.views.classroom.Course;
import com.jabuwings.views.classroom.CourseRegistration;
import com.jabuwings.views.classroom.LoggedInToPortal;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.main.LoginHook;
import com.jabuwings.views.registration.StudentRegistration;
import com.jabuwings.views.registration.StudentRegistrationJabuPortal;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseConfig;
import com.parse.ParseException;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.jabuwings.management.Const.COURSES;
import static com.jabuwings.management.Const.CURRENT_SESSION;

/**
 * Created by Falade James on 2/6/2016 All Rights Reserved.
 */
public class AcademicsManager extends Manager {
    DialogFragment fragment;
    EditText etPassword;
    public AcademicsManager(Context context)
    {
        super(context);
    }
    public float getCurrentCGPA()
    {
      return  pref.getFloat(Const.CGPA,0);
    }

    public boolean isActivated(){
        return pref.getBoolean(Const.ACADEMIC_MANAGER_ACTIVATED, false);
    }
    public int getAcademicAwardGoal()
    {
        return pref.getInt(Const.ACADEMIC_AWARD_GOAL, 0);
    }

    public boolean requireJABUPortalLoggedIn(JabuWingsActivity activity ,LoggedInToPortal loggedInToPortalListener)
    {
        if(!isLoggedInToJABUPortal())
        {
            showPortalLogin(activity, loggedInToPortalListener);
            return false;
        }
        else{
            loggedInToPortalListener.loggedIn();
            return true;
        }
    }

    public String getPortalError(ParseException e)
    {
        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(e.getMessage()).getAsJsonObject();
        return json.get(Const.MSG).getAsString();
    }
    public void checkPortalError(ParseException error)
    {

        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(error.getMessage()).getAsJsonObject();
        int code = json.get("code").getAsInt();
        if(code==209) {
            setLoggedOutOfPortal();
            errorToast("Your portal password has changed. Please updated");
        }

    }

    public void showPortalLogin(final JabuWingsActivity activity, final LoggedInToPortal loggedInToPortalListener)
    {
        Dialog.Builder builder;
        builder = new SimpleDialog.Builder(R.style.SimpleDialog) {


            @Override
            protected void onBuildDone(Dialog dialog) {
                dialog.layoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                final TextView textView = (TextView) dialog.findViewById(R.id.matric);
                etPassword = (EditText) dialog.findViewById(R.id.password);
                textView.setText(getMatricNo());




            }

            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {

                processPortalLogin(activity,loggedInToPortalListener);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
                fragment.dismiss();
                activity.finish();
            }
        };

        builder.title("First Things First")
                .positiveAction(context.getString(R.string.log_in))
                .negativeAction(context.getString(R.string.cancel))
                .contentView(R.layout.jabu_portal_post_login);
        fragment = DialogFragment.newInstance(builder);
        fragment.setCancelable(false);
        fragment.show(activity.getSupportFragmentManager(), null);
    }

    public boolean isLoggedInToJABUPortal() {
        return parseUser.getBoolean("portalA");
    }

    public void setLoggedInOfPortal() {
        parseUser.put("portalA", true);
        commitUser();

    }
    public void setLoggedOutOfPortal() {
        parseUser.put("portalA", false);
        commitUser();

    }
    private void processPortalLogin(Activity activity, final LoggedInToPortal loggedInToPortalListener)
    {
        String portalPassword = etPassword.getText().toString();

        if(TextUtils.isEmpty(portalPassword))
        {
            shortToast("Please enter your password");
            return;
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put(Const.PASSWORD, portalPassword);

        final SweetAlertDialog sweetAlertDialog = new DialogWizard(activity).showProgress(context.getString(R.string.please_wait), context.getString(R.string.connecting_jabu_portal), false);

        ParseCloud.callFunctionInBackground("verifyWithJabuPortal", params, new FunctionCallback<String>() {
            @Override
            public void done(String string, ParseException e) {
                sweetAlertDialog.dismiss();
                if (e == null) {
                    fragment.dismiss();
                    setLoggedInOfPortal();
                    loggedInToPortalListener.loggedIn();


                } else {
                    ErrorHandler.handleError(context, e);
                }

            }
        });
    }
    public boolean requireCourseRegistration(final Activity activity,final Class c)
    {
        if(!getCourseRegistrationStatus())
        {
         SweetAlertDialog dialog=   new SweetAlertDialog(activity,SweetAlertDialog.ERROR_TYPE).setTitleText("Course Registration").setContentText("You must register the courses you are offering with us before accessing this feature.").setConfirmText(context.getString(R.string.register)).setCancelText(context.getString(R.string.cancel)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    activity.finish();
                    activity.startActivity(new Intent(activity, CourseRegistration.class).putExtra(Const.CLASS,c));
                }
            }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
             @Override
             public void onClick(SweetAlertDialog sweetAlertDialog) {
                 activity.finish();
             }
         });

            dialog.setCancelable(false);
            dialog.show();
            return true;
        }
        else{
            return false;
        }
    }
    public boolean getCourseRegistrationStatus()

    {
        List<String> courses= getUserCourses();
        if(courses!=null && courses.size()>0){
           
            try{
                String registrationTag=courses.get(courses.size() - 1);
                String[] tags;
                tags=registrationTag.split("_");
                String registrationSession=tags[0];
                int semester=Integer.valueOf(tags[1]);
                return registrationSession.equals(config.getString(CURRENT_SESSION,context.getString(R.string.school_session))) &&  semester==getSemester();
            }

          catch (NullPointerException | ArrayIndexOutOfBoundsException e)
          {
              e.printStackTrace();
              log("AcademicsManager","Course: "+courses);
              return false;
          }

        }

        else{
            return false;
        }

    }

    public List<Course> getStudentCourses(ClassroomStudent student)
    {
        JsonArray jsonArray = new JsonParser().parse(student.getString("CoursesMap")).getAsJsonArray();
        List<Course> courses = new ArrayList<>();
        
        for(int i =0; i<jsonArray.size(); i++)
        {
            courses.add(new Gson().fromJson(jsonArray.get(i).toString(), Course.class));
        }
        
        return courses;
    }
    public List<String> getPlainUserCourses() {
        List<String> courses= parseUser.getList(COURSES);
        List<String> courses2=new ArrayList<>();

        if(courses!=null && courses.size()!=0) {
            for (String course : courses) {
                courses2.add(course);
            }
            courses2.remove(courses2.size()-1);
        }

        return courses2;
    }
    public int getSemester()
    {
        return ParseConfig.getCurrentConfig().getInt(Const.SEMESTER,1);
    }

    public void storeLatestCGPA(float cgpa)
    {
        editor.putFloat(Const.CGPA, cgpa);
        editor.commit();
    }

    /**
     *
     * @param position The position of the academic array {@link R.array}
     *
     */
    public void storeAcademicAwardGoal(int position)
    {
        editor.putInt(Const.ACADEMIC_AWARD_GOAL, position);
        setActivated();
        editor.commit();
    }
    public void setSemester(int semester)
    {
        editor.putInt(Const.SEMESTER,semester);
        editor.commit();
    }

    public void setActivated()
    {
        editor.putBoolean(Const.ACADEMIC_MANAGER_ACTIVATED, true);
        editor.commit();

    }

    public void setCourseRegistrationStatus(boolean status)
    {
        editor.putBoolean(Const.COURSE_REGISTRATION,status);
        editor.commit();
    }

    public float getFloatFromUser(String input)
    {
        try{
            return Float.valueOf(input);
        }
        catch (Exception e)
        {
            shortToast("Invalid input. Assumed as '0'");
            return 0;
        }
    }

}

