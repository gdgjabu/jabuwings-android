package com.jabuwings.management;

/**
 * The Class Const is a single common class to hold all the app Constants.
 *
 * Do not edit any variable within this class. The result may be unpredictable
 */
public class Const
{


	/** the shared preferences id */
    public static final String SHARED_PREFERENCES_NAME ="jabuwings_10232";
    public static final String APP_NAME= "JabuWings";
    public static final int NOTIFICATION_ID=100;
    public static final int MESSAGE_NOTIFICATION=674;
    public static final int LECTURE_NOTIFICATION=532;
    public static final String CLEAR_NOTIFICATIONS="clear_notifications";
    public static final String LOCAL_NOTIFICATIONS ="Local_Notifications";

    public static final String NOTIFICATION_CHANNEL="_&Channel";
    public static final String FRIEND_ID ="friendID";
    public static final String REQUEST_ID="requestID";
    public static final int PICK_PROFILE_IMAGE_ID=242;
    public static final int PICK_IMAGE_CROP=212;
    public static final int PICK_HUB_IMAGE=299;
    public static final int PICK_IMAGE_SHARE_ID=774;
    public static final int PICK_CAMERA_SHARE_ID=434;
    public static final int PICK_FEED_IMAGE=463;
    public static final int PICK_CHAT_BACKGROUND=286;
    public static final int PICK_FILE_SHARE=199;
    public static final String PICTURES_DIRECTORY_NAME="JabuWings Photos";
    public static final String NEW_LOGIN="newLogin";
    public static final String EMAIL_VERIFIED="emailVerified";
    public static final String NOTIFICATION_INTENT="notification_intent";
    public static final String CREATED_AT="createdAt";
    public static final String ONLINE="online";
    public static final String PRIVATE="private";
    public static final String PENDING="pending";
    public static final String LAST_SEEN="lastSeen";
    public static final String PUBLIC_COURSE_MATERIALS_DIRECTORY="/JabuWings/Course Materials/";
    public static final String PUBLIC_COURSE_FORM_DIRECTORY="/JabuWings/Course Forms/";
    public static final String PUBLIC_BIO_DATA_DIRECTORY="/JabuWings/Bio data/";
    public static final String PUBLIC_EXAM_CARD_DIRECTORY="/JabuWings/Exam cards/";
    public static final String PUBLIC_RESULTS_DIRECTORY="/JabuWings/Results/";
    public static final String PUBLIC_FILE_DIRECTORY="/JabuWings/Shared Files";
    public static final String BACKUP_DIRECTORY="/JabuWings/Backups";
    public static final String PUBLIC_IMAGE_DIRECTORY="/JabuWings/JabuWings Photos";
    public static final String CHAT_BACKGROUND_NAME="chat_background.jpg";
    public static final String TUTORIAL="tutorial";
    public static final String META="meta";
    public static final String NIGERIA ="Nigeria";
    public static final String CURRENT_SESSION ="current_session";
    public static final String USER_PUBLIC ="user_public";
    public static final String USER_FRIEND ="user_friend";
    public static final String USER_FRIENDS ="user_friends";
    public static final String DEFAULT_PORTAL_URL ="http://portal.jabu.edu.ng";
    public static final String DEFAULT_JABU_WEB_URL ="http://jabu.edu.ng";
    public static final String[] ALL_LEVELS={"100","200","300","400","500"};
    public static final String[] ALL_SEMESTERS= {"1","2"};


    public static final String MESSAGE_SENT="sent";
    public static final String MESSAGE_DELIVERED="delivered";
    public static final String MESSAGE_SENDING="sending";
    public static final String MESSAGE_READ="read";
    public static final String MESSAGE_FAILED="failed";

    public static final String USER_CREATOR = "userCreator";
    public static final String USER_UPDATER = "userUpdater";
    public static final String ACAD_CHOICE = "acad_choice";
    public static final String VERSION_CHECK="versionCheck";
    public static final String VERSION_EXPIRED="versionExpired";
    public static final String VERSION_EXPIRED_MESSAGE="versionExpiredMessage";
    public static final String USERNAME= "username";
    public static final String BLOCKED="blocked";
    public static final String ACADEMIC_SESSION ="academic_session";

    public static final String MANUFACTURER="manufacturer";
    public static final String MODEL="model";
    public static final String API_LEVEL="API_LEVEL";

    public static final char   SEPARATOR='_';
    public static final char   SPACE=' ';
    public static final char   AT='@';
    public static final String VIEW_THIS_IMAGE= "view_this_image";
    public static final String IMAGE_VIEWER_URI="image_viewer_uri";
    public static final String IS_USER_PROFILE= "is_user_profile";
    public static final String PROFILE_PICTURE="ProfilePicture";
    public static final String PROFILE_PICTURE_URL="profilePictureUrl";
    public static final String PROFILE_PICTURE_DATE ="profilePictureDate";
    public static final String PROFILE_PICTURE_NAME ="profilePicture.jpg";
    public static final String CHOOSE="Choose";
    public static final String REQUESTS ="requests";
    public static final String FRIEND_REQUESTS ="friend_requests";
    public static final String _FRIEND_REQUESTS ="Requests";
    public static final String FRIEND="friend";
    public static final String FRIENDS="friends";
    public static final String _FRIENDS="Friends";
    public static final String _HUBS="Hubs";
    public static final String HUBS="hubs";
    public static final String HUB_TOPICS="HubTopics";
    public static final String TOPIC="topic";
    public static final String HUB_REPLIES="HubReplies";


    public static final String USER="user";
    public static final String CHAT_BACKGROUND="chat_background";
    public static final String MEDIA_TRANSFER_CLASS ="MediaTransfer";
    public static final String HEADER="header";
    public static final String FROM="from";
    public static final String TO="to";
    public static final String POSITION="position";
    public static final String TAG="tag";
    public static final String FILE="file";
    public static final String DOWNLOAD_TYPE="download_type";
    public static final String URL="url";
    public static final String FILE_URL="fileUrl";
    public static final String COURSE="course";
    public static final String COURSES="courses";
    public static final String COURSE_REGISTRATION="course_registration";
    public static final String DATA="data";
    public static final String DATA2="data2";
    public static final String PIN="pin";
    public static final String VALID="valid";
    public static final String TYPE="type";
    public static final String MSG_TYPE="msgType";
    public static final String CONTENT="content";
    public static final String SUBMITTED="submitted";
    public static final String BEFORE="before";
    public static final String AMOUNT="amount";

    public static final String CGPA="cgpa";
    public static final String CLASS="class";
    public static final String ACADEMIC_MANAGER_ACTIVATED="academicManagerActivated";
    public static final String ACADEMIC_AWARD_GOAL="academicAwardGoal";
    public static final String SEMESTER="semester";
    public static final String SESSION="session";

    //Firebase
    public static final String FEED= "Feed";
    public static final String COMMENTS= "comments";
    public static final String LIKES= "likes";

    //Server Strings
    public static final String MESSAGE_BIRD="messageBird";
    public static final String FRIEND_AUTHENTICATOR= "friendAuthenticator";
    public static final String RELATIONSHIP_MEDIATOR= "relationshipMediator";
    public static final String IMAGE_PHOTOGRAPHER="imagePhotographer";
    public static final String SESSION_UPDATER="sessionUpdater";
    public static final String TYPING_WATCHDOG="typingWatchDog";
    public static final String RELATIONS="ABC_Relations";
    public static final String FRIENDS_RADAR="friendsRadar";
    public static final String MESSAGE_DELIVERY_SERVICE="messageDeliveryService";
    public static final String HUB_MESSAGING="hubMessaging";
    public static final String HUB_MEMBER_REMOVER="hubMemberRemover";
    public static final String CREDIT_TRANSFER_MANAGER ="creditTransferManager";
    public static final String REPORT_PROBLEM_CLASS="Problems_Reported";
    public static final String FEED_MONGER_CLASS="feedMonger";
    public static final String CHATS_CLASS="Chats";
    public static final String GET_STUDENT_COURSES="getStudentCourses";
    public static final String UPDATE_CLASSROOM_STUDENT="classroomStudentUpdate";
    public static final String REGISTER_STUDENT_COURSES="registerStudentCourses";
    public static final String GET_COURSE_FORM="getStudentCourseForm";
    public static final String GET_BIO_DATA="getStudentBioData";
    public static final String GET_EXAM_CARD="getStudentExamCard";
    public static final String CHATTING_WITH_LISA="ChattingWithLisa";
    public static final String HUBS_CHAT_CLASS="HubChats";
    public static final String ID="id";
    public static final String APP_CLASS="App";
    public static final String BY="by";
    public static final String OBJECT="object";
    public static final String LISA="lisa";
    public static final String USER_DATA="UserData";
    public static final String MEDICAL_DATA="medical_data.json";
    public static final String LECTURER="lecturer";
    public static final String CLASSROOM_STUDENT="ClassroomStudent";
    public static final String CLASSROOM_LECTURER="ClassroomLecturer";
    public static final String CLASSROOM="Classroom";
    public static final String CLASSROOM_NOTIFICATIONS="ClassroomNotifications";
    public static final String COURSE_TITLE="courseTitle";
    public static final String COURSE_DESC="courseDesc";
    public static final String COURSE_UNIT="units";
    public static final String SNAPSHOT="snapshot";
    public static final String CLASSROOM_STUDENT_REGISTRAR="classroomStudentRegistrar";
    public static final String CLASSROOM_LECTURER_REGISTRAR="classroomLecturerRegistrar";
    public static final String CLASSROOM_COURSE_REGISTRAR="classroomCourseRegistrar";

    public static final String DEVICE_TYPE="android";
    public static final String VERSION="version";
    public static final String ANDROID_VERSION="android_version";
    public static final String TIMETABLE_VERSION="timetable_version";
    public static final String PATH="path";

    public static final String DESC="desc";
    public static final String START="start";
    public static final String STOP="stop";
    public static final String CATEGORY="category";
    public static final String ANONYMOUS="anonymous";
    public static final String CATEGORIES="categories";
    public static final String CANDIDATES="candidates";

    public static final String HUB_TITLE ="hubTitle";
    public static final String HUB_ID ="hubId";
    public static final String HUB_DESC="hubDesc";
    public static final String HUB_CLASS_NAME ="ABC_HUBS";
    public static final String COURSE_MATERIALS_CLASS_NAME="Course_Materials";
    public static final String HUB_AUTHENTICATION_NO ="hubAuthenticationNo";
    public static final String CREATED_BY="createdBy";

    public static final String ADMIN="admin";
    public static final String _ADMIN="Admin";
    public static final String MEMBERS="members";
    public static final String _MEMBERS="Members";
    public static final String HUB_MEMBERS="hub_members";


    public static final String NAME ="name";
    public static final String EMAIL="email";
    public static final String PASSWORD="password";
    public static final String PORTAL_PASSWORD="portal_password";
    public static final String SHA="sha";
    public static final String STATE ="state";
    public static final String COUNTRY ="country";
    public static final String BIRTHDAY ="birthday";
    public static final String PHONE_NO ="phoneNo";
    public static final String MATRIC_NO ="matricNo";
    public static final String SEX="sex";
    public static final String MEDICAL_NO="medicalNo";
    public static final String DEPARTMENT ="department";
    public static final String ACADEMIC_DEPARTMENT="academic_department";
    public static final String YEAR_OF_GRADUATION="YOG";
    public static final String DATE_OF_BIRTH ="DOB";
    public static final String MODE ="mode";
    public static final String LEVEL ="level";
    public static final String STATUS ="status";
    public static final String DEVICE_NAME="deviceName";
    public static final String COMMITS="commits";
    public static final String CHRIMATA="chrimata";
    public static final String ACCOUNT_TYPE ="type";
    public static final String VERIFIED_AS ="verifiedAs";
    public static final String PHONE_VERIFIED="phoneVerified";
    public static final String IS_VERIFIED ="isVerified";
    public static final String AUTHENTICATION_ID="authenticationId";

    public static final String FEED_CLASS_NAME="Feed_Pool";

    //json data
    public static final String TITLE="title";
    public static final String ALERT="alert" ;
    public static final String SENDER="sender";
    public static final String RECEIVER="receiver";
    public static final String MSG_ID="msgId";
    public static final String OBJECT_ID="objectId";
    public static final String MSG="msg";
    public static final String REMARK="remark";
    public static final String NOT_ID="not_id";
    public static final String TIME="time";
    public static final String TIMESTAMP="timestamp";
    public static final String SIZE="size";
    public static final String DATE="date";
    public static final String UPDATE="update";


    public static final String DETAILS="details";
    public static final String HOSTEL="hostel";
    public static final String ROOM="room";
    public static final String POSITIVE="positive";
    public static final String NEGATIVE= "negative";


    public static final String AS="AS";
    public static final String ES="ES";
    public static final String HS="HS";
    public static final String HM="HM";
    public static final String LW="LW";
    public static final String MS="MS";
    public static final String NS ="NS";
    public static final String SS="SS";

    public static final String AGRICULTURAL_SCIENCES="Agricultural Sciences";
    public static final String ENVIRONMENTAL_SCIENCES="Environmental Sciences";
    public static final String HEALTH_SCIENCES="Health Sciences";
    public static final String HUMANITIES="Humanities";
    public static final String LAW="Law";
    public static final String MANAGEMENT_SCIENCES="Management Sciences";
    public static final String NATURAL_SCIENCES="Natural Sciences";
    public static final String SOCIAL_SCIENCES="Social Sciences";


    public static final String TIMETABLE_XML="timetable.xml";
    public static final String[] TIMETABLE_DAYS={"SUN","MON","TUE","WED","THU","FRI","SAT"};
    public static final String COURSE_CODE="courseCode";
    public static final String TIME_START="timeStart";
    public static final String TIME_STOP="timeStop";
    public static final String VENUE="venue";
    public static final String DAY="day";

    public static final String CLIENT_KEY="clientKey";
    public static final String CLIENT_ID="clientId";

    public static final String CHANNEL_NAME="channelName";
    public static final String CHANNEL_ID="channelId";


    public enum MEDIA_UPLOAD_TYPE {PROFILE_PICTURE, FEED_PICTURE, MESSAGE_PICTURE, HUB_PICTURE}

    public enum SERVER_MESSAGE_TYPE{TEXT,IMAGE,AUDIO,FILE}

    public  enum REGISTRATION_TYPE {  STUDENT, STAFF, ALUMNI, PRIVILEDGED}

    /**
     * An enum representing the friendship status between two users.
     * ACTIVE: The friendship is ok
     * BLOCKED:The other party(The friend of the user) has blocked the user
     * REMOVED:The other party does not have the user is his friends list. The other party usually, removed the user.
     * GONE:The other party's account no longer exists
     */
    public enum FRIENDSHIP_STATUS{ACTIVE,BLOCKED,REMOVED,GONE}


    public  static class Feed{
        public static final String TITLE="title";
        public static final String AUTHOR="author";
        public static final String AUTHOR_PUBLIC="author_public";
        public static final String AUTHOR_URL="authorUrl";
        public static final String CONTENT="content";
        public static final String PICTURE_URL="pictureUrl";
        public static final String EXTERNAL_URL ="externalLink";
        public static final String POSTED_BY="postedBy";
        public static final String VALID="valid";
        public static final String PICTURE_FILE="pictureFile";
        public static final String FEED_ID="feedId";
        public static final String BY="by";
        public static final String LIKES="likes";
        public static final String COMMENTS="comments";
        public static final String LIKES_COUNT="likes_count";
        public static final String COMMENTS_COUNT="comments_count";
        public static final String IS_LIKED="is_liked";

        public enum POSTED_BY{USER,HUB,ENTITY}

    }


}
