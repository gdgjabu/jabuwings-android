package com.jabuwings.management;

import android.content.Context;
import android.util.Log;

import com.jabuwings.R;
import com.jabuwings.views.main.Domicile;
import com.parse.ConfigCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseConfig;
import com.parse.ParseException;
import com.parse.ParseSession;

import java.util.HashMap;

/**
 * Created by Falade James on 11/18/2015 All Rights Reserved.
 */
public  class SessionManager extends Manager {

    String noUpdateDownload ="noUpdateDownload";
    String checkForUpdates="check_for_updates";
    public static String token;
    public SessionManager(Context context)
    {
        super(context);
    }
    public void check()
    {
       //TODO check for valid session

        ParseSession.getCurrentSessionInBackground(new GetCallback<ParseSession>() {
            @Override
            public void done(ParseSession parseSession, ParseException e) {
                if( e!=null && e.getCode()==ParseException.INVALID_SESSION_TOKEN){setUserSessionBlocked(true);}
                else if(parseSession!=null) token=parseSession.getSessionToken();
            }
        });

        final HashMap<String, Object> params = new HashMap<>();
        params.put(Const.VERSION, context.getString(R.string.version_number));
        params.put(Const.TYPE,Const.DEVICE_TYPE);
        params.put(Const.USERNAME, getUsername());

        ParseCloud.callFunctionInBackground(Const.VERSION_CHECK, params, new FunctionCallback<String>() {
            @Override
            public void done(String s, ParseException e) {

                if(e==null)
                if(!s.equals(Const.POSITIVE))
                {
                    editor.putBoolean(Const.VERSION_EXPIRED, true);
                    editor.putString(Const.VERSION_EXPIRED_MESSAGE, s);
                    editor.commit();
                    setUserSessionBlocked(true);
                    Log.d("Session","Session blocked for version check");

                }


            }
        });


        ParseConfig.getInBackground(new ConfigCallback() {
            @Override
            public void done(ParseConfig parseConfig, ParseException e) {
                if(e==null)
                {
                    log("Session","Config obtained "+parseConfig.getString(Const.ANDROID_VERSION));
                    String session =parseConfig.getString(Const.SESSION);
                    String semester=parseConfig.getString(Const.SEMESTER);
                    String latestVersion=parseConfig.getString(Const.ANDROID_VERSION);
                    String timeTableVersion=parseConfig.getString(Const.TIMETABLE_VERSION);
                    long noUpdate=getUpdateNoRemind();


                    if(defaultSharedPref.getBoolean(checkForUpdates,true))
                        if(noUpdate==0 || System.currentTimeMillis()-noUpdate>=604_800_000)
                            if(latestVersion!=null && !latestVersion.equals(context.getString(R.string.version_number)))
                            {
                                Domicile.showDownloadNewVersion=true;
                            }


                }








            }
        });




    }

    public void setUpdateNoRemind(long date)
    {
        editor.putLong(noUpdateDownload,date);
        editor.commit();
    }

    private long getUpdateNoRemind()
    {
        return pref.getLong(noUpdateDownload,0);
    }








}
