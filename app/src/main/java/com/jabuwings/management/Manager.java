package com.jabuwings.management;

/**
 * Created by Falade James on 25/07/2015.
 */
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.customtabs.CustomTabsCallback;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsService;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.jabuwings.R;
import static com.jabuwings.management.Const.*;
import com.jabuwings.database.DataProvider;
import com.jabuwings.database.DatabaseManager;
import com.jabuwings.database.realm.Friend;
import com.jabuwings.database.realm.Hub;
import com.jabuwings.database.realm.Message;
import com.jabuwings.feed.FeedItem;
import com.jabuwings.intelligence.BootHub;
import com.jabuwings.intelligence.FeedService;
import com.jabuwings.intelligence.LectureService;
import com.jabuwings.intelligence.ProfileWatchDog;
import com.jabuwings.intelligence.RealTimeMessagingService;
import com.jabuwings.intermediary.HouseKeeping;
import com.jabuwings.intermediary.UserQuery;
import com.jabuwings.models.JabuWingsPublicUser;
import com.jabuwings.models.JabuWingsUser;
import com.jabuwings.models.MessageItem;
import com.jabuwings.utilities.Utils;
import com.jabuwings.views.main.Domicile;
import com.jabuwings.views.main.JabuWingsActivity;
import com.jabuwings.views.main.Login;
import com.jabuwings.widgets.SweetAlert.SweetAlertDialog;
import com.jabuwings.widgets.toast.TastyToast;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseClassName;
import com.parse.ParseCloud;
import com.parse.ParseConfig;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.SecretKeySpec;

import io.realm.Realm;

/**
 * @author Falade James
 * The Manager is a class that can access all the data of the current user. It
 * is used to get the user profile, manipulate database, store login sessions.
 * It is one of the most used class in this project.
 *
 */

public class Manager extends Object{
    //The current user
    ParseUser parseUser;
    JabuWingsUser jabuWingsUser;

    // SharedPreferences object
    SharedPreferences pref;

    //SharedPreferences default object
    SharedPreferences defaultSharedPref;

    //SharedPreferences Editor
    Editor editor;

    // Context
    Context context;


    //The activity if any
    AppCompatActivity activity;

    CustomTabsClient mCustomTabsClient;

    protected ParseConfig config;
    // SharedPreferences mode
    int PRIVATE_MODE = 0;

    //The database helper
    DatabaseManager databaseManager;
    // SharedPreferences file name
    private static final String PREF_NAME = SHARED_PREFERENCES_NAME;

    // All SharedPreferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String IS_BLOCKED = "isBlocked";

    private static final String IS_SESSION_BLOCKED = "isSessionBlocked";
    private static final String KEY_USERNAME = "username";
    private static final String ID = "ID";
    private static final String KEY_PROFILE_PICTURE_URL = "profile_picture_url";

    //This checks that the user has passed through proper logging in procedures.
    private static boolean userLoggedIn=false;

    private static final String KEY_LATEST_FEED_ID = "latestFeedId";
    private static final String KEY_LATEST_FEED_CREATED_AT = "latestFeedCreatedAt";
    private static final String KEY_LATEST_MESSAGE_DATE="latestMessageDate";
    public static final String KEY_LECTURE_NOTIFICATIONS="lecture_notifications";
    public static final String KEY_MESSAGE_DELIVERY_TYPE="message_delivery_type";
    public static final String KEY_PREFERRED_TYPEFACE="preferred_typeface";
    public static final String KEY_TIMETABLE_VERSION="timetable_version";
    public static final String KEY_CUSTOM_TIMETABLE="custom_timetable";
    public static final String KEY_CUSTOM_BACKGROUND="custom_background";
    public static final String KEY_CHAT_BACKGROUND="chat_background";
    public static final String KEY_COLLEGE="college";
    public static final String KEY_LAST_HYMN="last_hymn";
    public static final String KEY_INTRO_VIEWED="intro_viewed";
    public static final String KEY_PROFILE_PICTURE_DATE="profile_picture_date";
    public static final String KEY_LAST_ENTITY_DRAFT ="last_draft";
    public static final String KEY_FEED_DRAFT="feed_draft";
    public static final String KEY_APP_OPENINGS="app_openings";
    public static final String KEY_RATED="rated";
    public static final String KEY_RATING="rating";
    public static final String KEY_DONT_SHOW_RATING="no_rating";
    String KEY_TIMETABLE_NOTIFICATION;
    private FirebaseAnalytics mFirebaseAnalytics;
    private boolean DEBUG_LOG = true;
    /**
     * The constructor of the manager class
     *
     * @param context The current context
     */
    public Manager(Context context) {
        this.context = context;
        pref = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        defaultSharedPref=PreferenceManager.getDefaultSharedPreferences(context);
        KEY_TIMETABLE_NOTIFICATION = context.getString(R.string.key_lecture_notifications);
        parseUser = ParseUser.getCurrentUser();
        jabuWingsUser= new JabuWingsUser(parseUser);
        databaseManager = new DatabaseManager(context);
        config = ParseConfig.getCurrentConfig();
        editor = pref.edit();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);


    }


    public Manager(AppCompatActivity activity)
    {

        this(activity.getApplicationContext());
        this.activity=activity;
    }

    /**
     * Create login session
     *
     * @param username The username of the user logging in.
     */
    public void createLoginSession(String username) {

        Bundle bundle = new Bundle();
        bundle.putString(Const.USERNAME, username);
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, getUserId());
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);




        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing user data in pref
        editor.putString(KEY_USERNAME, username);
        // commit changes
        commit();

        parseUser=ParseUser.getCurrentUser();

        try {
            addLisa();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        //Setting the user's college
        if(getUserType()==REGISTRATION_TYPE.STUDENT.ordinal())
        {
            determineCollege();
        }
        if(getUserCourses()!=null && getUserCourses().size()>0)
        {
            defaultSharedPref.edit().putBoolean(KEY_TIMETABLE_NOTIFICATION,true).commit();
        }
        login();

        activateLectureReminderService();
    }
    private void determineCollege()
    {
        String department = getDepartment();
        if(department.equalsIgnoreCase("Law"))
        {
            setCollege(LAW);
            return;
        }
        List<String> asDepartments= Arrays.asList(context.getResources().getStringArray(R.array.agricultural_sciences));
        List<String> esDepartments=Arrays.asList(context.getResources().getStringArray(R.array.environmental_sciences));
        List<String> hmDepartments=Arrays.asList(context.getResources().getStringArray(R.array.humanities));
        List<String> nsDepartments=Arrays.asList(context.getResources().getStringArray(R.array.natural_sciences));
        List<String> ssDepartments=Arrays.asList(context.getResources().getStringArray(R.array.social_sciences));
        List<String> msDepartments=Arrays.asList(context.getResources().getStringArray(R.array.management_sciences));
        String college;
        if(asDepartments.contains(department))
        {
            college=AGRICULTURAL_SCIENCES;
            setCollege(college);
            return;
        }
        if(esDepartments.contains(department))
        {
            college=ENVIRONMENTAL_SCIENCES;
            setCollege(college);
            return;
        }

        if(hmDepartments.contains(department))
        {
            college=HUMANITIES;
            setCollege(college);
            return;
        }

        if(nsDepartments.contains(department))
        {
            college=NATURAL_SCIENCES;
            setCollege(college);
            return;
        }

        if(ssDepartments.contains(department))
        {
            college=SOCIAL_SCIENCES;
            setCollege(college);
            return;
        }
        if(msDepartments.contains(department))
        {
            college=MANAGEMENT_SCIENCES;
            setCollege(college);
        }

    }


    /**
     * Initialise a user that is already logged to update some instances.
     */
    public void login()
    {
        if(parseUser==null || getUsername()==null)
        {
            outrightLogOut();
            return;
        }
        enableBootService();
        initialiseProfileWatchDog();
        initialiseFeedService();
        setUserOnline();
        setUpPush();
        setUpLectureReminder();
        setUpRealTimeMessagingService();
        ParseUser.getQuery().getInBackground(getUserId(), new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if(e==null && parseUser!=null)
                {
                    parseUser.getParseObject(Const.DATA).fetchIfNeededInBackground(new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject parseObject, ParseException e) {
                            if(e==null)
                            parseObject.pinInBackground();
                        }
                    });
                    parseUser.pinInBackground();
                    log("Manager","User updated");

                }
                else{
                    //errorToast("Unable to update user "+e.getMessage());
                }
            }
        });

        if(!Domicile.newSignIn)
        {
        checkFriends();
        checkHubs();
        }
        else

        if(getProfilePictureDate()!=getUserProfilePictureDate())
        {
           updateProfilePicture();
        }
        userLoggedIn=true;
        CreateSyncAccount(context);
        appOpened();
        initialiseChromePlugin();
//        getAllIpsOnWifi();
    }

    private void appOpened()
    {
        int former=pref.getInt(KEY_APP_OPENINGS,0);
        editor.putInt(KEY_APP_OPENINGS,++former);
        commit();

        Bundle bundle = new Bundle();
        bundle.putString(Const.USERNAME, getUsername());
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, getUserId());
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, bundle);
    }

    public void logEvent(String event, Bundle bundle)
    {
        mFirebaseAnalytics.logEvent(event, bundle);
    }


    public void logOutSession()
    {
        userLoggedIn=false;
    }
    public void initialiseProfileWatchDog() {
        context.startService(new Intent(context, ProfileWatchDog.class));
    }

    public void initialiseFeedService() {
//        context.startService(new Intent(context, FeedService.class));
    }


    public DatabaseManager getDatabaseManager()
    {
        return databaseManager;
    }

    public ParseConfig getConfig()
    {
        return config;
    }
    public boolean backupAndEncryptDatabase()
    {
        try {
            String name="/data/data/"+context.getPackageName()+"/databases/jabuwings.db";
            FileInputStream is=new FileInputStream(name);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int bytesRead;
            while (( bytesRead = is.read(b))!=-1)
            {
                bos.write(b, 0, bytesRead);
            }
            byte[] bytes = bos.toByteArray();
            log("Backup "+getLastUsername()+" "+getSHA());
            byte[] key = ("23343jkdfdsfSKFJELDDk134@#$%^&*(" + getLastUsername() + getSHA()).getBytes("UTF-8");
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16); // use only first 128 bit

            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
            Cipher cipher;
            byte[] encrypted=null;

            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            // Initialize cipher

            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            encrypted = cipher.doFinal(bytes);

            File dir;
            dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+Const.BACKUP_DIRECTORY+"/"+getLastUsername());
            dir.mkdirs();
            String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US).format(new Date());
            File file =new File(dir,"JabuWings_Backup"+".jwbackup");
            FileOutputStream fileOutputStream=new FileOutputStream(file);
            fileOutputStream.write(encrypted);
            return true;

        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public boolean restoreEncryptedDatabase(String path,String username)
    {
        Cipher cipher;


        byte[] decryptedData=null;


        CipherInputStream cis=null;


        try{
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            log("Restore "+username+" "+getSHA());
            byte[] key = ("23343jkdfdsfSKFJELDDk134@#$%^&*(" +username + getSHA()).getBytes("UTF-8");
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16); // use only first 128 bit

            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");


            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);


            // Create CipherInputStream to read and decrypt the image data


            FileInputStream fis = new FileInputStream(path);

            cis = new CipherInputStream(fis, cipher);


            // Write encrypted image data to ByteArrayOutputStream


            ByteArrayOutputStream buffer = new ByteArrayOutputStream();


            byte[] data = new byte[2048];


            while ((cis.read(data)) != -1) {


                buffer.write(data);


            }


            buffer.flush();


            decryptedData=buffer.toByteArray();

            String currentDBPath = "//data/com.jabuwings/databases/jabuwings.db";
            File currentDB = new File(Environment.getDataDirectory(), currentDBPath);
            currentDB.createNewFile();
            FileOutputStream outputStream=new FileOutputStream(currentDB);
            outputStream.write(decryptedData);
            //buffer.
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }

        /*
        try {
    File sd = Environment.getExternalStorageDirectory();
    File data = Environment.getDataDirectory();

    if (sd.canWrite()) {
    String currentDBPath = "//data/package name/databases/database_name";
        String backupDBPath = "database_name";
        File currentDB = new File(data, currentDBPath);
        File backupDB = new File(sd, backupDBPath);

        if (currentDB.exists()) {
            FileChannel src = new FileInputStream(backupDB).getChannel();
            FileChannel dst = new FileOutputStream(currentDB).getChannel();
            dst.transferFrom(src, 0, src.size());
            src.close();
            dst.close();
            Toast.makeText(getApplicationContext(), "Database Restored successfully", Toast.LENGTH_SHORT).show();
        }
    }
} catch (Exception e) {
}
         */
    }

    public void openHome()
    {
        context.startActivity(new Intent(context,Domicile.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }
    public boolean backupDatabase()
    {
        OutputStream output=null;
        try {
            String appPath=context.getFilesDir().getAbsolutePath();
            String name="/data/data/"+context.getPackageName()+"/databases/jabuwings.db";
            FileInputStream inputStream=new FileInputStream(name);
            File dir;
            dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+Const.BACKUP_DIRECTORY);
            dir.mkdirs();
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
            File file =new File(dir,"JabuWings_Backup_"+timeStamp+".db");
            output= new FileOutputStream(file);
            byte[] buffer = new byte[4*1024];
            int read;
            while((read=inputStream.read(buffer))!=-1)
            {
                output.write(buffer,0,read);
            }

            output.flush();
            output.close();
            return true;


        }
        catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }



    }
    public String getVersion()
    {
        return context.getString(R.string.version_number);
    }

    public HashMap<String,Object> getDefaultCloudParams()
    {
        HashMap<String,Object> params = new HashMap<>();
        params.put(VERSION,getVersion());
        params.put(USERNAME,getUsername());
        return params;
    }
    public int getHomeScreenPreference()
    {
        return Integer.valueOf(defaultSharedPref.getString("home_screen", "1"));
    }

    public boolean getResendMessagePreference()
    {
        return  defaultSharedPref.getBoolean("resend_failed_messages",true);
    }

    public String getBaseUrl()
    {
        return getWebUrl();
        //return Utils.getDeviceName().equals("SAMSUNG")? "10.0.2.2" : "192.168.1.11";
    }

    public String getWebUrl(){
        return "https://www.jabuwings.com";
        //return Utils.getDeviceName().equals("SAMSUNG")? "10.0.2.2:8080" : "192.168.1.11:8080";
    }
    public JabuWingsUser getJabuWingsUser()
    {
        return new JabuWingsUser(ParseUser.getCurrentUser());
    }
    public ParseUser getUser(){return ParseUser.getCurrentUser();}

    /**
     * This is true if the user has passed through {@code login()}
     * @return The value
     */
    public boolean isUserLoggedIn(){
        return userLoggedIn;
    }

    public String getUserId(){return  parseUser.getObjectId();}
    public String getFirebaseUserId(){return  FirebaseAuth.getInstance().getCurrentUser().getUid();}
    public int getUserType() {
        return parseUser.getInt(ACCOUNT_TYPE);
    }

    public String getAcademicDepartment()
    {
        return parseUser.getString(Const.ACADEMIC_DEPARTMENT);
    }
    /**
     * @return the username of the user that is logged in or immediate logged out user.
     * This is used to check during subsequent login attempts
     */
    public String getLastUsername()
    {
        return pref.getString(KEY_USERNAME, null);
    }
    /**
     * @return The username of the current user
     */
    public String getUsername() {
        return parseUser.getUsername();
    }

    /**
     * @return The email of the current user
     */
    public String getEmail() {
        return parseUser.getEmail();
    }

    /**
     * @return The name of the current user
     */
    public String getName() {
        return parseUser.getString(NAME);
    }

    /**
     * @return The department of the current user. This will return null if the user has no registered department
     */
    public String getDepartment() {
        return parseUser.getString(DEPARTMENT);
    }

    /**
     * @return The most recent status of the current user
     */
    public String getStatus() {
        return parseUser.getString(STATUS);
    }

    /**
     * @return The matric_no of the current user if any.
     */
    public String getMatricNo() {
        return parseUser.getString(MATRIC_NO);
    }


    /**
     * @return The medical number of the current user if any.
     */
    public String getMedicalNo()
    {
        return parseUser.getString(MEDICAL_NO);
    }

    /**
     * @return The phone number of the current user if any,
     */
    public String getPhoneNo() {
        return parseUser.getString(PHONE_NO);
    }

    /**
     * @return The registered state of origin of the current user
     */
    public String getState() {
        return parseUser.getString(STATE);
    }

    /**
     * @return The current academic level of the current user, if any.
     */
    public String getLevel() {
        return parseUser.getString(LEVEL);
    }

    /**
     * @return The available/balance credit of the current user in chrimata.
     */
    public String getCredit() {
        return String.valueOf(parseUser.getInt(CHRIMATA));
    }

    public String getCommits(){return String.valueOf(parseUser.getInt(COMMITS));}
    /**
     * @return The Profile Picture URL of the current user
     */
    public String getProfilePictureUrl() {
        return parseUser.getString(PROFILE_PICTURE_URL);
    }

    public String getSHA(){return pref.getString(SHA,null);}
    public void setSHA(String sha){ editor.putString(SHA,sha); commit();}
    public long getUserProfilePictureDate(){return jabuWingsUser.getProfilePictureDate();}
    public long getProfilePictureDate(){return pref.getLong(KEY_PROFILE_PICTURE_DATE, 0);}

    public boolean isVerified(){ return jabuWingsUser.isVerified();}
    public String getVerifiedAs(){return jabuWingsUser.isVerifiedAs();}
    /**
     * @param username The username of the friend whose url is queried.
     * @return The Profile Picture of the {@param username}
     */
    @Deprecated
    public String getFriendProfilePictureUrl(String username) {
        return databaseManager.getFriendProfilePictureURL(username);
    }

    public long getFriendProfilePictureDate(String username) {
        return databaseManager.getFriendProfilePictureDate(username);
    }

    public String getFriendOrHubProfilePictureName(String username)
    {
        return username+"_"+PROFILE_PICTURE;
    }

    public String getEntityLastDraft(String id)
    {
        return pref.getString(id+"_"+KEY_LAST_ENTITY_DRAFT,"");
    }


    public boolean isIntroViewed()
    {
        return pref.getBoolean(KEY_INTRO_VIEWED, false);
    }

    /**
     *
     * @param msgDatabaseId The database Id of the message being queried
     * @return This contains a MessageItem object including all the data in the message.
     */
    @Deprecated
    public MessageItem getFriendMessageItem(String msgDatabaseId)
    {
        return null;
    }
    /**
     *
     * @param msgDatabaseId The database Id of the message being queried
     * @return This contains a MessageItem object including all the data on the message.
     */
    @Deprecated
    public MessageItem getHubMessageItem(String msgDatabaseId)
    {
        return null;
    }



    public int getFeedId(String feedId){
        return pref.getInt(feedId + ID, 0);
    }

    public Date getLatestFeedTime() {
        return
        new Date(pref.getLong(KEY_LATEST_FEED_CREATED_AT, 0));
    }
    public Date getLatestMessageDate()
    {
      return new Date(pref.getLong(KEY_LATEST_MESSAGE_DATE,new Date().getTime()));
    }




    public Typeface getTypeFace()
    {
        String assetPath = JabuWingsActivity.getAssetPath(getPreferredTypeFace());
        if(assetPath==null) return null;
        return  Typeface.createFromAsset(context.getAssets(),assetPath);
    }
    public int getPreferredTypeFace()
    {
        return Integer.valueOf(defaultSharedPref.getString(KEY_PREFERRED_TYPEFACE, "0"));
    }

    public List<String>getUserHubs(){return parseUser.getList(HUBS);}

    public ParseRelation<ParseObject> _getUserFriends(){
        return parseUser.getRelation(_FRIENDS);
    }
    public ParseRelation<ParseObject> _getUserHubs(){
        return parseUser.getRelation(_HUBS);
    }

    /**
     * This is to obtain all the friends of the user
     * @return The usernames of all friends in the "Online" database
     */
    public List<String> getUserFriends()
    {
        return parseUser.getList(FRIENDS);
    }

    /**
     * This is to obtain a list of all the user's friend requests
     * The tag contains the remark of the
     * @return The usernames+tag of the friend requests and
     */
    public List<String> getFriendRequests()
    {
        List<String> course = parseUser.getList(REQUESTS);
        if(course ==null)
        {
            return new ArrayList<>();
        }
        return course;
    }

    public ParseRelation<JabuWingsPublicUser> _getFriendRequests()
    {
        return parseUser.getRelation(Const.FRIEND_REQUESTS);

    }

    /**
     * This is to obtain all the friends of the user
     * @return The usernames of all friends in the Local Database
     */
    public List<String> getFriendUsernames()
    {
        List<String> usernames= new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();

        for(Friend friend : realm.where(Friend.class).findAll())
        {
            usernames.add(friend.getUsername());
        }
        realm.close();
        return usernames;
    }

    /**
     * This is to obtain all the friends of the user
     * @return The usernames of all friends in the Local Database
     */
    public List<String> getHubIds()
    {
        List<String> ids= new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();

        for(Hub hub : realm.where(Hub.class).findAll())
        {
            ids.add(hub.getHubId());
        }
        realm.close();
        return ids;
    }

    public Context getContext()
    {return context;}
    public String getCollege()
    {
      return  defaultSharedPref.getString(KEY_COLLEGE,"Natural Sciences");
    }
    public String getTimeTableVersion()
    {
        return pref.getString(KEY_TIMETABLE_VERSION,null);
    }

    /**
     * This will get all local feeds ids
     * This must be called on a background thread and not the main thread
     */
    public List<String> getFeedIDs() throws ParseException
    {


        ParseQuery<FeedItem> feedQuery= FeedItem.getQuery();
        List<String> feedIds= new ArrayList<>();
        List<FeedItem> feeds=feedQuery.fromLocalDatastore().find();
        for(FeedItem feed: feeds)
        {
            feedIds.add(feed.getId());
        }
        return feedIds;

    }

    public List<String> getMessageIDs()
    {
        SQLiteDatabase db= databaseManager.getReadableDatabase();
        Cursor cursor=db.rawQuery("SELECT * FROM " + DataProvider.TABLE_MESSAGES, null);
        List<String> feedIds= new ArrayList<>();
        if(cursor.moveToFirst())
        {
            while(!cursor.isAfterLast())
            {
                String feedId= cursor.getString(cursor.getColumnIndex(DataProvider.COL_MESSAGE_ID));
                feedIds.add(feedId);
                cursor.moveToNext();
            }

            cursor.close();
            return feedIds;
        }


        else{
            return null;
        }
    }
    public List<String> getForbiddenUsernames() {
        String[] forbiddenUsernames =
                {"user", "lisa", "fuck", "pussy", "dick", "asshole", "wizard", "management", "jabu", "admin", "jabuwings_admin", "senate",
                        "bursary", "library", "delight", "security", "medical", "medical_centre", "cafeteria"
                        , "student_union", "student_affairs",
                        "compliance", "registrar", "it_unit","chaplain","jabuwings","you"};

        return Arrays.asList(forbiddenUsernames);
    }

    public List<String> getCompulsoryCourses()
    {
        String[] courses={"CMS","GAME","MRND","TRYN"};

        return Arrays.asList(courses);
    }
    public List<String> getUserCourses() {
        return parseUser.getList(COURSES);
    }


    /**
     *
     * @return true if the device is online
     */
    public boolean isDeviceOnline(Activity context)
    {
        ConnectivityManager cm =(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo= cm.getActiveNetworkInfo();
        if(netInfo!=null && netInfo.isConnected())
            return  true;
        else
        {
            errorToast("You are not connected to the internet.");
            return false;
        }
    }

    public void cacheFailedNetwork(){

    }
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }



    public boolean isBlocked() {
        return parseUser.getBoolean(BLOCKED);
    }

    public boolean isSessionBlocked()
    {
        return pref.getBoolean(IS_SESSION_BLOCKED, false);
    }

    public boolean isUserEmailVerified(){
        return parseUser.getBoolean(EMAIL_VERIFIED);
    }

    @Deprecated
    public boolean isUserAdminOfHub(String hubId)
    {
        return false;
    }

    public boolean isAppRated(){return pref.getBoolean(KEY_RATED,false);}
    public boolean isCustomTimeTable()
    {
        return pref.getBoolean(KEY_CUSTOM_TIMETABLE,false);
    }
    public boolean isCustomBackground(){return pref.getBoolean(KEY_CHAT_BACKGROUND,false);}

    public boolean isInstagramAuthenticated(){return parseUser.getBoolean("instaA");}
    public void authenticateInstagram()
    {
        String uri="https://www.jabuwings.com/instagram_reg?i="+getUserId();
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://api.instagram.com/oauth/authorize/?client_id=52b1e033dba44e7681576e6fc6669afb&redirect_uri="+uri+"&response_type=code"));
        context.startActivity(browserIntent);

    }
    public void setProfilePictureDate(long newDate)
    {
        editor.putLong(KEY_PROFILE_PICTURE_DATE,newDate);
        commit();
    }
    public void setUserBlocked()
    {
        parseUser.put(BLOCKED, true);
        commitUser();

    }
    public void setIntroViewed()
    {
        editor.putBoolean(KEY_INTRO_VIEWED,true);
        commit();
    }

    public void setCollege(String collegeCode)
    {
        Editor editor = defaultSharedPref.edit();
        editor.putString(KEY_COLLEGE, collegeCode);
        commit(editor);
    }

    public void setUserSessionBlocked(boolean blocked)

    {
        editor.putBoolean(IS_SESSION_BLOCKED, blocked);
        commit();
    }

    public void setTimeTableVersion(String version)
    {
        editor.putString(KEY_TIMETABLE_VERSION, version);
        commit();
    }

    public void setCustomBackground(boolean state)
    {
        editor.putBoolean(KEY_CHAT_BACKGROUND,state);
        commit();
    }
    public void setCustomTimetable(boolean state)
    {
        editor.putBoolean(KEY_CUSTOM_TIMETABLE, state);
        commit();
    }

    /**
     * Check if there is an existence of a friend present in the online database but not on the local database. An attempt is then made to get the friend.
     */
    public void checkFriends()
    {
        List<String> databaseFriends= databaseManager.getFriends();
        List<String> onlineFriends=getUserFriends();

        if(databaseFriends!=null && onlineFriends!=null)

        for(String friend : onlineFriends)
        {
            if(!databaseFriends.contains(friend))
            {
                UserQuery.getUserFriend(friend,context);
            }
        }
    }

    public void checkHubs()
    {
        List<String> databaseHubs= databaseManager.getHubs();
        List<String> onlineHubs=getUserHubs();

        if(databaseHubs!=null && onlineHubs!=null)

            for(String hub : onlineHubs)
            {
                if(!databaseHubs.contains(hub))
                {
                    UserQuery.getHub(hub, context);
                }
            }
    }

    public void logout(final Activity activity) {
        Bundle bundle = new Bundle();
        bundle.putString(Const.USERNAME, getUsername());
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, getUserId());
        mFirebaseAnalytics.logEvent("logout", bundle);

        ParseUser.logOutInBackground();
        ParseInstallation.getCurrentInstallation().deleteEventually();
        ParseObject.unpinAllInBackground();
        deactivateLectureReminderService();
        deactivateRealTimeMessagingService();
        editor.putBoolean(IS_LOGIN, false);
        commit();
        Intent intent = new Intent(context, Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        activity.finish();


    }

    @UiThread
    public void shortToast(String message)
    {
        TastyToast.makeText(context,message,TastyToast.LENGTH_SHORT,TastyToast.INFO);
//        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @UiThread
    public void shortToast(int msgResId)
    {
        TastyToast.makeText(context,context.getString(msgResId),TastyToast.LENGTH_SHORT,TastyToast.INFO);
//        Toast.makeText(context, msgResId, Toast.LENGTH_SHORT).show();
    }

    @UiThread
    public void longToast(int msgResId)
    {
        TastyToast.makeText(context,context.getString(msgResId),TastyToast.LENGTH_LONG,TastyToast.INFO);
//        Toast.makeText(context, msgResId, Toast.LENGTH_LONG).show();
    }

    @UiThread
    public void longToast(String message)
    {
        TastyToast.makeText(context,message,TastyToast.LENGTH_LONG,TastyToast.INFO);
//        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    @UiThread
    public void successToast(String message)
    {
        TastyToast.makeText(context,message,TastyToast.LENGTH_SHORT,TastyToast.SUCCESS);
//        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @UiThread
    public void errorToast(String message)
    {
        TastyToast.makeText(context,message,TastyToast.LENGTH_SHORT,TastyToast.ERROR);
//        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * This logs out the user without displaying any dialog.
     */
    public void outrightLogOut()
    {

        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                try{
                    editor.putBoolean(IS_LOGIN, false);
                    commit();
                    ParseUser.logOut();
                //ParseInstallation.getCurrentInstallation().deleteEventually();
                    ParseObject.unpinAll();
                }
                catch (ParseException e)
                {
                    e.printStackTrace();
                }

                deactivateLectureReminderService();
                deactivateRealTimeMessagingService();
                return null;
            }


            @Override
            protected void onPostExecute(Void aVoid) {
                Intent intent = new Intent(context, Login.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


                if(activity!=null) {
                    activity.finish();
                    activity.startActivity(intent);
                }
            }
        }.execute();


    }

    public void deletePreferences()
    {
        editor.clear().commit();
        defaultSharedPref.edit().clear().commit();

    }
    public void logout(final SweetAlertDialog dialog)

    {
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                try{
                    editor.putBoolean(IS_LOGIN, false);
                    commit();
                    ParseUser.logOut();
                    //ParseInstallation.getCurrentInstallation().deleteEventually();
                    ParseObject.unpinAll();
                }
                catch (ParseException e)
                {
                    e.printStackTrace();
                }

                deactivateLectureReminderService();
                deactivateRealTimeMessagingService();
                return null;
            }


            @Override
            protected void onPostExecute(Void aVoid) {
                dialog.dismiss();
                Intent intent = new Intent(context, Login.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


                if(activity!=null) {
                    activity.finish();
                    activity.startActivity(intent);
                }
            }
        }.execute();
    }


    public void storeProfilePictureUrl(String url) {

        editor.putString(KEY_PROFILE_PICTURE_URL, url);
        commit();

    }

    /**
     *
     * @param username The username of the friend
     * @param date The new date to be stored
     */
    public void storeFriendProfilePictureFileDate(String username, long date)
    {
        databaseManager.updateFriendProfilePictureDate(username, date);
    }

    public void storeHubProfilePictureFileDate(String hub_id, long date)
    {
        databaseManager.updateHubProfilePictureDate(hub_id, date);
    }


    public void storeLastHymn(String hymnNo)
    {
        editor.putString(KEY_LAST_HYMN,hymnNo);
        commit();
    }

    public void storeLastEntityDraft(String id,String msg)
    {
        if(msg!=null) {
            editor.putString(id + "_" + KEY_LAST_ENTITY_DRAFT, msg);
            commit();
        }
        else{
            editor.remove(id + "_" + KEY_LAST_ENTITY_DRAFT);
            commit();
        }
    }

    public void storeChatBackground(int no)
    {
        editor.putInt("background",no);
        commit();
    }

    public int getChatBackground()
    {
       return pref.getInt("background",5);
    }
    public void storeFeedDraft(String title,String content,String url)
    {
        if(url!=null)
        if(!TextUtils.isEmpty(title) && !TextUtils.isEmpty(content) && !TextUtils.isEmpty(url))
        editor.putString(KEY_FEED_DRAFT,title+ Utils.strSeparator+content+Utils.strSeparator+url);
        else editor.remove(KEY_FEED_DRAFT);
        else{
            if(!TextUtils.isEmpty(title)&& !TextUtils.isEmpty(content))
            editor.putString(KEY_FEED_DRAFT,title+ Utils.strSeparator+content);
            else editor.remove(KEY_FEED_DRAFT);
        }
        commit();
    }

    public String getFeedDraft() {return pref.getString(KEY_FEED_DRAFT,"");}
    public String getLastHymn()
    {
      return  pref.getString(KEY_LAST_HYMN,"001");
    }
    public int getAppOpenings(){return  pref.getInt(KEY_APP_OPENINGS,0);}
    public boolean dontShowRating(){return pref.getBoolean(KEY_DONT_SHOW_RATING,false);}
    public void deletePreviousInstallation() {
        ParseQuery<ParseInstallation> query = ParseInstallation.getQuery().whereEqualTo(USERNAME, getUsername());
        query.findInBackground(new FindCallback<ParseInstallation>() {
            @Override
            public void done(List<ParseInstallation> list, ParseException e) {
                for (ParseInstallation installation : list) {
                    installation.deleteEventually();
                }


            }
        });
    }

    public void setUserOnline()
    {
        parseUser.put(ONLINE,true);
        parseUser.saveEventually();
    }
    public void setUserOffline()
    {
        parseUser.put(ONLINE,false);
        parseUser.put(LAST_SEEN, new Date());
        parseUser.saveEventually();
    }

    public List<String> getPushChannels()
    {
        String userTypeChannel="";
        switch (getUserType())
        {
            case 0:
                userTypeChannel="student"+NOTIFICATION_CHANNEL;
                break;
            case 1:
                userTypeChannel="staff"+NOTIFICATION_CHANNEL;
                break;
            case 2:
                userTypeChannel="alumni"+NOTIFICATION_CHANNEL;
                break;
            case 3:
                userTypeChannel="priviledged"+NOTIFICATION_CHANNEL;
                break;
            case 4:
                userTypeChannel="management"+NOTIFICATION_CHANNEL;
                break;
        }
        List<String> channels=new ArrayList<>();
        if(getUsername()!=null) channels.add(getUsername()+NOTIFICATION_CHANNEL);
        if(getMatricNo()!=null)channels.add(getMatricNo()+NOTIFICATION_CHANNEL);
        if(getDepartment()!=null)channels.add(getDepartment()+NOTIFICATION_CHANNEL);
        if(getUserId()!=null){channels.add(getUserId()+NOTIFICATION_CHANNEL);}
        if(getLevel()!=null) channels.add(getLevel()+NOTIFICATION_CHANNEL);
        channels.add(userTypeChannel);
        channels.add(APP_NAME.toLowerCase());
        channels.add(APP_NAME.toLowerCase()+context.getString(R.string.version_number));


        if(getUserHubs()!=null &&getUserHubs().size()>0)
            for(String hub_id: databaseManager.getHubs())
            {
                channels.add(hub_id+NOTIFICATION_CHANNEL);
            }

        return channels;
    }

    public ParseInstallation getInstallationToSave()
    {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.remove("subscriptions");
        installation.addAllUnique("subscriptions", getPushChannels());
        installation.put(USERNAME,getUsername());
        installation.put(USER,getUser());
        installation.put(Const.DEVICE_NAME, Utils.getDeviceName());
        return installation;
    }
    public void setUpPush(){

        getInstallationToSave().saveInBackground();

    }


    public void retryFileDownload(final Message message)
    {
        String id = message.getId();
        ParseQuery.getQuery(Const.CHATS_CLASS).fromLocalDatastore().getInBackground(id, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if(e==null)
                {
                    HouseKeeping.getMedia(context,object,message.getSender(),0);
                }
            }
        });

    }

    public void setUpRealTimeMessagingService()
    {
        //Real time service always activated from v2
        activateRealTimeMessagingService();
    }
    public void setUpLectureReminder()
    {
        if(isTimetableNotificationEnabled())
        {
            activateLectureReminderService();
        }

        else {
            deactivateLectureReminderService();
        }
    }

    public void activateLectureReminderService(){
        context.startService(new Intent(context, LectureService.class));
    }

    public void activateRealTimeMessagingService()
    {
        context.startService(new Intent(context, RealTimeMessagingService.class));
    }

    public void deactivateLectureReminderService() {
        context.stopService(new Intent(context, LectureService.class));
    }

    public void deactivateRealTimeMessagingService(){
        context.stopService(new Intent(context, RealTimeMessagingService.class));
    }

    public void addFriend(String username)
    {
        parseUser.addUnique(FRIENDS,username);
        commitUser();
    }

    public void removeRequest(String username)
    {
        String[] friend = {username};
        parseUser.removeAll(REQUESTS,Arrays.asList(friend));

        ParseRelation<ParseUser> requests=parseUser.getRelation("Requests");
        //requests.remove();
        parseUser.saveEventually();
    }


    public void addHub(String hub_id)
    {
        parseUser.addUnique(HUBS,hub_id);
        commitUser();
    }

    public void addLisa()
    {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Friend friend  = realm.createObject(Friend.class,Const.LISA);
                friend.setName(Const.LISA);
                friend.setDepartment("Computer Science");
                friend.setLevel("As far as you can think");
                friend.setStatus(context.getString(R.string.status_lisa));
                friend.setState("JabuWings");
                friend.setType(0);
                friend.setFriendShipStatus(Const.FRIENDSHIP_STATUS.ACTIVE.ordinal());
//                friend.setPicture(parseFile.getUrl());
                friend.setVerifiedAs("Lisa, the JabuWings robot");

            }
        });
        realm.close();

      /*  ContentValues values = new ContentValues(10);
        values.put(DataProvider.COL_NAME,  "Lisa");
        values.put(DataProvider.COL_USERNAME,  "lisa");
        values.put(DataProvider.COL_DEPARTMENT, "Computer Science");
        values.put(DataProvider.COL_BIRTHDAY,  "Sep 03");
        values.put(DataProvider.COL_STATUS,  context.getString(R.string.status_lisa));
        values.put(DataProvider.COL_LEVEL, "As far as you can think");
        values.put(DataProvider.COL_STATE, APP_NAME);
        values.put(DataProvider.COL_FRIEND_OF,new Manager(context).getUsername());
        values.put(DataProvider.COL_PROFILE_PICTURE_URL, "");
        values.put(DataProvider.COL_VERIFIED_AS,"Lisa, the JabuWings robot");
        context.getContentResolver().insert(DataProvider.CONTENT_URI_FRIENDS, values);
        context.getContentResolver().notifyChange(DataProvider.CONTENT_URI_FRIENDS,null);}*/

    }
    public void storeFriendProfilePictureUrl(String username, String Url)
    {
        databaseManager.updateFriendProfilePictureUrl(username, Url);
    }
    public void storeHubProfilePictureUrl(String title, String Url)
    {
        databaseManager.updateHubProfilePictureUrl(title, Url);
    }



    /**
     *
     * @param feedId The unique identification token obtained from the server
     * @param id the local database id of the feed
     */
    public void storeFeedDatabaseId(String feedId, int id)
    {
        editor.putInt(feedId+ID,id);
        commit();
    }


   //TODO implement to server
    public Manager removeFriend(String username) {
        editor.remove(username + ID);
        commit();
        return this;
    }

    public void enableBootService()
    {
        ComponentName receiver = new ComponentName(context, BootHub.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

    }




    public void updateProfilePicture()
    { if(parseUser.getParseFile(PROFILE_PICTURE)!=null)
        parseUser.getParseFile(PROFILE_PICTURE).getDataInBackground(new GetDataCallback() {
            @Override
            public void done(byte[] bytes, ParseException e) {
                if(e==null)
                {
                    try {
                        FileOutputStream outputStream = context.openFileOutput(PROFILE_PICTURE_NAME, Context.MODE_PRIVATE);
                        outputStream.write(bytes);
                        setProfilePictureDate(getUserProfilePictureDate());
                        Domicile.refreshPicture();
                    } catch (IOException ee) {
                        ee.printStackTrace();
                    }
                }
            }
        });

    }
    public void storeLatestFeed(long createdAt)
    {
        if(createdAt>pref.getLong(KEY_LATEST_FEED_CREATED_AT,0))
        {
            editor.putLong(KEY_LATEST_FEED_CREATED_AT, createdAt);
            commit();
        }
    }

    public void storeLatestMessageDate(long milliseconds)
    {
        editor.putLong(KEY_LATEST_MESSAGE_DATE,milliseconds);
        commit();
    }

    public void copyToClipboard(String message)
    {
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip= ClipData.newPlainText("Copied from JabuWings",message);
        clipboardManager.setPrimaryClip(clip);
    }
    public  boolean isAppInBackground() {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }


    public boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if(status != ConnectionResult.SUCCESS) {
            if(googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
                Editor editor = defaultSharedPref.edit();
                editor.putString(KEY_MESSAGE_DELIVERY_TYPE,"1");
                editor.apply();
            }
            return false;
        }
        return true;
    }

    public void showRateUsDialog()
    {
        Dialog.Builder builder;
        builder = new SimpleDialog.Builder(R.style.SimpleDialog){

            @Override
            protected void onBuildDone(final Dialog dialog) {
                dialog.layoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                final RatingBar ratingBar=(RatingBar) dialog.findViewById(R.id.ratingBar);
                Button rate=(Button)dialog.findViewById(R.id.rate_us_on_google_play);
                rate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        editor.putBoolean(KEY_RATED,true);
                        commit();
                        directToGooglePlay();
                    }
                });
                Button notNow=(Button)dialog.findViewById(R.id.rate_us_not_now);
                notNow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });
                TextView dontShowAgain=(TextView) dialog.findViewById(R.id.rate_us_dont_show_again);
                dontShowAgain.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        editor.putBoolean(KEY_DONT_SHOW_RATING,true);
                        commit();

                    }
                });
                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                        storeRating(rating);
                    }
                });







            }

            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {

                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {

                super.onNegativeActionClicked(fragment);
            }
        };

        builder.title("Rate Us")
                .contentView(R.layout.rate_us);
        DialogFragment rateFragment = DialogFragment.newInstance(builder);
        rateFragment.show(activity.getSupportFragmentManager(), null);

    }
 
    /**
     * WARNING. Only use this when the manager object was constructed with {@code activity}
     */
    public void showTerms()
    {
        new MaterialDialog.Builder(activity)
                .title(R.string.terms_and_conditions)
                .content(R.string.terms)
                .positiveText(R.string.agree)
                .cancelable(false)
                .negativeText(R.string.disagree)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        activity.finish();
                        Uri packageUri = Uri.parse("package:com.jabuwings");
                        Intent uninstallIntent =
                                new Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageUri);
                        activity.startActivity(uninstallIntent);

                    }
                })
                .show();

    }
    public void directToGooglePlay()
    {
        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }

    }



    public  Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(
                getUsername(), "com.jabuwings.datasync");
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        Context.ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
        } else {
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */
        }
        return newAccount;
    }
    private void storeRating(float rating)
    {
        editor.putFloat("rating",rating);
        commit();
    }
    //Preferences
        public boolean isTimetableNotificationEnabled()
        {
            return   defaultSharedPref.getBoolean(KEY_TIMETABLE_NOTIFICATION, false);
        }

        public void newLoginSetTimetableEnabled(){


        }

        public boolean isRealTimeMessagingServiceEnabled()
        {
            return defaultSharedPref.getString(KEY_MESSAGE_DELIVERY_TYPE,"1").equals("1");
        }
    public void dial(String no)
    {

        try{
        activity.startActivity(new Intent(Intent.ACTION_CALL,ussdToURI(no)));

        }
        catch (SecurityException e)
        {
            shortToast("JabuWings was denied permission to make call.");
        }
    }

    private Uri ussdToURI(String no)
    {
        String uriString="";
        if(!no.startsWith("tel:")) uriString="tel:";
        for(char c: no.toCharArray())
        {
            if(c=='#') uriString= Uri.encode("#"); else uriString+=c;
        }
        return Uri.parse(uriString);
    }

    private void initialiseChromePlugin()
    {
        // Package name for the Chrome channel the client wants to connect to. This
// depends on the channel name.
// Stable = com.android.chrome
// Beta = com.chrome.beta
// Dev = com.chrome.dev
         final String CUSTOM_TAB_PACKAGE_NAME = "com.android.chrome";  // Change when in stable

        CustomTabsServiceConnection connection = new CustomTabsServiceConnection() {
            @Override
            public void onCustomTabsServiceConnected(ComponentName name, CustomTabsClient client) {
                mCustomTabsClient = client;
                mCustomTabsClient.warmup(2634236);
                CustomTabsSession session =
                mCustomTabsClient.newSession(new CustomTabsCallback(){

                });

                String portalUrl=config.getString("jabu_portal");
                if(portalUrl==null)
                    portalUrl= Const.DEFAULT_PORTAL_URL;
                List<Bundle> bundles = new ArrayList<>();
                Bundle bundle = new Bundle();
                bundle.putParcelable(CustomTabsService.KEY_URL,Uri.parse(Const.DEFAULT_JABU_WEB_URL));
                bundles.add(bundle);
                if(session!=null)
                session.mayLaunchUrl(Uri.parse(portalUrl),null,bundles);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        };
        boolean ok = CustomTabsClient.bindCustomTabsService(context, CUSTOM_TAB_PACKAGE_NAME, connection);
    }

    public void openInChrome(Uri uri)
    {
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(context.getResources().getColor(R.color.colorPrimary));
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(context, uri);
    }

    /**
     * This confirms the changes and writes to the preferences file
     */
    private void commit()
    {
        editor.commit();
    }

    private void commit(Editor editor)
    {
        editor.commit();
    }

    protected void commitUser(){parseUser.pinInBackground();}


    public void log(String tag,String message)
    {
        if(DEBUG_LOG)
        Log.d(tag,message);
    }

    public void log(String message)
    {
        if(DEBUG_LOG)
        Log.d(APP_NAME+" Logger",message);
    }
    
    public void reportError(String trace)
    {
        HashMap<String,Object> params = getDefaultCloudParams();
        params.put("trace",trace);
        ParseCloud.callFunctionInBackground("crashReporter",params);
    }

    public boolean arrayContains( int[]a , int n)
    {
        for(int x:a)
        {
            if(x==n) return true;
        }
        return false;
    }

    public void getAllIpsOnWifi() {
        new AsyncTask<Void, Void, Void>() {

            String TAG = "nstask";

            WeakReference<Context> mContextRef = new WeakReference<Context>(context);
            ;


            @Override
            protected Void doInBackground(Void... voids) {
                Log.d(TAG, "Let's sniff the network");

                try {
                    Context context = mContextRef.get();

                    if (context != null) {

                        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                        WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

                        WifiInfo connectionInfo = wm.getConnectionInfo();
                        int ipAddress = connectionInfo.getIpAddress();
                        String ipString = Formatter.formatIpAddress(ipAddress);


                        Log.d(TAG, "activeNetwork: " + String.valueOf(activeNetwork));
                        Log.d(TAG, "ipString: " + String.valueOf(ipString));

                        String prefix = ipString.substring(0, ipString.lastIndexOf(".") + 1);
                        Log.d(TAG, "prefix: " + prefix);

                        for (int i = 0; i < 255; i++) {
                            String testIp = prefix + String.valueOf(i);

                            InetAddress address = InetAddress.getByName(testIp);
                            boolean reachable = address.isReachable(1000);
                            String hostName = address.getCanonicalHostName();

                            if (reachable)
                                Log.i(TAG, "Host: " + String.valueOf(hostName) + "(" + String.valueOf(testIp) + ") is reachable!");
                            if(isPortOpen(testIp,1337,1000))
                                log("Ip "+testIp+" has port 1337 open");
                        }
                    }
                } catch (Throwable t) {
                    Log.e(TAG, "Well that's not good.", t);
                }

                return null;
            }
        }.execute();

    }

    public boolean isPortOpen(final String ip, final int port, final int timeout) {

        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(ip, port), timeout);
            socket.close();
            return true;
        }

        catch(ConnectException ce){
            ce.printStackTrace();
            return false;
        }

        catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    public void getConnectedWifi()
    {
       /* new AsyncTask<Void,Void,String>(){
            @Override
            protected String doInBackground(Void... params) {

                WifiManager wifii= (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                DhcpInfo d=wifii.getDhcpInfo();
                String connections = "";
                InetAddress host;
                try
                {
                    host = InetAddress.getByName(intToIp(d.dns1));

                    *//*String h=host.getHostName();
                    String[] t= h.split(".");
                    String j= t[0]+"."+t[1]+"."+t[2];
                    String r =j+".101";

                    InetAddress.getByAddress()*//*

                    byte[] ip = host.getAddress();

                    ip[3] = (byte) 101;
                    InetAddress address = InetAddress.getByAddress(ip);
                    if(address.isReachable(100))
                    {
                        log(address + " machine is turned on and can be pinged");
                        connections+= address+"\n";
                    }
                    else if(!address.getHostAddress().equals(address.getHostName()))
                    {
                        log(address + " machine is known in a DNS lookup");
                    }

                  *//*  for(int i = 1; i <= 254; i++)
                    {
                        ip[3] = (byte) i;
                        InetAddress address = InetAddress.getByAddress(ip);
                        if(address.isReachable(100))
                        {
                            log(address + " machine is turned on and can be pinged");
                            connections+= address+"\n";
                        }
                        else if(!address.getHostAddress().equals(address.getHostName()))
                        {
                            log(address + " machine is known in a DNS lookup");
                        }

                    }*//*
                }
                catch(UnknownHostException e1)
                {
                    e1.printStackTrace();
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }
                log(connections);

                return connections;
            }

            @Override
            protected void onPostExecute(String s) {
                shortToast("Wifi");
                shortToast(s);
                log("Wifi");
                log(s);
            }
        }.execute();*/

    }

    public String intToIp(int i)
    {
        return ( i & 0xFF) + "." +
                ((i >> 8 ) & 0xFF) + "." +
                ((i >> 16 ) & 0xFF) + "." +
                ((i >> 24 ) & 0xFF );

    }
    public String intToIp2(int i) {

        return ((i >> 24 ) & 0xFF ) + "." +
                ((i >> 16 ) & 0xFF) + "." +
                ((i >> 8 ) & 0xFF) + "." +
                ( i & 0xFF) ;
    }

}

