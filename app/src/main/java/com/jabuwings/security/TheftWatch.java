package com.jabuwings.security;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Falade James on 10/18/2015 All Rights Reserved.
 */
public class TheftWatch extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        String[] packageNames= intent.getStringArrayExtra("android.intent.extra.PACKAGES");

        if(packageNames!= null)
        {
            for (String packageName: packageNames)
            {
                if(packageName!=null && packageName.equals("com.jabuwings"))
                new Listener(context).start();
            }
        }



    }
}
